trigger deactivateOneOffUser on Partner_Application__c (after update) {
    
    Set<Id> accountIdSet = new Set<Id>();
    Set<Id> accountsToEmail = new Set<Id>();
    Set<Id> allAccounts =  new Set<Id>();
    String[] changeProfileList = new List<String>();
    
    for (Partner_Application__c partnerApp : trigger.new) {
        if (partnerApp.Application_Status__c.equalsIgnoreCase('Approved')&& partnerApp.Application_Status__C!=trigger.oldMap.get(partnerApp.Id).Application_Status__C) {
            if (partnerApp.One_Time_User__c) {
                accountIdSet.add(partnerApp.Account__c);
            } else {
                accountsToEmail.add(partnerApp.Account__c);
            }
            allAccounts.add(partnerApp.Account__c);
        }
    }
    
    System.debug('accountIdSet Size = ' + accountIdSet.size());
    if (! allAccounts.isEmpty()) {

        List<User> userList = [Select Id, ContactId, Contact.AccountId from User where Contact.AccountId In :allAccounts and IsActive = true];
        
        String[] usersToDeactivate = new List<String>();
        List<Id> mailingList = new List<Id>();
        for (User u : userList) {
            
            if (accountIdSet.contains(u.Contact.AccountId)) {
                usersToDeactivate.add(u.Id);
            } else {
                mailingList.add(u.ContactId);
                changeProfileList.add(u.Id);
            }
        }
        
        if (! mailingList.isEmpty()) {
            SendEmailNotifications.sendEmails(mailingList);
            DeactivateOneOffUser.changeUserProfile(changeProfileList);
        }
        
        System.debug('usersToDeactivate Size = ' + usersToDeactivate.size());
        if (! usersToDeactivate.isEmpty()) {
            //call it in a seperate method/class with the @future annotation to avoid the MIXED_DML_EXCEPTION
            DeactivateOneOffUser.deactivate(usersToDeactivate);
        }
        DeactivateOneOffUser.updatePartnerAccount(allAccounts);
    }

}