trigger PartnerSelfServiceUser on User (before insert, before update, after insert, after update) {
	if (!system.isBatch()) {
	/*    List<User> activeUsers = new List<User>(); //commenting out for Change Set to Prod
	    for(User u : trigger.new){
	        if(u.IsActive)    
	        activeUsers.add(u); 
	    }
	   	if(!activeUsers.isEmpty()){*/
	    
	        if(Trigger.isAfter == true){
	             
	            PartnerSelfService.processAfter(Trigger.New, Trigger.OldMap);
	            PartnerSelfService.flush(); 
	                
	        }else if(Trigger.isInsert == true){ 
	            
	            PartnerSelfService.process(Trigger.New, null, null);
	            PartnerSelfService.flush();     // added by Sukhdeep Singh, Ticket# 140917, 02/09/2013
	            
	        }else if(Trigger.isUpdate == true){
	            
	            PartnerSelfService.process(Trigger.New, Trigger.NewMap, Trigger.OldMap);
	    	}
	    //}
	}
}