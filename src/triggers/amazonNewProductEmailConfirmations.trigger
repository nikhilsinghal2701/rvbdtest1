trigger amazonNewProductEmailConfirmations on AmazonAccount__c (after update) 
{
	if(trigger.isAfter && trigger.isUpdate){
		ZeusUtility.amazonNewProductEmailConfirmations(trigger.new,trigger.oldMap);
	}
	
	/*START: Zeus original code. commented by prashant.riverbed.com on 10/18/2011
    void sendEmail(String subj, String msg, String rcpt)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] rcptArr = new String[] { rcpt };

        email.setSenderDisplayName('Zeus Technology');         
        email.setReplyTo('noreply@zeus.com');
        email.setSaveAsActivity(false);
        email.setToAddresses(rcptArr);
        email.setSubject(subj);
        email.setPlainTextBody(msg);
        
        Messaging.SendEmailResult[] sendResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }

    AmazonAccount__c pre = trigger.old[0];
    AmazonAccount__c post = trigger.new[0];
    String msg;
    String subj;
    String rcpt = post.ContactEmail__c;
    String accNo = [ SELECT AccountNumber FROM Account WHERE Id = :post.Account__c ][0].AccountNumber;
    boolean emailRequired = false;
    
    Integer preSupportLength = (pre.Support__c == null) ? 0 : pre.Support__c.length();
    Integer postSupportLength = (post.Support__c == null) ? 0 : post.Support__c.length();
    Integer preProductsLength = (pre.Products__c == null) ? 0 : pre.Products__c.length();
    Integer postProductsLength = (post.Products__c == null) ? 0 : post.Products__c.length();
    
    if(preSupportLength < postSupportLength)
    {
        emailRequired = true;
        subj = 'Welcome to Riverbed Support';
        msg = 'Welcome to Riverbed Support. In the following email you will find\n';
        msg += 'information relating to the Support services, their operation, how you\n';
        msg += 'contact support, and procedural details.\n';                              
        msg += '\n';                                                                      
        msg += 'To enable the Riverbed Support Team to deal with your requests in a timely\n';
        msg += 'and efficient manner, it is essential that the processes are adhered\n';  
        msg += 'to, any deviation may cause delays to your request.\n';                   
        msg += '\n'; 
        msg += 'Whenever you contact Zeus Support, please quote your account number which is ' + accNo + '.\n';                                                                     
        msg += '\n';                                                                      
        msg += 'Knowledge Hub\n';                                                         
        msg += '================\n';                                                      
        msg += 'A wealth of constantly reviewed and revised information on Product\n';    
        msg += 'Usage, common scenarios, and Feature articles is available at:\n';        
        msg += '\n';                                                                      
        msg += 'http://knowledgehub.zeus.com/\n';                                         
        msg += '\n';                                                                      
        msg += 'Contract Types\n';                                                        
        msg += '==============\n';                                                        
        msg += 'You will have either a Standard or Premium support contract, please see\n';
        msg += '\n';                                                                       
        msg += 'http://www.zeus.com/support/packages\n';                                   
        msg += '\n';                                                                       
        msg += 'which details full explanations of service type, requirements and\n';      
        msg += 'variations associated with contract levels.\n';                            
        msg += '\n';                                                                       
        msg += 'Technical Services\n';                                                     
        msg += '==================\n';                                                     
        msg += 'Details of Support procedures including escalation and Out of Hour\n';     
        msg += 'processes are covered in:\n';                                              
        msg += '\n';                                                                       
        msg += 'http://www.zeus.com/documents/en/ze/zeus_technical_support_services.pdf\n';
        msg += '\n';                                                                       
        msg += '\n';                                                                       
        msg += 'Contacting Support: UK & Rest of World\n';                                 
        msg += '==========================================\n';                             
        msg += 'Standard Contract: 08:00 to 23:59 hours GMT (Monday - Friday excluding\n'; 
        msg += 'UK Bank Holidays)\n';
        msg += 'Non-critial queries: email support@zeus.com\n';
        msg += '\n';
        msg += 'Critical queries - within support hours: email support@zeus.com - call\n';
        msg += '+44 1223 720 463 or 1-888-ZEUS-123 (USA)\n';
        msg += '\n';
        msg += 'Premium Contract: 08:00 TO 23:59 hours GMT (Monday - Friday excluding\n';
        msg += 'UK Bank Holidays)\n';
        msg += 'Non-critical queries: email support@zeus.com [Include your customer\n';
        msg += 'number in the subject header of the email]\n';
        msg += '\n';
        msg += 'Critical queries - within support hours: email support@zeus.com   - or\n';
        msg += 'call +44 1223 720 463 or 1-888-ZEUS-123 (USA)\n';
        msg += '\n';
        msg += 'Critical queries : Outside of support hours: call +44 1223 472 190 or\n';
        msg += '1-888-ZEUS-123 (USA) - quoting your customer number.\n';
        msg += '\n';
        msg += 'Contacting Support: US Customers\n';
        msg += '================================\n';
        msg += 'Standard Contract: 08:00 to 23:59 hrs UTC (Monday - Friday excluding UK\n';
        msg += 'Bank Holidays)\n';
        msg += 'Non-critial queries: email support@zeus.com [Include your customer\n';
        msg += 'number in the subject header of the email]\n';
        msg += '\n';
        msg += 'Critical queries - within support hours: email support@zeus.com - call\n';
        msg += '+1 650 965 4627 or 1-888-ZEUS-123 (USA)\n';
        msg += 'Premium Contract: 08:00 TO 23:59 hours UTC (Monday - Friday excluding\n';
        msg += 'UK Bank Holidays)\n';
        msg += 'Non-critical queries: email support@zeus.com\n';
        msg += '\n';
        msg += 'Critical queries - within support hours: email support@zeus.com   - or\n';
        msg += 'call +1 650 965 4627 or 1-888-ZEUS-123 (USA)\n';
        msg += '\n';
        msg += 'Critical queries : Outside of support hours: call +1 650 965 4627 or\n';
        msg += '1-888-ZEUS-123 (USA)\n';
        msg += '\n';
        msg += 'Customer Technical Team\n';
        msg += '======================\n';
        msg += 'Please provide up to 2 named individuals for Standard contracts, and up\n';
        msg += 'to 6 named individuals for Premium contracts, who will be authorised\n';
        msg += 'from your organisation to contact Zeus Support.\n';
        msg += '\n';
    }
    else if((preProductsLength < postProductsLength)
    || (pre.ContactEmail__c == null && post.ContactEmail__c != null))
    {
        emailRequired = true;
        subj = 'Thank you for purchasing a Riverbed product on Amazon EC2';
        msg = 'Thank you for signing up to use Riverbed software on Amazon EC2.\n\n'; 
        msg += 'What next?\n';
        msg += 'Refer to the EC2 Getting Started Guide at http://knowledgehub.zeus.com/docs '; 
        msg += 'to learn how to launch your software on Amazon EC2.\n\n';
        msg += 'Contact us:\n';
        msg += 'This subscription has been assigned to the Riverbed customer account ' + accNo + '.  '; 
        msg += 'If you have purchased support, please provide your account number in all correspondence.  ';
        
        if(! post.Products__c.contains(';'))
        {
            msg += 'If this is the first time you have subscribed to a Zeus product, you are entitled to 30 days\' free technical support, beginning now.  ';
            msg += 'To contact our support team, please email support@zeus.com and include your customer number (' + accNo + ') in the subject line.  ';
        }
        
        msg += 'If you would like to check your support status, or purchase support, please go to http://www.zeus.com/downloads/developers/ec2/choose_support.php\n\n';
        msg += 'If you have any questions, or need to update your account details, please send your account number ';
        msg += 'to orders@zeus.com.\n\n';
        msg += 'Regards,\n\n';
        msg += 'Riverbed Technology\n';
        msg += 'orders@zeus.com';
    }
    
    if(emailRequired && rcpt != null)
    {        
        sendEmail(subj, msg, rcpt);   
    }//END: Zeus original code
    */       
}