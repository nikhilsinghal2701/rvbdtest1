trigger OliValidations on OpportunityLineItem (before insert, before update, before delete) {
    
    Profile p = [select id from Profile where Name = 'System Administrator' limit 1];
    
    if(Trigger.isInsert || Trigger.isUpdate) {
        Set<Id> oppIds = new Set<Id>();
        for(OpportunityLineItem oli :trigger.new) {
            oppIds.add(oli.OpportunityId);
        }
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([select Sale_Type__c, StageName from Opportunity where id in :oppIds]);
        for(OpportunityLineItem oli :trigger.new) {
            if(UserInfo.getProfileId().substring(0, 15) == p.id || UserInfo.getUserName() == 'tibcointegration@riverbed.com.sfdc') {
                continue;
            } else {
                String saleType = oppMap.get(oli.OpportunityId).Sale_Type__c;
                String stgName = oppMap.get(oli.OpportunityId).StageName;
                if(saleType != null && saleType.equals('Conditional PO') && stgName != null && stgName.equals('5 - Negotiation & Closure'))
                    oli.addError(System.Label.OLI_Validation);
            }
        }
    }
    
    if(Trigger.isDelete) {
        Set<Id> oppIds = new Set<Id>();
        for(OpportunityLineItem oli :trigger.old) {//changed from trigger.new to trigger.old
            oppIds.add(oli.OpportunityId);
        }
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([select Sale_Type__c, StageName from Opportunity where id in :oppIds]);
        for(OpportunityLineItem oli :trigger.old) {
            if(UserInfo.getProfileId().substring(0, 15) == p.id || UserInfo.getUserName() == 'tibcointegration@riverbed.com.sfdc') {
                continue;
            } else {
                String saleType = oppMap.get(oli.OpportunityId).Sale_Type__c;
                String stgName = oppMap.get(oli.OpportunityId).StageName;
                if(saleType != null && saleType.equals('Conditional PO') && stgName != null && stgName.equals('5 - Negotiation & Closure'))
                    oli.addError(System.Label.OLI_Validation);
            }
        }   
    }
}