/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger UnderEvalAssets on Opportunity (before update) {
List<Asset>AstList=new List<Asset>();
List<opportunity>opptyList=new List<opportunity>();
List<Account>actList=new List<Account>();
Id[]searchID= new Id[1];
Id[]actsearchID= new Id[1];
Boolean doUpdate = false ;
if (Trigger.isBefore){
    if(trigger.isUpdate){
        util.salesOrderQuoteOnOppty(Trigger.newMap); //This method update Sales_Order_Quote__c on Opportunity based Used_For_Ordering__c in quotes.
        Util.OpptyPrimaryContactGID(Trigger.newMap,Trigger.oldMap,Trigger.new); //Added By Anil 10.17.2013 RFC::RFC#6215
        //Util.ProdSplintoSalesTeam(Trigger.newMap,Trigger.oldMap,Trigger.new);
        for(opportunity o:Trigger.new){
            opportunity o1=Trigger.oldMap.get(o.id);        
            if(o1.StageName !=o.StageName){ 
                if( o.StageName =='7 - Closed (Not Won)'&& o.Close_Eval__c !=True ){
                    opptyList.add(o);
                    //System.debug('old+++++++++'+o1.StageName+'new++++++++++++++++'+o.StageName);
                    //System.debug('OppList+++++++++'+opptyList);
                }else{
                    doUpdate = true ;
                }
            }
        }
        if(opptyList.size()>0){
            for(Opportunity o2 :opptyList){
                searchID[0]=o2.Id;
                System.debug('optyID+++++++++'+o2.ID);
                for(opportunity oty :[select id, accountId from opportunity where id in :searchID]){
                    actsearchID[0]=oty.accountId;
                    for(Asset ast:[select id from asset where IB_Status__c='Under Evaluation' and Asset.Opportunity__c=:o2.ID]){
                        AstList.add(ast);
                        //System.debug('AstID+++++++++'+AstList);                                       
                    }
                    if(!AstList.isEmpty()){
                        o2.addError('You cannot close this opportunity as there are existing evals for this customer. \n Click the button "Close Eval" to close this opportunity.');
                    }else{
                        doUpdate = true ;
                    }
                }
             }
         }
      }
   }
   if (Trigger.isBefore){
    if(trigger.isUpdate){
        Util.ProdSplintoSalesTeam(Trigger.newMap,Trigger.oldMap,Trigger.new);
        }
    }
    
}