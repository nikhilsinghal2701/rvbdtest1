trigger OpportunityInsertBefore on Opportunity (before insert,before update,after insert,after update)
{   /* Started on Oct 08 2009
        Type:before/after
        Action:insert/update
        1.To Auto Populate the ordering center,Sold to Partner Type,Sold to Partner Level,
        Sold to Partner Authorization,Tier2 Type,Tier2 Level,Tier2 Authorizarion on Opportunity.
        2.To add the ordering center,sold to Partner,Tier2 as partner related List.
        3.To add Sold to Partner User as sales team Member, as record inserted into Visible_to_Partners__c.
        By prashant.singh@riverbed.com
        */

        Partner partner;
        final string riverbedAccId='0015000000HoWN7AAN';
        final string riverbedAccName='Riverbed';
        Visible_to_Partners__c vPartner;
        List<Visible_to_Partners__c> lstVPartner=new List<Visible_to_Partners__c>();
        List<Partner> lstPartner=new List<Partner>();
        List<Partner> tempPat;
        List<Visible_to_Partners__c> stLst;
        set<Id> accountIds=new set<Id>();
        set<Id> oppIds=new set<Id>();
        Map<Id,Account> accountMap;
        Map<Id,List<Partner>> partnerMap=new Map<Id,List<Partner>>();
        Map<Id,List<Visible_to_Partners__c>> salesTeamMap=new Map<Id,List<Visible_to_Partners__c>>();
        for(Opportunity opp:Trigger.new){
                if(opp.Sold_To_Partner__c!=null){accountIds.add(opp.Sold_To_Partner__c);}
                if(opp.Tier2__c!=null){accountIds.add(opp.Tier2__c);}
                if(opp.Tier3_Partner__c!=null){accountIds.add(opp.Tier3_Partner__c );}
                if(opp.Influence_Partner__c!=null){accountIds.add(opp.Influence_Partner__c);}
                oppIds.add(opp.Id);
        }
        if(Trigger.isBefore){
                if(Trigger.isInsert){
                        //moved the section to set support provider outside of if block since the opp might not have any partner associated.
                        /* Added by prashant.singh@riverbed.com on 8/1/2011
                        * for making Riverbed as default support provider.
                        */
                        list<UpliftCalculationMethod__c> UCM =[select name,active__c from UpliftCalculationMethod__c where active__c = true]; //added by nikhil on 6/12/2015
                        
                        for(Opportunity opp : trigger.new){
                                if(opp.Support_Provider__c==NULL){// || opp.Support_Provider__c=='')// commented by prashant.singh@riverbed.com on 11/9/2011 - this is throwing error when creating opportunity through webservices
                                        opp.Support_Provider__c=riverbedAccId;
                                }
                                if(opp.Support_Provided__c==NULL){ //|| opp.Support_Provided__c=='')// commented by prashant.singh@riverbed.com on 11/9/2011 - this is throwing error when creating opportunity through webservices
                                        opp.Support_Provided__c=riverbedAccName;
                                }
                                
                          if(!ucm.IsEmpty()){
                          	opp.Uplift_Calculation__c=ucm[0].name; //added by nikhil on 6/12/2015. this is to set uplift calculation
                          }
                        }
                        if(accountIds.size()>0){
                                accountMap=new Map<Id,Account>([Select Id, Distributor_Ordering_Center__c,Type,Partner_Level__c,Partner_Type__c,
                                Authorizations_Specializations__c,Phone,Geographic_Coverage__c,Region__c, ParentId,Allowed_Product_Families__c,Number_Of_Competencies__c  from Account where id in :accountIds]);//added parent to the query by ankita on 12/26/2009 for Disti 1B
                        }//modified by psingh@riverbed.com on 09/30/2013 -- Partner Program Project -- added 'Allowed_Product_Families__c'field in soql.
                        if(accountMap!=null && accountMap.size()>0){
                                for(Opportunity opp:Trigger.new){
                                        Util.setPartnerInfo(opp, accountMap);
                                    //  opp.Channel__c=Util.setChannelInfo(opp);//commented by Ankita on 8/16/2012 for Rev pro
                                }
                        }
                }
                if(Trigger.isUpdate){
                        Opportunity oldOpp;
                        Map<Id,Account> accMap;
                        set<Id> accIds=new set<Id>();
                        final Id supportProvider = '0015000000HoWN7';//[select Id,Name from Account where Name='Riverbed - internal'].Id;
                        for(Opportunity tempOpp:Trigger.new){
                                oldOpp=trigger.oldMap.get(tempOpp.Id);
                                //for defaulting support provider to Riverbed
                                if((tempOpp.Sold_To_Partner__c==null && tempOpp.Sold_To_Partner__c!=oldOpp.Sold_To_Partner__c) || (tempOpp.Tier2__c==null && tempOpp.Tier2__c!=oldOpp.Tier2__c)){
                                        tempOpp.Support_Provider__c=riverbedAccId;
                                        tempOpp.Support_Provided__c=riverbedAccName;
                                        tempOpp.Support_Provider_Auth2__c='';
                                }
                                if(tempOpp.Sold_To_Partner__c==null){
                                        //tempOpp.Support_Provider__c= supportProvider;//commented by psingh@riverbed.com for Partner Program Project
                                        //tempOpp.Support_Provided__c='Riverbed';//commented by psingh@riverbed.com for Partner Program Project
                                        tempOpp.Ordering_Center__c = null;
                                        tempOpp.Sold_To_Partner_Type__c='';
                                        tempOpp.Sold_To_Partner_Level__c='';
                                        tempOpp.Sold_to_Partner_Authorizations__c='';
                                        tempOpp.Sold_to_Partner_Phone__c='';
                                        //tempOpp.Tier2_Authorizations__c = '';
                                        //tempOpp.Tier3_Authorization__c = '';
                                        //tempOpp.Influence_Partner_Authorization__c = '';
                                        tempOpp.Sold_To_Partner_Parent__c = null;//added by ankita 9/16 for sold to partner parent not being nulled out
                                        tempOpp.Sold_to_Partner_Geo__c = null;
                                        tempOpp.Sold_to_Partner_Region__c = null;
                                        tempOpp.STP_Allowed_Product_Families__c='';
                                        tempOpp.STP_Number_of_Competencies__c=null;
                                }
                                if(tempOpp.Tier2__c==null){
                                        tempOpp.Tier2_Type__c='';
                                        tempOpp.Tier2_Level__c='';
                                        tempOpp.Tier2_Authorizations__c='';
                                        tempOpp.Tier2_Partner_Phone__c='';
                                        tempOpp.Tier2_Partner_Geo__c = '';
                                        tempOpp.Tier2_Partner_Region__c = '';
                                        tempOpp.Tier_2_Partner_Parent__c = null;
                                        tempOpp.Tier_2_Number_of_Competencies__c=null;
                                        tempOpp.Tier_2_Allowed_Product_Families__c='';
                                        tempOpp.Partner_Allowed_Product_Families__c=tempOpp.STP_Allowed_Product_Families__c;
                                }
                                if(tempOpp.Sold_To_Partner__c!=oldOpp.Sold_To_Partner__c){
                                accIds.add(tempOpp.Sold_To_Partner__c);
                                if(tempOpp.Tier2__c != null){ accIds.add(tempOpp.Tier2__c); }//added by Nikhil
                                }
                                if(tempOpp.Tier2__c!=oldOpp.Tier2__c)accIds.add(tempOpp.Tier2__c);
                                //added by Ankita 7/8/2010 for RASP
                                if(tempOpp.Tier3_Partner__c != oldOpp.Tier3_Partner__c){
                                accIds.add(tempOpp.Tier3_Partner__c);
                                if(tempOpp.Tier2__c != null){ accIds.add(tempOpp.Tier2__c); }//added by Nikhil
                                if(tempOpp.Sold_To_Partner__c != null){ accIds.add(tempOpp.Sold_To_Partner__c);}// Added by Nikhil
                                }

                                if(tempOpp.Influence_Partner__c != oldOpp.Influence_Partner__c){
                                accIds.add(tempOpp.Influence_Partner__c);
                                if(tempOpp.Tier2__c != null){ accIds.add(tempOpp.Tier2__c); }//added by Nikhil
                                if(tempOpp.Sold_To_Partner__c != null){ accIds.add(tempOpp.Sold_To_Partner__c);}// Added by Nikhil
                                }
                                //added by psingh@riverbed.com for Partner Program Project
                                if(tempOpp.Support_Provider__c!=null && tempOpp.Support_Provider__c != oldOpp.Support_Provider__c){
                                accIds.add(tempOpp.Support_Provider__c);
                                if(tempOpp.Tier2__c != null){ accIds.add(tempOpp.Tier2__c); }//added by Nikhil
                                if(tempOpp.Sold_To_Partner__c != null){ accIds.add(tempOpp.Sold_To_Partner__c);}// Added by Nikhil

                                }
                                if(tempOpp.Tier_2_Allowed_Product_Families__c != oldOpp.Tier_2_Allowed_Product_Families__c){
                                if(tempOpp.Tier2__c != null){ accIds.add(tempOpp.Tier2__c); }//added by Nikhil
                                if(tempOpp.Sold_To_Partner__c != null){ accIds.add(tempOpp.Sold_To_Partner__c);}// Added by Nikhil
                                }
                                if(tempOpp.STP_Allowed_Product_Families__c != oldOpp.STP_Allowed_Product_Families__c){
                                if(tempOpp.Tier2__c != null){ accIds.add(tempOpp.Tier2__c); }//added by Nikhil
                                if(tempOpp.Sold_To_Partner__c != null){ accIds.add(tempOpp.Sold_To_Partner__c);}// Added by Nikhil
                                }
                                //end added by Ankitas
                        }
                        if(accIds.size()>0){
                                accMap=new Map<Id,Account>([Select Id, Distributor_Ordering_Center__c,Type,Partner_Level__c,Partner_Type__c,
                                Authorizations_Specializations__c,Phone,Geographic_Coverage__c,Region__c, ParentId,Allowed_Product_Families__c,Number_Of_Competencies__c from Account where id in :accIds]);//added parent to the query by ankita on 12/26/2009 for Disti 1B
                        }
                        if(accMap!=null && accMap.size()>0){
                                for(Opportunity opp:Trigger.new){
                                        oldOpp=trigger.oldMap.get(opp.Id);
                                        //for defaulting support provider to Riverbed
                                        if(opp.Sold_To_Partner__c!=oldOpp.Sold_To_Partner__c||opp.Tier2__c!=oldOpp.Tier2__c){
                                                opp.Support_Provider__c=riverbedAccId;
                                                opp.Support_Provided__c=riverbedAccName;
                                                opp.Support_Provider_Auth2__c='';
                                        }
                                        if(opp.Sold_To_Partner__c==null){
                                                opp.Ordering_Center__c = null;
                                                opp.Sold_To_Partner_Type__c='';
                                                opp.Sold_To_Partner_Level__c='';
                                                opp.Sold_to_Partner_Authorizations__c='';
                                                opp.Sold_to_Partner_Phone__c='';
                                                //added by Ankita Goel 12/26/2009 Disti 1B
                                                opp.Sold_to_Partner_Geo__c = '';
                                                opp.Sold_to_Partner_Region__c = '';
                                                opp.Sold_To_Partner_Parent__c = null;
                                                opp.STP_Allowed_Product_Families__c='';
                                                opp.STP_Number_of_Competencies__c=null;
                                                //opp.Support_Provider__c='';
                                                //opp.Support_Provided__c='';
                                        }else if(opp.Sold_To_Partner__c!=oldOpp.Sold_To_Partner__c){
                                                if(accMap.get(opp.Sold_To_Partner__c).Distributor_Ordering_Center__c!=null)
                                                        {
                                                        opp.Ordering_Center__c = accMap.get(opp.Sold_To_Partner__c).Distributor_Ordering_Center__c;
                                                        } else
                                                        {
                                                        opp.Ordering_Center__c = null;
                                                        }
                                                if(accMap.get(opp.Sold_To_Partner__c).Type!=null)
                                                        opp.Sold_To_Partner_Type__c=accMap.get(opp.Sold_To_partner__c).Type;
                                                if(accMap.get(opp.Sold_To_Partner__c).Partner_Level__c!=null)
                                                        opp.Sold_To_Partner_Level__c=accMap.get(opp.Sold_To_partner__c).Partner_Level__c;
                                                if(accMap.get(opp.Sold_To_Partner__c).Authorizations_Specializations__c!=null)
                                                        opp.Sold_to_Partner_Authorizations__c=accMap.get(opp.Sold_To_partner__c).Authorizations_Specializations__c;
                                                if(accMap.get(opp.Sold_To_Partner__c).Phone !=null)
                                                        opp.Sold_to_Partner_Phone__c=accMap.get(opp.Sold_To_Partner__c).Phone;
                                                //added by Ankita Goel 12/26/2009 Disti 1B
                                                if(accMap.get(opp.Sold_To_Partner__c).Geographic_Coverage__c != null){
                                                        opp.Sold_to_Partner_Geo__c = accMap.get(opp.Sold_To_Partner__c).Geographic_Coverage__c;
                                                }else {
                                                        opp.Sold_to_Partner_Geo__c = '';
                                                }
                                                if(accMap.get(opp.Sold_To_Partner__c).Region__c != NULL){
                                                        opp.Sold_to_Partner_Region__c = accMap.get(opp.Sold_To_Partner__c).Region__c;
                                                }else{
                                                        opp.Sold_to_Partner_Region__c = '';
                                                }
                                                if(accMap.get(opp.Sold_To_Partner__c).ParentId != null){
                                                        opp.Sold_To_partner_Parent__c = accMap.get(opp.Sold_To_Partner__c).ParentId;
                                                }else{
                                                        opp.Sold_To_partner_Parent__c = null;//added by Ankita 9/16 for nulling out the parent
                                                }
                                                if(accMap.get(opp.Sold_To_Partner__c).Allowed_Product_Families__c!=null){
                                                        opp.STP_Allowed_Product_Families__c=accMap.get(opp.Sold_To_Partner__c).Allowed_Product_Families__c;
                                                }else{
                                                        opp.STP_Allowed_Product_Families__c=null;
                                                }
                                                if(accMap.get(opp.Sold_To_Partner__c).Number_Of_Competencies__c!=null){
                                                        opp.STP_Number_of_Competencies__c=accMap.get(opp.Sold_To_Partner__c).Number_Of_Competencies__c;
                                                }else{
                                                        opp.STP_Number_of_Competencies__c=null;
                                                }
                                        }
                                        if(opp.Partner_Closest_to_Customer_Id__c!=null && accMap.containsKey(opp.Partner_Closest_to_Customer_Id__c)){
                                                opp.Partner_Close_to_Customer_Authorizations__c=accMap.get(opp.Partner_Closest_to_Customer_Id__c).Authorizations_Specializations__c;
                                        }else{
                                                opp.Partner_Close_to_Customer_Authorizations__c=null;
                                        }
                                        if(opp.Tier2__c!=null && accMap.containsKey(opp.Tier2__c)){
                                                opp.Partner_Allowed_Product_Families__c=accMap.get(opp.Tier2__c).Allowed_Product_Families__c;
                                        }else if(opp.Sold_To_Partner__c!=null && accMap.containsKey(opp.Sold_To_Partner__c)){
                                                opp.Partner_Allowed_Product_Families__c=accMap.get(opp.Sold_To_Partner__c).Allowed_Product_Families__c;
                                        }else{
                                                opp.Partner_Allowed_Product_Families__c=null;
                                        }
                                        //End
                                        if(opp.Tier2__c==null){
                                                opp.Tier2_Type__c='';
                                                opp.Tier2_Level__c='';
                                                opp.Tier2_Authorizations__c='';
                                                opp.Tier2_Partner_Phone__c='';
                                                //added by Ankita Goel 12/26/2009 Disti 1B
                                                opp.Tier2_Partner_Geo__c = '';
                                                opp.Tier2_Partner_Region__c = '';
                                                opp.Tier_2_Partner_Parent__c = null;
                                                opp.Tier_2_Number_of_Competencies__c=null;
                                                opp.Tier_2_Allowed_Product_Families__c='';
                                        }else if(opp.Tier2__c!=oldOpp.Tier2__c){
                                                if(accMap.get(opp.Tier2__c).Type!=null){
                                                        opp.Tier2_Type__c=accMap.get(opp.Tier2__c).Type;
                                                } else {
                                                    opp.Tier2_Type__c=null;
                                                }
                                                if(accMap.get(opp.Tier2__c).Partner_Level__c!=null){
                                                        opp.Tier2_Level__c=accMap.get(opp.Tier2__c).Partner_Level__c;
                                                } else {
                                                    opp.Tier2_Level__c=null;
                                                }
                                                if(accMap.get(opp.Tier2__c).Authorizations_Specializations__c!=null){
                                                        opp.Tier2_Authorizations__c=accMap.get(opp.Tier2__c).Authorizations_Specializations__c;
                                                } else {
                                                    opp.Tier2_Authorizations__c=null;
                                                }
                                                if(accMap.get(opp.Tier2__c).Phone !=null){
                                                        opp.Tier2_Partner_Phone__c=accMap.get(opp.Tier2__c).Phone;
                                                } else {
                                                    opp.Tier2_Partner_Phone__c=null;
                                                }
                                                //added by Ankita Goel 12/26/2009 Disti 1B
                                                if(accMap.get(opp.Tier2__c).Geographic_Coverage__c != null){
                                                        opp.Tier2_Partner_Geo__c = accMap.get(opp.Tier2__c).Geographic_Coverage__c;
                                                } else {
                                                    opp.Tier2_Partner_Geo__c='';
                                                }
                                                if(accMap.get(opp.Tier2__c).Region__c != null){
                                                        opp.Tier2_Partner_Region__c = accMap.get(opp.Tier2__c).Region__c;
                                                } else {
                                                    opp.Tier2_Partner_Region__c='';
                                                }
                                                if(accMap.get(opp.Tier2__c).ParentId != null){
                                                        opp.Tier_2_Partner_Parent__c = accMap.get(opp.Tier2__c).ParentId;
                                                } else {
                                                    opp.Tier_2_Partner_Parent__c = null;
                                                }
                                                if(accMap.get(opp.Tier2__c).Allowed_Product_Families__c != null){
                                                        opp.Tier_2_Allowed_Product_Families__c=accMap.get(opp.Tier2__c).Allowed_Product_Families__c;
                                                } else {
                                                    opp.Tier_2_Allowed_Product_Families__c=null;
                                                }
                                                
                                                if(accMap.get(opp.Tier2__c).Number_Of_Competencies__c!=null){
                                                        opp.Tier_2_Number_of_Competencies__c=accMap.get(opp.Tier2__c).Number_Of_Competencies__c;
                                                } else {
                                                    opp.Tier_2_Number_of_Competencies__c=null;
                                                }
                                        }
                                        //added by Ankita 7/8/2010 for RASP changes: Need to track the Tier3 Partner and Influence partner Authorization specializations too
                                        if(opp.Tier3_Partner__c == null){
                                                opp.Tier3_Authorization__c = '';
                                        }else if(opp.Tier3_Partner__c != oldOpp.Tier3_Partner__c){
                                                if(accMap.get(opp.Tier3_Partner__c).Authorizations_Specializations__c != null){
                                                        opp.Tier3_Authorization__c = accMap.get(opp.Tier3_Partner__c).Authorizations_Specializations__c;
                                                }
                                        }
                                        if(opp.Influence_Partner__c == null){
                                                opp.Influence_Partner_Authorization__c = '';
                                        }else if(opp.Influence_Partner__c != oldOpp.Influence_Partner__c){
                                                if(accMap.get(opp.Influence_Partner__c).Authorizations_Specializations__c != null){
                                                        opp.Influence_Partner_Authorization__c = accMap.get(opp.Influence_Partner__c).Authorizations_Specializations__c;
                                                }
                                        }
                                        //end added by Ankita
                                        //opp.Channel__c=Util.setChannelInfo(opp);//added by prashant on 1/14/2011 for feb 2011 release -- channel Automation
                                }
                        }
                }
        }
        if(Trigger.isAfter){
                if(Trigger.isInsert){
                        if(accountIds.size()>0){
                                accountMap=new Map<Id,Account>([Select Id, Distributor_Ordering_Center__c,Type,Partner_Level__c,Partner_Type__c,
                                Authorizations_Specializations__c,Phone,Geographic_Coverage__c,Region__c, ParentId from Account where id in :accountIds]);//added parent to the query by ankita on 12/26/2009 for Disti 1B
                        }
                        if(accountMap!=null && accountMap.size()>0){
                                for (Opportunity opp : Trigger.new){
                                        if (opp.Ordering_Center__c != NULL && opp.Ordering_Center__c!=opp.AccountId){
                                                partner = new Partner(Role=accountMap.get(opp.Sold_To_Partner__c).Type,IsPrimary=true,AccountToId =opp.Ordering_Center__c,OpportunityId=opp.id);
                                                lstPartner.add(partner);
                                                partner = new Partner(Role=accountMap.get(opp.Sold_To_Partner__c).Type,IsPrimary=false,AccountToId =opp.Sold_To_Partner__c,OpportunityId=opp.id);
                                                lstPartner.add(partner);
                                                if(opp.Sold_To_Partner_User__c!=null){
                                                        vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Sold_To_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Edit');
                                                        lstVPartner.add(vPartner);
                                                }
                                                //Added for Tier 2 Partner user read only visibility to opp for Disti 1C: Ankita 1/29/2010
                                                if(opp.Tier2_Partner_User__c!=null){
                                                        vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Tier2_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Read');
                                                        lstVPartner.add(vPartner);
                                                }
                                        }else if(opp.Sold_To_Partner__c !=null && opp.Sold_To_Partner__c!=opp.AccountId){
                                                //partner = new Partner(Role='Distributor',IsPrimary=false,AccountToId =opp.Sold_To_Partner__c,OpportunityId=opp.id);
                                                partner = new Partner(Role=accountMap.get(opp.Sold_To_Partner__c).Type,IsPrimary=true,AccountToId =opp.Sold_To_Partner__c,OpportunityId=opp.id);
                                                lstPartner.add(partner);
                                                if(opp.Sold_To_Partner_User__c!=null){
                                                        vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Sold_To_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Edit');
                                                        lstVPartner.add(vPartner);
                                                }
                                        }
                                        if(opp.Tier2__c!=null && opp.Tier2__c!=opp.AccountId){
                                                //system.debug('OppTest:'+opp);
                                                //partner = new Partner(Role='VAR/Reseller',IsPrimary=false,AccountToId =opp.Tier2__c,OpportunityId=opp.id);
                                                partner = new Partner(Role=accountMap.get(opp.Tier2__c).Type,IsPrimary=false,AccountToId =opp.Tier2__c,OpportunityId=opp.id);
                                                //system.debug('Partner:'+partner);
                                                lstPartner.add(partner);
                                                //Added for Tier 2 Partner user read only visibility to opp for Disti 1C: Ankita 1/29/2010
                                                if(opp.Tier2_Partner_User__c!=null){
                                                        vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Tier2_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Read');
                                                        lstVPartner.add(vPartner);
                                                }
                                        }
                                        if(opp.Tier3_Partner__c !=null && opp.Tier3_Partner__c !=opp.AccountId){
                                                partner = new Partner(Role=accountMap.get(opp.Tier3_Partner__c ).Type,IsPrimary=false,AccountToId =opp.Tier3_Partner__c,OpportunityId=opp.id);
                                                lstPartner.add(partner);
                                        }
                                        if(opp.Influence_Partner__c!=null && opp.Influence_Partner__c!=opp.AccountId){
                                                partner = new Partner(Role=accountMap.get(opp.Influence_Partner__c).Type,IsPrimary=false,AccountToId =opp.Influence_Partner__c,OpportunityId=opp.id);
                                                lstPartner.add(partner);
                                        }
                                }
                                try{
                                        if(lstPartner.size()>0)insert lstPartner;
                                        if(lstVPartner.size()>0)insert lstVPartner;
                                }catch(DMLException dme){
                                        system.debug('Exception:'+dme);
                                }
                        }
                }
                if(Trigger.isUpdate){
//            System.debug('After Update');
                        Opportunity oldOpp;
                        Partner newPartner;
                        List<Partner> tempLst=new List<Partner>();
                        List<Partner> delPatLst=new List<Partner>();
                        List<Partner> newPatLst=new List<Partner>();
                        List<Visible_to_Partners__c> delSTLst=new List<Visible_to_Partners__c>();
                        Set<Id> delSTIds = new Set<Id>();
                        List<Id> lstOppIds=new List<Id>();
                        for(Opportunity opps:Trigger.new){
                                oldOpp=Trigger.oldMap.get(opps.Id);
                                if(opps.Ordering_Center__c!=null && opps.Ordering_Center__c!=oldOpp.Ordering_Center__c){
                                        lstOppIds.add(opps.Id);
                                }else if((opps.Sold_To_Partner__c!=oldOpp.Sold_To_Partner__c)){//if((opps.Sold_To_Partner__c!=null && opps.Sold_To_Partner__c!=oldOpp.Sold_To_Partner__c)||opps.Sold_To_Partner_User__c!=oldOpp.Sold_To_Partner_User__c)
                                        lstOppIds.add(opps.Id);
                                        accountIds.add(oldOpp.Sold_To_Partner__c);
                                }else if(opps.Tier2__c!=null && opps.Tier2__c!=oldOpp.Tier2__c){
                                        //System.debug('change in tier 2 partner');
                                        lstOppIds.add(opps.Id);
                                }else if(opps.Tier3_Partner__c !=null && opps.Tier3_Partner__c !=oldOpp.Tier3_Partner__c){
                                        lstOppIds.add(opps.Id);
                                }else if(opps.Influence_Partner__c!=null && opps.Influence_Partner__c!=oldOpp.Influence_Partner__c){
                                        lstOppIds.add(opps.Id);
                                }else if(opps.Sold_To_Partner_User__c != oldOpp.Sold_To_Partner_User__c){
                                        lstOppIds.add(opps.Id);
                                        accountIds.add(oldOpp.Sold_To_Partner__c);
                                }else if(opps.Tier2_Partner_User__c != oldOpp.Tier2_Partner_User__c){//Added for Tier 2 Partner user read only visibility to opp for Disti 1C: Ankita 1/29/2010
                                        lstOppIds.add(opps.Id);
                                        accountIds.add(oldOpp.Tier2__c);
                                }else if(opps.AccountId != oldOpp.AccountId){//added by Ankita 2/19/2010 so partners are not lost on account change
                                        lstOppIds.add(opps.Id);
                                        accountIds.add(oldOpp.AccountId);
                                }
                        }
                        //system.debug('Size up1::'+lstOppIds.size());
                        if(lstOppIds.size()>0){
                                System.debug('Processing Opps');
                                if(accountIds.size()>0){
                                        accountMap=new Map<Id,Account>([Select Id, Distributor_Ordering_Center__c,Type,Partner_Level__c,Partner_Type__c,
                                        Authorizations_Specializations__c,Phone,Geographic_Coverage__c,Region__c, ParentId from Account where id in :accountIds]);//added parent to the query by ankita on 12/26/2009 for Disti 1B
                                }
                                //system.debug('Size up2::'+accountMap.size());
                                //creating Map of Partner to related opportunity
                                if(accountMap!=null && accountMap.size()>0){
                                        for(Partner pat:[select id,Role,IsPrimary,AccountToId,OpportunityId from Partner where OpportunityId in:lstOppIds]){
                                                if(partnerMap.containsKey(pat.OpportunityId)){
                                                        partnerMap.get(pat.OpportunityId).add(pat);
                                                }else{
                                                        tempPat=new List<Partner>();
                                                        tempPat.add(pat);
                                                        partnerMap.put(pat.OpportunityId,tempPat);
                                                }
                                        }
                                        //system.debug('Size up3::'+partnerMap.size());
                                        //creating Map of Visible_to_Partners__c to related opportunity
                                        for(Visible_to_Partners__c vPat:[Select v.Id, v.Name, v.Opportunity_Access__c, v.Opportunity_Name__c, v.PartnerName__c, v.Role__c,
                                                                                                v.OwnerId, v.PartnerName__r.Contact.AccountId//added by Ankita 3/5/10 for checking sales team member account
                                                                                                from Visible_to_Partners__c v where v.opportunity_Name__c in :lstOppIds]){
                                                if(salesTeamMap.containsKey(vPat.Opportunity_Name__c)){
                                                        salesTeamMap.get(vPat.Opportunity_Name__c).add(vPat);
                                                }else{
                                                        stLst=new List<Visible_to_Partners__c>();
                                                        stLst.add(vPat);
                                                        salesTeamMap.put(vPat.Opportunity_Name__c,stLst);
                                                }
                                        }
                                        //system.debug('Size up4::'+salesTeamMap.size());
                                        for (Opportunity opp : Trigger.new){
                                                oldOpp=trigger.oldMap.get(opp.Id);
                                                tempLst=partnerMap.get(opp.id);
                                                //system.debug('tempLst:'+tempLst);
                                                if(tempLst!=null)delPatLst.addAll(tempLst);

                                                if (opp.Ordering_Center__c != NULL && opp.Ordering_Center__c!=opp.AccountId){
                                                        partner = new Partner(Role=accountMap.get(opp.Sold_To_Partner__c).Type,IsPrimary=true,AccountToId =opp.Ordering_Center__c,OpportunityId=opp.id);
                                                        newPatLst.add(partner);
                                                        partner = new Partner(Role=accountMap.get(opp.Sold_To_Partner__c).Type,IsPrimary=false,AccountToId =opp.Sold_To_Partner__c,OpportunityId=opp.id);
                                                        newPatLst.add(partner);
                                                        // Code commented for 1B
                                                        if(salesTeamMap.containsKey(opp.Id)){
                                                                for(Visible_to_Partners__c temp:salesTeamMap.get(opp.Id)){
                                                                        if(temp.PartnerName__c==oldOpp.Sold_To_Partner_User__c && opp.Sold_To_Partner_User__c!=oldOpp.Sold_To_Partner_User__c){
                                                                                //System.debug('delete STP user from sales team1');
                                                                                 //delSTLst.add(temp);
                                                                                 delSTIds.add(temp.Id);
                                                                        }
                                                //Added for Tier 2 Partner user read only visibility to opp for Disti 1C: Ankita 1/29/2010
                                                                        if(temp.PartnerName__c==oldOpp.Tier2_Partner_User__c && opp.Tier2_Partner_User__c != oldOpp.Tier2_Partner_User__c){
                                                                                //System.debug('delete T2P user from sales team1');
                                                                                 //delSTLst.add(temp);
                                                                                 delSTIds.add(temp.Id);
                                                                        }
                                                                        //added by Ankita 3/5/10 to remove sales team members if they do not belong to partner accounts
                                                                        if(temp.PartnerName__r.Contact.AccountId != opp.Ordering_Center__c && temp.PartnerName__r.Contact.AccountId != opp.Influence_Partner__c
                                                                                && temp.PartnerName__r.Contact.AccountId != opp.Tier3_Partner__c && temp.PartnerName__r.Contact.AccountId != opp.Sold_to_Partner__c
                                                                                && temp.PartnerName__r.Contact.AccountId != opp.Tier2__c){
                                                                                 //delSTLst.add(temp);
                                                                                 delSTIds.add(temp.Id);
                                                                                }
                                                                }
                                                                        if( opp.Sold_To_Partner_User__c!=null && opp.Sold_To_Partner_User__c!=oldOpp.Sold_To_Partner_User__c){//opp.Sold_To_Partner_User__c!=temp.PartnerName__c &&
                                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Sold_To_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Edit');
                                                                                lstVPartner.add(vPartner);
                                                                        }
                                                //Added for Tier 2 Partner user read only visibility to opp for Disti 1C: Ankita 1/29/2010
                                                                        if( opp.Tier2_Partner_User__c!=null && opp.Tier2_Partner_User__c != oldOpp.Tier2_Partner_User__c){//opp.Tier2_Partner_User__c !=temp.PartnerName__c &&
                                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Tier2_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Read');
                                                                                lstVPartner.add(vPartner);
                                                                        }
                                                        }else if(opp.Sold_To_Partner_User__c!=null){
                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Sold_To_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Edit');
                                                                lstVPartner.add(vPartner);
                                                        }else if(opp.Tier2_Partner_User__c!=null){//Added for Tier 2 Partner user read only visibility to opp for Disti 1C: Ankita 1/29/2010
                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Tier2_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Read');
                                                                lstVPartner.add(vPartner);
                                                        }
                                                }else
                                                if(opp.Sold_To_Partner__c!=null && opp.Sold_To_Partner__c!=opp.AccountId){
                                                        //system.debug('Size up4::'+opp.Sold_To_Partner__c);
                                                        //partner = new Partner(Role='Distributor',IsPrimary=false,AccountToId =opp.Sold_To_Partner__c,OpportunityId=opp.id);
                                                        partner = new Partner(Role=accountMap.get(opp.Sold_To_Partner__c).Type,IsPrimary=true,AccountToId =opp.Sold_To_Partner__c,OpportunityId=opp.id);
                                                        newPatLst.add(partner);
                                                        if(salesTeamMap.containsKey(opp.Id)){
                                                                for(Visible_to_Partners__c temp:salesTeamMap.get(opp.Id)){
                                                                        if(temp.PartnerName__c==oldOpp.Sold_To_Partner_User__c && opp.Sold_To_Partner_User__c!=oldOpp.Sold_To_Partner_User__c){
                                                                                //System.debug('delete STP user from sales team2');
                                                                                 //delSTLst.add(temp);
                                                                                 delSTIds.add(temp.Id);
                                                                        }
                                                                        //added by Ankita 3/5/10 to remove sales team members if they do not belong to partner accounts
                                                                        if(temp.PartnerName__r.Contact.AccountId != opp.Ordering_Center__c && temp.PartnerName__r.Contact.AccountId != opp.Influence_Partner__c
                                                                                && temp.PartnerName__r.Contact.AccountId != opp.Tier3_Partner__c && temp.PartnerName__r.Contact.AccountId != opp.Sold_to_Partner__c
                                                                                && temp.PartnerName__r.Contact.AccountId != opp.Tier2__c){
                                                                                 //delSTLst.add(temp);
                                                                                 delSTIds.add(temp.Id);
                                                                                }
                                                                }
                                                                        if( opp.Sold_To_Partner_User__c!=null && opp.Sold_To_Partner_User__c!=oldOpp.Sold_To_Partner_User__c){//opp.Sold_To_Partner_User__c!=temp.PartnerName__c &&
                                                                                //System.debug('Add STP user from sales team2');
                                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Sold_To_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Edit');
                                                                                lstVPartner.add(vPartner);
                                                                        }
                                                        }else if(opp.Sold_To_Partner_User__c!=null){
                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Sold_To_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Edit');
                                                                lstVPartner.add(vPartner);
                                                        }
                                                }
                                                if(opp.Tier2__c!=null && opp.Tier2__c!=opp.AccountId){
                                                        system.debug('OppTest:'+opp);
                                                        //partner = new Partner(Role='VAR/Reseller',IsPrimary=false,AccountToId =opp.Tier2__c,OpportunityId=opp.id);
                                                        partner = new Partner(Role=accountMap.get(opp.Tier2__c).Type,IsPrimary=false,AccountToId =opp.Tier2__c,OpportunityId=opp.id);
                                                        //system.debug('Partner:'+partner);
                                                        newPatLst.add(partner);
                                                        //added for Tier 2 partner user read only visibility to opp: Ankita 1/29/2010: Disti 1C
                                                        if(salesTeamMap.containsKey(opp.Id)){
                                                                for(Visible_to_Partners__c temp:salesTeamMap.get(opp.Id)){
                                                                        //System.debug('checking sales team');
                                                                        if(temp.PartnerName__c==oldOpp.Tier2_Partner_User__c && opp.Tier2_Partner_User__c != oldOpp.Tier2_Partner_User__c){
                                                                                //System.debug('delete T2P user from sales team2');
                                                                                 //delSTLst.add(temp);
                                                                                 delSTIds.add(temp.Id);
                                                                        }
                                                                        //added by Ankita 3/5/10 to remove sales team members if they do not belong to partner accounts
                                                                        if(temp.PartnerName__r.Contact.AccountId != opp.Ordering_Center__c && temp.PartnerName__r.Contact.AccountId != opp.Influence_Partner__c
                                                                                && temp.PartnerName__r.Contact.AccountId != opp.Tier3_Partner__c && temp.PartnerName__r.Contact.AccountId != opp.Sold_to_Partner__c
                                                                                && temp.PartnerName__r.Contact.AccountId != opp.Tier2__c){
                                                                                 //delSTLst.add(temp);
                                                                                 delSTIds.add(temp.Id);
                                                                                }
                                                                }
                                                                        if(opp.Tier2_Partner_User__c!=null && opp.Tier2_Partner_User__c != oldOpp.Tier2_Partner_User__c){//opp.Tier2_Partner_User__c !=temp.PartnerName__c &&
                                                                                //System.debug('Add T2P user from sales team2');
                                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Tier2_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Read');
                                                                                lstVPartner.add(vPartner);
                                                                        }
                                                        }else if(opp.Tier2_Partner_User__c!=null){
                                                                vPartner = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=opp.Tier2_Partner_User__c ,Opportunity_Name__c=opp.id,Opportunity_Access__c='Read');
                                                                lstVPartner.add(vPartner);
                                                        }
                                                }
                                                if(opp.Tier3_Partner__c !=null && opp.Tier3_Partner__c !=opp.AccountId){
                                                        partner = new Partner(Role=accountMap.get(opp.Tier3_Partner__c ).Type,IsPrimary=false,AccountToId =opp.Tier3_Partner__c,OpportunityId=opp.id);
                                                        newPatLst.add(partner);
                                                }
                                                if(opp.Influence_Partner__c!=null && opp.Influence_Partner__c!=opp.AccountId){
                                                        partner = new Partner(Role=accountMap.get(opp.Influence_Partner__c).Type,IsPrimary=false,AccountToId =opp.Influence_Partner__c,OpportunityId=opp.id);
                                                        newPatLst.add(partner);
                                                }
                                        }//End of for
                                        try{
                                                if(delPatLst.size()>0) delete delPatLst;
                                                if(delSTIds.size()>0){
                                                        delSTLst = [select id from Visible_to_Partners__c where id in :delSTIds];
                                                        delete delSTLst;
                                                }
                                                if(newPatLst.size()>0) insert newPatLst;
                                                if(lstVPartner.size()>0) insert lstVPartner;
                                        }catch(DMLException dme){
                                                system.debug('Exception:'+dme);
                                                Trigger.new[0].addError('An error Occured:'+dme.getDmlMessage(0));
                                        }
                                }//End of if accountMap
                        }//End of if lstOppIds
                        OpportunitySalesTeamHlp.updateOpportunitySalesTeam(trigger.new, trigger.oldMap);//Added by prashant for adding addition sale team to opportunity sales team on 11/22/2010
                }
        }
}