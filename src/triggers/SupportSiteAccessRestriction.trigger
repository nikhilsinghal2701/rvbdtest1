trigger SupportSiteAccessRestriction on Account (before insert, before update, after update) {
	List<Account> accList=new List<Account>();
	List<Account> accNotifyList=new List<Account>();
	AccountUtil accUtil;
	if(trigger.isBefore){
		if(trigger.isUpdate){
			for(Account acc:Trigger.new){
				if((acc.Support_Site_Access_Expiry_Date__c!=Trigger.oldMap.get(acc.Id).Support_Site_Access_Expiry_Date__c
						||acc.Support_Site_Access_Expiry_Date__c==NULL||acc.Disable_Support_Method__c!=Trigger.oldMap.get(acc.Id).Disable_Support_Method__c ||
						acc.Partner_Status1__c!=Trigger.oldMap.get(acc.Id).Partner_Status1__c)){
					accList.add(acc);					
				}
			}
			if(accList.size()>0){
				accUtil=new AccountUtil();
				accUtil.updateSupportAccessDisabled(accList,Trigger.oldMap);
			}									
		}
	}
	if(trigger.isAfter){
		//system.debug('inside after event of supportsiteaccessrestriction');
		for(Account acc: trigger.New){
			Account oldAcc = trigger.oldMap.get(acc.Id);
			if((acc.X30_Day_Notification_Sent__c != oldAcc.X30_Day_Notification_Sent__c && acc.X30_Day_Notification_Sent__c)||
				(acc.X15_Day_Notification_Sent__c != oldAcc.X15_Day_Notification_Sent__c && acc.X15_Day_Notification_Sent__c)||
				(acc.Support_Access_Disabled__c != oldAcc.Support_Access_Disabled__c && acc.Support_Access_Disabled__c)){
				accNotifyList.add(acc);
			}
		}		
		if(accNotifyList.size()>0){
			if(accUtil==NULL)accUtil=new AccountUtil();
			accUtil.sendEmailNotification(accNotifyList);
		}
	}	
}