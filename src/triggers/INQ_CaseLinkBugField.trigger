trigger INQ_CaseLinkBugField on InQuira_Case_Info__c (after insert, before delete) {

    List<InQuira_Case_Info__c> triggerLinks = new List<InQuira_Case_Info__c>();
    Map<Id, Id> caseIds = new Map<Id, Id>();
    Map<Id, Case> caseMap = new Map<Id, Case>();
    Map<Id, Id> artIds = new Map<Id, Id>();
    Map<Id, InQuira_Article_Info__c> artMap = new Map<Id, InQuira_Article_Info__c>();
    Map<Id, Case> casesToUpdate = new Map<Id, Case>();
    Case relCase;
    InQuira_Article_Info__c relArt;
    String artUrl;
    String bugId;
    String bugIdSplit;
    
    // Assign triggerLinks based on whether records are being deleted or inserted
    if (trigger.isInsert) {
        triggerLinks = trigger.new;
    } else if (trigger.isDelete) {
        triggerLinks = trigger.old;
    }
    
    if (triggerLinks.isEmpty()) {
        System.debug(LoggingLevel.ERROR, 'Trigger is neither insert nor delete, unable to proceed with autopopulating case bug field');
    } else {
        // Compile maps of link object ids to related case & article ids
        for (InQuira_Case_Info__c ici : triggerLinks) {
            caseIds.put(ici.Id, ici.Related_Case__c);
            artIds.put(ici.Id, ici.Related_InQuira_Article__c);
        }
            
        // Query for related cases and articles
        List<Case> relCases = [SELECT Id, Bug_No__c FROM Case WHERE Id IN :caseIds.values()];
        List<InQuira_Article_Info__c> relArts = [SELECT Id, Display_URL__c FROM InQuira_Article_Info__c WHERE Id IN :artIds.values()];
        
        // Build maps for cases and articles
        for (Case c : relCases) {
            caseMap.put(c.Id, c);
        }
        
        for (InQuira_Article_Info__c iai : relArts) {
            artMap.put(iai.Id, iai);
        }
        
        for (InQuira_Case_Info__c ici : triggerLinks) {
            relCase = caseMap.get(caseIds.get(ici.Id));
            relArt = artMap.get(artIds.get(ici.Id));
            artUrl = (String)relArt.Display_URL__c;
            bugId = '';
            bugIdSplit = '';
            
            // Look for the bugzilla= parameter and verify that id parameter is included in URL
            if (artUrl.toLowerCase().contains('bugzilla=') && artUrl.toLowerCase().contains('id=')) {
                // Extract bugId from URL
                bugId = artUrl.substring((artUrl.toLowerCase().indexOf('id=')+3));
                if (bugId.contains('&')) {
                    bugId = bugId.substring(0, bugId.indexOf('&'));
                }
            }
            
            if (bugId != '') {
                if (trigger.isInsert && (relCase.Bug_No__c == '' || relCase.Bug_No__c == null)) {
                    // Article is being linked and there were no bugs previously linked to case, set Bug_No__c field to bugId from URL
                    relCase.Bug_No__c = bugId;
                } else if (trigger.isInsert) {
                    // Article is being linked and there were bugs previously linked to case, append comma, space, and bugId from URL to existing bugs
                    relCase.Bug_No__c += ', ' + bugId;
                } else if (trigger.isDelete && relCase.Bug_No__c.equalsIgnoreCase(bugId)) {
                    // Article is being unlinked and it is the only bug in the Bug_No__c field
                    relCase.Bug_No__c = null;
                } else if (trigger.isDelete && relCase.Bug_No__c.indexOf(bugId) == 0) {
                    // Article is being unlinked, there is more than one bug in the Bug_No__c field and it is the first bug listed
                    relCase.Bug_No__c = relCase.Bug_No__c.split(bugId + ', ')[1];
                } else if (trigger.isDelete && relCase.Bug_No__c.toLowerCase().contains(bugId.toLowerCase())) {
                    // Article is being unlinked, there is more than one bug in the Bug_No__c field and it is not the first bug listed
                    bugIdSplit = ', ' + bugId;
                    relCase.Bug_No__c = relCase.Bug_No__c.split(', ' + bugId)[0] + (relCase.Bug_No__c.split(', ' + bugId).size() > 1 ? relCase.Bug_No__c.split(', ' + bugId)[1] : '');
                }
                // Put case back in the case map (in case multiple links apply to same case)
                caseMap.put(ici.Id, relCase);
                // Add case to update now that Bug_No__c field has been changed
                casesToUpdate.put(relCase.Id, relCase);
            }
        }
        
        // Update case records with new Bug_No__c field values
        if (casesToUpdate != null && !casesToUpdate.isEmpty() && casesToUpdate.size() > 0) {
            update casesToUpdate.values();
        }
    }
}