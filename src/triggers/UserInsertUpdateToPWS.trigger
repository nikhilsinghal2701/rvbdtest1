trigger UserInsertUpdateToPWS on User (after insert, after update) {
    if (!system.isBatch()) {
        B2BIntegrationUtil.generateUserWSDL(trigger.new);
    }
}