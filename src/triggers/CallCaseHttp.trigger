trigger CallCaseHttp on CaseComment (after insert,after update) {
        Set<id> ids=new Set<id>();
        for(CaseComment  csc : trigger.new){
            if(csc.ParentId!=null){
                ids.add(csc.ParentId);
            }
                        
        }   
            if(Ids.Size()>0){
        		if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
        			system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
         		}
         		else {CaseHttp.getTheCase(Ids);}
        	}

}