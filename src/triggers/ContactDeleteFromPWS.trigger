/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger ContactDeleteFromPWS on Contact (before delete) {
    List<ID> objIDs=new List<ID>();// Added by Anil NPI 4.0 09.28.2014
    if(trigger.isBefore && trigger.isDelete){
        util.ContMergeReTouchChildRecords(trigger.oldMap.keySet()); 
        for(Contact c : trigger.old){
            objIDs.add(c.id);
        }
    }
    if(objIDs.size()>0)
        B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs);
}