trigger DiscountApprovalQuote on Quote__c (before insert, before update, after update,after insert) {
    // The trigger invoks a bunch of different methods on DiscountApprovalRouting
    // class to delegate processing. Not all of these methods do their work every single invocation.
    // Each method is responsible for filtering passed in records to determine applicability
    List<Quote__c> qlist = new List<Quote__c>();
    List<Quote__c> qulist = new List<Quote__c>();
    
    if(Trigger.isBefore){
        util.dealDeskStatusUpdate(Trigger.new);
        DiscountManagementHelper.setUpliftInQuote(Trigger.new);
    }
    
         if(trigger.isupdate && trigger.isAfter)//Added By Nikhil to copy Uplift calculation on Opp from Quote 
    {
        for(Quote__c q : trigger.new){
        
        Quote__c q1 = Trigger.OldMap.get(q.Id);
        
         if( (q.Forecasted_Quote__c && q1.Uplift_Calculation__c != q.Uplift_Calculation__c ) ||
            (q.Quote_Status__c != null && q.Quote_Status__c != q1.Quote_Status__c && q.Quote_Status__c.equalsIgnoreCase('Priced Quote'))){
            
            system.debug('came here ******'+q);
                qulist.add(q);
          }
        }
    }
    
    if(!qulist.IsEmpty())
    {
        DiscountManagementHelper.UpdateUpliftInOpp(qulist);
    }
   //Ended By Nikhil to copy Uplift calculation on Opp from Quote 
     
     
    for(Quote__c q : trigger.new) // Code Added by Santoshi on Nov-15-11 to restrict the trigger code on firing for Use For Forecasting Update.
    {
        if(trigger.isupdate)
        {
            Quote__c q1 = Trigger.OldMap.get(q.Id);
            if(q1.Forecasted_Quote__c != q.Forecasted_Quote__c)
            {
                qlist.add(q);
                System.debug('old+++++++++'+q1.Forecasted_Quote__c+'new++++++++++++++++'+q.Forecasted_Quote__c);
            }
        }
    }
    if(qlist.size()<1)
    {
        System.debug('Testing Inside..');
        if(trigger.isinsert && trigger.isafter)
        {
        /** Created by Santoshi on Oct - 2011 This method is called to copy opportunity discount detail into Quote on quote insertion..*/
         DiscountManagementHelper.InsertsDiscountDetailInQuote(trigger.New);
        }
        
        Boolean isManual = DiscountApprovalHlp.validateIsManual(Trigger.new, Trigger.oldMap);//added by Prashant for SFDC outage Project
        if(Trigger.isBefore && isManual){//added by Prashant for SFDC outage Project
            DiscountApprovalHlp.approveRecord(Trigger.new);
        }else if (Trigger.isBefore) {//added by Prashant for SFDC outage Project
            system.debug('***isManual:'+isManual);
            if (Trigger.isUpdate  && !isManual) {//modified by Prashant for SFDC outage Project
                DiscountApprovalHlp.setRSM(Trigger.new);
                DiscountApprovalHlp.setSegmentOnQuote(trigger.new);
                // Approval matrix needs to be generated in "before" trigger and prior to processQuotes
                // call as the latter examines Approval Matrix to check if RSM is the sole approver,
                // in which case a quote that otherwise requires approval is auto approved.
                DiscountApprovalRouting.getInstance().generateApprovalMatrix(Trigger.new, Trigger.oldMap);
                
                Boolean validated = DiscountApprovalHlp.validateSubmittedQuotes(Trigger.new, Trigger.oldMap);
                if (validated) {
                    DiscountApprovalEmailHlp.getInstance().processQuotes(Trigger.newMap);
                    DiscountApprovalRouting.getInstance().processApprovalStep(Trigger.newMap, Trigger.oldMap);
                    DiscountApprovalHlp.createHistory(Trigger.new, Trigger.oldMap);
                }
                DiscountApprovalRouting.processPromotions(Trigger.newMap, Trigger.oldMap);
                DiscountApprovalRouting.getInstance().processQuotes(Trigger.new, Trigger.oldMap);
            }
    
            if (Trigger.isInsert && !isManual) { //modified by Prashant for SFDC outage Project
                DiscountApprovalHlp.initializeQuotes(Trigger.new);
                DiscountApprovalHlp.setRSM(Trigger.new);
            }
        }
    
        if (Trigger.isAfter && !isManual) { //modified by Prashant for SFDC outage Project 
            if (Trigger.isUpdate) {
                DiscountApprovalHlp.shareToPartner(Trigger.newMap, Trigger.oldMap);
                DiscountApprovalRouting.getInstance().saveApprovedDiscountsToOpportunity(Trigger.new, Trigger.oldMap);
                DiscountApproverMatrixHelper.recalculateRoutingMatrix(Trigger.newMap, Trigger.oldMap);
            }
            // Send out any emails queued during the transaction
           DiscountApprovalEmailHlp.getInstance().flush();
        }else if(Trigger.isAfter && Trigger.isUpdate && isManual){//added by Prashant for SFDC outage Project
            try{
                DiscountApprovalHlp.approveRecord(trigger.new, trigger.oldMap);
            }catch(Exception e){
                trigger.new[0].addError('Error Occured in DiscountApprovalQuote Trigger:'+e.getMessage());
            }
        }
        
        if(Trigger.isUpdate && Trigger.isAfter && !isManual)/** Addded by Santoshi Mishra on Nov-2011 to create Margin Summary records.*/
        {
            System.debug('Inside my trigger----');
            DiscountManagementHelper.insertMarginSummaryRecords(Trigger.newMap,false); // This method will process Quote and Quote Line items and insert Margin Summary Records,
            System.debug('Outside my trigger----');
        
        }
    }   
}