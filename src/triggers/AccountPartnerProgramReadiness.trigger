trigger AccountPartnerProgramReadiness on Account (after insert, after update) {
    string patRecordType='012300000000NE5';
    List<Account> newAccountSummaryCertList=new List<Account>();
    if(trigger.isAfter & trigger.isInsert){
        for(Account accNew:trigger.new){
            if(accNew.RecordTypeId==patRecordType){
                newAccountSummaryCertList.add(accNew);
            }
        }
        if(newAccountSummaryCertList.size()>0)      
            AccountPartnerProgramRediness.createAccountSummaryCert(newAccountSummaryCertList);
    }else if(trigger.isAfter & trigger.isUpdate){
        for(Account accNew:trigger.new){
            if(trigger.oldMap.containsKey(accNew.Id)){
                if((accNew.RecordTypeId!=trigger.oldMap.get(accNew.Id).RecordTypeId) && accNew.RecordTypeId==patRecordType){// 
                    newAccountSummaryCertList.add(accNew);
                }
            }
        }
        if(newAccountSummaryCertList.size()>0)      
            AccountPartnerProgramRediness.createAccountSummaryCert(newAccountSummaryCertList);
        
    }
}