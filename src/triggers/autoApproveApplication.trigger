trigger autoApproveApplication on Partner_Application_Category__c (after update) {

	
	System.debug('autoSubmitPartnerAppCategory trigger before update');
	List<Id> partnerApplicationCategories = new List<Id>();
	
	for (Partner_Application_Category__c partnerAppCategory : trigger.new) {
		partnerApplicationCategories.add(partnerAppCategory.Partner_Application__c);
	}
	
	//get the application attached to the cateory and check
	List<Partner_Application_Category__c> partnerAppCategoryList = [Select Id, Incomplete_Tasks__c, Partner_Application__c from Partner_Application_Category__c where Partner_Application__c In :partnerApplicationCategories];
	Map<String, Integer> partnerApps = new Map<String, Integer>();

	for (Partner_Application_Category__c partnerAppCategory : partnerAppCategoryList) {
		Integer count = partnerApps.get(partnerAppCategory.Partner_Application__c);
		if (count == null) {
			count = 0;
		}
		System.debug('Incomplete_Tasks__c = ' + partnerAppCategory.id + ' : ' +  partnerAppCategory.Incomplete_Tasks__c.intValue());
		count = count + partnerAppCategory.Incomplete_Tasks__c.intValue();
		partnerApps.put(partnerAppCategory.Partner_Application__c, count);
	}
	
	List<Partner_Application__c> partnerAppList = new List<Partner_Application__c>();
	for (String s : partnerApps.keySet()) {
		Integer count = partnerApps.get(s);
		if (count == 0) {
			partnerAppList.add(new Partner_Application__c(Id = s, Application_Status__c = 'Approved', Application_Approval_Date__c = Date.today()));
		}
	}
	System.debug('partnerAppList=' + partnerAppList.size());
	if (! partnerAppList.isEmpty()) {
		update partnerAppList;
	}
}

/*

trigger assignToDynamicGroups on Account (after update) {

	List<Id> accountIds = new List<Id>();
	for (Account acc : trigger.new) {
		if (acc.isPartner) {
			String oldPartnerType = trigger.oldMap.get(acc.Id).Partner_Type__c;
			String newPartnerType = acc.Partner_Type__c;

			String oldPartnerLevel = trigger.oldMap.get(acc.Id).Partner_Level__c;
			String newPartnerLevel = acc.Partner_Level__c;

			String oldPartnerGeo = trigger.oldMap.get(acc.Id).Geo__c;
			String newPartnerGeo = acc.Partner_Geo__c;
			
			if (!oldPartnerType.equals(newPartnerType) || !oldPartnerType.equals(newPartnerType)
				|| !oldPartnerType.equals(newPartnerType))
			{
				accountIds.add(acc.Id);
			}
		}
	}
	
	Groupallocator.allocateUpdatedAccounts(trigger.oldMap, trigger.new, accountIds);
	
	allocateUpdatedAccounts(Map<Id, Account> oldAccountMap, Map<Id, Account> newAccountMap, List<Id> affectedAccounts) {
		for (Id accountId : affectedAccounts) {
			Account oldAccount = oldAccountMap.get(accountId);
			List<Dynamic_Group__c> dynamicGroupList = [Select Id, name, Group_Name__c, Group_Id__c, (Select Field__c, Operator__c, Value__c from Group_Rule__c) from Dynamic_Group__c where Active__c = true];
			
		}
	}
}
*/