/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger OpportunityDeleteFromPWS on Opportunity (before delete) {
    
    List<ID> objIDs=new List<ID>();
    if(trigger.isBefore && trigger.isDelete){
        
    for( ID a:trigger.oldMap.keySet()){objIDs.add(a);}
    
    if(objIDs.size()>0)
    B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs);
    }
}