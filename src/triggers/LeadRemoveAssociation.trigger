trigger LeadRemoveAssociation on Lead (Before Insert,Before Update) {
    Map <String, Remove_Lead_Auto_Convert__c> rl = Remove_Lead_Auto_Convert__c.getAll();
    List<Lead> nleads= new List<Lead>();
    Map<Id,Contact> probContactMap;
    Set<Id> contactIds=new Set<Id>();
    String  removeAsso, autoConvert; 
    Integer rAsso = 0;
    Integer aconvert= 0;
    String generalLeadId;
    public Id accountId;
    public Contact tempCon;
    
    for(Lead l:trigger.new){ 
        if(l.ownerid !=null){
            nleads.add(l);
        }
    }
    if( nleads.size()>0){
        
        util.leadPreviousOwnerUpdate(nleads,trigger.oldMap);
    }
    if(LeadStatusChange_DateStamp.firstRun)
    {
    LeadStatusChange_DateStamp.leadTimeStamp(trigger.new,trigger.oldMap);
    }
    if(rl.containsKey('Lead Update')){ 
    if(rl.get('Lead Update').Auto_Convert__c != null){     
    autoConvert = rl.get('Lead Update').Auto_Convert__c;
    } else {
        autoConvert = '0';
    }
    if(rl.get('Lead Update').Eloqua_Disassociation_Date__c != null){
    removeAsso =  rl.get('Lead Update').Eloqua_Disassociation_Date__c;
    } else {
        removeAsso = '0';
    }
    rAsso  = Integer.valueof(removeAsso);
    aconvert =  Integer.valueof(autoConvert);
    }
    
    //Id generalLeadId= [select id from RecordType where name ='General Leads' and SobjectType = 'Lead' ].Id;
    
    if(RecordTypeIds__c.getInstance('General Leads') != null){
    RecordTypeIds__c myCS2 = RecordTypeIds__c.getInstance('General Leads');
    if(myCS2.RecordTypeId__c != null){
        generalLeadId = myCS2.RecordTypeId__c;
    }
    }
     // populate Probable_Account__c field  RFC#7036
    if(trigger.isBefore){        
      Set<String> setOSIds = new Set<String>();
      for(Lead Ld:Trigger.new){
          if(Ld.OneSource__OSKeyID__c != null){
              setOSIds.add(Ld.OneSource__OSKeyID__c);
          }
          
      }
      
      system.debug('setOSIds'+setOSIds);
      Map<String, Account> mapAcc = new Map<String, Account>();
      
      for(Account aos: [Select Id,OneSource__OSKeyID__c, RecordType_Name__c From Account Where OneSource__OSKeyID__c in:setOSIds]){
          System.debug('aos '+aos);
        mapAcc.put(aos.OneSource__OSKeyID__c,aos);
      }
      
      system.debug('mapAcc'+mapAcc);
      if(mapAcc.size()>0){
          for(Lead ld:Trigger.New){
              if(mapAcc.containsKey(ld.OneSource__OSKeyID__c) && (mapAcc.get(ld.OneSource__OSKeyID__c).RecordType_Name__c == 'Customer Account') ){     // by Sukhdeep Singh Ticket# 209612 on 18/06/2014
                ld.Probable_Account__c = mapAcc.get(ld.OneSource__OSKeyID__c).id;
                  }
          }
      }        
    }
   // populate Auto_Convert,Lead_Expiration_Date  fields only if the Lead Record Type is of general lead    
  
   if(trigger.isBefore && trigger.isInsert){
        for(Lead lead:trigger.new){ 
            if(lead.RecordTypeId == generalLeadId){
                lead.Auto_Convert__c = System.Today().addDays(aconvert);
                lead.Eloqua_Disassociation_Date__c= System.Today().addDays(rAsso);
                }
            if(lead.Associated_Contact__c!=null){
                contactIds.add(lead.Associated_Contact__c);
            }
        }
        if(contactIds.size()>0){
            probContactMap = new Map<Id,Contact>([select id,AccountId from Contact where id IN:contactIds AND Account.RecordType_Name__c ='Customer Account']);
            if(probContactMap!=null && probContactMap.size()>0){
                for(Lead lead:trigger.new){
                    if(lead.Associated_Contact__c!=null){
                        if(probContactMap.containsKey(lead.Associated_Contact__c)){
                            lead.Probable_Account__c = probContactMap.get(lead.Associated_Contact__c).AccountId;
                        }
                    }else{
                        lead.Probable_Account__c=null;
                    }                   
                }               
            }   
        }
    } 
    
    //Blank out the date fields if the Record Type changes from the General Leads to any other Record Type
    if(trigger.isUpdate && trigger.isbefore){
        for(Lead newLead : trigger.new){  
        Lead oldLead = trigger.oldMap.get(newLead.Id);
        if(newLead.RecordTypeId != generalLeadId )
        {
            newLead.Auto_Convert__c = null ;
            newLead.Eloqua_Disassociation_Date__c = null;
        }
        
       //  Populate the Date fields if the Record Type changes back to General Lead Record Type
        if((oldLead.RecordTypeId != generalLeadId ) &&  (newLead.RecordTypeId == generalLeadId) ){
            newLead.Auto_Convert__c = System.Today().addDays(aconvert);
            newLead.Eloqua_Disassociation_Date__c=   System.Today().addDays(rAsso);
        }
       }
    }   
    
}