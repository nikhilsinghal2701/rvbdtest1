/*************************************************************************************************************
 Modified By: Anil Madithati
 Date: 08/23/2014
 Purpose: To send an outbound to TIBCO to handle the record deletions ((Part of NPI::3.0))
 Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 
*************************************************************************************************************/
trigger AssetProductFamily_ChassisTypeUpdate on Asset (before insert, before update,after update,before delete,after delete, after insert) {
        List<Asset>astList=new List<Asset>();
        List<ID> objIDs=new List<ID>();
        try{
                if(trigger.isBefore && trigger.isInsert){
                        AssetsUtil.productFamilyChasisTypeUpdate(trigger.new,null);
                        //ObjectHistoryUtil.AssetHistory(Trigger.new, Trigger.oldMap);
                         for( Asset a:Trigger.new){
                             if(a.Base_sku__c!=null && a.Base_sku__c!=''){
                                 astList.add(a);
                                 }
                             }
                }if(trigger.isAfter && trigger.isInsert){
                  ObjectHistoryUtil.AfterInsertAssetHistory(Trigger.new);
                         
                }else if(trigger.isBefore && trigger.isUpdate){
                        AssetsUtil.productFamilyChasisTypeUpdate(trigger.new,trigger.oldMap);
                        for( Asset a:Trigger.new){
                             if(a.Base_sku__c!=null && a.Base_sku__c!=''){
                                 astList.add(a);
                                 }
                             }
                }else if(trigger.isAfter && trigger.isUpdate){
                        AssetsUtil.OnAfterUpdateChild(trigger.newMap,trigger.oldMap);
                        ObjectHistoryUtil.AfterUpdateAssetHistory(Trigger.new, Trigger.oldMap);
                }else if(trigger.isBefore && trigger.isDelete){
                      ObjectHistoryUtil.BeforeDeleteAssetHistory(Trigger.old);
                      for( ID a:trigger.oldMap.keySet()){objIDs.add(a);}
        
                      if(objIDs.size()>0)
                      {
                      if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
                      system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
                        }
                      
                      else {
                        B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs);
                      }
                      }
                        
                }
        }catch(Exception e){
                trigger.new[0].addError('Exception:'+e.getMessage());
        }
         if(astList.size()>0){
             AssetsUtil.UpdateAssetProductUsingBaseSku(astList);
           }
}