trigger LeadPartnerDetails on Lead (before insert) 
{

//-- Set<Id> ownerIds = new Set<Id>();
 
//--for (Lead l : Trigger.new) 
//-- {
 	//Get the current user Id to identify the reseller details
//-- 	ownerIds.add(l.OwnerId);
//-- }

 //Query and Map all user related data
//--Map<Id,User> partnerAccts = new Map<Id,User>([Select Id,UserType,Contact.Account.Id ,Contact.Account.Name,Contact.phone,Contact.Name from User where id in :ownerIds]);

 
 
 for (Lead l : Trigger.new) 
	{
		//Only update the value for records that have an owner = partnerAccount
//		System.debug('User Type --> ' + partnerAccts.get(l.OwnerId).UserType);
		
//		if (((partnerAccts.get(l.OwnerId).Contact.Account.Id != NULL) || (partnerAccts.get(l.OwnerId).UserType == 'PowerPartner')) &&((l.RecordTypeId == '012700000005KVr') || (l.RecordTypeId == '0127000000059Be')))
		
		//o.Ordering_Center__c = dc.Distributor_Ordering_Center__c;
		

		if ((l.RecordTypeId == '012700000005KVr') || (l.RecordTypeId == '0127000000059Be'))
		{
			   User u = [Select Id,UserType,Contact.Account.Id ,Contact.Account.Name,Contact.phone,Contact.Name from User where id = :l.OwnerId];
			   System.debug('User Details --> ' + u);
				
			   if (u.UserType == 'PowerPartner')
			   {
	//			   l.Lead_Creator_Id__c = l.OwnerId;
	//			   System.debug('Creator Id --> ' + l.Lead_Creator_Id__c);
					
				   l.Registering_Partner_User__c = l.OwnerId;
//				   System.debug('Contact Name --> ' + l.Registering_Partner_User__c);	
		
				   //l.Registering_Partner__c = partnerAccts.get(l.OwnerId).Contact.Account.Id;
				   l.Registering_Partner__c = u.Contact.Account.Id;
//				   System.debug('Company Name --> ' + l.Registering_Partner__c);
				   
				   //l.Lead_Creator_Phone__c = partnerAccts.get(l.OwnerId).Contact.phone;
				   l.Lead_Creator_Phone__c = u.Contact.phone;
//				   System.debug('Contact Phone --> ' + l.Lead_Creator_Phone__c);
				   
		   
	//			   System.debug('Record Type Id --> ' + l.RecordTypeId);
			   }	
		}
	}

}