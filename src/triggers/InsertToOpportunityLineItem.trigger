/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger InsertToOpportunityLineItem on Quote_Line_Item__c (after insert, after delete) {
    
    Set<Id> qIds = new Set<Id>();
    Map<Id,Id> oppIdsMap = new Map<Id,Id>();
    Set<Id> pIds = new Set<Id>();
    List<Opportunity> optyList = new List<Opportunity>();
    List <OpportunityLineItem> oplist= new List <OpportunityLineItem>();
    Set <ID> oppIds = new Set<Id>();
    Map<String,Boolean> optyQuoteList = new Map<String,Boolean> ();
    Map<Id,Boolean> optyWithLineItems = new Map<Id,Boolean>();//prashant.singh@riverbed.com on 09/27/2012; november release
    if(trigger.isdelete && trigger.isafter)
     for(Quote_Line_Item__c qli : trigger.old){
        qIds.add(qli.Quote__c);
        //System.debug();
    }
    else if(trigger.isinsert && trigger.isafter)
    for(Quote_Line_Item__c qli : trigger.new){
        qIds.add(qli.Quote__c);
        //System.debug();
    }
    

    Pricebook2 pbook = [select Id,Name from pricebook2 where isactive=true limit 1];
    List<Quote__C> quoteList =[select id, Opportunity__c, Opportunity__r.HasOpportunityLineItem, Opportunity__r.Pricebook2Id,Quote_Status__C  from Quote__c where id in :qIds];
    for(Quote__C q :quoteList )
    {
        oppIds.add(q.Opportunity__c);
        optyWithLineItems.put(q.Id,q.Opportunity__r.HasOpportunityLineItem);
    }
    List<Opportunity> oppsList=[select id,HasOpportunityLineItem, (select Id from R00NS0000000MJ1pMAG__r ),Pricebook2Id from opportunity where id in : OppIds ];
    for(Opportunity o : oppsList)
    {
        if(o.R00NS0000000MJ1pMAG__r!=null && o.R00NS0000000MJ1pMAG__r.size()>1)
        optyQuoteList.put(o.Id,false);
        else if(o.R00NS0000000MJ1pMAG__r.size() <=1)
        optyQuoteList.put(o.Id,true);
        
        System.debug('checking++++++++++++'+optyQuoteList);
    }
   /** for(Quote__c q1 :o.R00NS0000000MJ1pMAG__r ){
        if(!optyQuoteList.containskey(q1.Opportunity__C))
        optyQuoteList.put(q1.Opportunity__C,new Set<String>());
        Set<String> str=optyQuoteList.get(q1.Opportunity__C);
        str.add(q1.Id);
        optyQuoteList.put(q1.Opportunity__C,str);
        System.debug('checking++++++++++++'+optyQuoteList);
    }*/
    for(Quote__c q1 :quoteList ){
       // Set <String> str = optyQuoteList.get(q1.Opportunity__C);
        //System.debug('rrrrrrrrrrrr++++++++'+str);
        if((!q1.Opportunity__r.HasOpportunityLineItem||optyQuoteList.get(q1.Opportunity__C)==true)&&q1.Quote_Status__C!='Approved' )
            
            {
                oppIdsMap.put(q1.Id, q1.Opportunity__c);
            if(q1.Opportunity__c!=null&&q1.Opportunity__r.Pricebook2Id==null && pbook!=null)
            {
             q1.Opportunity__r.Pricebook2Id=pbook.Id;
            optyList.add(q1.Opportunity__r);
            }
            if(!pids.contains(q1.Opportunity__r.Pricebook2Id))
            pIds.add(q1.Opportunity__r.Pricebook2Id);
            

        }
    }
    if(!optyList.isempty())
    update(optyList);
    
    if(oppIdsMap.size() > 0){
        if(trigger.isinsert && trigger.isafter)
        {
        List<ID> productIdList = new List<ID>();
        for (Quote_Line_Item__c q : trigger.new) {
            productIdList.add(q.Product2__c);
        }
        List<PricebookEntry> priceBookEntries = [Select id,Productcode, Product2Id, Pricebook2Id From PricebookEntry
                                             where pricebook2id in :pIds and
                                             product2id in :productIdList and isactive = true];   
       System.debug('pricebooks+++++++++++'+pricebookentries+'========='+pbook);                                      
        Map<String,Id> productIdToPricebookEntryIdMap = new Map<string,Id>();
        for(PricebookEntry p : pricebookEntries) {
            productIdToPricebookEntryIdMap.put(p.productcode, p.id);
        }
        for(Quote_Line_Item__c qlt: trigger.new){
            if(optyWithLineItems.containsKey(qlt.Quote__c) && !optyWithLineItems.get(qlt.Quote__c)){//prashant.singh@riverbed.com on 09/27/2012; november release
               OpportunityLineItem opl= new OpportunityLineItem();
               opl.PricebookEntryId= productIdToPricebookEntryIdMap.get(qlt.Product_Code__c);
               opl.Quantity = qlt.Qty_Ordered__c;
               opl.Description =qlt.Product_Code__c;
               opl.UnitPrice = qlt.D_Unit_Price__c;
               opl.R_Sales_Price__c=qlt.Unit_Price__c;
               opl.OpportunityId=oppIdsMap.get(qlt.Quote__c);
               opl.SKU_Reference__c =qlt.SKU_Reference__c;
               oplist.add(opl);
            }
        }
      try{
            insert oplist;
       }catch(DMLException e){
            System.debug('Error in copying Quote Lines to Opp Lines: ' + e.getDMLMessage(0));
        }
    }  
       else if(trigger.isdelete && trigger.isafter)
       {
        List<String> prodCode = new List<String>();
         for (Quote_Line_Item__c q : trigger.old) {
                if(oppIdsMap.containskey(q.Quote__C))
                {
                    prodCode.add(q.product_code__C);
                }
            }
            
            if(prodCode.size()>=1 && oppIdsMap.size()>=1)
            {
                List <OpportunityLineItem> opLineList=[select id,OpportunityId,Description from OpportunityLineItem where OpportunityId in :oppIdsMap.values() and Description in :prodCode];
                if(opLineList!=null && !opLineList.isempty())
                {
                    delete(opLineList);
                }
            }
       }
    
    }
    
}