trigger setLicenseKeyNameOnCreation on License_key__c (before insert) 
{
    for(License_Key__c lk : Trigger.new)
    {
        if(lk.Key__c == null)
        {
            lk.Name = 'Awaiting activation';
        }
    }
}