trigger PartnerSelfServiceContact on Contact (before update, after update) {
	
	//added by Ankita 6/17/2010 for LMS account feed
	if(Trigger.isBefore){
		for(Contact c : trigger.new){
			Contact oldRecord = trigger.oldMap.get(c.Id);
			if(Util.hasChanges(Util.contactFieldNamesForLMS, oldRecord, c)){
				//set flag
				c.Send_to_LMS__c = true;
			}
		}
	}else{
		ContactUserSync.process(trigger.newMap, trigger.oldMap);
	}
}