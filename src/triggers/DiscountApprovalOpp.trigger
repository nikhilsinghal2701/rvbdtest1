trigger DiscountApprovalOpp on Opportunity (before insert, before update) {
    List<Opportunity> oppList = new List<Opportunity>();
    if (trigger.isInsert) {
        if (DiscountApproval.runOnce()) {
            for (Opportunity opp : Trigger.new) {
                if (!opp.StageName.equals('6 - Order Accepted')) {
                    oppList.add(opp);
                }
            }
        }
    } else {
        for (Opportunity opp : Trigger.new) {
            if ( ! opp.StageName.equals('6 - Order Accepted')) {
                System.debug('opp.NSD_Calc__c = ' + opp.NSD_Calc__c);
                if (opp.NSD_Calc__c) {
                    oppList.add(opp);
                }
            }
        }
    }       
    
    if (oppList.size() > 0) {
        PartnerProgramDiscounts.loadAddOppScheduleAndDiscounts(oppList, trigger.oldMap);
        DiscountApproval.runRules(oppList, trigger.oldMap);
    }
}