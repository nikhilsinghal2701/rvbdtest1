//to automate the IsellIsocode and country field on Account object by Anil Madithati 02/29/2012

trigger AutomationOFiSellISOandCountryCode on Account (before insert , before update,after update  ) 
{ List<Account> accountWithISOCodeList = new List<Account>();
  List<Account> accountWithCountryList = new List<Account>();
  List<Account> uAcctList = new List<Account>();
  List<String> isoCodeList = new List<String>();
  List<String> countryList = new List<String>();
  
  if(Trigger.isBefore)
  {
    if(Trigger.isInsert)
    {
        for(Account a : Trigger.new) 
        {
            if(a.ISO_Country_Code__c != null)
            {
                accountWithISOCodeList.add(a);
                isoCodeList.add(a.ISO_Country_Code__c);
            }
            else if(a.D_B_Country__c != null)
            {
                accountWithCountryList.add(a);
                countryList.add(a.D_B_Country__c);
            }
        }
    }
  
    if(Trigger.isUpdate)
    {   Util.AccountUltimateTraverseFromChildToUltiParent(Trigger.newMap,Trigger.oldMap,Trigger.New);
        for(Account a : Trigger.new)
        {
            if(Trigger.newMap.get(a.Id).ISO_Country_Code__c != null && Trigger.newMap.get(a.Id).ISO_Country_Code__c != Trigger.oldMap.get(a.Id).ISO_Country_Code__c)
            {
                accountWithISOCodeList.add(a);
                isoCodeList.add(Trigger.newMap.get(a.Id).ISO_Country_Code__c);
            }
            else if(Trigger.newMap.get(a.Id).D_B_Country__c != null && Trigger.newMap.get(a.Id).D_B_Country__c != Trigger.oldMap.get(a.Id).D_B_Country__c)
            {
                accountWithCountryList.add(a);
                countryList.add(Trigger.newMap.get(a.Id).D_B_Country__c);
            }
        }
    }
    
    if(isoCodeList.size() != 0)
    {    
    Util.updateAccountCountryUsingISOCode(accountWithISOCodeList, isoCodeList);
        
        
    }
    if(countryList.size() != 0)
    {
        util.updateAccountISOCodeUsingCountry(accountWithCountryList, countryList);
    }
  }
    if(Trigger.isAfter){
     if(Trigger.isUpdate){
     for(Account a : Trigger.new)
     {
     if(Trigger.newMap.get(a.Id).ParentId==null && (Trigger.newMap.get(a.Id).OS_Industry_Sector__c != Trigger.oldMap.get(a.Id).OS_Industry_Sector__c||Trigger.newMap.get(a.Id).AnnualRevenue != Trigger.oldMap.get(a.Id).AnnualRevenue ))
     {
     uAcctList.add(a);
     }
     }
     if(uAcctList.size()!=0){
     util.AccountUltimateTraverseFromParentToChild(uAcctList);
     }
     
     }
    }
}