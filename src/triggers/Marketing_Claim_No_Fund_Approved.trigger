trigger Marketing_Claim_No_Fund_Approved on SFDC_MDF_Claim__c (before insert, before update) 
{
    if (Trigger.isInsert)
    {
        for(SFDC_MDF_Claim__c mdfc : Trigger.new)
        {
                SFDC_MDF__c mdfr = [Select Status__c, Id From SFDC_MDF__c where id = :mdfc.MDF__c];
                System.debug('MDF Request ID ************************--> ' + mdfr.id);
                
                If (mdfr.Status__c <> 'Approved')
                {
                    mdfc.addError('Marketing Fund request must be approved before submitting the claim');
                }
            }
        }
    }