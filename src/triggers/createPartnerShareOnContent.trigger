trigger createPartnerShareOnContent on Portlet_Right_Navigation__c (after update) {
	List<Id> rightNavIds = new List<Id>();
	for (Portlet_Right_Navigation__c rightNav : trigger.new) {
		rightNavIds.add(rightNav.Id);
	}
	
	List<Portlet_Right_Navigation__Share> shareList = new List<Portlet_Right_Navigation__Share>();
	//groupId for all partner users
	for (Id rightNavId : rightNavIds) {
		shareList.add(new Portlet_Right_Navigation__Share(ParentId = rightNavId, UserOrGroupId = '00G70000001KLGm', AccessLevel = 'Read',
			RowCause = 'Manual' ));
	}
	
	insert shareList;
}