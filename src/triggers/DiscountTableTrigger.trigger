/*************************************************************************************************************
 Author: Santoshi Mishra
 Purpose: This trigger is created on DiscountTable for data processing.
 Created Date : Oct 2011
*************************************************************************************************************/


trigger DiscountTableTrigger on DiscountCategory__c (after insert) {
	
	DiscountManagementHelper hlpr = new DiscountManagementHelper();
	hlpr.InsertDiscountTableChildRecords(Trigger.new);//This method is called by the Discount Table Trigger to create its child records.
}