trigger setLMSStatus on LMS_Activity__c (before insert, after insert) {
	Set<Id> cids = new Set<Id>();
	Map<String, LMS_Activity__c> activityMap = new Map<String,LMS_Activity__c>();
	for(LMS_Activity__c activity : trigger.new){
		cids.add(activity.Contact__c);
		//String key = activity.Event_Date__c + activity.event_Type__c + activity.lms_Item_id__c + activity.LMS_User_ID__c;
		//activityMap.put(key,activity);
	}
	if(trigger.isafter){
		Map<Id, Contact> contactMap = new Map<Id,Contact>([select id, LMS_Status__c from Contact where id in :cids]);
		for(LMS_Activity__c activity : trigger.new){
			Contact c = contactMap.get(activity.Contact__c);
			c.LMS_Status__c = activity.LMS_Status__c;
		}
		
		update contactMap.values();
	}else{
		//Set<LMS_Activity__c> newActivities = new Set<LMS_Activity__c>();
		//Set<String> newActivities = activityMap.keySet();//new Set<Id>();
		//System.debug('New activities = ' + newActivities);
		/*for(LMS_Activity__c act : activityMap.values()){
			newActivities.add(act.Id);
		}*/
		//newActivities.retainAll(activityMap.values());
		//added to check for existing data in the activities - Ankita 9/24/2010
		List<LMS_Activity__c> existingActivities = [select id, event_date__c, event_Type__c, lms_item_id__c, lms_User_Id__c 
													from LMS_Activity__c where contact__c in :cids];
		for(LMS_Activity__c a : existingActivities){
			String key = a.Event_Date__c + a.event_Type__c + a.lms_Item_id__c + a.LMS_User_ID__c;
		//	System.debug('Key = ' + key);
			if(!activityMap.containsKey(key)){
				activityMap.put(key,a);
			}
		}											
		for(LMS_Activity__c a : trigger.new){
			String key = a.Event_Date__c + a.event_Type__c + a.lms_Item_id__c + a.LMS_User_ID__c;
		//	System.debug('Key = ' + key);
			if(activityMap.containsKey(key)){
				System.debug('Duplicate found');
				a.addError('Duplicate Record, please disregard');
			}else{
				activityMap.put(key,a);
			}
		}
	}
}