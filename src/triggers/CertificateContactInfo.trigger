/*****************************************
  Name : CertificateContactInfo
  Purpose : Trigger on Certificate__c before insert and before update
  Modified By : Rashmi ( Perficient ) 10/01/2013
****************************************/
trigger CertificateContactInfo on Certificate__c ( before insert, after insert, before update, after update ) {
	
	if( trigger.isInsert ){
		
		if( trigger.isBefore ){
			CertificationTriggerHandler.autoPopulateCertificationFields( null , trigger.new, true );
			CertificationTriggerHandler.populateCertificationMasterFields( Trigger.new );
		}else{
			// code to fire before update
			CertificationTriggerHandler.calculateTotalCertifications( trigger.new );
		}
	}
	
	if( trigger.isUpdate ){
		
		if( trigger.isBefore ){
			CertificationTriggerHandler.autoPopulateCertificationFields( trigger.oldMap , trigger.new, false );
		}else{
			// code to fire on after update
			CertificationTriggerHandler.calculateTotalCertificationsOnUpdate( trigger.oldMap, trigger.new );
		}
	}
	
}