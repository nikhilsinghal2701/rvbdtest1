/*************************************************************************************************************
 Author: Santoshi Mishra
 Purpose: This trigger is created on Quote_Line_Item__c
  for data processing.
 Created Date : Oct 2011
*************************************************************************************************************/
trigger QuoteLineTrigger on Quote_Line_Item__c (after insert,after update,before insert,before update) {
    
    //if((trigger.isinsert ||trigger.isupdate)&& trigger.isbefore && DiscountManagementHelper.run){
       // DiscountManagementHelper.run=false;
       // DiscountManagementHelper.UpdateUpliftCalcQuote(trigger.New,Trigger.oldMap,trigger.isupdate);
        
    //}
    
    if((trigger.isinsert ||trigger.isupdate) && trigger.isbefore){

        
        DiscountManagementHelper.setQLIValues(trigger.New,Trigger.oldMap,trigger.isupdate);
        
    }
    
       
    if((trigger.isinsert ||trigger.isupdate)&& trigger.isafter)
    {
     
         /** This method is called to copy opportunity discount detail into Quote on quote insertion..*/
     DiscountManagementHelper.UpdatesDiscountDetailInQuote(trigger.New);
        System.debug('Inside quote line update-----');
        
    }  
    
  

}