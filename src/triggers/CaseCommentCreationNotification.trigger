trigger CaseCommentCreationNotification on CaseComment (after insert) {
    
    
    if(UserInfo.getName().equals('KeyServer Integration User') && CaseTriggerHelper.runOnceCmnt()) {
        Set<Id> caseIds = new Set<Id>();
        for(CaseComment cc :trigger.new) {
            caseIds.add(cc.ParentId);
        }
        
        List<CaseTeamMember> memberList = [Select TeamRole.Name, TeamRoleId, ParentId, MemberId From CaseTeamMember
                                                where ParentId in :caseIds and TeamRole.Name = 'CCed on Email'];
        
        Map<Id, Set<Id>> mapCaseMemberId = new Map<Id, Set<Id>>();
        Set<Id> conIdsTemp = new Set<Id>();
        Set<Id> conIds = new Set<Id>();
        
        for(CaseTeamMember ctm :memberList) {
            conIdsTemp = new Set<Id>();
            if(mapCaseMemberId != null && mapCaseMemberId.containsKey(ctm.ParentId)) {
                conIdsTemp.addAll(mapCaseMemberId.get(ctm.ParentId));
            }
            if(ctm.MemberId != null) {
                conIds.add(ctm.MemberId);
                conIdsTemp.add(ctm.MemberId);
                mapCaseMemberId.put(ctm.ParentId, conIdsTemp);
            }
        }
        System.debug('mapCaseMemberId==' + mapCaseMemberId);
        Map<Id, Contact> mapContact = new Map<Id, Contact>([select Email from Contact where id in :conIds and Email != null]);
        Map<Id, List<String>> mapCaseMemberEmail = new Map<Id, List<Id>>();
        List<String> emailTemp = new List<String>();
        for(Id csid :mapCaseMemberId.keySet()) {
            emailTemp.clear();
            for(Id cnid :mapCaseMemberId.get(csid)) {
                if(mapContact.containsKey(cnid))
                    emailTemp.add(mapContact.get(cnid).Email);
            }
            mapCaseMemberEmail.put(csid, emailTemp);
        }
        System.debug('mapCaseMemberEmail==' + mapCaseMemberEmail);
        Map<Id, Case> caseMap = new Map<Id, Case>([select CaseNumber, Contact.Email from Case where id in :caseIds and Contact.Email != null]);
        List<Messaging.SingleEmailMessage> msgLst = new List<Messaging.SingleEmailMessage>();
        
        List<OrgWideEmailAddress> owaList = [select id, Address from OrgWideEmailAddress where Address = 'support@riverbed.com'];

        for(CaseComment cc :trigger.new) {
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            if(owaList != null && owaList.size() > 0) {
                email.setOrgWideEmailAddressId(owaList[0].id); 
            }
            if(caseMap != null && caseMap.containsKey(cc.ParentId)) {
                email.setToAddresses(new List<String>{caseMap.get(cc.ParentId).Contact.Email});
            }
            if(mapCaseMemberEmail != null && mapCaseMemberEmail.containsKey(cc.ParentId)) {
                email.setCcAddresses(mapCaseMemberEmail.get(cc.ParentId));
            }
            if(caseMap != null && caseMap.containsKey(cc.ParentId) && 
                      ((email.getToAddresses() != null && email.getToAddresses().size() > 0) 
                    || (email.getCcAddresses() != null && email.getCcAddresses().size() > 0))) {
                email.setSubject('New Case Comment Added to Case ['+ caseMap.get(cc.ParentId).CaseNumber + ' ]');
                email.setPlainTextBody(cc.CommentBody);
                msgLst.add(email);
            }
        }
        try{
            if(msgLst != null && msgLst.size() > 0)
                Messaging.sendEmail(msgLst);
        }catch(Exception e){
            System.debug('Error sending mail: '+e.getMessage());
        }
    }
}