/*  This trigger sets the unit cost on quote line item from the product cost.
    For Cat A,B,C,D, Unit Cost = Total Cost of product
    For Cat E, Unit Cost = Unit List Price * Defined %
    
    Rev1 - Added by Rucha - Evaluate oracle category/product family for product on line item and stamp it on line item
    Rev:2 Added by Jaya - marks VSOE NON Compliant field true if it falls out of category
    Rev:3 Added by Anil Madithati NPI 4.0 11.14.2014   
*/
trigger UpdateQuoteLine on Quote_Line_Item__c (before insert, before update) {
    if(Trigger.isBefore){
        Set<Id> pIds = new Set<ID>();
        for(Quote_Line_Item__c ql : trigger.new){
            pIds.add(ql.Product2__c);
        }
        Map<id, Product2> prodMap = new Map<Id, Product2>([select id, Cost__c, Total_Cost__c, Overhead__c from Product2 where id in :pIds]);
        for(Quote_Line_Item__c ql : trigger.new){
            String cat = ql.Category__c == null ? '' : ql.Category__c;
            Decimal listprice = ql.Unit_List_Price__c == null ? 0 : ql.Unit_List_Price__c;
            Decimal overhead = 0;
            Decimal totalCost = 0;
            if(prodMap.containsKey(ql.Product2__c)){
                overhead = prodMap.get(ql.Product2__c).Overhead__c == null ? 0 : prodMap.get(ql.Product2__c).Overhead__c;
                totalCost = prodMap.get(ql.Product2__c).Total_Cost__c == null ? 0 : prodMap.get(ql.Product2__c).Total_Cost__c;
            } 
            /*if(cat.equalsIgnoreCase('E') ){
                ql.Unit_Cost__c = listprice * overhead;
            } else{*/
            if(cat.equalsIgnoreCase('L') && ql.UOM__c.equalsIgnoreCase('MTH')||ql.UOM__c.equalsIgnoreCase('Month')){
                ql.Unit_Cost__c = totalCost;
            }else if(ql.UOM__c.equalsIgnoreCase('MTH')||ql.UOM__c.equalsIgnoreCase('Month') ){//Added by Anil 08.13.2013 
                totalCost=totalCost/12;
                ql.Unit_Cost__c = totalCost;
            }else{
                ql.Unit_Cost__c = totalCost;
            }
        }
        
        //Rev1 - Added by Rucha
        DiscountApprovalHlp.populateProductCategory(Trigger.New);
        // Rev:2
        system.debug(LoggingLevel.INFO,'DiscountDetailCategoryHelper****:'+ Trigger.New);
        DiscountDetailCategoryHelper.validateDiscountRange(Trigger.New);
    }
}