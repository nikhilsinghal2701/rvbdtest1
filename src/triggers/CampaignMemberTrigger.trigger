trigger CampaignMemberTrigger on CampaignMember (before insert, before update) {

    ConciergeAcceptanceCapturing.conciergeAcceptanceCapturingRequestHandler(trigger.new, trigger.oldMap, trigger.isInsert);

}