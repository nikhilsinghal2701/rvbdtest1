/*******************************************************************
Trigger: ProductOracleCategoryTriggers
To execute functionality befor/after DML operations on Product_Oracle_Category__c
Author: Rucha Pradhan
Date: 6/13/2013
********************************************************************/
trigger ProductOracleCategoryTriggers on Product_Oracle_Category__c (before insert,before update) {
	if(Trigger.isBefore){
		List<Product_Oracle_Category__c> pocList = new List<Product_Oracle_Category__c>();
		if(Trigger.isInsert){
			for(Product_Oracle_Category__c poc : Trigger.new){
				if(poc.PWS_Oracle_category_id__c != null){
					pocList.add(poc);
				}
			}
		}
		if(Trigger.isUpdate){
			for(Product_Oracle_Category__c poc : Trigger.new){
				if(poc.PWS_Oracle_category_id__c != Trigger.oldMap.get(poc.Id).get('PWS_Oracle_category_id__c')){
					pocList.add(poc);
				}
			}
		}
		
		if(!pocList.isEmpty()){
			ProductOracleCategoryTriggerManagement.populateCategoryId(pocList);
		}
	}
}