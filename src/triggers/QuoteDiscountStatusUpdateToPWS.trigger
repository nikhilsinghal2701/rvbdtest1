/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger QuoteDiscountStatusUpdateToPWS on Quote__c (after insert, after update) {
    List<Quote__c> updateList=new List<Quote__c>(trigger.new);
    B2BIntegrationUtil.dealDeskSyncToPwsWSDL(updateList);
    for(Quote__c quote:trigger.new){
        if(quote.Force_Sync__c ||quote.Discount_Status__c.equalsIgnoreCase('Not Required')||quote.Discount_Status__c.equalsIgnoreCase('Required')||quote.Discount_Status__c.equalsIgnoreCase('Pending Approval')||quote.Discount_Status__c.equalsIgnoreCase('Approved')||quote.Discount_Status__c.equalsIgnoreCase('Rejected')||quote.Discount_Status__c.equalsIgnoreCase('Request for More Information')||quote.Discount_Status__c.equalsIgnoreCase('More Information')){
            if(trigger.isUpdate){
            Util.usedforOrderingUncheck(trigger.new);
                if(quote.Discount_Status__c!=trigger.oldMap.get(quote.Id).Discount_Status__c || quote.Force_Sync__c){
                    updateList.add(quote);
                }
            }else{
                updateList.add(quote);
            }
        }
    }
    if(updateList.size()>0){
        B2BIntegrationUtil.generateQuoteWSDL(trigger.new);
        
    }   
}