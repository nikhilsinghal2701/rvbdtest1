/*********************************************************************
Name : UpdateCertifications
Created On : 12/126/2011
Author : Clear Task, updated by garrik sturges Oct 18th, 2012
Usage : This trigger is used to update certifications fields when
        contact field is updated.
***********************************************************************/
trigger UpdateCertifications on Contact (after update) {//removing after insert event since we dont have certificates associated on contact insert - Ankita 1/28/2013
        //get all the contact ids
        set <Id> contactIds  = new Set<Id>();    //= trigger.newMap.keyset()
        
        //added by Ankita 1/28/2013 to restrict the execution of the remaining code only on change of required fields
        for(Contact c : trigger.new){
            if(c.LeftCompany__c != trigger.oldMap.get(c.id).LeftCompany__c || c.Support_Engineer__c != trigger.oldMap.get(c.id).Support_Engineer__c 
                || c.RCSP_W__c != trigger.oldMap.get(c.id).RCSP_W__c || c.RCSP_NPM__c != trigger.oldMap.get(c.id).RCSP_NPM__c){
                    contactIds.add(c.Id);
                }
        }
    if(contactIds.size() > 0){  
        //get list of related certs
        List<Certificate__c> certList = [Select c.Support_Engineer__c, c.Contact_RCSP_W__c,
                                                                         c.Contact_RCSP_NPM__c, c.Contact_Left_Account__c,
                                                                         c.Contact__c 
                                                                         From Certificate__c c
                                                                         where Contact__c in :contactIds];
        
        //assign values to certifications                                                
        Contact con;
        for(Certificate__c cf :certList) {
            con = trigger.newMap.get(cf.Contact__c);
            if(con.LeftCompany__c) cf.Contact_Left_Account__c = 'Yes';
            else cf.Contact_Left_Account__c = 'No';
            cf.Support_Engineer__c = con.Support_Engineer__c;
            cf.Contact_RCSP_W__c= con.RCSP_W__c;
            cf.Contact_RCSP_NPM__c= con.RCSP_NPM__c;
        } 
        
        if(certList != null && certList.size() > 0) {
            update certList;
        } 
    }                                              
}