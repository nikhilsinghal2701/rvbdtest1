/*
*Purpose: Calculating the average of reverse bingo fields of case to contact. i.e. Roll up summary kind of functionality.
*By:prashant.singh@riverbed.com
*Date:4/3/2012
*/
trigger ContactReverseBingoSummary on Case (after update) {
    CaseUtil.SendEmailOnOwnerChange(trigger.oldMap,trigger.newMap);    // by Sukhdeep Singh for Ticket# 169134
    if(trigger.isAfter && trigger.isUpdate){ 
        if(trigger.new[0].IsClosed!=trigger.old[0].IsClosed){// Raj- added for ticket# 136646 - 01/22/2013
        if(trigger.new[0].IsClosed && trigger.new[0].ContactId != null){
            Contact updateContact=[Select Id,Communications__c,Diagnostics_Data__c,Responsiveness__c,Technical_Expertise__c from Contact where id=:trigger.new[0].ContactId];
//            AggregateResult[] groupedResults = [select AVG(Technical_Expertise_Formula__c) avg1, AVG(Communications_formula__c) avg2, AVG(Diagnostics_Data_formula__c)avg3, AVG(Responsiveness_formula__c) avg4 from case where ContactId=:trigger.new[0].ContactId]; // deleted Communications_formula__c for ticket# 181510 & 174272
              AggregateResult[] groupedResults = [select AVG(Technical_Expertise_Formula__c) avg1, AVG(Diagnostics_Data_formula__c)avg3, AVG(Responsiveness_formula__c) avg4 from case where ContactId=:trigger.new[0].ContactId];
            for (AggregateResult ar : groupedResults)  {
                System.debug('Average Technical Expertise:' + ar.get('avg1'));
                updateContact.Technical_Expertise__c=(Double)ar.get('avg1');
                //updateContact.Communications__c=(Double)ar.get('avg2');// deleted Communications_formula__c for ticket# 181510 & 174272 by sukhdeep singh
                updateContact.Diagnostics_Data__c=(Double)ar.get('avg3');
                updateContact.Responsiveness__c=(Double)ar.get('avg4');
            }
            try{
                update updateContact;
            }catch(Exception e){
                system.debug('Exception occured during closed case average of contact:'+e);
            }           
        }       
    }
    }
}