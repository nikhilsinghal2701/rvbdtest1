trigger OppAddDefaultSplit on Opportunity (before insert, after insert, after update) {
	List<Split_Commissions__c> newSplits = new List<Split_Commissions__c>();
    List<Split_Commissions__c> deleteSplits = new List<Split_Commissions__c>();
    Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>();
    Map<Id,Id> oppOwnerMap = new Map<Id,Id>();
    List<ID> oppIds = new List<Id>();
    
    If (Trigger.isInsert)
    {
        if(Trigger.isBefore){
        	//set these fields to null in case opp was cloned
        	for(Opportunity Opp:Trigger.New){
        		Opp.Splits_Iteration__c = null;
        		Opp.Split_Rep1__c = null;
        		Opp.Split_Rep2__c = null;
        		Opp.Split_Rep3__c = null;
        		Opp.Split_Rep4__c = null;
        	}
        }else{ //isAfter - pass to @future method
        	
        	
        	for(Opportunity theOpp:Trigger.New){
        		oppIds.add(theOpp.Id);
        	}
        	if(oppIds.size() > 0){
				OppAddDefaultSplitHelper.AddSplits(oppIds);
				OppAddDefaultSplitHelper.addGAMtoSalesTeam(oppIds);
        	}
			//alternative code:
			//OppAddDefaultSplitHelper.AddSplits(Trigger.NewMap.keySet());
			
        }
    } 
    
    else //isUpdate
   {
        for (Opportunity theOpp:Trigger.New) //loop through Opps
        {

            //find the ones where owner has changed and add to Map
            if (theOpp.OwnerId != Trigger.oldMap.get(theOpp.Id).OwnerId){
				oppOwnerMap.put(theOpp.Id,theopp.OwnerId);
            }
            //find the opps where Account has changed
			if(theOpp.AccountId != Trigger.oldMap.get(theOpp.Id).AccountId){
				oppIds.add(theOpp.Id);
			}
            //System.Debug('oppOwnerMap::::::::::::' + oppOwnerMap);
        }
        if(oppOwnerMap.size() > 0){
			OppAddDefaultSplitHelper.UpdateSplits(oppOwnerMap);
        }
        if(oppIds.size() > 0){
			OppAddDefaultSplitHelper.addGAMtoSalesTeam(oppIds);
        }
		
    }

}