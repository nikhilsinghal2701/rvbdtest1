trigger setFieldsOnCreate on Developer__c (before insert) 
{
    private Date getExpiryDate()
    {
        Date d = Date.today();
        return d.addYears(1);
    }

    //Developer__c d = Trigger.new[0];
    for(Developer__c d: trigger.new){
    	d.Name = Generator.getNewCustomerNumber('D');
	    d.Password__c = Generator.getNewPassword();
	    d.Expires__c = getExpiryDate();   
    }    
}