trigger SplitCommisionsVisibility on Split_Commissions__c (before insert,before update, before delete) 
{
		//before update should not be required because the Split Rep is not allowed to change (validation rule) and nothing is executed
		if (Trigger.isDelete) 
		{
			// In a before delete trigger, the trigger accesses the records that will be
			// deleted with the Trigger.old list.
			
			
			//2011-03-10 Arnt Haering ahaering@cleartask.com
			//SOQL and DML in a for loop not recommended
			/*
			for (Split_Commissions__c sc : Trigger.old)
			{
				Visible_to_Partners__c[] vp = [select Id from Visible_to_Partners__c where Opportunity_Name__c = :sc.Opportunity__c and PartnerName__c =:sc.Split_Comm_Rep__c];
				delete vp;
			}
			*/
			
			//ahaering@cleartask.com new code
			//ahaering@cleartask.com create List of Opportunity Ids to query Visible to Partner Records
			List<ID> oppIDs = new List<ID>();
			for (Split_Commissions__c sc : Trigger.old){
				oppIds.add(sc.Id);
			}
			
			//ahaering@cleartask.com query Visible to Partner Recordsand store in Map, creating a Key by concatenating Opportunity_Name__c and ParterName__c.
			//ahaering@cleartask.com These will be more records than we need, we will identify the ones to kill later by this key
			Map<string,Visible_to_Partners__c> vpMap = new Map<string,Visible_to_Partners__c>();
			String string1, string2;
			for(Visible_to_Partners__c vp:[select Id from Visible_to_Partners__c where Opportunity_Name__c in :oppIds]){
				//convert IDs to string so I can concatenate
				string1 = vp.Opportunity_Name__c;
				string2 =  vp.PartnerName__c;
				vpMap.put(string1 + string2, vp);
			}
			
			
			//ahaering@cleartask.com Now loop through the trigger and find the matching Visible_To_Partner__c records in the vpMap and add them to a delete list
			List<Visible_to_Partners__c> deleteVPs = new List<Visible_to_Partners__c>();
			for(Split_Commissions__c sc:Trigger.old){
				string1 = sc.Opportunity__c;
				string2 = sc.Split_Comm_Rep__c;
				if(vpMap.containsKey(string1 + string2)){
					//if the Key exists, add the corresponding record to the delete list
					deleteVPs.add(vpMap.get(string1 + string2));
				}
			}
			
			if(deleteVPs.size() > 0){
				//try{
					delete deleteVPs;
				//}catch (Exception ex){
					//System.Debug('Exception in deleting Split Commissions::::::::: ' + ex.getMessage());
				//}
				
			}
			
			//ahaering@cleartask.com end new Code
			
		} 
		
		if (Trigger.isInsert) 
		{
			List<Visible_to_Partners__c> newMembers = new List<Visible_to_Partners__c>();
			
			for (Split_Commissions__c sc : Trigger.new) 
			{
				if(sc.Is_Opportunity_Owner__c == false){
					Visible_to_Partners__c loggedUser = new Visible_to_Partners__c(Role__c='Split Commission',PartnerName__c=sc.Split_Comm_Rep__c,Opportunity_Name__c=sc.Opportunity__c,Opportunity_Access__c='Edit');
					newMembers.add(loggedUser);
				}
			}
//			System.debug('newMembers ' + newMembers);
			try{
			if(newMembers.Size() > 0)
			{
  				 insert newMembers;
 			}
			}catch(DMLException e){
				System.debug('Exception in adding Split Commission User to Sales Team : '+ e.getDMLMessage(0));
			}
		}
}