trigger OPEXMktgClaimAttachment on Attachment (before delete, after insert) {

	List<Id> parentIdList = new List<Id>();
	Map<Id,Boolean> attachMap = new Map<Id,Boolean>();
	if(trigger.isDelete)
	{
		for(Attachment attach: trigger.old)
		{
			if(String.valueOf(attach.ParentId).startsWith('a0K'))
			{	parentIdList.add(attach.ParentId);
				attachMap.put(attach.Id,true);
			}
		}
	}
	if(trigger.isInsert)
	{
		for(Attachment attach: trigger.new)
		{
			if(String.valueOf(attach.ParentId).startsWith('a0K'))
				parentIdList.add(attach.ParentId);
		}
	}
	if(!parentIdList.isEmpty())
		Submittofinance.attachUpdate(parentIdList,attachMap);
}