trigger AccountOwner on Account (before insert, before update, after Update) {
	if(trigger.isBefore){	
		for (Account o : Trigger.New) {
			String OwnerString = String.valueOf(o.OwnerId);
			if (OwnerString.startsWith('005')) o.OwnerLookup__c = o.OwnerId;
		}
	/*
	Added this to set true value to these two fields IsPartnerStatusChanged and IsPartnerLevelChanged, In case of any change to
	Partner_Status1__c ,Partner_Level__c.
	Added on: 7/5/2012
	Added by: prashant.singh@riverbed.com
	*/
		if(trigger.isInsert && AccountPartnerProgramRediness.isTrigger==false){			
			for (Account a : Trigger.New) {
				if(a.Partner_Status1__c!=null)a.IsPartnerStatusChanged__c=true;
				if(a.Partner_Level__c!=null)a.IsPartnerLevelChanged__c=true;				
			}
			AccountPartnerProgramRediness.isTrigger=true;
		}else if(trigger.isUpdate && AccountPartnerProgramRediness.isTrigger==false){
			for (Account a : Trigger.New) {
				if(a.Partner_Status1__c!=trigger.oldMap.get(a.Id).Partner_Status1__c){
					a.IsPartnerStatusChanged__c=true;
				}
				if(a.Partner_Level__c!=trigger.oldMap.get(a.Id).Partner_Level__c){
					a.IsPartnerLevelChanged__c=true;
				}
			}
			AccountPartnerProgramRediness.isTrigger=true;
		}
	}
	/*
	Added this to handle account share rule upon account owner change, so that distributor/reseller
	will get list of reseller/disti respectively during deal reg.
	Added on: 5/3/2011
	Added by: prashant.singh@riverbed.com
	*/
	if(trigger.isAfter){
		if(trigger.isUpdate){
			set<Id> accountId=new set<Id>();
			List<Distributor_Reseller__c> lstDistRes;
			for(Account acc: trigger.new){
				if(acc.OwnerId!=trigger.oldMap.get(acc.Id).OwnerId && (acc.RecordTypeId=='012300000000NE5AAM'||acc.RecordTypeId=='012300000000NE5')){
					accountId.add(acc.Id);
				}
			}
			if(accountId.size()>0){
				AccountShareHelper asHelper = new AccountShareHelper();
				asHelper.insertIntoAccountShare(accountId);
			}
			/*1.Trigger on account owner change: update all contacts of this account to have the same owner as the account.
			** ticket#109263
			
			try{
				AccountPartnerProgramRediness.updateContactOwner(trigger.new,trigger.oldMap);
			}catch(Exception e){
				trigger.new[0].addError('An issue occured during contact owner update after account owner change');
			}*/
		}		
	}	
}