trigger AccountOwnerChangeSplitUpdate on Account (after update) {

    List<Id> accIds = new List<ID>();
    List<Id> oppIds = new List<Id>();
    List<ID> accOwnerIds=new List<Id>();//Add by Anil 2.2.2013
    Map<Id,Id> oppOwnerMap = new Map<Id,Id>();
    
    System.Debug('Trigger Fired');
    for(Account acc:Trigger.New){
        if(acc.OwnerId != Trigger.oldMap.get(acc.Id).OwnerId){
            accIds.add(acc.Id);
            accOwnerIds.add(acc.OwnerId);
        }
    }
    if(accIds.size() > 0){
	    for(Opportunity opp:[Select ID, OwnerId from Opportunity where AccountId in :accIds and IsClosed = False and OwnerId in :accOwnerIds]){
	        oppOwnerMap.put(opp.Id,opp.OwnerId);
	    }
	    
	    if(oppOwnerMap.keyset().size() > 0){
	        OppAddDefaultSplitHelper.UpdateSplits(oppOwnerMap);
	    }
    }

}