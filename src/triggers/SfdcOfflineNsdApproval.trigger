trigger SfdcOfflineNsdApproval on Quote__c (after update) {
	if(trigger.isAfter && trigger.isUpdate){
		Set<Id> qId=new Set<Id>();
		try{
			for(Quote__c qt:trigger.new){
				if(qt.Discount_Status__c==QuoteApprovalHlp.STATUS_CANCELLED && qt.NSD_Override__c!=Null){
					qId.add(qt.Id);
				}
			}
			if(qId.size()>0){
				DiscountApprovalHlp.offlineApproved(qId);	
			}			
		}catch(Exception e){
			trigger.new[0].addError('Error occured during SFDC offline quote approval process in SfdcOfflineNsdApproval:'+e.getMessage());
		}
	}
}