/* Rivision History 
Sunil Kumar [PRFT] 10/28/2013: Added new block to populate the Referral Amount from Agreement
*********************************/
trigger UpdateSalesGeoRegion on Lead (before update,before insert) 
{
    Set<String> ctrySet = new Set<String>();
    List<Country__c> ctryList = New List<Country__c>();
    Map<String,Country__c> ctryMap = new Map<String,Country__c>();
    List<Lead> ldList = new List<lead>();
    for (Lead L : Trigger.new)
    {
        if(Trigger.isInsert && L.Country != null)
        {
            ctrySet.add(L.Country);
            ldList.add(L);
        }
        if(Trigger.isUpdate && ((Trigger.oldmap.get(L.id).Country != L.Country)||(L.COuntry != null && L.Sales_Geo__c == null)))
        {
            ctrySet.add(L.Country);
            ldList.add(L);
        }                          
    }

    if(!ctrySet.isEMpty())
    {
        ctryList = [Select Name,Country__c,Geo__c,ISO2__c,Sales_Region__c,Concierge_Country__c FROM Country__c 
                        Where Name in:ctrySet or ISO2__c in :ctrySet ];
    
        if(!ctryList.isEMpty())
        {
            for(COuntry__c ct : ctryList)
            {
                ctryMap.put(ct.Name,ct);
                ctryMap.put(ct.ISO2__c,ct);
            }
        }
    
        for(Lead L : ldList)
        {
            if(ctryMap.containskey(L.Country))
            {
                L.Sales_Geo__c = ctryMap.get(L.Country).Geo__c;
                if(L.Sales_Region__c==null)
                    L.Sales_Region__c = ctryMap.get(L.Country).Sales_Region__c;
                else if(L.Sales_Region__c!=null && !L.Sales_Region__c.equalsIgnoreCase('Federal'))
                    L.Sales_Region__c = ctryMap.get(L.Country).Sales_Region__c;
                L.Concierge_Country__c = ctryMap.get(L.Country).Concierge_Country__c;
            }
        }
    }
//****Start****
/* comments as per JIRA PARPRG-201
    Set<Id> agreeIds = new Set<Id>();
    List<Apttus__APTS_Agreement__c> agreeList = New List<Apttus__APTS_Agreement__c>();
    Map<string, Apttus__APTS_Agreement__c> agreeMap = new Map<string, Apttus__APTS_Agreement__c>();
    List<Lead> sLeadList = new List<lead>();

    for (Lead sLead : Trigger.new)
    {
        if(Trigger.isInsert && sLead.Referral_Agreement__c != null)
        {
            agreeIds.add(sLead.Referral_Agreement__c);
            sLeadList.add(sLead);
        }
//          system.debug('agreeInsertList:' +agreeIds);
        if(Trigger.isUpdate && ((Trigger.oldmap.get(sLead.id).Referral_Agreement__c != sLead.Referral_Agreement__c)))
        {
            agreeIds.add(sLead.Referral_Agreement__c);
            sLeadList.add(sLead);
        }
//          system.debug('agreeUpdateList' +agreeIds);
    }
    for(Apttus__APTS_Agreement__c agg : [SELECT Id, Referral_Fee_Amount__c FROM Apttus__APTS_Agreement__c WHERE Id IN:agreeIds]){
        agreeMap.put(agg.Id, agg);  
    }
//  system.debug('AgreeMap:' +agreeMap);
    if(agreeMap!=null && !agreeMap.isEMpty() && !sLeadList.isEMpty()){
        for(Lead aLead: sLeadList){
            if(agreeMap.containsKey(aLead.Referral_Agreement__c)){
                aLead.Referral_Fee__c=agreeMap.get(aLead.Referral_Agreement__c).Referral_Fee_Amount__c;
            }
        }
    }
*/    
//***End****    
    
}