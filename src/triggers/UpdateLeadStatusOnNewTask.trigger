/*
*Description:On Lead - When the button ‘Log a Call’ is clicked the Status on the lead needs to be 
*automatically updated to "Attempted to contact".
*New field:Email_Opt_In__c - automatically check the Opt-in box. Ticket#97610; 
*modified on 02/20/2012; by prashant.singh@riverbed.com
*Created By:Prashant.Singh@riverbed.com
*Created Date:Sep 22,2010
*/
trigger UpdateLeadStatusOnNewTask on Task (before insert) {
    set<Id>leadId=new set<Id>();
    set<Id>conId=new set<Id>();
    List<Lead>lstLead=new List<Lead>();
    List<Lead>updateLstLead=new List<Lead>();
    string lId;
    List<Contact>lstCon=new List<Contact>();
    List<Contact>updateLstCon=new List<Contact>();
    if(trigger.isBefore){
        for(Task tempTask:trigger.new){
            lId=tempTask.whoId==null? '': tempTask.whoId;
            if(lId.startsWith('00Q')){
                leadId.add(tempTask.whoId);
            }else if(lId.startsWith('003')){
                conId.add(tempTask.whoId);
            }           
        }
        if(leadId.size()>0){
            lstLead=[select Id,Status,RecordTypeId,RecordType.Name,Email_Opt_In__c from Lead where Id IN:leadId and lead.RecordType.Name='General Leads'];
            //system.debug('List lead:'+lstLead);
            for(Lead tempLead:lstLead){
                if((tempLead.Status=='Open')&&(tempLead.RecordType.Name=='General Leads')){
                    //tempLead.Status='Attempted to contact(suspect)'; //commented for Concierge by prashant.singh@riverbed.com -- ticket#120819 -- 09/13/2012                   
                    if(UpdateLeadStatusOnNewTaskTest.isTest){
                        tempLead.How_Many_offices_do_you_have__c='0';
                    }
                    //updateLstLead.add(tempLead);  
                }
                tempLead.Email_Opt_In__c=true;
                updateLstLead.add(tempLead);
            }
            if(updateLstLead.size()>0){
                try{
                    update updateLstLead;                   
                }catch(System.DmlException de){
                    string error=de.getMessage();
                    error=error.substring(error.indexOf('first error:')+'first error:'.length()+1,error.lastIndexOf(':')-1);
                    error=error+'.';
                    trigger.new[0].addError(error);                 
                }catch(Exception e){
                    trigger.new[0].addError(e.getMessage());
                }
            }
        }else if(conId.size()>0){
            lstCon=[select Id,Email_Opt_In__c from Contact where Id IN:conId];
            for(Contact tempCon:lstCon){
                tempCon.Email_Opt_In__c=true;
                updateLstCon.add(tempCon);
            }
            if(updateLstCon.size()>0){
                try{
                    update updateLstCon;                   
                }catch(System.DmlException de){
                    string error=de.getMessage();
                    error=error.substring(error.indexOf('first error:')+'first error:'.length()+1,error.lastIndexOf(':')-1);
                    error=error+'.';
                    trigger.new[0].addError(error);                 
                }catch(Exception e){
                    trigger.new[0].addError(e.getMessage());
                }
            }  
        }       
    }
}