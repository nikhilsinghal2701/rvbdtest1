trigger InsertInToSalesTeam on Visible_to_Partners__c (before insert, after update, after delete) 
    {
        if (trigger.isDelete){
            OpportunitySalesTeamHlp.deleteFromSalesTeam(trigger.oldMap);
        }else{
            string result=OpportunitySalesTeamHlp.inserIntoSalesTeam(trigger.new);
            if(result.equalsIgnoreCase('isOwner')){
                trigger.new[0].PartnerName__c.addError('Opportunity Owner automatically recieves the visibility. Please select appropriate user');
            }else if(result.equalsIgnoreCase('isAllowed')){
                trigger.new[0].PartnerName__c.addError('Only Associated Partner Users can be added to the Sales Team'); 
            }else if(result.equalsIgnoreCase('isEdit')){
                trigger.new[0].PartnerName__c.addError('Tier2, Tier3 and Influence Partner Users can be added to the Sales Team only with Read access');
            }else if(result!='success'){
                trigger.new[0].PartnerName__c.addError('An error has occured while adding to sales team');
            }
        }   
        /*
        Set<Id> oppIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        if (Trigger.isDelete) 
        {
            // In a before delete trigger, the trigger accesses the records that will be
            // deleted with the Trigger.old list.
            for (Visible_to_Partners__c vp : Trigger.old)
            {
                oppIds.add(vp.Opportunity_Name__c);
                userIds.add(vp.PartnerName__c);
            }
            //Query for correct OpportunityTeamMember to be deleted based on the current record to be deleted from Visible_to_Partners__c object

           OpportunityTeamMember[] tm = [select Id,TeamMemberRole,OpportunityAccessLevel,OpportunityId,UserId 
                                            from OpportunityTeamMember 
                                            where OpportunityId in :oppIds and UserId in :userIds]; 
                //Delete the actual identified record from OpportunityTeamMember
            delete tm;
                // As trigger is a delete trigger , system will automatically delete the record from Visible_to_Partners__c

            //}
        } 
        else
        {   //system.debug('Sales Team:'+Trigger.new);
            Set<Id> partners = new Set<Id>();
            for(Visible_to_Partners__c vp : trigger.new){
                partners.add(vp.PartnerName__c);
                oppIds.add(vp.Opportunity_Name__c);
            }
            Map<Id,User> users = new Map<Id,User>([select id, Contact.AccountId, UserType from User where id in :partners]);
            Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([select Id,OwnerId,Sold_To_Partner__c, Tier2__c,Tier3_Partner__c, Influence_Partner__c, Ordering_Center__c
                                 from Opportunity where Id in :oppIds]);
            List<OpportunityTeamMember> team = new List<OpportunityTeamMember>();       
            Map<Id, String> accessLevelMap = new Map<Id,String>();           
            for (Visible_to_Partners__c vp : Trigger.new) 
            {
                // Query ownerId from the opportunity to make sure that the owner is not being added again.
             //   Opportunity oq = [select Id,OwnerId,Sold_To_Partner__c, Tier2__c,Tier3_Partner__c, Influence_Partner__c, Ordering_Center__c
               //                from Opportunity where Id=:vp.Opportunity_Name__c];
               Opportunity oq = oppMap.get(vp.Opportunity_Name__c);
                if ((vp.PartnerName__c == NULL) || (oq.OwnerId == vp.PartnerName__c))
                //if (oq.OwnerId == vp.PartnerName__c)
                {
                    vp.PartnerName__c.addError('Opportunity Owner automatically recieves the visibility. Please select appropriate user'); 
                }else if (users.get(vp.PartnerName__c).UserType.equalsIgnoreCase('PowerPartner') && 
                        (oq.Sold_To_Partner__c != users.get(vp.PartnerName__c).Contact.AccountId && 
                        oq.Tier2__c != users.get(vp.PartnerName__c).Contact.AccountId) && //added to allow Tier 2 Partner users to be added to sales team
                        oq.Tier3_Partner__c != users.get(vp.PartnerName__c).Contact.AccountId && //added to allow Tier 3 partner users to be added to sales team: Ankita 3/8/2010
                        oq.Influence_Partner__c != users.get(vp.PartnerName__c).Contact.AccountId && //added to allow Influence partner users to be added to sales team: Ankita 3/8/2010
                        oq.Ordering_Center__c != users.get(vp.PartnerName__c).Contact.AccountId && //added to allow Ordering center partner users to be added to sales team: Ankita 3/8/2010
                        !AddToSalesTeam.PROFILES.contains(UserInfo.getProfileId())){
                    vp.PartnerName__c.addError('Only Associated Partner Users can be added to the Sales Team');
                }else if(users.get(vp.PartnerName__c).UserType.equalsIgnoreCase('PowerPartner') && 
                        (oq.Tier2__c == users.get(vp.PartnerName__c).Contact.AccountId || oq.Tier3_Partner__c == users.get(vp.PartnerName__c).Contact.AccountId
                        || oq.Influence_Partner__c == users.get(vp.PartnerName__c).Contact.AccountId)//added by Ankita 3/8/2010 for allowing Tier3 and Influence partner users to be added only with Read access
                        && vp.Opportunity_Access__c.equalsIgnoreCase('Edit')){
                    vp.PartnerName__c.addError('Tier2, Tier3 and Influence Partner Users can be added to the Sales Team only with Read access');
                }
                else
                {   //system.debug('vp.PartnerName__c:'+vp.PartnerName__c);
                    OpportunityTeamMember otm = new OpportunityTeamMember(TeamMemberRole=vp.Role__c,OpportunityId=vp.Opportunity_Name__c,UserId =vp.PartnerName__c);
                    team.add(otm);
                    accessLevelMap.put(vp.partnerName__c, vp.Opportunity_Access__c);
                    //System.debug('otm --> ' + otm); 
                } 
            }
            //Insert into Opportunity Team Member
            try{
                //system.debug('calling insert');
                insert team;
                //system.debug('RALPH otm: ' + otm);
                //system.debug('called insert');
            }catch(DMLException dme){
                system.debug('ST_Exception:'+dme.getDMLMessage(0));
            }   
            //ankita : moved soql queries out of for loop to avoid running into limits
            
            List<OpportunityShare> osList = [select Id,OpportunityAccessLevel , UserOrGroupId
                                    from OpportunityShare 
                                    where OpportunityId in :oppIds 
                                    and UserOrGroupId in :partners and RowCause='Team'];
                                    
            //Log the queried record 
            //System.debug('os --> ' + os); 
            if(!osList.isEmpty()){
                for(OpportunityShare os : osList){
                    os.OpportunityAccessLevel = accessLevelMap.get(os.UserOrGroupId);
                }
                //osList[0].OpportunityAccessLevel= vp.Opportunity_Access__c;
            }
            //System.debug('OpportunityAccessLevel After --> ' + os.OpportunityAccessLevel); 
            
            //Insert into Opportunity Share based on the user visibility selection 
            update osList;   

        }  */ 
    }