trigger AccountCamCmmAutomation on Account (before insert,before update, after update) {
	if(trigger.isBefore){
        try{
            if(trigger.isInsert){
                AccountChannelManagerAutomation.populateManager(trigger.new,null);
            }else if(trigger.isUpdate){  
                List<Account> updatedAccs = new List<Account>();
                for(Account acc : trigger.new){                    
                    if((acc.Geographic_Coverage__c!=trigger.oldMap.get(acc.Id).Geographic_Coverage__c)||(acc.RecordTypeId!=trigger.oldMap.get(acc.Id).RecordTypeId)
                        ||(acc.Region__c!=trigger.oldMap.get(acc.Id).Region__c)||(acc.BillingCountry!=trigger.oldMap.get(acc.Id).BillingCountry)
                        ||(acc.Partner_Status1__c != trigger.oldMap.get(acc.Id).Partner_Status1__c)){
                            updatedAccs.add(acc);
                        }
                }
                if(updatedAccs.size()>0){ //Added by Anil 05.29.2014 to optimize SOQL executions     
                AccountChannelManagerAutomation.populateManager(updatedAccs,trigger.oldMap);
                }
            }
        }catch(System.nullPointerException ne){
            trigger.new[0].addError('Either Geo,Region or Billing Country left blank, Please fill the value.'); 
        }       
    }
    if(trigger.isAfter){
        try{
            OpportunitySalesTeamHlp.updateOpportunitySalesTeamUponAccountOwnerChange(trigger.new, trigger.oldMap);
        }catch(System.Exception e){
            trigger.new[0].addError(e.getMessage());
        }       
    }
}