trigger CaseDelete on Case (before delete) 
{
    if (Trigger.isDelete) 
    {
        for (Case c : Trigger.old) 
        {
            try
                {
                    c.addError('Case cannot be deleted.');
                }
            catch (DmlException e) 
                {
                    System.debug(e.getMessage());
                }
        }
    } 
}