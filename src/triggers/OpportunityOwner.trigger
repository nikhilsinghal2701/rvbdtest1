/* Rivision History 
Sunil Kumar [PRFT] 10/28/2013: Added new block to populate the Referral Amount from Agreement
*********************************/
trigger OpportunityOwner on Opportunity (before insert, before update) {
    for (Opportunity o : Trigger.New) {
        String OwnerString = String.valueOf(o.OwnerId);
        if (OwnerString.startsWith('005')) o.OwnerLookup__c= o.OwnerId;
    }
	//***Start***
	set<Id>agreeIds = new set<Id>();
	List<Apttus__APTS_Agreement__c> agreeList = new List<Apttus__APTS_Agreement__c>();
	Map<string, Apttus__APTS_Agreement__c> agreeMap = new Map<string, Apttus__APTS_Agreement__c>();
	List<Opportunity> opptyList = new List<Opportunity>();
	
	for(Opportunity oppty: Trigger.New){
		if(Trigger.isInsert && oppty.Referral_Agreement__c != null){
			agreeIds.add(oppty.Referral_Agreement__c);
			opptyList.add(oppty);	
		}
		if(Trigger.isUpdate && ((Trigger.oldMap.get(oppty.Id).Referral_Agreement__c !=oppty.Referral_Agreement__c))){
			agreeIds.add(oppty.Referral_Agreement__c);
			opptyList.add(oppty);	
		}	
	}	
	for(Apttus__APTS_Agreement__c agg : [SELECT Id, Referral_Fee_Amount__c, Apttus__Contract_End_Date__c, Apttus__Activated_Date__c,Apttus__Contract_Start_Date__c 
	                                     FROM Apttus__APTS_Agreement__c WHERE Id IN:agreeIds]){
		agreeMap.put(agg.Id, agg);	
	}	
	if(agreeMap!=null && !agreeMap.isEMpty() && !opptyList.isEMpty()){
		for(Opportunity op : opptyList){
			if(agreeMap.containsKey(op.Referral_Agreement__c)){
				if(agreeMap.get(op.Referral_Agreement__c).Referral_Fee_Amount__c!= null)
					op.Referral_Fee__c = agreeMap.get(op.Referral_Agreement__c).Referral_Fee_Amount__c;
				if(agreeMap.get(op.Referral_Agreement__c).Apttus__Contract_Start_Date__c!= null)
					op.Referral_Agreement_Start_Date__c = agreeMap.get(op.Referral_Agreement__c).Apttus__Contract_Start_Date__c;
				if(agreeMap.get(op.Referral_Agreement__c).Apttus__Contract_End_Date__c!= null)
					op.Referral_Agreement_End_Date__c = agreeMap.get(op.Referral_Agreement__c).Apttus__Contract_End_Date__c;							
			}		
		}
	}//***End***/  
}