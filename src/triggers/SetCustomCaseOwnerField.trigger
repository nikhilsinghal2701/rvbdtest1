/*
    Sets the custom Case_Owner__c User lookup field for reporting. (Case 30708)

    06/07/12 SV@IC - Created
*/
trigger SetCustomCaseOwnerField on Case (before insert, before update, after update) {
Set<Id> Ids=new Set<Id>();
if(Trigger.isBefore && !system.isBatch()){
   for(Case cs : trigger.new){
       String id = cs.ownerid;
       if(id != null && id != '' && id.startswith('005')){
           cs.Case_Owner__c = id;
       }else{
           cs.Case_Owner__c = null;
       }
   }
   
   }
   if(Trigger.isAfter && !system.isBatch()){
        for(Case cs : trigger.new){
        /*if(Limits.getFutureCalls() >= Limits.getLimitFutureCalls()) {
         system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
         } else {
            CaseHttp.getTheCase(cs.id);
            }*/
            Ids.add(cs.id); 
        }
        if(Ids.Size()>0){
                if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
                    system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
                }
                else {CaseHttp.getTheCase(Ids);}
            }
    }
   
   if(trigger.isBefore && trigger.isUpdate && !system.isBatch())    // by Sukhdeep Singh for Ticket# 181510, 175775, 169136
  { 
     try
      {
        /* String result = '';//CaseUtil.chkFieldRequired(Trigger.old, Trigger.new);
         if(result != null)
             Trigger.new[0].addError(result);*/
             CaseUtil.chkFieldRequired(trigger.oldMap,trigger.newMap);
          
      }
      catch (Exception e)
      {
          System.debug('Trigger checkFieldRequired Exception: '+e);
      }
  }
    
}