trigger setGlbLicenseKeyNameOnCreation on License_key_GLB__c (before insert) 
{
    for(License_Key_GLB__c lk : Trigger.new)
    {
        if(lk.Key__c == null)
        {
            lk.Name = 'Awaiting activation';
        }
    }
}