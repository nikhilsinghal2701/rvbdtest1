/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger AccountAuthorizationTrigger on Account_Authorizations__c ( before insert, after insert, before update, after update, before delete ) {
    
    List<ID> objIDs=new List<ID>();
           
    if( trigger.isInsert ){
        if( trigger.isBefore ){ 
            // code to fire before Insert
            AccountAuthorizationTriggerHandler.autoPopulateAuthMasterFieldsOnAccAuthorizations( trigger.new );
        }else{
            // code to fire after Insert
            AccountAuthorizationTriggerHandler.updateAuthorizationSpecializationFieldOnAccount( trigger.new );
            AccountAuthorizationTriggerHandler.sendEmailNotification(trigger.new, trigger.new, 'new');
        }
    }
    
    if( trigger.isUpdate ){
        if( trigger.isBefore ){
            AccountAuthorizationTriggerHandler.updateCompetencyProbationDateOnAuthStatusChange( trigger.oldMap, trigger.new );
        }else{
            AccountAuthorizationTriggerHandler.updateAccAuthorizationSpecializationFieldOnAuthUpdate( trigger.oldMap, trigger.new );
            AccountAuthorizationTriggerHandler.sendEmailNotification(trigger.new, trigger.old, 'update');
        }
    }
     if(trigger.isdelete && trigger.isBefore){
        AccountAuthorizationTriggerHandler.updateAccAuthorizationSpecializationFieldOnAuthDelete(trigger.oldMap, trigger.new);
        for( ID a:trigger.oldMap.keySet()){objIDs.add(a);}

          if(objIDs.size()>0)
          {
          if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
          system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
            }
          
          else {
            B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs); 
          } 
        }
    }
}