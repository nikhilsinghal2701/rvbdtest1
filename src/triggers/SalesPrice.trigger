trigger SalesPrice on OpportunityLineItem (Before insert, before update,before delete)
 {
     Set<String> st = new Set<String>{'System Administrator','System Administrator (no author apex)','Forecast Manager'};
      List<Quote__c> qtc= new List<Quote__c>();
      Opportunity op= new Opportunity();
      String result= Userinfo.getProfileId();
      profile pf = new profile();
      ID opt;
      Set<Id> oppIds = new Set<Id>();
      
    try
      {   
      if(Trigger.isDelete)
      {
          for(OpportunityLineItem i : trigger.old){
          oppIds.add(i.OpportunityId);
               } 
            
            
            
            pf=[select id,name from profile where id=: result];
              System.debug('&&&&&&&'+pf.name);
              String prof=pf.name;
                List<OpportunityLineItem> items;
                
                
           if(st.contains(prof))
              {
               
                }
               else
               {
               Map<Id,Opportunity> oppList = new Map<Id, Opportunity>([select id,StageName from Opportunity where id in :oppIds]);
                for(OpportunityLineItem a :Trigger.old)
                  {
                     op = oppList.get(a.OpportunityId);
                      System.debug('*******'+ opt);
                       if(op.StageName=='6 - Order Accepted' || op.StageName=='7 - Closed (Not Won)')
                          {
                            Trigger.old[0].addError('Products can not be added to closed opportunity');
                          } 
                    }  
               } 
            
                          
                             
      }      
      else
       {
          for(OpportunityLineItem i : trigger.New){
          oppIds.add(i.OpportunityId);
               } 
       }   
               
           List<Quote__c> qt = [select qe.id,qe.Forecasted_Quote__c, qe.Opportunity__c from Quote__c qe where qe.Forecasted_Quote__c=true and qe.Opportunity__c in :oppIds];
           
          
           for(Quote__c qte:qt)
             {
               qte.Forecasted_Quote__c=false;
               qtc.add(qte);
              }
            if(!qtc.IsEmpty()){update qtc;}           
           
           
           
           if((Trigger.isUpdate) ||(Trigger.isInsert))
           {
              pf=[select id,name from profile where id=: result];
              System.debug('&&&&&&&'+pf.name);
              String prof=pf.name;
                List<OpportunityLineItem> items;
                Boolean bl=st.contains(prof);
                
            if(st.contains(prof))
              {
               
                }
               else
               {
               Map<Id,Opportunity> oppList = new Map<Id, Opportunity>([select id,StageName from Opportunity where id in :oppIds]);
                for(OpportunityLineItem a :Trigger.old)
                  {
                     op = oppList.get(a.OpportunityId);
                      System.debug('*******'+ opt);
                       if(op.StageName=='6 - Order Accepted' || op.StageName=='7 - Closed (Not Won)')
                          {
                            Trigger.new[0].addError('Products can not be added to closed opportunity');
                            break;
                          } 
                    }  
               } 

                
             
       
           for(OpportunityLineItem a :Trigger.new)
            {
              if (a.Discount_Percentage__c!=null)
                 {
                   a.UnitPrice = (a.ListPrice)*(1-(a.Discount_Percentage__c/100));
                  }
              else
                  {
                     System.debug('Due to null value');
                   }
            }
            
            }
             
         
            
         } catch(Exception e)
             {System.debug(e);}
             
}