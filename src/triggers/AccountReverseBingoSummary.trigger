/*
*Purpose: Calculating the average of reverse bingo fields of contact to account. i.e. Roll up summary kind of functionality.
*By:prashant.singh@riverbed.com
*Date:4/3/2012
*/
trigger AccountReverseBingoSummary on Contact (after update) {
	if(trigger.isAfter && trigger.isUpdate){
		if(trigger.oldMap.get(trigger.new[0].id).Technical_Expertise__c!=trigger.new[0].Technical_Expertise__c ||
			trigger.oldMap.get(trigger.new[0].id).Communications__c!=trigger.new[0].Communications__c ||
			trigger.oldMap.get(trigger.new[0].id).Diagnostics_Data__c!=trigger.new[0].Diagnostics_Data__c ||
			trigger.oldMap.get(trigger.new[0].id).Responsiveness__c!=trigger.new[0].Responsiveness__c){
			Account updateAccount=[Select Id,Communications__c,Diagnostics_Data__c,Responsiveness__c,
									Technical_Expertise__c from Account where id=:trigger.new[0].AccountId];
			AggregateResult[] groupedResults = [select AVG(Technical_Expertise__c) avg1, AVG(Communications__c) avg2, 
												AVG(Diagnostics_Data__c)avg3, AVG(Responsiveness__c) avg4 from contact 
												where AccountId=:trigger.new[0].AccountId];
			for (AggregateResult ar : groupedResults)  {
			    System.debug('Average Technical Expertise:' + ar.get('avg1'));
			    updateAccount.Technical_Expertise__c=(Double)ar.get('avg1');
			    updateAccount.Communications__c=(Double)ar.get('avg2');
			    updateAccount.Diagnostics_Data__c=(Double)ar.get('avg3');
			    updateAccount.Responsiveness__c=(Double)ar.get('avg4');
			}
			try{
				update updateAccount;
			}catch(Exception e){
				system.debug('Exception occured during closed case average of contact:'+e);
			}			
		}		
	}
}