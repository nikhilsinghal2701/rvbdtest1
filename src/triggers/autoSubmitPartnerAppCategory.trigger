trigger autoSubmitPartnerAppCategory on Partner_Application_Category__c (before update, after update) {
	
	System.debug('autoSubmitPartnerAppCategory trigger before update');
	List<Id> partnerApplicationIds = new List<Id>();
	
	if (trigger.isBefore) {
		for (Partner_Application_Category__c partnerAppCategory : trigger.new) {
			if (partnerAppCategory.Unapproved_Tasks__c == 0) {
				String status = trigger.oldMap.get(partnerAppCategory.Id).Status__c;
				try {
					if (!status.equalsIgnoreCase('Approved'))
						partnerAppCategory.Status__c = 'Approved';
				} catch (Exception e) {
					partnerAppCategory.addError(e.getMessage());
				}	
			} else if (partnerAppCategory.Incomplete_Tasks__c == 0) {
				String status = trigger.oldMap.get(partnerAppCategory.Id).Status__c;
				try {
					//submitForApproval(partnerAppCategory.Id);
					if (!status.equalsIgnoreCase('Submitted for Review'))
						partnerAppCategory.Status__c = 'Submitted for Review';
					//partnerApplicationCategories.add(partnerAppCategory.Partner_Application__c);
				} catch (Exception e) {
					partnerAppCategory.addError(e.getMessage());
				}	
			}
		}
	} else {
		for (Partner_Application_Category__c partnerAppCategory : trigger.new) {
			partnerApplicationIds.add(partnerAppCategory.Partner_Application__c);
		}
		
		List<Partner_Application__c> updateList = new List<Partner_Application__c>();
		for (Partner_Application__c partnerApplication : 
			[Select p.Id, p.Application_Status__c, p.Application_Approval_Date__c,
				(Select Status__c From R00N80000002kYESEA2__r where Status__c != 'Approved') 
				From Partner_Application__c p 
				where Id In :partnerApplicationIds]) {
			List<Partner_Application_Category__c> l = partnerApplication.R00N80000002kYESEA2__r;
			System.debug('id = ' + partnerApplication.id);
			System.debug('l = ' + l.size());
			if (l.size() == 0) {
				partnerApplication.Application_Status__c = 'Approved';
				partnerApplication.Application_Approval_Date__c = Date.today();
				updateList.add(partnerApplication);
			} else if (	partnerApplication.Application_Status__c.equals('Approved')) {
				partnerApplication.Application_Status__c = 'Pending Approval';
				partnerApplication.Application_Approval_Date__c = null;
				updateList.add(partnerApplication);
			}		
		}
		
		if (! updateList.isEmpty()) {
			update updateList;	
		}
	}
	
	//get the application attached to the cateory and check
/*	List<Partner_Application_Category__c> partnerAppCategoryList = [Select Id, Incomplete_Tasks__c, Partner_Application__c from Partner_Application_Category__c where Partner_Application__c In :partnerApplicationCategories];
	Map<String, Integer> partnerApps = new Map<String, Integer>();
	for (Partner_Application_Category__c partnerAppCategory : partnerAppCategoryList) {
		Integer count = partnerApps.get(partnerAppCategory.Partner_Application__c);
		if (count == null) {
			count = 0;
		}
		System.debug('Incomplete_Tasks__c = ' + partnerAppCategory.id + ' : ' +  partnerAppCategory.Incomplete_Tasks__c.intValue());
		count = count + partnerAppCategory.Incomplete_Tasks__c.intValue();
		partnerApps.put(partnerAppCategory.Partner_Application__c, count);
	}
	
	List<Partner_Application__c> partnerAppList = new List<Partner_Application__c>();
	for (String s : partnerApps.keySet()) {
		Integer count = partnerApps.get(s);
		if (count == 0) {
			partnerAppList.add(new Partner_Application__c(Id = s, Application_Status__c = 'Approved', Application_Approval_Date__c = Date.today()));
		}
	}
	System.debug('partnerAppList=' + partnerAppList.size());
	if (! partnerAppList.isEmpty()) {
		update partnerAppList;
	}*/
	
    /*
    * Submit the Record for approval
    */
    private boolean submitForApproval(Id partnerAppCategoryId){
    
        Approval.ProcessSubmitRequest approvalReq = new Approval.ProcessSubmitRequest();
        approvalReq.setComments('Submitted.');
        approvalReq.setObjectId(partnerAppCategoryId);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(approvalReq);
        
        // Verify the result
        System.assert(result.isSuccess());    
        system.debug('result' + result);
        return result.isSuccess();
    }
	
}