trigger postLeadConvert on Lead (before update) {

	for (Lead l : trigger.new) {
		//get the lead 
		System.debug('lead id = ' + l.Id);
		//get isConverted value
		Boolean isConverted = l.isConverted;
		//old converted value
		Boolean oldIsConverted = trigger.oldMap.get(l.Id).isConverted;
		if (isConverted && (! oldIsConverted) && (!PostLeadConversion.runOnce)) {
			PostLeadConversion.runOnce = true;
			PostLeadConversion postLead = new PostLeadConversion();
			try {
				postLead.process(l);
			} catch (RuleEvaluatorException e) {
				System.debug('RuleEvaluatorException : ' + e.getMessage());
				l.addError('Error: ' + e.getMessage());	
			} catch (Exception e) {
				System.debug('Error: ' + e.getMessage());
				l.addError('Error: ' + e.getMessage());	
			}
		}
	}
}