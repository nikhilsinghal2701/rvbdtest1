public class ApplicationTaskInboxController {

	private List<Application_Task__c> applicationTasks;
	private String debug;
	
	public ApplicationTaskInboxController() {
	
	}
	
	public List<Application_Task__c> getApplicationTasks() {
		System.debug('Start getApplicationTasks()');		
		if (applicationTasks == null) {
			//get the accountId of the logged in partner user
			applicationTasks = [Select a.Id, a.Status__c, a.Name, a.Description__c, a.Due_Date__c, a.Application_Category__r.Name, 
				a.Application_Category__c From Application_Task__c a
				where a.Assigned_to__c = :UserInfo.getUserId() //and a.Status__c != 'Complete'
				order by a.Application_Category__r.Name, Due_Date__c];
		}
		System.debug('No Of Tasks' + applicationTasks.size());
		debug = 'No Of Tasks = ' + applicationTasks.size();	
		return applicationTasks;
	}
	
	public String getDebug() { return debug; }
}