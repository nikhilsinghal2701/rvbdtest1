/*
    Class: RCSCertRollupBatch
    Purpose: to roll up certifications from child to parent and grand parent account including siblings.
    			This batch rolls up only certs RCSA, RCSP and RCSA/RCSP
                Note: This class takes care of only three levels of account hirearchy. 
    Author: Jaya ( Perficient )
    Created Date: 7/25/2014                 
    Reference: pc=> Partner Competency  
*/
global without sharing class RCSCertRollupBatch implements Database.Batchable<sObject>, Database.Stateful{
	 global Map<String,Acct_Cert_Summary__c> globalPartnerCompetency;
	 global Set<Id> accountIds = new Set<Id>();
     global RCSCertRollupBatch(){
        globalPartnerCompetency = new Map<String,Acct_Cert_Summary__c>();
     }
     global Database.QueryLocator start(Database.BatchableContext BC){
        String general_TAP = Constant.GENERAL_PARTNER_COMPETENCY;
        Id partnerRecordType = CertRollup.getRecordType(Constant.ACCOUNT_PARTNER);
        String distyTypeAcct = Constant.DISTY_ACCOUNT;
        String query = 'SELECT Id,Account__c,RCSA__c,RCSP__c,RCSA_RCSP__c, TAP__c,Account__r.RecordTypeId, '
                            +' Roll_Up_RCSA__c,Roll_Up_RCSP__c,Roll_Up_RCSA_RCSP__c, '
                            +' Account__r.ParentId, Account__r.Parent.Do_not_rollup_Certs__c, '
                            +' Account__r.Parent.Parent.Do_not_rollup_Certs__c '
                            +' FROM Acct_Cert_Summary__c WHERE Account__r.RecordTypeId =: partnerRecordType '
        						+' AND Account__r.Type !=: distyTypeAcct ';
        // TAP__c !=: general_TAP AND general partner competency should also be considered 
        Set<Id> relatedacctIds = new Set<Id>();// is to retrive all realted accounts
        if(!accountIds.isEmpty()){
        	relatedacctIds = CertRollupBatchHelper.getRelatedAccounts(accountIds);
        	query += ' AND Account__c IN: relatedacctIds';
        }
        return Database.getQueryLocator(query);
      } 
     global void execute(Database.BatchableContext bcontext, List<sObject> scope){
     	Map<String,Acct_Cert_Summary__c> updatedglobalPC = new Map<String,Acct_Cert_Summary__c>();
     	updatedglobalPC = CertRollupRCSPRCSABatchHelper.certRollup(bcontext, scope, globalPartnerCompetency);
     	globalPartnerCompetency.putAll(updatedglobalPC);
     } 
     global void finish(Database.BatchableContext bcontext){
     	CompetencyRollDownBatch CompetencyStatus = new CompetencyRollDownBatch();
     	if(!accountIds.isEmpty()){
     		CompetencyStatus.accountIds.addAll(accountIds);
     	}
     	Database.executeBatch(CompetencyStatus);
     }      
}