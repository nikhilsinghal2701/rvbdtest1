@isTest
private class TestRQIAverageCtrl {

    static testMethod void testRQIAverage() {	
		TestAQIRQIUtils testTools = new TestAQIRQIUtils();
        User testUser = testTools.createMember();
        
        Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
    
    	Case caseObj = [SELECT Id,KCS_Known_vs_New__c,Case_Owner__c FROM Case WHERE Id NOT IN (Select Case__c FROM RQI_Audit__c)];
    	InQuira_Article_Info__c article = testTools.createArticle();
    	article.Article_Created_Date__c = DateTime.valueOf('2013-10-05 20:03:20');
    	insert article;
    	
    	// create Case Article Link
    	InQuira_Case_Info__c caseInfo = testTools.createCaseInfo();
    	caseInfo.Document_ID__c = article.Document_ID__c;
    	caseInfo.Related_Case__c = caseObj.Id;
    	caseInfo.Related_InQuira_Article__c = article.Id;
    	insert caseInfo;
    	update caseObj;
        
		List<case> needOneCase = [SELECT Id,KCS_Known_vs_New__c FROM Case LIMIT 1];
		system.debug('Case KCS: ' + needOneCase[0].KCS_Known_vs_New__c);
		/*
		RQI_Audit__c assessment = new RQI_Audit__c(Case__c = needOneCase[0].Id, 
									  Answer_New_1__c  = 'No' , Answer_New_2__c  = 'Yes', Answer_New_3__c  = 'No',
									  Answer_New_4__c  = 'Yes', Answer_New_5__c  = 'No', Answer_New_6__c  = 'Yes',
									  Answer_New_7__c  = 'No', Answer_New_8__c  = 'Yes', Answer_New_9__c  = 'No', Answer_New_10__c ='Yes',
									  Answer_New_11__c = 'Yes', Status__c='Complete',
									  Author__c = testUser.Id);*/
		RQI_Audit__c assessment = new RQI_Audit__c(Case__c = needOneCase[0].Id, 
									  Answer_Known_1__c  = 'Yes' , Answer_Known_2__c  = 'Yes', Answer_Known_3__c  = 'Yes', Answer_Known_4__c = 'Yes', Status__c = 'Complete',
									  Author__c = testUser.Id);
									  									  
		List<RecordType> recType;
		recType = [SELECT Id FROM RecordType WHERE Name='Known Case'];
	    assessment.RecordTypeId = recType[0].Id;
	    insert assessment;
	    RQI_Audit__c score = [SELECT Id, RecordType.Id, RecordType.Name FROM RQI_Audit__c WHERE Id =:assessment.Id];
    	
		RQIAverageCtrl AAC = new RQIAverageCtrl(new ApexPages.standardController(assessment));
		System.assert(AAC.theAverage != null);
		Test.stopTest();
    }
}