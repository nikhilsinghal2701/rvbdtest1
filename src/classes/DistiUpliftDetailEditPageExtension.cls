/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the extension class to DiscountUpliftDetailEditPage

Created Date : Oct 2011
*******************************************************************************************************/


public with sharing class DistiUpliftDetailEditPageExtension {
    Disti_Uplift_Detail__c pageObj;    
     String DistiUpliftId {get;set;}
     public String ParentName {get;set;}
    public List <Disti_Uplift_Detail__c> detailList {set;get;} 
      
    public DistiUpliftDetailEditPageExtension(ApexPages.StandardController controller) {
    pageobj=(Disti_Uplift_Detail__c) controller.getRecord();
        if(System.currentPageReference().getParameters().get('id')!=null)
        {
        DistiUpliftId = pageobj.Disti_Uplift__c;
        ParentName = pageobj.Disti_Uplift__r.Name;
        
        }
        else
        {
        DistiUpliftId = System.currentPageReference().getParameters().get('retUrl').substring(1);
        ParentName = System.currentPageReference().getParameters().get('parentName');
        pageobj.Disti_Uplift__c=DistiUpliftId;
        
        }
        
        detailList = [select Id,Name,Category_Master__c,category_Master__r.description__c,category_Master__r.Name,Disti_Uplift__C,Value__C,Uplift_of_Net__c from Disti_Uplift_Detail__c where Disti_Uplift__C =: DistiUpliftId order by category_Master__r.Name];
    }
    
     public pagereference Save()
      {
        update detailList;
        return (new Pagereference('/'+DistiUpliftId ));
      }
      
      public pagereference Cancel()
      {
        
        return (new Pagereference('/'+DistiUpliftId ));
      }

}