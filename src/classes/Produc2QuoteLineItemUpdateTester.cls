@isTest
private class Produc2QuoteLineItemUpdateTester {
static testMethod void Produc2QuoteLineItemUpdateTester(){ 
    Produc2QuoteLineItemUpdate prUpdate = new Produc2QuoteLineItemUpdate();
    prUpdate.query = 'SELECT Id,Product2__c,Product2__r.Old_cost__c,Product2__r.cost__c,Product2__r.RunBatch__c,Product2__r.OldCostEqualToNewCost__c FROM Quote_Line_Item__c where Product2__r.OldCostEqualToNewCost__c=True Limit 200';
    Test.startTest();
    Database.executeBatch(prUpdate, 200);
    Test.stopTest();
  }
  public static testMethod void testschedule() {
    test.starttest();
    SchedulerProduc2QuoteLineItemUpdate sco = new SchedulerProduc2QuoteLineItemUpdate();
    String sch = '0 0 23 * * ?';
    system.schedule('Produc2QuoteLineItemUpdate', sch, sco);
    test.stopTest();
    }
 }