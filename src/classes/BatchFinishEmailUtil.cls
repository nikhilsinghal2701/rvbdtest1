public class BatchFinishEmailUtil{
    
    public static Messaging.SingleEmailMessage finishEmail(Database.BatchableContext BC){
        
        messaging.singleEmailMessage mail = new messaging.singleEmailMessage();
        AsyncApexJob a = [SELECT Id,
                             Status,
                             NumberOfErrors,
                             JobItemsProcessed,
                             TotalJobItems, 
                             CompletedDate,
                             ExtendedStatus,
                             ApexClass.name,
                             CreatedBy.Email,
                             CreatedBy.Name
                      FROM AsyncApexJob 
                      WHERE Id =:BC.getJobId()];
   
    if(!(Label.BatchFinishEmailRecipient.equalsIgnoreCase('none'))){
        mail.setToAddresses(Label.BatchFinishEmailRecipient.split(';'));
        
    }
    mail.setSubject('Batch Code Completed -- '+a.ApexClass.name);
    mail.setUseSignature(false);

    string td1='"border:1px solid orange; width=200px;"';
    string td2='"width=200px; border:1px solid orange; background-color:red; color:white; font-weight:bold;"';
    string tdHead='"border:1px solid orange; width=200px; color:white; background-color:orange; font-weight:bold;"';
    string ExtendedStatus='';
    if(a.ExtendedStatus!=null)
        ExtendedStatus=a.ExtendedStatus;
    string tdErrors=td1;
    if(a.NumberOfErrors>0)
        tdErrors=td2; 
    string htmlBody = '<div style="border:2px solid orange; border-radius:15px;"><p>Hi,</p><p><span style="color:brown; font-weight:bolder;">Salesforce</span> completed running <b>Apex Batch Code</b>.</p>'
    +'<p>Results:</p>'
    +'<center><table style="border:3px solid orange; border-collapse:collapse;">'
    +'<tr><td style='+tdHead+'>Class Name</td><td style='+tdHead+'>'+a.ApexClass.name+'</td></tr>'
    +'<tr><td style='+td1+'>Completed Date</td><td style='+td1+'>'+a.CompletedDate+'</td></tr>'
    +'<tr><td style='+td1+'>Status</td><td style='+td1+'>'+a.Status+'</td></tr>'
    +'<tr><td style='+td1+'>Job Items Processed</td><td style='+td1+'>'+a.JobItemsProcessed+' / '+a.TotalJobItems+'</td></tr>'
    +'<tr><td style='+td1+'>NumberOfErrors</td><td style='+tdErrors+'>'+a.NumberOfErrors+'</td></tr>'
    +'<tr><td style='+td1+'>Extended Status</td><td style='+td1+'>'+ExtendedStatus+'</td></tr>'
    +'<tr><td style='+tdHead+'>Created By</td><td style='+tdHead+'>'+a.CreatedBy.Name+' ('+a.CreatedBy.Email+')</td></tr>'
    +'</table></center>'
    +'<p>RVBD Salesforce Team!</p><p><span style="font-family:"Courier New", Courier, monospace; color:orange; font-weight:bold; font-size:larger;"></span></p></div>';

    mail.setHtmlBody(htmlBody);
    return mail;
    }
 }