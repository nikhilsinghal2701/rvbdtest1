/*
** Purpose: SFDC Outage Project
** By: prashant.singh@riverbed.com
** Date: 10/8/2012
*/
public with sharing class SFDC_OutageMessageController {
    private Id id;
    public String message{get;set;}
    private Opportunity opportunity;
    private Quote__c quote;
    public Boolean show{get;set;}
    public SFDC_OutageMessageController(ApexPages.StandardController controller){
        id = ApexPages.currentPage().getParameters().get('id');
        if(String.valueOf(id).startsWith('006')){
            List<Quote__c> manualApproveQuote=[select id,NSD_Override__c from Quote__c where Opportunity__c=:id and NSD_Override__c!=null limit 1];
            if(!manualApproveQuote.isEmpty()){
                show=true;
                message='The discount on this opportunity has been approved by manual process';
            }else if(manualApproveQuote.isEmpty()){
                show=false;
                message='The discount on this opportunity has been approved by standard approved';
            }
        }else if(String.valueOf(id).startsWith('a0o')){
            this.quote=[select id,NSD_Override__c from Quote__c where Id=:id];
            if(quote!=NULL && quote.NSD_Override__c!=NULL){
                show=true;
                message='This Quote has been approved by manual process';
            }else if(quote!=NULL && quote.NSD_Override__c==NULL){
                show=false;
                message='This Quote has been approved by standard process';
            }
        }
    }
    public void onLoad(){
        show=show;
        message=message; 
    }
}