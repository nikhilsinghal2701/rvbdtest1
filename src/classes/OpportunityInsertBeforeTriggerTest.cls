@isTest(SeeAllData=true)
private class OpportunityInsertBeforeTriggerTest {

    static testMethod void myUnitTest() {

        //Rohit - 01082010
        DiscountApproval.isTest = true;

        List<Account> lstAcc=new List<Account>();
        Opportunity testOpp =new Opportunity();
        Opportunity testOpp1=new Opportunity();
        Opportunity testOpp2=new Opportunity();
        Opportunity testOpp3=new Opportunity();
        RecordType accRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
        Account cusAcc=new Account(RecordTypeId=accCusRT.Id,name='CustomeraAAccount',Type='Customer',Industry='Education', accountnumber = '123');
      /*  cusAcc.RecordTypeId=accCusRT.Id;
        cusAcc.name='CustomeraAAccount';
        cusAcc.Type='Customer';
        cusAcc.Industry='Education';*/
        lstAcc.add(cusAcc);
        
        Account stpAcc=new Account(RecordTypeId=accRT.Id,name='stpAccount',Type='Distributor',Industry='Education',RASP_Status__c='Authorized',
                                    Partner_Level__c='Distributor',Phone='4154154156',Authorizations_Specializations__c='RASP;RVSP',accountnumber = '123');
    /*    stpAcc.RecordTypeId=accRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Partner_Level__c='Distributor';
        stpAcc.Phone='4154154156';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';*/
        lstAcc.add(stpAcc);
        
        Account tier2Acc=new Account(RecordTypeId=accRT.Id,name='tier2Account',Type='VAR',Industry='Education',RASP_Status__c='Authorized',
                                    Partner_Level__c='Diamond',Phone='4154154156',Authorizations_Specializations__c='RASP',accountnumber = '123');
       /* tier2Acc.RecordTypeId=accRT.Id;
        tier2Acc.name='tier2Account';
        tier2Acc.Type='VAR';
        tier2Acc.Industry='Education';
        tier2Acc.RASP_Status__c='Authorized';
        tier2Acc.Partner_Level__c='Diamond';
        tier2Acc.Phone='4154154156';
        tier2Acc.Authorizations_Specializations__c='RASP';*/
        lstAcc.add(tier2Acc);
        
        Account tier3Acc=new Account(RecordTypeId=accRT.Id,name='tier3Account',Type='VAR',Industry='Education',RASP_Status__c='Not Reviewed',accountnumber = '123');
        /*tier3Acc.RecordTypeId=accRT.Id;
        tier3Acc.name='tier3Account';
        tier3Acc.Type='VAR';
        tier3Acc.Industry='Education';
        tier3Acc.RASP_Status__c='Not Reviewed';*/
        lstAcc.add(tier3Acc);
        
        Account stpAcc1=new Account(RecordTypeId=accRT.Id,name='stp1Account',Type='Distributor',Industry='Education',RASP_Status__c='Authorized',
                                    Partner_Level__c='Distributor',Phone='4154154156',Authorizations_Specializations__c='RASP',accountnumber = '123');
    /*    stpAcc1.RecordTypeId=accRT.Id;
        stpAcc1.name='stp1Account';
        stpAcc1.Type='Distributor';
        stpAcc1.Industry='Education';
        stpAcc1.RASP_Status__c='Authorized';
        stpAcc1.Partner_Level__c='Distributor';
        stpAcc1.Phone='4154154156';
        stpAcc1.Authorizations_Specializations__c='RASP';*/
        lstAcc.add(stpAcc1);
        
        Account tier4Acc=new Account(RecordTypeId=accRT.Id,name='tier5Account',Type='VAR',Industry='Education',Partner_Level__c='Diamond',
                                       Phone='4154154156', RASP_Status__c='RASP',accountnumber = '123');
      /*  tier4Acc.RecordTypeId=accRT.Id;
        tier4Acc.name='tier5Account';
        tier4Acc.Type='VAR';
        tier4Acc.Industry='Education';
        tier4Acc.Partner_Level__c='Diamond';
        tier4Acc.Phone='4154154156';
        tier4Acc.RASP_Status__c='RASP';*/
        lstAcc.add(tier4Acc);
        
        Account influAcc=new Account(RecordTypeId=accRT.Id,name='stpAccount',Type='VAR',Industry='Education',RASP_Status__c='Not Reviewed',accountnumber = '123');
      /*  influAcc.RecordTypeId=accRT.Id;
        influAcc.name='stpAccount';
        influAcc.Type='VAR';
        influAcc.Industry='Education';
        influAcc.RASP_Status__c='Not Reviewed';*/
        lstAcc.add(influAcc);
        
        insert lstAcc;
        
        User primaryISR=[select id,name,DefaultCurrencyIsoCode from User where isActive=true and UserType='Standard' limit 1];
        User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where isActive=true and UserType='PowerPartner' limit 2];
        
        testOpp.AccountId=cusAcc.Id;
        testOpp.Channel__c='Distributor';
        testOpp.Sold_To_Partner__c=stpAcc.Id;
        testOpp.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp.Tier2__c=tier2Acc.Id;
        testOpp.Tier2_Partner_User__c=powerPartner[1].Id;
        testOpp.Tier3_Partner__c=tier3Acc.Id;
        testOpp.Influence_Partner__c=influAcc.Id;
        testOpp.Ordering_Center__c=tier4Acc.Id;
        testOpp.Primary_ISR__c=primaryISR.Id;
        testOpp.Name='TestOpportunity';
        testOpp.Type='New';
        testOpp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp.CloseDate=System.today();
        testOpp.StageName='0-Prospect';
        testOpp.Forecast_Category__c='PipeLine';
        testOpp.Competitors__c='cisco';
        testOpp.LeadSource='Email';
        //testOpp.Support_Provider__c=stpAcc.Id;
        //testOpp.Support_Provided__c='Distributor';
        //try{
            insert testOpp;
        //}catch(Exception e){
        //  system.debug('Insert Exception:'+e);
        //}
        
        DiscountApproval.isTest = true;

        testOpp.Sold_To_Partner__c=stpAcc1.Id;
        update testOpp;        
        
        DiscountApproval.isTest = true;
        DiscountApproval.run = false; //added by Ankita 1/27/2012

        testOpp1.AccountId=cusAcc.Id;
        testOpp1.Channel__c='Reseler';
        testOpp1.Sold_To_Partner__c=stpAcc.Id;
        testOpp1.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp1.Tier2__c=tier2Acc.Id;
        testOpp1.Tier2_Partner_User__c=powerPartner[1].Id;
        testOpp1.Primary_ISR__c=primaryISR.Id;
        testOpp1.Name='TestOpportunity1';
        testOpp1.Type='New';
        testOpp1.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp1.CloseDate=System.today();
        testOpp1.StageName='0-Prospect';
        testOpp1.Forecast_Category__c='PipeLine';
        testOpp1.Competitors__c='cisco';
        testOpp1.LeadSource='Email';
        //testOpp1.Support_Provider__c=tier2Acc.Id;
        //testOpp1.Support_Provided__c='Reseller';
        insert testOpp1;
        
        DiscountApproval.isTest = true;

        testOpp1.Sold_To_Partner__c=tier4Acc.Id;
        update testOpp1;
        
        DiscountApproval.isTest = true;
        DiscountApproval.run = false; //added by Ankita 1/27/2012

        testOpp2.AccountId=cusAcc.Id;
        testOpp2.Channel__c='Direct';
        testOpp2.Sold_To_Partner__c=stpAcc.Id;
        testOpp2.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp2.Tier2__c=tier2Acc.Id;
        testOpp2.Tier2_Partner_User__c=powerPartner[1].Id;
        testOpp2.Primary_ISR__c=primaryISR.Id;
        testOpp2.Name='TestOpportunity1';
        testOpp2.Type='New';
        testOpp2.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp2.CloseDate=System.today();
        testOpp2.StageName='0-Prospect';
        testOpp2.Forecast_Category__c='PipeLine';
        testOpp2.Competitors__c='cisco';
        testOpp2.LeadSource='Email';
        //testOpp2.Support_Provider__c=tier2Acc.Id;
        //testOpp2.Support_Provided__c='Distributor';
        insert testOpp2;
        
        testOpp2.Sold_To_Partner__c=null;
        testOpp2.Sold_To_Partner_User__c=null;
        testOpp2.Tier2__c=null;
        testOpp2.Tier2_Partner_User__c=null;
        try{
            DiscountApproval.isTest = true;

            update testOpp2;
        }catch(Exception e){
            
        }

        DiscountApproval.run = false; //added by Ankita 1/27/2012

        testOpp3.AccountId=cusAcc.Id;
        testOpp3.Channel__c='Distributor';
        testOpp3.Sold_To_Partner__c=stpAcc.Id;
        testOpp3.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp3.Tier2__c=tier2Acc.Id;
        testOpp3.Tier2_Partner_User__c=powerPartner[1].Id;
        testOpp3.Tier3_Partner__c=tier3Acc.Id;
        testOpp3.Influence_Partner__c=influAcc.Id;
        testOpp3.Ordering_Center__c=tier4Acc.Id;
        testOpp3.Primary_ISR__c=primaryISR.Id;
        testOpp3.Name='TestOpportunity';
        testOpp3.Type='New';
        testOpp3.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp3.CloseDate=System.today();
        testOpp3.StageName='0-Prospect';
        testOpp3.Forecast_Category__c='PipeLine';
        testOpp3.Competitors__c='cisco';
        testOpp3.LeadSource='Email';
        insert testOpp3;
    }
}