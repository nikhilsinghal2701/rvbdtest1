/*  Class used to get disti uplift percentages based on partner level of reseller and the disti
    Author: Ankita
    Date: 1/29/2010
*/
public class UpliftPercentage {
    
    //static List<Disti_Uplift__c> upliftList = new List<Disti_Uplift__c> ();
    //static Map<Id,Disti_Uplift__c> distiUpliftMap = new Map<Id,Disti_Uplift__c> ();//map of Disti to its uplift
    public static Disti_Uplift__c getDistiUpliftWithGeo(String pLevel, Id disti, Boolean isRegistered, Id oppId){
            //system.debug('in side disti uplift with geo');
            Boolean stp_gfp=false,tier2_gfp=false,gfp=false,partial_gfp=false;
            Disti_Uplift__c uplift;
            List<String> lstRegion = new List<String>();
            List<Disti_Uplift__c> upliftList = new List<Disti_Uplift__c> ();
            List<Disti_Uplift__c> newList = new List<Disti_Uplift__c>();
            Map<Id,Disti_Uplift__c> distiUpliftMap = new Map<Id,Disti_Uplift__c> ();//map of Disti to its uplift
            Map<String,Id> partnerLevelDistiMap = new Map<String, Id>();//map of Partner Level to disti
            Map<String,Disti_Uplift__c> partnerLevelUpliftMap = new Map<String, Disti_Uplift__c>();//map of Partner Level to uplift
            Map<String,Disti_Uplift__c> geoUpliftMap = new Map<String, Disti_Uplift__c>();//map of geo to uplift
            Map<String,Disti_Uplift__c> regioUpliftMap = new Map<String, Disti_Uplift__c>();//map of region to uplift
            //Map<String,Disti_Uplift__c> geoRegioUpliftMap = new Map<String, Disti_Uplift__c>();//map of geo and region to uplift
            Opportunity opp = [select id,Sold_to_Partner__c,Sold_to_Partner__r.Global_Fulfillment_Partner__c,Sold_To_Partner_Parent__c,Sold_To_Partner_Parent__r.Global_Fulfillment_Partner__c,Tier2__c,Tier2__r.Global_Fulfillment_Partner__c,Tier_2_Partner_Parent__c,Tier_2_Partner_Parent__r.Global_Fulfillment_Partner__c,Sold_To_Partner_Geo__c, Sold_To_Partner_Region__c from Opportunity where id = :oppId]; //modified by psingh@riverbed.com on 08/15/2013 for GFP
            if(opp.Sold_To_Partner_Region__c!=null){
              lstRegion=getRegionList(opp.Sold_To_Partner_Region__c);
            }
            if(opp.Sold_To_Partner__r.Global_Fulfillment_Partner__c){
              stp_gfp=true;
            }else if(opp.Sold_To_Partner_Parent__r.Global_Fulfillment_Partner__c){
              stp_gfp=true;
            }
            if(opp.tier2__r.Global_Fulfillment_Partner__c){
              tier2_gfp=true;
            }else if(opp.Tier_2_Partner_Parent__r.Global_Fulfillment_Partner__c){
              tier2_gfp=true;
            }
            system.debug(LoggingLevel.Error,'***tier2_gfp:'+tier2_gfp+'::'+opp.Tier_2_Partner_Parent__r.Global_Fulfillment_Partner__c);
            system.debug(LoggingLevel.Error,'***tier2_gfp:'+opp.Tier_2_Partner_Parent__r);
            if(stp_gfp && tier2_gfp){
            	gfp=true;
            }else if(!stp_gfp &&!tier2_gfp){
            	partial_gfp=false;
            }else if(!stp_gfp||!tier2_gfp){
            	partial_gfp=true;
            }
            System.debug(LoggingLevel.INFO, 'Sold To Partner Region: ' + lstRegion + gfp); //added subquery for new discount category project - Ankita 10/14/2011            
            //assumption that partner level is not set for standard
            // below query modified by psingh@riverbed.com on 08/15/2013 for GFP
            upliftList = [Select d.PartnerLevel__c, d.Id, d.geo__c,d.region__c,d.Distributor__c, d.Active__c,(Select Uplift_of_Net__c,Value__c, Id, Category_Master__c From Disti_Uplift_Details__r ),d.DiscountSupport__c, d.DiscountSupport_3__c, d.DiscountSupport_2__c, d.DiscountSupport_1__c, d.DiscountSpare__c,d.DiscountService__c, d.DiscountProduct__c, d.DiscountDemos__c,d.DiscountOthers__c,d.Global_Fulfillment_Distributor__c,d.Global_Fulfillment_Reseller__c From Disti_Uplift__c d  where (PartnerLevel__c = :pLevel or PartnerLevel__c = 'Standard' or PartnerLevel__c = null) and (Distributor__c = null or Distributor__c = :disti)and active__c = true  and registered_Deal__c = :isRegistered and (geo__c = :opp.Sold_to_Partner_Geo__c or geo__c = null)and (region__c IN :lstRegion or region__c = null) order by PartnerLevel__c desc];
            system.debug(LoggingLevel.Error,'***upliftList:'+upliftList);
            if(upliftList.size()>0){
              for(Disti_Uplift__c d : upliftList){
                //below condition added by psingh@riverbed.com on 08/15/2013 for GFP
                //if((opp.Sold_To_Partner_Parent__r.Global_Fulfillment_Partner__c==true||opp.Sold_To_Partner__r.Global_Fulfillment_Partner__c==true)&&(opp.tier2__r.Global_Fulfillment_Partner__c==true||opp.Tier_2_Partner_Parent__r.Global_Fulfillment_Partner__c==true)&&(d.Global_Fulfillment_Distributor__c==true)&&(d.Global_Fulfillment_Reseller__c==true)){
                if(gfp==true &&(d.Global_Fulfillment_Distributor__c==true)&&(d.Global_Fulfillment_Reseller__c==true)){
					uplift=d;
					system.debug(LoggingLevel.Error, 'uplift0:'+uplift);
					if(DiscountApproval.istest==false)
					break;
                }else if(gfp!=true && stp_gfp==true &&(d.Global_Fulfillment_Distributor__c==true)&&(d.Global_Fulfillment_Reseller__c==false)){
                	uplift=d;
                  	system.debug(LoggingLevel.Error, 'uplift7:'+uplift);
                  	if(DiscountApproval.istest==false)
                  	break;
                }else if(gfp!=true && tier2_gfp==true &&(d.Global_Fulfillment_Distributor__c==false)&&(d.Global_Fulfillment_Reseller__c==true)){
                	uplift=d;
                  	system.debug(LoggingLevel.Error, 'uplift6:'+uplift);
                  	if(DiscountApproval.istest==false)
                  	break;
                }else if(partial_gfp!=true && d.Distributor__c!=NULL && d.Distributor__c==disti && gfp!=true &&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){//else if(d.Distributor__c!=Null && d.Distributor__c==disti &&(opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c==false||opp.tier2__r.Global_Fulfillment_Partner__c==false)&&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){
					uplift=d;
					system.debug(LoggingLevel.Error, 'uplift1:'+uplift);
					if(DiscountApproval.istest==false)
					break;
                }else if(partial_gfp!=true && d.PartnerLevel__c!=NULL && d.PartnerLevel__c.equals(pLevel)&& gfp!=true &&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){//d.PartnerLevel__c==opp.Sold_to_Partner_Level__c//else if(d.PartnerLevel__c!=Null && d.PartnerLevel__c.equals(pLevel)&&(opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c==false||opp.tier2__r.Global_Fulfillment_Partner__c==false)&&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){//d.PartnerLevel__c==opp.Sold_to_Partner_Level__c
					uplift=d;
					system.debug(LoggingLevel.Error, 'uplift2:'+uplift);
					if(DiscountApproval.istest==false)
					break;
                }else if(partial_gfp!=true && d.Geo__c!=NULL && d.Region__c!=NULL && d.Geo__c.equals(opp.Sold_to_Partner_Geo__c) && d.Region__c.contains(opp.Sold_to_Partner_Region__c)&& gfp!=true &&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){//else if(d.Geo__c!=Null && d.Region__c!=null && d.Geo__c.equals(opp.Sold_to_Partner_Geo__c) && d.Region__c.contains(opp.Sold_to_Partner_Region__c)&&(opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c==false||opp.tier2__r.Global_Fulfillment_Partner__c==false)&&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){
					uplift=d;
					system.debug(LoggingLevel.Error, 'uplift3:'+uplift);
					if(DiscountApproval.istest==false)
					break;
                }else if(partial_gfp!=true && d.Geo__c!=NULL && d.Region__c==NULL && d.Geo__c.equals(opp.Sold_to_Partner_Geo__c)&& gfp!=true &&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){//else if(d.Geo__c!=Null && d.Region__c==null && d.Geo__c.equals(opp.Sold_to_Partner_Geo__c)&&(opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c==false||opp.tier2__r.Global_Fulfillment_Partner__c==false)&&(d.Global_Fulfillment_Distributor__c==false||d.Global_Fulfillment_Reseller__c==false)){
					uplift=d;
					system.debug(LoggingLevel.Error, 'uplift4:'+uplift);
					if(DiscountApproval.istest==false)
					break;
                }
              }
            }           
           System.debug('uplift+++++++++++++++++++++++'+uplift);
           return uplift;
      }     
      
  private static List<String> getRegionList(String oppRegion){
    List<String> lstRegion=new List<String>();
    if(oppRegion.contains(';')){
      lstRegion=oppRegion.split(';');
    }else lstRegion.add(oppRegion);
    return lstRegion;
  }

    
    public class UpliftException extends Exception{
        
    }
    
}