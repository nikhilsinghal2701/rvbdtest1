//*******************************************************************************************
// Class Name: AccountPlanController
//
// Description: This class is the controller class for the Account Plan VF page
//
// Created By: Prashant Singh - 05th Sep, 2011
//*******************************************************************************************

public without sharing class AccountPlanController
{
    //Setting Account and AP objects
    public Account_Plan__c ap = null;
    Account apAccount = new Account();
    Id aId,id,cId;
    //Boolean properties
    //public String activeTab {get; set;} 
    Boolean showError = false;
    public Boolean show { get; set;}
    public Boolean show1 { get; set;}
    public String mode { get; set; }
    public String errorMsg{get;set;}
    public String gapLink{get;set;}
    public String worldMapLink{get;set;}
    public String opptyPipelineReportLink{get;set;}
    public List<SelectOption> statusList=new List<SelectOption>();
    public String status{get;set;}
    private PageReference retPage;
    public String[] contacts = new String[]{};
    List<Account_Plan__c> apList=new List<Account_Plan__c>();
    public List<Contact> keyContactList {get; private set;}
    Map<Id,GAP_Team__c> gapTeamMap = new Map<Id,GAP_Team__c>();
    private GAPPortalSettings__c gapProperty;
    //Constructor Definition
    public AccountPlanController(ApexPages.StandardController controller){
    	gapProperty=GAPPortalSettings__c.getValues('GAPPortalControl');
        aId = ApexPages.currentPage().getParameters().get('aId');
        cId = ApexPages.currentPage().getParameters().get('cId');
        mode = ApexPages.currentPage().getParameters().get('mode');
        //activeTab = ApexPages.currentPage().getParameters().get('tabFocus'); 
        this.ap = (Account_Plan__c)controller.getRecord();
        if(mode=='edit'){
            show=true;
            show1=false;
        }else if(mode=='save'){
            show=true;
            show1=false;
        }
        worldMapLink='/apex/GAPWorldMap?id='+cId;
        opptyPipelineReportLink='/'+gapProperty.OpportunityPipelineReport__c;
        keyContactList = [select Id, Name,title, email, phone from contact where AccountId=:aId and Key_Contact__c=true];
    }
    public AccountPlanController(){
        errorMsg=ApexPages.currentPage().getParameters().get('errormsg');
        gapLink='/apex/'+ApexPages.currentPage().getParameters().get('id');
    }
    
    //Getter method to return Account Plan
    public Account_Plan__c getAP(){
        return ap;
    }

    //Setter method to save Account Plan
    public void setAP(Account_Plan__c accPlan){
        ap = accPlan;
    }

    //Getter to return the Account object
    public Account getAPAccount(){ return apAccount; }


    //Getter method to return boolean variable to show errors on page
    public boolean getError(){
        return showError;
    }
   
    public Pagereference firstSaveAP(){
    	Boolean allow=allowAccess();  	
    	if(allow==false){
    		errorMsg='Welcome to the Global Accounts Program (GAP) virtual account plan.'+
                            'Access to the account plans are restricted to team members of the specific GAP account and controlled by the Global Account Manager (GAM).'+
                            'If you would like to join the team for this GAP account, please reach out to the GAM.';
            gapLink='GAPList';
            retPage = new PageReference('/apex/accountPlanErrorPage?Id=GAPList'+'&errormsg='+errorMsg);        
            return retPage;
    	}else if(aId!=null && allow==true){
            apAccount = [Select name,OwnerId,status__c from Account where Id =:aId];
            List<Account_Plan__c>apList=[Select Name,AccountId__c,AccountId__r.Amount_Closed_Opportunities__c,Co_term_ability__c,Critical_Contact__c,Customer_Preference__c,Describe_customers_buying_process__c,Executive_Summary__c,Global_Strategy__c,
                  Purchasing_Decision_Making__c,Situation_Summary__c,Uniform_RVBD_Message__c,CreatedById,CreatedDate,LastModifiedById,LastModifiedDate,Rvbd_Support__c,AccountId__r.Critical_Account__c,AccountId__r.status__c,Partner_Strategy__c
                  ,Total_Number_of_customer_Sites__c,Global_Discount_Level__c,Partner_Preferences__c,Partner_Eco_System__c from Account_Plan__c where AccountId__c =: aId];
            if(apList.size()>0 && mode == 'edit'){
                this.ap = apList[0];
                show=true;
                show1=false;
            }else if(apList.size()>0 && mode == 'save'){
                this.ap = apList[0];
                show=false;
                show1=true;
            }else{
                ap.Name = apAccount.Name + ' - Account Plan';
                ap.AccountId__c=aId;
                upsert ap;
                this.ap = ap;
                show=true;
                show1=false;
                ap = [Select Name,AccountId__c,AccountId__r.Amount_Closed_Opportunities__c,Co_term_ability__c,Critical_Contact__c,Customer_Preference__c,Describe_customers_buying_process__c,Executive_Summary__c,Global_Strategy__c,
                      Purchasing_Decision_Making__c,Situation_Summary__c,Uniform_RVBD_Message__c,CreatedById,CreatedDate,LastModifiedById,LastModifiedDate,Rvbd_Support__c,AccountId__r.Critical_Account__c,AccountId__r.status__c,Partner_Strategy__c
                      ,Total_Number_of_customer_Sites__c,Global_Discount_Level__c,Partner_Preferences__c,Partner_Eco_System__c from Account_Plan__c where AccountId__c =:aId];
            }  
        }else{
            errorMsg='Welcome to the Global Accounts Program (GAP) virtual account plan.'+
                        'Access to the account plans are restricted to team members of the specific GAP account and controlled by the Global Account Manager (GAM).'+
                        'If you would like to join the team for this GAP account, please reach out to the GAM.';
            gapLink='GAPList';
            retPage = new PageReference('/apex/accountPlanErrorPage?Id=GAPList'+'&errormsg='+errorMsg);        
            return retPage;
        }
        return null;
    }
    
    public PageReference saveAP(){
        if(ap.Name == '' || ap.Name == null){
            ap.Name=apAccount.Name+' - Account Plan';
            ap.AccountId__c=aId;
        }
        try{
        	upsert ap;
        }catch(Exception e){
        	system.debug('***Exception:'+e);
        }
        this.ap = ap;
        if(status!=null){
            apAccount.Status__c=status;
            update apAccount;
        }
        PageReference accPage = new PageReference ('/apex/AccountPlanPage?Id='+ap.Id+'&aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=edit');
        accPage.setRedirect(true);
        return accPage;
    }
    
    public PageReference editAP(){
        PageReference accPage = new PageReference ('/apex/AccountPlanPage?aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=save');
        accPage.setRedirect(true);
        return accPage;
    }
    
    //Method for cancel
    public pageReference cancel(){
        PageReference accPage = new PageReference ('/apex/GAPList');
        accPage.setRedirect(true);
        return accPage;
    }
    
    //Method to take back to the GAP Portal page
    public pageReference actPage(){
        //PageReference accPage = new PageReference ('/'+ap.AccountId__c);
        PageReference accPage = new PageReference ('/apex/GAPList');
        accPage.setRedirect(true);
        return accPage;
    }
    public List<SelectOption> getStatusList(){
        Schema.DescribeFieldResult fieldResult = Account.status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
        //system.debug('Picklistentry:'+'ple:::'+ple.size());
        for( Schema.PicklistEntry f : ple){
            statusList.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        return statusList;
    }    
    public void setStatusList(List<SelectOption> options){
        this.statusList=options;
    }
    
    private Boolean allowAccess(){
    	Boolean allow=false;
    	List<GAP_Team__c> gapTeam=[select Id,Name,Account_Name__c,Team_Member__c,Role__c from GAP_Team__c where Account_Name__c=:aId];
    	Map<Id,String> teamMap=new Map<Id,String>();
    	set<GAP_Team__c> gapTeamSet=new set<GAP_Team__c>();
    	for(GAP_Team__c temp:gapTeam){
    		gapTeamSet.add(temp);
    	}
    	for(GAP_Team__c temp:gapTeamSet){
    		teamMap.put(temp.Team_Member__c,temp.Role__c);
    	}
    	Account acc=[select id,name,Global_Account_Manager__c,Global_Account_Manager__r.Name from Account where id=:aId limit 1];
    	if((teamMap.containsKey(userInfo.getUserId())== true)){
    		allow=true;
    	}else if(userInfo.getProfileId().substring(0, 15)==gapProperty.SysAdminProfileId__c||userInfo.getProfileId().substring(0,15)==gapProperty.LimitedAdminProfile__c){
    		allow=true;
    	}else if(userInfo.getUserName()== acc.Global_Account_Manager__r.Name){
    		allow=true;
    	}
    	return allow;
    }    
}