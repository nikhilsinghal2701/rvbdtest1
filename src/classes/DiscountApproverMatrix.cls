/**
 * Provides functionality to display approvers matrix for a quote that is
 * applicable to a given Quote. From this matrix, the approval flow can be
 * determined.
 * 
 * Author: smohapatra
 // Code added by Santoshi on Nov-15-11 to calculate Approval Routings dynamically. 
 Rev1: modification to add deal desk for the approval process.
 Modified By: Jaya
 modified Date: 10/3/2014
 Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014
 */
public without sharing class DiscountApproverMatrix {
    public static final String REGIONAL_DIRECTOR = 'Reg Director';
    public static final String GEO_VP = 'Geo VP';
    public static final String CFO = 'CFO';
    public static final String CEO = 'CEO';
    public static final String WW_SALES_VP ='WW Sales VP';
    public static final String RSM = 'RSM';
    public static final String RVP = 'RVP';//Added for RVP requirement change by prashant
    
    /* public static List<Approvers_Routing_Matrix__c> getApproversMatrix(Quote__c qt) {
        String query=DiscountApproverMatrixHelper.createQueryFromFields(qt.Id);
        List<Approvers_Routing_Matrix__c> lstApprovers=Database.query(query);
        return lstApprovers;
    } */
    
    public static List<Approvers_Routing_Matrix__c> createApproversMatrixForQuote(Quote__c quote, Map<String,UserRoleObj> hierarchyByRoleName, Map<String,Decimal> discountsByCategory, Map<String,List<String>> quoteProductFamiliesList,Map<String,Map<String,String>> overlayRoleToOracleCategoryMap) {        
        MultiOrgSettings__c moSettings = MultiOrgSettings__c.getValues('Default');
        String defaultSegment = moSettings.Segment_Name__c;
        System.debug('default segment = ' + defaultSegment);
        Quote__c q = [select id, Opportunity__r.Federal_Deal__c from Quote__c where id = :quote.Id];
        Boolean isFederal = q.Opportunity__r.Federal_Deal__c == null ? false : q.Opportunity__r.Federal_Deal__c;
        String segment = quote.Segment__c == null? '' : quote.Segment__c;
        BaseMatrix baseMatrix = new BaseMatrix(segment, isFederal, defaultSegment);
        ApprovalChain approvalChain = baseMatrix.createDefaultApprovalChains(quote, discountsByCategory, quoteProductFamiliesList,overlayRoleToOracleCategoryMap);
        hierarchyByRoleName = standardizeHierarchyRoles(hierarchyByRoleName);
          
        // get exception roles and users add into otherusers roles
        // Rohit - 02242010 - Added the RSM to the method call
        //Map<String,UserRoleObj> mapException = DiscountApproverMatrixHelper.getExceptionMapping(quote.RSM__c);
        //changed by ankita for multi org
        Map<String,UserRoleObj> mapException = DiscountApproverMatrixHelper.getExceptionMapping(quote, isFederal, defaultSegment);
        for (UserRoleObj uro : mapException.values()) {
            hierarchyByRoleName.put(uro.RoleName, uro);
        }
        System.debug(LoggingLevel.INFO, 'exceptionnnnnnnn--------'+hierarchyByRoleName);
        approvalChain.assignUsers(hierarchyByRoleName);
        
        List<Approvers_Routing_Matrix__c> existingApprovers = [SELECT Quote__c,Actual_Role__c FROM Approvers_Routing_Matrix__c WHERE Quote__c = :quote.Id Limit 20];
        if (!existingApprovers.isEmpty()) {
            System.debug('existingApprovers = ' + existingApprovers);
            delete existingApprovers;
            
        }
        insert approvalChain.getApprovers();
        System.debug('approvalChain = ' + approvalChain.getApprovers());
        return approvalChain.getApprovers();
    }
    
    private static Map<String,UserRoleObj> standardizeHierarchyRoles(Map<String,UserRoleObj> hierarchyByRoleName) {
        Map<String,UserRoleObj> result = new Map<String,UserRoleObj>();
        for (UserRoleObj uro : hierarchyByRoleName.values()) {
            System.debug('URO = ' + uro);
            if (uro.RoleName.contains(RSM)) {
                result.put(RSM,uro);
            } else if (uro.RoleName.contains('Manager')) {
                result.put(REGIONAL_DIRECTOR, uro);
            } else if (uro.RoleName.contains('RVP')) { //Added for RVP requirement change by prashant
                result.put(RVP,uro);
            }else if (uro.RoleName.contains('Sales VP') && (uro.RoleName.indexOf('WW Sales') <= 0) ) {
                result.put(GEO_VP,uro);
            } else if (uro.RoleName.contains('WW Sales')) {
                result.put(WW_SALES_VP,uro);
            } else if (uro.RoleName.contains(CEO)) {
                result.put(CEO,uro);
            } else if (uro.RoleName.contains(CFO)) {
                result.put(CFO,uro);
            }
        }
        
        return result;
    }
    
    private class BaseMatrix {
        private List<Base_Approval_Matrix__c> baseMatrices;
        private Map<String,Decimal> levelsByRoleName;
        
        private BaseMatrix(String segment, Boolean isFederal, String dSegment) {
            if(isFederal || segment.equalsIgnoreCase(dSegment)){
                System.debug('Federal = '+ isFederal + 'Segment = ' + segment );
            //  baseMatrices = [SELECT BM_Role__c, BM_Start_Pct__c, BM_Region__c, BM_Order_Type__c, BM_Level__c, BM_Geo__c, BM_End_Pct__c, BM_Cat_A__c, BM_Cat_B__c, BM_Cat_C__c, BM_Cat_D__c, BM_Cat_E__c, BM_Cat_F__c, BM_Active__c FROM Base_Approval_Matrix__c WHERE BM_Active__c=true and Segment__c = null ORDER BY BM_Level__c ASC];
                baseMatrices = [SELECT BM_Role__c, BM_Start_Pct__c, BM_Region__c, BM_Order_Type__c, BM_Level__c, BM_Geo__c, BM_End_Pct__c,BM_Active__c,(select Category_Master__c,Category_Master__r.Name,Active__c,End__c,Start__c from Base_Approval_Matrix_Details__r where Active__c=true ) FROM Base_Approval_Matrix__c WHERE BM_Active__c=true and Segment__c = null ORDER BY BM_Level__c ASC];
            System.debug('baseMatrices = '+baseMatrices);
            }else{
                baseMatrices = [SELECT BM_Role__c, BM_Start_Pct__c, BM_Region__c, BM_Order_Type__c, BM_Level__c, BM_Geo__c, BM_End_Pct__c, BM_Active__c,(select Category_Master__c,Category_Master__r.Name,Active__c,   End__c,Start__c from Base_Approval_Matrix_Details__r where Active__c=true) FROM Base_Approval_Matrix__c WHERE BM_Active__c=true and (Segment__c = null or Segment__c = :segment) ORDER BY BM_Level__c ASC];
                System.debug('baseMatrices = '+baseMatrices);
            }
            levelsByRoleName = new Map<String,Decimal>();
        }
        
    /** private ApprovalChain createDefaultApprovalChain(Quote__c quote, Map<String,Decimal> discountsByCategory) {
            ApprovalChain approvalChain = new ApprovalChain(this);
            for (Base_Approval_Matrix__c baseMatrix : baseMatrices) {
                levelsByRoleName.put(baseMatrix.BM_Role__c, baseMatrix.BM_Level__c);
                for (String category : discountsByCategory.keySet() ) {
                    System.debug(LoggingLevel.INFO, 'Discount Category = ' + category);
                    Decimal discount = discountsByCategory.get(category);
                    if ( ((baseMatrix.BM_Start_Pct__c <= discount && baseMatrix.BM_End_Pct__c >= discount)  ||  baseMatrix.BM_End_Pct__c <= discount) && (baseMatrix.get('BM_Cat_' + category + '__c') == true)) {
                        Approvers_Routing_Matrix__c approver = approvalChain.getByRoleName(baseMatrix.BM_Role__c);
                        if (approver == null) {
                            System.debug(LoggingLevel.INFO, 'Add new approver');
                            approver = new Approvers_Routing_Matrix__c();
                            approver.Route__c = true;
                            approver.Reference_Role__c = baseMatrix.BM_Role__c; 
                            approver.Level__c = baseMatrix.BM_Level__c;
                            approver.Quote__c = quote.Id;
                            approvalChain.addApprover(approver);
                        }
                        if (approver.Category__c != null) {
                            if (!(approver.Category__c.contains(category))) {                        
                                approver.Category__c = category + approver.Category__c;
                            }
                            System.debug(LoggingLevel.INFO, 'Approver category = '+approver.Category__c);
                        } else {
                            System.debug(LoggingLevel.INFO, 'Assign Approver category ');
                            approver.Category__c = category;  
                        }
                    }
                }
            }
            return approvalChain;
        }*/ 
        // Code added by Santoshi on Nov-15-11 to calculate Approval Routing Matrix dynamically. 
        private ApprovalChain createDefaultApprovalChains(Quote__c quote, Map<String,Decimal> discountsByCategory,Map<String,List<String>> quoteProductFamilies,Map<String,Map<String,String>> overlayRoleToOracleCategoryMap) {//Parameters quoteProductFamiliesList,overlayRoleToOracleCategoryMap added by Rucha (6/5/2013) for product family routing
            ApprovalChain approvalChain = new ApprovalChain(this);                     
            Boolean dealDeskFlag = false;//by Anil
            for (Base_Approval_Matrix__c baseMatrix : baseMatrices) {
                levelsByRoleName.put(baseMatrix.BM_Role__c, baseMatrix.BM_Level__c);   
                   system.debug('baseMatrix.BM_Role__c: ' + baseMatrix.BM_Role__c);  
                    for (Base_Approval_Matrix_Detail__c b : baseMatrix.Base_Approval_Matrix_Details__r ) {
                        String category;
                        System.debug(LoggingLevel.INFO, 'Discount Category = ' + category);
                        Decimal discount = discountsByCategory.get(b.Category_Master__r.Name);
                        if ( ((b.Start__c <= discount && b.End__c >= discount)  ||  b.End__c <= discount) && (discountsByCategory.containsKey(b.Category_Master__r.Name))) {
                            Approvers_Routing_Matrix__c approver = approvalChain.getByRoleName(baseMatrix.BM_Role__c);
                            Boolean addOverlay = false;
                           // Boolean dealDeskFlag = false; // Rev:1 flag for deal desk check
                            system.debug('discount: ' + discount);
                            if(overlayRoleToOracleCategoryMap.containsKey(baseMatrix.BM_Role__c)){ //If role is an overlay role 
                                     if(quoteProductFamilies != null){//If oracle category on atleast one line item is present
                                        
                                        Map<String,String> oracleCategoryMap = overlayRoleToOracleCategoryMap.get(baseMatrix.BM_Role__c);
                        
                                        //Check if oracle category on quote line items match the categories corresponding to overlay roles that need routing
                                        List<String> quoteCatProducFamiliesList = quoteProductFamilies.get(b.Category_Master__r.Name);  
                                        system.debug('quoteCatProducFamiliesList: ' + baseMatrix.BM_Role__c + ' ' + b.Category_Master__r.Name + ' ' + quoteCatProducFamiliesList);                          
                                        if(quoteCatProducFamiliesList != null){//If the oracle category is applicable for this discount category
                                            Integer i = 0; 
                                            while(i< quoteCatProducFamiliesList.size() && !oracleCategoryMap.containsKey(quoteCatProducFamiliesList.get(i))){
                                                i++;
                                            }
                                            
                                            if(i< quoteCatProducFamiliesList.size()){
                                                addOverlay = true;
                                            }
                                            /*
                                            Rev:1 chek to see if the Quote is qualified for Deal desk approval.
                                            
                                            if(quote.Blanket__c || quote.No_Of_High_Risk_SKU_s__c >= 1 || quote.Non_VSOE_Rollup__c >= 1){
                                                dealDeskFlag = true;
                                            }*/
                                        }
                                     }
                            }//By Anil 10.15.2014
                            if(quote.Blanket__c==true || quote.No_Of_High_Risk_SKU_s__c >= 1 || quote.Non_VSOE_Rollup__c >= 1){
                               dealDeskFlag = true;
                               System.debug(LoggingLevel.INFO, 'dealDeskFlag'+dealDeskFlag);
                                            }
                            if (approver == null) {
                                 //Added by Rucha (6/5/2013) for product family routing                         
                                if(overlayRoleToOracleCategoryMap.containsKey(baseMatrix.BM_Role__c)){ //If role is an overlay role 
                                    if(addOverlay){
                                        System.debug(LoggingLevel.INFO, 'Add new overlay approver');
                                                approver = new Approvers_Routing_Matrix__c();
                                                approver.Route__c = true;
                                                approver.Reference_Role__c = baseMatrix.BM_Role__c; 
                                                approver.Level__c = baseMatrix.BM_Level__c;
                                                approver.Quote__c = quote.Id;
                                                approvalChain.addApprover(approver);
                                                System.debug(LoggingLevel.INFO, 'Overlay Approver roles-- = '+approver.Reference_Role__c);
                                    }                                    
                                }
                               // System.debug(LoggingLevel.INFO, 'dealDeskFlag'+dealDeskFlag);
                                
                                else if(baseMatrix.BM_Role__c.equalsIgnoreCase('Deal Desk')){
                                    if(dealDeskFlag==true){/* Rev:1 this condation is part of */
                                    System.debug(LoggingLevel.INFO, 'insidethedealDesk Loop');
                                        approver = new Approvers_Routing_Matrix__c();
                                        approver.Route__c = true;
                                        approver.Reference_Role__c = baseMatrix.BM_Role__c; 
                                        approver.Level__c = baseMatrix.BM_Level__c;
                                        approver.Quote__c = quote.Id;
                                        approvalChain.addApprover(approver);
                                        System.debug(LoggingLevel.INFO, 'DealDesk Approver roles-- = '+approver.Reference_Role__c);
                                    }
                                }
                                else{                               
                                    System.debug(LoggingLevel.INFO, 'Add new approver');
                                    approver = new Approvers_Routing_Matrix__c();
                                    approver.Route__c = true;
                                    approver.Reference_Role__c = baseMatrix.BM_Role__c; 
                                    approver.Level__c = baseMatrix.BM_Level__c;
                                    approver.Quote__c = quote.Id;
                                    approvalChain.addApprover(approver);
                                    System.debug(LoggingLevel.INFO, 'Approver roles-- = '+approver.Reference_Role__c);
                                }
                            }
                            
                            if(approver != null){
                                if (approver.Category__c != null) {
                                    if (!(approver.Category__c.contains(b.Category_Master__r.Name))) { 
                                        if(overlayRoleToOracleCategoryMap.containsKey(baseMatrix.BM_Role__c)){
                                            if(addOverlay){                                             
                                                approver.Category__c = b.Category_Master__r.Name + approver.Category__c;
                                            }
                                        }
                                        else{                     
                                            approver.Category__c = b.Category_Master__r.Name + approver.Category__c;
                                        }
                                    }
                                    System.debug(LoggingLevel.INFO, 'Approver category = '+approver.Category__c);
                                } else {
                                    System.debug(LoggingLevel.INFO, 'Assign Approver category-- '+b.Category_Master__r.Name);
                                    approver.Category__c = b.Category_Master__r.Name;
                                }
                            }//end of if(approver != null)
                        }
                    }               
            }
            return approvalChain;
        }
        
        private Decimal getLevelByRoleName(String roleName) {
            return levelsByRoleName.get(roleName);
        }
    }
    
    private class ApprovalChain {
        private Map<String,Approvers_Routing_Matrix__c> approversByRoleName;
        private BaseMatrix baseMatrix;
        
        private ApprovalChain(BaseMatrix baseMatrix) {
            approversByRoleName = new Map<String,Approvers_Routing_Matrix__c>();
            this.baseMatrix = baseMatrix;
        }
        
        private Approvers_Routing_Matrix__c getByRoleName(String roleName) {
            return approversByRoleName.get(roleName);
        }
        
        private void addApprover(Approvers_Routing_Matrix__c approver) {
            System.assert(approver.Reference_Role__c != null, 'Attempt to add quote approver with blank role: ' + approver);
            approversByRoleName.put(approver.Reference_Role__c, approver);
        }
        
        private List<Approvers_Routing_Matrix__c> getApprovers() {
            return approversByRoleName.values();
        }
        
        private void assignUsers(Map<String,UserRoleObj> hierarchyByRoleName) {
            for (Approvers_Routing_Matrix__c approver : approversByRoleName.values()) {
                UserRoleObj uro = hierarchyByRoleName.get(approver.Reference_Role__c);
                if ((uro != null) && approver.Route__c) {
                    approver.User__c = uro.UserId;
                    approver.Actual_Role__c = uro.RoleName;
                    approver.Category__c = DiscountApproverMatrixHelper.getSortedCategory(approver.Category__c);
                    System.debug(LoggingLevel.INFO, 'Approver Role and category = ' + approver.Actual_Role__c + ' : ' + approver.Category__c);
                } else if ((uro == null) && approver.Route__c) {
                    // There is nobody in this role in RSM's hierarchy
                    // Inactivate the approver for this role by setting Route to false
                    approver.Category__c = DiscountApproverMatrixHelper.getSortedCategory(approver.Category__c);
                    approver.Route__c = false;
                    System.debug(LoggingLevel.INFO,'No approver in this role with category = ' + approver.Category__c);

                    // ... and create an approver for next role up (if doesn't aready exist)
                    
                    System.debug(LoggingLevel.INFO,'aprover----------- = ' + approver.Reference_Role__c);
                    String nextRole = getNextRole(approver.Reference_Role__c);
                    System.debug(LoggingLevel.INFO,'next---------- = ' + nextRole);
                    System.debug(LoggingLevel.INFO,'rolename---------- = ' + nextRole);
                    System.debug(LoggingLevel.INFO, 'Next Role = ' + approversByRoleName);
                    if ((nextRole != null) && !approversByRoleName.containsKey(nextRole)) {
                        System.debug(LoggingLevel.INFO, 'Skipping approver role 1');
                        Approvers_Routing_Matrix__c nextApprover = new Approvers_Routing_Matrix__c(Quote__c=approver.Quote__c);
                        UserRoleObj nextUro = hierarchyByRoleName.get(nextRole);
                            System.debug(LoggingLevel.INFO, 'adding next approver role');
                            nextApprover.Reference_Role__c = nextRole;
                            nextApprover.Category__c = approver.Category__c;
                            nextApprover.Level__c = baseMatrix.getLevelByRoleName(nextRole);
                        if (nextUro != null) {
                            nextApprover.User__c = nextUro.UserId;
                            nextApprover.Actual_Role__c = nextUro.RoleName;
                            nextApprover.Route__c = (nextApprover.Level__c != null);
                        }
                        approversByRoleName.put(nextRole, nextApprover);
                        if(nextUro == null){ //added by Ankita 3/24 for skipping second level if does not exist
                            String next2nextRole = getNextRole(nextRole);
                            System.debug(LoggingLevel.INFO, 'Next2Next Role = ' + next2nextRole);
                            if ((next2nextRole != null) && !approversByRoleName.containsKey(next2nextRole)) {
                                System.debug(LoggingLevel.INFO, 'Skipping approver role 2');
                                Approvers_Routing_Matrix__c next2nextApprover = new Approvers_Routing_Matrix__c(Quote__c=approver.Quote__c);
                                UserRoleObj next2NextUro = hierarchyByRoleName.get(next2nextRole);
                                if (next2NextUro != null) {
                                    next2nextApprover.Reference_Role__c = next2nextRole;
                                    next2nextApprover.Category__c = approver.Category__c;
                                    next2nextApprover.Level__c = baseMatrix.getLevelByRoleName(next2nextRole);
                                    next2nextApprover.Route__c = (next2nextApprover.Level__c != null);
                                    next2nextApprover.User__c = next2NextUro.UserId;
                                    next2nextApprover.Actual_Role__c = next2NextUro.RoleName;
                                    approversByRoleName.put(next2nextRole, next2nextApprover);
                                }
                            }else{
                                System.debug(LoggingLevel.INFO, 'adding category to next role');
                                if(approversByRoleName.get(next2nextRole)!=null)
                                approversByRoleName.get(next2nextRole).category__c = DiscountApproverMatrixHelper.getSortedCategory(approversByRoleName.get(next2nextRole).category__c + ',' + approver.category__c);
                                
                            }
                        }//end added by Ankita
                    }
                }
            }
        }
        
        private String getNextRole(String role) {
            if (role.equalsIgnoreCase(REGIONAL_DIRECTOR)) {
                //return GEO_VP; //replaced GEO_VP by RVP for RVP requirement change by prashant
                return RVP;
            } else if (role.equalsIgnoreCase(RVP)) {//Added for RVP requirement change by prashant
                return GEO_VP;
            } else if (role.equalsIgnoreCase(GEO_VP)) { 
                return WW_SALES_VP; 
            } else if (role.equalsIgnoreCase(WW_SALES_VP)) {
                return CFO;
            } else if (role.equalsIgnoreCase(CFO)) {
                return CEO;
            }
            return null;
        }
    }
}