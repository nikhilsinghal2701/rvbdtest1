global class DiscountApprovalWS {
    
    /*
}
webService static String submit(Id qId, Id oId) { 

    
    DateTime dt = null;
    Id lastModId = null;
    Quote__c contextQuote = null;
    for (Quote__c q:[Select  q.LastModifiedDate, 
                        q.Id, 
                        q.Discount_Status__c,
                        q.RSM__c
                        From Quote__c q 
                        where q.Opportunity__c = : oId]){
                            
        if (dt == null || dt < q.LastModifiedDate){
            dt = q.LastModifiedDate;
            lastModId = q.Id;
        }
        
        if (qId == q.Id){
            contextQuote = q;   
        }
                        
    }
    
    if (qId != lastModId){
        return 'You may only submit the last modified Quote on an Opportunity.';    
    }  
    
    if (contextQuote.RSM__c == null){
        return 'You must select an RSM before submitting.'; 
    }
    
    if (contextQuote.Discount_Status__c == DiscountApprovalRouting.NOT_REQUIRED
        || contextQuote.Discount_Status__c == DiscountApprovalRouting.DRAFT){
        contextQuote.Discount_Status__c = DiscountApprovalRouting.APPROVED;
        update contextQuote;
    } else{
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setObjectId(contextQuote.id);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req);
        
        // Verify the result
        System.assert(result.isSuccess());  
    }
    return null;
}
*/

webService static void refresh(List<Id> oIds) { //ankita:2/4/2010 for making the method handle bulk modified the argument to a list 
    List<Opportunity> oppList = new List<Opportunity>();
    for(Id id : oIds){
        Opportunity opp = New Opportunity(id = id, NSD_Calc__c  = true);
        oppList.add(opp);
    }   
        update oppList;
}
//new method to move the validations inside the method so it can be called from powerstrip Ankita 2/11/2010
webService static String[] refreshDiscounts(List<Id> oIds) { //ankita:2/11/2010 for making the method handle bulk modified the argument to a list 
    List<Opportunity> oppList = new List<Opportunity>();
    String[] errorMsg = new List<String>();
    Boolean error = false;
    List<Opportunity> opptys = [select id, stagename, Support_Provider__c, Channel__c, Sold_to_Partner__c,name,
                                Tier2__c, Discount_Schedule__c
                                from Opportunity where id in :oIds];
    Set<Id> accIds = new Set<Id>{'0018000000amn5z', '0018000000amn69','0017000000PtO9E'};                           
    for(Opportunity o: opptys){
        if(o.Channel__c != null){
            if(accIds.contains(o.Tier2__c)){
                error = true;
                errorMsg.add('Please enter an actual Tier 2 Partner or remove before refreshing the discounts for Opportunity '+o.name) ;
            }else if(accIds.contains(o.Sold_to_Partner__c)){
                error = true;
                errorMsg.add('Please enter an actual Sold to Partner before refreshing the discounts for Opportunity '+o.name);
            }else if(o.Channel__c.equalsIgnoreCase('Distributor') && (o.Tier2__c == null || o.Sold_to_Partner__c == null)){
                error = true;
                errorMsg.add('Please enter Tier 2 Partner and Sold to Partner for Opportunity '+o.name + '\n');
            }
        } 
        if(o.Support_Provider__c == null ){//|| o.Support_Provider__c == ''
            error = true;
            errorMsg.add('Please Select and Save support provider for Opportunity ' + o.name) ;
        }
        if(o.stagename.equalsIgnorecase('6 - Order Accepted') || o.stagename.equalsIgnorecase('7 - Closed (Not Won)')){
            error = true;
            errorMsg.add(System.Label.Configure_Error_Msg);
        }
        if(error){
            return errorMsg;
        }else{
            Opportunity opp = New Opportunity(id = o.id, NSD_Calc__c  = true);
            oppList.add(opp);
        }
    }   
    try{
        update oppList;
        errorMsg.add('Success');
    }catch(DMLException e){
        System.debug('Exception in refreshing discounts '+ e.getDMLMessage(0));
        error = true;
        errorMsg.add(e.getDMLMessage(0));
    }
        return errorMsg;
}

webservice static void run(){
    List<Opportunity> oppList = [Select id, NSD_Calc__c from Opportunity 
                                where Sold_To_Partner__r.Name = 'Arrow Enterprise Computing Systems' 
                                and isClosed = false];
    List<Id> oppIds = new List<Id>();
    for(Opportunity opp : oppList){
        oppIds.add(opp.Id);
    }
    refresh(oppIds);
} 

static testMethod void unitTest() {
    //Rohit - 01082010
    DiscountApproval.isTest = true;
    Opportunity op =DiscountApprovalTestHlp.createOpp();
    //Quote__c q = DiscountApprovalTestHlp.createQuote(op.Id);
    //refresh(new List<Id>{q.Opportunity__c});
    refreshDiscounts(new List<Id>{op.Id});
    
}

static testMethod void unitTest1() {
    //Rohit - 01082010
    DiscountApproval.isTest = true;
    Opportunity op =DiscountApprovalTestHlp.createOpp();
    //Quote__c q = DiscountApprovalTestHlp.createQuote(op.Id);
    refresh(new List<Id>{op.Id});
    //refreshDiscounts(new List<Id>{op.Id});
    
}
}