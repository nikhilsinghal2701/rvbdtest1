public class INQ_EmailController {
    private String myCaseId;
    private Case myCase;
    private Folder myFolder;
    private EmailTemplate emailTemplate;
    String fromAddress;
    String fromName;
    String toAddress;
    String toName;
    String ccAddressString;
    String bccAddressString;
    String listNotReady;
    List<EmailTemplate> emailTemplates;
    String selectedTemplate;
    String emailMessage;
    String emailError;
    
    Boolean isEditing;
    
    String changeArticleId;
    Boolean changeArticleFlag;
    
    Boolean isEmailSent;
    private List<String> ccAddresses;
    private List<String> bccAddresses;
    private Map<String,String> fromNameMap;
    private Contact myContact;
    private Contact toContact;
    
    List<InQuira_Case_Info__c> relatedArticles;
    
    public INQ_EmailController(Case c) {
        init(c, c.Id);
    }
    
    public INQ_EmailController(ApexPages.StandardController stdController) {
        String caseId = ((Case)stdController.getRecord()).Id;
        Case c = [SELECT Id,ContactId,Subject,Article_Email_Message__c FROM Case WHERE Id=:caseId LIMIT 1];
        init(c, caseId);
    }
    
    private void init(Case c, String caseId) {
        this.myCaseId = caseId;
        this.myCase = c;
        this.myFolder = [SELECT Id,Name FROM Folder WHERE DeveloperName='INQ_Linked_Article_Templates' LIMIT 1];
        emailTemplates = [SELECT Name,Subject,Body FROM EmailTemplate WHERE FolderId=:this.myFolder.Id AND TemplateType='text'];
        emailTemplate = [SELECT Id From EmailTemplate WHERE DeveloperNAme = 'Emailed_Articles_Container' LIMIT 1];
        fromName = UserInfo.getUserName();
        ccAddresses = new List<String>();
        bccAddresses = new List<String>();
        relatedArticles = null;
        getRelatedArticles();
        listNotReady = 'true';
        selectedTemplate = null;
        myContact = null;
        emailError = null;
        isEmailSent = false;
        String myContactId = myCase.contactid;
        if (myContactId!=null) {
            List<Contact> contacts = [SELECT Id,Email,Name FROM Contact where Id=:myContactId];
            myContact = contacts.size()>0 ? contacts.get(0) : null;
        }
        toContact = myContact;
        toAddress = toContact!=null ? toContact.email : '';
        toName = toContact==null ? '[[UNKNOWN RECIPIENT]]' : toContact.Name;
        emailMessage = '';
        isEditing = false;
    }
    
    private void retrieveToContact(String contactemail)  {
        //check if we have to look for or create a new contact
        if (myContact == null || myContact.Email != contactemail) {
            //Look for the contact
            List<Contact> theNewContact = [SELECT Id, Email,Name FROM Contact WHERE Email =: contactemail LIMIT 1];
            if (theNewContact == null || theNewContact.isEmpty()) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, '"To" Addressee must be a Salesforce Contact'));
                toContact = null;
            } else {
                system.debug('@debug returning pre-existing contact:  id==' + theNewContact[0].Id);
                toContact = theNewContact[0];
            }
        } else {
            system.debug('@debug returning case contact:  id==' + myContact.Id);
            toContact = myContact;
        }
    }
    
    public Boolean getIsEditing() {
        return isEditing;
    }
    
    public void setIsEditing(Boolean flag) {
        isEditing = flag;
    }
    
    public String getMyCaseId() {
        return myCaseId;
    }
    
    public Boolean getIsEmailSent() {
        return isEmailSent;
    }
    
    public void setChangeArticleId(String s) {
        changeArticleId = s;
    }
    
    public String getChangeArticleId() {
        return changeArticleId;
    }
    
    public void setChangeArticleFlag(boolean f) {
        changeArticleFlag = f;
    }
    
    public boolean getChangeArticleFlag() {
        return changeArticleFlag;
    }
    
    public void changeArticle() {
        boolean isgood = false;
        for (Inquira_Case_Info__c article : relatedArticles) {
            if (article.Id==changeArticleId) {
                article.Email_Article__c = changeArticleFlag;
                isgood = true;
            }
        }
        if (!isgood) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Error updating article info ('+changeArticleId+')')); 
        } else {
            update relatedArticles;
            listNotReady='false';
        }
    }
    
    public List<SelectOption> getFromOptions() {
        List<SelectOption> options = new List<SelectOption>();
        String userid = UserInfo.getUserId();
        fromNameMap = new Map<String,String>();
        User user = [SELECT Name,Email FROM User WHERE id=:userid LIMIT 1];
        List<OrgWideEmailAddress> OrgWideUsers = [SELECT Id,DisplayName,Address 
                                                    FROM OrgWideEmailAddress 
                                                   WHERE DisplayName LIKE '%Support%'];
        options.add(new SelectOption('',user.Name + ' <' + user.Email + '>'));
        fromNameMap.put('',user.Name);
        for (OrgWideEmailAddress owea : OrgWideUsers) {
            options.add(new SelectOption(owea.Id, owea.DisplayName + ' <' + owea.Address + '>'));
            fromNameMap.put(owea.Id,owea.DisplayName);
        }
        return options;
    }

    public String getListNotReady() {
        return listNotReady;
    }
    
    public void setListNotReady(String s) {
        listNotReady = s;
    }

    public void setToAddress(String s) {
        retrieveToContact(s);
        toAddress = toContact==null ? '' : toContact.email;
        toName = toContact==null ? '[[UNKNOWN RECIPIENT]]' : toContact.Name;
    }
    
    public String getToAddress() {
        return toAddress;
    }
    
    public String getToName() {
        return toName;
    }
    
    public void jsNoop() {
        // empty method to hook into action functions that drive setters
    }
    
    public void setFromAddress(String s) {
        fromAddress = s;
        fromName = fromNameMap.get(s);
    }
    
    public String getFromAddress() {
        return fromAddress;
    }
    
    public String getFromName() {
        return fromName;
    }
    
   
    public void setCCAddressString(String s) {
        ccAddresses.clear();
        ccAddresses.addAll(s.split(',\\s*|;\\s*'));
        ccAddressString = '';
        for (String addr : ccAddresses) {
            ccAddressString += (ccAddressString.equals('') ? '' : '; ') + addr;
        }
    }
    
    public String getCCAddressString() {
        return ccAddressString;
    }
    
    public void setBCCAddressString(String s) {
        bccAddresses.clear();
        bccAddresses.addAll(s.split(',\\s*|;\\s*'));
        bccAddressString = '';
        for (String addr : bccAddresses) {
            bccAddressString += (bccAddressString.equals('') ? '' : '; ') + addr;
        }
    }
    
    public String getBCCAddressString() {
        return bccAddressString;
    }
    
    public List<InQuira_Case_Info__c> getRelatedArticles() {
        if (relatedArticles==null) {
            relatedArticles = [SELECT Id, Email_Article__c, 
                                      Related_InQuira_Article__r.Title__c,
                                      Related_InQuira_Article__r.Display_URL__c,
                                      Related_InQuira_Article__r.Document_Id__c 
                                 FROM InQuira_Case_Info__c 
                                WHERE Related_Case__c=:this.myCaseId
                                      AND 
                                      Related_InQuira_Article__r.Document_ID__c != NULL
                                      AND
                                      Related_InQuira_Article__r.Document_ID__c != ''
                                      AND
                                      (NOT Related_InQuira_Article__r.Display_URL__c LIKE '%id=S:%')
                                      ];
            for (InQuira_Case_Info__c r : relatedArticles) {
                r.Email_Article__c = false;
            }
        }
        return relatedArticles; 
    }
    
    public List<EmailTemplate> getEmailTemplates() {
        return emailTemplates;
    }
    
    public String getSelectedTemplate() {
        return selectedTemplate;    
    }
    
    public void setSelectedTemplate(String s) {
        selectedTemplate = s;
        system.debug('@debug selected template: ' + selectedTemplate);
    }
    
    public String getEmailMessage() {
        return emailMessage;
    }
    
    public void setEmailMessage(String s) {
        emailMessage = s;
        myCase.article_email_message__c = emailMessage;
    }
    
    public String getEmailMessageHTML() {
        return INQ_MailedArticlesController.HTMLENCODE(emailMessage);
    }
    
    public String getEmailError() {
        return emailError==null ? '' : emailError;
    }
    
    
    public void insertTemplate() {
        for (EmailTemplate et : emailTemplates) {
            if ((String)et.Id==selectedTemplate) {
                setEmailMessage(et.Body);
            }
        }
    }
    
    public void editTemplate() {
        setIsEditing(!isEditing);
    }
    
    public void sendEmail() {
        //check if toAddress is valid before doing any processing
        if (toAddress != null && !toAddress.trim().equals('')) {
            Integer relatedcount = 0;
            for (InQuira_Case_Info__c article : relatedArticles) {
                if (article.Email_Article__c) relatedcount++;
            }
            if (relatedcount>0) {
                // first save our checkboxes and stuff to the related links
                update relatedArticles;
                system.debug('@debug relatedArticles: ' + relatedArticles);
                // next save our comment to the article_email_message__c field in case
                update myCase;
                system.debug('@debug myCase: ' + myCase);
                // configure the e-mail
                system.debug('@debug toAddress: ' + toAddress);
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                system.debug('@debug email: ' + email);
                email.setSaveAsActivity(true);
                system.debug('@debug emailTemplate: ' + emailTemplate);
                if (emailTemplate != null) {
                    email.setTemplateId(emailTemplate.Id);
                }
                String mailTargetId = toContact.Id;
                if (mailTargetId==null) return;
                email.setTargetObjectId(mailTargetId);
                //email.setToAddresses(new String[] { toAddress });
                email.setWhatId(myCaseId);
    
                //check of ccAddresses is valid
                if (ccAddressString != null && !ccAddressString.trim().equals('')) {
                    system.debug('@debug ccAddresses: ' + ccAddresses);
                    email.setCCAddresses(ccAddresses);
                }
                
                //check of bccAddresses is valid
                if (bccAddressString != null && !bccAddressString.trim().equals('')) {
                    system.debug('@debug bccAddresses: ' + bccAddresses);
                    email.setBCCAddresses(bccAddresses);
                }
                
                if (fromAddress!=null && !fromAddress.trim().equals('')) {
                    system.debug('@debug fromAddress: ' + fromAddress);
                    email.setOrgWideEmailAddressId(fromAddress);
                }
                system.debug('@debug the email: '+ email);
                email.setUseSignature(false);
                //send it
                List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                emails.add(email);
                try {
                    system.debug('@debug sending emails: ' + emails); 
                    Messaging.sendEmail(emails);
                    isEmailSent = true;
                }
                catch (Exception e) {
                    system.debug('@debug an error occured: ' + e);
                    isEmailSent = false;
                    emailError = e.getMessage();
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, emailError)); 
                }
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'At least one article needs to be selected to send an e-mail.'));
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'A "To" Addressee is required and must be a Salesforce Contact'));
        }
    }
    
    @IsTest(SeeAllData=true)
    public static void testINQ_EmailController() {
        Case c = INQ_EmailControllerTest.testCase();
        List<InQuira_Article_Info__c> articles = INQ_EmailControllerTest.testArticles();
        for (InQuira_Article_Info__c a : articles) {
            INQ_EmailControllerTest.testLink(c, a, 0.0);
        }
        
        INQ_EmailController controller = new INQ_EmailController(c);
        String id = controller.getMyCaseId();
        System.assert(id==c.Id);
        List<SelectOption> opts = controller.getFromOptions();
        System.assert(opts.size()>0);
        if (opts.size()>1) {
            SelectOption opt = opts.get(1);
            controller.setFromAddress(opt.getValue());
            System.assert(controller.getFromAddress()==opt.getValue());
        }
        String naddr = controller.getToAddress();
        Contact toContact = INQ_EmailControllerTest.testContact();
        String addr = toContact.Email;
        controller.setToAddress(addr);
        String ccaddr = 'sharris@stonecobra.com; riverbedstrikeforce@stonecobra.com';
        controller.setCCAddressString(ccaddr);
        System.assert(controller.getCCAddressString()==ccaddr);
        String bccaddr = 'alt@stonecobra.com; alt2@stonecobra.com';
        controller.setBCCAddressString(bccaddr);
        System.assert(controller.getBCCAddressString()==bccaddr);
        List<EmailTemplate> etemps = controller.getEmailTemplates();
        if (etemps.size()>0) {
            EmailTemplate stemp = etemps.get(1);
            controller.setSelectedTemplate(stemp.Id);
            System.assert(controller.getSelectedTemplate()==stemp.Id);
            controller.insertTemplate();
        }
        List<InQuira_Case_Info__c> related = controller.getRelatedArticles();
        System.assert(related.size()>0);
        controller.setEmailMessage('test');
        System.assert(controller.getEmailMessage()=='test');
        //controller.sendEmail();
        //Boolean esent = controller.getIsEmailSent();
        //System.assert(esent==true);
        //String error = controller.getEmailError();
    }
    
    
}