@isTest
private class TestRCNRequestType {
	
  static testMethod void testRCNCustomerReferenceTaskController() 
  {  
  	  String CUSTOMER_REFERENCE_TASK = 'Customer Reference Task';
      PageReference pageRef = Page.RCNCustomerReferenceTask;
      Test.setCurrentPage(pageRef);
      
      Lead who = new Lead();
      who.lastName = 'Test Lead';
      who.LeadSource = 'Email';
      who.Company = 'Test company';
      who.Status = 'Open';
      insert who;
      
      Contact who1 = new Contact();
      who1.LastName = 'Test Contact';
      insert who1;
      
      Account what = new Account();
      what.Name = 'Test';
      what.Industry = 'Hi-Tech';
      insert what ;
      
      Opportunity what1 = new Opportunity();
      What1.name = 'testopp';
      what1.accountId = what.Id;
      what1.Primary_ISR__c = UserInfo.getUserId();
      what1.Type = 'New';
      what1.Channel__c = 'Direct';
      what1.LeadSource = 'Email';
      what1.CloseDate = System.today()+5;
      what1.StageName = '0-prospect';
      what1.Forecast_Category__c = 'Commit';
      what1.Competitors__c = 'Citrix';
      insert what1;
      
      Task t = new Task();
      t.whoId = who.Id;
      t.WhatId = what.Id;
      
      ApexPages.StandardController std = new ApexPages.StandardController(t);
	  RCNCustomerReferenceTaskController controller= new RCNCustomerReferenceTaskController(std);
      controller.init();
      
      Schema.DescribeSObjectResult taskRTDescribe = Schema.SObjectType.Task;
      Map<String,Schema.RecordTypeInfo> taskRecTypeMap = taskRTDescribe.getRecordTypeInfosByName();
      Schema.RecordTypeInfo rtById = taskRecTypeMap.get(CUSTOMER_REFERENCE_TASK);
      String taskrecordTypeId;
      if(rtById != null)
       taskrecordTypeId = rtById.getRecordTypeId();
      
      Task t1 = new Task();
      t1.whoId = who.Id;
      t1.WhatId = what1.Id;
      if(taskrecordTypeId != null)
      t1.RecordTypeId = taskrecordTypeId;
      
      Test.setCurrentPage(Page.RCNCustomerreferenceTask);
      ApexPages.currentPage().getParameters().put('RecordType',taskrecordTypeId);
      ApexPages.StandardController std1 = new ApexPages.StandardController(t1);
	  RCNCustomerReferenceTaskController controller1 = new RCNCustomerReferenceTaskController(std1);
	  controller1.temOpp = new Opportunity();
	  controller1.temOpp.Primary_Contact__c = who1.Id;
	  controller1.temLead = new Lead();
	  controller1.temLead.Sold_to_Partner_Contact__c = who1.Id;
      controller1.init();
      controller1.doSave();
      
     /*PageReference pageRef1 = Page.RCNCustomerRefTaskForMarketing;
      Test.setCurrentPage(pageRef1);
      Apexpages.currentPage().getParameters().put('RecordType',taskrecordTypeId);
      Apexpages.currentPage().getParameters().put('who_id',who.Id);
      
      Task t2 = new Task();
      ApexPages.StandardController std2 = new ApexPages.StandardController(t2);
	  RCNCustomerRefTaskForMarketing controller2 = new RCNCustomerRefTaskForMarketing(std2);
	  controller2.temOpp = new Opportunity();
	  controller2.temOpp.Primary_Contact__c = who1.Id;
	  controller2.temLead = new Lead();
	  controller2.temLead.Sold_to_Partner_Contact__c = who1.Id;
      controller2.doSave(); 
      controller2.getTypes(); 
      
      Apexpages.currentPage().getParameters().put('RecordType',taskrecordTypeId);
      Apexpages.currentPage().getParameters().put('who_id',who1.Id);
      Apexpages.currentPage().getParameters().put('what_id',what.Id);
      
      Task t3 = new Task();
      t3.Request_Type__c = 'Sales Request';
      ApexPages.StandardController std3 = new ApexPages.StandardController(t3);
	  RCNCustomerRefTaskForMarketing controller3 = new RCNCustomerRefTaskForMarketing(std3);
	  controller3.temOpp = new Opportunity();
	  controller3.temOpp.Primary_Contact__c = who1.Id;
	  controller3.temLead = new Lead();
	  controller3.temLead.Sold_to_Partner_Contact__c = who1.Id;
      controller3.doSave(); 
      controller3.getTypes();*/    
      
      Task t2 = new Task();
      t2.whoId = who.Id;
      //t2.WhatId = what1.Id;
      if(taskrecordTypeId != null)
      t2.RecordTypeId = taskrecordTypeId;
      
      t2.Customer_Reference_Contact__c = who1.Id;
      t2.Customer_Reference_Contact_other__c = who1.Id;
      insert t2;
      
      //Test.setCurrentPage(Page.RCNVIEWpage);
      //Apexpages.currentPage().getParameters().put('id',t2.Id);
      ApexPages.StandardController std4 = new ApexPages.StandardController(t2);
	  RCNCustomerRefTaskViewController controller4 = new RCNCustomerRefTaskViewController(std4); 
	  controller4.init();
	  
	  ApexPages.StandardController std5 = new ApexPages.StandardController(t2);
	  RCNCustomerReferenceTaskController controller5 = new RCNCustomerReferenceTaskController(std5);
      controller5.init();
      controller5.getTypes();
      controller5.doSave();
  }

}