public class INQ_MailedArticlesController {
    String caseId;
    String isPlainText;
    String listEmpty;
    String isSignature;
    String salutationName;
    String salutationEmail;
    
    Case thisCase;
    List<InQuira_Case_Info__c> sentArticles;
    
    public INQ_MailedArticlesController() {
        caseId = null;
        isPlainText = 'false'; 
        isSignature = null;
        sentArticles = null;
        salutationName = null;
        salutationEmail = null;
        listEmpty = 'false';
    }
    
    public String getCaseId() {
        return caseId;
    }
    
    public void setCaseId(String s) {
        String oldCaseId = caseId;
        caseId = s;
        if (oldCaseId!=caseId && caseId!=null) {
            thisCase = [SELECT Id,CaseNumber,ContactId,Subject,Article_Email_Message__c FROM Case WHERE Id=:caseId LIMIT 1];
        } else if (caseId==null) {
            thisCase = null;
        }
    }
    
    public Case getThisCase() {
        return thisCase;
    }
    
    public String getIsPlainText() {
        return isPlainText;
    }
    
    public void setListEmpty(String s) {
        listEmpty = s.equals('true') ? 'true' : 'false';
    }
    
    public String getListEmpty() {
        return listEmpty;
    }
    
    
    public void setIsPlainText(String s) {
        isPlainText = s.equals('true') ? 'true' : 'false';
    }

    public String getIsSignature() {
        return isSignature;
    }
    
    public void setIsSignature(String s) {
        isSignature = s.equals('true') ? 'true' : 'false';
    }
    
    public String getSalutationName() {
        return salutationName;
    }
    
    public void setSalutationName(String s) {
        salutationName = s;
    }
    
    public String getSalutationEmail() {
        return salutationEmail;
    }

    public void setSalutationEmail(String s) {
        salutationEmail = s;
    }
    
    public String getEmailMessage() {
        return isPlainText.equals('true') && thisCase!=null ? thisCase.Article_Email_Message__c 
                                                            : thisCase!=null ? HTMLENCODE(thisCase.Article_Email_Message__c)
                                                                             : '[mail message goes here]';
    }
        
    public List<InQuira_Case_Info__c> getSentArticles() {
        sentArticles = [SELECT Id, Email_Article__c, 
                                   Related_InQuira_Article__r.Title__c,
                                   Related_InQuira_Article__r.Display_URL__c,
                                   Related_InQuira_Article__r.Document_Id__c  
                              FROM InQuira_Case_Info__c 
                             WHERE Related_Case__c=:caseId
                               AND Email_Article__c=true];
        return sentArticles;    
    }
    
    public static String HTMLENCODE(String text) {
        String html = '<p>' + text.replaceAll('&', '&amp;')
                                  .replaceAll('\"', '&quot;')
                                  .replaceAll('<', '&lt;')
                                  .replaceAll('>', '&gt;')
                                  .replaceAll('\\n\\n', '</p><p>') + '</p>';
        return html;
    }
}