/****
*This batch class is used to query all Accounts that are not in a deleted state.  
*It first makes a query against the asset object and determines if the asset is active and updates the Assets Owned Supported
*and summarizes at the product_subfamily level to later update the Account with
*The next step summarizes based off of the OpportuntiyLineItem using the Account Id filter and the Opportunity not being closed
*The next step looks at the Opportunity using the Account Id as the filter and the Stage has to have a 6 in it.
*Finally updates the Account information based on the summarized data
*
*Change - Assets Under Trial can use the asset piece of this class to calculate
*Change - Probable Renewal Date can use this class to accomodate
*Change - Most Recent Deal Partner add logic to determine in this class
*Change - Assets Under Demo add logic to determine in this class
Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 
*/

global without sharing class BatchAccountRollups implements Database.Batchable<sObject>, Database.Stateful {

  global String Query;

  global BatchAccountRollups(){
   
    //Query = 'SELECT Id, (SELECT Id, SKU__c from Assets) from Account where Id =\'' + '00130000003Vl6A' + '\'';
    //Query = 'SELECT Id, (SELECT Id, SKU__c, Calculated_Contract_Status__c from Assets) from Account';   
    //Query = 'SELECT Id ,RecordTypeId,CreatedDate,Support_Site_Access_Expiry_Date__c from Account WHERE (IsDeleted = False)';
    Query = 'SELECT Id ,RecordTypeId,CreatedDate,Support_Site_Access_Expiry_Date__c,Category__c,Targeting_Customer_SubSegment__c,Targeting_Prospect_SubSegment__c,Targeting_Suspect_SubSegment__c,TargetingTopFCSTCat__c,TgtGreenfield__c,Targeting_Account_Segmentation__c from Account WHERE (IsDeleted = False) order by name';  //and Id = \''+'0015000000GAQ8N' + '\' 
    //Query = 'SELECT Id ,RecordTypeId,CreatedDate,Support_Site_Access_Expiry_Date__c,Category__c,Targeting_Customer_SubSegment__c,Targeting_Prospect_SubSegment__c,Targeting_Suspect_SubSegment__c,TargetingTopFCSTCat__c,TgtGreenfield__c,Targeting_Account_Segmentation__c,Probable_Renewal_Date__c from Account WHERE Id =\'' + '0015000000EdTgZ' + '\'';
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
         /*************  Existing Logic ********************/
       List<Account> AllAcct = new List<Account>();  // Will hold the AccountIds for all Updates
       
       List<Account> AccountToUpdate = new List<Account>();
       /*Deactivating support site expiry date population code
        //start: for support site access restriction project by prashant.singh@riverbed.com
        AccountUtil aUtil=new AccountUtil();
        Map<Id,Date> accMap=aUtil.getAllAssetByAccount(scope);
        //End: for support site access restriction project by prashant.singh@riverbed.com
        */
       for(sObject so : scope) {
         //try {
           Account a = (Account)so;
           
           AllAcct.add(a); // Collect Account Id for the Updates
           
           Integer SteelheadOwned = 0,CMCOwned = 0,CascadeOwned = 0,InterceptorOwned = 0,MazuLegacyOwned = 0,MemoryOwned = 0,RSPOwned = 0,MobileOwned =0,
             StingrayTMOwned = 0, StingrayTMLegacyOwned = 0, StingrayAptimizerOwned = 0, StingrayAptimizerLegacyOwned = 0, StingrayAFOwned = 0, 
             ProfilerOwned = 0, GraniteOwned = 0, SMCOwned = 0, WhitewaterOwned = 0, SharkOwned = 0, PilotOwned = 0, 
             SteelheadVCOwned = 0, SteelheadCAOwned = 0, NeopOwned = 0, AixOwned = 0, ArxOwned = 0, AtxOwned = 0, UcxOwned = 0,
             APMOwned = 0, npcmOwned = 0, sccOwned = 0,netExpressOwned = 0,
             SteelheadQuoted = 0,CMCQuoted = 0,CascadeQuoted = 0,InterceptorQuoted = 0,  MazuLegacyQuoted = 0,MemoryQuoted = 0,RSPQuoted = 0, MobileQuoted=0;                  
             
           
        Date minDesupportDate = Date.newInstance(2040, 1, 1);
        
        String ProductsOwned = '';
        Set<String> productSet=new Set<String>();
        Integer SupportedAssetCount = 0;
           for (Asset ast : [SELECT Id, SKU__c, RSP__c, Calculated_Contract_Status__c, Status, IB_Status__c, AccountId, Product2.Desupport_Date__c, Product2.Product_Sub_Family__c FROM Asset WHERE (AccountId = :a.Id) AND (IB_Status__c NOT IN ('Under Evaluation','Under Right of Return', 'Sold - Demo', 'WRITTEN OFF')) ])//removed limit 1000 - ankita 11/5/2014
        {
          if (ast.Status == 'ACTIVE') SupportedAssetCount++;
          //Start: added by prashant on 03/28/2013;ticket#141139
          if(ast.RSP__c=='Yes'){
            RSPOwned+=1;
          }//End          
          String productSubFamily = ast.Product2.Product_Sub_Family__c;
          if (productSubFamily == null) productSubFamily = '';          
             if(ast.AccountId == a.Id) {               
               if (ast.Product2.Desupport_Date__c < minDesupportDate) minDesupportDate = ast.Product2.Desupport_Date__c;               
            
            /*if(productSubFamily.startsWith('Cascade')||productSubFamily.startsWith('Profiler')||productSubFamily.startsWith('Shark')||productSubFamily.startsWith('Pilot')) {
              CascadeOwned += 1;
            }
            else if(productSubFamily.startsWith('Steelhead'))
              SteelheadOwned += 1;
            else if(productSubFamily.contains('CMC'))
              CMCOwned += 1;
            else if(productSubFamily.startsWith('Interceptor'))
              InterceptorOwned += 1;
            else if(productSubFamily.startsWith('Mazu Legacy'))
              MazuLegacyOwned += 1;
            else if(productSubFamily.startsWith('Memory') )
              MemoryOwned += 1;
            /*else if(ast.RSP__c=='Yes')//start
              RSPOwned+=1;*///End:commented by prashant on 03/28/2013;ticket#141139
            /*else if(productSubFamily.startsWith('SMC'))
              MobileOwned += 1;
            */
            
            if(productSubFamily.toUpperCase().startswith('CMC') || productSubFamily.toUpperCase().startswith('CMS') || productSubFamily.toUpperCase().startswith('MCMC'))
              CMCOwned += 1;
            else if(productSubFamily.toUpperCase().startsWith('INS') || productSubFamily.toUpperCase().startsWith('INT'))
              InterceptorOwned += 1;
            else if(productSubFamily.startsWith('Memory') )
              MemoryOwned += 1;
            else if(productSubFamily.startsWith('SMC'))
              MobileOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Stingray Traffic Manager'))
              StingrayTMOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Stingray Traffic Manager (Legacy)'))
              StingrayTMLegacyOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Stingray (Aptimizer)'))
              StingrayAptimizerOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Stingray Aptimizer (Legacy)'))
              StingrayAptimizerLegacyOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Stingray (App Firewall)'))
              StingrayAFOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Granite'))
              GraniteOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Whitewater'))
              WhitewaterOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Shark'))
              SharkOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Pilot'))
              PilotOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Profiler'))
              ProfilerOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Mazu Legacy'))
              MazuLegacyOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Steelhead (Virtual/Cloud)'))
              SteelheadVCOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Steelhead Cloud Accelerator'))
              SteelheadCAOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Steelhead'))
              SteelheadOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('NEOP'))
              NeopOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('OPNET-APM'))
                APMOwned +=1;
            else if(productSubFamily.equalsIgnoreCase('AIX'))
              AixOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('ARX'))
              ArxOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('ATX'))
              AtxOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('UCX'))
              UcxOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('Cascade'))
              cascadeOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('NPCM'))
              npcmOwned += 1;
            else if(productSubFamily.equalsIgnoreCase('NetExpress'))   
              netExpressOwned +=1;
            else if(productSubFamily.equalsIgnoreCase('SCC'))
              sccOwned += 1;
          }
          /* Start --commented by prasant.singh@riverbed.com for ticket#117476
          if (!ProductsOwned.contains(productSubFamily)){
            ProductsOwned += ';' + productSubFamily;
          } End*/
          productSet.add(productSubFamily);//added by prashant.singh@riverbed.com for ticket#117476
        }
        //system.debug('***ProductsOwned::'+ProductsOwned);
        //Start --Added by prasant.singh@riverbed.com for ticket#117476
        if(productSet.size()>0){
          for(String str:productSet){
          ProductsOwned += ';' + str;
        }
        }//End --commented by prasant.singh@riverbed.com for ticket#117476
        a.Assets_Owned_Supported__c = SupportedAssetCount;
        
    
          /* 
          Will update the “Assets Under Trial” to the count of all assets attached with an Asset.IB Status 
                  of “Under Evaluation” or “Under Right of Return”. 
            */
        
        // Added by Prashant singh. 10/5/12 ticket#119297  
        List<AggregateResult> aggAssetUndertrailLst = [Select AccountId, Count(Id) from Asset WHERE AccountId = :a.Id AND
                                    (IB_Status__c = 'Under Evaluation' OR IB_Status__c = 'Under Right of Return' )
                                   GROUP BY AccountId];
            
        if(aggAssetUndertrailLst.size()== 0){
           a.Assets_Under_Trial__c = 0;
           //system.debug('List Zero');
        }else {
          for(AggregateResult lcount1 : aggAssetUndertrailLst){
         /*for(AggregateResult lcount1 : [ Select AccountId, Count(Id) from Asset WHERE AccountId = :a.Id AND
                                    (IB_Status__c = 'Under Evaluation' OR IB_Status__c = 'Under Right of Return' )
                                   GROUP BY AccountId])*/       //Commented by Prashant singh. 10/5/12 ticket#119297                    
            //a.Assets_Under_Trial__c = Integer.ValueOf(lcount1.get('expr0'));
            a.Assets_Under_Trial__c = (Integer.ValueOf(lcount1.get('expr0')) == null ? 0 : Integer.ValueOf(lcount1.get('expr0'))); //added by Prashant singh. 10/5/12 ticket#119297
            system.debug('\n Assets Under Trial :'+Integer.ValueOf(lcount1.get('expr0')));
          }
        }
          
          /* 
                Will update the "Assets Under Demo" to the count of Asset.IB records having the field "Status" = “Sold – Demo”. 
        */
        
        // Added by Prashant singh. 10/5/12 ticket#119297
        List<AggregateResult> aggAssetDemolst = [Select AccountId, Count(Id) from Asset 
                                     WHERE AccountId = :a.Id AND (IB_Status__c = 'Sold – Demo')
                                     GROUP BY AccountId];
            
        if(aggAssetDemolst.size()== 0){
           a.AssetsDemo__c  = 0;
           //system.debug('List Zero DEmo');
        }else { 
          for( AggregateResult lcount2 :aggAssetDemolst){
          /*for(AggregateResult lcount2 :  [ Select AccountId, Count(Id) from Asset 
                                     WHERE AccountId = :a.Id AND (IB_Status__c = 'Sold – Demo')
                                     GROUP BY AccountId]) */   //Commenter by   Prashant singh. 10/5/12 ticket#119297                              
            //a.Assets_Under_Demo__c = Integer.ValueOf(lcount2.get('expr0'));
            //a.AssetsDemo__c = Integer.ValueOf(lcount2.get('expr0'));
            a.AssetsDemo__c = (lcount2.get('expr0') == null)? 0:Integer.ValueOf(lcount2.get('expr0')); //added by Prashant singh. 10/5/12 ticket#119297
            system.debug('\n Assets Under Demo'+Integer.ValueOf(lcount2.get('expr0')));
          }  
        }  
          /*
            Will update the Account.Probable Lead Count
          */          
          for(AggregateResult probLeadCount : [Select count(l.Id) From Lead l where l.Probable_Account__c = :a.Id])          
          {
            a.Probable_Lead_Count__c = Integer.ValueOf(probLeadCount.get('expr0'));
          }           
        /*          
          Set the Account.Probable Renewal Date to the nearest future date of the set of dates “Renewed End Date” 
                or “Support End Date” for all Account Assets.
            */
        Date ProbableRenewDate = null;
        for (Asset ast: [Select Id, Renewed_End_Date__c, Support_End_Date__c FROM Asset WHERE (AccountId = :a.id)order by Support_End_Date__c,Renewed_End_Date__c ]) {
          /*  
            if (ast.Renewed_End_Date__c < ast.Support_End_Date__c && ast.Renewed_End_Date__c > System.today() && ast.Renewed_End_Date__c != null )
                ProbableRenewDate = ast.Renewed_End_Date__c;
            else if ( ast.Support_End_Date__c < ast.Renewed_End_Date__c && ast.Renewed_End_Date__c > System.today() && ast.Support_End_Date__c != null )
              ProbableRenewDate = ast.Support_End_Date__c ;
              else
                system.debug('\n\n This Use Case was not taken into Consideration Support D: '+
                          ast.Support_End_Date__c+' Renewed Date: '+ast.Renewed_End_Date__c);
           */
          //system.debug('\n Support_End_Date__c: '+ast.Support_End_Date__c);
        //system.debug('\n today: '+System.today());
        //system.debug('\n Renewed_End_Date__c: '+ast.Renewed_End_Date__c);
        if(ast.Renewed_End_Date__c==null && (ast.Support_End_Date__c > System.today()|| ast.Support_End_Date__c < System.today())){
          ProbableRenewDate = ast.Support_End_Date__c;
        }else if(ast.Support_End_Date__c==null && (ast.Renewed_End_Date__c> System.today()||ast.Renewed_End_Date__c < System.today())){
          ProbableRenewDate = ast.Renewed_End_Date__c ;
          }else if (ast.Renewed_End_Date__c < ast.Support_End_Date__c && ast.Support_End_Date__c > System.today() && ast.Support_End_Date__c != null ){
                ProbableRenewDate = ast.Support_End_Date__c;
        }else if ( ast.Support_End_Date__c < ast.Renewed_End_Date__c && ast.Renewed_End_Date__c > System.today() && ast.Renewed_End_Date__c != null ){
              ProbableRenewDate = ast.Renewed_End_Date__c ;
          }else{
              system.debug('\n\n This Use Case was not taken into Consideration Support D: '+
                          ast.Support_End_Date__c+' Renewed Date: '+ast.Renewed_End_Date__c);
          } 
        }
        a.Probable_Renewal_Date__c = ProbableRenewDate;
        system.debug('\n ProbableRenewal Date5: '+ProbableRenewDate);
        
        
        /*
        * Update 5 : Set the Account field “Most Recent Partner” to the Opportunity field “Partner Closest to Customer” from the 
        *    Opportunity record having the most recent “Close Date”, a stage value of “Stage 6”, 
        *    and a non-null “Partner Closest to Customer”. 
        */
        
        for(AggregateResult lcount3 : [Select MIN(CloseDate), AccountId, MIN(Partner_Closest_to_Customer_Id__c) pClosest From Opportunity
                      WHERE StageName = '6 - Order Accepted' AND Partner_Closest_to_Customer_Id__c != Null
                      AND AccountId = :a.id                      
                        GROUP BY AccountId])        
        {          
              // Prevent circular references          
              if(String.ValueOf(lcount3.get('pClosest'))!=a.Id)
                //a.Most_Recent_Partner__c = String.ValueOf(lcount3.get('pClosest'));
                a.Most_Recent_Deal_Partner__c = String.ValueOf(lcount3.get('pClosest'));                
              system.debug('\n Most Recent Deal Partner: '+String.ValueOf(lcount3.get('pClosest')));
        }
        
        
           //for(OpportunityLineItem oli: [Select OpportunityId, Opportunity.AccountId, PricebookEntryId, PricebookEntry.ProductCode from OpportunityLineItem where Opportunity.AccountId = :a.Id and Opportunity.IsClosed = false])
           for(OpportunityLineItem oli : [SELECT OpportunityId, Quantity, Opportunity.AccountId, PricebookEntryId, PricebookEntry.ProductCode, PricebookEntry.Product2.Product_SubFamily__c FROM OpportunityLineItem WHERE (Opportunity.AccountId = :a.Id) AND (Opportunity.IsClosed = false) LIMIT 4000])
           {
             String productSubFamily = oli.PricebookEntry.Product2.Product_SubFamily__c;
             if (productSubFamily == null) productSubFamily = ''; 
             
             if(oli.Opportunity.AccountId == a.Id) {
            if(productSubFamily.startsWith('Cascade')||productSubFamily.startsWith('Profiler')||productSubFamily.startsWith('Shark')||productSubFamily.startsWith('Pilot'))
              CascadeQuoted += Math.Round(oli.Quantity);
            else if(productSubFamily.startsWith('Steelhead'))
              SteelheadQuoted += Math.Round(oli.Quantity);
            else if(productSubFamily.contains('CMC'))
              CMCQuoted += Math.Round(oli.Quantity);
            else if(productSubFamily.startsWith('Interceptor'))
              InterceptorQuoted += Math.Round(oli.Quantity);
            else if(productSubFamily.startsWith('Mazu Legacy'))
              MazuLegacyQuoted += Math.Round(oli.Quantity);
            else if(productSubFamily.startsWith('Memory') )
              MemoryQuoted += Math.Round(oli.Quantity);
            else if(productSubFamily.Contains('RSP'))
              RSPQuoted += Math.Round(oli.Quantity);
            else if(productSubFamily.startsWith('SMC'))
              MobileQuoted += Math.Round(oli.Quantity);
             }
           }  
           
           DateTime LastSoldToDate = null;
           Id SoldToPartner = null;
        for (Opportunity opp: [SELECT Id, Sold_To_Partner__c, Sold_To_Partner_Type__c, CloseDate, StageName, Partner_Closest_to_Customer_Id__c FROM Opportunity WHERE (AccountId = :a.Id) AND (StageName LIKE '%6%') LIMIT 1000])
        {
          if(opp.StageName.Contains('6') && (LastSoldToDate == null || opp.CloseDate > LastSoldToDate))          
          {
            LastSoldToDate = opp.CloseDate;
            SoldToPartner = opp.Partner_Closest_to_Customer_Id__c;
            //SoldToPartnerType = opp.Sold_To_Partner_Type__c;
            //Tier2 = opp.Tier2__c;
            //Tier2Type = opp.Tier2_Type__c;
          }
        }
        
        // Prevent circular references
        if (SoldToPartner != a.Id) a.Most_Recent_Deal_Partner__c = SoldToPartner;
  
        a.SteelheadOwned__c = SteelheadOwned;
        a.CMCOwned__c = CMCOwned;
        //a.CascadeOwned__c = CascadeOwned;  
        a.InterceptorOwned__c = InterceptorOwned;
        a.MazuLegacyOwned__c = MazuLegacyOwned;
        a.MemoryOwned__c = MemoryOwned;
        a.RSPOwned__c = RSPOwned;
        a.MobileOwned__c = MobileOwned;
        
        
        a.Stingray_Traffic_Manager_Assets_Owned__c = StingrayTMOwned;
        a.Stingray_Traffic_Manager_Legacy_Assets__c = StingrayTMLegacyOwned;
        a.Stingray_Aptimizer_Assets_Owned__c = StingrayAptimizerOwned;
        a.Stingray_Aptimizer_Legacy_Assets_Owned__c = StingrayAptimizerLegacyOwned;
        a.Stingray_App_Firewall_Assets_Owned__c = StingrayAFOwned;
        a.Profiler_Assets_Owned__c = ProfilerOwned;
        a.Granite_Assets_Owned__c = GraniteOwned;
        a.Whitewater_Assets_Owned__c = WhitewaterOwned;
        a.Shark_Assets_Owned__c = SharkOwned;
        a.Pilot_Assets_Owned__c = PilotOwned;
        a.Steelhead_Virtual_Cloud_Assets_Owned__c = SteelheadVCOwned;
        a.Steelhead_Cloud_Accelerator_Assets_Owned__c = SteelheadCAOwned;
        a.NEOP_Assets_Owned__c = NeopOwned;
        a.AIX_Assets_Owned__c = AixOwned;
        a.ARX_Assets_Owned__c = ArxOwned;
        a.ATX_Assets_Owned__c = AtxOwned;
        a.UCX_Assets_Owned__c = UcxOwned;
        a.CascadeOwned__c = cascadeOwned;
        a.SteelCentral_AppMapper_OPNETAPM_Assets__c = APMOwned;
        a.SteelCentral_NPCM_Assets_Owned__c = npcmOwned;
        a.SteelCentral_SCC_Assets_Owned__c = sccOwned;
        a.NetExpress__c = netExpressOwned;
        
        // family total
        a.Total_Account_SteelHead_Assets__c = NULLCHECK(a.Steelhead_Virtual_Cloud_Assets_Owned__c) + NULLCHECK(a.Steelhead_Cloud_Accelerator_Assets_Owned__c) + NULLCHECK(a.SteelheadOwned__c) + NULLCHECK(a.MobileOwned__c) + NULLCHECK(a.CMCOwned__c) + NULLCHECK(a.InterceptorOwned__c) + NULLCHECK(a.Granite_Assets_Owned__c);
        a.Total_Account_SteelCentral_Assets__c = NULLCHECK(a.NetExpress__c) + NULLCHECK(a.MazuLegacyOwned__c) + NULLCHECK(a.Shark_Assets_Owned__c) + NULLCHECK(a.Profiler_Assets_Owned__c) + NULLCHECK(a.Pilot_Assets_Owned__c) + NULLCHECK(a.ARX_Assets_Owned__c) + NULLCHECK(a.ATX_Assets_Owned__c) + NULLCHECK(a.AIX_Assets_Owned__c) + NULLCHECK(a.SteelCentral_AppMapper_OPNETAPM_Assets__c) + NULLCHECK(a.UCX_Assets_Owned__c) + NULLCHECK(a.NEOP_Assets_Owned__c) + NULLCHECK(a.CascadeOwned__c) + NULLCHECK(a.SteelCentral_NPCM_Assets_Owned__c) + NULLCHECK(a.SteelCentral_SCC_Assets_Owned__c);
        a.Total_Account_SteelApp_Assets__c = NULLCHECK(a.Stingray_App_Firewall_Assets_Owned__c) + NULLCHECK(a.Stingray_Aptimizer_Assets_Owned__c) + NULLCHECK(a.Stingray_Aptimizer_Legacy_Assets_Owned__c) + NULLCHECK(a.Stingray_Traffic_Manager_Legacy_Assets__c) + NULLCHECK(a.Stingray_Traffic_Manager_Assets_Owned__c);
        
        a.SteelheadQuoted__c = SteelheadQuoted;
        a.CMCQuoted__c = CMCQuoted;
        a.CascadeQuoted__c = CascadeQuoted;
        a.InterceptorQuoted__c = InterceptorQuoted;
        a.MazuLegacyQuoted__c = MazuLegacyQuoted;
        a.MemoryQuoted__c = MemoryQuoted;
        a.RSPQuoted__c = RSPQuoted;
        a.MobileQuoted__c = MobileQuoted;    
        
        if (minDesupportDate < Date.newInstance(2040, 1, 1))
          a.Next_Desupport__c = minDesupportDate;
        else
          a.Next_Desupport__c = null;  
        
        a.Riverbed_Products_Owned__c = ProductsOwned;
        
        /*Deactivating support site expiry date population code as per ticket#128573
        //start: for support site access restriction project by prashant.singh@riverbed.com
        if(accMap.size()>0){
            if(accMap.containsKey(a.Id)){
                System.debug('Support_Site_Access_Expiry_Date__c:' + a.Support_Site_Access_Expiry_Date__c);
                System.debug('accMap.get(a.Id):' + accMap.get(a.Id));
            if(a.Support_Site_Access_Expiry_Date__c!=null && accMap.get(a.Id)<= a.Support_Site_Access_Expiry_Date__c){
            // do nothing
            }else{
                  a.Support_Site_Access_Expiry_Date__c=accMap.get(a.Id);
                }
            }
        }
         //end: for support site access restriction project by prashant.singh@riverbed.com  
       */
       
       /**for Ticket#103984 -- The field "Targeting - Account Segmentation" will need to be a concatenate from 2 other fields
       ** by: prashant.singh@riverbed.com
       ** Date: 5/4/2012
       */                     
        if(a.Category__c.equalsIgnoreCase('Prospect')){
            a.Targeting_Account_Segmentation__c=a.Targeting_Prospect_SubSegment__c+'-'+a.TargetingTopFCSTCat__c;
        }else if(a.Category__c.equalsIgnoreCase('Suspect')){
            a.Targeting_Account_Segmentation__c=a.Targeting_Suspect_SubSegment__c+'-'+a.TgtGreenfield__c;
        }else if(a.Category__c.equalsIgnoreCase('New')||a.Category__c.equalsIgnoreCase('Existing')){
            a.Targeting_Account_Segmentation__c=a.Targeting_Customer_SubSegment__c+'-'+a.TargetingTopFCSTCat__c;
        }                     
        AccountToUpdate.add(a);
         /*}
         catch (Exception e) {
           // Do nothing
         }*/
       }
       //jsADM.jsADM_ContextProcessController.isInFutureContext = true;
    update AccountToUpdate;
    
   }
   
   public Decimal NULLCHECK(Decimal value) {
       if(value == null) return 0;
       else return value;
   }
   
   global void finish(Database.BatchableContext BC){

   }
  
}