@IsTest
private class TestAQIAuditSharingRules {
	
    static testMethod void testAQISharing() {
    	TestAQIRQIUtils testTools = new TestAQIRQIUtils();
    	Test.startTest();
    	User testUser = testTools.createMember();
    	
		AQI_Audit__c assessment = new AQI_Audit__c(Author__c = testUser.Id);
     	insert assessment;
     	
		System.runAs(testUser)  {
			List <AQI_Audit__c> assessments = [SELECT Id FROM AQI_Audit__c WHERE Id=:assessment.Id];
			System.assert(assessments.size() > 0);
		} 
		Test.stopTest(); 
    }
    
}