public class OpportunityWrapper  {
 
    public Boolean checked{ get; set; }
    public Opportunity ops{ get; set;}
 
    public OpportunityWrapper (){
    ops= new Opportunity();
        checked = false;

       }
 
    public OpportunityWrapper (Opportunity c){
        ops = c;
        checked = false;
    }
 
   }