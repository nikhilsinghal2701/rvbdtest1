global without sharing class BatchOpportunityCountRollup implements Database.Batchable<sObject>
{
	global BatchOpportunityCountRollup()
	{   		
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator([SELECT Id,(SELECT Id From Assets__r where IB_Status__c in ('Under Evaluation','Under Right of Return')) From Opportunity where Trial_Program__c!=null and Trial_Install_Date__c!=null]);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope) 
    {	
		List<Opportunity> updateList = new List<Opportunity>();
		for(sObject so : scope)
		{
			Opportunity opp = (Opportunity)so;
			if(opp.Assets__r !=null)
			{
				opp.Trial_Asset_Count__c = opp.Assets__r.size();
			}
			else
				opp.Trial_Asset_Count__c = 0;
			updateList.add(opp);			
		}
		update updateList;
    }
   
    global void finish(Database.BatchableContext BC)
    {

    }
}