@isTest (SeeAllData=true)
private class ConciergeAcceptanceCapturingTest {

    static testMethod void TestQueueOwner() 
    {
    	// Sets conciergeProfileIds and queueNames
    	setConciergeVars();
    	
		List<Id> queueIds = new List<Id>();
		for (QueueSobject queueobj : [Select Queue.Id, QueueId, Queue.Name From QueueSobject Where SobjectType = 'Lead' and Queue.DeveloperName = :queueNames]) queueIds.add(queueobj.QueueId);
        
        // Make sure org has custom setting Concierge_Owner_Criteria__c with valid queue DeveloperNames
        system.assert(queueIds.size() > 0);
        
        User user = [Select Id From User Where UserType = 'Standard' and IsActive = true Limit 1];
        
        List<Lead> lds = initTestLeads('lds', 2);
        lds[0].OwnerId = queueIds[0];
        lds[1].OwnerId = user.Id;
        insert lds;  
        
        List<Campaign> camps = initTestCampaigns('cmp', 1);
        insert camps;
        
        CampaignMember cmLead1 = new CampaignMember(Status='Responded', LeadId = lds[0].id, CampaignID = camps[0].id);
        CampaignMember cmLead2 = new CampaignMember(Status='Responded', LeadId = lds[1].id, CampaignID = camps[0].id);
        
        FCRM.FCR_SupportAPI.TreatNextCampaignAssociationsAsResponsePrompts();
        
        Test.startTest();
        insert new List<CampaignMember>{cmLead1, cmLead2};
        lds[1].OwnerId = queueIds[0];
        update lds[1]; 
        // Sync should set this field when not in test context 
        cmLead2.Owner_Changed_Timestamp__c = Datetime.now();     
        update cmLead2; 
		Test.stopTest();
		
		for (CampaignMember cm : [Select LeadId, Owner_Changed_Timestamp__c, FCRM__FCR_Response_Status__c, Concierge_Accepted__c, Id From CampaignMember Where LeadId = :lds[0].id or LeadId = :lds[1].id]) 
		{
			system.assert(cm.Concierge_Accepted__c);
		}
    }

    static testMethod void TestProfileOwner() 
    {
    	// Sets conciergeProfileIds and queueNames
    	setConciergeVars();
    	
		List<User> conciergeUsers = [Select Id From User where ProfileId IN :conciergeProfileIds and IsActive = true];
		
		// Make sure org has custom setting Concierge_Owner_Criteria__c with concierge profiles associated with active users
		system.assert(conciergeUsers.size() > 0);
        
        User user = [Select Id From User Where UserType = 'Standard' and IsActive = true Limit 1];
        
        List<Lead> lds = initTestLeads('lds', 2);
        lds[0].OwnerId = conciergeUsers[0].Id;
        lds[1].OwnerId = user.Id;
        insert lds;  
        
        List<Campaign> camps = initTestCampaigns('cmp', 1);
        insert camps;
        
        CampaignMember cmLead1 = new CampaignMember(Status='Responded', LeadId = lds[0].id, CampaignID = camps[0].id);
        CampaignMember cmLead2 = new CampaignMember(Status='Responded', LeadId = lds[1].id, CampaignID = camps[0].id);
        
        Test.startTest();
        insert new List<CampaignMember>{cmLead1, cmLead2};
        lds[1].OwnerId = conciergeUsers[0].Id;
        update lds[1]; 
        // Sync should set this field when not in test context 
        cmLead2.Owner_Changed_Timestamp__c = Datetime.now();        
        update cmLead2;        
		Test.stopTest();
		
		for (CampaignMember cm : [Select LeadId, Concierge_Accepted__c, Id From CampaignMember Where LeadId = :lds[0].id or LeadId = :lds[1].id]) 
		{
			system.assert(cm.Concierge_Accepted__c);
		}
    }    

	private static Set<String> conciergeProfileIds = new Set<String>();
	
	private static Set<String> queueNames = new Set<String>();    
    
    private static void setConciergeVars()
    {
		Concierge_Owner_Criteria__c setting = Concierge_Owner_Criteria__c.getInstance('default');
		if (setting.Concierge_Profile_IDs__c != null && setting.Queue_Names__c != null)
		{ 
			conciergeProfileIds.addAll(setting.Concierge_Profile_IDs__c.split(';'));
			queueNames.addAll(setting.Queue_Names__c.split(';'));	
		}    	
    }
    
    public static List<Contact> initTestContacts(String prefix, Integer count)  
    {    
        List<Contact>cts = new List<Contact>();    
        for(Integer x=1;x<count+1;x++)    
        {      
          cts.add(new Contact(LastName = prefix + '_' + String.valueOf(x)));
        }
        return cts;  
    }


    public static List<Campaign> initTestCampaigns(String prefix, Integer count)
    {
        List<Campaign> camps = new List<Campaign>();
        for(Integer x=1; x<count+1; x++)
        {
          camps.add(new Campaign(Campaign_Level__c='Disaster Recovery', Name = prefix+'_'+ String.ValueOf(x), IsActive = true));
        }
        return camps;
    }      
    
    public static List<Lead> initTestLeads(String prefix, Integer count)  
    {    
        List<Lead>lds = new List<Lead>();    
        for(Integer x=1;x<count+1;x++)    
        {      
          lds.add(new Lead(Company= prefix + '_' + String.valueOf(x), LastName = prefix + '_' + String.valueOf(x), Status='Nurture/Marketing'));    
        }    
        return lds;  
    }    
}