public class RuleEvaluator {

	public RuleEvaluator() {
	}
	
	public Id evaluate(Lead leadObj) {
		List<Task_Rule__c> taskRules = [Select Task_Template__c, Task_Template__r.Name 
		from Task_Rule__c 
			where Partner_Level__c = :leadObj.Partner_Level__c And
			Partner_Type__c = :leadObj.Partner_Type__c And
			Provides_Level_1_2_Support__c = :leadObj.Provides_Level_1_2_Support__c And 
			Purchase_through_a_Distributor__c = :leadObj.Purchase_through_a_Distributor__c And
			Geo__c = :leadObj.Geo__c 
			And
			Region__c INCLUDES (:leadObj.Region__c)
			Order By LastModifiedDate Desc
			Limit 1];
		if (! taskRules.isEmpty()) {
			System.debug('Returned Template : ' + taskRules.get(0).Task_Template__r.Name);
			return taskRules.get(0).Task_Template__c;
		}
		throw new RuleEvaluatorException('No Matching Task Templates found for the given configuration');
	}
	
	static testMethod void testTemplates() {
        Lead l = new Lead(firstname = 'TestFirst', lastName = 'TestLast', Company='TestCompany.com',
        	email = 'testfirst.last@testcompany.com',
        	Sales_Contact_Last_Name__c = 'testSalesContact',
        	Partner_Level__c = 'Gold', 
        	Partner_Type__c = 'VAR', 
        	Provides_Level_1_2_Support__c = false,
        	Purchase_through_a_Distributor__c = false,
        	Region__c = 'India;China',	
        	Geo__c = 'APAC',AnnualRevenue = 1200000, Number_Of_Employees__c = 200, 
        	Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
        	Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', 
        	Secondary_Application__c = 'UDP',Phone='4154154156', Street = 'Remington Dr', City = 'San Francisco', 
        	Country = 'US', State = 'CA', Preferred_Distributor__c = 'Arrow');
//for multiple regions - example - 'India;China'
        l.recordTypeId = [Select r.Id From RecordType r where SobjectType = 'Lead' And Name = 'Partner Recruitment'].Id;
        insert l;
		RuleEvaluator evaluator = new RuleEvaluator();
		try {
			evaluator.evaluate(l);
		} catch (RuleEvaluatorException e) {}
	}
}