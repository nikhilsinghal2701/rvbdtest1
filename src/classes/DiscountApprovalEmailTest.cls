/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */@isTest(SeeAllData=true)
private class DiscountApprovalEmailTest {
    testMethod public static void testEmailServiceApprove() {    
        DiscountApproval.isTest = true;

        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //System.assert(false, 'T: ' + [SELECT User__r.Name, Level__c FROM Approvers_Routing_Matrix__c WHERE Quote__c = :q.Id]);
        System.assertEquals('Required', q.Discount_Status__c);
         
        //DiscountApprovalTestHlp.submitQuote(q);
        Test.startTest();
        User app = getApprover(q);
        
        System.debug('cc------'+q.Approver2__C);
        System.debug(LoggingLevel.INFO,'cc------' +q.Approver2__C +'1--'+q.Approver3__C+'2--'+q.Approver1__C);
        DiscountApprovalEmail esvc = new DiscountApprovalEmail();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        DiscountApprovalEmail.isTest=true;
        email.plainTextBody = 'approve my comments\n' + DiscountApprovalEmail.QUOTE_DELIM + q.Id;
        email.fromAddress = app.Email;
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        esvc.handleInboundEmail(email, envelope);
                Test.stopTest(); 

    }
    
    
   testMethod public static void testEmailServiceReject() {    
        DiscountApproval.isTest = true;

        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        
         
        //DiscountApprovalTestHlp.submitQuote(q);
        Test.startTest();

        User app = getApprover(q);
        
        DiscountApprovalEmail esvc = new DiscountApprovalEmail();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
         DiscountApprovalEmail.isTest=true;
        email.plainTextBody = 'reject my comments\n' + DiscountApprovalEmail.QUOTE_DELIM + q.Id;
        email.fromAddress = app.Email;
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        esvc.handleInboundEmail(email, envelope);
        Test.stopTest(); 
    } 
    
    testMethod public static void testEmailServiceMoreInfo() {    
        DiscountApproval.isTest = true;

        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        
        system.assert(q.Discount_Status__c == 'Required');
         
        //DiscountApprovalTestHlp.submitQuote(q);
        
        User app = getApprover(q);
        
        DiscountApprovalEmail esvc = new DiscountApprovalEmail();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
         DiscountApprovalEmail.isTest=true;
        email.plainTextBody = 'more info my comments\n' + DiscountApprovalEmail.QUOTE_DELIM + q.Id;
        email.fromAddress = app.Email;
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        esvc.handleInboundEmail(email, envelope);
    } 
    testMethod public static void testEmailServiceInvalidEmail() {    
        DiscountApproval.isTest = true;

        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
            system.assert(q.Discount_Status__c == 'Required');
         
            //DiscountApprovalTestHlp.submitQuote(q);
            
            DiscountApprovalEmail esvc = new DiscountApprovalEmail();
            Messaging.InboundEmail email = new Messaging.InboundEmail();
             DiscountApprovalEmail.isTest=true;
            email.plainTextBody = 'approve my comments\n' + DiscountApprovalEmail.QUOTE_DELIM + q.Id;
            email.fromAddress = 'jimmyjazz@nowhere.com';
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
            esvc.handleInboundEmail(email, envelope);
    }

    testMethod public static void testEmailServiceNoQuoteId() {    
        DiscountApproval.isTest = true;

        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        
        system.assert(q.Discount_Status__c == 'Required');
         
        //DiscountApprovalTestHlp.submitQuote(q);
                       Test.startTest();

        User app = getApprover(q);

        DiscountApprovalEmail esvc = new DiscountApprovalEmail();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        DiscountApprovalEmail.isTest=true;
        email.plainTextBody = 'approve my comments\n' + DiscountApprovalEmail.QUOTE_DELIM;
        email.fromAddress = app.Email;
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        esvc.handleInboundEmail(email, envelope);
        Test.stopTest(); 
    }

    testMethod public static void testEmailServiceNotPending() {    
        DiscountApproval.isTest = true;

        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        system.assert(q.Discount_Status__c == 'Required');

        User app = getApprover(q);          
        DiscountApprovalEmail esvc = new DiscountApprovalEmail();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        DiscountApprovalEmail.isTest=true;
        email.plainTextBody = 'approve my comments\n' + DiscountApprovalEmail.QUOTE_DELIM + q.Id;
        email.fromAddress = app.Email;
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        esvc.handleInboundEmail(email, envelope);
    }


    
    testMethod public static void testApproved() {
        DiscountApprovalTestHlp.createNSDGeoMapData();
        DiscountApproval.isTest = true;
        
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        System.assertEquals('Required', q.Discount_Status__c);
        
        User usr=[select Id,Name,email from User where Id=:q.RSM__c];
        q.Submitter__c = usr.Id;
        // Save the Submitter as code in DiscountApprovalEmail looks at prior value.
        //update q;
         
        q.Discount_Status__c = 'Pending Approval';
        q.Send_Email__c = DiscountApprovalEmailHlp.EmailType.APPROVED.name();       
        
        Test.startTest();
        System.runAs(usr) {
            DiscountApproval.isTest = true;         
           // update q;       
        }    
        Test.stopTest();               
    } 

    testMethod public static void testMoreInfo() {
        DiscountApprovalTestHlp.createNSDGeoMapData();
        DiscountApproval.isTest = true;
    
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        System.assert(q.Discount_Status__c == 'Required');      
        q.Discount_Status__c = 'Pending Approval';
        q.Send_Email__c = DiscountApprovalEmailHlp.EmailType.MORE_INFO.name();
        //update q;
    } 

    testMethod public static void testCancell() {
        DiscountApprovalTestHlp.createNSDGeoMapData();
        DiscountApproval.isTest = true;
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        system.assert(q.Discount_Status__c == 'Required');      
        q.Discount_Status__c = 'Pending Approval';
        q.Send_Email__c = DiscountApprovalEmailHlp.EmailType.CANCELLED.name();
        //update q;       
    } 
    
    testMethod public static void testReject() {
        DiscountApprovalTestHlp.createNSDGeoMapData();
        DiscountApproval.isTest = true;
    
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        system.assert(q.Discount_Status__c == 'Required');
        
         User usr = [SELECT Id, Name, Email FROM User WHERE  Id = :q.RSM__c];
         q.Submitter__c = usr.Id;
         // Save the Submitter as code in DiscountApprovalEmail looks at prior value.
         //update q;
         
         q.Discount_Status__c = 'Pending Approval';
         q.Send_Email__c = DiscountApprovalEmailHlp.EmailType.REJECTED.name();
         Test.startTest();
         system.runAs(usr){
            DiscountApproval.isTest = true;
            //update q;
         }   
         Test.stopTest();         
    } 
    
    testMethod public static void testReminder() {
        DiscountApprovalTestHlp.createNSDGeoMapData();
        DiscountApproval.isTest = true;
    
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //Opportunity opp=getOpportunity();
        //Quote__c q= DiscountApprovalTestHlp.createQuote(opp.Id);
        system.assert(q.Discount_Status__c == 'Required');      
         q.Discount_Status__c = 'Pending Approval';
         q.Send_Email__c = DiscountApprovalEmailHlp.EmailType.REMINDER.name();
         //update q;          
    } 
    
    
    testMethod public static void ProcessApplicantTest(){
        DiscountApproval.isTest = true;
        //Quote__c q=[select Id,Name from Quote__c limit 1];
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();        
        
        email.subject = q.Id;
        email.plainTextBody = DiscountApprovalEmail.QUOTE_DELIM + q.Id+'\n'+'Approve';
        envelope.fromAddress = 'user@nowhere.com';
        DiscountApprovalEmail catcher = new DiscountApprovalEmail();
        Messaging.InboundEmailResult result = catcher.handleInboundEmail(email, envelope);
        catcher.handleInboundEmail(email, envelope);
        
        Messaging.InboundEmail email1 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope1 = new Messaging.InboundEnvelope();
        //Quote__c q=[select Id,Name from Quote__c limit 1];
        email1.subject = q.Id;
        email1.plainTextBody = DiscountApprovalEmail.QUOTE_DELIM + q.Id+'\n'+'Reject';
        envelope1.fromAddress = 'user@nowhere.com';
        DiscountApprovalEmail catcher1 = new DiscountApprovalEmail();
        Messaging.InboundEmailResult result1 = catcher1.handleInboundEmail(email1, envelope1);
        catcher1.handleInboundEmail(email1, envelope1);
        
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope2 = new Messaging.InboundEnvelope();
        //Quote__c q=[select Id,Name from Quote__c limit 1];
        email2.subject = q.Id;
        email2.plainTextBody = DiscountApprovalEmail.QUOTE_DELIM + q.Id+'\n'+'More Info';
        envelope2.fromAddress = 'user@nowhere.com';
        DiscountApprovalEmail catcher2 = new DiscountApprovalEmail();
        Messaging.InboundEmailResult result2 = catcher2.handleInboundEmail(email2, envelope2);
        catcher2.handleInboundEmail(email2, envelope2);
        
        Messaging.InboundEmail email3 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope3 = new Messaging.InboundEnvelope();
        email3.subject = q.Id;
        //email3.plainTextBody = DiscountApprovalEmail.QUOTE_DELIM + q.Id+'\n'+'Approve';
        envelope3.fromAddress = 'user@nowhere.com';
        DiscountApprovalEmail catcher3 = new DiscountApprovalEmail();
        Messaging.InboundEmailResult result3 = catcher.handleInboundEmail(email3, envelope3);  
        catcher3.handleInboundEmail(email3, envelope3);
        
        Messaging.InboundEmail email4 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope4 = new Messaging.InboundEnvelope();
        //Quote__c q=[select Id,Name from Quote__c limit 1];
        email4.subject = q.Id;
        email4.plainTextBody =DiscountApprovalEmail.QUOTE_DELIM + q.Id+'\n'+ 'EOM';
        System.debug(LoggingLevel.INFO+'chcking body--'+email4.plainTextBody);
        envelope4.fromAddress = 'user@nowhere.com';
        DiscountApprovalEmail catcher4 = new DiscountApprovalEmail();
        //Messaging.InboundEmailResult result4 = catcher4.handleInboundEmail(email4, envelope4);
        //catcher4.handleInboundEmail(email4, envelope4);
        
        Messaging.InboundEmail email5 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope5 = new Messaging.InboundEnvelope();
        //Quote__c q=[select Id,Name from Quote__c limit 1];
        email5.subject = q.Id;
        email5.plainTextBody =DiscountApprovalEmail.QUOTE_DELIM + q.Id+'\n'+ 'EOM \n SENT FROM';
        System.debug(LoggingLevel.INFO+'chcking body--'+email4.plainTextBody);
        envelope5.fromAddress = 'user@nowhere.com';
        DiscountApprovalEmail catcher5 = new DiscountApprovalEmail();
        //Messaging.InboundEmailResult result5 = catcher5.handleInboundEmail(email5, envelope5);
        //catcher4.handleInboundEmail(email5, envelope5);
        
        Messaging.InboundEmail email6 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope6 = new Messaging.InboundEnvelope();
        //Quote__c q=[select Id,Name from Quote__c limit 1];
        email6.subject = q.Id;
        email6.plainTextBody =DiscountApprovalEmail.QUOTE_DELIM + q.Id+'\n'+ 'EOM \n FROM: RIVERBED DISCOUNT APPROVAL';
        System.debug(LoggingLevel.INFO+'chcking body--'+email4.plainTextBody);
        envelope6.fromAddress = 'user@nowhere.com';
        DiscountApprovalEmail catcher6 = new DiscountApprovalEmail();
        //Messaging.InboundEmailResult result6 = catcher6.handleInboundEmail(email6, envelope6);
        //catcher4.handleInboundEmail(email6, envelope6);       
    }
    
    
    public static User getApprover(Quote__c q){
        return [SELECT Email FROM User WHERE Id = :q.Approver1__c ];
    }
    
    //Added by Rucha (7/9/2013) for Low Value Deals
     testMethod public static void testLVApproved() {
        DiscountApprovalTestHlp.createNSDGeoMapData();
        DiscountApproval.isTest = true;         
        
        //Create low value quote for approval
        Quote__c lvq = DiscountApprovalTestHlp.createLowValueQuote(null);
        //System.assertEquals('Required', lvq.Discount_Status__c);
        
        User usr=[select Id,Name,email from User where Id=:lvq.RSM__c];
        lvq.Submitter__c = usr.Id;
        //update lvq;
        
        lvq.Submitter__c = usr.Id;
        lvq.Discount_Status__c = 'Pending Approval';
        lvq.Send_Email__c = DiscountApprovalEmailHlp.EmailType.APPROVED.name();
        
        Test.startTest();
            DiscountApproval.isTest = true;         
            DiscountApprovalTestHlp.createContactWithQueueEmail(lvq);
            //update lvq;
        Test.stopTest();
     }
     
     testMethod public static void testLVCancel(){
        DiscountApprovalTestHlp.createNSDGeoMapData();
        DiscountApproval.isTest = true;        
        Quote__c lvq = DiscountApprovalTestHlp.createLowValueQuote(null);
        //system.assert(lvq.Discount_Status__c == 'Required');    
        lvq.Discount_Status__c = 'Pending Approval';
        lvq.Send_Email__c = DiscountApprovalEmailHlp.EmailType.CANCELLED.name();
        //update lvq;
     }
    //
    
}