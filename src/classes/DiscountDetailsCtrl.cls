/* 
Class: DiscountDetailsCtrl
Purpose: Retrives DiscountDetails records based on Opportunity Id or Quote Id
Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 
*/
public without sharing class DiscountDetailsCtrl {

        public static final String BLANKET_RT = 'Blanket';  
        public static final String OPPT_PREFIX = '006';  
        public static final String QUOTE_PREFIX = 'a0o';
        public Boolean blanketSwitch {get;set;}
        public String cUSD {get;set;}
        public String percent {get;set;}
        public List<OppDiscountDetails> discountDetails {get;set;}
        public String recordId {get;set;}
        public Map<String,Decimal> competencyMap = new Map<String,Decimal>();
        public Map<String,Decimal> nonCompetencyMap = new Map<String,Decimal>();
        public Map<String,Decimal> competencyListPriceMap = new Map<String,Decimal>();
        public Map<String,Decimal> nonCompetencyListPriceMap = new Map<String,Decimal>();
        public Set<String> competencyCategorySet = new Set<String>();
        public Decimal SumTotalNetAmt {get;set;}
        public Decimal SumTotalListAmt {get;set;} 
        public DiscountDetailsCtrl(ApexPages.StandardController controller) {
            SumTotalNetAmt = 0.0;
            SumTotalListAmt = 0.0;
            Id blanketScheduleID = getRecordType(BLANKET_RT);
            discountDetails = new List<OppDiscountDetails>();
            recordId = controller.getRecord().Id;
            competencyMap = new Map<String,Decimal>();
            nonCompetencyMap = new Map<String,Decimal>();
            competencyListPriceMap = new Map<String,Decimal>();
            nonCompetencyListPriceMap = new Map<String,Decimal>();
            if(recordId == null){
                recordId = ApexPages.currentPage().getParameters().get('id');
            }
            getData(recordId);
        }
        public void getData(String recordId){
            Id blanketScheduleID = getRecordType(BLANKET_RT);
            if(recordId != null){
                // if Opportunity
                if(recordId.startsWith(OPPT_PREFIX)){
                    System.debug(LoggingLevel.INFO,'opty:');
                    Opportunity opportunity = getOpportunity(recordId);
                    cUSD  = 'USD';
                    percent = '%';
                    blanketSwitch = opportunity.Discount_Schedule__r.RecordTypeId == blanketScheduleID? true: false;
                    // get quote related to the opty with Use for Ordering checked.
                    Quote__c use4Ordering = new Quote__c();
                    Map<String,Decimal> quoteDetails = new Map<String,Decimal>();
                    try{
                        use4Ordering = [SELECT Id,name,PWS_Quote_Id__c FROM Quote__c WHERE Opportunity__c =: recordId AND Used_For_Ordering__c = true Limit 1];
                                        System.debug(LoggingLevel.INFO,'use4Ordering:'+use4Ordering);
                        if(use4Ordering != null){
                            quoteDetails = useForOrderingDetails(use4Ordering.Id);
                        }
                    }catch(Exception e){
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Quote On Opportunity'));
                    }
                    // create wraper class data for Opty
                    List<Discount_Detail__c> ddList = new List<Discount_Detail__c>();
                    try{
                        ddList = Database.query('SELECT Description__c,Opportunity__c, Name, IsOpp__c, Id, Full_Discount__c, Discount__c,'
                                                        +' Special_Discount__c,Quote__c,Quote__r.Name,Quote__r.PWS_Quote_Id__c,Uplift__c,Disti_Special_Discount__c,'
                                                        +' Category_Master__c,Category_Master__r.Name, Category_Master__r.Group__c'
                                                        +' FROM Discount_Detail__c WHERE Opportunity__c =: recordId Order By Category_Master__r.Name');
                    }catch(Exception e){
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Quote Lines On Quote'));
                    }
                    for(Discount_Detail__c dd: ddList){
                        //String disCompetencyName = dd.Description__c+'('+dd.Category_Master__r.Name+') - Competency Products';
                        String disCompetencyName = 'Y';
                        //String disName = dd.Description__c+'('+dd.Category_Master__r.Name+') -  No Competency Products';
                        String disName = 'N';
                        OppDiscountDetails recDataComp = new OppDiscountDetails(dd.Id,dd.Description__c,dd.Category_Master__r.Name,disCompetencyName,dd.Full_Discount__c,dd.Special_Discount__c,dd.Uplift__c,dd.Quote__c,dd.Quote__r.Name,dd.Quote__r.PWS_Quote_Id__c);
                        OppDiscountDetails recData = new OppDiscountDetails(dd.Id,dd.Description__c,dd.Category_Master__r.Name,disName,dd.Discount__c,dd.Special_Discount__c,dd.Uplift__c,dd.Quote__c,dd.Quote__r.Name,dd.Quote__r.PWS_Quote_Id__c);
                        System.debug(LoggingLevel.INFO,'quoteDetails:'+quoteDetails);
                       // if(quoteDetails != null && quoteDetails.isEmpty() && quoteDetails.containskey(dd.Category_Master__r.Name)){
                            recDataComp.stage6Link = use4Ordering.Id;
                            recDataComp.stage6QuoteName = use4Ordering.name;
                            recDataComp.stage6PWSQuoteID = use4Ordering.PWS_Quote_Id__c;
                            System.debug(LoggingLevel.INFO,'stage6Link:'+use4Ordering.Id);
                            System.debug(LoggingLevel.INFO,'stage6Discount:'+ quoteDetails.get(dd.Category_Master__r.Name));
                            recDataComp.stage6Discount = quoteDetails.get(dd.Category_Master__r.Name) != null ? quoteDetails.get(dd.Category_Master__r.Name) :0.00;
                            
                            recData.stage6Link = use4Ordering.Id;
                            recData.stage6QuoteName = use4Ordering.name;
                            recData.stage6PWSQuoteID = use4Ordering.PWS_Quote_Id__c;
                            System.debug(LoggingLevel.INFO,'stage6Link:'+use4Ordering.Id);
                            System.debug(LoggingLevel.INFO,'stage6Discount:'+ quoteDetails.get(dd.Category_Master__r.Name));
                            recData.stage6Discount = quoteDetails.get(dd.Category_Master__r.Name) != null ? quoteDetails.get(dd.Category_Master__r.Name) :0.00 ;
                        //}
                        discountDetails.add(recDataComp);
                        discountDetails.add(recData);
                    }                   
                    if(discountDetails.isEmpty()){
                        
                    }
                }// end of if for opty
                // if Quote
                if(recordId.startsWith(QUOTE_PREFIX)){
                    
                    discountDetails.clear();
                    System.debug(LoggingLevel.INFO,'discountDetails:' + discountDetails);
                    System.debug(LoggingLevel.INFO,'quote:');
                    Quote__c q = [SELECT Opportunity__c FROM Quote__c WHERE Id =: recordId];
                    Opportunity opportunity = getOpportunity(q.Opportunity__c);
                    cUSD  = 'USD';
                    percent = '%';
                    blanketSwitch = opportunity.Discount_Schedule__r.RecordTypeId == blanketScheduleID? true: false;
                    // get margin indicator
                    Map<String,Margin_Indicator__c> catMap=new Map<String,Margin_Indicator__c>();
                    catMap = getMarginIndicator();
                    // get Margin group info
                    Map<String,Decimal> marginGroupInfo = new Map<String,Decimal>();
                    marginGroupInfo = getMarginGroupInfo(recordId);
                    // get quote line items 
                    getQuoteLineItem(recordId);
                    // create wraper class data for Opty
                    List<Discount_Detail__c> ddList = new List<Discount_Detail__c>();
                    try{
                        ddList = DataBase.query('SELECT Description__c,Opportunity__c, Name, IsOpp__c, Id, Full_Discount__c, Discount__c,Special_Discount__c,'
                                                +' Quote__c,Quote__r.Name,Quote__r.PWS_Quote_Id__c,Uplift__c,Disti_Special_Discount__c,Category_Master__c,'
                                                +' Category_Master__r.Name,Category_Master__r.Group__c'
                                                +' FROM Discount_Detail__c WHERE Quote__c =: recordId' 
                                                +' AND IsOpp__c=False'
                                                +' AND Category_Master__r.Name IN: competencyCategorySet Order By Category_Master__r.Name');
                    }
                    catch(Exception e){
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Quote Lines On Quote'));
                    }
                     
                     System.debug(LoggingLevel.INFO,'ddList'+ ddList);
                    if(!ddList.isEmpty()){
                        for(Discount_Detail__c dd: ddList){
                            //String disCompetencyName = dd.Description__c+'('+dd.Category_Master__r.Name+') - Competency Products';
                            System.debug(LoggingLevel.INFO,'dd ******'+ dd.Category_Master__r.Name);
                            String disCompetencyName =  'Y';
                            //String disName = dd.Description__c+'('+dd.Category_Master__r.Name+') -  No Competency Products';
                            String disName = 'N';
                            Boolean vsoe = false;
                            System.debug(LoggingLevel.INFO,'dd retrived');
                            System.debug(LoggingLevel.INFO,'dd.Category_Master__r.Name:'+dd.Category_Master__r.Name+':dd.Disti_Special_Discount__c:'+dd.Disti_Special_Discount__c);
                            if(dd.Disti_Special_Discount__c != null)
                            {
                                vsoe = detailCategoryRange(dd.Category_Master__r.Name,dd.Disti_Special_Discount__c);
                            }else{
                                vsoe = detailCategoryRange(dd.Category_Master__r.Name,dd.Full_Discount__c);
                            }                       
                            System.debug(LoggingLevel.INFO,'adding data to the table in Quote 1');
                            
                            OppDiscountDetails recDataComp = new OppDiscountDetails(dd.Id,dd.Description__c,dd.Category_Master__r.Name,disCompetencyName,dd.Full_Discount__c,dd.Special_Discount__c,dd.Uplift__c,dd.Quote__c,dd.Quote__r.Name,dd.Quote__r.PWS_Quote_Id__c);
                            
                            recDataComp.distiSpecial = dd.Disti_Special_Discount__c != null ? dd.Disti_Special_Discount__c :0.00;
                            recDataComp.totalNetAmt = competencyMap.get(dd.Category_Master__r.Name) != null ? competencyMap.get(dd.Category_Master__r.Name) : 0.00;
                            
                            System.debug(LoggingLevel.INFO,'totalNetAmt in Quote 1' +  competencyMap.get(dd.Category_Master__r.Name));
                            
                            recDataComp.totalListPrice = competencyListPriceMap.get(dd.Category_Master__r.Name) != null ? competencyListPriceMap.get(dd.Category_Master__r.Name) : 0.00;
                            
                            System.debug(LoggingLevel.INFO,'totalListPrice in Quote 1' + competencyListPriceMap.get(dd.Category_Master__r.Name));
                            
                            //recDataComp.VSOE = (vsoe) ? 'Yes' : 'No'; 
                           
                            System.debug(LoggingLevel.INFO,'adding data to the table in Quote 2');
                            
                            OppDiscountDetails recData = new OppDiscountDetails(dd.Id,dd.Description__c,dd.Category_Master__r.Name,disName,dd.Discount__c,dd.Special_Discount__c,dd.Uplift__c,dd.Quote__c,dd.Quote__r.Name,dd.Quote__r.PWS_Quote_Id__c);
                            
                            recData.distiSpecial = dd.Disti_Special_Discount__c != null ? dd.Disti_Special_Discount__c :0.00;
                            recData.totalNetAmt = nonCompetencyMap.get(dd.Category_Master__r.Name) != null ? nonCompetencyMap.get(dd.Category_Master__r.Name) : 0.00;
                            
                            System.debug(LoggingLevel.INFO,'totalNetAmt in Quote 2' +  competencyMap.get(dd.Category_Master__r.Name));
                            
                            recData.totalListPrice = nonCompetencyListPriceMap.get(dd.Category_Master__r.Name) != null ? nonCompetencyListPriceMap.get(dd.Category_Master__r.Name) : 0.00;
                            
                            System.debug(LoggingLevel.INFO,'totalListPrice in Quote 2' + competencyListPriceMap.get(dd.Category_Master__r.Name));
                            
                            if(dd.Category_Master__r.Name.equalsIgnoreCase('B')|| dd.Category_Master__r.Name.equalsIgnoreCase('K')){
                                recDataComp.VSOE = (vsoe) ? 'Yes' : 'No'; 
                                recData.VSOE = (vsoe) ? 'Yes' : 'No'; 
                            }
                            else{
                                recDataComp.VSOE ='';
                                recData.VSOE = '';
                            }
                            // adding margin status to wrapper class
                            if(catMap.containskey(dd.Category_Master__r.Group__c) && marginGroupInfo.containsKey(dd.Category_Master__r.Group__c)){
                                recData.color = getMarginColor(marginGroupInfo.get(dd.Category_Master__r.Group__c),catMap.get(dd.Category_Master__r.Group__c));
                                recDataComp.color = getMarginColor(marginGroupInfo.get(dd.Category_Master__r.Group__c),catMap.get(dd.Category_Master__r.Group__c));
                            }
                            
                            discountDetails.add(recDataComp);
                            System.debug(LoggingLevel.INFO,'add(recDataComp):'+discountDetails);
                            discountDetails.add(recData);
                            System.debug(LoggingLevel.INFO,'add(recData):'+discountDetails);
                            SumTotalNetAmt += recDataComp.totalNetAmt  + recData.totalNetAmt;
                            SumTotalListAmt += recDataComp.totalListPrice + recData.totalListPrice;
                        }
                    }
                    System.debug(LoggingLevel.INFO,'discountDetails:'+discountDetails);
                }// end of if for quote
            }
        }
        public Map<String,Decimal> useForOrderingDetails(Id quoteId){
            // get discount details related to the quote
            Map<String,Decimal> quoteDetails = new Map<String,Decimal>();
            for(Discount_Detail__c dd: [SELECT Full_Discount__c,Category_Master__r.Name,Special_Discount__c FROM Discount_Detail__c 
                                            WHERE Quote__c =: quoteId]){
                if(dd.Special_Discount__c != null && dd.Special_Discount__c != 0){
                    quoteDetails.put(dd.Category_Master__r.Name,dd.Full_Discount__c);
                }else{
                    quoteDetails.put(dd.Category_Master__r.Name,dd.Special_Discount__c);
                }
            }
            System.debug(LoggingLevel.INFO,'quoteDetails:'+quoteDetails);
            return quoteDetails;
        }
        public Opportunity getOpportunity(Id oppId){
            return [SELECT Id,  Discount_Schedule__c,Discount_Schedule__r.RecordTypeId FROM Opportunity WHERE Id =: oppId];
        }
        public String getRecordType(String rTypeName){
            Map<String,Schema.RecordTypeInfo> ScheduleRTypeMap;
            Schema.DescribeSObjectResult disScheduleObject = Schema.SObjectType.Discount_Schedule__c;
            ScheduleRTypeMap = disScheduleObject.getRecordTypeInfosByName();
            Map<Id,String> aRecordType = new Map<Id,String>();
            Id rtId =  scheduleRTypeMap.containsKey(rTypeName)?ScheduleRTypeMap.get(rTypeName).getRecordTypeId():null;
            return rtId;
        }
        public void getQuoteLineItem(Id quoteId){
            competencyListPriceMap.clear();
            competencyMap.clear();
            System.debug(LoggingLevel.INFO,'competencyListPriceMap***' + competencyListPriceMap);
            System.debug(LoggingLevel.INFO,'competencyMap***' + competencyMap);
            for(Quote_Line_Item__c qli: [SELECT Id,Additional_Discount__c,Disti_Approved_Discount__c,Unit_Price__c,D_Unit_Price__c,Qty_Ordered__c,Category__c, D_Ext_Price__c,Unit_List_Price__c,Tot_List_Price__c FROM Quote_Line_Item__c WHERE Quote__c =: quoteId])
            {
                String key = qli.Category__c;
                System.debug(LoggingLevel.INFO,'key***' + key);
                System.debug(LoggingLevel.INFO,'qli***' + qli.ID);
                System.debug(LoggingLevel.INFO,'qli.Tot_List_Price__c***' + qli.Tot_List_Price__c);
                if(qli.Additional_Discount__c != null && qli.Additional_Discount__c != 0){
                    Decimal competencySum = 0;
                    Decimal competencyListSum = 0;
                    if(competencyMap.containsKey(key)){
                        
                        competencySum = competencyMap.get(key) + qli.D_Ext_Price__c;
                        
                        
                    }else{
                         
                        competencySum = qli.D_Ext_Price__c;
                         
                    }
                    competencyMap.put(key,competencySum);
                    System.debug(LoggingLevel.INFO,'competencyMap***' + competencyMap);
                    
                    if(competencyListPriceMap.containsKey(key)){
                        
                        competencyListSum = competencyListPriceMap.get(key) + qli.Tot_List_Price__c;
                        System.debug(LoggingLevel.INFO,'competencyListPriceMap.get(key)***' + competencyListPriceMap.get(key) + qli.Tot_List_Price__c);
                    
                    }else{
                       
                        competencyListSum = qli.Tot_List_Price__c;
                    }
                    competencyListPriceMap.put(key,competencyListSum);
                    System.debug(LoggingLevel.INFO,'competencyListPriceMap***' + competencyListPriceMap);
                
                }else{
                    
                    Decimal nonCompetencySum = 0;
                    Decimal nonCompetencyListSum = 0;
                    
                    nonCompetencySum = qli.D_Ext_Price__c != null ? qli.D_Ext_Price__c :0;
                    
                    nonCompetencyListSum = qli.Tot_List_Price__c != null ? qli.Tot_List_Price__c :0;
                    
                    System.debug(LoggingLevel.INFO,'nonCompetencySum***' + nonCompetencySum);
                    System.debug(LoggingLevel.INFO,'nonCompetencyListSum***' + nonCompetencyListSum);
                    if(nonCompetencyMap.containsKey(key)){
                        //nonCompetencySum += nonCompetencyMap.get(key);
                        nonCompetencySum = nonCompetencyMap.get(key) + nonCompetencySum;
                        System.debug(LoggingLevel.INFO,'nonCompetencyMap.get(key)***' + nonCompetencyMap.get(key) + nonCompetencySum);
                         
                    }
                    nonCompetencyMap.put(key,nonCompetencySum);
                    System.debug(LoggingLevel.INFO,'nonCompetencyMap***' + nonCompetencyMap);
                    if(nonCompetencyListPriceMap.containsKey(key)){
                        //nonCompetencyListSum += nonCompetencyMap.get(key);
                        nonCompetencyListSum = nonCompetencyListPriceMap.get(key) + nonCompetencyListSum;
                        System.debug(LoggingLevel.INFO,'nonCompetencyMap.get(key)***' + nonCompetencyMap.get(key) + nonCompetencySum);
                    }
                    nonCompetencyListPriceMap.put(key,nonCompetencyListSum);
                    System.debug(LoggingLevel.INFO,'nonCompetencyListPriceMap***' + nonCompetencyListPriceMap);
                }
                /*retrives a set of QuoteLineitem category*/
                if(qli.Category__c != null && qli.Category__c != ''){
                    competencyCategorySet.add(qli.Category__c);
                }
            }
            /*if(competencyCategorySet.isEmpty()){
                    // add apex page messages.
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Quote Lines added'));
            } */
        }
        /* getting detail category range */
        public static Boolean detailCategoryRange(String categoryName, Decimal specialDiscount){
            DiscountDetailCategoryRange__c ddRange = new DiscountDetailCategoryRange__c();
            ddRange = DiscountDetailCategoryRange__c.getValues(categoryName);
            String categoryRange = ddRange != null && ddRange.Range__c !='' && ddRange.Range__c != null ? ddRange.Range__c : '';
            Boolean flag = false;
            System.debug(LoggingLevel.INFO,'categoryRange***'+categoryRange);
            if(categoryRange != '' && categoryRange != null){
                System.debug(LoggingLevel.INFO,'inside****');
                String[] splitRange = new List<String>();
                splitRange = categoryRange.split('-');
                Decimal min = Decimal.valueOf(splitRange[0].trim());
                Decimal max = Decimal.valueOf(splitRange[1].trim());
                if(specialDiscount >= min && specialDiscount <= max){
                    flag = true;
                }
            }
            return flag;
        }
        /*Margin status */
        /* Margin Indicator map */
        public Map<String,Margin_Indicator__c> getMarginIndicator(){
            Map<String,Margin_Indicator__c> catMap=new Map<String,Margin_Indicator__c>();
            List<Margin_Indicator__c> mIndicatorList = [SELECT Category__c, Above_Margin_Maximum__c, Below_Margin_Minimum__c, Above_Margin__c, 
                                                        Below_Margin__c, Within_Margin__c, Show__c 
                                                    FROM Margin_Indicator__c];
            if(mIndicatorList!=null && mIndicatorList.size()>0){
                for(Margin_Indicator__c mIndicator:mIndicatorList){
                    catMap.put(String.valueOf(mIndicator.Category__c),mIndicator);
                }
            }
            return catMap;
        }
        public Map<String,Decimal> getMarginGroupInfo(Id quoteId){
        Map<String,Decimal> marginGroupInfo = new Map<String,Decimal>();// map of groupname=>margin        
        for(Margin_Summary__c temp:[SELECT Total_Value__c, Quote__c,Margin__c, Id, Group_Name__c, Cost__c,Extended_List_Price__c,
                                                Extended_Discount__c 
                                            FROM Margin_Summary__c WHERE Quote__c =:quoteId]){
            marginGroupInfo.put(temp.Group_Name__c,temp.Margin__c);
                   
        }   
        return marginGroupInfo;
     }
     /*Margin group is compared aganist the indicator*/
     public String getMarginColor(Decimal margin,Margin_Indicator__c indicator){
         String color = '';
                if(margin>=indicator.Above_Margin_Maximum__c && indicator.Show__c){
                       color=indicator.Above_Margin__c;                        
                 }else if(margin>=indicator.Below_Margin_Minimum__c 
                            && margin<indicator.Above_Margin_Maximum__c && indicator.Show__c){
                       color=indicator.Within_Margin__c;                        
                    }else if(margin>0 && margin<indicator.Below_Margin_Minimum__c 
                                && indicator.Show__c){
                       color=indicator.Below_Margin__c;                        
                }
            return color;         
     }
        /* Innser wrapper class*/
        public class OppDiscountDetails{
            public String discountCategoryId {get;set;}
            public String Competency {get;set;}
            public String description {get;set;} 
            public String CategoryMasterName {get;set;}
            public Decimal full_Blanket {get;set;}
            public Decimal approvedNSD {get;set;}
            public Decimal discountDiff {get;set;}
            public Decimal uplift {get;set;}
            public Decimal distiSpecial {get;set;}
            public Decimal totalNetAmt {get;set;}
            public Decimal totalListPrice {get;set;}
            public String quote {get;set;}
            public String quoteName {get;set;}
            public Decimal pwsQuoteID {get;set;}
            public String VSOE {get;set;}
            public String color{get;set;}
            public String stage6Link{get;set;}
            public String stage6QuoteName {get;set;} 
            public Decimal stage6PWSQuoteID {get;set;}
            public Decimal stage6Discount{get;set;}
            public OppDiscountDetails(String categoryId,String descr,String CategoryMsName,String Compt, Decimal fBlanket, Decimal NSD, Decimal uplift, String quote, String quoteName ,Decimal pwsQuoteId){
                this.discountCategoryId = categoryId;
                this.Competency = Compt;
                this.description=descr;
                this.CategoryMasterName=CategoryMsName;
                this.full_Blanket = fBlanket != null ? fBlanket :0.00;
                this.approvedNSD = NSD != null ? NSD : 0.00;
                this.discountDiff = (NSD != null && fBlanket != null) ? NSD - fBlanket : 0.00;
                this.uplift = uplift != null? uplift:0.00;
                this.quote = quote;
                this.quoteName = quoteName;
                this.pwsQuoteID = pwsQuoteID;
            }
        }
}