/**
* Controller class for visual force page -
* This controller will prepare the data for above page 
* to display opportunity discount related information
* 
* @auther Remy.Chen
* @date 10/17/2013
*/
public with sharing class OppDiscountSchedulesController {
    public List<OppDiscountScheduleWrapp> oppDSList {get;set;}
    public List<String> categoryNames {get;set;}
    public Set<String> tmpCategoryNames {get;set;}
    public Id oppId {get;set;}
    /**
    * constructor method to prepare data for the usage of visualforce page
    * - get value of opportunity discount schedule list from three object
    *   1, Opportunty_Discount_Schedule__c 2, Category_Master__c
    *   3, Discount_Detail__c
    * @param standard controller
    *
    */
    public OppDiscountSchedulesController(ApexPages.StandardController stdController) {
        //Initialize the variable 
        oppId = stdController.getId();
        
        oppDSList = new List<OppDiscountScheduleWrapp>();
        categoryNames = new List<String>();
        tmpCategoryNames = new Set<String>();
        Map<Id, Category_Master__c> tmpCategoriesMap = new Map<Id, Category_Master__c>();
        
        //Get the list of Opportunty_Discount_Schedule__c with specific opportunity Id
        List<Opportunty_Discount_Schedule__c> oppDiscountSchedules = [ SELECT Authorization_Specialization__c, Name, Opportunity__c, Discount_Schedule__c, Discount_Schedule__r.Name
                                                                       FROM Opportunty_Discount_Schedule__c 
                                                                       WHERE Opportunity__c =: oppId ];
        //Get the whole list of existing categories
        tmpCategoriesMap = new Map<Id, Category_Master__c> ( [ SELECT Id, Name 
                                                            FROM Category_Master__c 
                                                            ORDER BY Name ] );
        		//Get rid of cate name with -
		Map<Id, Category_Master__c> categoriesMap = new Map<Id, Category_Master__c>();	
		for(String cateId: tmpCategoriesMap.keySet()){
			if( !tmpCategoriesMap.get(cateId).name.contains('-')){
				categoriesMap.put(cateId, tmpCategoriesMap.get(cateId));
			}
		}							   			
		                                                    
        
        //Get the list of Discount Detail with specific opportunity Id
        List<Additional_Discount_Detail__c> addDisDetails = [ SELECT Id, Discount_Detail__c, Type__c, Value__c, Discount_Detail__r.Category_Master__c, 
                                                                  Discount_Detail__r.Name, Authorization_Discount_Schedule__c 
                                                           FROM Additional_Discount_Detail__c 
                                                           WHERE Discount_Detail__r.Opportunity__c =: oppId ];
                                                           
       List<Discount_Detail__c> disDetails = [ SELECT Id,Category_Master__c,Discount__c 
                                                           FROM Discount_Detail__c  
                                                           WHERE Opportunity__c =: oppId ];
        
        Map<String, Map<String, Decimal>> IdMap = new Map<String, Map<String, Decimal>>();
        Map<String, Decimal> cateSumMap = new Map<String, Decimal>();
        
        Map<String,List<Additional_Discount_Detail__c>> addDisScheMap = generateAddDisDetailMap(addDisDetails);
        Map<String, Decimal> baseCateMap = new Map<String, Decimal>();
        generateBaseCategoryMap(disDetails, categoriesMap,cateSumMap,baseCateMap);
        

        
        for(String authId: addDisScheMap.keySet()){
            Map<String, Decimal> cateMap = new Map<String, Decimal>();
            List<Additional_Discount_Detail__c> tmpList = addDisScheMap.get(authId);
            generateCategoryMap(tmpList, categoriesMap, cateSumMap,cateMap);
            IdMap.put(authId, cateMap);
        }
        
        //Base Discount Schedule
        OppDiscountScheduleWrapp oppBaseDSW = new OppDiscountScheduleWrapp();  			
		Opportunity tmpOpportunity = [SELECT Discount_Schedule__r.Id, Discount_Schedule__r.Name, Id FROM Opportunity WHERE Id = : oppId];
		if( String.isNotEmpty(tmpOpportunity.Discount_Schedule__r.Id)){
			oppBaseDSW.scheduleId = tmpOpportunity.Discount_Schedule__r.Id;
			oppBaseDSW.scheduleName = tmpOpportunity.Discount_Schedule__r.Name;
		} else {
			oppBaseDSW.scheduleName = 'Base Discount';	
		}
        oppBaseDSW.scheduleFor = 'Base';
        oppBaseDSW.cateValueList = baseCateMap;
        oppDSList.add(oppBaseDSW);
        
        //Generate List of OppDiscountScheduleWrapp for page display
        For(Opportunty_Discount_Schedule__c oppds: oppDiscountSchedules) {
            OppDiscountScheduleWrapp oppDSW = new OppDiscountScheduleWrapp();
            oppDSW.scheduleName = oppds.Discount_Schedule__r.Name;
            oppDSW.scheduleId = oppds.Discount_Schedule__c;
            oppDSW.scheduleFor = oppds.Authorization_Specialization__c;
            oppDSW.cateValueList = IdMap.get( oppds.Discount_Schedule__c );
            oppDSList.add(oppDSW);
        }
        
        //Last Record to show the total 
        OppDiscountScheduleWrapp oppDSW = new OppDiscountScheduleWrapp();
        oppDSW.scheduleName = 'Full Discount (Total)';
        oppDSW.scheduleFor = 'Total';
        oppDSW.cateValueList = cateSumMap;
        oppDSList.add(oppDSW);
        categoryNames.AddAll( tmpCategoryNames );
        categoryNames.sort();
    }
    
  
    
    /**
    * Generate base discount map - key of category name
    * and value of category base discount*/
    public void generateBaseCategoryMap(List<Discount_Detail__c> detailList,
    Map<Id, Category_Master__c> categoriesMap, 
    Map<String, Decimal> cateSumMap,
    Map<String, Decimal> cateMap){
        for( Discount_Detail__c detail: detailList){
            for( Id cateId: categoriesMap.keySet()){
                String name = categoriesMap.get(cateId).Name;
                Decimal sum = 0;
                if( cateSumMap.get( name ) != null){
                    sum = cateSumMap.get( name );
                }
                
                if( !tmpCategoryNames.contains(name) ){
                    tmpCategoryNames.add( name );
                }
                
                if(detail.Category_Master__c == cateId){
                    Decimal tmpValue = 0;
                    if( detail.Discount__c != null){
                        tmpValue = detail.Discount__c;
                    }
                    cateMap.put(name, tmpValue);
                    cateSumMap.put( name, tmpValue);
                } else {
                    if( cateMap.get(name) == null){
                        cateMap.put(name, 0);
                    }
                    cateSumMap.put( name, sum );
                }
            }
        }
    }
    
    /**
    * Generate additional discount map - key of category name
    * and value of category value
    */
    public void generateCategoryMap(List<Additional_Discount_Detail__c> detailList,
    Map<Id, Category_Master__c> categoriesMap, 
    Map<String, Decimal> cateSumMap,
    Map<String, Decimal> cateMap){
        for( Additional_Discount_Detail__c detail: detailList){
            for( Id cateId: categoriesMap.keySet()){
                String name = categoriesMap.get(cateId).Name;
                Decimal sum = 0;
                
                if( cateSumMap.get( name ) != null){
                    sum = cateSumMap.get( name );
                }
                
                if( !tmpCategoryNames.contains(name) ){
                    tmpCategoryNames.add( name );
                }
                
                if(detail.Discount_Detail__r.Category_Master__c == cateId){
                    Decimal tmpValue = 0;
                    if(detail.Value__c != null ){
                        tmpValue = detail.Value__c;
                    }
                    cateMap.put(name, tmpValue);
                    cateSumMap.put( name, sum + tmpValue );
                } else {
                    if( cateMap.get(name) == null){
                        cateMap.put(name, 0);
                    }
                    cateSumMap.put( name, sum);
                }
            }
        }
    }
    
    /**
    * Generate additional discount map - key of Authorization_Discount_Schedule__c
    * and value of Additional_Discount_Detail__c list
    */
    public Map<String,List<Additional_Discount_Detail__c>> generateAddDisDetailMap(List<Additional_Discount_Detail__c> detailList){
        Map<String,List<Additional_Discount_Detail__c>> m = new Map<String,List<Additional_Discount_Detail__c>>();
        
        Set<String> tmpIds = new Set<String>();
        for(Additional_Discount_Detail__c detail: detailList){
            if(!tmpIds.contains(detail.Authorization_Discount_Schedule__c)){
                tmpIds.add(detail.Authorization_Discount_Schedule__c);
            }
        }
        
        for(String tmpId: tmpIds){
            List<Additional_Discount_Detail__c> subDetailList = new List<Additional_Discount_Detail__c>();
            for(Additional_Discount_Detail__c detail: detailList){
                if( tmpId == detail.Authorization_Discount_Schedule__c){
                    subDetailList.add(detail);
                }
            }
            m.put(tmpId, subDetailList);
        }
        return m;
    }
    
    /**
    * Wrap class for define the object 
    *
    */
    public class OppDiscountScheduleWrapp {
        public String scheduleName {get;set;}
        public Id scheduleId {get;set;}
        public String scheduleFor {get;set;}
        public Map<String,Decimal> cateValueList {get;set;}
        
        public OppDiscountScheduleWrapp() {
            scheduleName = '';
            scheduleId = null;
            scheduleFor = '';
            cateValueList = new Map<String, Decimal>();
        }
    }
}