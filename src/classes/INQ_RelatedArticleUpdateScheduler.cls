global class INQ_RelatedArticleUpdateScheduler implements Schedulable {

	global Boolean isTesting;
	
	global INQ_RelatedArticleUpdateScheduler() {
		isTesting=false;
	}
	global INQ_RelatedArticleUpdateScheduler(Boolean testmode) {
		this();
		isTesting=testmode;
	}
	
    global void execute(SchedulableContext sc) {
    	INQ_RelatedArticleBatchUpdate b = isTesting ? new INQ_RelatedArticleBatchUpdate(175,isTesting)
    	                                            : new INQ_RelatedArticleBatchUpdate();
        ID myBatchJobID = database.executebatch(b,100);
    }
}