global class ConvertLeadWS {

	webservice static String convertLead(Id leadId) {
		System.debug('Start LeadConvert');
		Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(leadId);
		LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
	  	try {
		    Database.LeadConvertResult result = Database.convertLead(lc);
/*		  	if (result.isSuccess()) {
		        System.debug('Conversion succeeded.');
		      	System.debug('The new Account id is: ' + result.getAccountId());
		      	System.debug('The new Contact id is: ' + result.getContactId());
		        System.debug('The new Opportunity id is: ' + result.getOpportunityId());
		        //enable account as partner account.
		        Boolean isPartnerAccount = enablePartnerAccount(result.getAccountId());
		        if (isPartnerAccount) {
		        	enablePartnerUser(result.getContactId());
		        }
		    } else {
	        	System.debug('The conversion failed because: ' + result.getErrors());
	        	return result.getErrors()[0].getMessage();
		    }*/
		  	return result.getOpportunityId();
	  	} catch (Exception e) {
	    	System.debug('Exception encountered: ' + e.getMessage());
        	return e.getMessage();
	  	}
	}
	
/*	private static Boolean enablePartnerAccount(String accountId) {
		Account acc = new Account(id = accountId, isPartner = true);
		update acc;
		return true;
	}
	
	private static Boolean enablePartnerUser(String contactId) {
		try {
			//get the contact
			Contact c = [Select Id, firstName, lastName, email, title, phone from Contact where Id = :contactId];
			User u = new User(firstName = c.firstName, lastName = c.lastName, Alias = createAlias(c),
				email = c.Email, Username = c.Email, title = c.title, phone = c.phone,
				ProfileId = '00e80000000tXCwAAM',
				IsActive = true, ContactId = c.Id, 
				TimeZoneSidKey = 'America/Los_Angeles', 
				LocaleSidKey = 'en_US',
				EmailEncodingKey = 'ISO-8859-1', 
				LanguageLocaleKey = 'en_US');
			//CommunityNickname = createAlias(c), 
			insert u;
		} catch(Exception e) {
			throw e;	
		}
		return true;
	}
	
	private static String createAlias(Contact c) {
		String alias = c.firstName;
		if (alias.length() > 0) {
			alias.substring(0, 1);	
		}
		alias += c.lastName;
		return alias;
	}
*/	
	

	// This method takes a lead and converts it
	public static testmethod void testLeadConvertWS(){
		User user = [SELECT Id FROM User WHERE IsActive = TRUE LIMIT 1];
		Lead L = new Lead(
					LastName = 'testlead',
					Status = 'Open',
					LeadSource = 'Web',
					ownerid = user.id,
					Company = 'testCo',
					Email = 'testing123@abc.com',
					Phone = '4084088888'
		);
		insert L;
		convertLead(L.id);		
	}
		
}