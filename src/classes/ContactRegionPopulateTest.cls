/*Test Class for ContactRegionPopulate Trigger
*Code coverate test method > 90%
*Author:Prashant.singh@riverbed.com
*Date:08/12/2010
*Added test method for setLMSStatus.trigger - Ankita 9/20/2010
*/

@isTest
private class ContactRegionPopulateTest {
	
    static  testMethod void coverage(){
		RecordType rTypeAccount=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
		Account patAcc1=new Account();
        patAcc1.RecordTypeId=rTypeAccount.Id;
        patAcc1.name='stpAccount';
        patAcc1.Type='Distributor';
        patAcc1.Industry='Education';
        patAcc1.Geographic_Coverage__c='EMEA';
        patAcc1.Region__c='Central Europe';
        patAcc1.BillingCountry='';
        insert patAcc1;
        
        Account patAcc2=new Account();
        patAcc2.RecordTypeId=rTypeAccount.Id;
        patAcc2.name='tier2Account';
        patAcc2.Type='VAR';
        patAcc2.Industry='Education';
        patAcc2.Geographic_Coverage__c='Americas';
        patAcc2.Region__c='Latin America';
        patAcc2.BillingCountry='Argentina';
        insert patAcc2;
        
        Contact contact1=new Contact();
        RecordType rTypeContact=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Contact' and Name='Standard Contact'];
        contact1.FirstName='Testclass1';
        contact1.LastName='Account Region';
        contact1.RecordTypeId=rTypeContact.Id;
        contact1.AccountId=patAcc1.Id;
        insert contact1;
        
        contact1.AccountId=patAcc2.Id;
        update contact1;
	}  
	
	static testmethod void testContactLMSStatus(){
		User u = [select contact.accountid, contactid, id from User 
					where usertype = 'PowerPartner' and username like 'akram.mohammed@planet1world.com%'];
		LMS_Activity__c activity = new LMS_Activity__c(contact__c = u.contactId, account__c = u.contact.accountid,
									event_Date__c = Date.today(), event_type__c = 'Login', LMS_Status__c = 'Active', User_Id__c = u.id);
		insert activity;
		Contact c = [select id, LMS_Status__c from Contact where id = :u.ContactId];
		System.assertequals(activity.LMS_Status__c,c.LMS_Status__c);
	}
	
	static testmethod void testDuplicateActivity(){
		User u = [select contact.accountid, contactid, id from User 
					where usertype = 'PowerPartner' and username like 'akram.mohammed@planet1world.com%'];
		List<LMS_Activity__c> acitivityList = new List<LMS_Activity__c>();			
		LMS_Activity__c activity0 = new LMS_Activity__c(contact__c = u.contactId, account__c = u.contact.accountid,
									event_Date__c = Date.today(), event_type__c = 'Login', LMS_Status__c = 'Active', 
									User_Id__c = u.id, LMS_User_ID__c = 'akram.mohammed@planet1world.com');
		acitivityList.add(activity0);
		LMS_Activity__c activity1 = new LMS_Activity__c(contact__c = u.contactId, account__c = u.contact.accountid,
									event_Date__c = Date.today(), event_type__c = 'Registration', LMS_Status__c = 'Active', 
									User_Id__c = u.id, LMS_Item_Id__c = 'SVC-TRA-00112', LMS_User_ID__c = 'akram.mohammed@planet1world.com');
		acitivityList.add(activity1);
		LMS_Activity__c activity2 = new LMS_Activity__c(contact__c = u.contactId, account__c = u.contact.accountid,
									event_Date__c = Date.today(), event_type__c = 'Registration', LMS_Status__c = 'Active', 
									User_Id__c = u.id, LMS_Item_Id__c = 'SVC-TRA-00112', LMS_User_ID__c = 'akram.mohammed@planet1world.com');
		acitivityList.add(activity2);
		LMS_Activity__c activity3 = new LMS_Activity__c(contact__c = u.contactId, account__c = u.contact.accountid,
									event_Date__c = Date.today(), event_type__c = 'Completion', LMS_Status__c = 'Active', 
									User_Id__c = u.id, LMS_Item_Id__c = 'SVC-TRA-00112', LMS_User_ID__c = 'akram.mohammed@planet1world.com');
		acitivityList.add(activity3);
		try{			
			insert acitivityList;
		}catch(Exception e){
			System.assert(!e.getMessage().contains('DUPLICATE'),'Duplicate activity');
		}
	}
}