public with sharing class RCNCustomerReferenceTaskController 
{
        public Task t {get; set;}
        Private String CUSTOMER_REFERENCE_TASK = 'Customer Reference Task';
        private String NEW_TASK_URL = '/00T/e?nooverride=1';
        private String EDIT_TASK_URL = '/';
        Public String taskRecordTypeName;
        private String OPPORTUNITY_PREFIX = '006'; 
        private String retURL;     
        //ApexPages.Standardcontroller stdController;
        public Boolean isEmailSend {get;set;}
        String RType1 = 'Sales Request';
        public Opportunity temOpp {get;set;}
        public Lead temLead{get;set;}
        private String recordtypeId;
        private String RecordType;
        private String taskId;
        public List<selectOption> cStatus {get; set;}
        public String selectedtaskstatus {get; set;}
        public RCNCustomerReferenceTaskController(ApexPages.StandardController stdController)
        {
         temOpp = new opportunity();
         temLead = new Lead();
         this.t = (Task) stdController.getRecord();
         recordtypeId = t.RecordTypeId;
         //Get Return URL from the URL parameter
         retURL = Apexpages.currentPage().getParameters().get('retURL');
         System.Debug('retURL => '+retURL);
         
         if(t.Id != null)
         {
         	taskId = t.Id;
         	this.t = [SELECT Id, type, whatId, Customer_Reference_Contact__c, Customer_Reference_Contact_other__c, ActivityDate, Request_Type__c, Subject, OwnerId, Status, Description, CreatedDate, CreatedById, WhoId, RecordTypeId, RecordType.Name  FROM Task WHERE Id =: t.Id];
         	selectedTaskStatus = t.Status;
         	//Check if the RecordType is updated using "Change RecordType" Link.
         	if(Apexpages.currentPage().getParameters().get('RecordType') != null && Apexpages.currentPage().getParameters().get('RecordType') != '')
         	{
         		recordtypeId = Apexpages.currentPage().getParameters().get('RecordType');
         	}
         	if(t.RecordTypeId == recordtypeId)
         	taskRecordTypeName = t.RecordType.Name;
         	
         	if(t.Customer_Reference_Contact__c != null && t.Customer_Reference_Contact__c.length() == 18)
    		temOpp.Primary_Contact__c = t.Customer_Reference_Contact__c;
    		
    		if(t.Customer_Reference_Contact_other__c != null && t.Customer_Reference_Contact_other__c.length() == 18)
    		temLead.Sold_to_Partner_Contact__c = t.Customer_Reference_Contact_other__c;
    		
         	
         	EDIT_TASK_URL += t.Id;
         }
         
         System.Debug('recordtypeId => '+recordtypeId);
         System.Debug('WhoId => '+t.WhoId);
         System.Debug('WhatId => '+t.WhatId);
         if(t.WhatId != null)
         {
            String whatIdStr = t.WhatId;
            if(whatIdStr.startsWith(OPPORTUNITY_PREFIX))
            t.WhoId = null;
         }
         
         if(taskRecordTypeName == null)
         {
	         Schema.DescribeSObjectResult taskRTDescribe = Schema.SObjectType.Task;
	         Map<Id,Schema.RecordTypeInfo> taskRecTypeMap = taskRTDescribe.getRecordTypeInfosById();
	         Schema.RecordTypeInfo rtById = taskRecTypeMap.get(recordtypeId);
	         System.Debug('rtById => '+rtById);
	         if(rtById != null)
	         taskRecordTypeName = rtById.getName();  
         }              
         System.Debug('taskRecordTypeName => '+taskRecordTypeName);
         
        
    }
   
    public PageReference init()
    {
        PageReference pr1;
        PageReference pr2;
        
        if(t.Id != null)
        {
        	EDIT_TASK_URL += '/e?nooverride=1';
        	EDIT_TASK_URL += '&RecordType='+recordtypeId;
        }
        if(t.WhatId != null)
        {
        	NEW_TASK_URL += '&what_id='+t.WhatId; 
       		EDIT_TASK_URL += '&what_id='+t.WhatId;
        }
        if(t.WhoId != null)
        {
        	NEW_TASK_URL += '&who_id='+t.WhoId;
        	EDIT_TASK_URL += '&who_id='+t.WhoId;
        }
        //
        if(retURL != null)
        {
        	NEW_TASK_URL += '&retURL='+retURL;
        	EDIT_TASK_URL += '&retURL='+retURL;
        }
        if(t.Id != null)
        {
        	pr2=new PageReference(EDIT_TASK_URL);
        }
        else
        {
        		if(!Userinfo.getUserType().equals('PowerPartner')){
        	NEW_TASK_URL += '&RecordType='+recordtypeId;
        		}
        	pr2=new PageReference(NEW_TASK_URL);
        }
        
        if(CUSTOMER_REFERENCE_TASK ==taskRecordTypeName)
        {
                 /*pr1 = new PageReference('/Apex/RCNCustomerReftaskForMarketing?RecordType='+recordtypeId+'&what_id='+t.WhatId+'&who_id='+t.WhoId+'&taskId='+t.Id);
                 pr1.setRedirect(true);
                 return pr1;*/
                 pr1 = null;
           t.OwnerId = UserInfo.getUserId();
             
             //Get Task Status List
             getStatus();
             System.Debug('Task Status List => '+cStatus);
             System.Debug('selectedtaskstatus => '+selectedtaskstatus);
             return pr1;
       }
        
        return Pr2;
    }
  
   public PageReference doSave() {
        
        System.debug('Opp Contact ==>>>' + temOpp.Primary_Contact__c);
        System.debug('Lead Contact ==>>> ' + temLead.Sold_to_Partner_Contact__c);
        try
        {
	        if(taskId != null)
	        {
	        	t.Status = selectedtaskstatus;
	        	t.RecordTypeId = recordtypeId;
	        	
	        	System.debug('###temOpp.Primary_Contact__c' + temOpp.Primary_Contact__c);
		        System.debug('###temLead.Sold_to_Partner_Contact__c' + temLead.Sold_to_Partner_Contact__c);
		        //if(temOpp.Primary_Contact__c != null)
	        	t.Customer_Reference_Contact__c = temOpp.Primary_Contact__c;
	        	//if(temLead.Sold_to_Partner_Contact__c != null)
	        	t.Customer_Reference_Contact_other__c = temLead.Sold_to_Partner_Contact__c;
	        	update t;
	        }
	        else
	        {
		        if(temOpp.Primary_Contact__c != null)
		        {
			        /*contact c = [select id ,name from contact where id =:temOpp.Primary_Contact__c];
			        t.Customer_Reference_Contact_other__c=c.name;*/
			        t.Customer_Reference_Contact__c = temOpp.Primary_Contact__c;
			        System.debug('###t.Customer_Reference_Contact_other__c' + t.Customer_Reference_Contact_other__c);
		        }
		        if(temLead.Sold_to_Partner_Contact__c != null)
		        {
		                /*Contact lc = [select id,name from Contact where id=:temLead.Sold_to_Partner_Contact__c];
		                t.Customer_Reference_Contact__c=lc.name;*/
		                t.Customer_Reference_Contact_other__c = temLead.Sold_to_Partner_Contact__c;
			        	System.debug('###t.Customer_Reference_Contact__c ' + t.Customer_Reference_Contact__c); 
		        }
		        t.RecordTypeId = recordtypeId;
		        t.RecordTypeId= ApexPages.currentPage().getParameters().get('RecordType');
		        t.status = selectedtaskstatus;
		        //Insert t;
		        Database.Saveresult sr=Database.insert(t);
		        if(sr.isSuccess() && isEmailSend){
		        	//send email
		        	string email=[select email from user where id=:t.ownerId].email;
		        	String[] toAddresses = new String[]{email};
		        	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		        	mail.setToAddresses(toAddresses);
				    mail.setSubject('A New Customer Reference Task created for you');
				    mail.setBccSender(false);
				    mail.setUseSignature(false);
				    mail.setSaveAsActivity(true);
				    mail.setPlainTextBody('A new Task: ' + t.Id +' has been created.');
				    if(mail!=null){
			           try{
			               Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
			           }catch(Exception e){
			           	system.debug('Exception:'+e);
			           }
			       }
		        }
	        }
        }
        catch(Exception e)
        {
        	System.debug('Exception ==>> ' + e.getMessage());
        }
        System.debug('###New Task' + T.recordtypeId);
        if(retURL != null && retURL != '')
        return new PageReference(retURL);
        else
        return new PageReference('/home/home.jsp');
        }
        public List<selectOption> getTypes() {
        List<selectOption> MktOptions = new List<selectOption>();
        List<selectOption> SlsOptions = new List<selectOption>();
        MktOptions.add(new selectOption('', 'None'));
        MktOptions.add(new selectOption('Analyst call', 'Analyst call'));
        MktOptions.add(new selectOption('Case study', 'Case study'));
        MktOptions.add(new selectOption('Launch', 'Launch'));
        MktOptions.add(new selectOption('Media', 'Media'));
        MktOptions.add(new selectOption('Press release', 'Press release'));
        MktOptions.add(new selectOption('Speaker', 'Speaker'));
        MktOptions.add(new selectOption('Video', 'Video'));
        MktOptions.add(new selectOption('Web Seminar', 'Web Seminar'));
        MktOptions.add(new selectOption('Other', 'Other'));
        SlsOptions.add(new selectOption('None', 'None'));
        SlsOptions.add(new selectOption('Reference Call', 'Reference Call'));
        SlsOptions.add(new selectOption('Case Study/One Slide', 'Case Study/One Slide'));
       
       If(t.Request_Type__c=='Sales Request')
       {
       return SlsOptions;
       }
       return MktOptions;
       }
       Public list<selectOption> getStatus(){
       cStatus = new List<selectOption>();
        if(taskId != null)
	        selectedtaskstatus = t.Status;
	    else
        selectedTaskStatus = 'Not Started';
        cStatus.add(new selectOption('Not Started', 'Not Started'));
        cStatus.add(new selectOption('In Progress', 'In Progress'));
        cStatus.add(new selectOption('Completed', 'Completed'));
        cStatus.add(new selectOption('Waiting on someone else', 'Waiting on someone else'));
        cStatus.add(new selectOption('Deferred', 'Deferred'));
        cStatus.add(new selectOption('Reference - Unavailable', 'Reference - Unavailable'));
        
        return cStatus;
        
    }
            
}