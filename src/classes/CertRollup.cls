/*
	Class: CertRollup
	TriggerAction: After update
	Purpose: to roll up certifications from child to parent and grand parent account in account hirearchy.
				Note: this class takes care of only three levels of account hirearchy
	Author: Jaya ( Perficient )
	Created Date: 7/21/2014					
	Reference: pc=> Partner Competency		 
*/
public without sharing class CertRollup 
{
	public static Boolean ROLL_UP = false;
	public static void rollupCert(Map<Id,Acct_Cert_Summary__c> oldPCMap, List<Acct_Cert_Summary__c> newPCList)
	{
		Set<Id> allAccountIdsSet = new Set<Id>();// includes current pc account, account.Parent, account.Parent.Parent ids
		//Set<String> allPCTAP = new Set<String>();// all Taps
		
		Map<String,Acct_Cert_Summary__c> allPartnerCompetencyMap = new Map<String,Acct_Cert_Summary__c>(); // each account[parent and grand parent] with the tap and its pc record
		Map<Id,Acct_Cert_Summary__c> currentPartnerCompetencyMap = new Map<Id,Acct_Cert_Summary__c>();// pc id and pc record
		Map<String,Acct_Cert_Summary__c> rollupCertsMap = new Map<String,Acct_Cert_Summary__c>();// pc records with the updated data
		Set<Id> validRecordIds = new Set<Id>();
		Id PartnerRecordType = getRecordType(Constant.ACCOUNT_PARTNER);
		String query = 'SELECT Id,Account__c,RSA__c,RSS__c,RTSA__c,RTSS__c,RCSA__c,RCSP__c,RCSA_RCSP__c, TAP__c,Account__r.RecordTypeId,'
						+' Roll_Up_RSS__c,Roll_Up_RSA__c,Roll_Up_RTSS__c,Roll_Up_RTSA__c,Roll_Up_RCSA__c,Roll_Up_RCSP__c,Roll_Up_RCSA_RCSP__c, '
						+' Account__r.ParentId, Account__r.Parent.Do_not_rollup_Certs__c, Account__r.Parent.Do_not_share_Competencies__c, '
						+' Account__r.Parent.Parent.Do_not_share_Competencies__c, Account__r.Parent.Parent.Do_not_rollup_Certs__c '; 
		// validate the code for cert count change
		validRecordIds = validateRecords(oldPCMap, newPCList);
		if(!validRecordIds.isEmpty()){
			String extquery = query +' FROM Acct_Cert_Summary__c WHERE Id IN: validRecordIds AND Account__r.RecordTypeId =: partnerRecordType';
								//+' Account__r.Parent.Do_not_Auto_Level__c, Account__r.Parent.Parent.Do_not_Auto_Level__c '
					System.debug('extquery:'+extquery);
			for(Acct_Cert_Summary__c pc : Database.query(extquery))
			{
				allAccountIdsSet.add(pc.Account__c);
				if(pc.Account__r.ParentId != null){
					allAccountIdsSet.add(pc.Account__r.ParentId);
					if(pc.Account__r.Parent.ParentId != null){
						allAccountIdsSet.add(pc.Account__r.Parent.ParentId);
					}
				}
				/*if(pc.TAP__c != null)
				{
					allPCTAP.add(pc.TAP__c);
				}*/
				currentPartnerCompetencyMap.put(pc.Id,pc);
			}
			//allHirearchyAcctIds = getAllAccounts(allAccountIdsSet);
			//allPartnerCompetencyMap = getAllPartnerCompetency(allHirearchyAcctIds);
			System.debug('allAccountIdsSet:'+allAccountIdsSet);
			String allPCquery = query + ' FROM Acct_Cert_Summary__c WHERE Account__c IN: allAccountIdsSet AND Account__r.RecordTypeId =: partnerRecordType';
			List<Acct_Cert_Summary__c> allPartnerCompetency = new List<Acct_Cert_Summary__c>();
			allPartnerCompetency = Database.query(allPCquery);
			allPartnerCompetencyMap = getAllPartnerCompetency(allPartnerCompetency);
			System.debug('allPartnerCompetencyMap:'+allPartnerCompetencyMap);
			//updAccount=new Account(Id=aAuth.Account__c,Authorizations_Specializations__c=authSpe,Allowed_Product_Families__c=allowedProductFamily,TAPs__c=authSpe);
			// updating the roll up certs
			for(Acct_Cert_Summary__c pc: currentPartnerCompetencyMap.values())
			{
				Acct_Cert_Summary__c oldChildPC = oldPCMap.get(pc.Id);
				String key = '';
				System.debug('pc:'+pc);
				// if current pc record exists in the rollupcerts then update their rollup fields with their cert values
				key = pc.TAP__c+'|'+pc.Account__c;
				Acct_Cert_Summary__c currentPC = rollupCertsMap.containsKey(key) ? rollupCertsMap.get(key):  allPartnerCompetencyMap.containsKey(key)?allPartnerCompetencyMap.get(key):null;
				if(currentPC != null){
					currentPC = createUpdatedCertRollup(currentPC,pc,oldChildPC);
					rollupCertsMap.put(key,currentPC);
				}
				// add certs to roll up too
				// Parent level 
				// && !pc.Account__r.Parent.Do_not_Auto_Level__c is removed
				if(pc.Account__r.ParentId != null && !pc.Account__r.Parent.Do_not_rollup_Certs__c )
				{
					key = pc.TAP__c+'|'+pc.Account__r.ParentId;
					System.debug('parent key:'+key);
					Decimal parentOldRollRss = 0;
					Acct_Cert_Summary__c pcOfParent = rollupCertsMap.containsKey(key)?
													 rollupCertsMap.get(key):
													 allPartnerCompetencyMap.containsKey(key)?allPartnerCompetencyMap.get(key):null;
					// parent level updating
					if(pcOfParent != null){
						Acct_Cert_Summary__c updatedPCParent = new Acct_Cert_Summary__c();
						 pcOfParent = createUpdatedCertRollup(pcOfParent,pc,oldChildPC);
						 rollupCertsMap.put(key,pcOfParent);
						 System.debug('rollupCertsMap:'+rollupCertsMap);
					}
					// grand parent level updating
					// && !pc.Account__r.Parent.Parent.Do_not_Auto_Level__c
					if(pc.Account__r.Parent.ParentId != null && !pc.Account__r.Parent.Parent.Do_not_rollup_Certs__c )
					{
						String gpkey = pc.TAP__c+'|'+pc.Account__r.Parent.ParentId;
						Acct_Cert_Summary__c pcOfgrandParent = rollupCertsMap.containsKey(gpkey)?
													 rollupCertsMap.get(gpkey):
													 allPartnerCompetencyMap.containsKey(gpkey)?allPartnerCompetencyMap.get(gpkey):null;
						if(pcOfgrandParent != null){
							
							pcOfgrandParent = createUpdatedCertRollup(pcOfgrandParent,pc,oldChildPC);
						 	rollupCertsMap.put(gpkey,pcOfgrandParent);
						 	System.debug('rollupCertsMap:'+rollupCertsMap);
						}
					}
				}
				System.debug('rollupCertsMap:'+rollupCertsMap);
			}
			if(!rollupCertsMap.isEmpty())
			{
				List<Acct_Cert_Summary__c> updatePCs = rollupCertsMap.values();
				//update updatePCs;// this needs to be changed to database method.
				System.debug('updatePCs:'+updatePCs);
				Database.SaveResult[] updateResults = Database.update(updatePCs,false);
	            List<Database.Error> dbErrors = new List<Database.Error>();
	            List<Error_Log__c> errorLogsList = new List<Error_Log__c>();
	            // Process the save results
	            Integer i = 0;
				for(Database.SaveResult sr: updateResults)
	            {
	                if(!sr.isSuccess())
	                {
	                     Database.Error dbErr = sr.getErrors()[0];
	                     Error_Log__c err = CompetencyRollDown.logError(updatePCs.get(i).Id,dbErr.getMessage(),'CertRollup class','');//Error_Cause__c = dbErr.getMessage(),
	                     errorLogsList.add(err);
	                 	updatePCs[i].addError(dbErr.getMessage(),false);    
	                }
	                i++;
	            }
	            if(!errorLogsList.isEmpty()){
	                insert errorLogsList;
	            }
				
			}
		}
	}
	/*object creation
	*/
	public static Acct_Cert_Summary__c createUpdatedCertRollup(Acct_Cert_Summary__c obj, Acct_Cert_Summary__c newObj, Acct_Cert_Summary__c oldObj)
	{
		Acct_Cert_Summary__c pcSummary = new Acct_Cert_Summary__c();
		if(newObj.RSS__c != oldObj.RSS__c){
			obj.Roll_Up_RSS__c = valueCompression(obj.Roll_Up_RSS__c,newObj.RSS__c,oldObj.RSS__c);
		}
		if(newObj.RSA__c != oldObj.RSA__c){
			obj.Roll_Up_RSA__c = valueCompression(obj.Roll_Up_RSA__c,newObj.RSA__c,oldObj.RSA__c);
		}
		if(newObj.RTSA__c != oldObj.RTSA__c){
			obj.Roll_Up_RTSA__c = valueCompression(obj.Roll_Up_RTSA__c,newObj.RTSA__c,oldObj.RTSA__c);
		}
		if(newObj.RTSS__c != oldObj.RTSS__c){
			obj.Roll_Up_RTSS__c = valueCompression(obj.Roll_Up_RTSS__c,newObj.RTSS__c,oldObj.RTSS__c);
		}
		if(newObj.RCSA__c != oldObj.RCSA__c){
			obj.Roll_Up_RCSA__c = valueCompression(obj.Roll_Up_RCSA__c,newObj.RCSA__c,oldObj.RCSA__c);
		}
		if(newObj.RCSP__c != oldObj.RCSP__c){
			obj.Roll_Up_RCSP__c = valueCompression(obj.Roll_Up_RCSP__c,newObj.RCSP__c,oldObj.RCSP__c);
		}
		if(newObj.RCSA_RCSP__c != oldObj.RCSA_RCSP__c){
			obj.Roll_Up_RCSA_RCSP__c = valueCompression(obj.Roll_Up_RCSA_RCSP__c,newObj.RCSA_RCSP__c,oldObj.RCSA_RCSP__c);
		}
		return (new Acct_Cert_Summary__c(Id = obj.Id, Roll_Up_RSS__c = obj.Roll_Up_RSS__c,
												Roll_Up_RSA__c = obj.Roll_Up_RSA__c, Roll_Up_RTSA__c = obj.Roll_Up_RTSA__c,
 												Roll_Up_RTSS__c = obj.Roll_Up_RTSS__c,Roll_Up_RCSA__c = obj.Roll_Up_RCSA__c,
 												Roll_Up_RCSP__c = obj.Roll_Up_RCSP__c,Roll_Up_RCSA_RCSP__c = obj.Roll_Up_RCSA_RCSP__c));
 		
	}
	/*old and new value compression
	* compares old and new values of teh certs and increments or decrements the roll up based on the change
	*/
	public static Decimal valueCompression(Decimal rollupValue, Decimal newValue, Decimal oldValue)
	{
		if(newValue != null && oldValue != null && newValue > oldValue){
							rollupValue = rollupValue != null && rollupValue != 0 ? rollupValue + (newValue- oldValue) : (newValue - oldValue);
		}
		if(newValue != null && oldValue != null && newValue < oldValue){
			//rollupValue = rollupValue != null && rollupValue != 0 ? rollupValue - (oldValue - newValue) : 0;
			rollupValue = rollupValue != null ? rollupValue - (oldValue - newValue) : 0;
		}
		if(newValue == null && oldValue != null){
			//rollupValue = rollupValue != null && rollupValue != 0 ? rollupValue - (oldValue) : 0;
			rollupValue = rollupValue != null ? rollupValue - (oldValue) : 0;
		}
		if(newValue != null && oldValue == null){
			rollupValue = rollupValue != null && rollupValue != 0 ? rollupValue + (newValue) : (newValue);
		}
		System.debug('rollupValue:'+rollupValue);
		return rollupValue;
		
	}
	/*Validate every record to check if any on the cert field values have changed*/
	public static Set<Id> validateRecords(Map<Id,Acct_Cert_Summary__c> oldPCMap, List<Acct_Cert_Summary__c> newPCList)
	{
		Set<Id> validRecordIds = new Set<Id>();
		for(Acct_Cert_Summary__c pc: newPCList)
		{
			Acct_Cert_Summary__c oldRec = oldPCMap.get(pc.Id);
			if(pc.RSS__c != oldRec.RSS__c || pc.RSA__c != oldRec.RSA__c || pc.RTSA__c != oldRec.RTSA__c
				|| pc.RTSS__c  != oldRec.RTSS__c || pc.RCSA__c  != oldRec.RCSA__c 
				|| pc.RCSP__c  != oldRec.RCSP__c || pc.RCSA_RCSP__c  != oldRec.RCSA_RCSP__c){
				validRecordIds.add(pc.Id);
			}
		}
		return validRecordIds;
	}
	/*
		Retrive all Partner Competency records for the grand parent, parent and child records.
		Create a map with the competency tap+account with the partner competency
	*/
	public static Map<String,Acct_Cert_Summary__c> getAllPartnerCompetency(List<Acct_Cert_Summary__c> allPartnerCompetency)
	{
		Map<String,Acct_Cert_Summary__c> allPartnerCompetencyMap = new Map<String,Acct_Cert_Summary__c>();
		Id partnerRecordType = getRecordType(Constant.ACCOUNT_PARTNER);
		for(Acct_Cert_Summary__c pc: allPartnerCompetency)
		{
			//String key = pc.TAP__c+'|'+pc.Account__c+'|'+pc.Id;
			if(pc.Account__c != null && pc.Account__r.RecordTypeId == PartnerRecordType){
				String key = pc.TAP__c+'|'+pc.Account__c;
				allPartnerCompetencyMap.put(key,pc);
			}
		}
		return allPartnerCompetencyMap;
	}
	/*
		retrives record type of the account
	*/
	public static Id getRecordType(String rTypeName)
	{
		Map<String,Schema.RecordTypeInfo> acctRTypeMap;
        Schema.DescribeSObjectResult acctsObject = Schema.SObjectType.Account;
        acctRTypeMap = acctsObject.getRecordTypeInfosByName();
        Map<Id,String> aRecordType = new Map<Id,String>();
        Id rtId =  acctRTypeMap.containsKey(rTypeName)?acctRTypeMap.get(rTypeName).getRecordTypeId():null;
        return rtId;
	}
	public static Decimal isNullCheck(Decimal value){
		if(value == null)
			return 0;
		else
			return value;
	}
	// Create or Update Account Authorization records based on the partner competency Status
	/*public static void accountAuthRecordUpsert(Map<Id,Acct_Cert_Summary__c> newPCMap){
		Map<String, Id> certSummaryIdsReqSatisfied = new Map<String,Id>();// status active pc
		Map<String, Id> certSummaryIdsFailedReqCriteria = new Map<String,Id>();// status inactive pc
		for(Acct_Cert_Summary__c pc: newPCMap.values()){
			String key = pc.TAP__c+pc.Id;
			if(pc.Status_Tap__c == 'Yes'){
				certSummaryIdsReqSatisfied.put(key,pc.Id);
			}else if(pc.Status_Tap__c == 'No'){
				certSummaryIdsFailedReqCriteria.put(key,pc.Id);
			}
		}
		 list<Account_Authorizations__c> accountAuthorizationList = CertificationSummaryTriggerHandler.getAccountAuthorizationsToUpsert(certSummaryIdsReqSatisfied,certSummaryIdsFailedReqCriteria,newPCMap);
			 System.debug('accountAuthorizationList:'+accountAuthorizationList);
			 if(!accountAuthorizationList.isEmpty()){
			 	upsert accountAuthorizationList;
			 }
	}*/
}