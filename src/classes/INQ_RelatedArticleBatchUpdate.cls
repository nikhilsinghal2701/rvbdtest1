global class INQ_RelatedArticleBatchUpdate implements Database.Batchable<sObject>,  Database.AllowsCallouts, Database.Stateful {
	
	class InfoManagerDocDetail {
		public String id = null;
		public String title = null;
		public String author = null;
		public String owner = null;
		public String version = null;
		public String lastmodified = null;
		public String createdate = null;
		public String authorid = null;
		public InfoManagerDocDetail() {}
		/*public InfoManagerDocDetail(String idArg, String titleArg, String authorArg, String ownerArg, String versionArg, String lastmodifiedArg) {
			id = idArg;
			title = titleArg;
			author = authorArg;
			owner = ownerArg;
			version = versionArg;
			lastmodified = lastmodifiedArg;
		}*/
	}

	class InfoManagerDocInfo {
		public List<InfoManagerDocDetail> docinfo;
		public InfoManagerDocInfo() {
			docinfo = new List<InfoManagerDocDetail>();
		}
	}
	
	global final String endpointURL;
	global final String endpointUsername;
	global final String endpointPassword;
	global final String endpointSecret;
	global final Integer endpointQueryLimit;
	
	global String query;
	global Boolean isTesting;
	
	global INQ_RelatedArticleBatchUpdate() {
		
		INQ_REST_Summary__c settings = INQ_REST_Summary__c.getInstance('INQ_REST_Summary_Settings');
		endpointURL = settings.Endpoint_URL__c;
		endpointUsername = settings.Endpoint_Username__c;
		endpointPassword = settings.Endpoint_Password__c;
		endpointSecret = settings.Endpoint_Secret__c;
		endpointQueryLimit = 100;
		isTesting = false;
		
		query = 'SELECT Id,Document_Id__c,Current_Version__c,Article_Author__c,Article_Last_Modified__c,Title__c,Display_URL__c,AuthorId__c,Article_Created_Date__c ' +
		          'FROM InQuira_Article_Info__c ' +
		          'WHERE Article_Type__c=\'IM\'';
	}
	
	global INQ_RelatedArticleBatchUpdate(Integer limiter, Boolean testflag) {
		this();
		query = query + ' LIMIT ' + limiter;
		isTesting = testflag;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		Integer start = 0;
		while (start<scope.size()) {
			handleRequest(scope, start);
			start += endpointQueryLimit; 
		}
		update scope;   // turn this on, disabled so we can test without updating records
	}
	
	global void finish(Database.BatchableContext BC) {
	}
	
	
	
	private HttpRequest buildRequest(String docids) {
		HttpRequest req = new HttpRequest();
		req.setHeader('Request-Service-User', endpointUsername);
		req.setHeader('Request-Service-Pass', endpointSecret + ';' + endpointPassword);
		req.setMethod('GET');
		req.setTimeout(20000);
		req.setEndpoint(endpointURL + (endpointURL.indexOf('?')>0 ? '&docids=' : '?docids=') + EncodingUtil.urlEncode(docids,'UTF-8'));
		return req;
	}
	
	private void updateArticleInfo(Map<String,InQuira_Article_Info__c> docidMap, InfoManagerDocInfo articleData) {
		InQuira_Article_Info__c article;
		String docid;
		for (InfoManagerDocDetail d : articleData.docinfo) {
			docid = d.id;
			article = docidMap.get(docid);
			article.Title__c = d.title;
			article.Article_Author__c = d.author;
			article.Current_Owner__c = d.owner;
			article.Current_Version__c = d.version;
			article.Article_Last_Modified__c = DateTime.valueOf(d.lastmodified);
			article.AuthorId__c = d.authorid;
			article.Article_Created_Date__c = DateTime.valueOf(d.createdate);
								
			//System.debug('AUTHORID' + article.AuthorId__c + ' ' + d.authorid); 
			//System.debug('CREATEDDATE' + article.Article_Created_Date__c + ' ' + d.createdate);  // TODO: remove after this works
		}
	}
	
	private void handleRequest(List<sObject> scope, Integer startPos) {
		Integer length = Math.min(scope.size()-startPos,endpointQueryLimit);
		String docids = '';
		Map<String,InQuira_Article_Info__c> docidMap = new Map<String,InQuira_Article_Info__c>();
		for (Integer i=startPos; i<startPos+length; i++) {
			InQuira_Article_Info__c article = (InQuira_Article_Info__c)scope[i];
			String docid = article.Document_ID__c;
			// remove leading S: from document id
			docid = docid.startsWith('S:') ? docid.substring(2) : docid;
			article.Document_ID__c = docid;
			docids = (docids.equals('') ? '' : docids + ',') + docid;
			docidMap.put(docid, article);
		}
		
		HttpRequest req = isTesting ? null : buildRequest(docids);
		Http http = isTesting ? null : new Http();
		HttpResponse resp = isTesting ? null : http.send(req);
		Integer statusCode = isTesting ? 200 : resp.getStatusCode();
		String fullStatus = isTesting ? 'OK 200' : resp.getStatus() + statusCode;
		String json = isTesting ? '{"docinfo":[{"id":"' + scope[startPos].get('Document_ID__c') + 
			                       '","title":"testtitle","author":"John Q. Tester","version":"1.5.3","owner":"John Q. Tester, Jr.","lastmodified":"2012-01-18 21:34:49 Etc/GMT"}'
			                       + ']}' : resp.getBody();
		if (statusCode==200) {
			InfoManagerDocInfo articleData = new InfoManagerDocInfo();
			InfoManagerDocDetail articleDetail = new InfoManagerDocDetail();
	
			try {
				articleData = (InfoManagerDocInfo)System.JSON.deserialize(json, InfoManagerDocInfo.class);
				updateArticleInfo(docidMap, articleData);
			} catch (Exception e) {
				System.debug('ERROR in InQuira Article Batch Update: ' + e.getMessage());
			}
		} else {
			System.debug('ERROR: was not status 200(' + fullStatus + '), cannot update article info using ' + endpointURL);
		}
	}
	
}