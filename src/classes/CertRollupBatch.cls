/*
    Class: CertRollupBatch
    Purpose: to roll up certifications from child to parent and grand parent account including siblings.
                Note: This class takes care of only three levels of account hirearchy. 
    Author: Jaya ( Perficient )
    Created Date: 7/25/2014                 
    Reference: pc=> Partner Competency  
*/
global without sharing class CertRollupBatch implements Database.Batchable<sObject>, Database.Stateful {
     global Map<String,Acct_Cert_Summary__c> globalPartnerCompetency;
     global Set<Id> accountIds = new Set<Id>();
     global CertRollupBatch(){
        globalPartnerCompetency = new Map<String,Acct_Cert_Summary__c>();
     }
     global Database.QueryLocator start(Database.BatchableContext BC){
        String general_TAP = Constant.GENERAL_PARTNER_COMPETENCY;
        Id partnerRecordType = CertRollup.getRecordType(Constant.ACCOUNT_PARTNER);
        String distyTypeAcct = Constant.DISTY_ACCOUNT;
        Set<Id> relatedacctIds = new Set<Id>();// is to retrive all realted accounts
        String query = 'SELECT Id,Account__c,RSA__c,RSS__c,RTSA__c,RTSS__c,TAP__c,Account__r.RecordTypeId, '
                            +' Roll_Up_RSS__c,Roll_Up_RSA__c,Roll_Up_RTSS__c,Roll_Up_RTSA__c, '
                            +' Account__r.ParentId, Account__r.Parent.Do_not_rollup_Certs__c, '
                            +' Account__r.Parent.Parent.Do_not_rollup_Certs__c '
                            +' FROM Acct_Cert_Summary__c WHERE Account__r.RecordTypeId =: partnerRecordType '
        					+' AND Account__r.Type !=: distyTypeAcct ';
        // TAP__c !=: general_TAP AND general partner competency should also be considered
        if(!accountIds.isEmpty()){
        	relatedacctIds = CertRollupBatchHelper.getRelatedAccounts(accountIds);
        	query += ' AND Account__c IN: relatedacctIds';
        }
        return Database.getQueryLocator(query);
      } 
     global void execute(Database.BatchableContext bcontext, List<sObject> scope){
     	Map<String,Acct_Cert_Summary__c> updatedglobalPC = new Map<String,Acct_Cert_Summary__c>();
     	updatedglobalPC = CertRollupBatchHelper.certRollup(bcontext, scope, globalPartnerCompetency);
     	globalPartnerCompetency.putAll(updatedglobalPC);
     } 
     global void finish(Database.BatchableContext bcontext){
     	
     	RCSCertRollupBatch rcsarcsprollup = new RCSCertRollupBatch();
     	if(!accountIds.isEmpty()){
     		rcsarcsprollup.accountIds.addAll(accountIds);
     	}
     	Database.executeBatch(rcsarcsprollup);
     	/* Competency roll down batch is being called in the RCSCertRollupBatch.
     	CompetencyRollDownBatch CompetencyStatus = new CompetencyRollDownBatch();
     	Database.executeBatch(CompetencyStatus);*/
     }      
}