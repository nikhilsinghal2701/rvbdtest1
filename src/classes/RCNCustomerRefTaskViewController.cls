public with sharing class RCNCustomerRefTaskViewController {
public Task t {get; set;}
private String VIEW_TASK_URL = '';
private String recordTypeName = '';
private String CUSTOMER_REFERENCE_TASK = 'Customer Reference Task';
private List<String> contactIdList;
public Opportunity temOpp {get;set;}
public Lead temLead{get;set;}
private String recordtypeId;
private String taskId;
Public RCNCustomerRefTaskViewController(ApexPages.StandardController stndController)
{
	taskId = Apexpages.currentPage().getParameters().get('id');
	VIEW_TASK_URL = '/' + taskId + '?nooverride=1';
	Task[] taskList = [Select Id, WhoId, WhatId,type,recordtypeId, RecordType.Name, Customer_Reference_Contact_other__c, Customer_Reference_Contact__c,ActivityDate,Subject,OwnerId,Status,Description,CreatedDate,CreatedByID  FROM Task WHERE Id =: taskId];
	if(taskList.size() != 0)
    {
		this.t = taskList[0];
		recordtypeId = t.RecordTypeId;
		System.Debug('RecordTypeName => '+recordtypeId);
	    recordTypeName =  t.RecordType.Name;
	    contactIdList = new List<String>();
	    temOpp = new Opportunity();
	    temLead = new Lead();
	   if(t.Customer_Reference_Contact__c != null && t.Customer_Reference_Contact__c.length() == 18)
	    contactIdList.add(t.Customer_Reference_Contact__c);
	    if(t.Customer_Reference_Contact_other__c != null && t.Customer_Reference_Contact_other__c.length() == 18)
	    contactIdList.add(t.Customer_Reference_Contact_other__c);
	   if(contactIdList.size() != 0)
	    {
	    	for (Contact con : [Select Id, Name from Contact Where Id IN : contactIdList])
	    	{
	    		if(t.Customer_Reference_Contact__c == con.Id)
	    		temOpp.Primary_Contact__c = con.Id;
	    		if(t.Customer_Reference_Contact_other__c == con.Id)
	    		temLead.Sold_to_Partner_Contact__c = con.Id;
	    	
	    	}
	    }
    }
}
public PageReference init()
{
	PageReference pr = null;
	if(recordTypeName != CUSTOMER_REFERENCE_TASK || t == null)
	{
		pr = new PageReference(VIEW_TASK_URL);
        pr.setRedirect(true);
    }
    return pr;
	
}


}