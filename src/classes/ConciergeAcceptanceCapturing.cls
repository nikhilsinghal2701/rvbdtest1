public with sharing class ConciergeAcceptanceCapturing {

    public static void conciergeAcceptanceCapturingRequestHandler(List<CampaignMember> newcms, Map<Id, CampaignMember> oldmap, Boolean isInsert)
    {       
        Set<Id> cmIdsToProcess = new Set<Id>();
        List<CampaignMember> cmsToProcess = new List<CampaignMember>();
        Set<Id> relLeadAndContactIds = new Set<Id>();
        for (CampaignMember cm : newcms)
        {           
            // Procoess campaign members with Owner_Changed_Timestamp__c set or changed            
            if (isInsert
                || (!isInsert && cm.Owner_Changed_Timestamp__c != null && cm.Owner_Changed_Timestamp__c != oldmap.get(cm.Id).Owner_Changed_Timestamp__c) && !cm.Concierge_Accepted__c)
            {               
                    
                cmsToProcess.add(cm);   
                relLeadAndContactIds.add(cm.ContactId != null ? cm.ContactId : cm.LeadId);
            }   
        }   
        
        if (cmsToProcess.size() > 0)
        {
            Concierge_Owner_Criteria__c setting = Concierge_Owner_Criteria__c.getInstance('default');
            Set<String> conciergeProfileIds = new Set<String>();
            Set<String> queueNames = new Set<String>();
            if (setting.Concierge_Profile_IDs__c != null && setting.Queue_Names__c != null)
            {
                // Grab all concierge Queue names and user Profile Ids from custom setting Concierge_Owner_Criteria__c 
                conciergeProfileIds.addAll(setting.Concierge_Profile_IDs__c.split(';'));
                queueNames.addAll(setting.Queue_Names__c.split(';'));   
            }   
            
            Set<Id> queueIds = new Set<Id>();
            for (QueueSobject queueobj : [Select Queue.Id, Queue.Name From QueueSobject Where Queue.DeveloperName = :queueNames]) queueIds.add(queueobj.Queue.Id);
                    
            Map<Id, Lead> relLeads = new Map<Id, Lead>([Select OwnerId, Owner.ProfileId, Owner.Name From Lead Where Id IN :relLeadAndContactIds]);
            Map<Id, Contact> relContacts = new Map<Id, Contact>([Select OwnerId, Owner.ProfileId, Owner.Name From Contact Where Id IN :relLeadAndContactIds]);          
            
            List<CampaignMember> cmsToUpdate = new List<CampaignMember>();
            for (CampaignMember cm : cmsToProcess)
            {   system.debug('***cm:'+cm);system.debug('***cm-contact:'+cm.contactId);
                if (
                    // Check if related contact owner matches by queue or profile
                    (cm.ContactId != null 
                        && (queueIds.contains(relContacts.get(cm.ContactId).OwnerId)
                            || (relContacts.get(cm.ContactId).Owner != null
                                && relContacts.get(cm.ContactId).Owner.ProfileId != null 
                                && conciergeProfileIds.contains(String.valueOf(relContacts.get(cm.ContactId).Owner.ProfileId).substring(0, 15)))
                            )
                    )                       
                    || 
                    // Check if related lead owner matches by queue or profile
                    (cm.LeadId != null 
                        && (queueIds.contains(relLeads.get(cm.LeadId).OwnerId)
                            || (relLeads.get(cm.LeadId).Owner != null 
                                && relLeads.get(cm.LeadId).Owner.ProfileId != null 
                                && conciergeProfileIds.contains(String.valueOf(relLeads.get(cm.LeadId).Owner.ProfileId).substring(0, 15)))
                            )
                        )                       
                  )
                {                   
                    // Update fields below if related lead/contact owners have concierge queue/profiles
                    cm.Concierge_Accepted_Date__c = Date.today();
                    cm.Concierge_Accepted__c = true;                  
                }                                   
            }
        }                                   
    }

}