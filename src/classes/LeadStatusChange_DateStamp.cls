public class LeadStatusChange_DateStamp {

public static boolean firstRun = True;

public static void leadTimeStamp(List<Lead>lds,Map<ID,Lead> oldLeadMap){
    List<Lead> uplds= new List<Lead>();
    Map<ID,Lead> upldsMap= new Map<ID,Lead>();
    set<String> upflds= new set<String>();
    set<String> allflds= new set<String>{'Lead_Automation_Qualified_Date__c',
                                        'Lead_Marketing_Qualified_Date__c',
                                        'Lead_Open_Date__c',
                                        'Lead_Qualified_for_Future_Date__c',
                                        'Lead_Sales_Accepted_Date__c',
                                        'Lead_Sales_Disqualified_Date__c',
                                        'Lead_Sales_Qualified_Date__c',
                                        'Lead_Tele_prospecting_Accepted_Date__c',
                                        'Lead_Tele_prospecting_Disqualified_Date__c',
                                        'Converted_Date_Time__c'
                                        };
    map<string,LeadStatusDateStamp__c> cSettingMap = LeadStatusDateStamp__c.getall();
    System.debug(LoggingLevel.INFO,'cSettingMap ***'+ cSettingMap);
    
    for(Lead l: lds){
        
        Lead oldLead;
        
        if(Trigger.isUpdate ){
            System.debug(LoggingLevel.INFO,'***oldLeadMap.get(l.id)' + oldLeadMap.get(l.id));  
             oldLead = oldLeadMap.get(l.id)!=null?oldLeadMap.get(l.id): new Lead();
            
            if(oldLead.status=='Sales Disqualified' && l.status=='Open'){
            
            for( string s1 : allflds){
                System.debug(LoggingLevel.INFO,'***fieldsToNull' + s1);  
                l.put(s1,null);
                }
            }
        }
            for(LeadStatusDateStamp__c cSetting:cSettingMap.values()){
                
                if(!Trigger.isUpdate){
                
                if(cSetting.Lead_Current_Status__c==l.status && cSetting.Lead_Prior_Status__c=='New' ){
                       
                       for(String s:cSetting.Fields_to_Update__c.split(';')){
                           
                           System.debug(LoggingLevel.INFO,'************cSetting in Insert ' + cSetting.Name);
                           System.debug(LoggingLevel.INFO,'************ ' + DateTime.now());
                           
                           if(upldsMap.get(l.ID)!=null){
                             
                             System.debug(LoggingLevel.INFO,'***** setting the valuein If'+  '######' + upldsMap.get(l.ID));
                             upldsMap.get(l.ID).put(s,DateTime.now());  
                           }
                           else{
                               System.debug(LoggingLevel.INFO,'***** setting the valuein else ');
                               l.put(s,DateTime.now());
                               upldsMap.put(l.id,l);
                               System.debug(LoggingLevel.INFO,'***** setting the valuein else '+'##'+l.get(s));
                               }
                         }
                }
                  
            }
            
            else
            if(l.status !=oldLead.status && ((cSetting.Lead_Current_Status__c==l.status && cSetting.Lead_Prior_Status__c==oldLead.status) || 
                (cSetting.Lead_Current_Status__c==l.status && (l.status=='Open' || l.status=='Tele-prospecting Disqualified' || l.status=='Sales Disqualified')))){
                       
                       for(String s:cSetting.Fields_to_Update__c.split(';')){
                           
                           System.debug(LoggingLevel.INFO,'************cSetting in update ' + cSetting.Name);
                           System.debug(LoggingLevel.INFO,'************ ' + DateTime.now());
                           
                           if(upldsMap.get(l.ID)!=null){
                             upldsMap.get(l.ID).put(s,DateTime.now());  
                           }
                           else{
                              l.put(s,DateTime.now());
                              upldsMap.put(l.id,l);
                           }
                         }
                }
                 
            }
          System.debug(LoggingLevel.INFO,'************ Lead update '+ l.Lead_Marketing_Qualified_Date__c);  
        }
    
    firstRun=False;
    
    }
}