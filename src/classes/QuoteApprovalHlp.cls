/************************************************
Rev1 Rucha	7/17/2013	Changed isApprovedDiscountExceeded to look at approved delta discount on opportunity instead of actual NSD value (in Special Discount)
*************************************************/
public without sharing class QuoteApprovalHlp {
    //Quote Discount Status
    public static final String STATUS_NOT_REQUIRED = 'Not Required';
    public static final String STATUS_REQUIRED = 'Required';
    public static final String STATUS_APPROVED = 'Approved';
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_PENDING = 'Pending Approval';
    public static final String STATUS_MORE_INFORMATION = 'More Information';
    public static final String STATUS_CANCELLED = 'Cancelled';
    public static final String STATUS_REJECTED = 'Rejected';
    
    // Status values below are valid for applying promotions.
    public static final Set<String> PROMOTION_VALID_STATUSES = 
        new Set<String>{STATUS_NOT_REQUIRED.toLowerCase(),STATUS_REQUIRED.toLowerCase(),STATUS_DRAFT.toLowerCase(),STATUS_APPROVED.toLowerCase()};
        
    // Quotes in these statues will not have its Discount Status re-evaluated.
    public static final Set<String> FIXED_STATUSES = 
        new Set<String>{STATUS_PENDING.toLowerCase(),STATUS_APPROVED.toLowerCase(),STATUS_REJECTED.toLowerCase()};
    
    public static final String CATEGORY_A = 'A';
    public static final String CATEGORY_B = 'B';
    public static final String CATEGORY_C = 'C';
    public static final String CATEGORY_D = 'D';
    public static final String CATEGORY_E = 'E';
    
    /**
     * Tests if any discount in specified map exceeds approved discount in that category
     * on specified Opportunity.
     */
     // Modified by Santoshi on Dec - 19 to add the new code based on the discount detail records ..
    public static Boolean isApprovedDiscountExceeded(Opportunity opp, Map<String,Decimal> discountsByCategory) {
        Boolean exceeded = false;
        //Rev1 - Added column Delta_Discount__c to query
      List<Discount_Detail__c> oppDiscountsList = [select Discount__c, Uplift__c,Special_Discount__c, IsOpp__c,Opportunity__c, Category_Master__c,Category_Master__r.Name,Delta_Discount__c from Discount_Detail__c where Opportunity__c = :opp.Id and IsOpp__c = true];
      /**  exceeded = exceeded || isExceeded(opp.Approved_Discount_A__c, discountsByCategory.get(CATEGORY_A));
        exceeded = exceeded || isExceeded(opp.Approved_Discount_B__c, discountsByCategory.get(CATEGORY_B));
        exceeded = exceeded || isExceeded(opp.Approved_Discount_C__c, discountsByCategory.get(CATEGORY_C));
        exceeded = exceeded || isExceeded(opp.Approved_Discount_D__c, discountsByCategory.get(CATEGORY_D));
        exceeded = exceeded || isExceeded(opp.Approved_Discount_E__c, discountsByCategory.get(CATEGORY_E));*/
     
        for(Discount_Detail__c d :oppDiscountsList)
        {
        	  exceeded = exceeded || isExceeded(d.Special_Discount__c, discountsByCategory.get(d.Category_Master__r.Name ));//uncommented by psingh@riverbed.com on 8/20/2013
        	   //Rev1 - Changed parameter from Special_discount__c to Delat_discount__c
        	   //exceeded = exceeded || isExceeded(d.Delta_Discount__c, discountsByCategory.get(d.Category_Master__r.Name )); commented by psingh@riverbed.com on 8/20/2013
        	   System.debug(LoggingLevel.INFO,'testing in hlp----'+exceeded+'--'+d.Category_Master__r.Name+'opty-discount--'+d.Special_Discount__c+'quoe-spldiscount--'+discountsByCategory.get(d.Category_Master__r.Name ));
        }
        return exceeded; 
    }
    
    public static Boolean isExceeded(Decimal approvedDiscount, Decimal discount) {
        if ((approvedDiscount == null) && (discount == null)) {
            // Both are blank so we are fine
            return false;
        } else if ((discount == null) || (discount == 0)) {
            // Discount is blank so we are fine
            return false;
        } else if (approvedDiscount == null) {
            return true;
        }
        return discount > approvedDiscount;
    }

   /** public static void updateOpportunityWithApprovedDiscounts(Quote__c quote, Opportunity opp, Map<String,Decimal> discountsByCategory) {
        if ((opp.Approved_Discount_A__c==null) || isExceeded(opp.Approved_Discount_A__c, discountsByCategory.get(CATEGORY_A))) {
            opp.Approved_Discount_A__c = discountsByCategory.get(CATEGORY_A);
            if (discountsByCategory.containsKey(CATEGORY_A)) {
                opp.Approved_Quote_A__c = quote.Id;
            }
        }
        if ((opp.Approved_Discount_A__c==null) || isExceeded(opp.Approved_Discount_A__c, discountsByCategory.get(CATEGORY_A))) {
            opp.Approved_Discount_A__c = discountsByCategory.get(CATEGORY_A);
            if (discountsByCategory.containsKey(CATEGORY_A)) {
                opp.Approved_Quote_A__c = quote.Id;
            }
        }
        if ((opp.Approved_Discount_B__c==null) || isExceeded(opp.Approved_Discount_B__c, discountsByCategory.get(CATEGORY_B))) {
            opp.Approved_Discount_B__c = discountsByCategory.get(CATEGORY_B);
            if (discountsByCategory.containsKey(CATEGORY_B)) {
                opp.Approved_Quote_B__c = quote.Id;
            }
        }
        if ((opp.Approved_Discount_C__c==null) || isExceeded(opp.Approved_Discount_C__c, discountsByCategory.get(CATEGORY_C))) {
            opp.Approved_Discount_C__c = discountsByCategory.get(CATEGORY_C);
            if (discountsByCategory.containsKey(CATEGORY_C)) {
                opp.Approved_Quote_C__c = quote.Id;
            }
        }
        if ((opp.Approved_Discount_D__c==null) || isExceeded(opp.Approved_Discount_D__c, discountsByCategory.get(CATEGORY_D))) {
            opp.Approved_Discount_D__c = discountsByCategory.get(CATEGORY_D);
            if (discountsByCategory.containsKey(CATEGORY_D)) {
                opp.Approved_Quote_D__c = quote.Id;
            }
        }
        if ((opp.Approved_Discount_E__c==null) || isExceeded(opp.Approved_Discount_E__c, discountsByCategory.get(CATEGORY_E))) {
            opp.Approved_Discount_E__c = discountsByCategory.get(CATEGORY_E);
            if (discountsByCategory.containsKey(CATEGORY_E)) {
                opp.Approved_Quote_E__c = quote.Id;
            }
        }
    }*/
    
    /**
     * Straight delegation to native update. Exists here only because this class runs without sharing enforced.
     */
    public static void updateOpportunities(List<Opportunity> opps) {
        update opps;
    }
    
    public static Boolean isUnderValidPromotion(Quote__c quote) {
        return ((quote.Promotional__c == true) && (quote.Promotion_Expired__c == false));
    }
    
    public static Boolean isJustApproved(Quote__c oldQuote, Quote__c newQuote) {
    	String newStatus = '';
    	if(newQuote != null){
	        newStatus = newQuote.Discount_Status__c != null ? newQuote.Discount_Status__c.toLowerCase(): '';
    	}
        String oldStatus = oldQuote.Discount_Status__c;
        // We need to consider both Pending Approval and More Information prior status.
        // Normally, More Information is changed back to Pending Approval on next approval step.
        // But if more info was requested by last approver prior status would still be More Information.
        Boolean priorStatusInApproval = oldStatus.equalsIgnoreCase(STATUS_PENDING) || oldStatus.equalsIgnoreCase(STATUS_MORE_INFORMATION);
        return newStatus.equalsIgnoreCase(QuoteApprovalHlp.STATUS_APPROVED) && priorStatusInApproval;
    }
    
    public static Boolean isOnlyApproverRSM(List<Approvers_Routing_Matrix__c> approvers) {
        return (approvers.size() == 1) && (approvers[0].Level__c == 0);
    }
}