/*******************************************************************************************
// Class Name: AccountPlanningViewExtension
//
// Description: This class is the controller class for the AccountPlanview and AccountPlanView PDF page.
// 
//
// Created By: Nikhil Singhal (Perficient) - 7/10/2014
*******************************************************************************************/


public with sharing class AccountPlanningViewExtension {
private final Account_Plan__c Ap;
public List<Account_Plan__History> APH  {get;set;}
public List<Attachment> attachments {get; set;}
public Attachment attachment1 {get; set;}
public  Account_Plan__c  APV {get; set;}
public  Account_Plan__c  APC {get; set;}
public  Account  Acc {get; set;}
public List<Account_Plan_Team__c> APTList {get;set;}
public List<Opportunity_Development__c> odList {get;set;}
public List<Customer_Partner__c> cpList {get;set;}
public List<Customer_Partner__c> cpaList {get;set;}
public List<Implementation_Customer__c> icList {get;set;}
public List<Competitive_Landscape__c> claList {get;set;}
public List<Competitive_Landscape__c> clbList {get;set;}
public List<task> tList {get;set;}
public List<ColorWrapper> WhmList {get; set;}
public List<FieldLabel> Wrows {get; set;}
String type='Account_Plan__c';
public List<Attachment> Dispattachments {get; set;}

public List<Account_Plan_Team__c[]> pageBrokenAPT {get; private set; }
public List<Opportunity_Development__c[]> pageBrokenOD {get; private set; }
public List<Customer_Partner__c[]> pageBrokencpa {get; private set; }
public List<Customer_Partner__c[]> pageBrokencp {get; private set; }
public List<Implementation_Customer__c[]> pageBrokenic {get; private set; }
public List<Competitive_Landscape__c[]> pageBrokencla {get; private set; }
public List<Competitive_Landscape__c[]> pageBrokenclb {get; private set; }
public List<task[]> pageBrokent {get; private set; }
public Boolean hasaccess {get; private set; }
public String var { get; set; }
//controls how many quote line items are displayed on page 1
    private static Integer FIRST_BREAK = 20;
    //controls how many quote line items are displayed on subsequent pages
    private static Integer SUBSEQ_BREAKS = 20;


public AccountPlanningViewExtension(ApexPages.StandardController controller) {
                WhmList = New List<ColorWrapper>(); 
                Wrows=New List<FieldLabel>(); 
                apc=new Account_Plan__c();
                Ap = (Account_Plan__c)controller.getRecord();
                Dispattachments= New List<Attachment>(); 
                Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                Schema.SObjectType leadSchema = schemaMap.get(type);
                Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
                
                if(ApexPages.currentPage().getParameters().get('Id') != null){
                APV=[SELECT Budget_Information_Other__c,total__c,X18_Month_Revenue_Potential__c,Critical_Business_Applications__c , AccountId__c, Name, Account_Profile_Notes__c, Account_Renewal_Information__c,
                Account_Trade_Up_Information__c, Co_term_ability__c, CreatedById, CreatedDate, Critical_Contact__c, CurrencyIsoCode,
                Current_FY_Account_IT_Budget__c, Current_FY_Network_Infrastructure_Budget__c, Customer_Preference__c, IsDeleted, 
                Describe_customers_buying_process__c, Executive_Summary__c, Fiscal_Year__c, Global_Discount_Level__c, 
                Global_Strategy__c, HQ_location__c, LastActivityDate, LastModifiedById, LastModifiedDate, Most_Recent_Quarter_Revenue__c, 
                Number_of_Sites__c, OwnerId, Partner_Eco_System__c, Partner_Preferences__c, Partner_Strategy__c, Past_Fiscal_Year_Revenue__c, 
                Previous_Year_1_2_RVBD_Revenue__c, Purchase_Driver__c, Purchasing_Decision_Making__c, Q_1_Revenue__c, Q_2_Revenue__c, Q_3_Revenue__c, 
                Q_4_Revenue__c, Q_5_Revenue__c, Q_6_Revenue__c, Q_7_Revenue__c, Q_8_Revenue__c, Id, Riverbed_Share_of_Wallet__c, Rvbd_Support__c, 
                Situation_Summary__c, Status__c, SystemModstamp, Top_Executive_Conversations__c, Total_Number_of_customer_Sites__c, 
                Uniform_RVBD_Message__c,owner.name FROM Account_Plan__c where id=:Ap.id];
                
                hasaccess=[select RecordId, HasEditAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :Ap.id].HasEditAccess;
                
                
                Acc=[Select ownerid,owner.name,name,CreatedDate,SystemModstamp,Category__c from account where id=:Apv.AccountId__c];
                
                
                APTList = [Select id,User__c,Role__c,Account_Plan__c,Access__c,CreatedDate,User__r.name from Account_Plan_Team__c where Account_Plan__c =: ap.Id];
               
               
                odList = [Select id,Customer_Impact_of_doing_nothing__c,Any_Related_Customer_Project_Pending__c,Account_Plan__c,Current_Situation_Being_Addressed__c,Customer_intiative_Area__c,
                         Do_we_have_a_plan_to_attack__c,opportunity_We_Could_Address__c,Revenue_Potential__c from Opportunity_Development__c where Account_Plan__c =: ap.Id];
                
                
                cpList = [Select id,Account_Manger_name_of_the_Customer_Pa__c,Customer_Partner__c,Account_Plan__c,Influence_decision_Makers__c,Riverbed_Ally__c,
                         Riverbed_Partner_Influencer__c,Riverbed_Partner_Influencer__r.name,Type__c,Riverbed_Partner_Influencer__r.owner.name from Customer_Partner__c where Account_Plan__c =: ap.Id  and Riverbed_Partner_Influencer__c != null];
                
                
                cpaList = [Select id,Account_Manger_name_of_the_Customer_Pa__c,Customer_Partner__c,Account_Plan__c,Influence_decision_Makers__c,Riverbed_Ally__c,
                         Riverbed_Partner_Influencer__c,Riverbed_Partner_Influencer__r.name,Type__c,Riverbed_Partner_Influencer__r.owner.name from Customer_Partner__c where Account_Plan__c =: ap.Id and Customer_Partner__c != null ];
                
                                
                icList = [Select id,Units__c,Completion_Date__c,Account_Plan__c,Current_Status__c,Description_and_functional_Location__c,
                         Hardware_Model__c,Project__c from Implementation_Customer__c where Account_Plan__c =: ap.Id];  
               
                claList = [Select id,Vendor__c,Products__c,Units__c,Use_Case__c,Account_Plan__c,Refresh_cycle__c,Customer_Perception_Of_Incumbent_vendor__c from Competitive_Landscape__c where Account_Plan__c =: ap.Id and Vendor__c != null];
               
                clbList = [Select id,Account_Plan__c,Competitor__c,Current_Past_Project__c,Competitor_Revenue__c,Competitor_Key_Champions__c,Customer_Perceptions_of_Competitor__c from Competitive_Landscape__c where Account_Plan__c =: ap.Id and Competitor__c != null];
                
                tList = [Select id,Ownerid,owner.name,whatid,whoid,ActivityDate,Resources_Required__c,Strategy_Objective__c,Section__c,Planned_Actions__c,status,subject,type from task where whatid =: ap.Id ORDER BY status DESC  NULLS LAST];
                
                
                for(Heat_Map__c hmw : [Select id,Products__c,Stage__c,Account_Plan__c from Heat_Map__c where Account_Plan__c =: ap.Id]) {
                    AccountPlanHeatMap__c myC = AccountPlanHeatMap__c.getValues(hmw.Stage__c);
                    WhmList.add(new ColorWrapper(hmw,myc.color__c));
                    }
                    
                 if(ap.id != null){
                 APH=[ SELECT Field, CreatedById,CreatedBy.name, CreatedDate, IsDeleted, Id, NewValue, OldValue, ParentId FROM Account_Plan__History where parentid=: ap.Id];
                 for(Account_Plan__History a : APH){
                    if(a.Field == 'created'){
                    Wrows.add(new FieldLabel('created',a));
                    }else if(fieldMap.containskey(a.field)){
                    Wrows.add(new FieldLabel(fieldMap.get(a.field).getDescribe().getLabel(),a));
                    }else{
                    Wrows.add(new FieldLabel(a.field,a));
                    }
                 }
                }
                
                attachment1 = new Attachment();
                attachments = [ SELECT Id, Name,CreatedBy.name, BodyLength, CreatedById, LastModifiedDate ,CreatedDate
                                FROM Attachment 
                                WHERE parentId =: ap.Id ORDER BY CreatedDate DESC];
                 
                 for(Attachment a: attachments){
                if(a.name.endsWith('.jpg'))
                Dispattachments.add(a);
                }
                //PDf methods to Print data with repeat headers on row break
                prepareteamForPrinting();
                prepareodForPrinting();
                prepareicForPrinting();
                preparecpForPrinting();
                preparecpaForPrinting();
                prepareclbForPrinting();
                prepareclaForPrinting();
                preparetForPrinting();
                
                }
}

//Wrapper Class For Heat map
public class ColorWrapper
{
    public String Color {get; set;}
    public Heat_Map__c hmData {get;set;}

    public ColorWrapper(Heat_Map__c hm, String docolor)
    {
        hmData = hm;
        Color = docolor;
    }
}

// Delete attachment        by Sukhdeep Singh 23/12/2014
public pageReference deleteAttachment(){
    System.debug('deleteAttachment    ==> ');
    System.debug('deleteAttachment   yourvariablefromController ==> '+var);
    Pagereference p = new Pagereference( '/apex/AccountPlanView?id='+apv.Id);
 try{
    Attachment att=new Attachment(id=var);
    delete att;
    }catch(Exception e){
        System.debug('AccountPlanningViewExtension.deleteAttachment    Exception: '+e);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment can not be deleted'));
        return null;
    }
 p.setRedirect( true ); 
 return p;
}


//Clone Method
public PageReference CClone(){
apc.X18_Month_Revenue_Potential__c=apv.X18_Month_Revenue_Potential__c;
apc.AccountId__c=apv.AccountId__c;
apc.Name=apv.Name;
apc.Account_Profile_Notes__c=apv.Account_Profile_Notes__c;
apc.Account_Renewal_Information__c=apv.Account_Renewal_Information__c;
apc.Account_Trade_Up_Information__c=apv.Account_Trade_Up_Information__c;
apc.Co_term_ability__c=apv.Co_term_ability__c;
apc.Critical_Contact__c=apv.Critical_Contact__c;
apc.Current_FY_Account_IT_Budget__c=apv.Current_FY_Account_IT_Budget__c;
apc.Current_FY_Network_Infrastructure_Budget__c=apv.Current_FY_Network_Infrastructure_Budget__c;
apc.Customer_Preference__c=apv.Customer_Preference__c;
apc.Describe_customers_buying_process__c=apv.Describe_customers_buying_process__c;
apc.Executive_Summary__c=apv.Executive_Summary__c;
apc.Fiscal_Year__c=apv.Fiscal_Year__c;
apc.Global_Discount_Level__c=apv.Global_Discount_Level__c;
apc.Global_Strategy__c=apv.Global_Strategy__c;
apc.HQ_location__c=apv.HQ_location__c;
apc.Most_Recent_Quarter_Revenue__c=apv.Most_Recent_Quarter_Revenue__c;
apc.Number_of_Sites__c=apv.Number_of_Sites__c;
apc.Partner_Eco_System__c=apv.Partner_Eco_System__c;
apc.Partner_Preferences__c=apv.Partner_Preferences__c;
apc.Partner_Strategy__c=apv.Partner_Strategy__c;
apc.Past_Fiscal_Year_Revenue__c=apv.Past_Fiscal_Year_Revenue__c;
apc.Previous_Year_1_2_RVBD_Revenue__c=apv.Previous_Year_1_2_RVBD_Revenue__c;
apc.Purchase_Driver__c=apv.Purchase_Driver__c;
apc.Purchasing_Decision_Making__c=apv.Purchasing_Decision_Making__c;
apc.Q_1_Revenue__c=apv.Q_1_Revenue__c;
apc.Q_2_Revenue__c=apv.Q_2_Revenue__c;
apc.Q_3_Revenue__c=apv.Q_3_Revenue__c;
apc.Q_4_Revenue__c=apv.Q_4_Revenue__c;
apc.Q_5_Revenue__c=apv.Q_5_Revenue__c;
apc.Q_6_Revenue__c=apv.Q_6_Revenue__c;
apc.Q_7_Revenue__c=apv.Q_7_Revenue__c;
apc.Q_8_Revenue__c=apv.Q_8_Revenue__c;
apc.Riverbed_Share_of_Wallet__c=apv.Riverbed_Share_of_Wallet__c;
apc.Rvbd_Support__c=apv.Rvbd_Support__c;
apc.Situation_Summary__c=apv.Situation_Summary__c;
apc.Status__c=  apv.Status__c;
apc.Top_Executive_Conversations__c=apv.Top_Executive_Conversations__c;
apc.Total_Number_of_customer_Sites__c=apv.Total_Number_of_customer_Sites__c;
apc.Uniform_RVBD_Message__c=apv.Uniform_RVBD_Message__c;
upsert apc;
     return (new ApexPages.StandardController(apc)).Edit();
 }

//Method to upload attachment
public PageReference upload(){
        if (attachment1.name == null){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select File'));
        return null;
        }
        if (attachment1.Body.size() > 5242880){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment 1: Max attachment size is 5 Mb'));
        return null;
        }
        attachment1.ParentId = apv.Id;
        attachment1.IsPrivate=false;
        insert attachment1;
        attachment1.body = null;
        attachment1.clear();
        attachment1 = new Attachment();
        Pagereference p = new Pagereference( '/apex/AccountPlanView?id='+apv.Id);
        p.setRedirect( true ); 
        return p;
    }
  
 // WrapperClass For  history field labels 
    
public without sharing class FieldLabel{
public String Name      {get;set;}
public Account_Plan__History APHs {get;set;}
public FieldLabel(String c , Account_Plan__History s){
this.Name=c;
this.APHs=s;
}
}

    public PageReference genpdf() {
        PageReference secondPage = Page.AccountPlanPDF;
        secondPage.setRedirect(true);
        secondPage.getParameters().put('id',apv.id); 
        return secondPage; 
    }
    
    
 //splits the lines into an approximate number of rows that can be 
    //displayed per page
    
    
    
    private void prepareteamForPrinting()
    {
         pageBrokenAPT = new List<Account_Plan_Team__c[]>();
        
        Account_Plan_Team__c[] pageOfteam = new Account_Plan_Team__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Account_Plan_Team__c q : APTList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfteam.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokenAPT.add(pageOfteam);
                    pageOfteam = new Account_Plan_Team__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfteam.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokenAPT.add(pageOfteam);
                    pageOfteam = new Account_Plan_Team__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfteam.isEmpty())
            pageBrokenAPT.add(pageOfteam);
    }

    private void prepareodForPrinting()
    {
         pageBrokenOD = new List<Opportunity_Development__c[]>();
        
        Opportunity_Development__c[] pageOfOD = new Opportunity_Development__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Opportunity_Development__c q : odList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfOD.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokenOD.add(pageOfOD);
                    pageOfOD = new Opportunity_Development__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfOD.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokenOD.add(pageOfOD);
                    pageOfOD = new Opportunity_Development__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfOD.isEmpty())
            pageBrokenOD.add(pageOfOD);
    }
    
      private void preparecpaForPrinting()
    {
         pageBrokencpa = new List<Customer_Partner__c[]>();
        
        Customer_Partner__c[] pageOfcpa = new Customer_Partner__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Customer_Partner__c q : cpaList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfcpa.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokencpa.add(pageOfcpa);
                    pageOfcpa = new Customer_Partner__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfcpa.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokencpa.add(pageOfcpa);
                    pageOfcpa = new Customer_Partner__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfcpa.isEmpty())
            pageBrokencpa.add(pageOfcpa);
    }

      private void preparecpForPrinting()
    {
         pageBrokencp = new List<Customer_Partner__c[]>();
        
        Customer_Partner__c[] pageOfcp = new Customer_Partner__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Customer_Partner__c q : cpList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfcp.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokencp.add(pageOfcp);
                    pageOfcp = new Customer_Partner__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfcp.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokencp.add(pageOfcp);
                    pageOfcp = new Customer_Partner__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfcp.isEmpty())
            pageBrokencp.add(pageOfcp);
    }
    
          private void prepareicForPrinting()
    {
         pageBrokenic = new List<Implementation_Customer__c[]>();
        
        Implementation_Customer__c[] pageOfic = new Implementation_Customer__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Implementation_Customer__c q : icList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfic.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokenic.add(pageOfic);
                    pageOfic = new Implementation_Customer__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfic.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokenic.add(pageOfic);
                    pageOfic = new Implementation_Customer__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfic.isEmpty())
            pageBrokenic.add(pageOfic);
    }
    
          private void prepareclaForPrinting()
    {
         pageBrokencla = new List<Competitive_Landscape__c[]>();
        
        Competitive_Landscape__c[] pageOfcla = new Competitive_Landscape__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Competitive_Landscape__c q : claList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfcla.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokencla.add(pageOfcla);
                    pageOfcla = new Competitive_Landscape__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfcla.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokencla.add(pageOfcla);
                    pageOfcla = new Competitive_Landscape__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfcla.isEmpty())
            pageBrokencla.add(pageOfcla);
    }
    
     private void prepareclbForPrinting()
    {
         pageBrokenclb = new List<Competitive_Landscape__c[]>();
        
        Competitive_Landscape__c[] pageOfclb = new Competitive_Landscape__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Competitive_Landscape__c q : clbList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfclb.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokenclb.add(pageOfclb);
                    pageOfclb = new Competitive_Landscape__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfclb.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokenclb.add(pageOfclb);
                    pageOfclb = new Competitive_Landscape__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfclb.isEmpty())
            pageBrokenclb.add(pageOfclb);
    }
    
         private void preparetForPrinting()
    {
         pageBrokent = new List<task[]>();
        
        task[] pageOft = new task[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(task q : tList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOft.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokent.add(pageOft);
                    pageOft = new task[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOft.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokent.add(pageOft);
                    pageOft = new task[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOft.isEmpty())
            pageBrokent.add(pageOft);
    }
    


}