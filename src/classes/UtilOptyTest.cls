/**
 Created by Santoshi on Jan-2012 to merge the test class for Util Class an OptyProductViewController.
 */
@isTest
private class UtilOptyTest {

    static  testMethod void coverage(){
    
        Account a = new Account(name = 'account');
        insert a;
        
        Account stpAcc=new Account();
        stpAcc.RecordTypeId='012300000000NE5AAM';
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Partner_Level__c='Distributor';
        stpAcc.Phone='4154154156';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        insert stpAcc;
        
        Account tier2Acc=new Account();
        tier2Acc.RecordTypeId='012300000000NE5AAM';
        tier2Acc.name='tier2Account';
        tier2Acc.Type='VAR';
        tier2Acc.Industry='Education';
        tier2Acc.RASP_Status__c='Authorized';
        tier2Acc.Partner_Level__c='Diamond';
        tier2Acc.Phone='4154154156';
        tier2Acc.Authorizations_Specializations__c='RASP';
        insert tier2Acc;
        
        User primaryISR=[select id,name,DefaultCurrencyIsoCode from User limit 1];
        User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' limit 2];
        
        Opportunity testOpp=new Opportunity();
        testOpp.AccountId=a.Id;
        testOpp.Channel__c='Distributor';
        testOpp.Sold_To_Partner__c=stpAcc.Id;
        testOpp.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp.Tier2__c=tier2Acc.Id;
        testOpp.Tier2_Partner_User__c=powerPartner[1].Id;        
        testOpp.Primary_ISR__c=primaryISR.Id;
        testOpp.Name='TestOpportunity';
        testOpp.Type='New';
        testOpp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp.CloseDate=System.today();
        testOpp.StageName='0-Prospect';
        testOpp.Forecast_Category__c='PipeLine';
        testOpp.Competitors__c='cisco';
        testOpp.LeadSource='Email';
        //testOpp.Support_Provider__c=stpAcc.Id;
        //testOpp.Support_Provided__c='Reseller';
        insert testOpp;
        
        List<Lead> testLead=[select id,name from lead limit 2];
        set<Id>lId=new set<Id>{testLead[0].Id,testLead[1].Id};
        Util.setDMLOptions(lId);
        
        Quote__c qt=[select Id, name from quote__c limit 1];
        Util.sendEmail(qt.Id);
        
        List<string> strLst=new List<string>{'logitech','mouse',''};
        string result1=Util.join(strLst,'-','');
        set<Id> recId=new set<Id>{'001R000000TT58y'};
        string result2=Util.join(recId,'-','');
        set<string> recStr=new set<string>{'A100','A101','A102'};
        string result3=Util.join(recStr,'-','');
        
        Contact c = new Contact (accountid = a.Id, lastname = 'con', email='test@test.com');
        insert c;
        
        a.IsPartner = true;
        update a;
    
        Profile[] p = [Select p.Id,p.Name, p.UserType From Profile p where p.userType='PowerPartner' and p.Name like 'PRM - Indirect Partner No Pricing%'];
    
        User newPartner = new User(alias = 'standt', email='standarduser@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        contactId = c.Id, localesidkey='en_US', profileid = p[0].Id, 
        timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com.rvbd');   
    
        system.test.starttest();
        insert newPartner;
        User u=[select id from user where name='Prashant Singh' and UserType='Standard'];
        system.runAs(u){
         //modified by santoshi on Jan-2012 to remove the error of index out of bound error
        if(p.size()>=2)   
        {           
        newPartner.ProfileId=p[1].Id;
        update newPartner;
        }
         //modified by santoshi on Jan-2012 to remove the error of index out of bound error
        if(p.size()>=3)   
        {
        newPartner.ProfileId=p[2].Id;
        update newPartner;
        } 
        }
        system.test.stoptest();
    }
    
    static testmethod void testProductView(){
        //create new account
        Account a = new account (name = 'no match',BillingState='Birmingham',BillingCountry='AF');
        insert a;
        
        //get a reseller user
        //User u = [select Id,Contact.AccountId from User where profileid in :resellerProfileIds and isactive = true limit 1];  // Commneted there is no users in the resellerProfileIds list code failing
        List <User> u = [select Id,Contact.AccountId from User where profileid='00eA0000000d3Vs' and isactive = true limit 2];
         Opportunity opp = new Opportunity();
        opp.CloseDate = System.today();
        opp.Name = 'Test Opp';  
        opp.StageName = '0 - Prospect';        
        opp.accountId = a.id;
        opp.Channel__c='Dstributor';
        if(!u.isempty())//modified by santoshion Jan-2012
        {
        opp.Tier2__c = u[0].Contact.AccountId;
        opp.Tier2_Partner_User__c = u[0].Id;
        opp.Sold_to_Partner__c = u[1].Contact.AccountId;
        }
        insert opp;
        
        PriceBookEntry pbe = [select id from Pricebookentry where isactive = true and CurrencyIsoCode = 'USD' limit 1];
        OpportunityLineItem item = new OpportunityLineItem(OpportunityId = opp.Id, Quantity = 1, PriceBookEntryId = pbe.id, unitprice = 100);
        insert item;
        //test as reseller
        System.runAs(u[0]){
            PageReference pref = Page.ViewProduct;
            Test.setCurrentPage(pref);
            OpportunityProductViewController con = new OpportunityProductViewController(new ApexPages.StandardController(item));
            PageReference p = con.onLoad();
        }
        //test as admin
        PageReference pref1 = Page.ViewProduct;
        Test.setCurrentPage(pref1);
        OpportunityProductViewController con1 = new OpportunityProductViewController(new ApexPages.StandardController(item));
        PageReference p1 = con1.onLoad(); 
    }
}