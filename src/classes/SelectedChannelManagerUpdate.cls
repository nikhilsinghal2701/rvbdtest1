public class SelectedChannelManagerUpdate {
    /*
    Purpose:If the mapping changes, to propagate the changes to accounts
    Author:Prashant Singh
    Date:15June2010
    */
    private ApexPages.StandardSetController setCon;
    private set<Id> selectedId=new set<Id>();
    private static Map <Id, String> m_accountUpdates = new Map <Id, String> ();
    private List<Channel_Manager_Assignments__c> reclist;
    public SelectedChannelManagerUpdate(ApexPages.StandardSetController controller) {
        setCon = controller;
    }
    public integer getMySelectedSize() {
        return setCon.getSelected().size();
    }
    public integer getMyRecordsSize() {
        return setCon.getRecords().size();
    }
    /**
    *   Process mass update account and populate CAM/CMM on the basis of new mappings.
    *   
    *   @param  
    *   @param  
    *   @return PageReference
    */
    public PageReference massUpdate(){
        set<string> geoSet=new set<string>();
        set<string> regionSet=new set<string>();
        String parseString;
        reclist=(List<Channel_Manager_Assignments__c>) setCon.getSelected();
        //system.debug('reclist size:'+reclist.size()); 
        RecordType rType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
        for(Channel_Manager_Assignments__c obj:reclist){
            selectedId.add(obj.Id); 
        }
        List<Channel_Manager_Assignments__c> selectedRecord=[select Id, geo__c,region__c,country__c,
                                            Channel_Account_Manager__c,Channel_Marketing_Manager__c 
                                            from Channel_Manager_Assignments__c where Id IN :selectedId];
        //system.debug('selectedRecord size:'+selectedRecord.size());
        for(Channel_Manager_Assignments__c obj:selectedRecord){
            geoSet.add(obj.geo__c);
            regionSet.add(obj.region__c);
        }
        for(Account acc:[select Id,Geographic_Coverage__c,Region__c,BillingCountry,Channel_Account_Manager__c,RecordTypeId,Type, 
                         Channel_Marketing_Manager__c,Override_CAM_or_CMM_updates__c 
                         from Account where Geographic_Coverage__c IN :geoSet
                         and Region__c IN:regionSet and Override_CAM_or_CMM_updates__c=False 
                         and (Type='VAR'OR Type='Distributor') and RecordTypeId=:rType.Id and billingcountry != null]){
            for(Channel_Manager_Assignments__c channel:selectedRecord){
                if(acc.Geographic_Coverage__c.equalsIgnoreCase(channel.Geo__c)&& acc.Region__c.equalsIgnoreCase(channel.Region__c)&& channel.Country__c==null){
                    parseString=channel.Channel_Account_Manager__c+'|'+channel.Channel_Marketing_Manager__c;
                    m_accountUpdates.put(acc.Id,parseString);
                }else if(acc.Geographic_Coverage__c.equalsIgnoreCase(channel.Geo__c)&& acc.Region__c.equalsIgnoreCase(channel.Region__c)&& 
                		channel.Country__c.contains(acc.BillingCountry)){
                    parseString=channel.Channel_Account_Manager__c+'|'+channel.Channel_Marketing_Manager__c;
                    m_accountUpdates.put(acc.Id,parseString);
                }           
            }     
        }
        if(m_accountUpdates.size()>0){
            asynchAccountUpdate(m_accountUpdates);
        }
        try{
	        PageReference listPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));        
	        listPage.setRedirect(true);           
	        return listPage;
        }catch(Exception e){
        	return null;
        }
    }
    @future
    /**
    *   Future method for asynchronous update of CAM/CMM ofpartner account as per new mapping changes.
    *   
    *   @param Map<Id,String> to be updated account map 
    *   @return void
    */
    private static void asynchAccountUpdate(Map <Id, String> accountUpdateMap){
        List <Account> accountList = new List <Account> ();
        String parseRecord ,channelAccManager , channelMarManager;        
        if (accountUpdateMap.size() > 0){
            for (Id accountId : accountUpdateMap.keyset()){
                parseRecord = accountUpdateMap.get(accountId);
                channelAccManager = parseRecord.substring(0, parseRecord.indexOf('|'));
                //system.debug('channelAccManager:'+channelAccManager);             
                channelMarManager = parseRecord.substring(parseRecord.indexOf('|')+1,parseRecord.length());
                //system.debug('channelMarManager:'+channelMarManager);
                accountList.add(New Account (Id = accountId,  Channel_Account_Manager__c = channelAccManager , Channel_Marketing_Manager__c=channelMarManager));    
            }
            update accountList;
        }                               
    }
    
    /**
	*	Code coverate test method > 90%
	*	@return void
	*/
    static testMethod void myUnitTest() {
        SelectedChannelManagerUpdate massUpdate;
        List<Channel_Manager_Assignments__c> testAssignment=new List<Channel_Manager_Assignments__c>();
        RecordType rType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
        User[] user=[select id,Name from User Limit 4];
        Channel_Manager_Assignments__c cma1 = new Channel_Manager_Assignments__c();
        cma1.Channel_Account_Manager__c=user[0].Id;
        cma1.Channel_Marketing_Manager__c=user[1].Id;
        cma1.Country__c='';
        cma1.Geo__c='EMEA';
        cma1.Region__c='Central Europe';
        testAssignment.add(cma1);
        
        Channel_Manager_Assignments__c cma2 = new Channel_Manager_Assignments__c();
        cma2.Channel_Account_Manager__c=user[2].Id;
        cma2.Channel_Marketing_Manager__c=user[3].Id;
        cma2.Country__c='';
        cma2.Geo__c='APAC';
        cma2.Region__c='India';
        testAssignment.add(cma2);
        
        insert testAssignment;
        
        Account patAcc1=new Account();
        patAcc1.RecordTypeId=rType.Id;
        patAcc1.name='stpAccount';
        patAcc1.Type='Distributor';
        patAcc1.Industry='Education';
        patAcc1.Geographic_Coverage__c='EMEA';
        patAcc1.Region__c='Central Europe';
        patAcc1.BillingCountry='';
        insert patAcc1;
        
        cma1.Channel_Account_Manager__c=user[3].Id;
        cma1.Channel_Marketing_Manager__c=user[2].Id;
        update cma1;
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(testAssignment);
		controller.setSelected(testAssignment);
		massUpdate = new SelectedChannelManagerUpdate(controller);
		massUpdate.getMySelectedSize();
		massUpdate.getMyRecordsSize();
		massUpdate.massUpdate();
    }
}