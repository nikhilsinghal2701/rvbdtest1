/*
    Class: CompetencyRollDownBatch
    Purpose: to roll down PartnerCompetency status from parent and grand parent account to child.
                Note: This class takes care of only three levels of account hirearchy. 
    Author: Jaya ( Perficient )
    Created Date: 8/04/2014                 
    Reference: pc=> Partner Competency 
*/
global without sharing class CompetencyRollDownBatch implements Database.Batchable<sObject>{
     global Set<Id> accountIds = new Set<Id>();
     global Database.QueryLocator start(Database.BatchableContext BC){
        String general_TAP = Constant.GENERAL_PARTNER_COMPETENCY;
        Id partnerRecordType = CertRollup.getRecordType(Constant.ACCOUNT_PARTNER);
        String distyTypeAcct = Constant.DISTY_ACCOUNT;
        Set<Id> relatedacctIds = new Set<Id>();// is to retrive all realted accounts
        //List<String> accIds = new List<String>{'001f000000gs90R','001f000000gs9VX','001f000000gs90v'};  AND Account__c IN: accIds // this is only for testing 
        String query = 'SELECT Id, Account__c, Account__r.Do_not_Auto_Level__c, Account__r.Do_not_share_Competencies__c, Status_Tap__c,TAP__c,'
							+' Account__r.ParentId, Account__r.Parent.Do_not_Auto_Level__c, Account__r.Parent.Do_not_share_Competencies__c,'
							+' Account__r.Parent.ParentId, Account__r.Parent.Parent.Do_not_Auto_Level__c, Account__r.Parent.Parent.Do_not_share_Competencies__c,'
							+' RSA__c,RSS__c,RTSA__c,RTSS__c,RCSA__c,RCSP__c,RCSA_RCSP__c, RSS_Needed__c, RTSS_Needed__c,RCSA_RCSP_Needed__c,'
							+' Roll_Up_RSS__c,Roll_Up_RSA__c,Roll_Up_RTSS__c,Roll_Up_RTSA__c,Roll_Up_RCSA__c,Roll_Up_RCSP__c,Roll_Up_RCSA_RCSP__c,'
							+' Account__r.Type, Account__r.Partner_Status1__c ' ;
		query += ' FROM Acct_Cert_Summary__c WHERE Account__r.RecordTypeId =: partnerRecordType'
            		+' AND Account__r.Type !=: distyTypeAcct ';
            		// TAP__c !=: general_TAP AND ==> general partner competency should also be considered for competecny roll down  
        if(!accountIds.isEmpty()){
        	relatedacctIds = CertRollupBatchHelper.getRelatedAccounts(accountIds);
        	query += ' AND Account__c IN: relatedacctIds';
        }
        return Database.getQueryLocator(query);
     }
     global void execute(Database.BatchableContext bcontext, List<sObject> scope){
        List<Acct_Cert_Summary__c> competencyList = (List<Acct_Cert_Summary__c>)scope;
        Map<Id,Acct_Cert_Summary__c> certSummaryMap = new Map<Id,Acct_Cert_Summary__c>();
        for(Acct_Cert_Summary__c pc: competencyList)
        {
            certSummaryMap.put(pc.Id,pc);
        }
        Constant.disablePartnerCompetencyTrigger = true;
        CertRollup.ROLL_UP = true;
        CompetencyRollDown.rolldownCompetencyStatus(certSummaryMap,bcontext);
        // calling old methods in batch
        /* this method is added into the roll down logic
        CertificationSummaryTriggerHandler.upsertAccountAuthorizationRecordsBasedOnTAPRequirementCriteria( certSummaryMap, competencyList);*/
		CertificationSummaryTriggerHandler.updatePartnerLevelOnRSACountChanged( certSummaryMap, competencyList,bcontext );
		/* Rev:1 not needed as the code merge is done for methods updatePartnerLevelOnRSACountChanged and updateAccountPartnerOrComplianceLevelOnCertificationCountChange into
			updatePartnerLevelOnRSACountChanged*/
		//CertificationSummaryTriggerHandler.updateAccountPartnerOrComplianceLevelOnCertificationCountChange( certSummaryMap, competencyList );
     }
     global void finish(Database.BatchableContext bcontext){}
     
     
     

}