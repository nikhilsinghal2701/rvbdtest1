/*************
Class: ContactAfterUpdate
Description: This class consists of all operations that happen on a contact record after
it is updated

Initial Version: Added method to update sticky notes on case when sticky on contact are updated
Author: Clear Task (Rucha) - 4/1/2013
**************/

public with sharing class ContactAfterUpdate {
  //Update sticky notes on case when sticky on contact are updated
  @future
  public static void updateCaseStickyNotesForContacts(Set<Id> conIdSet){
    Map<Id,Contact> conIdToConMap = new Map<Id,Contact>([Select Id, Contact_Sticky_Notes__c from Contact where Id in :conIdSet]);
    
    //Get all cases for each contact and update sticky notes
    List<Case> casesToUpdateList = new List<Case>();
    for(Case c : [Select Id, ContactId, Contact_Sticky_Notes__c from Case where ContactId in :conIdSet]){
      c.Contact_Sticky_Notes__c = conIdToConMap.get(c.ContactId).Contact_Sticky_Notes__c;
      casesToUpdateList.add(c);
    }
    
    if(!casesToUpdateList.isEmpty()){
      update casesToUpdateList;
    }
  }
  
  @isTest(SeeAllData=true)
  static void testUpdateCaseStickyNotesForContacts(){
    List<Contact> contactList = [Select Id, Contact_Sticky_Notes__c from Contact where Name in ('Ct Test 1','Ct Test 2')];
     
    
    List<Case> caseList = new List<Case>();
    
    for(Integer i=0; i<contactList.size(); i++){
      for(Integer j=0; j<5; j++){
        caseList.add(new Case(ContactId=contactList.get(i).Id,Status='New'));
      }
    }
    insert caseList;
    
    List<Contact> conToUpdateList = new List<Contact>();
    for(Integer i=0; i<contactList.size(); i++){
      Contact c1 = contactList.get(i);
      c1.Contact_Sticky_Notes__c = 'Changed in test class';
      conToUpdateList.add(c1);
    }
    
    Test.startTest();
    update conToUpdateList;
    Test.stopTest();
    
    caseList = [Select Id,ContactId,Contact_Sticky_Notes__c from Case where id=:caseList];
    for(Integer i=0; i<conToUpdateList.size(); i++){
      Contact c2 = conToUpdateList.get(i);
      for(Integer j=0; j<caseList.size(); j++){
        Case updCase = caseList.get(j);
        if(updCase.ContactId == c2.Id)
          System.assertEquals(c2.Contact_Sticky_Notes__c,updCase.Contact_Sticky_Notes__c);
      }
    }    
  }
}