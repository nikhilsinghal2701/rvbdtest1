@isTest
private class PartnerSSOPortaltest
{
 static testMethod void testMe() {
 
  Account acc = new Account();
  acc.Name = 'Test';
  acc.Industry = 'Hi-Tech';
  insert acc ;
  RecordType accRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
  Account stpAcc1=new Account(RecordTypeId=accRT.Id,name='stp1Account',Type='Distributor',Industry='Education',RASP_Status__c='Authorized',                                    
                  Partner_Level__c='Distributor',Phone='4154154156',Authorizations_Specializations__c='RASP-W',accountnumber = '123');
  insert stpAcc1;
  Opportunity opp = new Opportunity();
  opp.name = 'testopp';
  opp.accountId = acc.Id;
  opp.Primary_ISR__c = UserInfo.getUserId();
  opp.Type = 'New';
  opp.Channel__c = 'Direct';
  opp.LeadSource = 'Email';
  opp.CloseDate = System.today()+5;
  opp.StageName = '4 - Selected';
  opp.Forecast_Category__c = 'Commit';
  opp.Competitors__c = 'Citrix';
  opp.Sold_To_Partner__c=stpAcc1.Id;
  insert opp;
  
  PageReference pageRef = Page.PartnerSSO_OpportunityList;
  List<OpportunityWrapper> oppList = new List<OpportunityWrapper>();
  List<OpportunityWrapper> oppLists = new List<OpportunityWrapper>();
  Test.setCurrentPage(pageRef);
  //PartnerSSOPortalController controller = new PartnerSSOPortalController();
  //User u = [select id from User where name='Richard Harvey'];  
  //controller.stage = '';
  //controller.acc = '';
   Contact c = new Contact (accountid = stpAcc1.Id, lastname = 'con', email='test@test.com');
    insert c;
    
    stpAcc1.IsPartner = true;
    update stpAcc1;
    
   Profile p=[select id,name from profile where name='PRM - Distributor - Admin'];      
   
   User newPartner = new User(alias = 'standt', email='standarduser@testorg.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
								    contactId = c.Id, localesidkey='en_US', profileid = p.Id, 
								    timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com');   

 insert newPartner;
 System.runAs(newPartner) {
 PartnerSSOPortalController controller = new PartnerSSOPortalController();
 controller.stage = '';
 controller.acc = '';
 controller.getStages();
 controller.search();
 //oppList = controller.showList;
 oppLists = controller.selectedopp; 
 controller.ow[0].checked = true;
 controller.updateopp();
 controller.back();
 controller.stage = '4 - Selected';
 controller.acc = stpAcc1.id;
 controller.opp = 'test';
 controller.search();
}

        OpportunityWrapper cw = new OpportunityWrapper();
        System.assertEquals(cw.checked,false);      
 
        OpportunityWrapper cw2 = new OpportunityWrapper(new Opportunity (name='Test1'));
        //System.assertEquals(cw2.ops.name,'Test1');
        //System.assertEquals(cw2.checked,false);       
 
    }
    }