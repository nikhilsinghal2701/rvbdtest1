@isTest(SeeAllData=true)
private class TestOppAddDefaultSplit {

    static testMethod void insertOpp() {
        
        //User user = new User(FirstName='First', LastName = 'Last')
        //Opportunity
        User user0 = [Select Id, Username, FirstName, LastName, Email, Alias, CommunityNickname,
            TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey, IsActive 
            from User where IsActive = true and usertype != 'PowerPartner' limit 1];
        User user1 = user0.clone(false,true);
        user1.username = 'testuser1@riverbed.test';
        user1.CommunityNickname = 'testuser1.test';
        User user2 = user0.clone(false,true);
        user2.username = 'testuser2@riverbed.test';
        user2.CommunityNickname = 'testuser2.test';
        insert new List<User>{user1,user2};
        
        Account acc1 = new Account(Name='Test',BillingState='CA',BillingCountry='US', OwnerId=user2.Id);
        insert acc1;
        
       test.starttest();
        Opportunity opp = new Opportunity(Name='test', AccountId = acc1.id, StageName='Prospect',CloseDate=Date.today(),
            OwnerId = user1.Id);
        insert opp;
        /*
        opp.OwnerId=userinfo.getUserId();
        update opp;
        
        acc1.OwnerId = user1.Id;
        update acc1;
        */
        test.stoptest(); 
        
        /*Split_Commissions__c split  = [Select Id, Name, Is_Opportunity_Owner__c, Split_Comm_Rep__c 
            from Split_Commissions__c where Opportunity__c = :opp.Id limit 1];
        
        System.Debug('The Split:::: ' + split);
        System.AssertEquals(split.Split_Comm_Rep__c,opp.OwnerId); */
    }
    
    static testMethod void updateOpp() {
        
        /*User user0 = [Select Id, Username, FirstName, LastName, Email, Alias, CommunityNickname,
            TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey, IsActive 
            from User where IsActive = true and usertype != 'PowerPartner'limit 1];
            
        User user1 = user0.clone(false,true);
        user1.username = 'testuser1@test.test';
        user1.CommunityNickname = 'testuser1.test';
        User user2 = user0.clone(false,true);
        user2.username = 'testuser2@test.test';
        user2.CommunityNickname = 'testuser2.test';
        insert new List<User>{user1,user2};
                
        Account acc1 = new Account(Name='Test',BillingState='CA',BillingCountry='US', OwnerId=user1.Id, Global_Account_Manager__c = user0.Id);
        insert acc1;
        
        test.starttest();
        Opportunity opp = new Opportunity(Name='test', AccountId = acc1.id, StageName='Prospect',CloseDate=Date.today(),
        OwnerId = user1.Id);
        insert opp;
        
        
        opp.OwnerId = user2.Id;
        update opp;
        test.stoptest();
        
        Split_Commissions__c split  = [Select Id, Name, Is_Opportunity_Owner__c, Split_Comm_Rep__c 
            from Split_Commissions__c where Opportunity__c = :opp.Id limit 1];
        
        System.Debug('The Split:::: ' + split);
        System.AssertEquals(split.Split_Comm_Rep__c,opp.OwnerId);
        
        //Test Account Owner Chhangein trigger AccountOwnerChangeSplitUpdate
        acc1.OwnerId = user0.Id;
        update acc1;
        */
        User[] usr = [Select Id, Username, FirstName, LastName, Email, Alias, CommunityNickname,
            TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey, IsActive 
            from User where IsActive = true and usertype != 'PowerPartner'limit 5];
        Account acc1 = new Account(Name='Test',BillingState='CA',BillingCountry='US',Global_Account_Manager__c = usr[0].Id);
        insert acc1;
        Opportunity opp = new Opportunity(Name='test', AccountId = acc1.id, StageName='Prospect',CloseDate=Date.today());
        insert opp;
        test.starttest();
        opp.OwnerId=usr[1].Id;
        update opp;
        
        acc1.OwnerId = usr[2].Id;
        update acc1;
        test.stoptest();
    }
}