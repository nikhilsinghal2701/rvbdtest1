/*****************************************
                Name : CertificationTriggerHandler
                Purpose : Helper class to the CertificateContactInfo trigger
                Date: 10/01/2013
                Author: Rashmi ( Perficient )
****************************************/
public class CertificationTriggerHandler {


                // helper method to autopopulate certification fields from Contact on Insert
                // and update of Certification records
                public static void autoPopulateCertificationFields( Map<Id, Certificate__c> oldCertificationsMap, List<Certificate__c> newCertificates, Boolean isInsert ){
                                //list of certifications that need to be updated
                                list<Certificate__c> updatelist = new list <Certificate__c>();
                                list<string> contactIds = new list<string>();

                                //go through each certificate and see if it needs to have fields filled out
                                //if so add the cert to list and get its contact lookup id
                                for(Certificate__c c: newCertificates){
                                                if(( isInsert||(c.Contact__c != oldCertificationsMap.get(c.id).contact__c))&& c.Contact__c != null){
                                                                updateList.add(c);
                                                                contactIds.add(c.Contact__c);
                                                }
                                }

                                //make list of contacts to get fields from
                                list<Contact> contactList = [Select c.RCSP_W__c, c.RCSP_NPM__c, c.LeftCompany__c,
                                                                                                                                                                                                                                                                Support_Engineer__c
                                                                                                                                                                                                                                                                From Contact c
                                                                                                                                                                                                                                                                where id in :contactIds];

                                                //make map of contactid to contact
                                                map<string, Contact> conMap =  new map<string, Contact>();
                                                for(Contact con: contactList){
                                                                conMap.put(con.id, con);
                                                }

                                                //for each certification in update list, update fields with contact values
                                                //Contact con;
                                                for(Certificate__c cf: updateList){
                                                                Contact Con = new Contact();
                                                                        if(conMap.containsKey(cf.Contact__c)){
                                                                                if(conMap.get(cf.Contact__c) != null){
                                                                con = conMap.get(cf.Contact__c);
                                                                if(con.LeftCompany__c) { cf.Contact_Left_Account__c = 'Yes'; }
                                                                else {cf.Contact_Left_Account__c = 'No';}
                                                                cf.Support_Engineer__c = con.Support_Engineer__c;
                                                                if( con.RCSP_W__c != null) {cf.Contact_RCSP_W__c= con.RCSP_W__c;}
                                                                if( con.RCSP_NPM__c != null) {cf.Contact_RCSP_NPM__c= con.RCSP_NPM__c;}
                                                                        }
                                                                }
                                                }
                }


                // helper method to populate the certification master record
                public static void populateCertificationMasterFields( List<Certificate__c> newCertificates ){
                                Set<String> types = new Set<String>();
                                for( Certificate__c c : newCertificates ){
                                                types.add( c.Type__c );
                                }

                                Map<String, Certificate_Master__c> certMastersMap = new Map<String, Certificate_Master__c>();
                                if( !types.isEmpty() ){
                                                for( Certificate_Master__c master : [ SELECT Id, Name, Competency__c FROM Certificate_Master__c WHERE Name IN : types ] ){
                                                                certMastersMap.put( master.Name, master );
                                                }
                                                string tapValue;
                                                for( Certificate__c c: newCertificates ){
                                                                Certificate_Master__c master = certMastersMap.get( c.Type__c );
                                                                tapValue = c.TAP__c != null ? c.TAP__c.substringAfter('(').substringBefore(')') : null;

                                                                if( master != null && master.Competency__c != null &&
                                                                                ( ( c.ClassName__c != null && c.ClassName__c.contains( master.Competency__c )  ) ||
                                                                                                ( tapValue != null && master.Competency__c.contains( tapValue ) ) )){
                                                                                c.Certification_Master__c = master.Id;
                                                                }
                                                }
                                }

                }


                // helper method to calculate the total certifications of each type on the certification summary record
                // used every time a certification record is inserted or a certificate is expired
                public static void calculateTotalCertifications( List<Certificate__c> newCerts ){
                    Set<Id> certificationSummaryIds = new Set<Id>();
                    Set<Id> contactIds = new Set<Id>();
                    Map<Id, Acct_Cert_Summary__c> certSummariesToUpdateMap = new Map<Id, Acct_Cert_Summary__c>();
                    final string SUFFIX = '__c';

                    for( Certificate__c cert : newCerts ){
                        if(!cert.name.containsIgnoreCase('Cascade') && !cert.name.containsIgnoreCase('Sales Spec') ){
                            certificationSummaryIds.add( cert.Certification_Summary__c );
                            contactIds.add( cert.Contact__c );
                        }
                    }
                                if( !certificationSummaryIds.isEmpty() || !contactIds.isEmpty() ){
                                    Map<Id, Acct_Cert_Summary__c> certificationSummaries = new Map<Id, Acct_Cert_Summary__c>( [ SELECT Id, RCSA__c, RCSP__c, RTSS__c, RSA__c, RSS__c, RTSA__c,Support_RCSA_RCSP__c, RCSA_RCSP__c,
                                                                                            ( SELECT Id, Contact__c, Name, IsExpired__c,Certification_Summary__c, Type__c, TAP__c,Status__c, Contact_Left_Account__c,Certification_Master__r.Support_Provided__c,
                                                                                            Contact__r.Support_Engineer__c FROM Certificates__r
                                                                                            //WHERE isExpired__c = false 
                                                                                            ORDER BY CreatedDate ASC )FROM Acct_Cert_Summary__c WHERE Id IN : certificationSummaryIds ] );
                                    Acct_Cert_Summary__c certSummary;
                                    Integer certCount;
                                    string key;

                                    Map<String, Certificate__c> uniqueCertsPerContact = new Map<String, Certificate__c>();
                                    for( Acct_Cert_Summary__c certSum : certificationSummaries.values() ){
                                        for( Certificate__c existingCert : certSum.Certificates__r ){
                                            //system.debug(LoggingLevel.ERROR,'***existingCert:'+existingCert);
                                            if((existingCert.Status__c == 'Complete' || existingCert.Status__c == 'Expired' ) && !existingCert.name.containsIgnoreCase('Cascade') 
                                            && !existingCert.name.containsIgnoreCase('Sales Spec')  /*&& existingCert.Contact_Left_Account__c == 'No'*/ ){//commented by prashant.singh@riverbed.com on 11/22/2013 for JIRA#PARPRG-93
                                            // if an individual has both RCSA and RCSP count them only once
                                            key = existingCert.Type__c + existingCert.TAP__c + existingCert.Contact__c ;
                                                if( !uniqueCertsPerContact.containsKey( key ) ){
                                                    uniqueCertsPerContact.put( key, existingCert );
                                                }else{
                                                    //if( uniqueCertsPerContact.get( key ).isExpired__c ) -- added to the below condition
                                                    if( uniqueCertsPerContact.get( key ).Status__c=='Expired' || uniqueCertsPerContact.get( key ).isExpired__c){
                                                        uniqueCertsPerContact.put( key, existingCert );
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //system.debug(LoggingLevel.ERROR,'***uniqueCertsPerContact:'+uniqueCertsPerContact);
                                    Set<String> uniqueKeysToIgnore = new Set<String>();
                                    Map<String, Acct_Cert_Summary__c> uniqueCertSummaryContactMap = new Map<String, Acct_Cert_Summary__c>();
                                    for( Certificate__c cert : uniqueCertsPerContact.values() ){
                                        if( !certSummariesToUpdateMap.containsKey( cert.Certification_Summary__c ) ){
                                            certSummary = new Acct_Cert_Summary__c( Id = certificationSummaries.get( cert.Certification_Summary__c ).Id,
                                                        RCSA__c = 0, RCSP__c = 0, RTSS__c = 0, RSA__c = 0, RSS__c = 0, RTSA__c = 0,RCSA_RCSP__c = 0 );
                                            certCount = 0;
                                            //system.debug(LoggingLevel.ERROR,'***certCount1:'+certCount );
                                            //system.debug(LoggingLevel.ERROR,'***certSummary1:'+certSummary);
                                        }else{
                                            certSummary = certSummariesToUpdateMap.get( cert.Certification_Summary__c );
                                            try{
                                                certCount = certSummary.get( cert.Type__c + SUFFIX ) == null ? 0 : Integer.valueof( certSummary.get( cert.Type__c + SUFFIX ) );
                                            }catch(Exception e){
                                                system.debug(LoggingLevel.ERROR,'Exception: '+e );
                                                certCount = 0;
                                            }
                                            //system.debug(LoggingLevel.ERROR,'***certCount2:'+certCount );
                                            //system.debug(LoggingLevel.ERROR,'***certSummary2:'+certSummary);
                                        }
                                        //system.debug(LoggingLevel.ERROR,'***cert:'+cert );
                                        if(cert.Contact_Left_Account__c != 'yes'){//Added by prashant.singh@riverbed.com on 11/22/2013 for JIRA#PARPRG-93
                                            if(!cert.IsExpired__c){
                                                if(cert.TAP__c.equalsIgnoreCase('APM') && cert.Type__c.equalsIgnoreCase('RTSS')){

                                                } else if( (cert.TAP__c.equalsIgnoreCase('APM') || cert.TAP__c.equalsIgnoreCase('NPM')) &&
                                                    (cert.Type__c.equalsIgnoreCase('RCSA') || cert.Type__c.equalsIgnoreCase('RCSP') ) ){//AND Condition Added by nikhil
                                                    if(cert.TAP__c.equalsIgnoreCase('APM')){
                                                        key = cert.Type__c + 'NPM' + cert.Contact__c ;
                                                        //system.debug( key );
                                                        if( uniqueCertsPerContact.containsKey( key ) && !uniqueCertsPerContact.get( key ).IsExpired__c && !uniqueKeysToIgnore.contains( key ) ){
                                                            certCount = certCount + 1;
                                                            uniqueKeysToIgnore.add( key );
                                                            uniqueKeysToIgnore.add( cert.Type__c + cert.TAP__c + cert.Contact__c  );
                                                        }else{//added by psingh@riverbed.com on 4/21/2014
                                                            key = cert.Type__c + 'APM' + cert.Contact__c ;
                                                            //system.debug(LoggingLevel.ERROR,'***Key1:'+key );
                                                            if( !uniqueCertsPerContact.containsKey( key )){
                                                                certCount = certCount + 1;
                                                                //system.debug(LoggingLevel.ERROR,'***certCount1:'+certCount);
                                                                uniqueKeysToIgnore.add( key );
                                                                uniqueKeysToIgnore.add( cert.Type__c + 'NPM' + cert.Contact__c  );  
                                                            }else if( uniqueCertsPerContact.containsKey( key )&& !uniqueCertsPerContact.get( key ).IsExpired__c && !uniqueKeysToIgnore.contains( key )){
                                                                certCount = certCount + 1;
                                                                //system.debug(LoggingLevel.ERROR,'***certCount2:'+certCount);
                                                                uniqueKeysToIgnore.add( key );
                                                                uniqueKeysToIgnore.add( cert.Type__c + 'NPM' + cert.Contact__c  );  
                                                            }
                                                        }//End
                                                        //system.debug(LoggingLevel.ERROR,'***uniqueKeysToIgnore:'+uniqueKeysToIgnore );
                                                    }else{
                                                        key = cert.Type__c + 'APM' + cert.Contact__c ;
                                                        //system.debug( key );
                                                        if( uniqueCertsPerContact.containsKey( key ) && !uniqueCertsPerContact.get( key ).IsExpired__c && !uniqueKeysToIgnore.contains( key ) ){
                                                            certCount = certCount + 1;
                                                            uniqueKeysToIgnore.add( key );
                                                            uniqueKeysToIgnore.add( cert.Type__c + cert.TAP__c + cert.Contact__c  );
                                                        }else{//added by psingh@riverbed.com on 4/21/2014
                                                            key = cert.Type__c + 'NPM' + cert.Contact__c ;
                                                            //system.debug(LoggingLevel.ERROR,'***Key2:'+key );
                                                            if( !uniqueCertsPerContact.containsKey( key )){
                                                                certCount = certCount + 1;
                                                                //system.debug(LoggingLevel.ERROR,'***certCount3:'+certCount);
                                                                uniqueKeysToIgnore.add( key );
                                                                uniqueKeysToIgnore.add( cert.Type__c + 'APM' + cert.Contact__c  );
                                                            }else if( uniqueCertsPerContact.containsKey( key )&& !uniqueCertsPerContact.get( key ).IsExpired__c && !uniqueKeysToIgnore.contains( key )){
                                                                certCount = certCount + 1;
                                                                //system.debug(LoggingLevel.ERROR,'***certCount4:'+certCount);
                                                                uniqueKeysToIgnore.add( key );
                                                                uniqueKeysToIgnore.add( cert.Type__c + 'APM' + cert.Contact__c  );
                                                            }       
                                                        }//End
                                                        //system.debug(LoggingLevel.ERROR,'***uniqueKeysToIgnore:'+uniqueKeysToIgnore );
                                                    }
                                                }else {
                                                    certCount = certCount + 1 ;
                                                }
                                            }
                                        }//End:addeded by prashant.singh@riverbed.com on 11/22/2013 for JIRA#PARPRG-93
                                        try{
                                            certSummary.put( cert.Type__c + SUFFIX, certCount );
                                        }catch(Exception e){
                                            system.debug(LoggingLevel.ERROR,'Exception: '+e );
                                        }
                                        if( (cert.TAP__c.equalsIgnoreCase('APM') || cert.TAP__c.equalsIgnoreCase('NPM')) &&
                                            (cert.Type__c.equalsIgnoreCase('RCSA') || cert.Type__c.equalsIgnoreCase('RCSP') )&& !cert.IsExpired__c && (cert.Contact_Left_Account__c==null || cert.Contact_Left_Account__c.equalsIgnoreCase('No'))){
                                            if( !uniqueCertSummaryContactMap.containsKey( cert.Contact__c + '-' + cert.Certification_Summary__c ) && (uniqueKeysToIgnore.contains(cert.Type__c + 'APM' + cert.Contact__c) || 
                                            uniqueKeysToIgnore.contains(cert.Type__c + 'NPM' + cert.Contact__c))){                                              
                                                certSummary.RCSA_RCSP__c = certSummary.RCSA_RCSP__c == null? 0: certSummary.RCSA_RCSP__c + 1;
                                                uniqueCertSummaryContactMap.put( cert.Contact__c + '-' + cert.Certification_Summary__c, certSummary );
                                            }
                                            if( cert.Certification_Master__r.Support_Provided__c && cert.Contact__r.Support_Engineer__c ){
                                                certSummary.put( 'Support_' + cert.Type__c + SUFFIX, certSummary.get( cert.Type__c + SUFFIX ) );
                                                certSummary.put( 'Support_RCSA_RCSP__c', certSummary.get( 'RCSA_RCSP__c' ) );
                                            }
                                        }else if( (cert.Type__c == 'RCSA' || cert.Type__c == 'RCSP') && !cert.IsExpired__c && (cert.Contact_Left_Account__c==null ||cert.Contact_Left_Account__c.equalsIgnoreCase('No'))){
                                            if( !uniqueCertSummaryContactMap.containsKey( cert.Contact__c + '-' + cert.Certification_Summary__c ) ){
                                                certSummary.RCSA_RCSP__c = certSummary.RCSA_RCSP__c == null? 0: certSummary.RCSA_RCSP__c + 1;
                                                uniqueCertSummaryContactMap.put( cert.Contact__c + '-' + cert.Certification_Summary__c, certSummary );
                                            }
                                            if( cert.Certification_Master__r.Support_Provided__c && cert.Contact__r.Support_Engineer__c ){
                                                certSummary.put( 'Support_' + cert.Type__c + SUFFIX, certSummary.get( cert.Type__c + SUFFIX ) );
                                                certSummary.put( 'Support_RCSA_RCSP__c', certSummary.get( 'RCSA_RCSP__c' ) );
                                            }
                                        }
                                        certSummariesToUpdateMap.put( certSummary.Id, certSummary );
                                    }
                        if( !certSummariesToUpdateMap.isEmpty() ){
                            try{
                                update certSummariesToUpdateMap.values();
                            }catch( Dmlexception ex ){
                                Acct_Cert_Summary__c summary;
                                for( Integer i=0; i< ex.getNumDml(); i++ ){
                                    //summary = certSummariesToUpdateMap.values()[i];
                                    //newCerts.get( summary.)
                                }
                            }
                        }
                    }
                }


                public static List<Certificate__c> getExpiredCerts( Map<Id, Certificate__c> oldCertsMap, List<Certificate__c> newCerts ){
                                Certificate__c oldCert;
                                List<Certificate__c> certs = new List<Certificate__c>();
                                for( Certificate__c cert : newCerts ){
                                                oldCert = oldCertsMap.get( cert.Id );
                                                if((cert.IsExpired__c != oldCert.IsExpired__c && cert.IsExpired__c) || (cert.Contact_Left_Account__c!=oldCert.Contact_Left_Account__c) ||(cert.Certification_Summary__c!=oldCert.Certification_Summary__c)){//commented by prashant.singh@riverbed.com on 11/22/2013 for JIRA#PARPRG-93
                                                                certs.add( cert );
                                                }
                                }
                                return certs;
                }


                public static void calculateTotalCertificationsOnUpdate( Map<Id, Certificate__c> oldCertsMap, List<Certificate__c> newCerts ){
                                List<Certificate__c> expiredCerts = getExpiredCerts( oldCertsMap, newCerts );
                                calculateTotalCertifications( expiredCerts );
                }

                //Added by prashant.singh@riverbed.com on 11/22/2013 for JIRA#PARPRG-95
                public static void recalculateTotalCertificationOnContactAccountChange(set<Id> accountIds){
                                List<Certificate__c>certs=new List<Certificate__c>();
                                List<Acct_Cert_Summary__c>updateAccCertSummaryList=new List<Acct_Cert_Summary__c>();
                                set<Id>noCertAccount=new set<Id>();
                                Acct_Cert_Summary__c certSummary;
                                Map<Id,List<Certificate__c>>accountCertMap=new Map<Id,List<Certificate__c>>();
                                Map<Id,List<Acct_Cert_Summary__c>>accountCertSummaryMap=new Map<Id,List<Acct_Cert_Summary__c>>();

                                List<Certificate__c>newCerts = [Select c.name,c.Contact__c, c.Certification_Summary__r.TAP__c,c.Certification_Master__c,
                                                                                                                                                                c.Certification_Summary__r.Id, c.Certification_Summary__c,c.Contact_Left_Account__c,c.IsExpired__c,
                                                                                                                                                                c.Contact__r.AccountId,c.Status__c
                                                                                                                                                                From Certificate__c c
                                                                                                                                                                where c.Contact__r.AccountId IN :accountIds];
                                for( Certificate__c cert : newCerts ){
                                                if(!cert.IsExpired__c || (cert.Contact_Left_Account__c=='No')){
                                                                certs.add( cert );
                                                }
                                }
                                if(!certs.isEmpty() && certs.size()>0){
                                                calculateTotalCertifications(certs);
                                }
                                //Start:Added by prashant.singh@riverbed.com on 12/11/2013 for JIRA#PARPRG-179
                                for(Certificate__c cert : newCerts){
                                                if( !accountCertMap.containsKey(cert.Contact__r.AccountId) ){
                                                                accountCertMap.put(cert.Contact__r.AccountId, new List<Certificate__c> { cert } );
                                                }else{
                                                                accountCertMap.get(cert.Contact__r.AccountId).add( cert );
                                                }
                                }
                                if(!accountCertMap.isEmpty() && accountCertMap.size()>0){
                                                for(Id i:accountIds){
                                                                if(!accountCertMap.containsKey(i)){
                                                                                noCertAccount.add(i);
                                                                }
                                                }
                                }
                                if(!noCertAccount.isEmpty() && noCertAccount.size()>0){
                                                List<Acct_Cert_Summary__c> accCertSumList=[select id,Account__c from Acct_Cert_Summary__c where Account__c IN:noCertAccount];
                                                for(Acct_Cert_Summary__c temp:accCertSumList){
                                                                if( !accountCertSummaryMap.containsKey(temp.Account__c) ){
                                                                                accountCertSummaryMap.put(temp.Account__c, new List<Acct_Cert_Summary__c> { temp } );
                                                                }else{
                                                                                accountCertSummaryMap.get(temp.Account__c).add( temp );
                                                                }
                                                }
                                }
                                if(!accountCertSummaryMap.isEmpty() && accountCertSummaryMap.size()>0){
                                                for(Id aId:noCertAccount){
                                                                if(accountCertSummaryMap.containsKey(aId)){
                                                                                for(Acct_Cert_Summary__c temp:accountCertSummaryMap.get(aId)){
                                                                                                certSummary=new Acct_Cert_Summary__c(id=temp.Id,RSA__c=0,RSS__c=0,RTSA__c=0,RTSS__c=0,RCSA__c=0,RCSP__c=0);
                                                                                                updateAccCertSummaryList.add(certSummary);
                                                                                }
                                                                }
                                                }
                                }
                                if(!updateAccCertSummaryList.isEmpty() && updateAccCertSummaryList.size()>0){
                                                update updateAccCertSummaryList;
                                }
                                //End:Added by prashant.singh@riverbed.com on 12/11/2013 for JIRA#PARPRG-179
                }
}