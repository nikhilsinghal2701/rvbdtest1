@isTest (SeeAllData=true)
private class TestMailToGeneratorCtrl{

    static testMethod void TestMailMethod() {
        Contact cont = [SELECT name FROM Contact LIMIT 1];
       
        Case cs = new Case(ContactID = cont.id, Product__c = 'CMC', status = 'New', Priority = 'P2 - Serious', Origin = 'Email' );
        insert cs;
        
        /*Contact cont = new Contact(firstname = 'FirstTest', lastName = 'LastTest');
        insert cont;*/
        
        CaseTeamRole ctr = [SELECT id FROM CaseTeamRole WHERE name = 'CCed on Email'];
        
        /*CaseTeamRole ctr = new CaseTeamRole(name = 'CCed on Email');
        insert ctr;*/
        
        CaseTeamMember ctm = new CaseTeamMember(parentid = cs.id, memberid = cont.id, TeamRoleId = ctr.id);
        insert ctm;
              
        ApexPages.StandardController sc = new ApexPages.StandardController(cs);
        MailToGeneratorCtrl mgControl = new MailToGeneratorCtrl(sc);
       
        mgControl.getMailtoLink();  
    }   
}