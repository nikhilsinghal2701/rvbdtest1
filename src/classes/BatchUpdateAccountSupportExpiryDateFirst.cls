global class BatchUpdateAccountSupportExpiryDateFirst implements Database.Batchable<Sobject>{
  global final List<Account> AccountToUpdate=new List<Account>();
  global final String query;
  global String errorMsgs = '<b>Account Update Record Log</b><br>';
  global BatchUpdateAccountSupportExpiryDateFirst() {
    query='Select Id,RecordTypeId,CreatedDate from Account WHERE (IsDeleted = False)';
    //query = 'SELECT Id,RecordTypeId,createdDate from Account where Id =\'' + '0013000000ERmi8' + '\'';
  }
  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext BC, List<sObject> records) {
   /* AccountUtil aUtil=new AccountUtil();
  // Map<Id,Date> accMap=aUtil.getAllAssetByAccount(records);
    Date dt;
    if(accMap.size()>0){
        for(sObject so:records){
            try{
                Account a = (Account)so;
                system.debug('Account:'+a);
                dt=accMap.get(a.Id);
                if(dt==null){
                    a.Support_Site_Access_Expiry_Date__c=dt;
                }else if(dt!=null && dt==a.CreatedDate.date()){
                    a.Support_Site_Access_Expiry_Date__c=dt;
                }else if(dt!=null && dt<=System.today().addDays(41)){
                    a.Support_Site_Access_Expiry_Date__c=System.today().addDays(41);
                }else if(dt!=null && dt>System.today().addDays(41)){
                    a.Support_Site_Access_Expiry_Date__c=dt;
                }
                AccountToUpdate.add(a);
            }catch(Exception e){
                // Do nothing
            }
        }
        
        if(AccountToUpdate.size()>0){
             //update AccountToUpdate;
            Database.SaveResult[] lsr = Database.update(AccountToUpdate,false);
            system.debug('saveresult:'+lsr[0]);
            Integer recordid = 0;
            for (Database.SaveResult SR : lsr) {
                if (!SR.isSuccess()) {
                    this.errorMsgs += 'Account Id: ' +AccountToUpdate[recordid].id+'|'+'Error: '+SR.getErrors()[0].getMessage()+'<br>';
                }
            recordid++;
            }
        }   
    }  */  
  }
  global void finish(Database.BatchableContext BC) {
  }
}