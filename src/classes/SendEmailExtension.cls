/*
* Send an email to the contacts who have the Partner_Win_Story_Contact__c = true 
*/
public class SendEmailExtension {

    private ApexPages.StandardController controller;
    public List<SelectOption> emailOptions {get; set;}
    public String[] selectedEmail {get; set;}
    public String msg {get; set;}
    public Boolean emailSelected {get; set;}
    //public Boolean isEligible {get; set;}
    
    public Map<String, Id> mapEmailContactId = new Map<String, Id>();
    public String distributionList;
    
    public SendEmailExtension(ApexPages.StandardController controller) {
       this.controller = controller;
       //isEligible = true;
       //checkEligibility();
       Partner_Win_Story__c pws = (Partner_Win_Story__c)controller.getRecord();
       if(!pws.Sent_to_Distribution__c) {
           emailOptions = new List<SelectOption>();
           createEmailOptions();
       }
    }
    
    public void createEmailOptions() {
        
       List<Contact> conList = [select Email from Contact where Partner_Win_Story_Contact__c = true AND Email != null];
       
       for(Contact con :conList) {
           mapEmailContactId.put(con.Email, con.id);
       }
       
       for(String email :mapEmailContactId.keySet()) {
           String emailLabel = email;
           if(emailLabel.endswith('@riverbed.com')) {
               emailLabel = emailLabel.replace('@riverbed.com', '');
           }
           emailOptions.add(new SelectOption(email, emailLabel));
       }
    }
    
    /*public void checkEligibility() {
        Partner_Win_Story__c pws = [select Sent_to_Distribution__c from Partner_Win_Story__c where id = :this.controller.getId()];
        if(pws.Sent_to_Distribution__c) {
            isEligible = false;
            ApexPages.addMessage(new Apexpages.Message(ApexPAges.Severity.ERROR, 'You have already sent email to distribution list.'));
        }
    }*/
    
    public PageReference confirm() {
        if(selectedEmail == null || selectedEmail.size() == 0) {
            ApexPages.addMessage(new Apexpages.Message(ApexPAges.Severity.ERROR, 'Please select atleast one email.'));
            return null;
        }
        
        emailSelected = true;
        msg = 'You are going to send email to ';
        distributionList = '';
        
        for(String s :selectedEmail) {
            String emailLabel = s;
            if(s.endswith('@riverbed.com')) {
                emailLabel = s.replace('@riverbed.com', '');
            }
            distributionList += emailLabel + ', ';
        }
        
        if(distributionList.length() > 1)
            distributionList = distributionList.substring(0, distributionList.length() - 2);
       
        msg += distributionList;
        
        return null;
    }
    
    public PageReference backtoDistList() {
        //selectedEmail = new List<String>();
        emailSelected = false;
        return null;
    }
    
    public PageReference sendToDistribution() {
        EmailTemplate template = [Select e.Name, e.OwnerId From EmailTemplate e where Name = :System.Label.Partner_Story_Email_Template];
            
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage email = createEmail(this.controller.getId(), template);
        mails.add(email);
        List<String> toAddresses = new List<String>();
        
        Boolean targetObjectSet = false; //need to set one contact as target and rest as toAddresses
        
        for(String s :selectedEmail) {
            if (!targetObjectSet && mapEmailContactId != null && mapEmailContactId.containsKey(s)) {
                email.setTargetObjectId(mapEmailContactId.get(s));
                targetObjectSet = true;
            } else {
                toAddresses.add(s);
            }
        }
        email.setToAddresses(toAddresses);
        
        User u = [select Email From User where id = :UserInfo.getUserId()];
        if(u.Email != null) {
            email.setCcAddresses(new String[] { 
              u.Email 
            });
        }
        
        Partner_Win_Story__c story = new Partner_Win_Story__c(
            Id = this.controller.getId(),
            Date_Sent__c = System.now(),
            Distribution_List__c = distributionList,
            Sent_to_Distribution__c = true
        );
            
        try {
            if (targetObjectSet) {
                Messaging.SendEmailResult [] r = Messaging.sendEmail(mails);
            }
            
            update story;
            
            return controller.view();
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
        return null;
    }  
    
    public Messaging.SingleEmailMessage createEmail(Id sObjectId, EmailTemplate template)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(template.id);
        email.setSaveAsActivity(false);
        email.setWhatId(sObjectId);
        return  email;
       
    } 
}