public without sharing class Generator
{
	public static Boolean isTest=false;
    public static String getNewPassword()
    {
        String passwd = '';
        String[] chars = new String[] {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                       'a','b','c','d','e','f','g','h','h','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                                       '0','1','2','3','4','5','6','7','8','9'  };            

        for(Integer i=0; i<8; i++)
        {
            passwd += chars[(Math.random() * 62).intValue()];
        }
        
        return passwd;
    }
    
    public static String getNewCustomerNumber(String startLetter)
    {
        String[] alphabet = new String[] {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' };
        String customerNumber = startLetter.toUpperCase();
        
        while(true)
        {
            Integer pos2 = (Math.random() * 26).intValue();
            Integer pos3 = (Math.random() * 26).intValue();
            Integer pos4 = (Math.random() * 26).intValue();
            Integer num = (Math.random() * 898).intValue() + 101;
            
            customerNumber += alphabet[pos2] + alphabet[pos3] + alphabet[pos4] + ' ' + num;
            System.debug('Generated account number [' + customerNumber + ']');
            
            if(startLetter.toUpperCase() == 'D')
            {
               // if([ SELECT count() FROM Developer__c WHERE Name = :customerNumber ] == 0)
               // {
                    System.debug('Account number is unique!');
                    
                    if((! customerNumber.startsWith('DICK'))
                    && (! customerNumber.startsWith('DAGO'))
                    && (! customerNumber.startsWith('DOLT'))
                    && (! customerNumber.startsWith('DORK'))
                    && (! customerNumber.startsWith('DAFT'))
                    && (! customerNumber.startsWith('DAMN'))
                    && (! customerNumber.startsWith('DOPE'))
                    && (! customerNumber.startsWith('DYKE'))
                    && (! customerNumber.startsWith('DONG'))
                    && (! customerNumber.startsWith('DUMB')))
                    {
                        return customerNumber;
                    }
                    else
                    {
                        System.debug('Account number is potentially offensive!');
                    }
                //}
            }
            else{
            	//if([ SELECT count() FROM Account WHERE Type = 'Customer EC2' AND AccountNumber != null AND AccountNumber = :customerNumber ] == 0)//AND AccountNumber != null
            	//if(!ZeusUtility.checkCustomerNumber(customerNumber))
                //{
                    System.debug('Account number is unique!');
                    
                    if((! customerNumber.startsWith('ACNE'))
                    && (! customerNumber.startsWith('AMEN'))
                    && (! customerNumber.startsWith('ANAL'))
                    && (! customerNumber.startsWith('ANUS'))
                    && (! customerNumber.startsWith('ARSE'))
                    && (! customerNumber.startsWith('COCK'))
                    && (! customerNumber.startsWith('CUNT'))
                    && (! customerNumber.startsWith('CLIT'))
                    && (! customerNumber.startsWith('CRAP'))
                    && (! customerNumber.startsWith('CRUD'))
                    && (! customerNumber.startsWith('COON'))
                    && (! customerNumber.startsWith('CHAV'))
                    && (! customerNumber.startsWith('SPAZ'))
                    && (! customerNumber.startsWith('SCUD'))
                    && (! customerNumber.startsWith('SCUM'))
                    && (! customerNumber.startsWith('SEX'))
                    && (! customerNumber.startsWith('SLAG'))
                    && (! customerNumber.startsWith('SLUT'))
                    && (! customerNumber.startsWith('SODS'))
                    && (! customerNumber.startsWith('SPIK'))
                    && (! customerNumber.startsWith('SHIT')))
                    {
                        return customerNumber;
                    }
                    else
                    {
                        System.debug('Account number is potentially offensive!');
                    }
               // }
            }
        }
        
        return 'Error!';
    }
}