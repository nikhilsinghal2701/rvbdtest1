global class AccountHierarchyScheduler implements Schedulable 
{
  global void execute(SchedulableContext SC) 
  {
    Id recTypeid = '012300000000NE5';
    String query = 'SELECT id, name, OneSource__OSKeyID__c, OS_Parent_Key_ID__c,OS_Ultimate_Parent_Key_ID__c FROM Account where recordtypeid != \'rectypeid\'';
    
    AccountHierarchy accHierarchy = new AccountHierarchy();
    accHierarchy.query = query;
    database.executebatch(accHierarchy);
   }
}