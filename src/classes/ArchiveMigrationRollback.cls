global class ArchiveMigrationRollback implements Database.Batchable <Sobject>{
    
      global Final String query;
    
    global ArchiveMigrationRollback()
        {
            String ids='\''+'0068000000S6aIF'+'\'';
           DateTime dt = Date.today().addYears(-2);
           String now_datetime_as_string = dt.format('yyyy-MM-dd')+'T'+dt.format('HH:mm:ss')+dt.format('Z').substring(0, dt.format('Z').length()-2)+':00';
           // dt = DateTime.valueof(now_datetime_as_string);
           query = 'Select Id,StageName, CreatedDate, (Select Id From R00NS0000000MJ1pMAG__r), (Select Id, Name, CurrencyIsoCode, Category_Master__c, Date_Approved__c, Discount__c, Disti_Discount__c, Disti_Special_Discount__c, IsOpp__c, Opportunity__c, Special_Discount__c, Uplift__c From Discount_Detail_Archive__r) From Opportunity where createddate<'+now_datetime_as_string+' and (StageName!=\'6 - Order Accepted\' or StageName!=\'7 - Closed (Not Won)\')';
            
        }  
        
        global ArchiveMigrationRollback(String ids)//Called from test method
        {
            ids='\''+ids+'\'';
          
           query = 'Select Id, CreatedDate, (Select Id From R00NS0000000MJ1pMAG__r), (Select Id, Name, CurrencyIsoCode, Category_Master__c, Date_Approved__c, Discount__c, Disti_Discount__c, Disti_Special_Discount__c, IsOpp__c, Opportunity__c, Special_Discount__c, Uplift__c From Discount_Detail_Archive__r) From Opportunity where id='+ids;
            
        }  
          
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<Opportunity> queryList){
        List<Discount_Detail__c> detailList = new List<Discount_Detail__c>();
        List<Discount_Detail_Archive__c> archivelist = new List<Discount_Detail_Archive__c> ();
        List<Discount_Detail_Archive__c> quoteDetailList = new List<Discount_Detail_Archive__c> ();
        Set<ID> archive = new Set<Id>();
        
        Set<Id> quoteIds = new Set<ID>();
        for(Opportunity op :queryList)  
        {
            for(Discount_Detail_Archive__c d :op.Discount_Detail_Archive__r)
            {
                Discount_Detail__c dc = new Discount_Detail__c();
                dc.Date_Approved__c=d.Date_Approved__c;
                dc.Discount__c=d.Discount__c;
                dc.Category_Master__c=d.Category_Master__c;
                dc.IsOpp__c=d.IsOpp__c;
                dc.Opportunity__c=d.Opportunity__c;
                dc.Special_Discount__c=d.Special_Discount__c;
                dc.Uplift__c=d.Uplift__c;
                if(!archive.contains(d.ID))
                {
                    detailList.add(dc);
                    archiveList.add(d);
                    archive.add(d.Id);
                }
            }
            for(Quote__C q :op.R00NS0000000MJ1pMAG__r)
            {
                quoteIds.add(q.Id);
                
            }
        }
        quoteDetailList=[Select Id, Name, CurrencyIsoCode, Category_Master__c, Date_Approved__c, Discount__c, Disti_Discount__c, Disti_Special_Discount__c, IsOpp__c, Opportunity__c, Special_Discount__c, Uplift__c,quote__c From Discount_Detail_Archive__c where quote__C in :quoteIds];
        for(Discount_Detail_Archive__c d :quoteDetailList)
            {
                Discount_Detail__c dc = new Discount_Detail__c();
                dc.Date_Approved__c=d.Date_Approved__c;
                dc.Discount__c=d.Discount__c;
                dc.Category_Master__c=d.Category_Master__c;
                dc.IsOpp__c=d.IsOpp__c;
                dc.Quote__c=d.Quote__c;
                dc.Special_Discount__c=d.Special_Discount__c;
                dc.Uplift__c=d.Uplift__c;
                if(!archive.contains(d.ID))
                {
                    detailList.add(dc);
                    archiveList.add(d);
                    archive.add(d.Id);
                }
            }
            if(archiveList!=null)
            delete(archiveList);
            if(detailList!=null)
            insert(detailList);
            
    
    }
    
     global void finish(Database.BatchableContext BC){

   }

/* moving test method to ArchiveMigrationTest class //by psingh@riverbed.com
    static testmethod void rollbackTest()
    {
        test.starttest();
     List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
       
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        Quote__C q = DiscountApprovalTestHlp.createQuote(opp.Id);
         ArchiveMigrationRollback cls1 = new ArchiveMigrationRollback(); 
          ArchiveMigrationClass clss = new ArchiveMigrationClass(opp.Id); 
        database.executebatch(clss);
        ArchiveMigrationRollback cls = new ArchiveMigrationRollback(opp.Id); 
        database.executebatch(cls);
            test.stoptest();
    }
    */

}