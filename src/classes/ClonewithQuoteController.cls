/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
public with sharing class ClonewithQuoteController {
    
    private ApexPages.StandardController controller {get; set;}
    private Opportunity opp {get;set;}
    public Opportunity opportunity{get;set;}
    string Id;
    public boolean render{ get; set; }
    public Transient String errorMsg{get;set;}
    public Transient String oppLink{get;set;}
    private static string algorithmName='MD5';
    private Transient String addString='';
    public String url2 {get;set;}
    public Transient String urls {get;set;}
    public string ownerName {get;set;}
    private List<Discount_Detail__c> detailList;
    public ID newRecordId {get;set;}
    
    public  Id recId;      
    private PageReference retPage;
 
    public String selectedOpnetGrpId{get;set;}
    public Boolean opnetAccReq{get;set;}
    
    
    
    public ClonewithQuoteController(ApexPages.StandardController controller) { 
        recId = controller.getId();
        //urls = '';
        url2=String.valueOf(URL.getSalesforceBaseUrl().toExternalForm());
        render=true;
        opp = (Opportunity)controller.getRecord();
        String newOPID=
        Id = ApexPages.currentPage().getParameters().get('id');
        this.opportunity=(Opportunity)controller.getRecord();
       
        if(Id!=null){
            opportunity=[Select Id,AccountId,Account.Name,Bake_Off__c,Cascade_Rep__c,Competitors__c,Deal_Initiator__c,Uplift_Calculation__c,             
            Description,RTA_partner_influence__c,Federal_deal__c,GAM_Email__c,Federal__c,Influence_Partner__c,
            Key_Applications_by_name__c,LeadSource,Mid_Market_Account__c,
            Recommendations_to_Complete_Pilot__c,Opportunity_Disposition__c,Opportunity_Disposition_RD__c,Name,
            Partner_Involvement__c,Partner_Leverage__c,Reseller_Sales_Rep__c,Partner_Sales_Rep_Phone__c,
            Partner_SE_Involvement__c,Reseller_with_No_Account_Record__c,Pre_Qualified_by__c,Pre_Qualified_Date__c,
            Primary_App__c,Primary_Contact__c,Primary_Contact_Role__c,Primary_ISR__c,Segment__c,QL_Date__c,Registered_Deal__c,
            Registered_Partner__c,Registration_Period__c,Sale_Type__c,SecondaryApplication__c,Sold_to_Partner__c,Sold_to_Partner_Phone__c,
            Sold_To_Partner_User__c,Tier2__c,Tier2_Partner_Phone__c,lastmodifieddate,Tier2_Partner_User__c,Tier3_Partner__c,Trial_Program__c,Type,Whitewater_Storage_Provider__c,
            Sold_to_Partner_Geo__c,Channel__c,Support_Provider__c,CloseDate,StageName,Opnet_Group_Id__c,Product_Specialist_2__c,RD_RVP_Comments__c,Product_Specialist_4__c,Product_Specialist_3__c
            from Opportunity where Id=:opp.Id];
            system.debug(LoggingLevel.Error,'Opportunity = :'+opportunity);
        }     
        ownerName=userinfo.getName();
        opportunity.name='Clone of '+opportunity.Name;
       // opportunity.Support_Provider__c=opportunity.Support_Provider__c;
        opportunity.StageName='0 - Prospect';
        opportunity.CloseDate=system.today().addMonths(1);
        opportunity.Type='Existing';
        if (opportunity.Channel__c == 'VAR' || opportunity.Channel__c == 'Reseller' || opportunity.Channel__c == 'Distributor' || opportunity.Channel__c == 'SI/SP'){
            opportunity.Channel__c=null;
        }
        system.debug(LoggingLevel.Error,'Opportunity1 = :'+opp);
    }
    
     public PageReference Save() { 
        Opportunity newOpp; 
        newOpp = opportunity.clone(false);
        insert newOpp;
        system.debug(LoggingLevel.Error,'New Opportunity = :'+newOpp);
        newRecordId = newOpp.id; 
        List<Id> oppIds = new List<Id>();
        oppIds.add(newRecordId);
        List<String> result=DiscountApprovalWS.refreshDiscounts(oppIds);
              if(result[0] == 'Success'){ 
                  system.debug(LoggingLevel.Error,'Opportunity refresh Success = :');
                  list<UpliftCalculationMethod__c> UCM =[select name,active__c from UpliftCalculationMethod__c where active__c = true]; //added by nikhil on 7/6/2015

                  detailList =[select Uplift_of_Net__c,Discount__c,LastModifiedDate,Category_Master__c,Category_Master__r.Name,isopp__c,Uplift__c,opportunity__c, Special_Discount__c,(Select Id,Discount_Detail__c,Value__c,Type__c from Additional_Discount_Detail__r order by Type__c,Value__c)from Discount_Detail__c where isopp__C =true and opportunity__c=:newRecordId and Category_Master__c!=null order by Category_Master__r.Name];
                  String oppid=opportunity.Id;
                  String newoppid=newOpp.Id;
                  if(oppId.length()==18)
                  oppId=oppId.substring(0,oppid.length()-3);  
                   if(newoppid.length()==18)
                  newoppid=newoppid.substring(0,newoppid.length()-3);           
                  if(UserInfo.getUserType().equalsIgnoreCase('PowerPartner')){
                      String rvr = RVBD_Email_Properties__c.getinstance('PWS Partner URL').Server_URL__c;
                      urls= rvr+ '&login!!sessionId='+System.userinfo.getSessionId()+'&login!!serverUrl='+url2+'/services/Soap/c/9.0/'+System.userinfo.getOrganizationId()+'&login!!oldOpportunity='+oppId+'&quotes!!OpportunityId ='+newoppid+'&ic=1&login!!discountHash=';
                      system.debug(LoggingLevel.Error,'Partner Configure URL = :'+urls);
                  }else{
                      
                      String rvr = RVBD_Email_Properties__c.getinstance('PWS').Server_URL__c;
                      urls= rvr+ '&quotes!!oldOpportunity='+oppId+'&quotes!!OpportunityId='+newoppid+'&quotes!!user_id='+System.userinfo.getUserId()+'&quotes!!session_id='+System.userinfo.getSessionId()+'&quotes!!server_url='+url2+'/services/Soap/c/11.0/'+System.userinfo.getOrganizationId()+'&quotes!!LastModified='+opportunity.lastmodifieddate+'&ic=1&quotes!!discountHash=';
                      system.debug(LoggingLevel.Error,'Configure URL = :'+urls);
                       }
                  system.debug(LoggingLevel.Error,'---------------------------------------'+urls);
                  String url1=''; 
                  system.debug(LoggingLevel.Error,'detailList**'+detailList);
                  for(Discount_Detail__c d :detailList){                    
                    String disc = String.ValueOf(d.discount__c); 
                    if(disc == null || disc.equalsIgnoreCase('null')){
                        disc = d.category_Master__r.Name+'0.00';
                    }else if(disc!=null||disc!=''){                        
                        disc=d.category_Master__r.Name+disc;
                    } 
                    String splD = String.ValueOf(d.Special_Discount__c);
                    if(splD==null||splD.equalsIgnoreCase('null')){
                        splD='';
                    }else if(splD != null ||splD!='' ){
                        splD='Special'+splD;
                    }
                    
                    String uplift='';
                    //Added If else Loop to check Opp uplift calculation value and send proper field in Discount string-Nikhil@6/23/15
                     if(!ucm.IsEmpty()){
                        
                     if(ucm[0].name.equalsIgnoreCase('net')){
                        uplift= String.Valueof(d.Uplift_of_Net__c);
                        if(uplift == null || uplift.equalsIgnoreCase('null')){
                            uplift = 'Uplift'+'0.00';                       
                        }else if(uplift!=null||uplift!=''||!uplift.equalsIgnoreCase('null')){
                            uplift='Uplift'+uplift;
                        }
                        
                     }else{
                        uplift= String.Valueof(d.Uplift__c);
                        if(uplift == null || uplift.equalsIgnoreCase('null')){
                            uplift = 'Uplift'+'0.00';                       
                        }else if(uplift!=null||uplift!=''||!uplift.equalsIgnoreCase('null')){
                            uplift='Uplift'+uplift;
                        }
                     }
                     
                     }else{
                        uplift= String.Valueof(d.Uplift__c);
                        if(uplift == null || uplift.equalsIgnoreCase('null')){
                            uplift = 'Uplift'+'0.00';                       
                        }else if(uplift!=null||uplift!=''||!uplift.equalsIgnoreCase('null')){
                            uplift='Uplift'+uplift;
                        }
                     }
                      //End-Nikhil
                    
                    addString='';
                    for(Additional_Discount_Detail__c addTemp:d.Additional_Discount_Detail__r){
                      
                        addString+=addTemp.Type__c+String.ValueOf(addTemp.Value__c);
                    }
                    if(addString!=null||addString!=''){
                        addString=addString.trim();
                    }else if(addString == null || addString.equalsIgnoreCase('null')){
                        addString = '';
                    }                   
                    url1+=disc.trim()+splD.trim()+addString.trim()+uplift.trim();
                                 
                }
                system.debug(LoggingLevel.Error,'Discounts to be sent CQ ----- '+ url1);                              
                /* Uncomment for new discount url*/
                String finalurl=newoppid+url1; 
                system.debug(LoggingLevel.Error,'***finalurl::'+finalurl);           
                Blob myDigest = Crypto.generateDigest(algorithmName, Blob.valueOf(finalurl));
                string myDigestString = EncodingUtil.ConvertToHex(myDigest);
                myDigestString=isNull(myDigestString);
                urls = urls+myDigestString;
                system.debug(LoggingLevel.Error,'url------------------------'+urls);
                retPage = new PageReference(urls);
                render=false;
                return null;
               /* if(UserInfo.getUserType().equalsIgnoreCase('PowerPartner')){
                    return retPage;
                }else{  
                    return null;
                }  */              
            }else{ 
                system.debug(LoggingLevel.Error,'Result1:'+result);
                errorMsg=result[0];
                oppLink='/'+recId;
                retPage = new PageReference('/apex/customerrorpage?Id='+recId+'&errormsg='+errorMsg);        
                return retPage;
            }
        }
        private string isNull(string originalStr){
        string str='';
        if(originalStr==null)return str;
        else return originalStr;
        }  
    public string getURLEncode(string originalStr){
        string encodeStr=EncodingUtil.urlEncode(originalStr, 'UTF-8');
        return encodeStr;
    }
    
     public PageReference newcancel() {
        PageReference opportunityPage = new PageReference('/'+opp.Id);
        opportunityPage.setRedirect(true);
        return opportunityPage;
    }
    
 }