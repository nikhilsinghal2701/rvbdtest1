/**
Code Modified by Santoshi on Nov-18-2011 to create URL dynamically to pass to PWS.
Rev2 - Code Modified by Rucha on May-7-2013 to validate Selected Group Id field before going to PWS
Rev3 - Modified by Rucha on June-26-2013 to validate existence of Account on Opportunity
Rev4 - Modified by psingh@riverbed.com on Oct-18-2013 to create MD5 encoded opportunity discount to pass to PWS for Partner Program Project
*/
public without sharing class ConfigureController {
    Id recId;
    private static string algorithmName='MD5';
    public Transient String errorMsg{get;set;}
    public Transient String oppLink{get;set;}
    public Transient String urls {get;set;}
    List<Id> oppIds=new List<Id>();
    Opportunity opp;
    private List<Discount_Detail__c> detailList;
    private PageReference retPage;
    public String url2 {get;set;}
    private Transient String addString='';
    public String selectedOpnetGrpId{get;set;}
    public Boolean opnetAccReq{get;set;}
    public ConfigureController(ApexPages.StandardController con){
        recId = con.getId();
        oppIds.add(recId);
        url2=String.valueOf(URL.getSalesforceBaseUrl().toExternalForm());
        opp=[select Id,Sale_Type__c,Uplift_Calculation__c,Is_Service_Renewal__c, lastmodifieddate,StageName,DiscountProduct__c,DiscountSupport__c,Discount_Demos__c,DiscountSpare__c,DiscountService__c,DiscountOthers__c,
        UpliftProducts__c,UpliftSupport__c,UpliftDemos__c,UpliftSpares__c,UpliftServices__c,UpliftOthers__c,Selected_Opnet_Group_Id__c,AccountId,Group_Id_Not_Needed__c from Opportunity where Id=:recId];//Rev2 - Added Selected_Opnet_Group_Id__c,AccountId,Group_Id_Not_Needed__c
        //detailList =[select Discount__c,Category_Master__c,Category_Master__r.Name,isopp__c,Uplift__c,opportunity__c, Special_Discount__c,(Select Id,Discount_Detail__c,Value__c,Type__c from Additional_Discount_Detail__r)from Discount_Detail__c where isopp__C =true and opportunity__c=:recId and Category_Master__c!=null order by Category_Master__r.Name];//commented by prashant.singh@riverbed.com on 11/20/2013
    }
    public ConfigureController(){
        errorMsg=ApexPages.currentPage().getParameters().get('errormsg');
        oppLink='/'+ApexPages.currentPage().getParameters().get('id');
        
        //Rev2
        String oAccReq = ApexPages.currentPage().getParameters().get('opnetAccReq');
        system.debug(LoggingLevel.Error,'***oAccReq::'+oAccReq);
        if(oAccReq!=null && oAccReq.equals('true')){
            opnetAccReq = true;         
        }
        else{
            opnetAccReq = false;
        }
        opp = [Select Uplift_Calculation__c, AccountId,Selected_Opnet_Group_Id__c,Group_Id_Not_Needed__c from Opportunity Where Id = :ApexPages.currentPage().getParameters().get('id')];   
        //
    }
    public Pagereference callWebService(){
      if(opp.StageName.equalsIgnoreCase('6 - Order Accepted')||opp.StageName.equalsIgnoreCase('7 - Closed (Not Won)')){
            errorMsg=System.Label.Configure_Error_Msg;
            oppLink=recId;
            retPage = new PageReference('/apex/customerrorpage?Id='+recId+'&errormsg='+errorMsg+'&opnetAccReq=false');        
            return retPage;
        }if(opp.Sale_Type__c != null && opp.Sale_Type__c.equalsIgnoreCase('Conditional PO') && opp.StageName.equalsIgnoreCase('5 - Negotiation & Closure')){
            errorMsg=System.Label.Cannot_Configure_a_Conditional_PO_in_stage_5;
            oppLink=recId;
            retPage = new PageReference('/apex/customerrorpage?Id='+recId+'&errormsg='+errorMsg+'&opnetAccReq=false');        
            return retPage;
        }else if(opp.Is_Service_Renewal__c){
            errorMsg='You cannot configure a Service Renewal Opportunity';
            oppLink=recId;
            retPage = new PageReference('/apex/customerrorpage?Id='+recId+'&errormsg='+errorMsg+'&opnetAccReq=false');        
            return retPage;
        }
        else if(opp.AccountId == null){//Rev3
            errorMsg='Account is missing on this Opportunity. Please add an Account before continuing';
            oppLink=recId;
            retPage = new PageReference('/apex/customerrorpage?Id='+recId+'&errormsg='+errorMsg+'&opnetAccReq=false');        
            return retPage;
        }//End of Rev3
        else{
                //Rev2
                if(opp.Selected_Opnet_Group_Id__c==null && opp.Group_Id_Not_Needed__c == false){
                    List<Opnet_Account_Group__c> accList = [Select Id from Opnet_Account_Group__c where Account__c=:opp.AccountId];
                    if(accList.size() > 0){
                        errorMsg='Account has Group IDs, Please select an option';
                        oppLink=recId;
                        retPage = new PageReference('/apex/customerrorpage?Id='+recId+'&errormsg='+errorMsg+'&opnetAccReq=true');                   
                        return retPage;
                    }
                    else{
                        opp.Selected_Opnet_Group_Id__c = '-1';
                        update opp;
                    }               
                }
              List<String> result=DiscountApprovalWS.refreshDiscounts(oppIds);
              //List<String> result=new List<String>();//for testing new discount url string -- remove before prod deployment
              //result.add('Success');//for testing new discount url string -- remove before prod deployment
              //system.debug('Result:'+result);
              if(result[0] == 'Success'){
                   list<UpliftCalculationMethod__c> UCM =[select name,active__c from UpliftCalculationMethod__c where active__c = true]; //added by nikhil on 7/6/2015
 
                  //Code Modified by Santoshi on Nov-18-2011 to create URL dynamically to pass to PWS.
                  //string url = '/servlet/servlet.Integration?scontrolCaching=1&lid=00bA0000000Im0m&eid=' + recId +'&quotes!!LastModified='+opp.lastmodifieddate+ '&ic=1&quotes!!d=';
                  //added by Prashant.Singh@riverbed.com on 11/20/2013 for partner program project 2013
                  detailList =[select Uplift_of_Net__c,Discount__c,Category_Master__c,Category_Master__r.Name,isopp__c,Uplift__c,opportunity__c, Special_Discount__c,(Select Id,Discount_Detail__c,Value__c,Type__c from Additional_Discount_Detail__r order by Type__c,Value__c)from Discount_Detail__c where isopp__C =true and opportunity__c=:recId and Category_Master__c!=null order by Category_Master__r.Name];
                  String oppid=Opp.Id;
                  if(oppId.length()==18)
                  oppId=oppId.substring(0,oppid.length()-3);              
                  if(UserInfo.getUserType().equalsIgnoreCase('PowerPartner')){
                      String rvr = RVBD_Email_Properties__c.getinstance('PWS Partner URL').Server_URL__c;
                      urls= rvr+ '&login!!sessionId='+System.userinfo.getSessionId()+'&login!!serverUrl='+url2+'/services/Soap/c/9.0/'+System.userinfo.getOrganizationId()+'&login!!OpportunityId='+oppId+'&ic=1&login!!discountHash=';
                      //urls= rvr+ '&login!!sessionId='+System.userinfo.getSessionId()+'&login!!serverUrl='+url2+'/services/Soap/c/9.0/'+System.userinfo.getOrganizationId()+'&login!!OpportunityId='+oppId+'&ic=1&login!!d=';
                      //system.debug('Partner Configure URL = ' + urls);
                  }else{
                      String rvr = RVBD_Email_Properties__c.getinstance('PWS').Server_URL__c;
                      urls= rvr+ '&quotes!!OpportunityId='+oppId+'&quotes!!user_id='+System.userinfo.getUserId()+'&quotes!!session_id='+System.userinfo.getSessionId()+'&quotes!!server_url='+url2+'/services/Soap/c/11.0/'+System.userinfo.getOrganizationId()+'&quotes!!LastModified='+opp.lastmodifieddate+'&ic=1&quotes!!discountHash=';
                      //urls= rvr+ '&quotes!!OpportunityId='+oppId+'&quotes!!user_id='+System.userinfo.getUserId()+'&quotes!!session_id='+System.userinfo.getSessionId()+'&quotes!!server_url='+url2+'/services/Soap/c/11.0/'+System.userinfo.getOrganizationId()+'&quotes!!LastModified='+opp.lastmodifieddate+'&ic=1&quotes!!d=';
                  }
                  //system.debug('---------------------------------------'+urls);
                  String url1='';
                  for(Discount_Detail__c d :detailList){                    
                    //url1+= d.category_Master__r.Name+'|'+String.ValueOf(d.discount__c!=null?d.discount__c:0.00)+'|'+String.Valueof(d.Uplift__C!=null?d.uplift__c:0.00);
                    //changed by Ankita for JIRA FEBR-474
                    String disc = String.ValueOf(d.discount__c);
                    if(disc == null || disc.equalsIgnoreCase('null')){
                        disc = d.category_Master__r.Name+'0.00';
                    }else if(disc!=null||disc!=''){                        
                        disc=d.category_Master__r.Name+disc;
                    }
                    String splD = String.ValueOf(d.Special_Discount__c);
                    if(splD==null||splD.equalsIgnoreCase('null')){
                        splD='';
                    }else if(splD != null ||splD!='' ){
                        splD='Special'+splD;
                    }
                   
                    String uplift='';
                    //Added If else Loop to check Opp uplift calculation value and send proper field in Discount string-Nikhil@6/23/15
                     if(!ucm.IsEmpty()){
                        
                     if(ucm[0].name.equalsIgnoreCase('net')){
                        uplift= String.Valueof(d.Uplift_of_Net__c);
                        if(uplift == null || uplift.equalsIgnoreCase('null')){
                            uplift = 'Uplift'+'0.00';                       
                        }else if(uplift!=null||uplift!=''||!uplift.equalsIgnoreCase('null')){
                            uplift='Uplift'+uplift;
                        }
                        
                     }else{
                        uplift= String.Valueof(d.Uplift__c);
                        if(uplift == null || uplift.equalsIgnoreCase('null')){
                            uplift = 'Uplift'+'0.00';                       
                        }else if(uplift!=null||uplift!=''||!uplift.equalsIgnoreCase('null')){
                            uplift='Uplift'+uplift;
                        }
                     }
                     
                     }else{
                        uplift= String.Valueof(d.Uplift__c);
                        if(uplift == null || uplift.equalsIgnoreCase('null')){
                            uplift = 'Uplift'+'0.00';                       
                        }else if(uplift!=null||uplift!=''||!uplift.equalsIgnoreCase('null')){
                            uplift='Uplift'+uplift;
                        }
                     }
                      //End-Nikhil 
                        
                    
                    //url1+= d.category_Master__r.Name+'|'+disc+'|'+splD+'|'+uplift;//old pattern -- commented by psingh@riverbed.com 10/18/2013
                    //string finalurl=EncodingUtil.urlencode(url1,'UTF-8');//commented by psingh@riverbed.com 10/18/2013                
                    //urls = urls+finalurl;//commented by psingh@riverbed.com 10/18/2013
                      
                    /*Start -- added by psingh@riverbed.com 10/18/2013 -- uncomment for new discount url*/
                    addString='';
                    for(Additional_Discount_Detail__c addTemp:d.Additional_Discount_Detail__r){
                        //system.debug('***Discount_Detail::'+d+'::'+d.Additional_Discount_Detail__r);
                        addString+=addTemp.Type__c+String.ValueOf(addTemp.Value__c);
                    }
                    if(addString!=null||addString!=''){
                        addString=addString.trim();
                    }else if(addString == null || addString.equalsIgnoreCase('null')){
                        addString = '';
                    }
                    
                    url1+=disc.trim()+splD.trim()+addString.trim()+uplift.trim();
                                      
                    //End -- added by psingh@riverbed.com 10/18/2013                  
                }
                system.debug(LoggingLevel.INFO,'Discounts to be sent ----- '+ url1);                              
                /* Uncomment for new discount url*/
                String finalurl=oppId+url1;//added by psingh@riverbed.com 10/18/2013 
                system.debug(LoggingLevel.Error,'***finalurl::'+finalurl);           
                Blob myDigest = Crypto.generateDigest(algorithmName, Blob.valueOf(finalurl));//added by psingh@riverbed.com 10/18/2013
                string myDigestString = EncodingUtil.ConvertToHex(myDigest);//added by psingh@riverbed.com 10/18/2013
                myDigestString=isNull(myDigestString);//added by psingh@riverbed.com 10/18/2013
                urls = urls+myDigestString; //+'&quotes!!LastModified= '; 
                system.debug(LoggingLevel.Error,'url------------------------'+urls);
                retPage = new PageReference(urls);
                //system.debug(LoggingLevel.INFO,'RETPAGE:'+retPage);
                if(UserInfo.getUserType().equalsIgnoreCase('PowerPartner')){
                    return retPage;
                }else{
                    return null;
                }               
            }else{ 
                system.debug(LoggingLevel.INFO,'Result1:'+result);
                errorMsg=result[0];
                oppLink='/'+recId;
                retPage = new PageReference('/apex/customerrorpage?Id='+recId+'&errormsg='+errorMsg);        
                return retPage;
            }
        }
    }
    
    private string isNull(string originalStr){
        string str='';
        if(originalStr==null)return str;
        else return originalStr;
    }  
    private string getURLEncode(string originalStr){
        string encodeStr=EncodingUtil.urlEncode(originalStr, 'UTF-8');
        //system.debug('>>> String:'+encodeStr);
        return encodeStr;
    }
    
    //Rev2
    public List<SelectOption> opportunityAccGroupIds;
    public List<SelectOption> getOpportunityAccGroupIds(){
        if(opportunityAccGroupIds ==null){          
            opportunityAccGroupIds = createAccountGroupIdList(opp.AccountId);
        }
        return opportunityAccGroupIds;
    }
    
    Map<String,Id> opnetGrpNameToIdMap = new Map<String,Id>();  
    public List<SelectOption> createAccountGroupIdList(Id accountId){
        List<SelectOption> selList = new List<SelectOption>();      
        for(Opnet_Account_Group__c ogid : [Select Id,Name From Opnet_Account_Group__c where Account__c=:accountId]){
            selList.add(new SelectOption(ogid.Name,ogid.Name));
            opnetGrpNameToIdMap.put(ogid.Name,ogid.Id);
        }
        selList.add(new SelectOption('-3','Group Id not needed'));
        selList.add(new SelectOption('-1','Create New Group Id'));
        return selList;
    }
    
    public PageReference continueToPowerStrip(){
        if(selectedOpnetGrpId == '-3'){
            opp.Group_Id_Not_Needed__c = true;
        }
        else{
            opp.Selected_Opnet_Group_Id__c = selectedOpnetGrpId;
            opp.Opnet_Group_Id__c = opnetGrpNameToIdMap.get(selectedOpnetGrpId);
        }
        update opp;         
        return new PageReference('/apex/Configurer_VF?Id='+opp.Id); 
    }
    //
}