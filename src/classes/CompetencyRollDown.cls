/*
* Class: CompetencyRollDown
	TriggerAction: After update
	Purpose: to roll down partner competency status to child from parent and grand parent account in account hirearchy.
				Note: this class takes care of only three levels of account hirearchy
	Author: Jaya ( Perficient )
	Created Date: 7/30/2014					
	Reference: pc=> Partner Competency	
*/
public without sharing class CompetencyRollDown {
	
	public static void rolldownCompetencyStatus(Map<Id, Acct_Cert_Summary__c> certsMap, Database.BatchableContext bcontext)
	{
		Map<String,Acct_Cert_Summary__c> tapAcctPCMap = new Map<String,Acct_Cert_Summary__c>();
		Map<Id,Id> acctHirearchyMap = new Map<Id,Id>();
		Set<Id> acctIds = new Set<Id>();
		Set<String> tapType = new Set<String>();
		Set<Acct_Cert_Summary__c> partnerCompetencySet = new Set<Acct_Cert_Summary__c>();
		List<Acct_Cert_Summary__c> partnerCompetencyUpdateList = new List<Acct_Cert_Summary__c>();
		Map<Id,Acct_Cert_Summary__c> pcForAcctMap = new Map<Id,Acct_Cert_Summary__c>();
		
		// for calling account authorization
		Map<String, Id> certSummaryIdsReqSatisfied = new Map<String,Id>();// status active pc
		Map<String, Id> certSummaryIdsFailedReqCriteria = new Map<String,Id>();// status inactive pc
		String query = 'SELECT Id, Account__c, Account__r.Do_not_Auto_Level__c, Account__r.Do_not_share_Competencies__c, Status_Tap__c,TAP__c,'
							+' Account__r.ParentId, Account__r.Parent.Do_not_Auto_Level__c, Account__r.Parent.Do_not_share_Competencies__c,'
							+' Account__r.Parent.ParentId, Account__r.Parent.Parent.Do_not_Auto_Level__c, Account__r.Parent.Parent.Do_not_share_Competencies__c,'
							+' RSA__c,RSS__c,RTSA__c,RTSS__c,RCSA__c,RCSP__c,RCSA_RCSP__c, RSS_Needed__c, RTSS_Needed__c,RCSA_RCSP_Needed__c,RCSP_Needed__c,RCSA_Needed__c,'
							+' Roll_Up_RSS__c,Roll_Up_RSA__c,Roll_Up_RTSS__c,Roll_Up_RTSA__c,Roll_Up_RCSA__c,Roll_Up_RCSP__c,Roll_Up_RCSA_RCSP__c ';
							
		Set<Id> pcIds = certsMap.keySet();
		String generalPC = Constant.GENERAL_PARTNER_COMPETENCY;
		String iQuery = query +' FROM Acct_Cert_Summary__c  WHERE Id IN: pcIds ';// ==> AND TAP__c != :generalPC' => general partner competency should also be considered 
		System.debug('pcIds:'+pcIds);
		for(Acct_Cert_Summary__c pc: Database.query(iQuery)){
			partnerCompetencySet.add(pc);
			if(pc.Account__c != null){
				acctIds.add(pc.Account__c);
				String key = pc.TAP__c+'|'+pc.Account__c;
				tapAcctPCMap.put(key,pc);
				tapType.add(pc.TAP__c);
			}
			System.debug('parent:'+pc.Account__c);
			if(pc.Account__r.ParentId != null && !pc.Account__r.Parent.Do_not_Auto_Level__c && !pc.Account__r.Parent.Do_not_share_Competencies__c){
				acctIds.add(pc.Account__r.ParentId);
				System.debug('parent:'+pc.Account__r.ParentId);
				if(pc.Account__r.Parent.ParentId != null && !pc.Account__r.Parent.Parent.Do_not_Auto_Level__c && !pc.Account__r.Parent.Parent.Do_not_share_Competencies__c){
					acctIds.add(pc.Account__r.Parent.ParentId);
					System.debug('parent:'+pc.Account__r.Parent.ParentId);
				}
			}
		}
		// with the same query as below iterate and create a map of key and pc. use this map in the for loop below to get the parent pc record.
		// in case of batch consider this query
		String kQuery = query +' ,Account__r.Type, Account__r.Partner_Status1__c FROM Acct_Cert_Summary__c '
							+' WHERE TAP__c IN: tapType AND (Account__c IN: acctIds OR Account__r.ParentId IN: acctIds OR Account__r.Parent.ParentId IN: acctIds)';
		for(Acct_Cert_Summary__c pc: Database.query(kQuery)){
			String key = pc.TAP__c+'|'+pc.Account__c;
			tapAcctPCMap.put(key,pc);
		}
		
		for(Acct_Cert_Summary__c pc: tapAcctPCMap.values()){
			Acct_Cert_Summary__c ppc = new Acct_Cert_Summary__c();
			Acct_Cert_Summary__c gppc = new Acct_Cert_Summary__c();
			if(pc.Account__r.ParentId != null && !pc.Account__r.Parent.Do_not_Auto_Level__c && !pc.Account__r.Parent.Do_not_share_Competencies__c){
				// get parent pc
				String ppcKey = pc.TAP__c+'|'+pc.Account__r.ParentId;
				ppc = tapAcctPCMap.get(ppcKey);
				if(pc.Account__r.Parent.ParentId != null && !pc.Account__r.Parent.Parent.Do_not_Auto_Level__c && !pc.Account__r.Parent.Parent.Do_not_share_Competencies__c){
					// get grandparent pc
					String gppcKey = pc.TAP__c+'|'+pc.Account__r.Parent.ParentId;
					gppc = tapAcctPCMap.get(gppcKey);
				}
			}
			
			//Compare grandparent, parent roll up cert fields or self cert fields child needed
			String acctAuthKey = pc.TAP__c+pc.Id;
			Acct_Cert_Summary__c updatedRecord = new Acct_Cert_Summary__c();
			if((gppc != null && gppc.Id != null && CertRollup.isNullCheck(gppc.Roll_Up_RSS__c) >= pc.RSS_Needed__c && CertRollup.isNullCheck(gppc.Roll_Up_RTSS__c) >= pc.RTSS_Needed__c 
					&& CertRollup.isNullCheck(gppc.Roll_Up_RCSA_RCSP__c) >= pc.RCSA_RCSP_Needed__c && CertRollup.isNullCheck(gppc.Roll_Up_RCSP__c) >= pc.RCSP_Needed__c && CertRollup.isNullCheck(gppc.Roll_Up_RCSA__c) >= pc.RCSA_Needed__c)
				|| (ppc != null && ppc.Id != null && CertRollup.isNullCheck(ppc.Roll_Up_RSS__c) >= pc.RSS_Needed__c && CertRollup.isNullCheck(ppc.Roll_Up_RTSS__c) >= pc.RTSS_Needed__c 
					&& CertRollup.isNullCheck(ppc.Roll_Up_RCSA_RCSP__c) >= pc.RCSA_RCSP_Needed__c && CertRollup.isNullCheck(ppc.Roll_Up_RCSP__c) >= pc.RCSP_Needed__c && CertRollup.isNullCheck(ppc.Roll_Up_RCSA__c) >= pc.RCSA_Needed__c)
				|| (CertRollup.isNullCheck(pc.Roll_Up_RSS__c) >= pc.RSS_Needed__c && CertRollup.isNullCheck(pc.Roll_Up_RTSS__c) >= pc.RTSS_Needed__c 
						&& CertRollup.isNullCheck(pc.Roll_Up_RCSA_RCSP__c) >= pc.RCSA_RCSP_Needed__c && CertRollup.isNullCheck(pc.Roll_Up_RCSP__c) >= pc.RCSP_Needed__c && CertRollup.isNullCheck(pc.Roll_Up_RCSA__c) >= pc.RCSA_Needed__c)
				|| (CertRollup.isNullCheck(pc.RSS__c) >= pc.RSS_Needed__c && CertRollup.isNullCheck(pc.RTSS__c) >= pc.RTSS_Needed__c 
						&& CertRollup.isNullCheck(pc.RCSA_RCSP__c) >= pc.RCSA_RCSP_Needed__c) && CertRollup.isNullCheck(pc.RCSP__c) >= pc.RCSP_Needed__c && CertRollup.isNullCheck(pc.RCSA__c) >= pc.RCSA_Needed__c){
							
				updatedRecord = createPartnerCompetency(pc.Id,'Yes');
				certSummaryIdsReqSatisfied.put(acctAuthKey,pc.Id);
			}else{
				updatedRecord = createPartnerCompetency(pc.Id,'No');
				certSummaryIdsFailedReqCriteria.put(acctAuthKey,pc.Id);
			}
			System.debug('updatedRecord:'+updatedRecord); 
			partnerCompetencyUpdateList.add(updatedRecord);
			pcForAcctMap.put(pc.Id,pc);// for updating account authorization records.
			System.debug('partnerCompetencyUpdateList:'+partnerCompetencyUpdateList);
		}
		if(!partnerCompetencyUpdateList.isEmpty()){
			// convert the set to list and update with error logs.
			System.debug('partnerCompetencyUpdateList:'+partnerCompetencyUpdateList);
			List<Acct_Cert_Summary__c> listToUpdate = new List<Acct_Cert_Summary__c>(partnerCompetencyUpdateList);
			Database.SaveResult[] updateResults = Database.update(partnerCompetencyUpdateList,false);
			System.debug('certSummaryIdsReqSatisfied:'+certSummaryIdsReqSatisfied);
			System.debug('certSummaryIdsFailedReqCriteria:'+certSummaryIdsFailedReqCriteria);
			System.debug('pcForAcctMap:'+pcForAcctMap);
			 list<Account_Authorizations__c> accountAuthorizationList = CertificationSummaryTriggerHandler.getAccountAuthorizationsToUpsert(certSummaryIdsReqSatisfied,certSummaryIdsFailedReqCriteria,pcForAcctMap);
			 System.debug('accountAuthorizationList:'+accountAuthorizationList);
			 if(!accountAuthorizationList.isEmpty()){
			 	upsert accountAuthorizationList;
			 }
             List<Database.Error> dbErrors = new List<Database.Error>();
             List<Error_Log__c> errorLogsList = new List<Error_Log__c>();
            // Process the save results
            Integer i = 0;
            for(Database.SaveResult sr: updateResults)
            {
                if(!sr.isSuccess())
                {
                     Database.Error dbErr = sr.getErrors()[0];
                     String batchId = (System.isBatch()&& bcontext != null) ? bcontext.getJobId() : '';
                     Error_Log__c err = logError(partnerCompetencyUpdateList.get(i).Id,dbErr.getMessage(),'CompetencyRollDown class',batchId);
                     errorLogsList.add(err);
                }
                i++;
            }
            if(!errorLogsList.isEmpty()){
                insert errorLogsList;
            }
		}
	}
	
	public static Acct_Cert_Summary__c createPartnerCompetency (Id pcId, String pcStatus){
		return (new Acct_Cert_Summary__c(Id = pcId, Status_Tap__c = pcStatus));
		
	}
	
	public static Error_Log__c logError(String recordId, String errorMsg, String errorSrc, String batchId){
		 Error_Log__c errLog = new Error_Log__c(Error_Causing_Data__c = recordId,
                                            Error_Cause__c = errorMsg, Error_Source__c = errorSrc);//
         errLog.Batch_Id__c = batchId != null ? batchId: '';
         return errLog;
	}
	
}