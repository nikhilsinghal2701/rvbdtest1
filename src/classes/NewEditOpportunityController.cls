/********************************************************
Rev#    Author              Date        Description
Rev1    Clear Task-Rucha    5/9/2013    Added methods to set Opnet Account Group Id on Opportunity based on its account
Rev2    Added by Rucha      6/25/2013   To show opnet group id if account is not populated
Rev3    Changed by Remy     10/22/2013  To change the field related logic about
Rev4    Changed by Nikhil   12/12/13    To Add Riverbed as Support Provider-Method Save3
Rev:1 changes by Jaya   Date: 08/13/2014 To display error message if the selected provider is illegal, Display a link to provider. validate on clik of save and finish. 
********************************************************/
public without sharing class NewEditOpportunityController {
        public Opportunity newOpportunity{get;set;}
        public static final string riverbedAccId='0015000000HoWN7AAN';
        public static final string riverbedAccName='Riverbed';
        private string id;
        public boolean showAPM {get; set;}
        private string support;
        public string supportName {get;set;}
        private string supportBy;
        private List<SelectOption> supports = new List<SelectOption>();
        private List<SelectOption> supportsBy = new List<SelectOption>();
        public string providerAcctId {get;set;} // Rev:1 for adding link to the account 
        public boolean isNotFound {get;set;}
        ApexPages.StandardController ctrl;



        //Rev1 - Added by Rucha (5/7/2013)
        private Opportunity newOpportunityCopy;
        private String selectedOpnetGroupId;
        public String getSelectedOpnetGroupId(){
                        if(newOpportunity.Group_Id_Not_Needed__c){
                                selectedOpnetGroupId = '-3';
                        }
                        else{
                                selectedOpnetGroupId = newOpportunity.Selected_Opnet_Group_ID__c;
                        }

                        return selectedOpnetGroupId;
                }
        public void setSelectedOpnetGroupId(String value){
                selectedOpnetGroupId = value;
        }
        //

        public NewEditOpportunityController(ApexPages.StandardController controller) {

                ctrl = controller;
                id = ApexPages.currentPage().getParameters().get('id');
                this.newOpportunity=(Opportunity)controller.getRecord();
                system.debug('New Opportunity::'+newOpportunity);

                newOpportunityCopy = [Select Id, AccountId,Selected_Opnet_Group_Id__c,Group_Id_Not_Needed__c,OpnetGroup_ID__c from Opportunity where id=:newOpportunity.Id];//Added by Rucha (5/7/2013)

                if(id!=null){
                        Opportunity tempOpp=[select id,Support_Provider__c,Support_Provided__c,Sold_To_Partner__c,Tier2__c,Tier3_Partner__c,Influence_Partner__c from Opportunity where id=:id];
                        system.debug('Temp Opp::'+tempOpp);
                        this.support=tempOpp.Support_Provider__c;
                        List<Account> supportAcc = [select Id, Name from Account where Id = : support limit 1];
                        if(supportAcc.size() == 0){
                            supportName = '';
                        } else {
                            this.supportName = supportAcc[0].Name;
                        }
                        if(tempOpp.Support_Provided__c != null){
                                supportsBy.add(new SelectOption(tempOpp.Support_Provided__c, tempOpp.Support_Provided__c));
                        }
                        this.supportBy=tempOpp.Support_Provided__c;
                        isNotFound = false;
                        providerAcctId = tempOpp.Support_Provider__c;
                }
                RVBD_Email_Properties__c em = RVBD_Email_Properties__c.getinstance('ValidateSelectedGroupId');
                {   //system.debug('hiii'+em.server_URL__c);
                        showAPM = boolean.valueof(em.server_URL__c);
                }
        }

        public List<SelectOption> getSupports(){
            List<SelectOption> supports = new List<SelectOption>();
            Map<Id,Account> mapAccount;
            set<Id> accIds=new set<Id>();
            Boolean isRASP=false;
            Opportunity tempOpp;
            if(id!=null){
                    tempOpp = [select id,Sold_To_Partner__c,Sold_To_Partner__r.Name,
                                                Tier2__c,Tier2__r.Name,Tier3_Partner__c,Tier3_Partner__r.Name,
                                                Influence_Partner__c,Influence_Partner__r.Name
                                                from Opportunity where id=:id];
            }
            supports.add(new SelectOption('','-None-'));
            supports.add(new SelectOption(riverbedAccId,riverbedAccName));
            if(tempOpp!=null){
                    if(tempOpp.Sold_To_Partner__c!=null)accIds.add(tempOpp.Sold_To_Partner__c);
                    if(tempOpp.Tier2__c!=null)accIds.add(tempOpp.Tier2__c);
                    if(tempOpp.Tier3_Partner__c!=null)accIds.add(tempOpp.Tier3_Partner__c);
                    if(tempOpp.Influence_Partner__c!=null)accIds.add(tempOpp.Influence_Partner__c);
            }
            if(accIds.size()>0)
                mapAccount=new Map<Id,Account>([select Id,Name,Type,Authorizations_Specializations__c from Account where id in:accIds]);
            if(mapAccount!=null&&mapAccount.size()>0){
                for(Account acc:mapAccount.values()){
                        isRASP=getRASPAccount(acc);
                        if(isRASP)supports.add(new SelectOption(acc.Id,acc.Name));
                }
            }
            /*if(tempOpp.Sold_To_Partner__c!=null)
                supports.add(new SelectOption(tempOpp.Sold_To_Partner__c,tempOpp.Sold_To_Partner__r.Name));
            if(tempOpp.Tier2__c!=null)
                supports.add(new SelectOption(tempOpp.Tier2__c,tempOpp.Tier2__r.Name));
            if(tempOpp.Tier3_Partner__c!=null)
                supports.add(new SelectOption(tempOpp.Tier3_Partner__c,tempOpp.Tier3_Partner__r.Name));
            if(tempOpp.Influence_Partner__c!=null)
                supports.add(new SelectOption(tempOpp.Influence_Partner__c,tempOpp.Influence_Partner__r.Name));*/
        return supports;
        }

        public void setSupport(string support){
                this.support=support;
        }
        public string getSupport(){
                providerAcctId=support;
                return support;
        }
        public void providedBy(){
                supportsBy = new List<SelectOption>();
                if(support==null||support==''){
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Service Provider');
                        ApexPages.addMessage(msg);
                }
                system.debug('support1::'+support);

                if(support!=null){

                        List<Account> accTempList =[select Id,Name,Authorizations_Specializations__c,Type from Account where id=:support];
                        if(accTempList != null && !accTempList.isEmpty()){
                            Account accTemp = accTempList[0];

                            System.debug('AccTemp: ' + accTemp);
                            if(accTemp!=null && accTemp.Type!=null){
                                    if(accTemp.Type.equalsIgnoreCase('Distributor')){
                                            supportsBy.add(new SelectOption('Distributor','Distributor'));
                                    }else if(accTemp.Type.equalsIgnoreCase('System Integrator')||accTemp.Type.equalsIgnoreCase('VAR')||accTemp.Type.equalsIgnoreCase('Service Provider')||accTemp.Type.equalsIgnoreCase('Direct Market Reseller (DMR)')){

                                            supportsBy.add(new SelectOption('Reseller','Reseller'));
                                            supportsBy.add(new SelectOption('Managed Services','Managed Services'));
                                    }else if(support==riverbedAccId){
                                            system.debug('support==riverbedAccId::'+support+'::'+riverbedAccId);
                                            supportsBy.add(new SelectOption('Riverbed','Riverbed'));
                                    }
                            }
                            newOpportunity.Support_Provider_Auth2__c=accTemp.Authorizations_Specializations__c;
                        }
                        system.debug('support--::'+supportBy);
                }
                //save2();
        }
        public List<SelectOption> getSupportsBy(){
                return supportsBy;
        }
        public void setSupportBy(List<SelectOption> allSupportBy){
                this.supportsBy=allSupportBy;
        }
        public void setSupportBy(string supportBy){
                this.supportBy=supportBy;
        }
        public string getSupportBy(){
                return supportBy;
        }

        public PageReference save2(){
                system.debug('Support::'+support);
                system.debug('supportBy::'+supportBy);
                Boolean flagExists = false;
                if(support==null){
                        ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Support Provider.');
                        ApexPages.addMessage(errorMsg);
                        return null;
                }
                else{
                     flagExists = providerExists(supportName);
                    } 
                System.debug('flagExists:'+flagExists);
                if(flagExists){
                    newOpportunity.Support_Provider__c=support;
                    newOpportunity.Support_Provided__c=supportBy;
    
                    //Rev1 - Added by Rucha (5/9/2013)
                    if(selectedOpnetGroupId == '-3') {
                            newOpportunity.Selected_Opnet_Group_Id__c = null;
                            newOpportunity.Opnet_Group_Id__c = null;
                            newOpportunity.Group_Id_Not_Needed__c = true;
                    }
                    else if(selectedOpnetGroupId == '-2'){
                            newOpportunity.Selected_Opnet_Group_Id__c = null;
                            newOpportunity.Opnet_Group_Id__c = null;
                            newOpportunity.Group_Id_Not_Needed__c = false;
                    }
                    else{
                            newOpportunity.Selected_Opnet_Group_Id__c = selectedOpnetGroupId;
                            newOpportunity.Opnet_Group_Id__c = opnetGrpNameToIdMap.get(selectedOpnetGroupId);
                            newOpportunity.Group_Id_Not_Needed__c = false;
                    }
                    // provider by to add
                    providedBy();
    
                    try{
                            update newOpportunity;
                            providerAcctId = newOpportunity.Support_Provider__c; // Rev:1 added for updating the varible 
                            isNotFound = false;
                    }catch(DMLException dme){
                            //system.debug('i m here');
                        // ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Either Sold to Partner or Tier2 Partner is not selected.');
                        // ApexPages.addMessage(errorMsg);
                        return null;
                    }
                    //PageReference oppDetailPage = new PageReference('/'+newOpportunity.id);
                    //oppDetailPage.setRedirect(true);
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Support Provider data saved.');
                    ApexPages.addMessage(errorMsg);
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Support provider not found.'));// Rev:1 added for displaying error messages.
                    isNotFound = true;
                }
                return null;
        }
        public PageReference save3(){//Added by Nikhil for PARPRG-163
                system.debug('Support::'+support);
                system.debug('supportBy::'+supportBy);

                newOpportunity.Support_Provider__c=riverbedAccId;
                newOpportunity.Support_Provided__c=riverbedAccName;
                newOpportunity.Support_Provider_Auth2__c='';

                //Rev1 - Added by Rucha (5/9/2013)
                if(selectedOpnetGroupId == '-3') {
                        newOpportunity.Selected_Opnet_Group_Id__c = null;
                        newOpportunity.Opnet_Group_Id__c = null;
                        newOpportunity.Group_Id_Not_Needed__c = true;
                }
                else if(selectedOpnetGroupId == '-2'){
                        newOpportunity.Selected_Opnet_Group_Id__c = null;
                        newOpportunity.Opnet_Group_Id__c = null;
                        newOpportunity.Group_Id_Not_Needed__c = false;
                }
                else{
                        newOpportunity.Selected_Opnet_Group_Id__c = selectedOpnetGroupId;
                        newOpportunity.Opnet_Group_Id__c = opnetGrpNameToIdMap.get(selectedOpnetGroupId);
                        newOpportunity.Group_Id_Not_Needed__c = false;
                }
                //

                try{
                        update newOpportunity;

                }catch(DMLException dme){
                        //system.debug('i m here');
                    // ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Either Sold to Partner or Tier2 Partner is not selected.');
                    // ApexPages.addMessage(errorMsg);
                    return null;
                }
                //PageReference oppDetailPage = new PageReference('/'+newOpportunity.id);
                PageReference oppDetailPage = ApexPages.currentPage();
                oppDetailPage.setRedirect(true);
                //ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Support Provider data saved.');
                //ApexPages.addMessage(errorMsg);
                return oppDetailPage;

        }

        private Boolean getRASPAccount(Account account){
                Boolean isAuthorized=false;
                List<string> lstAuth=new List<string>();
                string authorized=account.Authorizations_Specializations__c;
                system.debug('Authorized::'+authorized);
                if(authorized!=null){
                        if(authorized.contains(';')){
                        lstAuth=authorized.split(';');
                        for(string auth:lstAuth){
                                if(auth.equalsIgnoreCase('RASP')||auth.equalsIgnoreCase('SUP')){
                                        isAuthorized=true;
                                        break;
                                }
                        }
                        }else if(authorized.equalsIgnoreCase('RASP')||authorized.equalsIgnoreCase('SUP')){
                                isAuthorized=true;
                        }
                }
                return isAuthorized;
        }

        //Rev1 - Added by Rucha (5/7/2013)
        Map<String,Id> opnetGrpNameToIdMap = new Map<String,Id>();
        public List<SelectOption> getOpportunityAccGroupIds(){
                List<SelectOption> selList = new List<SelectOption>();

                selList.add(new SelectOption('-2','--None--'));

                List<Opnet_Account_Group__c> oagList = [Select Id,Name From Opnet_Account_Group__c where Account__c=:newOpportunityCopy.AccountId and Account__c!=null];

                //Rev2 - Added by Rucha (6/25/2013) to show group id if account is not populated
                if(newOpportunityCopy.OpnetGroup_ID__c != null && newOpportunityCopy.OpnetGroup_ID__c!='' && oagList.size()==0){
                        selList.add(new SelectOption(newOpportunityCopy.OpnetGroup_ID__c,newOpportunityCopy.OpnetGroup_ID__c));
                }

                for(Opnet_Account_Group__c ogid : oagList){
                        selList.add(new SelectOption(ogid.Name,ogid.Name));
                        opnetGrpNameToIdMap.put(ogid.Name,ogid.Id);
                }

                selList.add(new SelectOption('-3','Group Id not needed'));
                selList.add(new SelectOption('-1','Create New Group Id'));
                return selList;
        }
        //
        // Rev:1 validation when Save is clicked
        public Boolean providerExists(String searchStr){
            if(searchStr.contains(riverbedAccName)){support=riverbedAccId; return true;}
            Boolean flagExists = false;
             String qry = 'SELECT Id, Name,Type,Authorizations_Specializations__c FROM Account'+ 
            ' WHERE Authorizations_Specializations__c !=null'+
            ' AND Name =: searchStr';
            System.debug('searchStr:'+searchStr);
            List<sObject> soList = Database.query(qry);
            List<Account>newList=new List<Account>();
            System.debug('soList:'+soList);
            System.debug('soList:'+soList.size());
            for(sObject temp:soList){
                Account acc=(Account)temp;
                if(acc.Authorizations_Specializations__c.contains('RASP')||acc.Authorizations_Specializations__c.contains('RASA')||acc.Authorizations_Specializations__c.contains('SUP')){
                    newList.add(acc);
                }
            }
            if(!newList.isEmpty()){
                flagExists = true;
                support = newList[0].Id;
            }
            System.debug('flagExists:'+flagExists);
            return flagExists;
        }
}