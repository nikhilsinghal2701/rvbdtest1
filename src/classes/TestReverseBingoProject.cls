@isTest
public with sharing class TestReverseBingoProject {
    // Test method for AccountRecordTypeUpdateOnLeadConvert trigger
    static testMethod void AccountRecordTypeUpdateOnLeadConvertTest() {
        RecordType accRecType = [Select r.Id, r.Name, r.SobjectType from RecordType r where r.name='Support Customer' and r.SobjectType='Account'];
        Account myAccount = new Account(Name = 'Jones And Sons',recordTypeId=accRecType.Id);
        insert myAccount;
        Lead myLead = new Lead(LastName = 'Fry', Company='Jones And Sons',ByPassValidationsForConversions__c=true);
        insert myLead;
        
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(myLead.id);
        lc.setAccountId(myAccount.Id); // setting the existing account to lead convert
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true and MasterLabel='Sales Qualified' LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);       
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
    }
    
    static testmethod void ReverseBingoSummaryTriggersTest(){
        RecordType[] accRecType = [Select r.Id, r.Name, r.SobjectType from RecordType r where r.SobjectType='Account'];
        Account myAccount = new Account(Name = 'Jones And Sons',recordTypeId=accRecType[0].Id);
        insert myAccount;
        contact myContact = new Contact (firstName='john', lastName='Abrahim', AccountId=myAccount.Id, email='jabrahim@john.com');
        insert myContact;
        case myCase=new case(AccountId=myAccount.Id,ContactId=myContact.Id,Origin='Email',Priority='P4 - Support Request',
                    Status='New');
        insert myCase;
        myCase.Status='Closed';
        myCase.Diagnostics_Data__c='1';
        myCase.Diagnostics_Data__c='2';
        myCase.Diagnostics_Data__c='3';
        myCase.Technical_Expertise__c='3';
        myCase.Version__c='1.0';
        update myCase;
        
        //for AccountReverseBingoReport
        ApexPages.StandardController rba = New ApexPages.StandardController(myAccount);        
        System.currentPageReference().getParameters().put('id',myAccount.id);
        //ReverseBingoController newReverseBingoAccount=new ReverseBingoController(rba);
        //newReverseBingoAccount.init();
        AccountReverseBingoReport accountReverseReport=new AccountReverseBingoReport();
        
        //for AccountReverseBingoReport
        ApexPages.StandardController rbc = New ApexPages.StandardController(myContact);
        System.currentPageReference().getParameters().put('id',myAccount.id);        
        System.currentPageReference().getParameters().put('cid',myContact.id);
        //ReverseBingoContactController newReverseBingoContact=new ReverseBingoContactController(rbc);
        //newReverseBingoContact.init();
        AccountReverseBingoReport contactReverseReport=new AccountReverseBingoReport();      
    }
}