public with sharing class CustomerInitiativesExtension {
	Customer_Initiatives__c ci{get;set;}
	Account_Plan__c ap=null;
	public CustomerInitiativesExtension(ApexPages.StandardController controller){
			this.ci = (Customer_Initiatives__c)controller.getRecord();
			ap=[select id, name,AccountId__c from Account_Plan__c where id=:ci.Account_Plan__c];
	}
	//custome save and redirect to account plan page
	public pageReference customerInitiativesSave(){
		if(ci!=null){    	
	    	try{
	    		upsert ci;
	    	}catch(Exception e){
	    		system.debug('Error:+e.getMessage()');
	    	}
	    	PageReference accPage = new PageReference ('/apex/AccountPlanPage?Id='+ap.Id+'&aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=edit');
        	accPage.setRedirect(true);
        	return accPage;
		}
		return null;    	
    }
    //custome cancel and redirect to account plan page
    public pageReference customerInitiativesCancel(){
    	PageReference accPage = new PageReference ('/apex/AccountPlanPage?Id='+ap.Id+'&aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=edit');
        accPage.setRedirect(true);
        return accPage;
    }
}