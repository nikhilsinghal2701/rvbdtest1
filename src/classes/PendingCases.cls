public with sharing class PendingCases {
     public List<Case> pendingcaseList {get;set;}
     public String caseId;
     Id actId;
          
    public PendingCases(ApexPages.StandardController controller)
    {
        caseId = ApexPages.currentpage().getParameters().get('id');
        List<Case> accList = [select AccountId from Case where id=:caseId ];
        if(accList.size() > 0){
        	actId= accList[0].AccountId;
        }
        pendingcaseList = [Select id,CaseNumber,Case.Contact.name,Subject,case.Owner.name,Status,LastModifiedDate from Case where AccountId =:actId order by LastModifiedDate desc limit 5];
    }
    
    

}