public with sharing class RVBD_RevisitsController {
    
    
    public String userid; 
    public String selectedTab { get; set; }
    public String sortOrder {get; set;}
    public String strleadName {get; set;}
    public String strLdStatus {get; set;}
    public String strLdemail {get; set;}
    public String strldCompany {get; set;}
    public String strConName {get; set;}
    public String strAccName {get; set;}
    public String strConemail {get; set;}
    public String strConCompany {get; set;}
    public String strConDisp {get; set;}
    public String score;
    public String orderlead;
    public String ordercon;
    
    public Lead srchLead {get; set;}
    public Contact srchContact {get; set;} 
    public ApexPages.StandardSetController leadSet {get; set;}
    public ApexPages.StandardSetController conSet {get; set;}   
    public Integer PAGE_SIZE = 20;
    public List<Lead> lstlead = new List<Lead>();
    public List<Contact> lstContact = new List<Contact>();
    
    public  RVBD_RevisitsController(){
        orderlead ='Revisit_Time__c';
        ordercon ='Revisit_Time__c';
        selectedTab = 'Leads';
        sortOrder = 'desc';
        userid = userinfo.getUserID();
        score='B';
        searchLeadsContacts();
        strleadName = '';
        strLdStatus = '';
        strLdemail = '';
        strldCompany = '';
        strConName = '';
        strAccName = '';
        strConCompany = '';
        strConemail ='';
        strConDisp ='';
    }
    
    

   public void refresh(){
        strleadName = '';
        strLdStatus = '';
        strLdemail = '';
        strldCompany = '';
        strConName = '';
        strAccName = '';
        strConCompany = '';
        strConemail = '';
   }
   
    public List<Lead> getLeads(){
        if(leadSet!= null){
         lstlead = (List<Lead>)leadSet.getRecords();
        return lstlead; }
        else return null;   
    }
    
    public List<Contact> getContacts(){
        if(conSet!= null){
        lstContact = (List<Contact>)conSet.getRecords();
        return lstContact; }
        else return null;
    }
    
    public void flipSortOrder() {
   if(ApexPages.currentPage().getParameters().get('ordercon') != null) ordercon =ApexPages.currentPage().getParameters().get('ordercon');
    if(ApexPages.currentPage().getParameters().get('orderlead') != null) orderlead =ApexPages.currentPage().getParameters().get('orderlead');
       sortOrder = (sortOrder == 'desc') ? 'asc':'desc'; 
       system.debug('sort order::::::::::'+sortOrder);
       searchLeadsContacts();
    }
    
    public PageReference searchLeadsContacts(){
        system.debug('selected Tab::::::'+selectedTab);
    
        //if(selectedtab == 'Leads') (!strleadName.equals('')
            
            String ldSql='SELECT Id, Name,Title,Email,Company,Phone,Status,Revisit_Time__c FROM Lead where concierge_Rep__c= :userid and Revisit_Time__c!=null and Lead_Score__c= :score'; 
            if( strleadName != null && strleadName !='')  { ldSql +=  ' and Name  LIKE \''+strleadName.trim()+'%\''; }
            if( strLdStatus != null && strLdStatus !='')  { ldSql +=  ' and Status Like\'%'+strLdStatus.trim()+'%\''; }
            if( strLdemail != null && strLdemail!='')     { ldSql +=  ' and Email = \''+strLdemail.trim()+ '\''; }
            if( strldCompany != null && strldCompany !=''){ ldSql +=  ' and Company = \''+strldCompany.trim()+ '\''; }
            
            ldSql = ldSql + ' ' + 'order by' + ' ' + orderlead + ' ' + sortOrder + ' ' + 'limit 1000' ;
            System.debug('ldSql:::::::::::::::::::::::::::::::::::::'+ldSql);
           
            leadSet  = new ApexPages.StandardSetController(Database.getQueryLocator(ldSql));          
            leadSet.setPageSize(PAGE_SIZE);
            //system.debug(Database.getQueryLocator('SELECT Id, Name,Title,Email,Company,Phone,Status,Revisit_Time__c FROM Lead where concierge_Rep__c = :userid and Revisit_Time__c!=null and Lead_Score__c = :score order by Revisit_Time__c ' + ' ' + sortOrder + ' ' + 'limit 1000' ));
    
        //else if(selectedtab== 'Contacts' )
            String conSql ='SELECT Id, Name,Title,Disposition_Status__c,Email,phone,Account.Name,Revisit_Time__c FROM Contact WHERE concierge_Rep__c = :userid and Revisit_Time__c!=null and Contact_Score__c= :score';
            
            if( strConName != null && strConName !='')       { conSql +=  ' and Name  LIKE \''+strConName.trim()+'%\''; }
            if( strAccName != null && strAccName !='')       { conSql +=  ' and Contact.Account.Name LIKE \''+strAccName.trim()+'%\''; }
            if( strConCompany != null && strConCompany!='')  { conSql +=  ' and Company = \''+strConCompany.trim()+ '\''; }
            if( strConemail != null && strConemail !='')     { conSql +=  ' and Email = \''+strConemail.trim()+ '\''; }
            if( strConDisp != null && strConDisp !='')       { conSql +=  ' and Disposition_Status__c LIKE \'%'+strConDisp.trim()+ '%\''; } 
            
            conSql = conSql + ' ' + 'order by' + ' ' + ordercon + ' ' + sortOrder + ' ' + 'limit 1000' ;
            conSet  = new ApexPages.StandardSetController(Database.getQueryLocator(conSql));
            //system.debug(Database.getQueryLocator('SELECT Id, Name,Title,concierge_Rep__c,LastModifiedDate,Createddate,Email,phone,owner.Alias, Account.Name,Revisit_Time__c FROM Contact WHERE concierge_Rep__c = :userid and Revisit_Time__c!=null and Contact_Score__c= :score order by Revisit_Time__c ' + ' ' + sortOrder + ' ' + 'limit 1000' ));
         
         return null;
    }
     
    public Boolean hasNext {
        get { 
             boolean hasNext;
            if(selectedtab == 'Leads'){
                if(leadSet==null) return false;
                hasNext=leadSet.gethasNext();
            }
            else if(selectedtab == 'Contacts'){
                if(conSet==null) return false;
                hasNext= conSet.gethasNext();
                }
                return hasNext;
        }
    }
    
    public Boolean hasPrevious {
        get { 
             boolean hasPrevious;
            if(selectedtab == 'Leads'){
                if(leadSet==null) return false;
                hasPrevious=leadSet.getHasPrevious();
            }       
            else if(selectedtab == 'Contacts'){
                if(conSet==null) return false;
                hasPrevious= conSet.gethasPrevious();
            }
                return hasPrevious;
        }
        set;
    }
    
    public void first() {
        if(selectedtab == 'Leads'){
        if(leadSet!= null)
            leadSet.first();
        }
        else if(selectedtab == 'Contacts'){
             if(conSet!= null)
                conSet.first();
        }
    }

    public void last() {
        if(selectedtab == 'Leads'){
        if(leadSet !=null) 
            leadSet.last();
        }
        else if(selectedtab == 'Contacts'){
            if(conSet!= null)
               conSet.last();
        }
        
    }

    public void previous() {
        
        if(selectedtab == 'Leads'){
        if(leadSet !=null) 
            leadSet.previous();
        }
        else if(selectedtab == 'Contacts'){
            if(conSet!= null)
               conSet.previous();
        }
    }

    public void next() {
        
        if(selectedtab == 'Leads'){
        if(leadSet !=null) 
            leadSet.next();
        }
        else if(selectedtab == 'Contacts'){
            if(conSet!= null)
               conSet.next();
        }
    }
    
    public Integer totalLeadRecords{
        get { return (leadSet == null)? 0: leadSet.getResultSize(); }
        set;
     }
     
     public Integer totalConRecords{
        get { return (conSet == null)? 0 : conSet.getResultSize(); }
        set;
     }
     
     public Integer leadPageNo{
        get { return (leadSet == null)? 0: leadSet.getPageNumber(); }
        set;
     }
     
     public Integer conPageNo{
        get  { return (conSet == null)? 0 : conSet.getPageNumber(); }
        set;
     }
     
     public Integer noLeadPages{
        get { Double leadno = math.round(leadSet.getResultSize() / leadSet.getPageSize());
             return (leadSet == null)? 0: leadno.intValue();  }
        set;
     }
     
     public Integer noConPages{
        get { Double conno = math.round(conSet.getResultSize() /conSet.getPageSize());
            return (conSet == null)? 0: conno.intValue(); }
        set;
     }
}