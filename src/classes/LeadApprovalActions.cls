/* Util class for Deal Reg approval related methods
    Author: Ankita Goel
    Date: 12/22/2009
*/
public class LeadApprovalActions {

    public static final Set<String> country = new Set<String> {'USA','US','UNITED STATES','UNITED STATES OF AMERICA','U.S.A.','U.S.A','U.S.','U.S'};
    public static Boolean run = true;
    
    public static Boolean runOnce() {
        if (run) {
            run = false;
            return true;
        } else {
            return false;
        }
    }

    //added by Ankita 12/22/09 for setting Email ISR field when lead is submitted for approval - Disti 1B
    @future
    public static void setEmailISROnLead(Set<Id> leadIds){
        System.debug('LeadApprovalActions   setEmailISROnLead  Line - 22');
        //List<Lead> leads = [select id, status, Email_ISR__c from Lead where id in :leadIds];
        Map<Id,Lead> leads = new Map<Id,Lead>([select id, status, Email_ISR__c, Riverbed_ISR__c from Lead where id in :leadIds]);
        Set<Id> leadList = new Set<Id>();
        run = false;
        for(Lead lead : leads.values()){
            if(lead.Status.equals('Submitted') && lead.Email_ISR__c == null){
                leadList.add(lead.Id);  
                //leads.add(new Lead(id = lead.Id, Email_ISR__c = u.email));
            }
        }
        Map<Id, ProcessInstance> pInstance = new Map<Id, ProcessInstance>();
        for(ProcessInstance pi : [Select p.TargetObjectId, p.Status, p.Id, 
                                            (Select Id, ActorId, IsDeleted From Workitems order by SystemModStamp limit 1) 
                                            From ProcessInstance p 
                                            where targetobjectid in :leadList and
                                            status = 'Pending' limit 1000
                                            ]){
                    pInstance.put(pi.Id,pi);                            
                                            }
        System.debug('pInstance Size = ' + pInstance.size());                                   
        Map<id,ProcessInstance> processLeadMap = new Map<Id,ProcessInstance>();
        Map<Id,Id> userIds = new Map<Id,Id>();
        for(ProcessInstance pi : pInstance.values()){
            processLeadMap.put(pi.targetObjectId, pi);
            userIds.put(pi.Id,pi.WorkItems[0].ActorId);
        }                                   
        System.debug('processLeadMap Size = ' + processLeadMap.size());                                 
        Map<Id,User> userMap = new Map<id, User>([select email from User where id in :userIds.values()]);
        if(!processLeadMap.isEmpty()){
        for(Id id : processLeadMap.keySet()){
            if(leads.containsKey(id)){
                Lead lead = leads.get(id);
                if(userIds.containsKey(processLeadMap.get(id).id)){
                    if(UserMap.containsKey(userIds.get(processLeadMap.get(id).id))){
                        lead.Email_ISR__c = UserMap.get(userIds.get(processLeadMap.get(id).id)).Email;
                        lead.Riverbed_ISR__c = userIds.get(processLeadMap.get(id).id);
                    }
                }
            }
        }
        }
        try{
           if(!leads.isEmpty()){
                update leads.values();
            }
        }catch(DMLException e){
            System.debug('Exception in setting email ISR : ' + e.getDMLMessage(0));
        }
        
    }
    
    //added by Nikhil 2/10/14 for removing Future Error
    
    public static void nonfuturesetEmailISROnLead(Set<Id> leadIds){
        System.debug('LeadApprovalActions   setEmailISROnLead  Line - 22');
        //List<Lead> leads = [select id, status, Email_ISR__c from Lead where id in :leadIds];
        Map<Id,Lead> leads = new Map<Id,Lead>([select id, status, Email_ISR__c, Riverbed_ISR__c from Lead where id in :leadIds]);
        Set<Id> leadList = new Set<Id>();
        run = false;
        for(Lead lead : leads.values()){
            if(lead.Status.equals('Submitted') && lead.Email_ISR__c == null){
                leadList.add(lead.Id);  
                //leads.add(new Lead(id = lead.Id, Email_ISR__c = u.email));
            }
        }
        Map<Id, ProcessInstance> pInstance = new Map<Id, ProcessInstance>();
        for(ProcessInstance pi : [Select p.TargetObjectId, p.Status, p.Id, 
                                            (Select Id, ActorId, IsDeleted From Workitems order by SystemModStamp limit 1) 
                                            From ProcessInstance p 
                                            where targetobjectid in :leadList and
                                            status = 'Pending' limit 1000
                                            ]){
                    pInstance.put(pi.Id,pi);                            
                                            }
        System.debug('pInstance Size = ' + pInstance.size());                                   
        Map<id,ProcessInstance> processLeadMap = new Map<Id,ProcessInstance>();
        Map<Id,Id> userIds = new Map<Id,Id>();
        for(ProcessInstance pi : pInstance.values()){
            processLeadMap.put(pi.targetObjectId, pi);
            userIds.put(pi.Id,pi.WorkItems[0].ActorId);
        }                                   
        System.debug('processLeadMap Size = ' + processLeadMap.size());                                 
        Map<Id,User> userMap = new Map<id, User>([select email from User where id in :userIds.values()]);
        if(!processLeadMap.isEmpty()){
        for(Id id : processLeadMap.keySet()){
            if(leads.containsKey(id)){
                Lead lead = leads.get(id);
                if(userIds.containsKey(processLeadMap.get(id).id)){
                    if(UserMap.containsKey(userIds.get(processLeadMap.get(id).id))){
                        lead.Email_ISR__c = UserMap.get(userIds.get(processLeadMap.get(id).id)).Email;
                        lead.Riverbed_ISR__c = userIds.get(processLeadMap.get(id).id);
                    }
                }
            }
        }
        }
        try{
            if(!leads.isEmpty()){
                update leads.values();
            }
        }catch(DMLException e){
            System.debug('Exception in setting email ISR : ' + e.getDMLMessage(0));
        }
        
    }

    public static void approvalAction(Id objId, String action, String comments){
        System.debug('LeadApprovalActions   approvalAction line - 77 ');
        List<ProcessInstance> pInstance = [Select p.TargetObjectId, p.Status, p.Id, 
                                            (Select Id, ActorId, IsDeleted From Workitems order by SystemModStamp limit 1), 
                                            (Select Id, StepStatus From Steps order by SystemModStamp limit 1) 
                                            From ProcessInstance p 
                                            where targetobjectid = :objId and
                                            status = 'Pending'];
        System.debug('ProcessInstance  = '+ pInstance );    
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments(comments);
        req2.setAction(action);
        if (!pInstance.isEmpty()) {
            System.debug('pInstance = ' + pInstance);
            if(pInstance.get(0).Workitems.size() > 0){
                System.debug('workitem = ' + pInstance.get(0).Workitems[0]);
                req2.setWorkitemId(pInstance.get(0).Workitems[0].Id);
                // Submit the request for approval
                try{
                    Approval.ProcessResult result2 =  Approval.process(req2);
                }catch(Exception e){
                    System.debug('Exception in rejecting the approval step: ' + e.getMessage());
                }
            }
        }
    }

    //added by Ankita 3/11/10 for setting lead status = submitted when lead is submitted for approval - Disti 1B
    @future
    public static void setLeadSubmitted(Set<Id> leadIds){
        System.debug('LeadApprovalActions   setLeadSubmitted line - 107 ');
        //List<Lead> leads = [select id, status, Email_ISR__c from Lead where id in :leadIds];
        Map<Id,Lead> leads = new Map<Id,Lead>([select id, status, isSubmitted__c,Email_ISR__c, Riverbed_ISR__c from Lead where id in :leadIds]);
        Set<Id> leadList = new Set<Id>();
        run = false;
        for(Lead lead : leads.values()){
            if(!lead.Status.equals('Submitted') && lead.isSubmitted__c){
                lead.status = 'Submitted';  
                lead.isSubmitted__c = false;
                leadList.add(lead.Id);  
                //leads.add(new Lead(id = lead.Id, Email_ISR__c = u.email));
            }
        }
        Map<Id, ProcessInstance> pInstance = new Map<Id, ProcessInstance>();
        for(ProcessInstance pi : [Select p.TargetObjectId, p.Status, p.Id, 
                                            (Select Id, ActorId, IsDeleted From Workitems order by SystemModStamp limit 1) 
                                            From ProcessInstance p 
                                            where targetobjectid in :leadList and
                                            status = 'Pending' limit 1000
                                            ]){
                    pInstance.put(pi.Id,pi);                            
                                            }
        System.debug('pInstance Size = ' + pInstance.size());                                   
        Map<id,ProcessInstance> processLeadMap = new Map<Id,ProcessInstance>();
        Map<Id,Id> userIds = new Map<Id,Id>();
        for(ProcessInstance pi : pInstance.values()){
            processLeadMap.put(pi.targetObjectId, pi);
            userIds.put(pi.Id,pi.WorkItems[0].ActorId);
        }                                   
        System.debug('processLeadMap Size = ' + processLeadMap.size());                                 
        Map<Id,User> userMap = new Map<id, User>([select email from User where id in :userIds.values()]);
        if(!processLeadMap.isEmpty()){
        for(Id id : processLeadMap.keySet()){
            if(leads.containsKey(id)){
                Lead lead = leads.get(id);
                if(userIds.containsKey(processLeadMap.get(id).id)){
                    if(UserMap.containsKey(userIds.get(processLeadMap.get(id).id))){
                        lead.Email_ISR__c = UserMap.get(userIds.get(processLeadMap.get(id).id)).Email;
                        lead.Riverbed_ISR__c = userIds.get(processLeadMap.get(id).id);
                    }
                }
            }
        }
        }
        try{
            //jsADM.jsADM_ContextProcessController.isInFutureContext = true;//added by Ankita 7/18/2011 for Jigsaw ADM pkg
            if(!leads.isEmpty()){
                update leads.values();
            }
        }catch(DMLException e){
            System.debug('Exception in setting email ISR : ' + e.getDMLMessage(0));
        }
        
    }
    
    public static void nonfuturesetLeadSubmitted(Set<Id> leadIds){
        System.debug('LeadApprovalActions   setLeadSubmitted line - 107 ');
        //List<Lead> leads = [select id, status, Email_ISR__c from Lead where id in :leadIds];
        Map<Id,Lead> leads = new Map<Id,Lead>([select id, status, isSubmitted__c,Email_ISR__c, Riverbed_ISR__c from Lead where id in :leadIds]);
        Set<Id> leadList = new Set<Id>();
        run = false;
        for(Lead lead : leads.values()){
            if(!lead.Status.equals('Submitted') && lead.isSubmitted__c){
                lead.status = 'Submitted';  
                lead.isSubmitted__c = false;
                leadList.add(lead.Id);  
                //leads.add(new Lead(id = lead.Id, Email_ISR__c = u.email));
            }
        }
        Map<Id, ProcessInstance> pInstance = new Map<Id, ProcessInstance>();
        for(ProcessInstance pi : [Select p.TargetObjectId, p.Status, p.Id, 
                                            (Select Id, ActorId, IsDeleted From Workitems order by SystemModStamp limit 1) 
                                            From ProcessInstance p 
                                            where targetobjectid in :leadList and
                                            status = 'Pending' limit 1000
                                            ]){
                    pInstance.put(pi.Id,pi);                            
                                            }
        System.debug('pInstance Size = ' + pInstance.size());                                   
        Map<id,ProcessInstance> processLeadMap = new Map<Id,ProcessInstance>();
        Map<Id,Id> userIds = new Map<Id,Id>();
        for(ProcessInstance pi : pInstance.values()){
            processLeadMap.put(pi.targetObjectId, pi);
            userIds.put(pi.Id,pi.WorkItems[0].ActorId);
        }                                   
        System.debug('processLeadMap Size = ' + processLeadMap.size());                                 
        Map<Id,User> userMap = new Map<id, User>([select email from User where id in :userIds.values()]);
        if(!processLeadMap.isEmpty()){
        for(Id id : processLeadMap.keySet()){
            if(leads.containsKey(id)){
                Lead lead = leads.get(id);
                if(userIds.containsKey(processLeadMap.get(id).id)){
                    if(UserMap.containsKey(userIds.get(processLeadMap.get(id).id))){
                        lead.Email_ISR__c = UserMap.get(userIds.get(processLeadMap.get(id).id)).Email;
                        lead.Riverbed_ISR__c = userIds.get(processLeadMap.get(id).id);
                    }
                }
            }
        }
        }
        try{
            //jsADM.jsADM_ContextProcessController.isInFutureContext = true;//added by Ankita 7/18/2011 for Jigsaw ADM pkg
            if(!leads.isEmpty()){
                update leads.values();
            }
        }catch(DMLException e){
            System.debug('Exception in setting email ISR : ' + e.getDMLMessage(0));
        }
        
    }
    
    public static Boolean runApproved = true;
    
    public static Boolean runOnceApproved() {
        if (runApproved) {
            runApproved = false;
            return true;
        } else {
            return false;
        }
    }
    
    @future
    public static void updateLeads(Set<Id> leads){
        System.debug('LeadApprovalActions   updateLeads line - 176 ');
        List<Lead> leadList = new List<Lead>();
        for(Lead l : [select id, status from Lead where id in :leads]){
            if(l.status.equalsIgnoreCase('Approved')){
                leadList.add(l);
            }
        }
        try{
            //jsADM.jsADM_ContextProcessController.isInFutureContext = true;//added by Ankita 7/18/2011 for Jigsaw ADM pkg
            if(!leadList.isEmpty()){
                update leadList;
            }
        }catch(DMLException e){
            System.debug('Exception in updating leads : ' + e.getDMLMessage(0));
        }
    }

}