/*-----------------------------------------------------------------------------
* File Name      : TimeoutQueueManager
* Description    : class to update the Owner with Time Out queue and auto convert Lead
* @author        : Riverbed
* Modification Log
===============================================================================
* Ver    Date        Author                 Modification
-------------------------------------------------------------------------------
* 1.0    01/01/13    Raj Kumar
---------------------------------------------------------------------------- */

global without sharing class TimeoutQueueManager {
    

    public void changeOwner(){
        // get the no of days for auto convert from custom setting
        List<Lead> updateleads = new List<Lead>();
        Map <String, Remove_Lead_Auto_Convert__c> rl = Remove_Lead_Auto_Convert__c.getAll();        
        string aconvert = rl.get('Lead Update').Auto_Convert__c;
        Integer autoConvert  = integer.valueof(aconvert);
        Date myDate = system.today();
        myDate=myDate.adddays(-1);          
        Id timeoutQueueId = ([select id from group where name ='Time Out' limit 1].Id);
        Id generalLeadId= [select id from RecordType where name ='General Leads' and SobjectType = 'Lead'].Id;
                 
        for(Lead ld :[SELECT Id ,isConverted,Auto_Convert__c from Lead where Auto_Convert__c =: mydate and isConverted= false and recordtypeId=:generalLeadId]) {       
            ld.OwnerId  = timeoutQueueId;
            updateleads.add(ld);
        }               
        //jsADM.jsADM_ContextProcessController.isInFutureContext = true;
        update updateleads;
       //auto convert lead 
    /*if(ld.Associated_Contact__c != null) {
            tempCon = new Contact();
          tempCon = [Select AccountId,Id From Contact where id=:ld.Associated_Contact__c];
         
         if(tempCon != null){ 
                accountId = tempCon.AccountId;
         }
          }
       
       if(accountId!=null || accountId!=''){  
         Database.LeadConvert lc = new Database.LeadConvert();
    
      lc.setLeadId(ld.Id);
      // lc.setOwnerId(ld.Ownerid);
      if(accountid != null) { lc.setAccountId(accountid); }
      if(ld.Associated_Contact__c != null) { lc.setContactId(ld.Associated_Contact__c); }
      lc.setDoNotCreateOpportunity(true); 
    
      // Set Lead Converted Status
      LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
      lc.setConvertedStatus(convertStatus.MasterLabel);
    
      // Convert!
      Database.LeadConvertResult lcr = Database.convertLead(lc);
          if(lcr.isSuccess())
          {   
            system.debug('Conversion Process is Success:::::::::::'+lcr.isSuccess());
            system.debug('Converted AccountID ::::::::::::::'+lcr.getAccountId());
           system.debug('Converted ContactID ::::::::::::::'+lcr.getContactId());
           system.debug('Converted OpportunityID:::::::::::'+lcr.getOpportunityId());       
          }
       }*/
       
        
    }
}