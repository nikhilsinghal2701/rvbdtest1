/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the test class for the class DistiUpliftDetailEditPageExtension 
Created Date : Oct 2011
*******************************************************************************************************/

@isTest
private class DistiUpliftDetailEditPageExtenTest {

    static testMethod void DistiUpliftDetailEditPageExtenTest () // Test Method for the class DistiUpliftDetailEditPageExtension 
     {
        test.starttest();
        Disti_Uplift_Detail__c Obj = new Disti_Uplift_Detail__c ();
        String detailListID;
        List<Category_Master__c> catList=DiscountApprovalTestHlp.insertCategoryMasterData();
        Disti_Uplift__c dis = new Disti_Uplift__c(Name='Disti 1');
        insert(dis);
        Disti_Uplift_Detail__c det;
       // Tests mass edit from Disti Uplift Page 
        PageReference VFpage = Page.DistiUpliftDetailEditPage;
        VFpage.getParameters().put('retUrl','/'+dis.id);
        test.setCurrentPage(VFpage);
         ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(obj);
        DistiUpliftDetailEditPageExtension CLS = new DistiUpliftDetailEditPageExtension(VFpage_Extn);
        if(!CLS.detailList.isempty())
         {
            detailListID = CLS.detailList[0].Id;
            CLS.detailList[0].Value__c = 10;
           
         }
         
        cls.save();
        if(detailListID!=null)
        det = [select Id,Value__c from Disti_Uplift_Detail__c where Id=:detailListID];
        System.assertEquals(det.Value__c,10); // Verifies that record is updated or not
        cls.Cancel();
        // Tests mass edit for single record click scenario.
        VFpage = Page.DistiUpliftDetailEditPage;
        VFpage.getParameters().put('id','/'+det.id);
        test.setCurrentPage(VFpage);
         VFpage_Extn = new ApexPages.StandardController(obj);
         CLS = new DistiUpliftDetailEditPageExtension(VFpage_Extn);
        if(!CLS.detailList.isempty())
         {
            detailListID = CLS.detailList[0].Id;
            CLS.detailList[0].Value__c = 10;
           
         }
         
       
        cls.save();
        if(detailListID!=null)
        det = [select Id,Value__c from Disti_Uplift_Detail__c where Id=:detailListID];
        System.assertEquals(det.Value__c,10); // Verifies that record is updated or not
        cls.Cancel();
        test.stopTest();
    }
}