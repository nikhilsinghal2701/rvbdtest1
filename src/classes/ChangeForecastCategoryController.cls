/*
 * Class Name : ChangeForecastCategoryController
 * Description: Controller class for "Change Forecast Category List View" VF page
 *              which has the logic to update forecast category independent of stage 
 *              for the selected opportunity.
 * Authour    : Sorna (Perficient)
 * Version    : 1.0
 * Date       : 07/10/2013
 */
 
public with sharing class ChangeForecastCategoryController
{
    public Opportunity o {get;set;}
    public string category {get;set;}
    public List<SelectOption> categoriesList {get;set;}

    public date closeDate {get;set;}
    public string NextStep {get;set;}
    public string RDRVPComments {get;set;}
    public string ReasonLost {get;set;}
    
    /* Constructor:
     * gets the opportunity data; intializes and populates the custom category picklist (selectOption) variable
     */
    public ChangeForecastCategoryController(Apexpages.standardcontroller controller)
    {
        o = [Select Id, Stagename, Reason_Lost__c, ForecastCategoryName, ForecastCategory, CloseDate, NextStep, RD_RVP_Comments__c from Opportunity where Id = :apexpages.currentpage().getparameters().get('id')];
        System.debug(' 1  o===> '+o);
        if(o.ForecastCategoryName == null)
            category = '';
        else
            category = o.ForecastCategoryName;

        if(o.Reason_Lost__c == null)
            ReasonLost = '';
        else
            ReasonLost = o.Reason_Lost__c;
        
        categoriesList = ChangeForecastCategoryUtil.getForecastCategoryValues();
    }
    
    /* SaveOpty:
     * sets the stage and forecast category chosen by the user to the opportunity; 
     * saves the optys and return back to its detail view;
     */
    public PageReference saveOpty()
    {
        try
        {
            if(ChangeForecastCategoryUtil.validateCategory(category))
            {
                o.ForecastCategoryName = category;                
                if (o.StageName == '7 - Closed (Not Won)' && o.Reason_Lost__c == null)
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select "Reason Lost " if you want to change this opportunity stage to 7 - closed (Not Won)'));
                    return null;
                }
                if (o.StageName == '7 - Closed (Not Won)' && o.ForecastCategoryName != 'Omitted')
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Forecast Category to "Omitted" if you want to change this opportunity stage to 7 - closed (Not Won)'));
                    return null;
                }
              update o;
                
                return new pageReference('/'+o.Id);
            }
            else
            {
                return null;
            }
        }
        catch(Exception e)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }
}