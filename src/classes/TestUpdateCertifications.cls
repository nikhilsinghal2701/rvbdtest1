@isTest
private class TestUpdateCertifications {

    static testMethod void myUnitTest() {
        Account a = new Account();
				a.Name = 'test account';
				a.Type ='Partner Account';
				insert a;
				
				Acct_Cert_Summary__c c1 = new Acct_Cert_Summary__c();
				c1.Account__c = a.id;
				c1.TAP__c = 'General';
				insert c1;	
			
				Contact cn1 = new Contact();
				cn1.AccountId = a.id;
				cn1.FirstName = 'test';
				cn1.LastName = 'tester';
				insert cn1;
				
				Certificate__c ce1 = new Certificate__c();
				ce1.Name = 'RSA';
				ce1.Contact__c = cn1.id;
				ce1.Certification_Summary__c = c1.id;
    		insert ce1;
    		
    		cn1 = [select LeftCompany__c from Contact where id = :cn1.id];
    		cn1.LeftCompany__c = true;
    		update cn1;
    		
    		ce1 = [select Contact_Left_Account__c from Certificate__c where id = :ce1.id];	
    		system.assert(ce1.Contact_Left_Account__c == 'Yes');
    		
    		cn1 = [select LeftCompany__c from Contact where id = :cn1.id];
    		cn1.LeftCompany__c = false;
    		update cn1;
    		
    		ce1 = [select Contact_Left_Account__c from Certificate__c where id = :ce1.id];	
    		system.assert(ce1.Contact_Left_Account__c == 'No');
    }
}