public class DeactivateOneOffUser {

	/*
	* This method deactivates the partner user. It takes an array of user ids which need to be deactivated.
	* It runs asynchronously (Future) as User operations are not permitted within the same transaction.
	*/
	@future
    public static void deactivate(String[] userIds)
    {
    	List<User> users = new List<User>();
    	for (Integer i = 0; i < userIds.size(); i++) {
    		users.add(new User(Id = userIds[i], isActive = false));	
    	}
    	try {
    		update users;
    	} catch(Exception e) {}
    }

	/*
	* This method sets the partner user's profile based on account fields. 
	* It runs asynchronously (Future) as User operations are not permitted within the same transaction.
	*/
    @future
    public static void changeUserProfile(String[] userIds) {
    	List<User> users = [Select Id, Contact.AccountId from User where Id In :userIds and IsActive = true];
		Set<Id> accountIds = new Set<Id>();
		for (User u : users) {
			accountIds.add(u.Contact.AccountId);
		}
		
    	Map<Id, Account> accountMap = new Map<Id, Account>([Select Geographic_Coverage__c,Region__c, Preferred_Distributor_Lead_Mapped__c
    															from Account where Id In :accountIds]);
		
		for (User u : users)  {
			Id accountId = u.Contact.AccountId;
			Account acc = accountMap.get(accountId);
			Id profileId = getProfileForAccount(acc);
			if (profileId != null) {
				u.ProfileId = profileId;
			}
		}
		
		update users;														
    }
    
    public static Id getProfileForAccount(Account acc) {
    	String geoCoverage = acc.Geographic_Coverage__c;
    	geoCoverage = (geoCoverage != null) ? geoCoverage : '';
    	String region = acc.Region__c;
    	region = (region != null) ? region : '';
    	String distributor = acc.Preferred_Distributor_Lead_Mapped__c;
    	
    	Id profileId = null;
    	
		if (geoCoverage.equals(AMERICAS)) {
			if (region.contains(LATAM)) {
				profileId = PROFILE_NA_LATAM;
			} else {
				profileId = PROFILE_NA;
			}
		} else if (geoCoverage.equals(EMEA)) {
			if (distributor == null) {
				profileId = PROFILE_EMEA_DIRECT;
			} else {
				profileId = PROFILE_EMEA_INDIRECT;
			}
		} else if (geoCoverage.equals(APAC)) {
			profileId = PROFILE_APAC;	
		}
    	return profileId;
    }
    
    public static String PROFILE_APAC = '00e70000000vPmr';    //PRM - APAC Reseller - Admin
    public static String PROFILE_EMEA_INDIRECT = '00e70000000wXnJ';	//PRM - EMEA IndirectVAR - Admin
    public static String PROFILE_EMEA_DIRECT = '00e70000000wXmz';	//PRM - EMEA DirectVAR - Admin
    public static String PROFILE_NA_LATAM = '00e70000000vPmr';	//PRM - APAC Reseller - Admin
    public static String PROFILE_NA = '00e70000000vPmm';	//PRM - NA Reseller - Admin
 
    
    //geos
    public static String AMERICAS = 'Americas';    
    public static String EMEA = 'EMEA';    
    public static String APAC = 'APAC';
    //region
    public static String LATAM = 'Latin America';
    //added by Ankita 5/12/2010 to update account to uncheck the partner recruitment flag
    public static void updatePartnerAccount(Set<Id> accIds){
    	List<Account> accts = [select Partner_Recruitment_Account__c from Account where id in :accIds];
    	for(Account a : accts){
    		a.Partner_Recruitment_Account__c = false;
    	}
    	update accts;
    }
    
    
}