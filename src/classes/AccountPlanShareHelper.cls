/*******************************************************************************************
// Class Name: AccountPlanShareHelper
//
// Description: This class is the trigger handler for AccountPlanteamtrigger.
// This class add or delete Account Plan Shares based on Account Team Member under Account plan
//
// Created By: Nikhil Singhal (Perficient) - 7/10/2014
*******************************************************************************************/


public without sharing class AccountPlanShareHelper {
    public static void assignAccountPlanShare(List<Account_Plan_Team__c> accountPlanteamlist,Boolean isupdate,Boolean Isdelete) {
        Set<Id> orgAccPlanteamIdSet = new Set<Id> ();
        Set<Id> orgAccPlanIdSet = new Set<Id> ();
        Set<Id> orgAccPlanuserIdSet = new Set<Id> ();
        if(!accountPlanteamlist.isEmpty() && !isupdate && !Isdelete){//After Insert
        for(Account_Plan_Team__c apt: accountPlanteamlist) {
            if(apt.user__c !=null){
            orgAccPlanteamIdSet.add(apt.id);
            }
            }
        if(!orgAccPlanteamIdSet.isEmpty()){    
        insertAccountPlanShare(orgAccPlanteamIdSet);
        }
        } else if(isupdate && !accountPlanteamlist.isEmpty() && !Isdelete){//After Update
            for(Account_Plan_Team__c apt: accountPlanteamlist) {
            if(apt.user__c !=null){
            orgAccPlanteamIdSet.add(apt.id);
            orgAccPlanIdSet.add(apt.Account_Plan__c);
            orgAccPlanuserIdSet.add(apt.user__c);
            }
            }
            if(!orgAccPlanteamIdSet.isEmpty() && !orgAccPlanIdSet.isEmpty() && !orgAccPlanuserIdSet.isEmpty()){
        delteAccountPlanShare(orgAccPlanteamIdSet,orgAccPlanIdSet,orgAccPlanuserIdSet,true);   
            } 
        } else if(!isupdate && !accountPlanteamlist.isEmpty() && Isdelete){//After Delete
            for(Account_Plan_Team__c apt: accountPlanteamlist) {
             if(apt.user__c !=null){        
            orgAccPlanteamIdSet.add(apt.id);
            orgAccPlanIdSet.add(apt.Account_Plan__c);
            orgAccPlanuserIdSet.add(apt.user__c);
             }
            }
         if(!orgAccPlanteamIdSet.isEmpty() && !orgAccPlanIdSet.isEmpty() && !orgAccPlanuserIdSet.isEmpty()){    
        delteAccountPlanShare(orgAccPlanteamIdSet,orgAccPlanIdSet,orgAccPlanuserIdSet,false);   
         }
        }
        
    }
    
    //Add Share
    public static void insertAccountPlanShare(Set<Id> orgAccPlanteamIdSet) {
        Map<id,Account_Plan_Team__c> apTMap = new Map<id,Account_Plan_Team__c>([Select Id,Access__c,Account_Plan__c,Role__c,User__c from Account_Plan_Team__c where id=:orgAccPlanteamIdSet]);
        List<Account_Plan__Share> aptShrs = new List<Account_Plan__Share>();
        system.debug('apTMap'+apTMap);
        for(id aptid :apTMap.keyset()){
            Account_Plan__Share APshare = new Account_Plan__Share();
            APshare.UserOrGroupId=aptmap.get(aptid).User__c;
            APshare.ParentId=aptmap.get(aptid).Account_Plan__c;
            if(aptmap.get(aptid).Access__c=='Read/Edit'){
            APshare.AccessLevel ='Edit';
            } else {
            APshare.AccessLevel ='Read';    
            }
            aptShrs.add(APshare);
        }
        if(!aptShrs.isEmpty()){
        insert aptShrs;
        }
    }
    
    //delete Share
    public static void delteAccountPlanShare(Set<Id> orgAccPlanteamIdSet,Set<Id> orgAccPlanIdSet,Set<Id> orgAccPlanuserIdSet,Boolean doinsert) {
                  
        Map<id,Account_Plan__Share> apTshareMap = new Map<id,Account_Plan__Share>([Select Id,ParentId from Account_Plan__Share where ParentId in :orgAccPlanIdSet and UserOrGroupId in :orgAccPlanuserIdSet and RowCause = :Schema.Account_Plan__Share.RowCause.Manual]);
        if(!apTshareMap.isEmpty()){
        delete apTshareMap.values();
        }
        if(doinsert){
        insertAccountPlanShare(orgAccPlanteamIdSet);
        }
    }

}