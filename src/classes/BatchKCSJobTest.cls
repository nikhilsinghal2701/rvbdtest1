@isTest
public class BatchKCSJobTest {

	//**** ONLY EXECUTE When blockage triggers are disabled
	static testMethod void testBatchKcsJob() {
		Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
        
		BatchKCSJob task = new BatchKCSJob(1);
		Database.executeBatch(task);
		Test.stopTest();		
	}
	
	
	// ensure query return cases that are greater than specified date
	static testMethod void testQuery() {
		Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
        
		List<Case> caseList = new List<Case>();
		DateTime startDate = DateTime.parse('1/1/2014 12:00 AM');
		
		String query = 'SELECT Id,Status,CreatedDate FROM Case ' + 
				'WHERE (Status=\'Closed\' ' + 
				'OR Status=\'Closed - Resolved\' ' +
				'OR Status=\'Closed - Resolved (First Time Fix)\' ' +
				'OR Status=\'Closed - Unresolved\') ' +
				'AND CreatedDate >= :startDate LIMIT 100';
				
		caseList = Database.query(query);
		
		for (Case c : caseList) {
			system.assert(c.CreatedDate >= startDate, 'Created Date is incorrect');
		}
		Test.stopTest();
	}
	

}