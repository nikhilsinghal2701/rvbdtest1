/*************************************************************************************************************
 Name:ArchiveMigrationTest
 Author: Santoshi Mishra
 Purpose: This is test class for the class ArchiveMigration.
 Created Date : Nov 2011
*************************************************************************************************************/
@isTest(seeAllData=true)
private class  ArchiveMigrationTest {
    
    static testmethod void ArchiveMigrationTest()
    {
    test.starttest();
     List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
       
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        Quote__C q = DiscountApprovalTestHlp.createQuote(opp.Id);
        ArchiveMigrationClass cls1 = new ArchiveMigrationClass(); 
        ArchiveMigrationClass cls = new ArchiveMigrationClass(opp.Id); 
        database.executebatch(cls);        
        /*
        List<Discount_Detail__c> detList = [select id,opportunity__C,quote__C from Discount_Detail__c where opportunity__C=:opp.id or quote__C=:q.Id];
        List<Discount_Detail_Archive__c> archiveList =  [select id,opportunity__C,quote__C from Discount_Detail_Archive__c where opportunity__C=:opp.id or quote__C=:q.Id];
        System.assertequals(detList.isempty(),true);
        System.assertequals(archiveList.isempty(),false);*/
        test.stoptest();
            
    }
    static testmethod void rollbackTest()
    {
        test.starttest();
        List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();       
        Opportunity opp =DiscountApprovalTestHlp.createOpp();        
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        Quote__C q = DiscountApprovalTestHlp.createQuote(opp.Id);
        ArchiveMigrationRollback cls1 = new ArchiveMigrationRollback(); 
        ArchiveMigrationClass clss = new ArchiveMigrationClass(opp.Id); 
        database.executebatch(clss);
        ArchiveMigrationRollback cls = new ArchiveMigrationRollback(opp.Id); 
        database.executebatch(cls);
        test.stoptest();
    }
}