public class DiscountApprovalVar {
public static final Id WW_SALES =  '00E30000000dLJZ';
public static final Id CEO =  '00570000000mvxC';
public static final Id CFO =  '00530000000cYPV';
public static final Id APPROVAL_EXCEPTION =  '00570000000mfzm';
public static final Id PS_DIRECTOR = '00E70000000wFJ5';
public static final Id VP_SERVICES = '00E70000000xlcr';
public static final String REPLY_TO = 'discount_approval@7dugwvebhxdbxb35fyu8dj6knq4nlt5xcif2uehp6ik46xrm0.19-efgveak.cs24.apex.sandbox.salesforce.com';
public static final String URL = 'https://riverbed--test1.cs24.my.salesforce.com/';
}