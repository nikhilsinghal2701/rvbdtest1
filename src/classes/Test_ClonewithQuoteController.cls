/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
@isTest(SeeAllData=true)
private class Test_ClonewithQuoteController {

    private static testMethod void test_ClonewithQuote() {
        
        PageReference pref = Page.CloneOpportunityWithQuote;
        Test.setCurrentPage(pref);
        Opportunity Obj = DiscountApprovalTestHlp.createopp(); 
        System.debug(LoggingLevel.Error,'New Opportunity Created : '+obj.Id);       
        test.setCurrentPage(new PageReference('/apex/CloneOpportunityWithQuote?opnetAccReq=false&id=' + Obj.Id));
        
        //RVBD_Email_Properties__c rvbdEmail = new RVBD_Email_Properties__c(Name = 'PWS',Server_URL__c='https://powerstripuat.riverbed.com',
          //                              mc_org_wide_email_Id__c = 'false');
        
        ApexPages.StandardController con = new ApexPages.StandardController(Obj); 
        ClonewithQuoteController ext = new ClonewithQuoteController(con); 
        Test.startTest(); 
        String nextPage = con.save().getUrl();
        //ext.getURLEncode(rvbdEmail.Name);
        PageReference ref = ext.Save();
        PageReference ref2 = ext.newcancel();
        Test.stopTest(); 

    }

}