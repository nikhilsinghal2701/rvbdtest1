@isTest(SeeAllData=true)
private class ForcastTestClass {

static testMethod void Testmyclass() {

      
             
        Opportunity op = new Opportunity(Name='Test',StageName='Testing',CloseDate=Date.newInstance(2011, 01, 01),ownerid=userinfo.getUserId());
        insert op;
        
        Quote__c o=new Quote__c(Name='Test',Opportunity__c=op.id,Forecasted_Quote__c = true,ownerid=userinfo.getUserId());
        insert o;
        Product2 prod = [SELECT Name, ProductCode FROM Product2 WHERE ProductCode = 'SVC-PSD-00002'];//Added by prashant on 1/17/2013 for test code failure
        Quote_Line_Item__c ql = new Quote_Line_Item__c(Quote__c=o.Id, Non_Standard_Discount__c=30,Qty_Ordered__c=1, Category__c='E', Product_Code__c=prod.ProductCode, Product2__c=prod.Id, CurrencyIsoCode=UserInfo.getDefaultCurrency()); //Added by prashant on 1/17/2013 for test code failure
        try{
            insert ql;
        }catch(Exception e){
            system.debug('Exception:'+e);
        }
        test.starttest();
        ApexPages.StandardController sc = new ApexPages.StandardController(o);
        product2quote cn = new product2quote(sc);
        pagereference pg=new pagereference('/apex/product2quote?id='+o.ID); 
        system.test.setCurrentpage(pg); 
        pagereference pg1= cn.product2quotes();
        cn.Setproduct2quotes(pg.getparameters().get('id')); 
        String st=cn.Getproduct2quotes();       
        test.stoptest();
     }
}