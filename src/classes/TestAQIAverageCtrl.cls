@IsTest

private class TestAQIAverageCtrl {

    static testMethod void testAQIAverage() {	
    	TestAQIRQIUtils testTools = new TestAQIRQIUtils();
    	Test.startTest();
    	User testUser = testTools.createMember();
    	
    	AQI_Audit__c assessment = new AQI_Audit__c(Author__c = testUser.Id);
    	assessment.Answer_1__c = 'Yes';
    	assessment.Answer_2__c = 'Yes';
    	assessment.Answer_3__c = 'Yes';
    	assessment.Answer_4__c = 'Yes';
    	assessment.Answer_5__c = 'Yes';
    	assessment.Answer_6__c = 'Yes';
    	assessment.Answer_7__c = 'Yes';
    	assessment.Answer_8__c = 'Yes';
    	assessment.Answer_9__c = 'Yes';
    	assessment.Answer_10__c = 'Yes';
    	assessment.Status__c='Complete';
    	 
     	insert assessment;
    	
		AQIAverageCtrl AAC = new AQIAverageCtrl(new ApexPages.standardController(assessment));
		System.assert(AAC.theAverage != null);
		Test.stopTest();
    }
}