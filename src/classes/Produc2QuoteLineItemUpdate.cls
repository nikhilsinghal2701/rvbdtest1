/*========================================================================================================================*/
/* Added 3 fields on Product Object called 'Old cost','OldCostNotEqualToNewCost'and'RunBatch'. Aim is when COGS updated corresponding all open
 quote line items needs to be synced so that margin summary will be updated accurately */
global class Produc2QuoteLineItemUpdate implements Database.Batchable<sobject>
{
    global String query;
    global List<Quote_Line_Item__c> UpdateQLines;
    Product2 prod;
    List<Product2> prodList=new List<Product2>();
    Map<Id,Product2> prodMap=new Map<Id,Product2>();
    global String error,DiscountStatus='\'Approved\'', QuoteStatus='\'%Draft%\'';
    global Produc2QuoteLineItemUpdate() {
        query='SELECT Id,Product2__c,Product2__r.Old_cost__c,Product2__r.cost__c,Product2__r.RunBatch__c,Product2__r.OldCostEqualToNewCost__c FROM Quote_Line_Item__c where Quote__r.Discount_Status__c<>'+
        DiscountStatus+' AND Quote__r.Opportunity__r.isclosed=false AND Quote__r.Quote_Status__c like '+QuoteStatus
        +' AND Product2__r.OldCostEqualToNewCost__c=True';
        system.debug('Query:'+query);
    }
    
    global database.queryLocator start(Database.BatchableContext BC) 
    {  
        return database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {   UpdateQLines = new List<Quote_Line_Item__c>();
        for(sObject so : scope){
            Quote_Line_Item__c Qltm=(Quote_Line_Item__c) so;
            UpdateQLines.add(Qltm);
             prod=new Product2(id=Qltm.product2__c,RunBatch__c=true);
             prodMap.put(Qltm.product2__c,prod);
        }
        system.debug('UpdateQLines ******:'+UpdateQLines.size());
            try{
                if(UpdateQLines.size()>0){
                    update UpdateQLines;
                    system.debug('UpdateQLines:'+UpdateQLines);
                    }
                 system.debug('UpdatedProducts ******:'+prodMap.values());  
                if(prodMap.size()>0){
                    update prodMap.values();    
                }   
                }
        catch(DMLException e){
        system.debug('Quote line item updated:'+e); 
        }
    }
    global void finish(Database.BatchableContext BC)
    {  
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =: BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toEmailAddresses = new String[] {'anil.madithati@riverbed.com'};
        mail.setToAddresses(toEmailAddresses);
        mail.setSubject('BatchJob Status' + a.Status);
        mail.setHTMLBody
        ('Total No of Batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
    }
}