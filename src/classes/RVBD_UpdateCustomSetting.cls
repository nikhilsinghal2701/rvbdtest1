public with sharing class RVBD_UpdateCustomSetting {
    public Integer removeLead                  {get;set;}
    public Integer autoConvert                 {get;set;}
    public Remove_Lead_Auto_Convert__c rlac    {get;set;}
    
    public  RVBD_UpdateCustomSetting(){
        rlac = [SELECT Auto_Convert__c, Eloqua_Disassociation_Date__c FROM Remove_Lead_Auto_Convert__c WHERE name = 'Lead Update'];    
         autoConvert  = integer.valueof(rlac.Auto_Convert__c);
         removeLead = integer.valueof(rlac.Eloqua_Disassociation_Date__c);
    }
     
    public pagereference save(){
        rlac.Auto_Convert__c = string.valueof(autoConvert);
        rlac.Eloqua_Disassociation_Date__c = string.valueof(removeLead);
        update rlac;    
        //updateLeads();
        return null;
    }
    
   /* public void updateLeads(){
        List<Lead> leadlst = [Select id,Auto_Convert__c,Eloqua_Disassociation_Date__c,CreatedDate from Lead where isConverted = false  and  Eloqua_Disassociation_Date__c != null and Auto_Convert__c!=null ];
        List<Lead> uplead = new List<Lead>();
        Date created ;
        Boolean isupdate = false;
        Integer ac,ra;
        ac = integer.valueof(autoConvert);
        ra = integer.valueof(removeLead);
        system.debug('::::::::::::::::::::::::: ac rc'+ac +ra);
        for(Lead ld : leadlst){
            created = Date.valueof(ld.createdDate);
            if(ld.Auto_Convert__c != created.addDays(ac)){
             ld.Auto_Convert__c = created.addDays(ac);
             isupdate = true;
            }
            
            if(ld.Eloqua_Disassociation_Date__c != created.addDays(ac)){
                 ld.Eloqua_Disassociation_Date__c = created.addDays(ra);
                isupdate = true;
            }
            
            if(isupdate){
                uplead.add(ld); 
            }       
        }
        update uplead;  
        //system.debug(':::::::::::::::::::::::::'+uplead);
    }*/
}