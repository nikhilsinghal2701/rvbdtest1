/*  This class is the controller for the MarginCalculator - What if scenario page.
    Date: 1/11/2011
    Author: Ankita Goel
*/
public with sharing class MarginCalculatorController {
    public List<Quote_Line_Item__c> quoteLines {get;set;}
    public List<QuoteLineWrapper> scenarioLines {get;set;}
    public Quote__c quote {get; private set;}
    private List<Product2> products {get; set;}
    public List<ProductWrapper> productsList {get; set;}
    public Product2 tempProduct {get; set;} 
    public List<SelectOption> options;
    public String family {get;set;}
    public String keyword {get;set;}
    public String subFamily {get;set;}
    public Boolean showNonCogs {get; private set;} 
    public Boolean error {get; private set;}
    public String errorMsg {get; private set;}
    public String severe {get; private set;}
    //for email -- start
    private RVBD_Email_Properties__c eProp;
    public String subj {get; set;}
    public String body {get; set;}
    public String toAddress {get; set;}
    public String ccAddress {get; set;}
    private List<string> toAddresses = new List<string>();
    private List<string> ccAddresses = new List<string>();
    private List<Quote_Line_Item__c> secList = new List<Quote_Line_Item__c>();
    //MarginCalculatorEmailHlp mEmailHlp;
    //for email -- end
    String prodQuery = ''; 
    public QuoteSummary qs {get; private set;}
/*
    Constructor
*/  
    public MarginCalculatorController(ApexPages.StandardController controller){     
        System.debug('MarginCalculatorController - Constructor called');
        eProp=RVBD_Email_Properties__c.getValues('rvbd_email_settings');
        error = false;
        errorMsg = '';
        severe = '';
        qs = new QuoteSummary();
        Id quoteId = controller.getId();
        quote = [select id, name, Total_Weighted_Average__c, Total_Weighted_Average_for_Products__c, Total_Weighted_Average_for_Support__c,
                D_Discount_Demos__c, Margin_Product__c, Margin_Support__c, Margin_Training__c, Margin_Total__c,Opportunity__r.Primary_ISR__c, RSM__c,
                Scenario_Margin_P_Product__c, Scenario_Margin_P_Support__c, Scenario_Margin_P_Total__c, Scenario_Margin_P_Training__c,
                Opportunity__r.Pricebook2Id, Opportunity__r.stagename,Opportunity__c, Product_Quote_Total__c, Support_Quote_Total__c,
                Training_Quote_Total__c, Quote_Total__c
                from Quote__c where id = :quoteId];
            
        qs.quoteProdDiscount = quote.Total_Weighted_Average_for_Products__c == null ? 0 : quote.Total_Weighted_Average_for_Products__c;
        qs.quoteSupportDiscount = quote.Total_Weighted_Average_for_Support__c == null ? 0 : quote.Total_Weighted_Average_for_Support__c;
        qs.quoteTotalAvgDiscount = quote.Total_Weighted_Average__c == null ? 0 : quote.Total_Weighted_Average__c;       
        quoteLines = [select id, product_code__c, Qty_Ordered__c, D_Standard_Discount__c, Unit_List_Price__c, D_Unit_Price__c, D_Ext_Price__c,Unit_Cost__c,
                        Margin__c, Margin_Percent__c,Scenario_Discount__c, Scenarion_Margin_Percent__c, Scenario_Quantity__c,Scenario_Extended_Price__c,
                        Disti_Approved_Discount__c, Category__c
                        from Quote_Line_Item__c where quote__c = :quoteId 
                        and (Unit_List_Price__c != null and Unit_List_Price__c != 0)];//and (Unit_Cost__c != 0 and Unit_Cost__c  != null)
        scenarioLines = new List<QuoteLineWrapper>();
        for(Quote_Line_Item__c ql : quoteLines){
            Boolean costFlag = false;
            if(ql.Unit_Cost__c == null || ql.Unit_Cost__c == 0){
                costFlag = true;
            }
            QuoteLineWrapper qw = new QuoteLineWrapper(ql, costFlag);
            scenarioLines.add(qw);      
        }
        showNonCogs = true; 
        try{
            if(quote.Opportunity__r.stagename.equalsIgnoreCase('6 - Order Accepted') || quote.Opportunity__r.stagename.equalsIgnoreCase('7 - Closed (Not Won)'))    {
                error = true;
                severe = 'error';
                errorMsg = 'The opportunity is already closed.';
            }
        }catch(Exception e){
            error = true;
            severe = 'error';
            errorMsg = 'You are not authorized to perform the action on this quote. The quote\'s opportunity is not owned by you.';
        }
    }
    
    //opens a new page for product search and selection to be added to the scenario
    public PageReference addProducts(){
        if(products != null && !products.isEmpty()){
            products.clear();
        }
        if(productsList != null){
            productsList.clear();
        }else{
            productsList = new List<ProductWrapper>();
        }
        //tempProduct = new Product2();
        return Page.AddProductsToScenario;
    }
    
    public List<SelectOption> getOptions(){
        options = new List<SelectOption>();
        Schema.DescribeFieldResult F = Product2.Family.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();       
        options.add(new SelectOption('','None'));
        for(Schema.PickListEntry ple : P){
            options.add(new SelectOption(ple.getValue(), ple.getLabel()));
        }       
        return options;
    }
    //search products based on the criteria
    public PageReference searchProducts(){
        error = false;
        System.debug('Search Products ' + keyword);
        
/*      if(keyword == null || keyword == ''){
            error = true;
            severe = 'error';
            errorMsg = 'Please enter a keyword to search';
            return null;
        }*/
        System.debug('Family = ' + family);
        if(productsList != null){
            productsList.clear();
        }else{
            productsList = new List<ProductWrapper>();
        }
        if(prodQuery != ''){
            prodQuery = '';
        }
        prodQuery += 'select id, name, family, productcode, description, category__c, cost__c from Product2 ';
        System.debug('Query1 = ' + prodQuery);
        prodQuery += ' where (name like \'%' + keyword + '%\' or description like \'%' + keyword + '%\')';
        System.debug('Query2 = ' + prodQuery);
        if(subFamily != null && subFamily != ''){
            prodQuery += ' and Product_SubFamily__c = \'' + subFamily + '\'';
        }
        System.debug('Query3 = ' + prodQuery);
        if(family != null && family != ''){
            prodQuery += ' and family = \'' + family + '\'';
        }
        prodQuery += ' limit 1000';
        /*if(tempProduct.family != null ){
            prodQuery += ' and family = \'' + tempProduct.family + '\'';
        }*/
        System.debug('Query4 = ' + prodQuery);
        
    //  prodQuery =  and (name like \'%' + keyword + '%\' or description like \'%' + keyword + '%\'';
        products = Database.query(prodQuery);
        if(!products.isempty()){
            for(Product2 prod : products){
                ProductWrapper pw = new ProductWrapper(prod);
                productsList.add(pw);
            }
        }else{
            error = true;
            severe = 'info';
            errorMsg = 'Your search did not return any results. Please select a different criteria and try again.';
        }
        return null;
    }
    
    //add selected products to the scenario
    public PageReference addSelectedProducts(){
        Set<Id> productIds = new Set<Id>();
        for(ProductWrapper pw : productsList){
            if(pw.checked){
                productIds.add(pw.prod.id);
            }
        }
        List<PricebookEntry> priceBookEntries = [Select id,Productcode, Product2Id, Pricebook2Id,unitprice From PricebookEntry
                                             where pricebook2id  in (select pricebook2id from opportunity where id = :quote.Opportunity__c) 
                                             and product2id in :productIds and isactive = true];   
        Map<Id,PricebookEntry> productIdToPricebookEntryMap = new Map<Id,PricebookEntry>();
        for(PricebookEntry p : pricebookEntries) {
            productIdToPricebookEntryMap.put(p.Product2Id, p);
        }
        for(ProductWrapper pw : productsList){
            if(pw.checked){
                Decimal price = 0;
                Boolean costFlag = false;
                if(productIdToPricebookEntryMap.containsKey(pw.prod.Id)){
                    price = productIdToPricebookEntryMap.get(pw.prod.id).UnitPrice;
                    System.debug('Price = ' + price);
                    if(pw.prod.Cost__c == null || pw.prod.cost__c == 0){
                        costFlag = true;
                    }                                       
                }
                scenarioLines.add(new QuoteLineWrapper(new Quote_Line_Item__c(product_code__c = pw.prod.productcode,unit_List_price__c = price,
                                                        Unit_Cost__c = pw.prod.Cost__c, Category__c = pw.prod.category__c),costFlag));
            }
        }
        
        return Page.MarginCalculator;
    }
    
    //remove products from the scenario
    public PageReference removeProducts(){
        List<QuoteLineWrapper> removeLines = new List<QuoteLineWrapper>();
        removeLines.addAll(scenarioLines);
        scenarioLines.clear();
        for(QuoteLineWrapper qw : removeLines){
            if(!qw.checked){
                scenarioLines.add(qw);
            }
        }
        return null;
    }

    //sends email to the requestor or any other recepients specified by the user
    public pageReference email(){
        set<Id> userIds=new set<Id>();      
        if(quote!=null){
            if(quote.Opportunity__r.Primary_ISR__c!=null) userIds.add(quote.Opportunity__r.Primary_ISR__c);
            if(quote.RSM__c!=null)userIds.add(quote.RSM__c);
            userIds.add(UserInfo.getUserId());
            if(userIds.size()>0){
                List<User> userList=[Select Id,email from User where Id IN:userIds];
                if(userList.size()>0){
                    for(User u:userList){
                        if(u.Id!=UserInfo.getUserId()){
                            toAddresses.add(u.email);
                        }else{
                            ccAddresses.add(u.email);
                            ccAddress = u.email;
                        }
                    }
                }
            }
            for(String temp : toAddresses){
                if(toAddress==null||toAddress==''){
                    toAddress=temp;
                }else{
                    toAddress = toAddress+','+temp;
                }  
            }       
            subj = 'Margin Analysis for quote: '+quote.Name;
        }
        return Page.scenarioToEmail;
    }
    
    public PageReference sendEmail() { 
        body =body+getBody(scenarioLines);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if(toAddress!=null && toAddress.length()>0){
            toAddresses.clear();
            toAddresses=toAddress.split(',');
            mail.setToAddresses(toAddresses);
        }
        if(ccAddress!=null && ccAddress.length()>0){
            ccAddresses=ccAddress.split(',');
            mail.setCcAddresses(ccAddresses);
        }
        //mail.setSenderDisplayName('Riverbed Business Application');
        //mail.setOrgWideEmailAddressId('0D2Q00000004Chj');
        mail.setOrgWideEmailAddressId(eProp.mc_org_wide_email_Id__c);
        mail.setSubject(subj);
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);      
        mail.setHtmlBody(body);
        if(mail!=null){
            try{
                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
                error = true;
                severe = 'Info';
                errorMsg = 'An Email has been sent to specified users';
            }catch(Exception e){
                error = true;
                severe = 'error';
                errorMsg = e.getMessage();
            }
        }
        toAddresses.clear();
        ccAddresses.clear();
        toAddress='';
        ccAddress='';
        body='';
        return Page.MarginCalculator;         
    }
    public PageReference mCancel(){
        toAddresses.clear();
        ccAddresses.clear();
        toAddress='';
        ccAddress='';
        body='';
        return Page.MarginCalculator;
    }
    private String getBody(List<QuoteLineWrapper> scenarioLines){
        //String body = '<body  style=" background-color:#FFFFFF; bLabel:body; bEditID:b1st1;"><center ><table id="topTable" height="450" width="500" cellpadding="0" cellspacing="0" ><tr valign="top" ><td  style=" vertical-align:middle; height:50; text-align:left; background-color:#FFFFFF; bLabel:header; bEditID:r1st1;"><img id="r1sp1" bLabel="headerImage" border="0" bEditID="r1sp1" src="/servlet/servlet.ImageServer?id=01570000000u6Qa&oid=00DT0000000Iopb"></img></td></tr><tr valign="top" ><td  style=" height:5; background-color:#697ca1; bLabel:accent1; bEditID:r2st1;"></td></tr><tr valign="top" ><td styleInsert="1" height="300"  style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;"><table height="400" width="600" cellpadding="5" border="0" cellspacing="5" ><tr height="50" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r1" aEditID="c1r1" locked="0" ></td></tr><tr height="300" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r2" aEditID="c1r2" locked="0" >';        
        String body = '<body  style=" background-color:#FFFFFF; bLabel:body; bEditID:b1st1;"><center ><table id="topTable" height="450" width="500" cellpadding="0" cellspacing="0" ><tr valign="top" ><td  style=" vertical-align:middle; height:50; text-align:left; background-color:#FFFFFF; bLabel:header; bEditID:r1st1;"><img id="r1sp1" bLabel="headerImage" border="0" bEditID="r1sp1"';
        body += 'src='+eProp.Server_URL__c+'/servlet/servlet.ImageServer?id=01570000000u6Qa&oid='+userInfo.getOrganizationId()+'></img></td></tr><tr valign="top" ><td  style=" height:5; background-color:#697ca1; bLabel:accent1; bEditID:r2st1;"></td></tr><tr valign="top" ><td styleInsert="1" height="300"  style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;"><table height="400" width="600" cellpadding="5" border="0" cellspacing="5" ><tr height="50" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r1" aEditID="c1r1" locked="0" ></td></tr><tr height="300" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r2" aEditID="c1r2" locked="0" >';
        body += '<b><u>Margin Analysis</u> - Scenario Details</b><br>';
        body += '<b>Scenario Information</b><br>';
        body += createTable(scenarioLines);  
        body += '</td></tr><tr height="50" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r3" aEditID="c1r3" locked="0" ></td></tr></table></td></tr><tr valign="top" ><td  style=" height:5; background-color:#697ca1; bLabel:accent2; bEditID:r4st1;"></td></tr><tr valign="top" ><td  style=" vertical-align:top; height:10; text-align:left; background-color:#FFFFFF; bLabel:footer; bEditID:r5st1;"></td></tr><tr valign="top" ><td  style=" height:5; background-color:#ffffff; bLabel:accent3; bEditID:r6st1;"></td></tr></table></center>';     
        return body;
    }
    
    private String createTable(List<QuoteLineWrapper> scenarioLines){
        String table = '<table cellspacing="2" cellpadding="3" border="1" Width="800">' +
                          '<tr>' +
                            '<th>' + 
                              '<b>Product Code</b>' + 
                            '</th>' +
                            '<th>' + 
                              '<b>Quote Quantity</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Scenario Quantity</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Quoted Discount (%)</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Scenario Discount (%)</b>' + 
                            '</th>' +                            
                            '<th>' + 
                              '<b>Extended Price (USD)</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Scenario Extended Price (USD)</b>' + 
                            '</th>' +
                          '</tr>' ;
        for(QuoteLineWrapper qw : scenarioLines){
            table=table+'<tr>' +
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Product_Code__c) + '</td>' + 
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Qty_Ordered__c) + '</td>' +
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Scenario_Quantity__c) + '</td>' +
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Disti_Approved_Discount__c)+'%' + '</td>' + 
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Scenario_Discount__c)+'%' + '</td>' + 
                '<td style="text-align:center">' + '$'+blankIfNull(qw.qLine.D_Ext_Price__c) + '</td>' +       
               '<td style="text-align:center">' +'$'+ Decimal.valueOf(blankIfNull(qw.qLine.Scenario_Extended_Price__c)) + '</td>' +       
                '</tr>';                
        } 
        return table+'</table>';                
    }
    
    private String blankIfNull(Object value){
        if (value == null){
            return '0';
        } else {
            return String.valueOf(value); 
        }   
    }

    //this method clears out the page data and refreshes the page with the quote lines from the quote
    public PageReference reset(){
        scenarioLines.clear();
        for(Quote_Line_Item__c ql : quoteLines){
            Boolean costFlag = false;
            if(ql.Unit_Cost__c == null || ql.Unit_Cost__c == 0){
                costFlag = true;
            }
            QuoteLineWrapper qw = new QuoteLineWrapper(ql,costFlag);
            scenarioLines.add(qw);      
        }       
        return null;
    }

    public PageReference back(){
        family = '';
        subFamily = '';
        keyword = '';
        errorMsg = '';
        error = false;
        return Page.MarginCalculator;
    }

    public PageReference showNonCOGSItems(){
        List<Quote_Line_Item__c> nonCogsQuoteLines = [select id, product_code__c, Qty_Ordered__c, D_Standard_Discount__c, Unit_List_Price__c, D_Unit_Price__c, D_Ext_Price__c,
                        Margin__c, Margin_Percent__c,Scenario_Discount__c, Scenarion_Margin_Percent__c, Scenario_Quantity__c,Scenario_Extended_Price__c, Unit_Cost__c,
                        Disti_Approved_Discount__c, Category__c
                        from Quote_Line_Item__c where quote__c = :quote.Id 
                        and (Unit_Cost__c = 0 or Unit_Cost__c = null) and (Unit_List_Price__c = null or Unit_List_Price__c = 0)];
        //scenarioLines = new List<QuoteLineWrapper>();
        for(Quote_Line_Item__c ql : nonCogsQuoteLines){
            Boolean costFlag = false;
            if(ql.Unit_Cost__c == null || ql.Unit_Cost__c == 0){
                costFlag = true;
            }
            QuoteLineWrapper qw = new QuoteLineWrapper(ql,costFlag);
            scenarioLines.add(qw);      
        }       
        showNonCogs = false;
        return null;
    }
    
    public PageReference hideNonCOGSItems(){
        List<QuoteLineWrapper> removeLines = new List<QuoteLineWrapper>();
        removeLines.addAll(scenarioLines);
        scenarioLines.clear();
        for(QuoteLineWrapper qw : removeLines){
            if(qw.qLine.Unit_List_Price__c != null && qw.qLine.Unit_List_Price__c != 0){//qw.qLine.Unit_Cost__c != null && qw.qLine.Unit_Cost__c != 0 && 
                scenarioLines.add(qw);
            }
        }
        showNonCogs = true;
        return null;
    }
    
    
    public PageReference calculate(){
        qs.reset();
        for(QuoteLineWrapper qw : scenarioLines){
            String cat = qw.qLine.Category__c == null ? '' : qw.qLine.Category__c;
            Decimal scQty = qw.qLine.Scenario_Quantity__c == null ? qw.qLine.Qty_Ordered__c : qw.qLine.Scenario_Quantity__c;
            Decimal scDisc = qw.qLine.Scenario_Discount__c == null ? (qw.qLine.Disti_Approved_Discount__c == null ? 0 : qw.qLine.Disti_Approved_Discount__c) : qw.qLine.Scenario_Discount__c;
            System.debug('Scenario Discount = ' + scDisc);
            Decimal unitCost = qw.qLine.Unit_Cost__c == null ? 0 : qw.qLine.Unit_Cost__c;
            Decimal unitPrice = qw.qLine.Unit_List_Price__c == null ? 0 : qw.qLine.Unit_List_Price__c;
            if(unitPrice == 0){
                qw.qLine.Scenario_Extended_Price__c = 0;
            }else{
                qw.qLine.Scenario_Extended_Price__c = scQty * unitPrice * (1-scDisc/100);
                qw.qLine.Scenario_Extended_Price__c = qw.qLine.Scenario_Extended_Price__c.setscale(2);
//              qw.qLine.Scenarion_Margin_Percent__c = (qw.qLine.Scenario_Extended_Price__c - (scQty * unitCost))*100/qw.qLine.Scenario_Extended_Price__c;
                qw.qLine.Scenarion_Margin_Percent__c = 100 * (1 - ((scQty * unitCost)/qw.qLine.Scenario_Extended_Price__c));
            }
            if(cat.equals('A') || cat.equals('C') || cat.equals('D')){
                qs.productCost += (scQty * unitCost);
                qs.prodExtPrice += qw.qLine.Scenario_Extended_Price__c;
                qs.prodExtListPrice += (scQty * unitPrice);
            }else if (cat.equals('B')){
                qs.supportCost += (scQty * unitCost);
                qs.supportExtPrice += qw.qLine.Scenario_Extended_Price__c;
                qs.supportExtListPrice += (scQty * unitPrice);
            }else if(cat.equals('E')){
                qs.trngCost += (scQty * unitCost);
                qs.trngExtPrice += qw.qLine.Scenario_Extended_Price__c;
                qs.trngExtListPrice += (scQty * unitPrice);
            }
            qs.totalCost += (scQty * unitCost);
            qs.totalExtPrice += qw.qLine.Scenario_Extended_Price__c;
            qs.totalExtListPrice += (scQty * unitPrice);
        }   
        if(qs.prodExtPrice != 0){
            quote.Scenario_Margin_P_Product__c = (1 - (qs.productCost.divide(qs.prodExtPrice,2)))*100;
            //quote.Scenario_Margin_P_Product__c = 100 * ((prodExtPrice - productCost)/prodExtPrice);
        }
        if(qs.supportExtPrice != 0){
            quote.Scenario_Margin_P_Support__c = (1 - (qs.supportCost.divide(qs.supportExtPrice,2))) * 100;
        }   
        if(qs.trngExtPrice != 0){
            quote.Scenario_Margin_P_Training__c = 100 *(1 - (qs.trngCost.divide(qs.trngExtPrice,2)));
        }   
        if(qs.totalExtPrice != 0){
            quote.Scenario_Margin_P_Total__c = 100 * (1 - (qs.totalCost.divide(qs.totalExtPrice,2)));
        }
        if(qs.prodExtListPrice != 0){
            qs.scenarioProdDiscount = 100 * (qs.prodExtListPrice - qs.prodExtPrice).divide(qs.prodExtListPrice,2);
        }
        if(qs.supportExtListPrice != 0){
            qs.scenarioSupportDiscount = 100 * (qs.supportExtListPrice - qs.supportExtPrice).divide(qs.supportExtListPrice,2);
        }
        if(qs.trngExtListPrice != 0){
            qs.scenariotrngDiscount = 100 * (qs.trngExtListPrice - qs.trngExtPrice).divide(qs.trngExtListPrice,2);
        }
        if(qs.totalExtListPrice != 0){
            qs.scenarioTotalDiscount = 100 * (qs.totalExtListPrice - qs.totalExtPrice).divide(qs.totalExtListPrice,2);
        }
        return null;
    }
    
    //wrapper class for products selection
    public class ProductWrapper  {
 
        public Boolean checked{ get; set; }
        public Product2 prod{ get; set;}
     
        public ProductWrapper (){
            prod = new Product2();
            checked = false;
        }
     
        public ProductWrapper (Product2 p){
            prod = p;
            checked = false;
        }
   }
   
   //wrapper class for quote lines
    public class QuoteLineWrapper  {
 
        public Boolean checked{ get; set; }
        public Quote_Line_Item__c qLine{ get; set;}
        public Boolean noCost {get; set;}
     
        public QuoteLineWrapper (Quote_Line_Item__c q, Boolean flag){
            qLine = q;
            checked = false;
            noCost = flag;
        }
   }
   
   //class holding categorized discounts and prices
   public class QuoteSummary{
        public Decimal productCost {get; set;}
        public Decimal trngCost {get;  set;}
        public Decimal supportCost {get;  set;}
        public Decimal totalExtPrice {get;  set;}
        public Decimal totalCost {get;  set;}
        public Decimal prodExtPrice {get;  set;}
        public Decimal supportExtPrice {get;  set;}
        public Decimal trngExtPrice {get;  set;}
        public Decimal prodExtListPrice {get;  set;}
        public Decimal supportExtListPrice {get;  set;}
        public Decimal trngExtListPrice {get;  set;}
        public Decimal totalExtListPrice {get;  set;}
        //scenario discount for summary
        public Decimal scenarioProdDiscount {get; set;}
        public Decimal scenarioSupportDiscount {get;  set;}
        public Decimal scenarioTrngDiscount {get;  set;}
        public Decimal scenarioTotalDiscount {get;  set;}
        //Quote Weighted Averages - declared variables for these to handle null on page
        public Decimal quoteProdDiscount {get;  set;}
        public Decimal quoteSupportDiscount {get; set;}
        public Decimal quoteTrngDiscount {get; set;}
        public Decimal quoteTotalAvgDiscount {get; set;}
        
        public QuoteSummary(){
            productCost = 0;
            trngCost = 0;
            supportCost = 0;
            totalExtPrice = 0;
            totalCost = 0;
            prodExtPrice = 0;
            supportExtPrice = 0;
            trngExtPrice = 0;
            prodExtListPrice = 0;
            supportExtListPrice = 0;
            trngExtListPrice = 0;
            totalExtListPrice = 0;
        }
        
        public void reset(){
            productCost = 0;
            trngCost = 0;
            supportCost = 0;
            totalExtPrice = 0;
            totalCost = 0;
            prodExtPrice = 0;
            supportExtPrice = 0;
            trngExtPrice = 0;
            prodExtListPrice = 0;
            supportExtListPrice = 0;
            trngExtListPrice = 0;
            totalExtListPrice = 0;
            
        }
   }   
}