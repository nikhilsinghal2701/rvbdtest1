public with sharing class GAPTabController {

	public Boolean show{get;private set;}
	public List<Account> accList {get; private set;}
	public String splitMatrixLink{get;set;}
	public String roeLink{get;set;}
	public String gapLibraryLink{get;set;}
	public List<SelectOption> statusList=new List<SelectOption>();
	public String status{get;set;}
	private GAPPortalSettings__c gapProperty;
	public GAPTabController(){
		gapProperty=GAPPortalSettings__c.getValues('GAPPortalControl');
		if(UserInfo.getProfileId()=='00e50000000uVEPAA2'||UserInfo.getProfileId()=='00eA0000000xg7kIAA'||UserInfo.getProfileId()=='00e70000000wZ1SAAU'
		||UserInfo.getProfileId()=='00e400000017J8DAAU'||UserInfo.getProfileId()=='00e400000017J8EAAU'||UserInfo.getProfileId()=='00e50000000uVEtAAM'
		||UserInfo.getProfileId()=='00e50000000uVFwAAM'||UserInfo.getProfileId()=='00e70000000x186AAA'||UserInfo.getProfileId()=='00eA0000000xg7fIAA'
		||UserInfo.getProfileId()=='00e400000017J8FAAU'||UserInfo.getProfileId()=='00e30000000bqt9AAA'){
			show=true;
			//system.debug('within if condition');
		}else{
			//system.debug('Profile Id:'+userInfo.getProfileId());
			//system.debug('within else condition');
			show=false;
		}
		roeLink=gapProperty.RulesofEngagements__c;
		splitMatrixLink=gapProperty.Split_Matrix__c;
		gapLibraryLink=gapProperty.GAP_Library__c;
		accList = [select id, name, Global_Ultimate_Account_Name__c, Global_Account_Manager__c, Global_Account_Manager__r.Name, 
					Global_Account_Alerts__c, WW_HQ__c, WW_HQ__r.Name,status__c,Report_Id__c,Global_Contract__c 
					from Account 
					where global_ultimate__c = true and 
					account_coverage_program__c = 'GAP' ORDER BY Name];
		 			
	}
}