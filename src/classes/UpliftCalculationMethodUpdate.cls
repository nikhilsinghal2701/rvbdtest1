public with sharing class UpliftCalculationMethodUpdate {
	
	public String answer{set;get;}
	
	public List<SelectOption> getMethods() {
       List<SelectOption> options = new List<SelectOption>(); 
       options.add(new SelectOption('true','net')); 
       options.add(new SelectOption('false','list')); 
       return options; 
   	}
   	
   	
   	public PageReference doSave() { 
   	Callouts sc = new Callouts();	
   	sc.SendRequest();	
   	list<UpliftCalculationMethod__c> UC=[select id from UpliftCalculationMethod__c];
   	delete UC;
   	UpliftCalculationMethod__c UCN;
   	UpliftCalculationMethod__c UCL;
   	list<UpliftCalculationMethod__c> UClist = new list<UpliftCalculationMethod__c>();
   	if(answer.equalsIgnoreCase('true')){
   		UCN = new UpliftCalculationMethod__c(name='net',Active__c=true);
   		UCL = new UpliftCalculationMethod__c(name='list',Active__c=false);
   	}else{
   		UCN = new UpliftCalculationMethod__c(name='net',Active__c=false);
   		UCL = new UpliftCalculationMethod__c(name='list',Active__c=true);
   	}
	UClist.add(UCN);
	UClist.add(UCL);
   	if(!UClist.IsEmpty())try {
   	insert UClist;
   	} catch(Exception e) {
    System.debug('==Exception Message==='+e.getMessage());
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error'+e.getMessage()));
	}
	
	 return new PageReference('/home/home.jsp');
	} 

}