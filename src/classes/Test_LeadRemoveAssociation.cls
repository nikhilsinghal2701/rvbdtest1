@isTest(SeeAllData=true)
public with sharing class Test_LeadRemoveAssociation {
    
    static testmethod void testLeadRemoveAssociation(){
        
        //RecordType genLead = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
        Id genLead ='012300000000V6iAAE';
        //RecordType p2pLead = [select id from RecordType where name = 'Lead Passed To Partner' and sObjectType = 'Lead'];
        Id p2pLead='012700000005B3vAAE';
        User eloquaUser = [select id, name from User where name = 'Eloqua Administrator'];
        Lead newLead;
        Account acc;
        Contact con;
        test.startTest();
        System.runAs(eloquaUser){
            newLead = new Lead(firstname = 'Tony', lastname = 'Montana', company = 'new company', 
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead );
            insert newLead;
            
        }
        acc = new Account( Name ='Test Account',Industry= 'Software');
        insert acc;
        con = new Contact(firstname = 'Steve', lastname = 'Jobs',accountId = acc.Id );
        insert con;
        //Lead l = [select id, ownerid,Lead_Score__c from Lead where id = :newLead.id];
        //l.recordTypeid = p2pLead.id ;
        newLead.recordTypeid = p2pLead;
        //update l;
        update newLead;        
        //l.recordTypeid = genLead.id;
        newLead.recordTypeid = genLead;
        //update l;
        update newLead;        
        //l.AutoConvertFlag__c = true;
        //l.TestContactId__c = con.Id;
        //update l;    
        
        Test.stopTest();
    }
}