/*************************************************************************************************************
 Name:DiscountManagementHelperTest
 Author: Santoshi Mishra
 Purpose: This is test class for the class DiscountManagementHelper.
 Created Date : Oct 2011
*************************************************************************************************************/
@isTest(seeAllData=true)
private class DiscountManagementHelperTest {  

  
    static testMethod void InsertDiscountTableChildRecordsTest() // This is the test method for the method InsertDiscountTableChildRecords
    {
        test.starttest();
        DiscountApprovalTestHlp.insertCategoryMasterData(); //This test data insertion also covers the Trigger on Category Master.
        DiscountCategory__c dis = new DiscountCategory__c(Name='Discount 1');
        insert(dis);
        //List<Discount_Category_Detail__c> childList = [select Id,Discount_Table__c,Category__c,Category__r.Name from Discount_Category_Detail__c where Discount_Table__c = :dis.Id ];
        //List<Category_Master__c> catList = [select Id from Category_Master__c];
        //System.assertequals(catList.size(),childList.size() ); // Verifies that number of record inserted is same as number of category present.
        test.stoptest();
        
    }  
    
    static testMethod void InsertDistiUpliftChildRecordsTest() // This is the test method for the method InsertDistiUpliftChildRecords
    {
        test.starttest();
        DiscountApprovalTestHlp.insertCategoryMasterData(); //This test data insertion also covers the Trigger on Category Master.
        Disti_Uplift__c dis = new Disti_Uplift__c(Name='Disti 1');
        insert(dis);
        //List<Disti_Uplift_Detail__c> childList = [select Id,Disti_Uplift__c from Disti_Uplift_Detail__c where Disti_Uplift__c = :dis.Id ];
        //List<Category_Master__c> catList = [select Id from Category_Master__c];
       //System.assertequals(catList.size(),childList.size()); // Verifies that number of record inserted is same as number of category present.
        test.stoptest();
        
    }
    
     static testMethod void InsertsDiscountDetailInQuoteTest() // This is the test method for the method InsertsDiscountDetailInQuote
    {
       test.starttest();
        List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        Quote__C q = DiscountApprovalTestHlp.createQuote(opp.Id);
        //detailList=[select Id ,Opportunity__c,IsOpp__c from Discount_Detail__c where Opportunity__c = :opp.Id];
        //List<Discount_Detail__c> detailQuoteList = [select Id ,Quote__c,IsOpp__c from Discount_Detail__c where Quote__C = :q.Id];
        //System.debug('quote--'+detailQuoteList.size()+'opty--'+detailList.size());
        //System.assertequals(detailQuoteList.size(),detailList.size()); // Verifies that number of quote detail records is equal to number of opty detail records.
        //System.assertequals(detailQuoteList[0].IsOpp__c,false);
        test.stoptest();
        
    }
    
     static testMethod void UpdatesDiscountDetailInQuoteTest() // This is the test method for the method UpdatesDiscountDetailInQuote
    {
       test.starttest();
        List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        Quote__C q = DiscountApprovalTestHlp.createQuote(opp.Id);
        //List<Discount_Detail__c> detailQuoteList = [select Id ,Category_Master__r.Name,Quote__c,IsOpp__c,special_Discount__C from Discount_Detail__c where Quote__C = :q.Id and Category_Master__r.Name='A'];
        // Verifies that the special discount is updated to the highest one in this case it should be 75.
        //System.assertequals(detailQuoteList[0].special_Discount__C,75);
        test.stoptest();
    }
    static testMethod void categoryMasterTrigger()
    {
        test.starttest();
         List<Category_Master__c> catList =new List<Category_Master__c>();
         Category_Master__c c = new Category_Master__c(Name ='PO',Active__C=true);
          Category_Master__c c1 = new Category_Master__c(Name ='POB',Active__C=true);
          catList.add(c);
          catlist.add(c1);
          insert catList;
         Product2 p = new Product2(Category__C=catList[0].Name,Name='ABC', ProductCode='test');
        
        
        insert(p);
        String result='';
        catList[0].Active__C=true;
        catList[1].Active__C=false;
         update (CatList);
         System.assert(CatList[0].Active__C,true);
          System.assert(CatList[0].Active__C,false);
        
         test.stoptest();
    }
  /** static testmethod void UpdatesStdDiscountInDisDetailtest1()
    {
         List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
       
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        
     //   List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
    //    Quote__C q = DiscountApprovalTestHlp.createQuote(opp.Id);
        List<Quote_Line_Item__C> qlist = new List<Quote_Line_Item__C>();
    Quote__c q = new Quote__c (Opportunity__c=opp.Id, Name='test', RSM__c=DiscountApprovalTestHlp.getRSM(), Discount_Code__c='Competitive Pricing', Discount_Reason__c='this',Discount_Product__c=9);
        insert q;
        Product2 prod=[SELECT Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE'];
        //Test.startTest();  
        Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=q.Id,Qty_Ordered__c=1,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=0,Unit_List_Price__c=0);
        //added by santoshi on 10/25/2011 to add multiple quote line items for testing purpose
        Quote_Line_Item__c qli1 = new Quote_Line_Item__c(Quote__c=q.Id,Qty_Ordered__c=1,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25);
         
        qlist.add(qli);
        qlist.add(qli1);
        
        insert qlist;   
        
        DiscountApproval.isTest = true;
        
        Discount_Detail__c oppDetail;
        
         oppDetail=[Select d.Uplift__c, d.Special_Discount__c, d.Quote__c, d.Opportunity__c, d.IsOpp__c, d.Discount__c,
                                             d.Date_Approved__c, d.Category_Master__c,d.Category_Master__r.Name,createddate From Discount_Detail__c d where d.Opportunity__c =:opp.Id
                                              and d.Category_Master__r.Name='A' and d.isopp__C=true order by createddate desc limit 1 ];
    
        
            oppDetail.discount__c=8;    
        
        update(oppDetail);
        

        q.Discount_Status__C='Approved';
        update q;
        // Two times updating is done as the code checks for the trigger.old values.
        q.Discount_Status__C='Approved';
        update q;
            Test.startTest();

            
            Discount_Detail__c qDetail;
         qDetail=[Select d.Uplift__c, d.Special_Discount__c, d.Quote__c, d.Opportunity__c, d.IsOpp__c, d.Discount__c,
                                             d.Date_Approved__c, d.Category_Master__c,d.Category_Master__r.Name,createddate From Discount_Detail__c d where
                                      d.Quote__c =:q.Id and d.Category_Master__r.Name='A' and d.isopp__C=false order by createddate desc limit 1 ];
        
        
        
    //  system.debug('gg-----------'+qDetail +'gg================='+oppDetail );
        //System.assertEquals(qDetail.discount__C, oppDetail.discount__C); // Testing whether quote discount detail is updated by opty discount detail or not.
        test.stoptest();
    }**/
    
    testMethod static void UpdatesStdDiscountInDisDetailtest2() {
         List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        List<Discount_Detail__c> detailList1= [select Discount__c, Uplift__c,Special_Discount__c, IsOpp__c,Opportunity__c, Category_Master__c,Category_Master__r.Name from Discount_Detail__c where Opportunity__c = :opp.Id and IsOpp__c = true];
        delete(detailList1);
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        List<Quote_Line_Item__C> qlist = new List<Quote_Line_Item__C>();
    Quote__c quote = new Quote__c (Opportunity__c=opp.Id, Name='test', RSM__c=DiscountApprovalTestHlp.getRSM(), Discount_Code__c='Competitive Pricing', Discount_Reason__c='this');
        insert quote;
        Product2 prod=[SELECT Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE'];
        
        Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=quote.Id,Non_Standard_Discount__c=2,Qty_Ordered__c=1,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency());
        //added by santoshi on 10/25/2011 to add multiple quote line items for testing purpose
        
        //insert qli; 
        
        DiscountApproval.isTest = true;
        detailList1= [select Discount__c, Uplift__c,Special_Discount__c, IsOpp__c,Opportunity__c, Category_Master__c,Category_Master__r.Name from Discount_Detail__c where Opportunity__c = :opp.Id and IsOpp__c = true and Category_Master__r.Name='A'];
        detailList1[0].Special_Discount__c=3;
        update(detailList1);
        System.debug(LoggingLevel.INFO,'quote updated..'+qli);
                test.starttest();

        quote.Discount_Status__C='Approved';
        quote.Promotional__c = true;
        quote.Promotion_Expired__c =true;
        update quote;
        Test.stopTest();
    }
     static testMethod void configureControllerTest() {
        test.starttest();
        Opportunity Obj = DiscountApprovalTestHlp.createopp();        
        test.setCurrentPage(new PageReference('/apex/Configurer_VF?opnetAccReq=false&id=' + Obj.Id));
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(obj);
        ConfigureController CLS = new ConfigureController(VFpage_Extn);
          ConfigureController CLS1 = new ConfigureController();
          cls.callWebService();
        obj.StageName = '6 - Order Accepted';
        update obj;        
        
        test.setCurrentPage(new PageReference('/apex/Configurer_VF?opnetAccReq=false&id=' + Obj.Id));
        VFpage_Extn = new ApexPages.StandardController(obj);
        CLS = new ConfigureController(VFpage_Extn);
        cls.callWebService();
        
         test.stoptest();
     }
     
      static testMethod void marginSummaryTest() {
        test.starttest();
        Quote__C Obj = DiscountApprovalTestHlp.createquote(null);
        //update obj;
        PageReference VFpage = Page.MarginSummaryPage;
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(obj);
        MarginSummaryPageExtension CLS = new MarginSummaryPageExtension(VFpage_Extn);
         
        test.stoptest();
        
        
     }
     
     static testMethod void DiscountDetailQuoteClassTest() {
        List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
       
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        Quote__C obj = DiscountApprovalTestHlp.createQuote(opp.Id);
        //update obj;
        test.startTest();
        PageReference VFpage = Page.DiscountDetailonQuotePage;
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(obj);
        DiscountDetailQuoteClass CLS = new DiscountDetailQuoteClass(VFpage_Extn);
        cls.createQuoteLineItemCategories();
        ArchiveMigrationClass a = new ArchiveMigrationClass(opp.Id); 
        Database.executebatch(a);
        PageReference VFpage1 = Page.DiscountDetailonQuotePage;
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn1 = new ApexPages.StandardController(obj);
        DiscountDetailQuoteClass CLS1 = new DiscountDetailQuoteClass(VFpage_Extn1);
        test.stopTest();
        
     }
     
      static testMethod void UpdateQuote() {
        Opportunity  opp = DiscountApprovalTestHlp.createOpp();
        Quote__c q = new Quote__c (Opportunity__c=opp.id, Name='test', Discount_Code__c='Competitive Pricing', Discount_Reason__c='this',Discount_Product__c=9, Quote_Line_Sync_Complete__c = true, Segment__c='RVBD Operating Unit - US');
        insert q;
        list<UpliftCalculationMethod__c> UCM =[select name,active__c from UpliftCalculationMethod__c where active__c = true]; //added by nikhil on 7/24/2015
         q.Quote_Status__c='Priced Quote';
         if(!ucm.IsEmpty()){
         if(ucm[0].name.equalsIgnoreCase('net')){
         q.Uplift_Calculation__c ='list';
         }else{
         q.Uplift_Calculation__c ='net';
         }
         }
        test.starttest();
           update q;
        system.debug('post update'+q);
        test.stoptest();
     }
}