@isTest
private class TestCreateRQIAQICtrl {
	
	static testMethod void testCreateRQI() {
		CreateRQIAQICtrl CRAC = new CreateRQIAQICtrl();
        TestAQIRQIUtils testTools = new TestAQIRQIUtils();
        User testUser = testTools.createMember();
        
        Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
    
        
        Case needOneCase = [Select Id,Case_Owner__c from Case LIMIT 1];        
   		needOneCase.Case_Owner__c = testUser.Id;
                
        //Test Case Creation
        ApexPages.currentPage().getParameters().put('id',needOneCase.Id);
    	ApexPages.currentPage().getParameters().put('authorId',testUser.Id);
    	ApexPages.currentPage().getParameters().put('casesorarticles','retCases');
    	CRAC.createAudit(); 
        
        //Test case creation on audited Case
    	ApexPages.currentPage().getParameters().put('id',needOneCase.Id);
    	ApexPages.currentPage().getParameters().put('authorId',testUser.Id);
    	ApexPages.currentPage().getParameters().put('casesorarticles','retCases');
    	CRAC.createAudit();
    	Test.stopTest();   
	}
	
	static testMethod void testRQICreateKnownCase() {
		CreateRQIAQICtrl CRAC = new CreateRQIAQICtrl();
        TestAQIRQIUtils testTools = new TestAQIRQIUtils();
        User testUser = testTools.createMember();
        
        Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
    
        
        Case needOneCase = [Select Id,Case_Owner__c from Case LIMIT 1];        
   		needOneCase.Case_Owner__c = testUser.Id;
   		
    	Case caseObj = [SELECT Id,KCS_Known_vs_New__c,Case_Owner__c FROM Case WHERE Id NOT IN (Select Case__c FROM RQI_Audit__c)];
    	InQuira_Article_Info__c article = testTools.createArticle();
    	article.Article_Created_Date__c = DateTime.valueOf('2013-10-05 20:03:20');
    	insert article;
    	
    	// create Case Article Link
    	InQuira_Case_Info__c caseInfo = testTools.createCaseInfo();
    	caseInfo.Document_ID__c = article.Document_ID__c;
    	caseInfo.Related_Case__c = caseObj.Id;
    	caseInfo.Related_InQuira_Article__c = article.Id;
    	insert caseInfo;
    	update caseObj;
    	
    	 caseObj = [
            SELECT Id,KCS_Known_vs_New__c,Case_Owner__c
            FROM Case
            WHERE KCS_Known_vs_New__c='Known'
            AND Id NOT IN (SELECT Case__c FROM RQI_Audit__c)
            LIMIT 1
        ];
	    	   
        ApexPages.currentPage().getParameters().put('id',caseObj.Id);
    	ApexPages.currentPage().getParameters().put('authorId',caseObj.Case_Owner__c);
    	ApexPages.currentPage().getParameters().put('casesorarticles','retCases');
    	CRAC.createAudit();
    	Test.stopTest();
            
	}
	
	
	static testMethod void testRQICreateNewCase() {
		CreateRQIAQICtrl CRAC = new CreateRQIAQICtrl();
        TestAQIRQIUtils testTools = new TestAQIRQIUtils();
        User testUser = testTools.createMember();
        
        Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
    
        
        Case needOneCase = [Select Id,Case_Owner__c from Case LIMIT 1];        
   		needOneCase.Case_Owner__c = testUser.Id;
   		
   		Case caseObj = [SELECT Id,KCS_Known_vs_New__c,Case_Owner__c FROM Case WHERE Id NOT IN (Select Case__c FROM RQI_Audit__c)];
    	InQuira_Article_Info__c article = testTools.createArticle();
    	article.Article_Created_Date__c = DateTime.Now();
    	insert article;
    	
    	// create Case Article Link
    	InQuira_Case_Info__c caseInfo = testTools.createCaseInfo();
    	caseInfo.Document_ID__c = article.Document_ID__c;
    	caseInfo.Related_Case__c = caseObj.Id;
    	caseInfo.Related_InQuira_Article__c = article.Id;
    	insert caseInfo;
    	update caseObj;
    	
		caseObj = [
	        SELECT Id,KCS_Known_vs_New__c,Case_Owner__c
	        FROM Case
	        WHERE KCS_Known_vs_New__c='New'
	        AND Id NOT IN (SELECT Case__c FROM RQI_Audit__c)
	        LIMIT 1
    	];
    	
        ApexPages.currentPage().getParameters().put('id',caseObj.Id);
    	ApexPages.currentPage().getParameters().put('authorId',caseObj.Case_Owner__c);
    	ApexPages.currentPage().getParameters().put('casesorarticles','retCases');
    	CRAC.createAudit();
    	
    	
    	// test Create AQI
    	List<InQuira_Article_Info__c> kavs = [SELECT Title__c FROM InQuira_Article_Info__c LIMIT 1];
    	if(!kavs.isEmpty()){
	    	ApexPages.currentPage().getParameters().put('id',kavs[0].id);
	    	ApexPages.currentPage().getParameters().put('authorId',testUser.Id);
	    	ApexPages.currentPage().getParameters().put('casesorarticles','retArticles');
	    	CRAC.createAudit(); 
    	}
    	 	
    	//test for neither case or article
    	ApexPages.currentPage().getParameters().put('casesorarticles','');
    	CRAC.createAudit(); 
   		Test.stopTest();
	}
	
	
}