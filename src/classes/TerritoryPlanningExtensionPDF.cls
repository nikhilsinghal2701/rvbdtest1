/*******************************************************************************************
// Class Name: TerritoryPlanningExtension
//
// Description: This class is the controller class for the TerritoryPlanPDF VF page.
//
// Created By: Nikhil Singhal (Perficient) - 8/4/2014
*******************************************************************************************/


public with sharing class TerritoryPlanningExtensionPDF {
    public  Territory__c  Tr {get; set;}
    public List<Territory_Plan_Detail__c> TPDList {get;set;}
    public Territory_Plan_Detail__c Mtier1{get; set;}
    public Territory_Plan_Detail__c Mtier2{get; set;}
    public Territory_Plan_Detail__c Mtier3{get; set;}
    public Territory_Plan_Detail__c Atier1{get; set;}
    public Territory_Plan_Detail__c Atier2{get; set;}
    public Territory_Plan_Detail__c Atier3{get; set;}
    public Territory_Plan_Detail__c ctier1{get; set;}
    public Territory_Plan_Detail__c ctier2{get; set;}
    public Territory_Plan_Detail__c ctier3{get; set;}
    public  String Tpowner {get; set;}
     public List<Attachment> attachments {get; set;}
 	public Attachment attachment1 {get; set;}
 	public List<Territory_Plan__History> TPH  {get;set;}
 	public List<FieldLabel> Wrows {get; set;}
	String type='Territory_Plan__c';
	public List<Attachment> Dispattachments {get; set;}
	
	
//controls how many quote line items are displayed on page 1
    private static Integer FIRST_BREAK = 20;
    //controls how many quote line items are displayed on subsequent pages
    private static Integer SUBSEQ_BREAKS = 20;
	public List<Territory_Team__c[]> pageBrokenTPT {get; private set; }
	
	
    private final Territory_Plan__c Tp;
    public Territory_Plan__c Tpv{get; set;}

    public List<Territory_Planning__c> TpList {get;set;}

    public List<Territory_Team__c> TList {get;set;}

    
    public TerritoryPlanningExtensionPDF(ApexPages.StandardController controller) { 
                if (!Test.isRunningTest())  {
                controller.addFields(new String[]{'Territory__c'});
                }
                Wrows=New List<FieldLabel>();
                Tp = (Territory_Plan__c)controller.getRecord();
                if(ApexPages.currentPage().getParameters().get('tid') != null){
               Tp.Territory__c=ApexPages.currentPage().getParameters().get('tid');
               }
                Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
				Schema.SObjectType leadSchema = schemaMap.get(type);
				Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
               Dispattachments= New List<Attachment>(); 
               
               if(Tp.id != null){
               Tpv=[SELECT CreatedById, CreatedDate, CurrencyIsoCode, IsDeleted, Fiscal_Year__c, LastModifiedById, 
               LastModifiedDate, OwnerId, Id, Status__c, SystemModstamp, TSE__c, Territory__c, Territory_Competitors__c, 
               Name, Territory_Top_Customers__c, Tier_1_of_Accounts__c, Tier_2_of_Accounts__c, 
               Tier_3_of_Accounts__c,owner.name FROM Territory_Plan__c where id=:tp.id];
               
                attachment1 = new Attachment();
		        attachments = [ SELECT Id, Name,CreatedBy.name, BodyLength, CreatedById, LastModifiedDate ,CreatedDate
		                        FROM Attachment 
		                        WHERE parentId =: Tp.Id ORDER BY CreatedDate DESC];
              for(Attachment a: attachments){
              	if(a.name.endsWith('.jpg'))
              	Dispattachments.add(a);
              }
               
               
               
               TPH =[ SELECT Field, CreatedById,CreatedBy.name, CreatedDate, IsDeleted, Id, NewValue, OldValue, ParentId FROM Territory_Plan__History where parentid=: tp.Id];
                 for(Territory_Plan__History a : TPH){
                 	if(a.Field == 'created'){
                 	Wrows.add(new FieldLabel('created',a));
                 	}else if(fieldMap.containskey(a.field)){
                 	Wrows.add(new FieldLabel(fieldMap.get(a.field).getDescribe().getLabel(),a));
                 	}else{
                 	Wrows.add(new FieldLabel(a.field,a));
                 	}
                 }
               
               
               }
               
                if(Tp.id != null){
                	Tpowner=Tpv.owner.name;
                }else{
                	Tpowner=UserInfo.getName();
                 }
                 
                 
                 
                Tr=[Select ownerid,name,CreatedDate,SystemModstamp,TSE__r.name from Territory__c where id=:Tp.Territory__c];
               
               if(Tp.id != null){
                TpList =[SELECT Assessment_Area__c,Score__c, Id, SystemModstamp, Territory_Plan__c, Name FROM Territory_Planning__c where Territory_Plan__c =: Tp.Id];
                TList =[Select id,User__c,Role__c,Territory_Plan__c,Access__c,CreatedDate,User__r.name from Territory_Team__c where Territory_Plan__c =: Tp.Id];
               	try{
                Mtier1 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier1' and Type__c='Marketing / Pipeline'];
                }catch(exception e){
   				}
               	try{
                Mtier2 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier2' and Type__c='Marketing / Pipeline'];
                }catch(exception e){
   				}
                try{
                Mtier3 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier3' and Type__c='Marketing / Pipeline'];
                }catch(exception e){
   				}
                try{
                Atier1 = [SELECT Action_Items__c, Existing_Accounts__c, Prospect_Accounts__c, Suspect_Accounts__c,  Expected_Revenue__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier1' and Type__c='Account Coverage'];
                }catch(exception e){
   				}
                try{
                Atier2 = [SELECT Action_Items__c, Existing_Accounts__c, Prospect_Accounts__c, Suspect_Accounts__c,  Expected_Revenue__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier2' and Type__c='Account Coverage'];
                }catch(exception e){
   				}
                try{
                Atier3 = [SELECT Action_Items__c, Existing_Accounts__c, Prospect_Accounts__c, Suspect_Accounts__c,  Expected_Revenue__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier3' and Type__c='Account Coverage'];
               	}catch(exception e){
   				}
   				 try{
                ctier1 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier1' and Type__c='Channel/Partner'];
                }catch(exception e){
   				}
                try{
                ctier2 = [SELECT Action_Items__c,Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier2' and Type__c='Channel/Partner'];
                }catch(exception e){
   				}
                try{
                ctier3 = [SELECT Action_Items__c,Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier3' and Type__c='Channel/Partner'];
               	}catch(exception e){
   				}
               
               }
       
               
               prepareteamForPrinting();

    }
    
  
  // WrapperClass For  history field labels 
     
		public without sharing class FieldLabel{
		public String Name      {get;set;}
		public Territory_Plan__History APHs {get;set;}
		public FieldLabel(String c , Territory_Plan__History s){
		this.Name=c;
		this.APHs=s;
		}
} 

//splits the lines into an approximate number of rows that can be 
    //displayed per page
 private void prepareteamForPrinting()
    {
         pageBrokenTPT = new List<Territory_Team__c[]>();
        
        Territory_Team__c[] pageOfteam = new Territory_Team__c[]{};
        Integer counter = 0;
        
        boolean firstBreakFound = false;
        for(Territory_Team__c q : TList)
        {
            if(!firstBreakFound)
            {
                if(counter < FIRST_BREAK)
                {
                    pageOfteam.add(q);
                    counter++;  
                }
                if(counter == FIRST_BREAK)
                {
                    firstBreakFound = true;
                    counter = 0;
                    pageBrokenTPT.add(pageOfteam);
                    pageOfteam = new Territory_Team__c[]{};
                }   
            }
            else
            {
                if(counter < SUBSEQ_BREAKS)
                {
                    pageOfteam.add(q);
                    counter++;
                }
                if(counter == SUBSEQ_BREAKS)
                {
                    counter = 0;
                    pageBrokenTPT.add(pageOfteam);
                    pageOfteam = new Territory_Team__c[]{};
                }   
            }
        }
        //if we have finished looping and have some quotes left lets assign them
        if(!pageOfteam.isEmpty())
            pageBrokenTPT.add(pageOfteam);
    }





    
}