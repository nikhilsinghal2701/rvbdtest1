/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This class is the controller class for OptyDataMigration
Created Date : Nov 2011
*******************************************************************************************************/


global  class OptyDataMigrationClass implements Database.Batchable <Sobject> {
        global Final String query;
      Boolean Test;
    global OptyDataMigrationClass()
        {
           // Commneted to free up the look up fields By Pramod/Ankita
           // query = 'Select o.Approved_Quote_A__c,o.Approved_Quote_B__c,o.Approved_Quote_C__c,o.Approved_Quote_E__c,o.UpliftSupport__c,o.Id, o.UpliftSupport_3__c, o.UpliftSupport_2__c, o.UpliftSupport_1__c, o.UpliftSpares__c, o.UpliftServices__c, o.UpliftProducts__c, o.UpliftOthers__c, o.UpliftDemos__c, o.Discount_Demos__c, o.DiscountSupport__c, o.DiscountSupport_3__c, o.DiscountSupport_2__c, o.DiscountSupport_1__c, o.DiscountSpare__c, o.DiscountService__c, o.DiscountProduct__c, o.DiscountOthers__c, o.CreatedDate, o.Approved_Discount_F__c, o.Approved_Discount_E__c, o.Approved_Discount_D__c, o.Approved_Discount_C__c, o.Approved_Discount_B__c, o.Approved_Discount_A__c From Opportunity o where o.createddate >= 2009-04-01T00:00:00.0000Z and o.createddate < 2012-02-25T02:00:00.0000Z';
            
            query = 'Select o.UpliftSupport__c,o.Id, o.UpliftSupport_3__c, o.UpliftSupport_2__c, o.UpliftSupport_1__c, o.UpliftSpares__c, o.UpliftServices__c, o.UpliftProducts__c, o.UpliftOthers__c, o.UpliftDemos__c, o.Discount_Demos__c, o.DiscountSupport__c, o.DiscountSupport_3__c, o.DiscountSupport_2__c, o.DiscountSupport_1__c, o.DiscountSpare__c, o.DiscountService__c, o.DiscountProduct__c, o.DiscountOthers__c, o.CreatedDate, o.Approved_Discount_F__c, o.Approved_Discount_E__c, o.Approved_Discount_D__c, o.Approved_Discount_C__c, o.Approved_Discount_B__c, o.Approved_Discount_A__c From Opportunity o where o.createddate >= 2009-04-01T00:00:00.0000Z and o.createddate < 2012-02-25T02:00:00.0000Z';test=false;
        }
        
        global OptyDataMigrationClass(String str)
        {
            String ids='\''+str+'\''; 
              // Commneted to free up the look up fields By Pramod/Ankita 12.17.2013
            //query = 'Select o.Approved_Quote_A__c,o.Approved_Quote_B__c,o.Approved_Quote_C__c,o.Approved_Quote_E__c,o.UpliftSupport__c,o.Id, o.UpliftSupport_3__c, o.UpliftSupport_2__c, o.UpliftSupport_1__c, o.UpliftSpares__c, o.UpliftServices__c, o.UpliftProducts__c, o.UpliftOthers__c, o.UpliftDemos__c, o.Discount_Demos__c, o.DiscountSupport__c, o.DiscountSupport_3__c, o.DiscountSupport_2__c, o.DiscountSupport_1__c, o.DiscountSpare__c, o.DiscountService__c, o.DiscountProduct__c, o.DiscountOthers__c, o.CreatedDate, o.Approved_Discount_F__c, o.Approved_Discount_E__c, o.Approved_Discount_D__c, o.Approved_Discount_C__c, o.Approved_Discount_B__c, o.Approved_Discount_A__c From Opportunity o where o.id='+ids;
            query = 'Select o.UpliftSupport__c,o.Id, o.UpliftSupport_3__c, o.UpliftSupport_2__c, o.UpliftSupport_1__c, o.UpliftSpares__c, o.UpliftServices__c, o.UpliftProducts__c, o.UpliftOthers__c, o.UpliftDemos__c, o.Discount_Demos__c, o.DiscountSupport__c, o.DiscountSupport_3__c, o.DiscountSupport_2__c, o.DiscountSupport_1__c, o.DiscountSpare__c, o.DiscountService__c, o.DiscountProduct__c, o.DiscountOthers__c, o.CreatedDate, o.Approved_Discount_F__c, o.Approved_Discount_E__c, o.Approved_Discount_D__c, o.Approved_Discount_C__c, o.Approved_Discount_B__c, o.Approved_Discount_A__c From Opportunity o where o.id='+ids;
            
        }
        
        global OptyDataMigrationClass(Boolean fromtest)
        {
            String ids='\''+'0064000000IZ8dP'+'\'';
             // Commneted to free up the look up fields By Pramod/Ankita 12.17.2013
            //query = 'Select o.Approved_Quote_A__c,o.Approved_Quote_B__c,o.Approved_Quote_C__c,o.Approved_Quote_E__c,o.UpliftSupport__c,o.Id, o.UpliftSupport_3__c, o.UpliftSupport_2__c, o.UpliftSupport_1__c, o.UpliftSpares__c, o.UpliftServices__c, o.UpliftProducts__c, o.UpliftOthers__c, o.UpliftDemos__c, o.Discount_Demos__c, o.DiscountSupport__c, o.DiscountSupport_3__c, o.DiscountSupport_2__c, o.DiscountSupport_1__c, o.DiscountSpare__c, o.DiscountService__c, o.DiscountProduct__c, o.DiscountOthers__c, o.CreatedDate, o.Approved_Discount_F__c, o.Approved_Discount_E__c, o.Approved_Discount_D__c, o.Approved_Discount_C__c, o.Approved_Discount_B__c, o.Approved_Discount_A__c From Opportunity o where o.createddate >= 2009-04-01T00:00:00.0000Z order by o.createddate desc limit 1  ';
            query = 'Select o.UpliftSupport__c,o.Id, o.UpliftSupport_3__c, o.UpliftSupport_2__c, o.UpliftSupport_1__c, o.UpliftSpares__c, o.UpliftServices__c, o.UpliftProducts__c, o.UpliftOthers__c, o.UpliftDemos__c, o.Discount_Demos__c, o.DiscountSupport__c, o.DiscountSupport_3__c, o.DiscountSupport_2__c, o.DiscountSupport_1__c, o.DiscountSpare__c, o.DiscountService__c, o.DiscountProduct__c, o.DiscountOthers__c, o.CreatedDate, o.Approved_Discount_F__c, o.Approved_Discount_E__c, o.Approved_Discount_D__c, o.Approved_Discount_C__c, o.Approved_Discount_B__c, o.Approved_Discount_A__c From Opportunity o where o.createddate >= 2009-04-01T00:00:00.0000Z order by o.createddate desc limit 1  ';
            test=true;
        }
        
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC,List<Opportunity> queryList){
       
      Map<Id,Opportunity> optyMap = new Map<Id,Opportunity>(queryList);
        Map<String,Discount_Detail__c> existingdetailMap = new Map<String,Discount_Detail__c>();    
        List<Category_Master__C> catList ;
      List<String> catSet = new List<String>{'A','B','B-2','B-3','B-4','C','D','E','F'};
            List<Discount_Detail__c> detailList = new List<Discount_Detail__c>();
           if(test==true)
            {
            
            catSet.add('B1');
            catSet.add('B2');
            catList = [select Id,Name from Category_Master__C where name in : catSet order by createddate desc ];
            }
            else            
        catList = [select Id,Name from Category_Master__C where name in : catSet];
        Map<String,Category_Master__C> catMap = new Map<String,Category_Master__C>();
        for(Category_Master__C cat : catList )
            {
                catMap.put(cat.Name,cat);
            }
        
    
        
    List<Discount_Detail__c> existingList = [Select d.Uplift__c, d.Special_Discount__c, d.Opportunity__c,d.Name, d.IsOpp__c, d.Id,
                                                     d.Disti_Special_Discount__c, d.Disti_Discount__c, d.Discount__c, d.Date_Approved__c, 
                                                     d.CurrencyIsoCode, d.Category_Master__c,d.Category_Master__r.Name From 
                                                     Discount_Detail__c d where d.Opportunity__C in : optyMap.keySet() 
                                                     and Category_Master__r.Name in :catMap.keyset() ];
                            
                          
                                
        for(Discount_Detail__c d : existingList)
        {
            existingdetailMap.put(d.Category_Master__r.Name+','+d.Opportunity__C, d);
            //detailIdsSet.add(d.Id);
        }       
        for(Opportunity q : optyMap.values())
        {
            Discount_Detail__c det;
            for( Category_Master__C cat : catList)
            {
                if(existingdetailMap.containsKey(cat.Name+','+q.Id))
                det =existingdetailMap.get(cat.Name+','+q.Id);
                else
                {
                    det = new Discount_Detail__c();
                    det.Opportunity__c = q.Id;
                    
                }
                    det.Category_Master__c = cat.Id;
                    det.isopp__c=true;
                if(cat.Name=='A') 
                {
                    
                det.Uplift__c = q.UpliftProducts__c;
                    det.Discount__c = q.DiscountProduct__c;
                    det.Special_Discount__c=q.Approved_Discount_A__c;
                   // det.Quote__c=q.Approved_Quote_A__c;
                    System.debug('inside loop=='+det+'---'+ q.UpliftProducts__c+'--id--'+'pp--'+det.Quote__c);
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='B')
                {
                    
                    det.Uplift__c = q.UpliftSupport__c;
                    det.Discount__c = q.DiscountSupport__c;
                    det.Special_Discount__c=q.Approved_Discount_B__c;
                   // det.Quote__c=q.Approved_Quote_B__c;
                    detailList.add(det);
                    
                }  
                
                else if(cat.Name=='B-2')
                {
                    
                    det.Uplift__c = q.UpliftSupport_1__c;
                    det.Discount__c = q.DiscountSupport_1__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='B-3')
                {
                    
                    det.Uplift__c = q.UpliftSupport_2__c;
                    det.Discount__c = q.DiscountSupport_2__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='B-4')
                {
                    
                    det.Uplift__c = q.UpliftSupport_3__c;
                    det.Discount__c = q.DiscountSupport_3__c;
                    detailList.add(det);
                    
                }
                
                
                else if(cat.Name=='C')
                {
                    det.Uplift__c = q.UpliftDemos__c;
                    det.Discount__c = q.Discount_Demos__c;
                    det.Special_Discount__c=q.Approved_Discount_C__c;
                    //det.Quote__c=q.Approved_Quote_C__c;
                    detailList.add(det);
                    
                }
                
                 else if(cat.Name=='D')
                {
                    det.Uplift__c = q.UpliftSpares__c;
                    det.Discount__c = q.DiscountSpare__c;
                    det.Special_Discount__c=q.Approved_Discount_D__c;
                    //det.Quote__c=q.Approved_Quote_D__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='E')
                {
                
                    det.Uplift__c = q.UpliftServices__c;
                    det.Discount__c = q.DiscountService__c;
                    det.Special_Discount__c=q.Approved_Discount_E__c;
                    //det.Quote__c=q.Approved_Quote_E__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='F')
                {
                
                det.Uplift__c = q.UpliftOthers__c;
                    det.Discount__c = q.DiscountOthers__c;
                    det.Special_Discount__c=q.Approved_Discount_F__c;
                    detailList.add(det);
                    
                }
                
                
                
                
            }
              
            
                        
        }
        
        upsert(detailList);
        
        System.debug('Data updated Successfully..'+detailList+'--'+detailList.size());  
   }

   global void finish(Database.BatchableContext BC){

   }
    
    


}