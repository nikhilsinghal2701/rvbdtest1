@isTest
private class TestGAPPortal {
    static testMethod void GAPTabControllerTest() {
        createAccount();
        GAPTabController newGapList=new GAPTabController ();
    }
    static testMethod void GAPTeamTest(){
        Account myAccount=createAccount();
        ApexPages.StandardController gTeam = New ApexPages.StandardController(myAccount);        
        System.currentPageReference().getParameters().put('id',myAccount.id);
        AccountGapTeamVisibility newGapTeam=new AccountGapTeamVisibility(gTeam);
        newGapTeam.checkAccess();
        
        GAP_Team__c gapteam=createGapTeam(myAccount);
        System.runAs(getUser()){
            newGapTeam.checkAccess();
        }
        User user=[Select Id from User where profileId='00e70000000vdzP'and IsActive=true limit 1];
        System.runAs(user){
            newGapTeam.checkAccess();
        }
    }
    static testMethod void AccountPlanControllerTest(){
        Account myAccount=createAccount();
        GAP_Team__c gapteam=createGapTeam(myAccount);
        Account_Plan__c newAccountPlan=new Account_Plan__c(name=myAccount.Name,Uniform_RVBD_Message__c='test123');
        ApexPages.StandardController aPlan = New ApexPages.StandardController(newAccountPlan);        
        
        System.currentPageReference().getParameters().put('aId',myAccount.id);
        System.currentPageReference().getParameters().put('cId',myAccount.WW_HQ__c);
        System.currentPageReference().getParameters().put('mode','edit');
        AccountPlanController testAccountPlan1 = new AccountPlanController(aPlan);       
        testAccountPlan1.firstSaveAP();
        testAccountPlan1.editAP();
        newAccountPlan.Uniform_RVBD_Message__c='testing save ap';
        testAccountPlan1.setStatusList(testAccountPlan1.getStatusList());
        testAccountPlan1.status='Global Roll Out';
        newAccountPlan.name=null;
        testAccountPlan1.saveAP();
        testAccountPlan1.cancel();
        testAccountPlan1.actPage();
        
        
        AccountPlanController ifError = new AccountPlanController();
        ifError.getError(); 
        
        User user=[Select Id from User where profileId='00e70000000wNjR'and IsActive=true limit 1];
        System.runAs(user){       
            testAccountPlan1.firstSaveAP();
        }
        System.runAs(getUser()){
            testAccountPlan1.firstSaveAP();
        }
        User user1=[Select Id from User where profileId='00e70000000vdzP'and IsActive=true limit 1];
        System.runAs(user1){
            testAccountPlan1.firstSaveAP();
        }     
    }
    
    static testMethod void GapNominatioFormTest(){
        GapNominatioForm newNominationForm=new GapNominatioForm();
        newNominationForm.ans1='test account';
        newNominationForm.ans2='GAP';
        newNominationForm.ans3='test manager';
        newNominationForm.ans4='USA';
        newNominationForm.ans5='Yes';
        newNominationForm.ans6='Yes';
        newNominationForm.ans7='Centralized';
        newNominationForm.ans8='Director';
        newNominationForm.ans8='yes';
        newNominationForm.ans8b='Manager';
        newNominationForm.ans8c='test manager';
        newNominationForm.ans8d='No';
        newNominationForm.ans8e='No';
        newNominationForm.ans9='200000';
        newNominationForm.ans9a='120000';
        newNominationForm.ans9b='100000';
        newNominationForm.ans9c='stingray';
        newNominationForm.prePareEmail();
        newNominationForm.toAddress='test@test.com';
        newNominationForm.subj='test';
        newNominationForm.body='test';
        newNominationForm.getDeliverAsPDF();
        newNominationForm.mCancel();
    }
    
    static testMethod void CompetitiveLandscapeExtensionTest(){
        Account myAccount=createAccount();
        Account_Plan__c newAccountPlan=new Account_Plan__c(name=myAccount.Name,Uniform_RVBD_Message__c='test123');
        ApexPages.StandardController aPlan = New ApexPages.StandardController(newAccountPlan);        
        System.currentPageReference().getParameters().put('aId',myAccount.id);
        System.currentPageReference().getParameters().put('cId',myAccount.WW_HQ__c);
        System.currentPageReference().getParameters().put('mode','edit');
        AccountPlanController testAccountPlan1 = new AccountPlanController(aPlan);       
        testAccountPlan1.firstSaveAP();
        
        Competitive_Landscape__c newCompLand=new Competitive_Landscape__c(Account_Plan__c=newAccountPlan.Id);
        ApexPages.StandardController compLand = New ApexPages.StandardController(newCompLand);
        CompetitiveLandscapeExtension newObj=new CompetitiveLandscapeExtension(compLand);
        newObj.competitiveLandscapeSave();
        newObj.competitiveLandscapeCancel();
    }
    
    static testMethod void CustomerInitiativesExtensionTest(){
        Account myAccount=createAccount();
        Account_Plan__c newAccountPlan=new Account_Plan__c(name=myAccount.Name,Uniform_RVBD_Message__c='test123');
        ApexPages.StandardController aPlan = New ApexPages.StandardController(newAccountPlan);        
        System.currentPageReference().getParameters().put('aId',myAccount.id);
        System.currentPageReference().getParameters().put('cId',myAccount.WW_HQ__c);
        System.currentPageReference().getParameters().put('mode','edit');
        AccountPlanController testAccountPlan1 = new AccountPlanController(aPlan);       
        testAccountPlan1.firstSaveAP();
        
        Customer_Initiatives__c newCusIni=new Customer_Initiatives__c(Account_Plan__c=newAccountPlan.Id);
        ApexPages.StandardController cusIni = New ApexPages.StandardController(newCusIni);
        CustomerInitiativesExtension newObj=new CustomerInitiativesExtension(cusIni);
        newObj.customerInitiativesSave();
        newObj.customerInitiativesCancel();
    }
    
    static testMethod void RelationshipMatrixExtensionTest(){
        Account myAccount=createAccount();
        Account_Plan__c newAccountPlan=new Account_Plan__c(name=myAccount.Name,Uniform_RVBD_Message__c='test123');
        ApexPages.StandardController aPlan = New ApexPages.StandardController(newAccountPlan);        
        System.currentPageReference().getParameters().put('aId',myAccount.id);
        System.currentPageReference().getParameters().put('cId',myAccount.WW_HQ__c);
        System.currentPageReference().getParameters().put('mode','edit');
        AccountPlanController testAccountPlan1 = new AccountPlanController(aPlan);       
        testAccountPlan1.firstSaveAP();
        
        Relationship_Matrix__c newRelMat=new Relationship_Matrix__c(Account_Plan__c=newAccountPlan.Id);
        ApexPages.StandardController relMat = New ApexPages.StandardController(newRelMat);
        RelationshipMatrixExtension newObj=new RelationshipMatrixExtension(relMat);
        newObj.relationshipMatrixSave();
        newObj.relationshipMatrixCancel();
    }
    
    private static Account createAccount(){
        RecordType accRecType = [Select r.Id, r.Name, r.SobjectType from RecordType r where r.name='Customer Account' and r.SobjectType='Account'];
        WW_HQ__c testCompany = new WW_HQ__c(Name='testCompany');
        insert testCompany;
        Account testAccount = new Account(Name = 'Jones And Sons',recordTypeId=accRecType.Id,
                            global_ultimate__c = true,account_coverage_program__c = 'GAP',
                            Global_Account_Alerts__c='Need to focus',WW_HQ__c=testCompany.Id,
                            Global_Contract__c=true,Global_Account_Manager__c=getUser().Id);
        insert testAccount;
        return testAccount;
    }
    private static User getUser(){
        User user=[select id from user where name='Craig Koltonow' and IsActive=true limit 1];
        return user;
    }
    private static GAP_Team__c createGapTeam(Account myAccount){
        GAP_Team__c newMember=new GAP_Team__c(Team_Member__c=getUser().Id,Role__c='BDR',Account_Name__c=myAccount.Id);
        insert newMember;
        return newMember;
    }
}