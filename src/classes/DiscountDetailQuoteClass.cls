/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the extension class to DiscountDetailQuotePage
Created Date : Dec 2011

Revision Log:
Rev#    Date        Author  Description
Rev1  4/22/2013     Rucha   Added delta discount column in queries for detailList and detailArchiveList
                            In discount table, only display those records that match categories on qli that have NSD
*******************************************************************************************************/

public with sharing class DiscountDetailQuoteClass {

    Quote__C pageObj ;
    public List<Discount_Detail__c> detailList {get;set;}
    public List<Discount_Detail_Archive__c> detailArchiveList {get;set;}
    public Boolean show {get;set;}
    private Map<String,Decimal> qliCatToStdDiscountMap;
    
    public DiscountDetailQuoteClass(Apexpages.StandardController controller)
    {
        pageObj = (Quote__c)controller.getrecord();
        
        //Rev1
        createQuoteLineItemCategories();
        system.debug('qliCatToStdDiscountMap Key****'+qliCatToStdDiscountMap.keySet());
         system.debug('qliCatToStdDiscountMap****'+qliCatToStdDiscountMap);
        detailList = new List<Discount_Detail__c>();        
        for(Discount_Detail__c dd : [Select d.FinalUpliftwithText__c,d.Final_Effective_Uplift__c,d.Uplift__c, d.Special_Discount__c,d.Full_Discount__c,d.Delta_Discount__c,d.Disti_Discount__c,d.Disti_Special_Discount__c, d.Quote__c, d.Opportunity__c,d.description__c, d.IsOpp__c, d.Discount__c,
                                             d.Date_Approved__c, d.Category_Master__c,d.Category_Master__R.Name From Discount_Detail__c d where 
                                             d.Quote__c = :pageObj.Id and IsOpp__c=false order by d.Category_Master__R.Name]){
                                                
            
            for(String cat : qliCatToStdDiscountMap.keySet()){
             system.debug('****'+cat);              
                if(dd.Category_Master__R.Name!=null && dd.Category_Master__R.Name.startsWith(cat)){
                    detailList.add(dd);
                }
            }
        }
        system.debug('****DetailList'+detailList);
        //
    
        show=true;
    
        if(detailList.isempty())  
        {   
            //Rev1
            detailArchiveList=new List<Discount_Detail_Archive__c>();
                                                             
            for(Discount_Detail_Archive__c dd : [Select d.description__c,d.Uplift__c, d.Special_Discount__c,d.Delta_Discount__c,d.Disti_Discount__c,d.Disti_Special_Discount__c, d.Quote__c, d.Opportunity__c, d.IsOpp__c, d.Discount__c,
                                                 d.Date_Approved__c, d.Category_Master__c,d.Category_Master__R.Name From Discount_Detail_Archive__c d where 
                                                 d.Quote__c = :pageObj.Id and IsOpp__c=false order by d.Category_Master__R.Name]){
                                                    
                for(String cat : qliCatToStdDiscountMap.keySet()){
                    if(dd.Category_Master__R.Name!=null && dd.Category_Master__R.Name.startsWith(cat)){
                        detailArchiveList.add(dd);
                    }
                }
            }
            //
            
            show=false;
        }
    
    }
    
    //Rev1
    public void createQuoteLineItemCategories(){
        //quoteToQliCategoryMap = new Map<Id,Map<String,String>>();
        qliCatToStdDiscountMap = new Map<String,Decimal>();
        Map<String,Decimal> discounts = new Map<String,Decimal>(); 
         
        for(Quote_Line_Item__c item : [Select Id, Quote__c,Category__c,Standard_Discount__c,Non_Standard_Discount__c
                                        from Quote_Line_Item__c 
                                        where Quote__c=:pageObj.Id 
                                            and Category__c!=null
                                            and Non_Standard_Discount__c!=null]){           
                String cat = item.Category__c;
                Decimal newValue  =  0;
                
               
                  
                        Decimal std = (item.Standard_Discount__c == null) ? 0: item.Standard_Discount__c;
                        if (std >= item.Non_Standard_Discount__c) {
                            newValue = item.Non_Standard_Discount__c - std;
                            newValue = (newValue < 0) ? 0 : newValue;
                            newValue = (newValue > 100) ? 100 : newValue; 
                        } else {                            
                            newValue = item.Non_Standard_Discount__c - std;
                        }
                  
                  
                    if (discounts.get(cat) != null) {
                        Decimal current = discounts.get(cat);                      
                        if (current < newValue) {
                            discounts.put(cat, newValue);                          
                            qliCatToStdDiscountMap.put(cat,item.Non_Standard_Discount__c);               
                        }
                    } else {
                        discounts.put(cat, newValue);
                        qliCatToStdDiscountMap.put(cat,item.Standard_Discount__c);
                    }           
        }       
    }
    //
}