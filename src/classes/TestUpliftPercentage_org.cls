/*******************************
* Name : TestUpliftPercentage
* Purpose : Test class for UpliftPercentage to cover all the test scenarios
* Date: 10/10/2013
* Author: Sunil ( Perficient )
********************************/
@isTest
private class TestUpliftPercentage_org {

    testMethod static void testRunRules() {
      //TestingUtilPP.createADMConfig();    
      DiscountApproval.isTest = true;
      inactivateExistingRules();
      system.test.startTest();
     
//---Create new Opportnity--------------      
      Opportunity opp= new Opportunity(Discount_Schedule__c=null);
//---Create Account records-------------
      Account Acct = new  Account(NAME='TestAccount',TYPE='Customer',Record_Type__c  ='Customer Account',Industry='Electronics');
        insert Acct;
      Account AcctSoldTo1 = new  Account(NAME='Zycko LTD',Partner_Level__c='Distributor',TYPE='Distributor',Record_Type__c  ='Partner Account', ApplyUplift__c =true,
                                        Partner_Status1__c = 'Good Standing',Country_Category__c = 'Category A', Geographic_Coverage__c='EMEA', Region__c='UK',Global_Fulfillment_Partner__c=true);
        insert AcctSoldTo1;
        Account AcctSoldTo = new  Account(NAME='Zycko Fr',ParentId=AcctSoldTo1.id,Partner_Level__c='Distributor',TYPE='Distributor',Record_Type__c  ='Partner Account', ApplyUplift__c =true,
                                        Partner_Status1__c = 'Good Standing',Country_Category__c = 'Category A', Geographic_Coverage__c='EMEA', Region__c='UK',Global_Fulfillment_Partner__c=true);
        insert AcctSoldTo;  
      Account AcctTier2 = new  Account(NAME='Tier2_Account1',TYPE='VAR',Record_Type__c  ='Partner Account',Industry='Electronics',
                            Partner_Level__c='Premier',Partner_Status1__c = 'Good Standing', Country_Category__c = 'Category A', Geographic_Coverage__c='Americas', Region__c='US National Partner',Global_Fulfillment_Partner__c=true);
        insert AcctTier2;        
//---Update Opportunity reocrd------- 
      List<Account> accList= [select Id,Name from Account where Name in ('TestAccount','Zycko LTD','Tier2_Account1')];
      for(Account ac : accList)
      {
        if(ac.Name=='Tier2_Account1')
        opp.Tier2__c=ac.Id;
        else if(ac.Name=='TestAccount')
        opp.AccountId=ac.ID;
        else if(ac.Name=='Zycko LTD')
        opp.Sold_to_Partner__c=ac.Id;
      }
        opp.Sold_to_Partner_Level__c='Distributor';
        opp.Tier2_Level__c='Premier';
        opp.CloseDate = System.today();
        opp.Name = 'Test Opp1';
        opp.StageName = '0 - Prospect';
        opp.Number_Eval_Products__c = 5;
        opp.Registered_Deal__c = false;
        opp.Channel__c='Direct';
        opp.Support_Provided__c = 'Riverbed';
        opp.DiscountProduct__c=10;
        opp.Approved_Discount_A__c=8;
        insert(opp);
//      System.debug(LoggingLevel.INFO,'***Opportunity'+opp);   
//---Create Discount Category------------      
        DiscountCategory__c dc = new DiscountCategory__c();
        dc.DiscountProducts__c = 15;
        dc.DiscountSupportBand0__c = 15;
        dc.DiscountSupportBand1__c = 15;
        dc.DiscountSupportBand2__c = 15;
        dc.DiscountDemos__c = 15;
        dc.DiscountNone__c = 15;
        dc.DiscountSpare__c = 15;
        dc.DiscountServices__c = 15;
        insert dc;
        opp.Discount_Category__c = dc.Id;
        update(opp);
//      system.debug('Discount_Category:' +opp.Discount_Category__c) ;
//---Create Discount Detail-------------
        List<Category_Master__c> catList = new List<Category_Master__c>();
        Category_Master__c cat = new Category_Master__c(Name='A', Group__c ='Appliance', Active__c=true,Uplift_Value__c=0.00);
        Category_Master__c cat1 = new Category_Master__c(Name='B', Group__c ='Software', Active__c=true,Uplift_Value__c=0.00);
        catList.add(cat);
        catList.add(cat1);
        insert(catList);
        
/*      set<Id> ids1=new set<Id>();
        for(Category_Master__c temp1:catList){
            ids1.add(temp1.id); */
//---Create Discount Category Detail record------
        List<Discount_Category_Detail__c> detailList = new List<Discount_Category_Detail__c>();
        Discount_Category_Detail__c det = new Discount_Category_Detail__c (Active__c = true, Category__c = catList[0].id, Discount_Table__C =dc.Id, Registered_Discount__c =0, Standard_Discount__c = 15.00);
        Discount_Category_Detail__c det1 = new Discount_Category_Detail__c (Active__c = true, Category__c = catList[1].id, Discount_Table__C =dc.Id, Registered_Discount__c =0, Standard_Discount__c = 20.00);
        detailList.add(det);
        detailList.add(det1);
        insert(detailList);
//      system.debug('Discount_Category_Detail:'+detailList);
//---Create Discount Schedule----------------
        Discount_Schedule__c rule = new Discount_Schedule__c();
        rule.Discount_Category__c = dc.id;
        rule.Active__c = true;
        rule.Partner_Type__c='Distributor';
        rule.Start_Date__c=date.newinstance(2008, 10, 15);
        rule.End_Date__c=date.newinstance(2020,12,1);
        rule.ApplyUplift__c=false;
        insert rule;
//      system.debug('***Discount_Schedule:' +rule.Id) ;
//---Create Discount Condition--------------
        Discount_Condition__c c1 = new Discount_Condition__c();
        c1.Discount_Schedule__c = rule.Id;
        c1.Field__c = DiscountApproval.END_USER_ACCOUNT + '.Name';
        c1.Operator__c = 'equals';
        c1.Value__c = 'TestAccount';
        insert c1;
        opp.NSD_Calc__c = true;
        DiscountApproval.isTest = true;

        AcctSoldTo.Global_Fulfillment_Partner__c = false;
        update(AcctSoldTo);
        AcctTier2.Global_Fulfillment_Partner__c = false;
        update(AcctTier2);

        update opp;
//      system.debug(LoggingLevel.INFO,'***Opportunitylast'+opp); 
//---Create Disti Uplift record--------------
        List<Disti_Uplift__c> disti = createDistiuplift(opp.Sold_to_Partner__c,opp.Id,opp.Sold_to_Partner_Geo__c,opp.Sold_to_Partner_Region__c,opp.Tier2_Level__c);
        system.debug(LoggingLevel.INFO,'***distiuplift:'+disti); 

        List<Opportunity> opptlist = new List<Opportunity>();
        opptlist.add(opp);        
        DiscountApproval.runRules(opptlist,null);
        system.test.stopTest();     

//------------Start testing-------------    
        List<Disti_Uplift_Detail__c> upliftdetails = [SELECT Category_Master__c, Value__c, Disti_Uplift__c,Disti_Uplift__r.Distributor__c, Disti_Uplift__r.Geo__c,
                                                      Disti_Uplift__r.Region__c,Disti_Uplift__r.PartnerLevel__c,Disti_Uplift__r.Registered_Deal__c,Disti_Uplift__r.Global_Fulfillment_Distributor__c,
                                                      Disti_Uplift__r.Global_Fulfillment_Reseller__c
                                                      FROM Disti_Uplift_Detail__c WHERE Category_Master__c =:catList[0].id];
        system.debug(LoggingLevel.INFO,'***upliftdetails'+upliftdetails); 

        List<Discount_Detail__c> discountdetail = [SELECT Category_Master__c, Uplift__c,Discount__c FROM Discount_Detail__c 
                                                   WHERE Category_Master__c = :catList[0].id];
        system.debug(LoggingLevel.INFO,'***discountdetail'+discountdetail);         

//*******This for loop will cover uplift1,2,3,4 & 5 scenarios, to test uplift0, we need to update the accounts
                
        for(Disti_Uplift_Detail__c d : upliftdetails){
            if(d.Disti_Uplift__r.Global_Fulfillment_Distributor__c == true && d.Disti_Uplift__r.Global_Fulfillment_Reseller__c == true
               && (opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c == true || opp.tier2__r.Global_Fulfillment_Partner__c==true)){//uplift0 
                system.debug('***inside0:'+d);
                system.assertEquals(discountdetail[0].Category_Master__c, d.Category_Master__c);
                system.assertEquals(discountdetail[0].Uplift__c, d.Value__c);
            } else if(d.Disti_Uplift__r.Global_Fulfillment_Distributor__c == false && d.Disti_Uplift__r.Global_Fulfillment_Reseller__c == false
                      && (opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c == false || opp.tier2__r.Global_Fulfillment_Partner__c==false)
                      && d.Disti_Uplift__r.Distributor__c != null && d.Disti_Uplift__r.PartnerLevel__c == null
                      && d.Disti_Uplift__r.Geo__c == null && d.Disti_Uplift__r.Region__c == null){//uplift1 
                system.debug('***inside1:'+d);
                system.assertEquals(discountdetail[0].Category_Master__c, d.Category_Master__c);
                system.assertEquals(discountdetail[0].Uplift__c, d.Value__c);
            } else if(d.Disti_Uplift__r.Global_Fulfillment_Distributor__c == false && d.Disti_Uplift__r.Global_Fulfillment_Reseller__c == false
                      && (opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c == false || opp.tier2__r.Global_Fulfillment_Partner__c==false)
                      && d.Disti_Uplift__r.PartnerLevel__c != null){//uplift2
                system.debug('***inside2:'+d);
                system.assertEquals(discountdetail[0].Category_Master__c, d.Category_Master__c);
                system.assertEquals(discountdetail[0].Uplift__c, d.Value__c);
            } else if(d.Disti_Uplift__r.Global_Fulfillment_Distributor__c == false && d.Disti_Uplift__r.Global_Fulfillment_Reseller__c == false
                      && (opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c == false || opp.tier2__r.Global_Fulfillment_Partner__c==false)
                      && d.Disti_Uplift__r.Geo__c != null && d.Disti_Uplift__r.Region__c != null){////uplift3
                system.debug('***inside3:'+d);
                system.assertEquals(discountdetail[0].Category_Master__c, d.Category_Master__c);
                system.assertEquals(discountdetail[0].Uplift__c, d.Value__c);
            } else if(d.Disti_Uplift__r.Global_Fulfillment_Distributor__c == false && d.Disti_Uplift__r.Global_Fulfillment_Reseller__c == false
                      && (opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c == false || opp.tier2__r.Global_Fulfillment_Partner__c==false)
                      && d.disti_Uplift__r.Geo__c != null && d.Disti_Uplift__r.Region__c == null){////uplift4
                system.debug('***inside4:'+d);
                system.assertEquals(discountdetail[0].Category_Master__c, d.Category_Master__c);
                system.assertEquals(discountdetail[0].Uplift__c, d.Value__c);
            } else if(d.Disti_Uplift__r.Global_Fulfillment_Distributor__c == false && d.Disti_Uplift__r.Global_Fulfillment_Reseller__c == false
                      && d.Disti_Uplift__r.PartnerLevel__c == null && d.Disti_Uplift__r.Geo__c == null && d.Disti_Uplift__r.Region__c == null 
                      && d.Disti_Uplift__r.Distributor__c == null){////uplift5
                system.debug('***inside5:'+d);
                system.assertEquals(discountdetail[0].Category_Master__c, d.Category_Master__c);
                system.assertEquals(discountdetail[0].Uplift__c, d.Value__c);
            }
        }

        //***uplift0
        AcctSoldTo.Global_Fulfillment_Partner__c = true;
        update(AcctSoldTo);
        AcctTier2.Global_Fulfillment_Partner__c = true;
        update(AcctTier2);
        opp.NSD_Calc__c = true; 
        update(opp); 

        system.debug('accntsoldeto_gfp' +opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c);
        system.debug('tier2_gfp' +opp.tier2__r.Global_Fulfillment_Partner__c);
        
        for(Disti_Uplift_Detail__c d2 : upliftdetails){
            if(d2.Disti_Uplift__r.Global_Fulfillment_Distributor__c == true && d2.Disti_Uplift__r.Global_Fulfillment_Reseller__c == true
               && (opp.Sold_to_Partner__r.Global_Fulfillment_Partner__c == true || opp.tier2__r.Global_Fulfillment_Partner__c==true)){//uplift0 
            system.debug('***inside0:'+d2);
            system.assertEquals(discountdetail[0].Category_Master__c, d2.Category_Master__c);
            system.assertEquals(discountdetail[0].Uplift__c, d2.Value__c);
            }
        }

    } 
       
       public static List<Disti_Uplift__c> createDistiuplift(String SoldToId, String OptyId,String geos,String Region,String level)
       {
        Disti_Uplift__c disti = new Disti_Uplift__c(active__c=true,registered_Deal__c=false,Global_Fulfillment_Distributor__c=true,Global_Fulfillment_Reseller__c=true); //uplift0
        Disti_Uplift__c disti1 = new Disti_Uplift__c(active__c=true,registered_Deal__c=false, Distributor__C=SoldTOID,Global_Fulfillment_Distributor__c=false,Global_Fulfillment_Reseller__c=false); // uplift1
        Disti_Uplift__c disti3 = new Disti_Uplift__c(active__c=true,registered_Deal__c=true,PartnerLevel__c='Premier',Distributor__C=SoldTOID,Global_Fulfillment_Distributor__c=false,Global_Fulfillment_Reseller__c=false); //uplift2
        Disti_Uplift__c disti2 = new Disti_Uplift__c(active__c=true,geo__c='EMEA',Region__c='UK',Global_Fulfillment_Distributor__c=false,Global_Fulfillment_Reseller__c=false); //uplift3       
        Disti_Uplift__c disti4 = new Disti_Uplift__c(active__c=true,geo__C='EMEA',Global_Fulfillment_Distributor__c=false,Global_Fulfillment_Reseller__c=false); // uplift4
        Disti_Uplift__c disti5 = new Disti_Uplift__c(active__c=true,Global_Fulfillment_Distributor__c=false,Global_Fulfillment_Reseller__c=false); // uplift5
        List<Disti_Uplift__c> listDisti= new List<Disti_Uplift__c>();
        listDisti.add(disti);
        listDisti.add(disti1);
        listDisti.add(disti2);
        listDisti.add(disti3);
        listDisti.add(disti5);    
        listDisti.add(disti4);
        insert(listDisti);
        system.debug('***disti_uplift_c:'+listDisti);       
        return listDisti;
       }    
  
    private static DiscountCategory__c getDiscountCategory(Id oppId){
        
        DiscountCategory__c dc = null;
        
        Id dcId = [select Discount_Category__c from Opportunity where Id = : oppId].Discount_Category__c;
        system.debug('discountcatId:' +dcId);
        if (dcId != null){
             dc = [select Id,
                    DiscountProducts__c,
                    DiscountSupportBand0__c,
                    DiscountSupportBand1__c,
                    DiscountSupportBand2__c,
                    DiscountDemos__c,
                    DiscountNone__c,
                    DiscountSpare__c,
                    DiscountServices__c 
                    from DiscountCategory__c 
                    where Id = : dcId];
        }
        return dc;  
    }   
   
    private static void inactivateExistingRules() {
 //---Turn off all existing rules------------------ 
        List<Discount_Schedule__c> rules = new List<Discount_Schedule__c>();
        for (Discount_Schedule__c rule : [SELECT Id,Active__c FROM Discount_Schedule__c limit 200]) {
            rule.Active__c = false;
            rules.add(rule);
        }
        update rules;
    }
}