public class ContactUserSync {
/**
*	
*	@param triggerNew the trigger context new List 
*	@return void
*/
//For asynchronous updates
private static Map <Id, String> m_userUpdates = new Map <Id, String> ();

private static final String DELIMITER = '□';

public static void process(Map <Id, Contact> triggerNewMap, Map <Id, Contact> triggerOldMap){
	
	Map <Id, User> userMap = new Map <Id, User>() ;
	
	for (User u : [Select u.Title, u.ContactId, u.Street, u.State, u.PostalCode, u.Phone, u.Name, u.LastName, u.FirstName, u.Fax, u.Country, u.City 
						From User u
						where u.ContactId in : triggerNewMap.keyset()]){
							
		userMap.put(u.ContactId, u);
	}
	
	for (Contact c : triggerNewMap.Values()){
		User u = userMap.get(c.Id);
		Contact oldCon = triggerOldMap.get(c.Id);
		if (u != null
			&& (oldCon.MailingStreet != c.MailingStreet
				|| oldCon.MailingCity != c.MailingCity
				|| oldCon.MailingState != c.MailingState
				|| oldCon.MailingPostalCode != c.MailingPostalCode 
				|| oldCon.MailingCountry != c.MailingCountry
				|| oldCon.Phone != c.Phone
				|| oldCon.Title != c.Title
				|| oldCon.FirstName != c.FirstName
				|| oldCon.LastName != c.LastName)){
					
			User updtUsr = new User(Id = u.Id, 
									Street = c.MailingStreet, 
									State = c.MailingState, 
									PostalCode =  c.MailingPostalCode, 
									Country = c.MailingCountry, 
									City = c.MailingCity,
									Title = c.Title, 
									Phone = c.Phone,  
									FirstName = c.FirstName,  
									LastName = c.LastName); 
									
			m_userUpdates.put(updtUsr.Id, serializeUser(updtUsr));	
		}															
	}
	if (m_userUpdates.size() > 0){
		System.debug('m_userUpdates: ' + m_userUpdates);
		asyncFlush(m_userUpdates);
	}	
}

/**
*	Write any model changes made during the after trigger.
*/
public static void flush(){

	asyncFlush(m_userUpdates);

}

private static String serializeUser(User u){
	
	String result =   u.Id + DELIMITER + 
								u.Street + DELIMITER + 
								u.State + DELIMITER + 
								u.PostalCode + DELIMITER +  
								u.Country + DELIMITER + 
								u.City + DELIMITER + 
								u.Title + DELIMITER +  
								u.Phone + DELIMITER +  
								u.FirstName + DELIMITER +   
								u.LastName;
								
								
	return result;								
}

private static User deSerializeUser(String uStr){
	
	System.debug(uStr);
	String [] tokens =   uStr.split(DELIMITER);
	User u = new User (Id = tokens[0],
						Street = tokens[1] == 'null' ? null: tokens[1],
						State = tokens[2]== 'null' ? null: tokens[2],
						PostalCode = tokens[3]== 'null' ? null: tokens[3],
						Country = tokens[4] == 'null' ? null: tokens[4],
						City = tokens[5] == 'null' ? null: tokens[5],
						Title = tokens[6] == 'null' ? null: tokens[6],
						Phone = tokens[7] == 'null' ? null: tokens[7],
						FirstName = tokens[8] == 'null' ? null: tokens[8],
						LastName = tokens[9] == 'null' ? null: tokens[9]);
								
								
	return u;								
}

/**
*	Write any model changes made during the after trigger in asynchronous mode
*/
@future
private static void asyncFlush(Map <Id, String> userUpdateMap){
									

	List <User> userList = new List <User> ();
 
	

	if (userUpdateMap.size() > 0){
		for (Id userId : userUpdateMap.keyset()){
			String ctx = userUpdateMap.get(userId);
			User u = deserializeUser(ctx);
			userList.add(u);	
		}
		update userList;
	}	
						
}
/**
*	Code coverate test method > 85%
*	@return void
*/
static  testMethod void coverage(){
	
		User updtUsr = new User(Id = UserInfo.getUserId(), 
								Street = 'MailingStreet', 
								State = 'MailingState', 
								PostalCode =  'MailingPostalCode', 
								Country = 'MailingCountry', 
								City = null,
								Title = 'Title', 
								Phone = 'Phone',  
								FirstName = 'FirstName',  
								LastName = 'LastName'); 
								
		String ctx = serializeUser(updtUsr);
		
		User u = deSerializeUser(ctx);
		
		User pu = [Select Id, ContactId from user 
							where ContactId != null
							and isActive = true 
							limit 1];		
		
		Contact oldc =	new Contact(Id = pu.ContactId,
									MailingStreet  = 'aStreet', 
									MailingState = 'aState', 
									MailingPostalCode =  'aPostalCode', 
									MailingCountry = 'aCountry', 
									MailingCity = 'aCity',
									Title = 'aTitle', 
									Phone = '91234567890',  
									FirstName = 'aFirstName',  
									LastName = 'aLastName');
											
		Contact newc =	new Contact(Id = pu.ContactId,
									MailingStreet  = 'Street', 
									MailingState = 'State', 
									MailingPostalCode =  'PostalCode', 
									MailingCountry = 'Country', 
									MailingCity = 'City',
									Title = 'Title', 
									Phone = '1234567890',  
									FirstName = 'FirstName',  
									LastName = 'LastName'); 
									
		update newc;
		
		
		//process(new Map <Id,Contact> {newc.Id => newc},new Map <Id,Contact> {oldc.Id => oldc});
											
		Contact c =	[Select MailingStreet, 
				MailingState, 
				MailingPostalCode, 
				MailingCountry, 
				MailingCity,
				Title, 
				Phone,  
				FirstName,  
				LastName
				from Contact
				where Id = : pu.ContactId]; 		
									
	/*System.Assert(c.MailingStreet  == 'Street'&& 
					c.MailingState == 'State'&& 
					c.MailingPostalCode ==  'PostalCode'&& 
					c.MailingCountry == 'Country'&& 
					c.MailingCity == 'City'&&
					c.Title == 'Title'&& 
					c.Phone == '1234567890'&&  
					c.FirstName == 'FirstName'&&  
					c.LastName == 'LastName');	*/															
}
}