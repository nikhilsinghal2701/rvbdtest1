public with sharing class AssetsUtil {
        /*Purpose: Product Family and Chassis Type Implementation project business logic.
        **By: prashant.singh@riverbed.com
        **Date: 7/11/2012
        **Modified By: prashant.singh@riverbed.com
        **Date: 10/15/2012
        **Purpose: ticket#124346
        **Modified By: Nikhil.singhal@riverbed.com
        **Date: 12/19/2013
        **Purpose: Update Child Assets
        **Modified By: Anil Madithati 
        **Date: 08/23/2014
        **Purpose: to Auto populate Product Info based on Base SKU((Part of NPI 3.0))
        */
        private static Map<String,SKU_Product_Family_Mapping__c>skuMap=new Map<String,SKU_Product_Family_Mapping__c>();
        private static Map<String,SKU_Product_Family_Mapping__c>sn3CharsMap=new Map<String,SKU_Product_Family_Mapping__c>();
        private static Map<String,SKU_Product_Family_Mapping__c>sn2CharsMap=new Map<String,SKU_Product_Family_Mapping__c>();
        public static void productFamilyChasisTypeUpdate(List<Asset> newTrigger,Map<Id,Asset> oldMap){
                string sn3Char,skuStartsWith;
                Boolean isSet;
                getSkuDetails();
                for(Asset asst:newTrigger){
                        isSet=false;
                        sn3Char=asst.Name;
                        sn3Char=sn3Char.toUpperCase(); 
                        skuStartsWith=asst.SKU__c;
                        if(!skuMap.isEmpty()){
                                for(String pString:skuMap.keySet()){
                                        if(skuStartsWith.startsWithIgnoreCase(pString)){
                                                asst.Product_Family__c=skuMap.get(pString).Product_Family__c;
                                                asst.Product_Family1__c=skuMap.get(pString).ProductFamily__c;
                                                asst.ChassisType__c=skuMap.get(pString).Chassis_Type__c;
                                                isSet=true;
                                                break;
                                        }
                                }
                                if(!sn3CharsMap.isEmpty() &&!isSet && sn3CharsMap.containsKey(sn3Char.subString(0,3))){
                                        asst.Product_Family__c=sn3CharsMap.get(sn3Char.subString(0,3)).Product_Family__c;
                                        asst.Product_Family1__c=sn3CharsMap.get(sn3Char.subString(0,3)).ProductFamily__c;
                                        asst.ChassisType__c=sn3CharsMap.get(sn3Char.subString(0,3)).Chassis_Type__c;
                                }else if(!sn2CharsMap.isEmpty() &&!isSet && sn2CharsMap.containsKey(sn3Char.subString(0,2))){
                                        asst.Product_Family__c=sn2CharsMap.get(sn3Char.subString(0,2)).Product_Family__c;
                                        asst.Product_Family1__c=sn2CharsMap.get(sn3Char.subString(0,2)).ProductFamily__c;
                                        asst.ChassisType__c=sn2CharsMap.get(sn3Char.subString(0,2)).Chassis_Type__c;
                                }else if(!isSet){
                                        asst.Product_Family__c=null;
                                        asst.Product_Family1__c=null;
                                        asst.ChassisType__c=null;
                                }
                        }else if(!sn3CharsMap.isEmpty() && sn3CharsMap.containsKey(sn3Char.subString(0,3))){
                                asst.Product_Family__c=sn3CharsMap.get(sn3Char.subString(0,3)).Product_Family__c;
                                asst.Product_Family1__c=sn3CharsMap.get(sn3Char.subString(0,3)).ProductFamily__c;
                                asst.ChassisType__c=sn3CharsMap.get(sn3Char.subString(0,3)).Chassis_Type__c;
                        }else if(!sn2CharsMap.isEmpty() && sn2CharsMap.containsKey(sn3Char.subString(0,2))){
                                asst.Product_Family__c=sn2CharsMap.get(sn3Char.subString(0,2)).Product_Family__c;
                                asst.Product_Family1__c=sn2CharsMap.get(sn3Char.subString(0,2)).ProductFamily__c;
                                asst.ChassisType__c=sn2CharsMap.get(sn3Char.subString(0,2)).Chassis_Type__c;
                        }else{
                                asst.Product_Family__c=null;
                                asst.ChassisType__c=null;
                                asst.Product_Family1__c=null;
                        }
                }
        }
        private static void getSkuDetails(){
                List<SKU_Product_Family_Mapping__c> skuList=[select id,SKU_Starts_with__c,SN_Prefix__c,Product_Family__c,
                Chassis_Type__c,ProductFamily__c from SKU_Product_Family_Mapping__c];
                string sn2Char,sn3Char;
                if(skuList!=null && skuList.size()>0){
                        for(SKU_Product_Family_Mapping__c temp:skuList){
                                if(temp.SKU_Starts_with__c!=null && !skuMap.containsKey(temp.SKU_Starts_with__c)){
                                        skuMap.put(temp.SKU_Starts_with__c,temp);
                                }
                                if(temp.SN_Prefix__c!=null && !sn3CharsMap.containsKey(temp.SN_Prefix__c)){
                                        sn3Char=temp.SN_Prefix__c;
                                        if(sn3Char!=null && sn3Char.length()==3){
                                                sn3CharsMap.put(sn3Char.toUpperCase(),temp);
                                        }
                                }
                                if(temp.SN_Prefix__c!=null && !sn2CharsMap.containsKey(temp.SN_Prefix__c)){
                                        sn2Char=temp.SN_Prefix__c;
                                        if(sn2Char!=null && sn2Char.length()==2){
                                                sn2CharsMap.put(sn2Char.toUpperCase(),temp);
                                        }
                                }
                        }
                }
        }

        //Added By Nikhil
        public static void OnAfterUpdateChild(Map<Id,Asset>NewAssetMap,Map<Id,Asset>OldAssetMap){
            Map<Id,Asset> ChangedAssetIdMap = new Map<Id,Asset>();

            for(Asset ast: NewAssetMap.values()){
                if( NewAssetMap.get(ast.id).Instance_Expiry_Date__c != OldAssetMap.get(ast.id).Instance_Expiry_Date__c ||
                    NewAssetMap.get(ast.id).Contract_Termination_Date__c != OldAssetMap.get(ast.id).Contract_Termination_Date__c ||
                    NewAssetMap.get(ast.id).IB_Status__c != OldAssetMap.get(ast.id).IB_Status__c ||
                    NewAssetMap.get(ast.id).Support_End_Date__c != OldAssetMap.get(ast.id).Support_End_Date__c ||
                    NewAssetMap.get(ast.id).SupportStartDate__c != OldAssetMap.get(ast.id).SupportStartDate__c ||
                    NewAssetMap.get(ast.id).Renewed_Start_Date__c != OldAssetMap.get(ast.id).Renewed_Start_Date__c ||
                    NewAssetMap.get(ast.id).Renewed_End_Date__c != OldAssetMap.get(ast.id).Renewed_End_Date__c ||
                    NewAssetMap.get(ast.id).IB_Start_Date__c != OldAssetMap.get(ast.id).IB_Start_Date__c ||
                    NewAssetMap.get(ast.id).Status != OldAssetMap.get(ast.id).Status ||
                    NewAssetMap.get(ast.id).SKU__c != OldAssetMap.get(ast.id).SKU__c ||
                    NewAssetMap.get(ast.id).SupportLevel__c != OldAssetMap.get(ast.id).SupportLevel__c ||
                    NewAssetMap.get(ast.id).Support_Provider__c != OldAssetMap.get(ast.id).Support_Provider__c ){

                    ChangedAssetIdMap.put(ast.id,ast);
                }
            }

            if(!ChangedAssetIdMap.isEmpty()){
                UpdateChildAssets(ChangedAssetIdMap);
            }

        }

        Private static void UpdateChildAssets(Map<Id,Asset> ChangedAssetIdMap){
            Map<Id,Id> ChildParentMap= new Map<Id,Id>();
            List<Asset> AssetToUpdate = new List<Asset>();
            for(Asset ChildAsset :[SELECT Id,Parent_Asset__c FROM Asset WHERE Parent_Asset__c in :ChangedAssetIdMap.keyset()]){
                    ChildParentMap.put(ChildAsset.Id,ChildAsset.Parent_Asset__c);
            }

            for(id cid : ChildParentMap.keyset()){

                AssetToUpdate.add( new Asset( Id = cid,
                                Instance_Expiry_Date__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).Instance_Expiry_Date__c,
                                Contract_Termination_Date__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).Contract_Termination_Date__c,
                                IB_Status__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).IB_Status__c,
                                Support_End_Date__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).Support_End_Date__c,
                                SupportStartDate__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).SupportStartDate__c,
                                Renewed_Start_Date__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).Renewed_Start_Date__c,
                                Renewed_End_Date__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).Renewed_End_Date__c,
                                IB_Start_Date__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).IB_Start_Date__c,
                                Status = ChangedAssetIdMap.get(ChildParentMap.get(cid)).Status,
                                SKU__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).SKU__c,
                                Support_Provider__c=ChangedAssetIdMap.get(ChildParentMap.get(cid)).Support_Provider__c,
                                SupportLevel__c = ChangedAssetIdMap.get(ChildParentMap.get(cid)).SupportLevel__c ) );

            }

            if( !AssetToUpdate.isEmpty() ){
                        try{
                            update AssetToUpdate;

                        }catch( Dmlexception ex){

                        }
                }
    }
   // Added By Anil Madithati on 08.05.2014 to Auto populate Product Info based on Base SKU(Part of NPI 3.0)   
    Public static void UpdateAssetProductUsingBaseSku (List<Asset> AsstLst){ 
            Map<string,List<Asset>> assetMap = new Map<String,List<Asset>>();
            Map<string,Product2> prodMap = new Map<String,Product2>();
            for(Asset asst: AsstLst){
            if (assetMap.containsKey(asst.Base_sku__c))
              {
               assetMap.get(asst.Base_sku__c).add(asst);
               }
               else{
                assetMap.put(asst.Base_sku__c,new List <Asset> { asst }); // put base sku in assetmap  
               }
             } 
                
            system.debug('base SKU Asset map***:'+assetMap);
            List<Product2> ProdList = new List<Product2>([ Select id,name,family,ProductCode,Product_SubFamily__c,Part_Number__c 
                                           FROM Product2 
                                           WHERE ProductCode IN :AssetMap.keyset()]);
            system.debug('ProdList***:'+ProdList);                          
            if(ProdList.size()>0){
                for(Product2 p:ProdList){
                system.debug('ProductCode***:'+p.ProductCode);
                    prodMap.put(p.ProductCode,p);
                }
                system.debug('Product map***:'+prodMap); 
                if(assetMap.size()>0){
                    for(string s:prodMap.keyset()){
                        for(Asset a: assetMap.get(s)){
                            a.Product2Id=prodMap.get(s).id;
                        } 
                        
                        }
                    }
                }
         }
}