/*

Apex class to generate custom mailto link for external mail application
06/10/13 RT@IC Initial creation. (Case 76150)
08/19/13 RT@IC Modified fields to clients specifications (Case 76150).
09/04/13 RT@IC Removed BCC field; Included Case suppliedEmail to the "To" field of the email.

*/

public class MailToGeneratorCtrl{

    public Case cs;
    public Case[] caseTemp;
    public MailToGeneratorCtrl(ApexPages.StandardController controller){
        this.cs = (Case)controller.getRecord();
        caseTemp = [SELECT id, case.owner.email, suppliedEmail, contact.email, CaseNumber, Subject, Case_Thread_ID__c, E2CP__AdditionalBCC__c, E2CP__AdditionalCC__c, E2CP__AdditionalTo__c 
                    FROM Case 
                    WHERE id = :cs.id LIMIT 1];
    }
    
    public String getMailtoLink(){
        String link = '';
        String webEmail;
        if(String.isNotBlank(caseTemp[0].SuppliedEmail))
            webEmail = String.valueOf(caseTemp[0].SuppliedEmail);
        
        // Email settings
        //No longer needed
        //String to = 'mailto:support@riverbed.com?';
        String to ='mailto:NoEmailOnContactInCase@riverbed.com?';
        if(String.isNotBlank(caseTemp[0].contact.email)){
            to = 'mailto:' + caseTemp[0].contact.email + '?';   
        }
        String cc = 'CC=';
        //No longer needed
        //String bcc = 'BCC=' + caseTemp[0].CaseNumber + '@riverbedsupport.com';
        String subject = 'Subject=[RVBD: ' + caseTemp[0].CaseNumber + '] ' + caseTemp[0].Subject + ' '+ caseTemp[0].Case_Thread_ID__c; 

        //Get "CCed on Email" Role id
        CaseTeamRole[] CTR = [SELECT id, name FROM CaseTeamRole WHERE name = 'CCed on Email' LIMIT 1];
        
        if(CTR.isEmpty()){ //If CCed on Email Team Role does not exist.
            System.debug('Error getting CC Team Role Id.');
            return null;
        }
        
        //Get Case Team members with "CCed On Email" Role ID
        CaseTeamMember[] CTM = [SELECT id, ParentId, MemberId FROM CaseTeamMember WHERE ParentID = :cs.id AND TeamRoleId in :CTR];
        Set<id> ListOfMember = new Set<id>();
        for(CaseTeamMember a : CTM){
            ListOfMember.add(a.MemberId);
        }
        
        // Get Contacts email team member
        //Contact[] ListOfCC = [SELECT id, email FROM Contact WHERE id in :ListOfMember];
        
        /*for(Contact c : ListOfCC){
            if(c.Email != null){
                cc += c.Email + ';';
            }
        }
        cc = cc.removeEnd(';'); //Remove end ; off of cc*/
        
        cc += caseTemp[0].owner.email;

        if(caseTemp[0].E2CP__AdditionalTo__c != null){
            if(to.equals('mailto:NoEmailOnContactInCase@riverbed.com?'))
                to = 'mailto:NoEmailOnContactInCase@riverbed.com;' + caseTemp[0].E2CP__AdditionalTo__c + '?';
            else
                to = 'mailto:' + caseTemp[0].contact.email + ';' + caseTemp[0].E2CP__AdditionalTo__c + '?';
        }
        
        //Check if there is a web email to include with the generator
        if(String.isNotBlank(webEmail)){
            to = to.removeEnd('?') + ';' + webEmail + '?';
        }
                System.debug('!@# TO EMAIL: ' + to);
        //Check to see if 'support@riverbed.com' is in the "To" field due to E2CP_AdditionalTo__c including it
        if(to.containsIgnoreCase('support@riverbed.com.test1') || Test.isRunningTest()){
            to = to.remove('support@riverbed.com.test1');
            to = to.replace(';;;',';');
            to = to.replace(';;',';');
        }
        System.debug('!@# TO EMAIL: ' + to);
        
        if(cc.contains('@'))
            cc += ';support@riverbed.com.test1';
        else
            cc += 'support@riverbed.com.test1';
        
        /*if(caseTemp[0].E2CP__AdditionalBCC__c != null){
            bcc += ';' + caseTemp[0].E2CP__AdditionalBCC__c;
        }*/
        
        if(caseTemp[0].E2CP__AdditionalCC__c != null){
            cc += caseTemp[0].E2CP__AdditionalCC__c;
        }
        
        if(cc != 'CC='){ //if CC is not empty
            //Creates mail to link
            //link = to + cc + '&' + bcc + '&' + Subject;
            link = to + cc + '&' + Subject;
        }
        else{
            //link = to + bcc + '&' + Subject;
            link = to + '&' + Subject;
        }
        
        System.debug('!@# Link: ' + link);
        return link;
    }
}