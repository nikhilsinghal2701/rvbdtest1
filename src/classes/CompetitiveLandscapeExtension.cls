public with sharing class CompetitiveLandscapeExtension{
    Competitive_Landscape__c cl{get;set;}
    Account_Plan__c ap=null;
    public CompetitiveLandscapeExtension(ApexPages.StandardController controller){
        this.cl = (Competitive_Landscape__c)controller.getRecord();
        ap=[select id, name,AccountId__c from Account_Plan__c where id=:cl.Account_Plan__c];
    }
    //custome save and redirect to account plan page
    public pageReference competitiveLandscapeSave(){
        if(cl!=null){       
            try{
                upsert cl;
            }catch(Exception e){
                system.debug('Error:+e.getMessage()');
            }
            PageReference accPage = new PageReference ('/apex/AccountPlanPage?Id='+ap.Id+'&aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=edit');
            accPage.setRedirect(true);
            return accPage;
        }
        return null;        
    }
    //custome cancel and redirect to account plan page
    public pageReference competitiveLandscapeCancel(){
        PageReference accPage = new PageReference ('/apex/AccountPlanPage?Id='+ap.Id+'&aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=edit');
        accPage.setRedirect(true);
        return accPage;
    }
}