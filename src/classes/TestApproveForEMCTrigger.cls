@isTest
private class TestApproveForEMCTrigger {

    static testMethod void testApproveForEMCTrigger() 
    {
      Account acc = new Account();
	  acc.Name = 'Test';
	  acc.Industry = 'Hi-Tech';
	  insert acc ;
	  
	  Lead l = new Lead();
	  l.FirstName = 'Test Lead';
	  l.LastName = 'LN';
	  l.Company = 'Test Company';
	  l.LeadSource = 'Web';
	  l.Status = 'Open';
	  insert l;
	  
	  try
	  {
	  	Lead updateLead = new Lead(id=l.id, Status = 'Approved', Sold_To_Partner__c ='0017000000WLAyL');
	  	update updateLead;
	  }
	  Catch(Exception e)
	  {
	  	System.Debug('Error => '+e);
	  }
	  
    }
}