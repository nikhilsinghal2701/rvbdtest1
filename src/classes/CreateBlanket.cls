public class CreateBlanket{
    
private ApexPages.StandardController controller {get; set;}
private Quote__c qt {get;set;}
private List<Quote__c> qtList {get;set;}
private Opportunity op {get;set;}
private Discount_Schedule__c ds {get;set;}
private DiscountCategory__c dt {get;set;} 
public String recordId {get;set;}
public String contactId{get;set;}
public ID newRecordId {get;set;}
public String qSchName {get;set;}

Id discCat;
    public CreateBlanket(ApexPages.StandardController controller){
       
       this.controller = controller;
       //categoryIds  =  new Set<Id>();
        // load the current record
        qt = (Quote__c)controller.getRecord();
        detailList = new List<Discount_Detail__c>();
        ds  = new Discount_Schedule__c ();
       
       if(recordId == null){
          recordId = ApexPages.currentPage().getParameters().get('id');
          }
          qtList =[SELECT Blanket__c,opportunity__r.Primary_ISR__c, Deal_Desk_Status__c,Comments__c,
           Discount_Status__c, Discount_Schedule_Created__c, Name,Discount_Reason__c, Opportunity__r.Account.Name,
           Opportunity__r.Sold_to_Partner__c, Opportunity__r.Tier2__c,Opportunity__r.AccountId,
           Opportunity__c, PWS_Quote_Id__c,End_Date__c, Start_Date__c FROM Quote__c 
           where id = :qt.id AND Discount_Status__c = 'Approved' AND Blanket__c = true];   
    }
    
    public void showBlanketName(){
        if(qtList  != null && qtList.size()>0){
            qSchName  = qtList[0].Opportunity__r.Account.Name ;
        }
    
    }
    
    public void createBlanket(){
        displayCond = false;
        discScheduleList = new List<discSchWrapper>();
        if(qtList  != null && qtList.size()>0){
            qSchName  = qtList[0].Opportunity__r.Account.Name ;
        }
    }   
    
    public PageReference autoBlanketCreate(){
        
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        DescribeSObjectResult result = gd.get('Discount_Schedule__c').getDescribe();
        Map<String,Schema.RecordTypeInfo>recordTypeInfo = result.getRecordTypeInfosByName();
        system.debug('##RECORD TYPES:' + recordTypeInfo);
        String dsrecordTypeId = recordTypeInfo.get('Blanket').getRecordTypeId();
        
       Savepoint sp = Database.setSavepoint();
       //Discount_Schedule__c newds;
       List<Discount_Detail__c> ddList = new List<Discount_Detail__c>();
       
       Map<String,Discount_Detail__c>oppDdetailMap=new Map<String,Discount_Detail__c>();
       
       op=[SELECT AccountId,Account.name, Name, Sold_to_Partner__c, Tier2__c,Tier2__r.name, Tier3_Partner__c, Support_Provided__c,Primary_ISR__c FROM Opportunity where id = :qtList[0].Opportunity__c];
       ddList=[SELECT Description__c,Opportunity__c, Name, IsOpp__c, Id, Discount__c, Full_Discount__c,Special_Discount__c,Quote__c,Uplift__c,Disti_Discount__c,Category_Master__c,Category_Master__r.Name,Category_Master__r.Group__c
           //FROM Discount_Detail__c WHERE Opportunity__c =:qt.Opportunity__c and Quote__c=:qt.id and IsOpp__c=True];
           FROM Discount_Detail__c WHERE Opportunity__c =:qtList[0].Opportunity__c];
       DiscountCategory__c dsTable= new DiscountCategory__c();
       
       dsTable.Name = qSchName  ; //op.Account.name+' '+ '-'+' '+op.Tier2__r.name;
       
       for(Discount_Detail__c dd:ddList){
           oppDdetailMap.put(dd.Category_Master__c,dd);
       }
       system.debug(LoggingLevel.INFO,'##oppDdetailMap'+oppDdetailMap);
      try{
           insert dsTable;
           system.debug(LoggingLevel.INFO,'##dsTable'+ dsTable);
           
           Discount_Schedule__c newds=new Discount_Schedule__c() ;
           
           newds.Active__c=True;
           newds.Name = qSchName  ;//op.Account.name+' '+ '-'+' '+op.Tier2__r.name;
           newds.Discount_Type__c='Blanket';
           newds.recordtypeid=dsrecordTypeId;
           newds.Discount_Category__c=dsTable.id;
           newds.ApplyUplift__c = false;
           newds.quote__c=qtList[0].id;
           newds.Reason_Text__c= qtList[0].Id + '\n' +qtList[0].Discount_Reason__c +  '\n'  + qtList[0].Comments__c  ;
           newds.Start_Date__c= System.today();
           //qtList[0].Start_Date__c;
           newds.End_Date__c=qtList[0].End_Date__c;
           newds.ApplyUplift__c=false;
           newds.Discount_Type__c = 'Standard';
           newds.ISR__c=qtList[0].opportunity__r.Primary_ISR__c;
           if(qtList[0].Comments__c  != null)
               newds.Reason_Text__c += qtList[0].Comments__c + '\n' ;
           if(qtList[0].opportunity__r.Sold_to_Partner__c != null)
               newds.Sold_to__c = qtList[0].opportunity__r.Sold_to_Partner__c ;
           if(qtList[0].opportunity__r.Tier2__c != null)
               newds.Tier2__c = qtList[0].opportunity__r.Tier2__c  ;
           if(qtList[0].opportunity__r.AccountId != null)
               newds.Account__c = qtList[0].opportunity__r.AccountId ;
                  
           insert newds;
           
           newRecordId = newds.id;
           
           system.debug(LoggingLevel.INFO,'##ds***:'+newds);
           
           List<Discount_Category_Detail__c> dstDetails = new List<Discount_Category_Detail__c>();
           List<Discount_Category_Detail__c> updstDetails = new List<Discount_Category_Detail__c>();
           dstDetails=[SELECT Active__c, Category__c,Description__c, Discount_Category__c, Discount_Table__c, Name, Id, Registered_Discount__c, Standard_Discount__c FROM Discount_Category_Detail__c where  Discount_Table__c =:dsTable.id];
           system.debug(LoggingLevel.INFO,'##dstDetails***:'+dstDetails); 
           
           for(Discount_Category_Detail__c dtDetails:dstDetails){
               system.debug(LoggingLevel.INFO,'##dtDetails.Category__c:'+ dtDetails.Category__c);
               dtDetails.Standard_Discount__c=oppDdetailMap.get(dtDetails.Category__c)!=null?oppDdetailMap.get(dtDetails.Category__c).Full_Discount__c:null;
               dtDetails.Registered_Discount__c=oppDdetailMap.get(dtDetails.Category__c)!=null?oppDdetailMap.get(dtDetails.Category__c).Special_Discount__c:null;
               updstDetails.add(dtDetails);
           }
           Update updstDetails;
           contactId = newds.id;
           discCat = dsTable.id; 
           //selectDiscSch();
           if(!qtList.isEmpty()){
              for(Discount_Detail__c d : [Select id , Category_Master__c , Category_Master__r.name ,
                                      Special_Discount__c, Quote__c ,Full_Discount__c
                                      from Discount_Detail__c Where
                                       Quote__c = : qtList[0].Id  AND IsOpp__c = false 
                                       Order by Category_Master__r.name]){
          
              detailList.add(d);  
      //        categoryIds.add(d.Category_Master__c);
              
              }
          }
           return page.Blanket;
       }
       catch(Exception ex){
            System.debug('In EXCEPtion---'+ex.getMessage());
            //Database.rollback(sp);
            ApexPages.addMessages(ex);
            return null;
            }
       system.debug('##newRecordId'+newRecordId);
       return null;
       /*PageReference temp = new PageReference('/' + newRecordId);
       temp.setRedirect(true);
       return temp;*/
       //return null;
       //return new PageReference ('/' + newRecordId);
   }
 /*
   Author : Ankita Goel
   Date :   6/23/2015
   Objective : 
 */  
   public boolean displayCond {get; set;}
   public List<discSchWrapper> discScheduleList {get; set;}
   public List<Discount_Detail__c> detailList {get;set;}
   //Set<String> qIds  = new Set<String>();
   public Discount_Schedule__c discSch {get; set;}
   
   public pageReference displayBlanketSchedule(){
       if(qtList != null && qtList.size()== 0){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Create Blanket functionality is available for only Approved discount status Quotes'));
            return null;
       
       }
       
      discScheduleList = new List<discSchWrapper>();   
      Quote__c quote = [ Select Opportunity__r.Account.Name , Name From Quote__c Where Id= :qtList[0].Id ] ;
      String endUserAccName = quote.Opportunity__r.Account.Name+'%';
      system.debug('##end user account name'+endUserAccName);
      Id recTypeId = [ Select SobjectType, Name, Id From RecordType  Where SobjectType='Discount_Schedule__c' And Name = 'Blanket' Limit 1 ].Id ;
      system.debug('##record type Id'+recTypeId);
      for(Discount_Schedule__c d : [Select
       Start_Date__c, Partner_Type__c, Name, Id, End_Date__c, Quote__c,Reason_Text__c,
                                      Discount_Type__c, Discount_Category__c, Blanket_Discount__c 
                                      From Discount_Schedule__c Where RecordTypeId = :recTypeId 
                                      And Name Like :endUserAccName ]){
                                     //discScheduleList.add(d); 
                                      
          discScheduleList.add(new discSchWrapper (d, false));
          
      }
      system.debug('## discount schedule list'+discScheduleList);
      if (discScheduleList != null &&  discScheduleList.size() != 0){
          system.debug('## discScheduleList.size() != 0');
          displayCond = true;
          return null;
      } else if(discScheduleList.size() == 0) {
         system.debug('## discScheduleList.size()==0');
         displayCond = false;
         //autoBlanketCreate();
         showBlanketName();
         //return new PageReference ('/' + newRecordId);
      }
      return null ;
   }
   
   
   public PageReference selectDiscSch(){
      system.debug('## SelectedId '+contactId);
      detailList  = new List<Discount_Detail__c>();
      
      String reasonText;
      //Set<Id> qIds = new Set<Id>();
      Discount_Schedule__c qRec = new Discount_Schedule__c ();
      
      for(discSchWrapper d : discScheduleList){
          if(contactId != null && contactId.equals(d.discSchRec.Id)){
              system.debug('## SelectedId '+d.discSchRec);
              //System.debug('d.discSchRec.Reason_Text__c::::'+d.discSchRec.Reason_Text__c); 
              if(qtList != null && qtList.size()>0){ 
                  d.discSchRec.Reason_Text__c += '\n' + qtList[0].Id + '\n' ;
                  if(qtList[0].Discount_Reason__c != null)
                      d.discSchRec.Reason_Text__c += qtList[0].Discount_Reason__c + '\n' ;
                  if(qtList[0].Comments__c  != null)
                      d.discSchRec.Reason_Text__c += qtList[0].Comments__c + '\n' ;
                  if(qtList[0].Start_Date__c != null)    
                      d.discSchRec.Start_Date__c = System.today();
                  if(qtList[0].End_Date__c != null)        
                      d.discSchRec.End_Date__c = qtList[0].End_Date__c;
                  if(qtList[0].opportunity__r.Primary_ISR__c != null)        
                      d.discSchRec.ISR__c=qtList[0].opportunity__r.Primary_ISR__c;
                  
                  qRec  =  d.discSchRec;
                  if(qtList[0].opportunity__r.Sold_to_Partner__c != null)
                      d.discSchRec.Sold_to__c = qtList[0].opportunity__r.Sold_to_Partner__c ;
                  if(qtList[0].opportunity__r.Tier2__c != null)
                      d.discSchRec.Tier2__c = qtList[0].opportunity__r.Tier2__c  ;
                  if(qtList[0].opportunity__r.AccountId != null)
                      d.discSchRec.Account__c = qtList[0].opportunity__r.AccountId ;
           
                  d.discSchRec.Quote__c  = qtList[0].Id; 
                  if(d.discSchRec.Discount_Category__c != null)
                  discCat = d.discSchRec.Discount_Category__c; 
                  break;
                    
              }
              //System.debug('reasonText::::'+reasonText);  
              //reasonText = d.discSchRec.Reason_Text__c;
              //qIds.add(d.discSchRec.Quote__c );
              
              //qIds.add(d.discSchRec.Quote__c); 
          }
      }
      if(qRec != null && qRec.Id != null){
          update qRec ;
      }
      //List<String> strSplit = new List<String>();
      
      /*if(reasonText  != null){
          update qRec ;
          strSplit  = reasonText.split(';');
          System.debug('reasonText::::'+reasonText);  
          for(String s : strSplit){
              qIds.add(s.SubString(0,15));  
          }
      }*/
      //System.debug('qIds::::'+qIds);
      if(!qtList.isEmpty()){
          for(Discount_Detail__c d : [Select id , Category_Master__c , Category_Master__r.name ,
                                      Special_Discount__c, Quote__c ,Full_Discount__c,Discount__c
                                      from Discount_Detail__c Where
                                       Quote__c = : qtList[0].Id  AND IsOpp__c = false 
                                       Order by Category_Master__r.name]){
                                       
              d.Discount__c = d.Full_Discount__c;
              detailList.add(d);  
      //        categoryIds.add(d.Category_Master__c);
              
          }
      }
     // return Page.Blanket;
      PageReference  ref = new PageReference ('/apex/Blanket');
      return ref;
      //discSch = [ Select Id From Discount_Schedule__c Where Id = :selectedId ];
      //return new PageReference ('/' + selectedId);
      //system.debug('## return pageReference successful');
      //return null;
   }
   
   public PageReference onSave(){
       Map<String, Discount_Detail__c> tempMap = new Map<String, Discount_Detail__c>();
       for(Discount_Detail__c d : [Select id , Category_Master__c , Category_Master__r.name ,
                                      Special_Discount__c, Quote__c ,Full_Discount__c, Discount__c
                                      from Discount_Detail__c Where
                                       Quote__c = : qtList[0].Id AND IsOpp__c = false 
                                       Order by Category_Master__r.name]){
          
              
      //        categoryIds.add(d.Category_Master__c);
              tempMap.put(d.Category_Master__r.Name  , d);  
       }
       system.debug(LoggingLevel.ERROR,'##tempMap'+tempMap );
       Map<String, Decimal> changedRecMap = new Map<String, Decimal>();
       for(Discount_Detail__c d : detailList){
           system.debug(LoggingLevel.ERROR,'##Record'+d);
           if(tempMap != null && tempMap.containsKey(d.Category_Master__r.Name)){
               Discount_Detail__c  tempRec = tempMap.get(d.Category_Master__r.Name);  
               
               Decimal oldval = tempRec.Special_Discount__c != null ? tempRec.Special_Discount__c : tempRec.Discount__c;
               Decimal val = d.Special_Discount__c != null ? d.Special_Discount__c : d.Discount__c;
               if(val != null && oldval != null && oldval  != val){
                   changedRecMap.put(d.Category_Master__r.Name, val); 
               }
               system.debug(LoggingLevel.ERROR,'##tempRec '+tempRec +'oldval :::'+oldval +'val :::'+val );
                          
           }
       }
       system.debug(LoggingLevel.ERROR,'##changedRecMap'+changedRecMap);
       system.debug(LoggingLevel.ERROR,'##discCat '+discCat );
       List<Discount_Category_Detail__c> detailList = new List<Discount_Category_Detail__c>();
      
       for(Discount_Category_Detail__c d : [Select id , Category__c, Category__r.Name, Registered_Discount__c 
       from Discount_Category_Detail__c Where  Discount_Table__c  = :discCat ]){
           if(changedRecMap != null && changedRecMap.containsKey(d.Category__r.Name)){
               Decimal val = changedRecMap.get(d.Category__r.Name);
               if(val != null){
                   d.Registered_Discount__c  = val;
                   d.Standard_Discount__c = val;
                   detailList.add(d); 
               }
           }
       }
       system.debug(LoggingLevel.INFO,'##detailList'+detailList);
       if(detailList != null && detailList.size()>0){
           update detailList;
       }
       PageReference  ref = new PageReference ('/'+contactId);
       return ref;
   }
   
   public class discSchWrapper{
   
   
      public Discount_Schedule__c discSchRec{get;set;}
      public Boolean isChecked{get;set;}
      
      public discSchWrapper(Discount_Schedule__c discSchRec, Boolean isChecked){
          this.discSchRec = discSchRec;
          this.isChecked = false;
      
      }
      
   }
}