/* Class: CertRollupBatchHelper
	Purpose: to roll up CErtification count to the account hirearchy from partnercompetency records.
		Note: this class takes care of only three levels of account hirearchy
	Author: Jaya ( Perficient )
	Created Date: 8/05/2014					
	Reference: pc=> Partner Competency	
*/
public without sharing class CertRollupBatchHelper {

	public static Map<String,Acct_Cert_Summary__c> certRollup(Database.BatchableContext bcontext, List<sObject> scope, Map<String,Acct_Cert_Summary__c> globalPartnerCompetency)
	{	
		List<Acct_Cert_Summary__c> primaryCompetencyList = (List<Acct_Cert_Summary__c>)scope;
		Map<String,Acct_Cert_Summary__c> localPartnerCompetencyMap = new Map<String,Acct_Cert_Summary__c>();
        List<Acct_Cert_Summary__c> partnerCompetencyUpdateList = new List<Acct_Cert_Summary__c>();
        
        System.debug('primaryCompetencyList:'+primaryCompetencyList);
        
        Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
       
        for(Acct_Cert_Summary__c pc: primaryCompetencyList){
            // Parent account of the current Partner Competency
            String key = pc.TAP__c+'|'+pc.Account__c;
            Acct_Cert_Summary__c temp_PC = new Acct_Cert_Summary__c();
            if(globalPartnerCompetency.containsKey(key)){
            	temp_PC = globalPartnerCompetency.get(key);
            	temp_PC.Id = pc.Id;
            	temp_PC.Roll_Up_RSS__c = CertRollup.isNullCheck(temp_PC.Roll_Up_RSS__c) + CertRollup.isNullCheck(pc.RSS__c);
            	temp_PC.Roll_Up_RSA__c = CertRollup.isNullCheck(temp_PC.Roll_Up_RSA__c) + CertRollup.isNullCheck(pc.RSA__c);
            	temp_PC.Roll_Up_RTSA__c = CertRollup.isNullCheck(temp_PC.Roll_Up_RTSA__c) + CertRollup.isNullCheck(pc.RTSA__c);
            	temp_PC.Roll_Up_RTSS__c = CertRollup.isNullCheck(temp_PC.Roll_Up_RTSS__c) + CertRollup.isNullCheck(pc.RTSS__c);
            	
            }else{
            	temp_PC.Id = pc.Id;
                temp_PC.Roll_Up_RSS__c = pc.RSS__c;
                temp_PC.Roll_Up_RSA__c = pc.RSA__c;
                temp_PC.Roll_Up_RTSA__c = pc.RTSA__c;
                temp_PC.Roll_Up_RTSS__c = pc.RTSS__c;
            }
            localPartnerCompetencyMap.put(key,temp_PC);
            globalPartnerCompetency.put(key,temp_PC);
            System.debug('localPartnerCompetencyMap:'+localPartnerCompetencyMap);
            // if current PC's account has a parent account then add the current PC's cert values to the parent account
            // in globalPartnerCompetency
            //&& !pc.Account__r.Parent.Do_not_Auto_Level__c is not needed roll up certs
            if(pc.Account__r.ParentId != null && !pc.Account__r.Parent.Do_not_rollup_Certs__c ){
                String parentKey = pc.TAP__c+'|'+pc.Account__r.ParentId;
                Acct_Cert_Summary__c temp_PPC = new Acct_Cert_Summary__c();
                if(globalPartnerCompetency.containsKey(parentKey)){
                    temp_PPC = globalPartnerCompetency.get(parentKey);
                    // add all the roll up certs of the current pc to temp__pc
                    temp_PPC.Roll_Up_RSS__c = CertRollup.isNullCheck(temp_PPC.Roll_Up_RSS__c) + CertRollup.isNullCheck(pc.RSS__c);
            		temp_PPC.Roll_Up_RSA__c = CertRollup.isNullCheck(temp_PPC.Roll_Up_RSA__c) + CertRollup.isNullCheck(pc.RSA__c);
            		temp_PPC.Roll_Up_RTSA__c = CertRollup.isNullCheck(temp_PPC.Roll_Up_RTSA__c) + CertRollup.isNullCheck(pc.RTSA__c);
            		temp_PPC.Roll_Up_RTSS__c = CertRollup.isNullCheck(temp_PPC.Roll_Up_RTSS__c) + CertRollup.isNullCheck(pc.RTSS__c);
                }
                else{
                	temp_PPC.Roll_Up_RSS__c = CertRollup.isNullCheck(pc.RSS__c);
            		temp_PPC.Roll_Up_RSA__c = CertRollup.isNullCheck(pc.RSA__c);
            		temp_PPC.Roll_Up_RTSA__c = CertRollup.isNullCheck(pc.RTSA__c);
            		temp_PPC.Roll_Up_RTSS__c = CertRollup.isNullCheck(pc.RTSS__c);
                }
                globalPartnerCompetency.put(parentKey,temp_PPC);
                if(temp_PPC.id != null){
                	localPartnerCompetencyMap.put(parentKey,temp_PPC);
                }
                System.debug('localPartnerCompetencyMap:'+localPartnerCompetencyMap);
                // GrandParent account of the current PC
                // if current PC's account has a Grandparent account then add the current PC's cert values to the Grandparent account
                // in globalPartnerCompetency
                // && !pc.Account__r.Parent.Parent.Do_not_Auto_Level__c is not needed for roll up certs
                if(pc.Account__r.Parent.ParentId != null && !pc.Account__r.Parent.Parent.Do_not_rollup_Certs__c ){
                    String grandParentKey = pc.TAP__c+'|'+pc.Account__r.Parent.ParentId;
                    Acct_Cert_Summary__c temp_GPPC = new Acct_Cert_Summary__c();
                    if(globalPartnerCompetency.containsKey(grandParentKey)){
                        temp_GPPC = globalPartnerCompetency.get(grandParentKey);
                        // add all the roll up certs of the current pc to temp pc
                        temp_GPPC.Roll_Up_RSS__c = CertRollup.isNullCheck(temp_GPPC.Roll_Up_RSS__c) + CertRollup.isNullCheck(pc.RSS__c);
            			temp_GPPC.Roll_Up_RSA__c = CertRollup.isNullCheck(temp_GPPC.Roll_Up_RSA__c) + CertRollup.isNullCheck(pc.RSA__c);
            			temp_GPPC.Roll_Up_RTSA__c = CertRollup.isNullCheck(temp_GPPC.Roll_Up_RTSA__c) + CertRollup.isNullCheck(pc.RTSA__c);
            			temp_GPPC.Roll_Up_RTSS__c = CertRollup.isNullCheck(temp_GPPC.Roll_Up_RTSS__c) + CertRollup.isNullCheck(pc.RTSS__c);
                    }
                    else{
                    	temp_GPPC.Roll_Up_RSS__c = CertRollup.isNullCheck(pc.RSS__c);
            			temp_GPPC.Roll_Up_RSA__c = CertRollup.isNullCheck(pc.RSA__c);
            			temp_GPPC.Roll_Up_RTSA__c = CertRollup.isNullCheck(pc.RTSA__c);
            			temp_GPPC.Roll_Up_RTSS__c = CertRollup.isNullCheck(pc.RTSS__c);
                    }
                    globalPartnerCompetency.put(grandParentKey,temp_GPPC);
                    if(temp_GPPC != null){
                    	localPartnerCompetencyMap.put(grandParentKey,temp_GPPC);
                    }
                   System.debug('localPartnerCompetencyMap:'+localPartnerCompetencyMap);
                }
                
            }
            System.debug('globalPartnerCompetency size:'+globalPartnerCompetency.size());
        }
        System.debug('localPartnerCompetencyMap:'+localPartnerCompetencyMap);
       if(!localPartnerCompetencyMap.isEmpty()){
            for(Acct_Cert_Summary__c pc: localPartnerCompetencyMap.values()){
                if(pc.Id != null){
                    partnerCompetencyUpdateList.add(pc);
                }
            }
            System.debug('partnerCompetencyUpdateList:'+partnerCompetencyUpdateList);
            Constant.disablePartnerCompetencyTrigger = true;
            CertRollup.ROLL_UP = true;
            update partnerCompetencyUpdateList;
             Database.SaveResult[] updateResults = Database.update(partnerCompetencyUpdateList,false);
             List<Database.Error> dbErrors = new List<Database.Error>();
             List<Error_Log__c> errorLogsList = new List<Error_Log__c>();
            // Process the save results
            Integer i = 0;
            for(Database.SaveResult sr: updateResults)
            {
                if(!sr.isSuccess())
                {
                     Database.Error dbErr = sr.getErrors()[0];
                     Error_Log__c err = CompetencyRollDown.logError(partnerCompetencyUpdateList.get(i).Id,dbErr.getMessage(),'CertRollupBatch class',bcontext.getJobId());
	                 errorLogsList.add(err); 
                }
                i++;
            }
            if(!errorLogsList.isEmpty()){
                insert errorLogsList;
            }
        }
        return globalPartnerCompetency;
	}
	
	public static Set<Id> getRelatedAccounts(Set<Id> acctIds){
		Set<Id> relatedacctIds = new Set<Id>();
		for(Account acct: [SELECT Id,ParentId,Parent.ParentId FROM Account
												WHERE Id IN :acctIds OR ParentId IN: acctIds OR Parent.ParentId IN: acctIds])
		{
			relatedacctIds.add(acct.Id);
		}
		return relatedacctIds;
	}
}