/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
@IsTest(seeAllData=true)
private class testWWHQ {
    public static List<WW_HQ__c> WWHQs;
    public static WW_HQ__c WWHQ;
    
    public static List<Account> AccountList;
    public static Account a;
    
    public static List<Contact> ContactList;
    public static Contact c;
    
    public static List<Lead> LeadList;
    public static Lead l;
    
    public static List<Manticore_Marketing_Activity_History__c> MarketingList;
    public static Manticore_Marketing_Activity_History__c m;
    
    public static Asset asst;
    public static Case cs;
    
    public static Task t1;
    public static Task t2;
    public static Task t3;
    
    public static Opportunity o1;
    public static Opportunity o2;
    public static Opportunity o3;
    
    public static Product2 p;
    public static PricebookEntry pbe;
    public static Pricebook2 pb;
    
    public static User currentUser;
    
    static {
        currentUser = [SELECT Id, Region__c, Geo__c FROM User WHERE (Id = :UserInfo.getUserId())];
            currentUser.Geo__c = 'Test';
            currentUser.Region__c = 'Test';
        update currentUser;
        
        WWHQ = new WW_HQ__c();
            WWHQ.Name = 'Test WWHQ';
            WWHQ.Latitude__c = 10;
            WWHQ.Longitude__c = 10;
            WWHQ.GlobalRank__c = 10;
            WWHQ.AmountClosedOpportunities__c = 10;
            WWHQ.AmountOpenOpportunities__c = 10;
            //WWHQ.Rev_Potential_Remain__c = 10;
            //WWHQ.Category__c = 'Test';
            WWHQ.LastClosedOpportunity__c = Date.Today().addDays(-1);
            WWHQ.NextRenewalDate__c = Date.Today().addDays(1);
            WWHQ.RiverbedProductsOwned__c = 'Test';
        insert WWHQ;
        WWHQs = new List<WW_HQ__c>{WWHQ};
        
        a = new Account();
            a.Name = 'Test Account';
            a.WW_HQ__c = WWHQ.Id;
            a.Riverbed_Products_Owned__c = 'Test';
            a.Cloud_Providers__c = 'Test';
            a.Key_Applications__c = 'Test';
            a.Network_Hardware__c = 'Test';
            a.Network_Management__c = 'Test';
            a.Storage_Hardware__c = 'Test';
            a.Storage_Management__c = 'Test';
            a.National_VAR__c = 'Test';
            a.SI__c = 'Test';
            a.SI_Comments__c = 'Test';
            a.SP__c = 'Test';
            a.SP_Comments__c = 'Test';
            a.Reference_Private__c = 'Test';
            a.Reference_Public__c = 'Test';
            
        insert a;
        AccountList = new List<Account>{a};
        
        c = new Contact();
            c.LastName = 'Test 1';
            c.AccountId = a.Id;
            c.Support_Role__c = 'Support Admin';
            c.zContact_DateFlex_1__c = Date.Today();
        insert c;
        ContactList = new List<Contact>{c};
        
        l = new Lead();
            l.LastName = 'Test 2';
            l.Company = 'Test Company';
            l.WW_HQ__c = WWHQ.Id;
            l.Email = 'ctadmin@cloudtrigger.com';
            l.Phone = '555-555-1212';
            l.Fax = '555-555-1234';
        insert l;
        LeadList = new List<Lead>{l};
        
        pb = [SELECT Id FROM Pricebook2 WHERE (IsStandard = true)];
        p = new Product2(Name = 'Test Product', ProductCode = 'CAA', Desupport_Date__c = Date.Today());
        insert p;
        //system.debug('Product Id:'+p.Id);
        pbe = new PricebookEntry(Product2Id = p.Id, Pricebook2Id = pb.Id, UnitPrice = 10.0);
        insert pbe;
        //system.debug('PricebookEntry Id:'+pbe.Id);
        asst = new Asset();
            asst.Name = 'Test Asset';
            asst.SKU__c='CAS-VE';
            asst.AccountId = a.Id;
            asst.Instance_Number__c = '1';
            asst.Product2Id = p.Id;
            asst.Status = 'ACTIVE';
        insert asst;
        
        t1 = new Task();
            t1.WhatId = a.Id;
            //t1.WhoId = l.Id;
            t1.ActivityDate = Date.Today();
        insert t1;
        
        t2 = new Task();
            //t2.WhatId = a.Id;
            t2.WhoId = l.Id;
            t2.ActivityDate = Date.Today();
        insert t2;
        
        t3 = new Task();
            //t2.WhatId = a.Id;
            t3.WhoId = c.Id;
            t3.ActivityDate = Date.Today();
        insert t3;
        
        cs = new Case();
            cs.AccountId = a.Id;
            cs.ContactId = c.Id;
        insert cs;
        
        DiscountApproval.isTest = true;

        List<Discount_Schedule__c> rules = new List<Discount_Schedule__c>();
        for (Discount_Schedule__c rule : [SELECT Id,Active__c FROM Discount_Schedule__c limit 200]) {
            rule.Active__c = false;
            rules.add(rule);
        }
        update rules;
        
        o1 = new Opportunity();
            o1.Name = 'Test Opportunity';
            o1.StageName = '6 - Order Accepted';
            o1.CloseDate = Date.Today();
            o1.AccountId = a.Id;
            o1.Amount = 10;
        insert o1;
        DiscountApproval.run = false;//added by Ankita 2/23/2012
        
        o2 = new Opportunity();
            o2.Name = 'Test Opportunity';
            o2.StageName = '7 - Closed (Not Won)';
            o2.CloseDate = Date.Today();
            o2.Amount = 10;
            o2.AccountId = a.Id;
            o2.Reason_Lost__c='Duplicate';
            o2.Why_did_we_win_lose__c = 'Test Opportunity';
        insert o2; 
        
        DiscountApproval.run = false;//added by Ankita 2/23/2012
        o3 = new Opportunity();
            o3.Name = 'Test Opportunity';
            o3.StageName = '4 - Selected';
            o3.CloseDate = Date.Today();
            o3.AccountId = a.Id;
            o3.Amount = 10;
        insert o3;
        
        m = new Manticore_Marketing_Activity_History__c();
            m.Account__c = a.Id;
            m.Contact__c = c.Id;
            m.EventDateTime__c = DateTime.Now();
            m.EventDescription__c = 'Test';
            m.Name = 'Test Event';
            m.ActivityType__c = 'Web Visit';
        insert m;
    }
    
    public static testMethod void testWWHQMap1() {
        PageReference testPage = Page.WWHQMap;
        Test.setCurrentPage(testPage);
        
        Test.startTest();
        
        WWHQMapController controller = new WWHQMapController();
        
        User tempU = controller.getOwnerSelect();
        tempU.Geo__c = 'Test';
        tempU.Region__c = 'Test';
        
        WW_HQ__c temp = controller.getWWHQTemp();
        temp.LastClosedOpportunity__c = Date.Today();
        temp.NextRenewalDate__c = Date.Today();
        temp.RiverbedProductsOwned__c = 'Test';
        
        controller.category = 'None';
        controller.currentUser = false;
        controller.fromOO = '5';
        controller.fromOpp = '5';
        controller.fromRank = '5';
        controller.fromRP = '5';
        controller.toOO = '15';
        controller.toOpp = '15';
        controller.toRank = '15';
        controller.toRP = '15';
        controller.hasOpenOpps = 'false';

        controller.lastClosedDirection = '<';
        controller.nextRenewalDirection = '>';
        
        controller.Filter();
        System.Debug(LoggingLevel.Debug, WWHQ);
        List<WWHQmapController.HQInfo> HQs = controller.HQ;
        
        for (WWHQmapController.HQInfo HQ : HQs) {
            String AccountName = HQ.AccountName;
            String Color = HQ.Color;
            String PinText = HQ.PinText;
            String PinType = HQ.PinType;
            String LifetimeSpend = HQ.LifetimeSpend;
            String OpenOpps = HQ.OpenOpps;
        }
        
        Test.stopTest();
    }
    
    public static testMethod void testWWHQMap2() {
        PageReference testPage = Page.WWHQMap;
        Test.setCurrentPage(testPage);
        
        Test.startTest();
        
        WWHQMapController controller = new WWHQMapController();
        
        User tempU = controller.getOwnerSelect();
        tempU.Geo__c = null;
        tempU.Region__c = null;
        
        WW_HQ__c temp = controller.getWWHQTemp();
        //temp.LastClosedOpportunity__c = Date.Today();
        //temp.NextRenewalDate__c = Date.Today();
        //temp.RiverbedProductsOwned__c = 'Test';
        
        //controller.category = 'None';
        controller.currentUser = false;
        
        //controller.Filter();
        System.Debug(LoggingLevel.Debug, WWHQ);
        List<WWHQmapController.HQInfo> HQs = controller.HQ;
        
        for (WWHQmapController.HQInfo HQ : HQs) {
            String AccountName = HQ.AccountName;
            String Color = HQ.Color;
            String PinText = HQ.PinText;
            String PinType = HQ.PinType;
            String LifetimeSpend = HQ.LifetimeSpend;
            String OpenOpps = HQ.OpenOpps;
        }
        
        Test.stopTest();
    }
    
    public static void testTasks(WWHQViewController controller) {
        controller.sortExpression = 'name';
        String sortExpression = controller.sortExpression;
        String sortDirection = controller.getSortDirection();
        controller.setSortDirection('ASC');
        
        //controller.viewData();
        
        Integer AccountSize = controller.AccountsSize;
        List<WWHQViewController.AccountInfo> AccountInfos = controller.Accounts;
        
        for (WWHQViewController.AccountInfo ai : AccountInfos) {
            String name = ai.AccountName;
            Integer ContactSize = ai.ContactsSize;
            Integer ProductSize = ai.ProductSize;
            
            Integer OpenCasesSize = ai.OpenCasesSize;
            
            /*
            Integer WonOpportunitesSize = ai.WonOpportunitiesSize;
            Decimal WonOpportunitiesAmount = ai.WonOpportunitiesAmount;
            String WonOpportunitiesAmountFormatted = ai.WonOpportunitiesAmountFormatted;
            Integer OpenOpportunitiesSize = ai.OpenOpportunitiesSize;
            Decimal OpenOpportunitiesAmount = ai.OpenOpportunitiesAmount;
            String OpenOpportunitiesAmountFormatted = ai.OpenOpportunitiesAmountFormatted;
            
            Integer OpportunitiesSize = ai.OpportunitiesSize;
            */
            
            String pinText = ai.PinText;
            String pinType = ai.PinType;
            String color = ai.Color;
        }
        
        Integer ContactSize = controller.ContactsSize;
        List<Contact> Contacts = controller.Contacts;
        
        Integer AssetSize = controller.AssetsSize;
        List<WWHQViewController.AssetInfo> Assets = controller.Assets;
        
        for (WWHQViewController.AssetInfo ai : Assets) {
            String Color = ai.Color;
            String status = ai.Status;
        }
        
        //Integer OpportunitySize = controller.OpportunitiesSize;
        //List<WWHQViewController.OpportunityInfo> Opportunities = controller.Opportunities;
        
        Integer OpenOpportunitiesSize = controller.OpenOpportunitiesSize;
        Decimal OpenOpportunitiesAmount = controller.OpenOpportunitiesAmount;
        List<WWHQViewController.OpportunityInfo> OpenOpportunities = controller.OpenOpportunities;
        
        Integer LostOpportunitiesSize = controller.LostOpportunitiesSize;
        List<WWHQViewController.OpportunityInfo> LostOpportunities = controller.LostOpportunities;
        
        Integer WonOpportunitiesSize = controller.WonOpportunitiesSize;
        Decimal WonOpportunitiesAmount = controller.WonOpportunitiesAmount;
        List<WWHQViewController.OpportunityInfo> WonOpportunities = controller.WonOpportunities;
        
        Integer LeadSize = controller.LeadsSize;
        List<Lead> Leads = controller.Leads;
        
        Integer EntrypointSize = controller.EntrypointSize;
        List<Lead> Entrypoints = controller.Entrypoints;
        
        Integer RecentCaseSize = controller.RecentCasesSize;
        List<Case> RecentCases = controller.RecentCases;
        
        Integer TaskSize = controller.TasksSize;
        List<WWHQViewController.TaskInfo> Tasks = controller.Tasks;
        
        Integer MarketingSize = controller.MarketingSize;
        List<Manticore_Marketing_Activity_History__c> Marketing = controller.MarketingHistory;
        
        String LeadReport = controller.LeadReport;
        String OppReport = controller.OppReport;
        String AccountReport = controller.AccountReport;
        String ContactReport = controller.ContactReport;
        String AssetReport = controller.AssetReport;
        
        controller.doEdit();
        controller.doSave();
    }
    
    public static testMethod void testWWHQView1() {
        PageReference testPage = Page.WWHQView;
            testPage.getParameters().put('id', WWHQ.Id);
        Test.setCurrentPage(testPage);
        
        Test.startTest();
        
        WWHQViewController controller = new WWHQViewController(new ApexPages.StandardController(WWHQ));
        testTasks(controller);
        
        Test.stopTest();
    }
    
    public static testMethod void testWWHQView2() {
        PageReference testPage = Page.WWHQView;
            testPage.getParameters().put('aid', a.Id);
            testPage.getParameters().put('id', WWHQ.Id);
        Test.setCurrentPage(testPage);
        
        Test.startTest();
        
        WWHQViewController controller = new WWHQViewController(new ApexPages.StandardController(WWHQ));
        testTasks(controller);
        
        Test.stopTest();
    }
    
    public static testMethod void testSchedulers1() {
        Schedulable scheduler;
        
        Test.StartTest();
        
        scheduler = new SchedulerAccountContactUpdate();
        scheduler.execute(null);
        
        scheduler = new SchedulerWWHQAssignLeads();
        scheduler.execute(null);
        
        Test.StopTest();
    }
    
    public static testMethod void testSchedulers2() {
        Schedulable scheduler;
        
        Test.StartTest();
        
        scheduler = new SchedulerWWHQUpdates();
        scheduler.execute(null);
        
        scheduler = new SchedulerOpportunityUpdates();
        scheduler.execute(null);
        
        Test.StopTest();
    }
    
    public static testMethod void testSchedulers3() {
        Schedulable scheduler;
        
        Test.StartTest();
        
        scheduler = new SchedulerOpportunityCountUpdates();
        scheduler.execute(null);
        
        Test.StopTest();
    }
    
    public static testMethod void testSchedulerWWHQUpdates() {
        Test.startTest();
        BatchWWHQUnions bwu = new BatchWWHQUnions() ;
        bwu.Query = bwu.Query + ' LIMIT 200';
        bwu.start(null);
        bwu.execute(null, WWHQs);
        
        BatchWWHQStatistics bws = new BatchWWHQStatistics() ;
        bws.Query = bws.Query + ' LIMIT 200';
        bws.start(null);
        bws.execute(null, WWHQs);
     
        Test.stopTest();
    }
    
    public static testMethod void testSchedulerWWHQAssignLeads() {
        Test.startTest();
        
        BatchAssignLead bal = new BatchAssignLead(true) ;
        bal.start(null);   
        bal.execute(null, LeadList);
     
        Test.stopTest();
    }
    
    public static testMethod void testSchedulerAccountContactUpdate() {
        Test.startTest();        
        BatchContactScoring bcs = new BatchContactScoring() ;
        bcs.Query = bcs.Query + ' LIMIT 200';
        ID batchprocessid = Database.executeBatch(bcs);     
        Test.stopTest();
    }
    
     public static testMethod void testBatchAccountRollups() {
        Test.startTest();        
        BatchAccountRollups bar = new BatchAccountRollups();
        bar.Query = bar.Query + ' LIMIT 200';
        ID batchprocessid = Database.executeBatch(bar);
        Test.stopTest();
    }
    
    public static testMethod void testBatchOpportunityRollup() {
        Test.startTest();        
        BatchOpportunityRollup bor = new BatchOpportunityRollup();
        bor.Query = bor.Query + ' LIMIT 200';
        ID batchprocessid = Database.executeBatch(bor); 
        Test.stopTest();
    }
}