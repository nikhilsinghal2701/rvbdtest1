@IsTest
public class TestAQIRQIUtils {

	public static final String TEST_COACH_LOGIN = 'hpremkumar@riverbed.com.test';
	public static final String TEST_COACH_NAME = 'Harit Premkumar';
	public static final String TEST_COACH_TEAM = 'HARIT_COACH_TEAM';
		
	public static final String TEST_ADMIN_LOGIN = 'jsumi@riverbed.com.test';
	public static final String TEST_ADMIN_NAME = 'Jeffery Sumi';
	public static final String TEST_ADMIN_TEAM = 'JEFF_SUMI_COACH_TEAM';
	
	public static final String TEST_MEMBER_LOGIN = 'bnguyen@riverbed.com.test';
	public static final String TEST_MEMBER_NAME = 'Ben Nguyen';
	public static final String TEST_MEMBER_TEAM = 'HARIT_COACH_TEAM';
	    
    public User createMember() {
		User member;				
		member = new User(FirstName = 'Ben', LastName = 'Nguyen', Username = TEST_MEMBER_LOGIN, Email = TEST_MEMBER_LOGIN, 
			Alias = 'agent', TimeZoneSidKey = 'America/Los_Angeles', ProfileId = '00e50000000uVEg', KCS_Coach__c = false, KCS_Knowledge_Administrator__c = false,
			KCS_Engineer__c = true, KCS_Work_Team__c = TEST_MEMBER_TEAM, EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',Internal_Department__c='283 APM Support', Segment__c='RVBD Operating Unit - US');		
			insert member;
		return member;
	}
	
	public User createAdmin() {
		User admin;
		admin = new User(FirstName = 'Jeffery', LastName = 'Sumi', Username = TEST_ADMIN_LOGIN, Email = TEST_ADMIN_LOGIN, 
			Alias = 'agent', TimeZoneSidKey = 'America/Los_Angeles', ProfileId = '00e50000000uVEy', KCS_Coach__c = true, KCS_Knowledge_Administrator__c = true,
			KCS_Engineer__c = true, KCS_Work_Team__c = TEST_ADMIN_TEAM, EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',Internal_Department__c='220 Support Operations',Segment__c='RVBD Operating Unit - US');
			insert admin;
		return admin;
	}
	
	public User createCoach() {
		User coach;
		coach = new User(FirstName = 'Harit', LastName = 'Premkumar', Username = TEST_COACH_LOGIN, Email = TEST_COACH_LOGIN, 
			Alias = 'agent', TimeZoneSidKey = 'America/Los_Angeles', ProfileId = '00e50000000uVEg', KCS_Coach__c = true, KCS_Knowledge_Administrator__c = false,
			KCS_Engineer__c = true, KCS_Work_Team__c = TEST_COACH_TEAM, EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',Internal_Department__c='283 APM Support',Segment__c='RVBD Operating Unit - US');
			insert coach;
		return coach;
	}
	

	public InQuira_Article_Info__c createArticle() {
		InQuira_Article_Info__c article = new InQuira_Article_Info__c(Article_Type__c = 'IM', Article_Author__c = TEST_MEMBER_NAME, Display_URL__c ='https://www.supportkb.riverbed.com',
			Document_ID__c ='S54612', Excerpt__c='Test Test Test', Title__c ='Test Article');
		return article;
	}
	
	public InQuira_Case_Info__c createCaseInfo() {
		InQuira_Case_Info__c caseInfo = new InQuira_Case_Info__c();
		return caseInfo;
	}
	
	public List<Case> sixCasesForRQI() {
		List<Case> c = new List<Case>();
		
		for (Integer i = 0; i < 5; i++) {
			Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww' + i);
        insert cObj;
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test' + i;
        cObj.Environment_Details__c = 'Test' + i;
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test' + i;
        cObj.Issue_First_Seen__c = 'Test' + i;
        cObj.Problem_Definition__c = 'Test' + i;
        cObj.Problem_Details__c = 'Test' + i;
        cObj.Recent_Changes__c = 'Test' + i;
        cObj.Relevant_Data__c = 'Test' + i;
        cObj.Reproducibility__c = 'Test' + i;
        cObj.Version__c = 'Test' + i;
        cObj.Staff_Engineer__c = 'Chriss Gearyy' + i;
        update cObj;
        
        cObj.Re_escalation_Reason__c = 'Test' + i;
        cObj.Current_Status__c = 'Test' + i;
        cObj.Status = 'Closed';
        update cObj;
        c.add(cObj);
		}
		
		return c;
	}
}