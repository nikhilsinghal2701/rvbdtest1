public class extServerDateRT{

    //fields
    private final Case myCase;
    
    //constructor
    public extServerDateRT(ApexPages.StandardController stdController) {
        this.myCase = (Case)stdController.getRecord();
    }
    
    public PageReference updateField() {
        String strResponse = '';
        DateTime d = System.now();
        //get case
        Case c = [select Id,Case_Resolution__c,IsClosed from Case where id = :myCase.id limit 1];
        
        //update case
        if (c.IsClosed == false){
            c.Case_Resolution__c = d;
            update c;
            PageReference pageRef = Page.E2CP__New_Comment;
            pageRef.getParameters().put('id', c.Id);
            pageRef.setRedirect(true);
            return pageRef;
        } else {
            return null;
        }
       
    }
    
    static testMethod void extServerDateRTTest() {
        
        Case c = new Case();

        insert c;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        extServerDateRT esdrt = new extServerDateRT(stdController);
        PageReference pg;
        
        //case is open
        pg = esdrt.updateField();
        System.Assert(pg != null);
        System.Assert([select Case_Resolution__c from case where id=:c.id][0].Case_Resolution__c != null);
        
        //case is closed
        c.Status = [select MasterLabel from casestatus where isclosed = true][0].MasterLabel;
        c.Version__c='1.0';
        update c;
        pg = esdrt.updateField();
        System.Assert(pg == null);

     }
    
}