@isTest
private class TestRQIAQIForm {
	
    static testMethod void testRQIAQIForm() {
    	RQI_Audit__c testObj = new RQI_Audit__c();
    	ApexPages.StandardController sc = new ApexPages.StandardController(testObj);
        RqiAqiForm RAF = new RqiAqiForm(sc);
        TestAQIRQIUtils testTools = new TestAQIRQIUtils();
        
        User engineer = testTools.createMember();
        User coach = testTools.createCoach();
        User KCSAdmin = testTools.createAdmin();
        Date testDate = Date.today();
        
        Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
               Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
                
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        cObj.Case_Owner__c = engineer.Id;
        cObj.OwnerId = engineer.Id;
        update cObj;
        
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed - Resolved';
        update cObj;
      	
      	cObj = [SELECT Id, isClosed, Case_Owner__c, OwnerId, ClosedDate,Status FROM Case WHERE Id =:cObj.Id];
        RAF.curUserId = [SELECT Id, KCS_Coach__c,KCS_Knowledge_Administrator__c,KCS_Work_Team__c 
        FROM User WHERE KCS_Knowledge_Administrator__c = true LIMIT 1];
        RAF.caseOwner.Case_Owner__c = engineer.Id;
        
        //returning cases for RQI      
        ApexPages.currentPage().getParameters().put('casesorarticles','retCases');
    	ApexPages.currentPage().getParameters().put('datestart',testDate.format());
    	ApexPages.currentPage().getParameters().put('dateend',testDate.addDays(2).format());
    	
    	//testing > 6 cases for RQI
    	RAF.valuesFromUser();
    	RAF.getcaseList();
    	//System.assert(RAF.getcaseList().size() > 0);
    	RAF.setupCasesOrArticles();
		
    	
    	// test cases returned range from datestart - dateend
    	List<Case> rqiCaseList = RAF.getCaseList();
    	Date dStart = Date.parse(RAF.dateRange1);
    	Date dEnd = Date.parse(RAF.dateRange2);
    	for (Case c : rqiCaseList) { 		
    		System.assert(c.ClosedDate >= dStart);
    		System.assert(c.ClosedDate <= dEnd);    		
    	}
     	
        //testing < 6 cases
    	RAF.valuesFromUser();
    	
    	
    	//test Coach
    	RqiAqiForm CoachTest = new RqiAqiForm(sc);
    	CoachTest.curUserId = [SELECT Id, KCS_Coach__c,KCS_Knowledge_Administrator__c,KCS_Work_Team__c FROM User 
    							WHERE KCS_Coach__c = true AND Id != :KCSAdmin.Id LIMIT 1];
    	CoachTest.getTeamMembers();
		List<User> kav = [SELECT Id From User WHERE Id IN :CoachTest.teamMembers.keyset()];
		CoachTest.caseOwner.Case_Owner__c = kav[0].Id;
		
		ApexPages.currentPage().getParameters().put('casesorarticles','retCases');
    	ApexPages.currentPage().getParameters().put('datestart',testDate.format());
    	ApexPages.currentPage().getParameters().put('dateend',testDate.addDays(2).format());
    	
    	CoachTest.valuesFromUser();
    	CoachTest.getcaseList();
    	//System.assert(CoachTest.getcaseList().size() > 0);
    	  	
    	
    	//prev 2 months
    	//testing no cases
    	ApexPages.currentPage().getParameters().put('casesorarticles','retCases');
    	ApexPages.currentPage().getParameters().put('datestart','');
    	ApexPages.currentPage().getParameters().put('dateend','');
    	RAF.valuesFromUser();
    	
    	// test cases return range
    	rqiCaseList = RAF.getCaseList();
    	for (Case c : rqiCaseList) { 		
    		System.assert(c.ClosedDate >= Date.today().addMonths(-2));
    		System.assert(c.ClosedDate <= Date.today().addDays(1));    		
    	}
    	
    	
    	// test more than 6 articles
    	for (Integer i = 0; i < 10; i++) {
    	InQuira_Article_Info__c article = testTools.createArticle();
    	article.Article_Created_Date__c = DateTime.Now();
    	article.OwnerId = engineer.Id;
    	insert article;
    	}
    	
    	// test exclusion of existing AQI Audits
    	CreateRQIAQICtrl CRAC = new CreateRQIAQICtrl();
    	List<InQuira_Article_Info__c> kavs = [SELECT Title__c FROM InQuira_Article_Info__c LIMIT 1];
    	if(!kavs.isEmpty()){
	    	ApexPages.currentPage().getParameters().put('id',kavs[0].id);
	    	ApexPages.currentPage().getParameters().put('authorId',engineer.Id);
	    	ApexPages.currentPage().getParameters().put('casesorarticles','retArticles');
	    	CRAC.createAudit(); 
    	}
    	
    	//returning articles for AQI
    	//set date range
    	ApexPages.currentPage().getParameters().put('casesorarticles','retArticles');
    	ApexPages.currentPage().getParameters().put('datestart',testDate.format());
    	ApexPages.currentPage().getParameters().put('dateend',testDate.addDays(2).format());
    	
    	//testing articles > 4
    	RAF.valuesFromUser();
    	RAF.getarticleList();
    	//System.assert(RAF.getarticleList().size() > 0);
    	
    	
    	//testing articles < 4
    	RAF.valuesFromUser();
    	RAF.setupCasesOrArticles();
    	
    	//prev 2 months
    	//testing no articles
    	ApexPages.currentPage().getParameters().put('casesorarticles','retArticles');
    	ApexPages.currentPage().getParameters().put('datestart','');
    	ApexPages.currentPage().getParameters().put('dateend','');
    	RAF.valuesFromUser();  	
    }
}