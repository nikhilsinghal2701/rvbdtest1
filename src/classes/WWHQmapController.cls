public with sharing class WWHQmapController {
	public String OwnerAlias {get; set;}

    public String toOO { get; set; }
    public String fromOO { get; set; }
    
    public List<SelectOption> GeoOptions {get; set;}
    public List<SelectOption> RegionOptions {get; set;}
    
    public String GeoValue { get; set; }
    public String RegionValue { get; set; }

    public String nextRenewalDirection { get; set; }
    public String nextRenewalValue { get; set; }

    public String lastClosedDirection { get; set; }
    public String lastClosedValue { get; set; }
    
    public List<SelectOption> ProductsOptions {get; set;}
    public List<String> ProductsOwned { get; set; }

    public String toRP { get; set; }
    public String fromRP { get; set; }

    public String hasOpenOpps { get; set; }

	private Boolean myCurrentUser = true;
    public Boolean currentUser {
    	get {return myCurrentUser;}
    	set {myCurrentUser = value;}
    }

    public String toOpp { get; set; }
    public String fromOpp { get; set; }

    public String toRank { get; set; }
    public String fromRank { get; set; }

    public String category { get; set; }
    public Boolean bProductExclude{get;set;}
    
    public String HQQuery {get; private set;}
    public Double Latitude {get; set;}
    public Double Longitude {get; set;}
    public String userClause = '';
    
    User u = new User();
    WW_HQ__c wwhq = new WW_HQ__c();
    public User getOwnerSelect() 
    {                                               
        return u;
    }
    public WW_HQ__c getWWHQTemp() 
    {                                               
        return wwhq ;
    }    
          
    public WWHQmapController() {
        HQQuery = 'SELECT Id, Name, GlobalRank__c, AccountDisposition_Summary__c, Latitude__c, Longitude__c, OwnerId, Owner.Name, LastClosedOpportunity__c, AmountClosedOpportunities__c, AmountOpenOpportunities__c, TotalClosedOpportunities__c, TotalOpenOpportunities__c, Next_Desupport__c ';
        HQQuery = HQQuery + ' FROM WW_HQ__c WHERE (Latitude__c != null)';
        HQQuery = HQQuery + ' AND (OwnerId = \'' + UserInfo.getUserId() + '\')';         
        HQQuery = HQQuery + ' Limit 1000';
        
        Latitude = 0; // 46;
        Longitude = 0; //-103;
        
        Schema.DescribeFieldResult OptionsDescr;
        
		OptionsDescr = User.Geo__c.getDescribe();
		GeoOptions = PickList2SelectOption( OptionsDescr , true );
        
		OptionsDescr = User.Region__c.getDescribe();
		RegionOptions = PickList2SelectOption( OptionsDescr , true );
		
		OptionsDescr = WW_HQ__c.RiverbedProductsOwned__c.getDescribe();
		ProductsOptions = PickList2SelectOption( OptionsDescr , false );
		ProductsOwned = new List<String>();
    }
	
	// Converts a picklist schema description into a list of Picklist options
	public List<SelectOption> PickList2SelectOption(Schema.DescribeFieldResult descr , Boolean addBlank) {
		List<Schema.PicklistEntry> entries = descr.getPicklistValues();
		List<SelectOption> options = new List<SelectOption>();
		
		if (addBlank)
			options.add(new SelectOption('', ''));
			
		for (Schema.Picklistentry entry: entries) {
			if (entry.isActive()) options.add(new SelectOption(String.escapeSingleQuotes(entry.getValue()), entry.getLabel()));
		}
		
		return options;
	}

        public List<WWHQmapController.HQInfo> HQ {
        get {           
                integer count = 1;
            HQ = new List<WWHQmapController.HQInfo>();
            
            System.Debug(LoggingLevel.Debug, HQQuery);

            for (WW_HQ__c w : Database.query(HQQuery)) {
                HQInfo hqi = new HQInfo();
                hqi.record = w;
                hqi.count = count++;
                HQ.add(hqi);
            }                
                

            return HQ;
        }
        private set;
    }
    
    public String IncludeExclude {get; set;}
    
    public PageReference Filter() 
    {
    	String ownerSelect = null;
    	
        //this is just a lookup to a user
        if(myCurrentUser)
        {
            userClause = ' AND (OwnerId = \'' + UserInfo.getUserId() + '\')';
        }
        else if(OwnerAlias != null && OwnerAlias != '')
        {
                userClause = ' AND (Owner.Name LIKE \'%' + String.escapeSingleQuotes(OwnerAlias) + '%\')';
        }
        else //check region and geo dropdown values
        {
                userClause = '';
                
                ownerSelect = 'Select Id FROM User where (isActive = true) ';
                Boolean doSelect = false;
                if(RegionValue != null && RegionValue != '')
                {
                        ownerSelect = ownerSelect + ' AND (Region__c = \'' + RegionValue + '\')';
                        doSelect = true;
                }
                if(GeoValue != null && GeoValue != '')
                {
                        ownerSelect = ownerSelect + ' AND (Geo__c = \'' + GeoValue + '\')';
                        doSelect = true;
                }
                
                /*
                if(doSelect)
                {
                        ownerSelect = ownerSelect + ' Limit 10000';
                        
                        //look for users with the selected region or geo
                        string users = '';
                        for(User us : Database.query(ownerSelect))
                        {
                                users = users + '\'' + us.Id + '\', ';
                        }
                        
                        //if we found some set the userClause
                        if(users != '')
                        {
                                users = users.substring(0, users.length() - 1);
                                userClause = ' AND ownerId in (' + users + ') ';
                        }
                        else
                        {
                                //didnt find any, so have to have a way to not return any records
                                userClause = ' AND ownerId = null';
                        }                               
                }
                */
        }
        
        String wwhqClause= '';
        if(category != 'None')
        {
            wwhqClause= ' AND Category__c = \'' + category + '\'';
        }
        
        if(fromRank != null && fromRank!= '' && toRank != '')
        {
            wwhqClause += ' AND GlobalRank__c >=' + fromRank;
        }
        
        if(toRank != null && toRank != '')
        {
            wwhqClause += ' AND GlobalRank__c <= ' + toRank;
        }        
        
        if(fromOpp != null && fromOpp!= '' )
        {
            wwhqClause += ' AND AmountClosedOpportunities__c >=' + fromOpp;
        } 
        if(toOpp != null && toOpp!= '')
        {
            wwhqClause += ' AND AmountClosedOpportunities__c <= ' + toOpp;
        }
        
        if(fromOO != null && fromOO!= '' )
        {
            wwhqClause += ' AND AmountOpenOpportunities__c >=' + fromOO;
        } 
        if(toOO != null && toOO!= '')
        {
            wwhqClause += ' AND AmountOpenOpportunities__c <= ' + toOO;
        }     

        if(fromRP != null && fromRP!= '' )
        {
            wwhqClause += ' AND Rev_Potential_Remain__c >=' + fromRP;
        } 
        if(toRP != null && toRP!= '')
        {
            wwhqClause += ' AND Rev_Potential_Remain__c <= ' + toRP;
        }                         
        
        if(hasOpenOpps == 'true')
        {
            wwhqClause += ' AND TotalOpenOpportunities__c > 0 ';
        }
        
        if(lastClosedValue != null && lastClosedValue != '')
        {
            String d = formatDate(lastClosedValue);
            if (d != null) {
	            if(lastClosedDirection == '<')
	                wwhqClause += ' AND LastClosedOpportunity__c <  ' + d;
	            else
	                wwhqClause += ' AND LastClosedOpportunity__c > ' + d;
            }
        }
        
        if(NextRenewalValue != null && NextRenewalValue != '')
        {
            String d = formatDate(NextRenewalValue);

			if (d != null) {
	            if(nextRenewalDirection == '<')
	                wwhqClause += ' AND NextRenewalDate__c <  ' + d;
	            else
	                wwhqClause += ' AND NextRenewalDate__c > ' + d;            
            }
        }
        
        if(ProductsOwned.size() > 0)
        {
            String strTmp = '';
            for(String s : ProductsOwned)
            {
                strTmp += '\'' + s + '\',';
            }
            
            bProductExclude = (IncludeExclude == 'exclude');
            
            if(bProductExclude)
                wwhqClause += ' AND RiverbedProductsOwned__c NOT IN ('+strTmp;   
            else
                wwhqClause += ' AND RiverbedProductsOwned__c INCLUDES ('+strTmp;    
            
            wwhqClause = wwhqClause.substring(0, wwhqClause.length() - 1);
            wwhqClause += ') ';
        }
     
        //reset the query
        HQQuery = 'SELECT Id, Name, GlobalRank__c, AccountDisposition_Summary__c, Latitude__c, Longitude__c, OwnerId, Owner.Name , LastClosedOpportunity__c, AmountClosedOpportunities__c, AmountOpenOpportunities__c, TotalClosedOpportunities__c, TotalOpenOpportunities__c, Next_Desupport__c ';
        HQQuery = HQQuery + ' FROM WW_HQ__c WHERE (Latitude__c != null)';
        
        if (userClause != null)
        	HQQuery = HQQuery + userClause;
        	
        if (ownerSelect != null)
        	HQQuery = HQQuery + ' AND (OwnerId IN (' + ownerSelect + '))';
        	         
        HQQuery = HQQuery + wwhqClause;
           
        HQQuery = HQQuery + ' Limit 1000';

        system.debug(HQQuery);
                                                        
        return null;
    }
    
    
        public class HQInfo {
            public WW_HQ__c record {get; set;}
            public Integer count {get; set;}            
                

            public String AccountName{
                get{
                if(AccountName== null){
                    if(record.GlobalRank__c !=null)  
                    {                  
                        AccountName= 'G' + record.GlobalRank__c + ' ' + record.Name;
                    }
                    else
                    {AccountName = record.Name;}
                                          
                }      
                return AccountName;
                }
                private set;
            }
                            
            public String OpenOpps{
                get{
                if(OpenOpps== null){
                     if(record.TotalOpenOpportunities__c !=null)
                    { 
                        OpenOpps = record.TotalOpenOpportunities__c.format();
                    }
                                          
                }      
                return OpenOpps;
                }
                private set;
            }
            
            public String LifetimeSpend{
                get{
                if(LifetimeSpend== null){ 
                    if(record.AmountClosedOpportunities__c !=null)
                    {                    
                        LifetimeSpend= record.AmountClosedOpportunities__c.format();
                    }
                                          
                }      
                return LifetimeSpend;
                }
                private set;
            }
            
            
            public String PinText{
                  get {
            if(PinText == null){
              
                                if(record.TotalClosedOpportunities__c > 0)
                {
                                        if(record.LastClosedOpportunity__c >= System.today().addMonths(-12))
                            {PinText = '0';}
                            else if(record.LastClosedOpportunity__c >= System.today().addMonths(-24))
                            {PinText = '1';}
                            else if(record.LastClosedOpportunity__c >= System.today().addMonths(-36))
                            {PinText = '2';}
                            else if(record.LastClosedOpportunity__c != null)// > System.today().addMonths(-48))            {PinText = '3';}
                            {PinText = '3';}
                            else 
                            {PinText = '';}
                }
                else //treat as a prospect
                    {
                                        string disp = '';
                        if(record.AccountDisposition_Summary__c != null)
                        { PinText = record.AccountDisposition_Summary__c.substring(0,1);}
                        else
                        {PinText = '';}                        
                }
                      }      
                return PinText;
                        }
            private set;
        }
        
        public String PinType{
          get{
            if(PinType == null){
              
              if(record.TotalClosedOpportunities__c > 0)  //only for customers
              {
                if(record.AmountClosedOpportunities__c < 250000)
                        {PinType = 'd_map_xpin_letter&chld=pin_sleft|';}
                else if(record.AmountClosedOpportunities__c < 1000000)
                        {PinType = 'd_map_pin_letter&chld=';}  
                else
                        {PinType = 'd_map_xpin_letter&chld=pin_sright|';}
              }
              else //prospect, normal upright pin
                {{PinType = 'd_map_pin_letter&chld=';}}                        
            }      
            return PinType;
            }
            private set;
        }
        
        public String Color{
            get {
            if(Color == null){
              
              if(record.TotalClosedOpportunities__c > 0) 
              {
                Date d = System.today();
                if(record.LastClosedOpportunity__c <= d.addMonths(-24))
                  {Color = '00A400';}//light green    
                else if(record.LastClosedOpportunity__c <= d.addMonths(-12))
                  {Color = '006E00';}//medium green                    
                else
                  {Color = '003C00';}//dark green
                
              }
              else ///prospect
              {
                string disp = '';
                if(record.AccountDisposition_Summary__c != null)
                { disp = record.AccountDisposition_Summary__c.substring(0,1);}            
                
                if(disp == 'D')
                {Color = '8A2BE2';} //purple
                else if(disp == 'C')
                {Color = 'FF001D';}//red  
                else if(disp == 'B')
                {Color = 'FFA500';}//orange   
                else if (disp == 'A')
                {Color = 'FFF400';}//yellow  
                else
                {Color ='FFFFFF';} //white   
              }                          
            }          
              return Color;
            }
            private set;
        }
    } 
    
    Private String formatDate(String s) {
    	Date d;
		try {
			d = Date.parse(s);
		}
		catch (Exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to parse date, try using YYYY-MM-DD format'));
			return null;
		}
		
        String dd = d.year() + '-';
        
        if(d.month().format().length() == 1)
        {
                dd += '0' + d.month() + '-';    
        }
        else
        { 
                dd += d.month() + '-';
        }
        
        if(d.day().format().length() == 1)
        {
                dd += '0' + d.day();    
        }
        else
        { 
                dd += d.day();
        }                
                return dd;
    }                      
}