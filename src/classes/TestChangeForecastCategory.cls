/*
 * Class Name : TestChangeForecastCategory
 * Description: Test Class for ChangeForecastCategoryController and 
                ChangeForecastCategoryListViewController.
 * Authour    : Sorna (Perficient)
 * Version    : 1.0
 * Date       : 07/10/2013
 */

@isTest
public class TestChangeForecastCategory
{
    /* createTestAccount:
     * To create Test Accounts
     */
    public static Account createTestAccount(Boolean isInsert)
    {
        Account acc = new Account(Name = 'Test Account');
        
        if(isInsert)
            insert acc;
        
        return acc;
    }
    
    /* createTestOpty:
     * To create Test Opportunities
     */
    public static Opportunity createTestOpty(Boolean isInsert, Id accId, string stage, string forecastCategory)
    {
        Opportunity opty = new Opportunity(Name = 'Test Opty', AccountId = accId,
                                           StageName = stage, ForecastCategoryName = forecastCategory,
                                           CloseDate = System.Today().addDays(10));
        if(isInsert)
            insert opty;
        
        return opty;
    }
    
    /* testDetailPage:
     * Test Method for ChangeForecastCategoryController
     */
    private static testMethod void testDetailPage()
    {
        Account acc = createTestAccount(true);
        Opportunity opty = createTestOpty(true, acc.Id, '0 - Prospect', 'Pipeline');
        
        Test.startTest();
        
        Test.setCurrentPage(Page.Change_Forecast_Category_Detail_Page);
        Apexpages.currentPage().getParameters().put('id',opty.Id);
        
        ChangeForecastCategoryController obj = new ChangeForecastCategoryController(new Apexpages.StandardController(opty));
        obj.o.StageName = '6 - Order Accepted';
        obj.category = '';
        obj.saveOpty();
        
        obj.category = 'Omitted';
        obj.saveOpty();
        
        opty = [Select StageName, ForecastCategoryName from Opportunity where Id = :opty.Id];
        
        system.assertEquals('6 - Order Accepted',opty.StageName);
        system.assertEquals('Omitted',opty.ForecastCategoryName);
        
        Test.stopTest();
    }
    
    /* testListView:
     * Test Method for ChangeForecastCategoryListViewController
     */
    private static testMethod void testListView()
    {
        Account acc = createTestAccount(true);
        List<Opportunity> optyList = new List<Opportunity>();
        set<Id> optyIds = new set<Id>();
        
        for(Integer i=0; i<10; i++)
            optyList.add(createTestOpty(false, acc.Id, '0 - Prospect', 'Pipeline'));
        
        insert optyList;
        
        for(Opportunity o : optyList)
        {
            optyIds.add(o.Id);
        }
        
        Test.startTest();
        
        Test.setCurrentPage(Page.Change_Forecast_Category_List_View);
        Apexpages.currentPage().getParameters().put('retURL','/006/o');
        
        Apexpages.StandardsetController setController = new Apexpages.StandardsetController(optyList);
        
        ChangeForecastCategoryListViewController obj = new ChangeForecastCategoryListViewController(setController);
        system.assert(ApexPages.getMessages()[0].getDetail().contains('Please select atleast one Opportunity'));
        
        setController.setSelected(optyList);
        obj = new ChangeForecastCategoryListViewController(setController);
        obj.o.StageName = '6 - Order Accepted';
        obj.category = '';
        obj.saveOpty();
        
        obj.category = 'Omitted';
        obj.saveOpty();
        
        optyList = [Select StageName, ForecastCategoryName from Opportunity where Id in :optyIds];
        
        for(Opportunity o : optyList)
        {
            system.assertEquals('0 - Prospect',o.StageName);
            system.assertEquals('Pipeline',o.ForecastCategoryName);
        }
        
        Test.stopTest();
    }
}