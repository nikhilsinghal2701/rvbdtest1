/******************
Submittofinance class sends email to the recipient field on the SFDC_MDF_Claim__c object.
if there are attachments on the SFDC_MDF_Claim__c record.
Gives an error message if the submit to finance button is clicked without any attachments.

This class also updated the claim record[field: Attachments__c] on insertion or delation of attachments on a given SFDC_MDF_Claim__c record.
******************/

public with sharing class Submittofinance {
    
    public Boolean popupFlgvalue{get;set;}
    public Boolean displayPopUp{get;set;}
    public Boolean priceFlg{get;set;}
    
    public PageReference back(){
        return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    }
    public PageReference closepopup(){
      displayPopUp = false;
      if(popupFlgvalue)
        sendMail();
      return null;
   }
    public SFDC_MDF_Claim__c claim;
    public Submittofinance(ApexPages.StandardController controller){
        claim = (SFDC_MDF_Claim__c)controller.getRecord();
    }
    public PageReference sendMail(){ 
       // query OPEX Mktg Claims for attahments
        
        SFDC_MDF_Claim__c sfdcClaim = [SELECT Id,Attachments__c,Recipient_Email__c,FC_Status__c,(SELECT Id FROM Attachments) 
                                    FROM SFDC_MDF_Claim__c WHERE id=:claim.Id];
        
        List<ID> attachIdList = new List<ID>();
        if(sfdcClaim.FC_Status__c == 'Not Submitted'){
            for(Attachment attach: sfdcClaim.Attachments)
            {
                attachIdList.add(attach.Id);
            }
            List<Messaging.EmailFileAttachment> emailAttachmentList = new List<Messaging.EmailFileAttachment>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
             // to address set up
             List<String> address = new List<String>();
             address = sfdcClaim.Recipient_Email__c.split(',');
            // mail.setCcAddresses(new String[]{UserInfo.getUserEmail()});
             mail.setTemplateId(System.Label.SubmitToFinance_EmailTemplate);
             mail.setTargetObjectId(UserInfo.getUserId());
             mail.setToAddresses(address);
            
             mail.setWhatId(sfdcClaim.Id);
            
             mail.setSaveAsActivity(false);
           
                if(!attachIdList.isEmpty()){
                     
                    for(Attachment attach: [SELECT Id,Name,ContentType,Body FROM Attachment WHERE Id IN : attachIdList])
                    {
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        efa.setBody(attach.body);
                        efa.setContentType(attach.ContentType);
                        efa.setFileName(attach.Name);
                        emailAttachmentList.add(efa);
                    }
                    
                     // email attachment
                        mail.setFileAttachments(emailAttachmentList);
                        
                        try{
                            System.debug('mail:'+mail);
                           
                            //send email
                            List<Messaging.SendEmailResult> emailResultList = Messaging.sendEmail(new Messaging.Email[]{mail});
                            System.debug('emailResultList.get(0).success:'+emailResultList.get(0).success);
                            
                            if(emailResultList.get(0).success)
                            {
                                claim.FC_Status__c = 'Submitted - to Finance for Payment';
                                update claim;
                            
                            }
                            ApexPages.addMessage(new Apexpages.Message(ApexPages.SEVERITY.INFO,System.Label.Claim_Submitted));
                        }
                        catch(Exception e){
                            System.debug('msg:'+e.getMessage());
                            System.debug('cause:'+e.getCause());
                            ApexPages.addMessage(new Apexpages.Message(ApexPages.SEVERITY.ERROR,System.Label.No_Invoice_Claim));
                        }
                }
                else{
                    ApexPages.addMessage(new Apexpages.Message(ApexPages.SEVERITY.ERROR,System.Label.No_Invoice_Claim));
                }
           
        }
        else{
             ApexPages.addMessage(new Apexpages.Message(ApexPages.SEVERITY.ERROR,System.Label.Duplicate_claim_msg));
        }
        return null;
    }
    
    //methods used for trigger on attachments
    public static void attachUpdate(List<Id> parentIdList,Map<Id,Boolean> attachAction)
    {
        List<SFDC_MDF_Claim__c> updateClaimList = new List<SFDC_MDF_Claim__c>();
        Map<Id,List<Attachment>> claimattachMap = new Map<Id,List<Attachment>>();
        
        for(SFDC_MDF_Claim__c claim: [SELECT Id,Attachments__c,FC_Status__c,(SELECT Id FROM Attachments) 
                                        FROM SFDC_MDF_Claim__c WHERE id IN : parentIdList]) 
        {
            List<Attachment> attachmentList = claim.Attachments;
            claimattachMap.put(claim.Id,attachmentList);
            if(!attachmentList.isEmpty())
            {
                Boolean attachFlag = false;
                System.debug('attachAction:'+attachAction);
                if(!attachAction.isEmpty()){
                    for(Attachment attach: attachmentList)
                    {
                        if(attachAction.containsKey(attach.Id)){
                            attachFlag = true;
                            break;
                        }
                    }
                }
                if((attachFlag && attachmentList.size()>1) || attachAction.isEmpty())
                {
                    claim.Attachments__c = true;
                } 
                else
                    claim.Attachments__c = false;
                    
            }
            System.debug('claim.Attachments__c:'+claim.Attachments__c);
            updateClaimList.add(claim);
        }
       
            
        System.debug('updateClaimList:'+updateClaimList);
        if(!updateClaimList.isEmpty())
        {
            try{
                update updateClaimList;
            }
            catch(Exception e){
                 ApexPages.addMessage(new Apexpages.Message(ApexPages.SEVERITY.ERROR,e.getMessage()));

            }
        }
        
    }
    
    public static testMethod void submittofinanceTest(){
        
        //create budget
        SFDC_Channel_Budget__c budget = new SFDC_Channel_Budget__c(Name = 'testBudget',Geography__c = 'Field Marketing - Americas',Start_Date__c = Date.today());
        insert budget;
        // create mktg activity
        SFDC_MDF__c mktg = new SFDC_MDF__c(SFDC_Channel_Budget__c = budget.id,
                                            Request_Name__c = 'test activity',
                                            Geography__c = 'Americas',Status__c = 'All Claims Submitted',
                                            Total_Activity_Cost__c = 2000,Activity_Category__c = 'Awareness',Activity_Type__c = 'Serarch');
        insert mktg;
        //create claim
        SFDC_MDF_Claim__c claim = new SFDC_MDF_Claim__c(MDF__c = mktg.Id,Claim_Name__c='testclaim',FC_Status__c = 'Not Submitted',
                                                        Status_of_Marketing_Activity_Claims__c = 'Last or Only Claim',Requested_Amount__c=1000,
                                                        Attachments__c = true);
                                                        
        insert claim;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(claim);
        Submittofinance obj = new Submittofinance(controller);
        obj.sendMail();
        //create attachment
        Attachment attach = new Attachment(Name = 'attachTest',IsPrivate = false,Body=Blob.valueOf('asefgfawerqwtg'),ParentId = claim.Id);
        insert attach;
        obj.popupFlgvalue = true;
        obj.closepopup();
        obj.displayPopUp = true;
        claim.Attachments__c = false;
        claim.FC_Status__c = 'Pending - Additional Info Needed';
        update claim;
        delete attach;
        obj.back();
    }
    
}