/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAccountPlanning {

    static testMethod void myUnitTest() {
    List<Account> accList = TestingUtilPP.createAccounts(2, true, new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'RASP'});
    Account_Plan__c Ap= new Account_Plan__c(name = 'Account Plan Test123',AccountId__c = accList[0].id);
    insert Ap;
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    User u = new User(Alias = 'standt', Email=System.now().millisecond() +'standarduser@testorg.com', Internal_Department__c = '200 Tech support management',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', Segment__c = 'RVBD Operating Unit - US',
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=System.now().millisecond() + 'standarduser@testorg.com');
      insert u;
      Account_Plan_Team__c Apt= new Account_Plan_Team__c(User__c=u.id,Account_Plan__c=Ap.id,Access__c='Read');
      
    PageReference pageRef = Page.AccountPlanning;
    Test.setCurrentPageReference(pageRef);
    ApexPages.CurrentPage().getparameters().put('id',Ap.id);
    ApexPages.StandardController sc = new ApexPages.standardController(Ap);
    test.startTest();
    AccountPlanningExtension sic = new AccountPlanningExtension(sc);
    insert Apt;
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddTaskCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddComBCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddComCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddImpleCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddOppDevCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddHMCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddCustomerCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddRiverbedCusCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveTaskCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveComCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveComBCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    
    sic.onRemoveImpleCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveHMCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveOppDevCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveCustomerCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveRiverbedCustCondition();
    sic.Save();
    sic.SaveTab();
    sic.Cancel();
    sic.onChange();
    
    apt.Access__c='Read/Edit';
    update apt;
    test.stopTest();
    }
     static testMethod void myUnitTest2() {
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];    
    List<Account> accList = TestingUtilPP.createAccounts(2, true, new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'RASP'});
    Account_Plan__c Ap= new Account_Plan__c(name = 'Account Plan Test123',AccountId__c = accList[0].id);
    insert Ap;
     User u = new User(Alias = 'standt', Email=System.now().millisecond() +'standarduser@testorg.com', Internal_Department__c = '200 Tech support management',Segment__c = 'RVBD Operating Unit - US',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=System.now().millisecond() + 'standarduser@testorg.com');
      insert u;
      Account_Plan_Team__c Apt= new Account_Plan_Team__c(User__c=u.id,Account_Plan__c=Ap.id,Access__c='Read');
     insert Apt;
    PageReference pageRef = Page.AccountPlanning;
    Test.setCurrentPageReference(pageRef);
    ApexPages.CurrentPage().getparameters().put('id',Ap.id);
    ApexPages.CurrentPage().getparameters().put('AccountId',accList[0].id);
    ApexPages.StandardController sc = new ApexPages.standardController(Ap);
    test.startTest();
    AccountPlanningExtension sic = new AccountPlanningExtension(sc);
    
    
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddTaskCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddComBCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddComCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddImpleCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddOppDevCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddHMCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddCustomerCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddRiverbedCusCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveTaskCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveComCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveComBCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    
    sic.onRemoveImpleCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveHMCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveOppDevCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveCustomerCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveRiverbedCustCondition();
    sic.Save();
    sic.SaveTab();
    sic.Cancel();
    sic.onChange();
    
    
    test.stopTest();
    }
    static testMethod void myUnitTest3() {
    List<Account> accList = TestingUtilPP.createAccounts(2, true, new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'RASP'});
    Account_Plan__c Ap= new Account_Plan__c(name = 'Account Plan Test123',AccountId__c = accList[0].id);
    PageReference pageRef = Page.AccountPlanning;
    Test.setCurrentPageReference(pageRef);
    ApexPages.CurrentPage().getparameters().put('AccountId',accList[0].id);
    ApexPages.StandardController sc = new ApexPages.standardController(Ap);
    test.startTest();
    AccountPlanningExtension sic = new AccountPlanningExtension(sc);
    insert Ap;
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddTaskCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddComBCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddComCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddImpleCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddOppDevCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddHMCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddCustomerCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onAddRiverbedCusCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveTaskCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveComCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveComBCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    
    sic.onRemoveImpleCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveHMCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveOppDevCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveCustomerCondition();
    ApexPages.CurrentPage().getparameters().put('selectedId','1');
    sic.onRemoveRiverbedCustCondition();
    sic.Save();
    sic.SaveTab();
    sic.Cancel();
    sic.onChange();
    
    test.stopTest();
    }
 static testMethod void myUnitTestView() {
    List<Account> accList = TestingUtilPP.createAccounts(2, true, new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'RASP'});
    Account_Plan__c Ap= new Account_Plan__c(name = 'Account Plan Test123',AccountId__c = accList[0].id);
    insert Ap;
    PageReference pageRef = Page.AccountPlanView;
    Test.setCurrentPageReference(pageRef);
    ApexPages.CurrentPage().getparameters().put('id',Ap.id);
    ApexPages.CurrentPage().getparameters().put('AccountId',accList[0].id);
    ApexPages.StandardController sc = new ApexPages.standardController(Ap);
    test.startTest();
    AccountPlanningViewExtension sic = new AccountPlanningViewExtension(sc);
    sic.CClone();
      Attachment attachment1 = new Attachment( );
      attachment1.Body = Blob.valueOf( 'Test 123');
      attachment1.Name = 'Test attachment.jpg';
      sic.attachment1 = attachment1;
      sic.upload();
    test.stopTest();
    }
         static testMethod void myUnitTestView2() {
    List<Account> accList = TestingUtilPP.createAccounts(2, true, new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'RASP'});
    Account_Plan__c Ap= new Account_Plan__c(name = 'Account Plan Test123',AccountId__c = accList[0].id,Fiscal_Year__c='2014');
    insert Ap;
    
    Heat_Map__c Hp= new Heat_Map__c(Account_Plan__c=ap.id,Products__c='SteelHead',Stage__c='Competitor');
    insert Hp;
    AccountPlanHeatMap__c Ahp= new AccountPlanHeatMap__c(Color__c='#000000',Name='Competitor');
    insert Ahp;

    
    TestingUtilPP.createOD(23, true, new Map<String, Object>{'Customer_Impact_of_doing_nothing__c'=>'Silver','Account_Plan__c'=>ap.id});
    TestingUtilPP.createcp(23, true, new Map<String, Object>{'Account_Plan__c'=>ap.id});
    TestingUtilPP.createic(23, true, new Map<String, Object>{'Account_Plan__c'=>ap.id});
    TestingUtilPP.createcp(23, true, new Map<String, Object>{'Account_Plan__c'=>ap.id,'Riverbed_Partner_Influencer__c'=>accList[0].id});
    TestingUtilPP.createcl(23, true, new Map<String, Object>{'Account_Plan__c'=>ap.id,'Vendor__c'=>'test'});
    TestingUtilPP.createcl(23, true, new Map<String, Object>{'Account_Plan__c'=>ap.id,'Competitor__c'=>'test'});
    
    PageReference pageRef = Page.AccountPlanView;
    Test.setCurrentPageReference(pageRef);
    ApexPages.CurrentPage().getparameters().put('id',Ap.id);
    ApexPages.CurrentPage().getparameters().put('AccountId',accList[0].id);
    ApexPages.StandardController sc = new ApexPages.standardController(Ap);
    test.startTest();
    AccountPlanningViewExtension sic = new AccountPlanningViewExtension(sc);
    sic.CClone();
    Ap.Fiscal_Year__c='2015';
    ap.Status__c='New';
    update Ap;
    test.stopTest();
    }
    
}