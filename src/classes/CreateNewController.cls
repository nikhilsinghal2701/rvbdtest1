public with sharing class CreateNewController
{
    List<String> ObjectsList = new List<String>();

    private String topWindowURL;
    public String gettopWindowURL() 
    { 
         topWindowURL=System.currentPageReference().getParameters().get('topWindowURL');
         if(topWindowURL==null)
         {
             topWindowURL ='%2Fhome%2Fhome.jsp';
         }
        return this.topWindowURL; 
    }
    
    public void settopWindowURL(String strtopWindowURL) { this.topWindowURL = strtopWindowURL; }

    public List<String> getObjects() 
    {
        if(ObjectsList.size()==0) 
        {
             Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
             Integer i=0;
             for( String objectName : gd.keySet() ) 
             {
                 SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
                 DescribeSObjectResult objDef = objToken.getDescribe();

                 if(IsExcludedObject(objDef.Name)) 
                 {  
                    if((objDef.Createable==true) &&(objDef.Accessible==true) )
                     {
                         RecordType[] rt = [SELECT Id, Name FROM RecordType WHERE SObjectType=:objDef.Name];
                         if(rt.size()==0)
                         {                  
                             ObjectsList.add(objDef.Label);
                         }
                         else
                         {
                             for (RecordType RType:rt)
                             {
                                 Map<Id,Schema.RecordTypeInfo> rtMapById = objDef .getRecordTypeInfosById();
                                 Schema.RecordTypeInfo rtById = rtMapById.get(RType.id);
                                 if(rtById.Available)
                                 {
                                     ObjectsList.add(RType.Name);
                                 }   
                             }
                         } 
                     }
                 }
             }
        }
         ObjectsList.Sort();

        return ObjectsList ;
    }


    Public String Redirect()
    {
         String SelectedObjectName=System.currentPageReference().getParameters().get('SelectedObjectName');
         String topWindowURL=System.currentPageReference().getParameters().get('topWindowURL');
         if(topWindowURL==null)
         {
             topWindowURL ='%2Fhome%2Fhome.jsp';
         }
         String returnString='';
        
         Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
         for( String objectName : gd.keySet() ) 
         {
            SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
            DescribeSObjectResult objDef = objToken.getDescribe();
           
            if(objDef.Label==SelectedObjectName)
            {
                returnString = '/' + objDef.getKeyPrefix() + '/e?retURL=' +topWindowURL ;
                settopWindowURL('/' + objDef.getKeyPrefix());
            }
         }
            
         if(returnString=='')
         {
             List<RecordType> rt = [SELECT Id , Name, SObjectType FROM RecordType where Name=:SelectedObjectName];
             if(rt.Size()>0)
             {
                 for( String objectName : gd.keySet() ) 
                 {
                    SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
                    DescribeSObjectResult objDef = objToken.getDescribe();

                    if(objectName==rt[0].SObjectType)
                    {
                       returnString = '/' + objDef.getKeyPrefix() + '/e?retURL=' + topWindowURL +'&RecordType='+rt[0].Id;
                    }
                 }
             }
         }
         return returnString ;
    }

    Public Boolean IsExcludedObject(String ObjName)
    {
//         Set<String> objects = new Set <String>{'AccountContactRole','','AdditionalNumber','ApexClass','ApexComponent','ApexPage','ApexTrigger','Approval','Asset','Attachment','BrandTemplate','BusinessHours','BusinessProcess','CallCenter','Campaign','CampaignMember','CampaignMemberStatus','Case','CaseComment','CaseContactRole','CaseSolution','CaseTeamMember','CaseTeamRole','CaseTeamTemplate','CaseTeamTemplateMember','CaseTeamTemplateRecord','CategoryData','CategoryNode','Contract','ContractContactRole','Document','DocumentAttachmentMap','EmailServicesAddress','EmailServicesFunction','EmailTemplate','Event','Folder','Group','GroupMember','Idea','IdeaComment','MailmergeTemplate','Note','OpportunityCompetitor','OpportunityContactRole','OpportunityLineItem','Partner','Pricebook2','PricebookEntry','Product2','QueueSobject','RecordType','Scontrol','SelfServiceUser','Solution','StaticResource','Task','User','UserRole','Vote','WebLink','Content_Approval__c', 'OpportunityLineItemSchedule'}; 
         Set<String> objects = new Set <String>{'Opportunity','Lead','Account','Contact','SFDC_MDF__c','SFDC_MDF_Claim__c'};         
         Boolean returnValue=false;
         if(objects.contains(ObjName))
         {
             returnValue=true;
         } 
         else
         {
             if(ObjName.indexOf('Share')>=0)
             {
                 returnValue=false;
             }
             if(ObjName.indexOf('share')>=0)
             {
                 returnValue=false;
             }
             if(ObjName.indexOf('TeamMember')>=0)
             {
                 returnValue=false;
             }             
         }
         return returnValue;
    }

    static testmethod void TestMethod1()
    {
        PageReference testPage= new PageReference('/apex/Create_New');
        Test.setCurrentPage(testPage);
        CreateNewController testobj=new CreateNewController();     
        List<String> tmpObjectsList=testobj.getObjects();
        testobj.settopWindowURL('www.salesforce.com');
        testobj.gettopWindowURL();
        
 //       system.debug('--><--topWindowURL'+ testobj.topWindowURL);
 //       system.debug('--><--gettopWindowURL'+ testobj.gettopWindowURL());
 
        testPage.getParameters().put('SelectedObjectName', 'Submit a Lead');//Give here the Record type name (here Manual Lead is the record type for Lead) that has record type your org
        String strRedirect1=testobj.Redirect();

        testPage.getParameters().put('SelectedObjectName', 'Lead');//Give here the object that has no record type in your org
        String strRedirect3=testobj.Redirect();

        testPage.getParameters().put('SelectedObjectName', 'RTA__c');//Give here the object that has no record type in your org
        String strRedirect2=testobj.Redirect();


//        testPage.getParameters().put('SelectedObjectName', 'Campaigns');//Give here the object that has no record type in your org
//       String strRedirect4=testobj.Redirect();

 
        System.debug(testobj.IsExcludedObject('Account'));
        System.debug(testobj.IsExcludedObject('AccountContactRole')); 
        System.debug(testobj.IsExcludedObject('iShare'));
        System.debug(testobj.IsExcludedObject('ishare'));        
        System.debug(testobj.IsExcludedObject('TeamMember')); 

   }
}