/**
 * This class contains unit tests for validating the behavior of Apex classes.
 * Test class for CombinedAssetsController/CombinedContactsController/CombinedCasesController
*/
@isTest
private class CombinedAssetCaseContactTest {

    static testMethod void combinedCaseTest() {
        RecordType accPatRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accPatRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        insert stpAcc;
        
        Contact testCon=new Contact();
        testCon.FirstName='test1';
        testCon.LastName='14Nov';
        testCon.AccountId=stpAcc.Id;
        testCon.PartnerRole__c='orders';
        testCon.User_Profile__c='Admin';
        insert testCon;
        
        Case testCase1=new Case();
        testCase1.AccountId=stpAcc.Id;
        testCase1.ContactId=testCon.Id;
        testCase1.Status='New';
        testCase1.Priority='P4 - Support Request';
        testCase1.Origin='Email';
        insert testCase1;
        
        ApexPages.StandardController sc = New ApexPages.StandardController(testCase1);        
        System.currentPageReference().getParameters().put('id',testCase1.id);
        CombinedCaseController temp1=new CombinedCaseController(sc);
        
        ApexPages.StandardController sc1 = New ApexPages.StandardController(stpAcc);        
        System.currentPageReference().getParameters().put('id',stpAcc.id);
        CombinedCaseController temp2=new CombinedCaseController(sc1);
    }
}