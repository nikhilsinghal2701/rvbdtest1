public class GapNominatioForm{
    public string ans1{get;set;}
    public string ans2{get;set;}
    public string ans3{get;set;}
    public string ans4{get;set;}
    public string ans5{get;set;}
    public string ans6{get;set;}
    public string ans7{get;set;}
    public string ans8{get;set;}
    public string ans8a{get;set;}
    public string ans8b{get;set;}
    public string ans8c{get;set;}
    public string ans8d{get;set;}
    public string ans8e{get;set;}    
    public string ans9{get;set;}
    public string ans9a{get;set;}
    public string ans9b{get;set;}
    public string ans9c{get;set;}
    public string ans10{get;set;}
    public string ans10a{get;set;}
    public string ans10b{get;set;}
    public string ans10c{get;set;}
    //public string ans10d{get;set;}
    public string ans11{get;set;}
    //public string ans11a{get;set;}
    //public string ans11b{get;set;}
    public string ans12{get;set;}
    public string ans13{get;set;}
    public string ans14{get;set;}
    public string ans15{get;set;}
    //public string ans16{get;set;}
    public string currentPage {get; set;}
    public Boolean fillForm=true;
    public Boolean showMail{get;set;}
    public Boolean showMessage{get;set;}
    public String errorMsg{get;set;}
    public String gapLink{get;set;}
    public String subj {get; set;}
    public String body {get; set;}
    public String toAddress {get; set;}
    private List<string> toAddresses = new List<string>();
    public GapNominatioForm(){
        currentPage= ApexPages.currentPage().getParameters().get('currentPage');
        if(currentPage!=null){
            currentPage=currentPage;
        }
        showMail=false;
        showMessage=false;
    }
    public Boolean getFillForm(){
        return fillForm;
    }
    private void setFillForm(Boolean val){
        fillForm=val;
    }
    public PageReference prePareEmail(){
        fillForm=false;
        showMail=true;
        return null;
    }
    
    public PageReference getDeliverAsPDF() {  
        // Reference the page, pass in a parameter to force PDF
        PageReference pdf =  Page.GAPNominationPDFForm;
        pdf.getParameters().put('p','p');
        pdf.setRedirect(true);    
        // Grab it!
        Blob b;
        try{//unable to call .getContent() within tests
           b = pdf.getContent();
        }catch(VisualforceException e){
            b = Blob.valueOf('Some Text');
        } 
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();    
        efa.setFileName('FilledGAPNominationForm.pdf');  
        efa.setBody(b); //attach the PDF        
        // Create an email    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
        mail.setSubject(subj);
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        if(toAddress!=null && toAddress.length()>0){
            toAddresses.clear();
            toAddresses=toAddress.split(',');
            mail.setToAddresses(toAddresses);
        }
        mail.setHtmlBody(body);  
        // Create an email attachment        
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        try{
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with PDF sent to '+toAddress));
        }catch(Exception e){
            system.debug(LoggingLevel.INFO,'Email Exception:'+e.getMessage());
        }
        errorMsg='An email has been sent to the id:'+toAddress;
        gapLink='/apex/GAPList';
        showMessage=true;
        fillForm=false;
        showMail=false;   
        //return Page.GAPNominationPDFForm;
        return null;
    }
        
    public PageReference mCancel(){
        toAddresses.clear();
        toAddress='';
        body='';
        subj='';
        fillForm=true;
        showMail=false;
        return Page.GAP_Nomination_Form;
    }    
}