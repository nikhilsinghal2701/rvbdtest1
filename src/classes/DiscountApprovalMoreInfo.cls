/************************************************************
Rev1 Rucha 7/16/2013 Changed test class to use custom setting data created in test class
************************************************************/
public class DiscountApprovalMoreInfo {

    Quote__c quote = null;

    PageReference pageRef = ApexPages.currentPage();
    Id qId = pageRef.getParameters().get('qId');
 
    public String Comments {get;set;}
    
    public Quote__c getQuote(){
        if (quote == null){
            quote = [select Id, Comments__c, Name, OwnerId, Owner.Name from Quote__c where Id = :qId];  
        }
        return quote;
    }
    public PageReference cancel() {

        PageReference quotePage = new PageReference('/' + qId);
        quotePage.setRedirect(true);
        return quotePage;
    }
    

    public PageReference save() {
        
        quote.Discount_Status__c = QuoteApprovalHlp.STATUS_MORE_INFORMATION;
        update quote;
        User u = DiscountApprovalHlp.getUser(UserInfo.getUserId());
        DiscountApprovalEmailHlp.getInstance().doMoreInfo(quote.Id, Comments, u);
        PageReference quotePage = new PageReference('/' + qId);
        quotePage.setRedirect(true);
        return quotePage;
    }
    
    static testMethod void unitTest() {
        DiscountApprovalTestHlp.createNSDGeoMapData();//Rev1
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        
        
        DiscountApprovalMoreInfo mo = new DiscountApprovalMoreInfo();
        mo.qId = q.Id;
        Quote__c q2 = mo.getQuote();
        
        mo.cancel();
        
        try{
            //mo.save();
        }
        catch(Exception e){
            System.debug(e.getMessage());
        }
        
        
    }   
}