@isTest
public class TestAccountMergeCertificateSummaryJoin {
	public static testMethod void testTrigger(){
		Account a = new Account();
		a.Name = 'test account';
		a.Type ='Partner Account';
		insert a;
		
		Acct_Cert_Summary__c c1 = new Acct_Cert_Summary__c();
		c1.Account__c = a.id;
		c1.TAP__c = 'General';
		
		Acct_Cert_Summary__c c2 = new Acct_Cert_Summary__c();
		c2.Account__c = a.id;
		c2.TAP__c = 'General';
		
		list<Acct_Cert_Summary__c> csList = new Acct_Cert_Summary__c[]{c1, c2};
		insert csList;
		
		Contact cn1 = new Contact();
		cn1.AccountId = a.id;
		cn1.FirstName = 'test';
		cn1.LastName = 'tester';
		
		Contact cn2 = new Contact();
		cn2.AccountId = a.id;
		cn2.firstName = 'testmore';
		cn2.LastName = 'testing';
		list <contact> conList = new Contact[]{cn1, cn2};
		insert ConList;
		
		Certificate__c ce1 = new Certificate__c();
		ce1.Name = 'RSA';
		ce1.Contact__c = cn1.id;
		ce1.Certification_Summary__c = c1.id;
		
		Certificate__c ce2 = new Certificate__c();
		ce2.Name = 'RSA';
		ce2.Contact__c = cn2.id;
		ce2.Certification_Summary__c = c2.id;
		list<Certificate__c> certList = new Certificate__c[]{ce1, ce2};
		insert certList;
		
		test.startTest();
		a = [select name from Account where id = :a.id];
		a.name='new name';
		update a;
		
		ce2 = [select Certification_Summary__c from Certificate__c where id = :ce2.id];
		ce1 = [select Certification_Summary__c from Certificate__c where id = :ce1.id];
		
		system.assert(ce2.Certification_Summary__c == c1.id || ce1.Certification_Summary__c == c2.id);
		test.stopTest();
	}
}