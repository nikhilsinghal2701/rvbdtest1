@IsTest
/* Class to test Links to Case & Article
 * Test article KCS_New_vs_Known__c field 
 * 
 */
public class TestKcsArticleLink {

    static testMethod void testKCS() {
    	
    	TestAQIRQIUtils testTools = new TestAQIRQIUtils();
    	
        Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
        
        // select 3 case
        List<Case> caseList = [
            SELECT Id,KCS_Known_vs_New__c
            FROM Case
            WHERE Id NOT IN (SELECT Related_Case__c FROM InQuira_Case_Info__c)
            LIMIT 1
        ];
        
        // ensure we have 3 cases
        System.assertEquals(1, caseList.size()); 
        update caseList;
             
        caseList = [
            SELECT Id,KCS_Known_vs_New__c
            FROM Case
            WHERE Id IN :caseList
        ];
        
        // test for N/A
        for (Case c :caseList){
            System.assertEquals('N/A', c.KCS_Known_vs_New__c);
        }
        
        InQuira_Article_Info__c a = testTools.createArticle();
        
        insert a;
        
        List<InQuira_Article_Info__c> articleList = new List<InQuira_Article_Info__c>();
       	articleList = [SELECT Id, Document_ID__c FROM InQuira_Article_Info__c LIMIT 1];
                
        // create a new Related Knowledge Article and add it to List
        List<InQuira_Case_Info__c> caseArticleList = new List<InQuira_Case_Info__c>();
      
        InQuira_Case_Info__c curCaseArticle = testTools.createCaseInfo();
        curCaseArticle.Related_Case__c = caseList[0].Id;
        curCaseArticle.Document_ID__c = articleList[0].Document_ID__c;
        curCaseArticle.Related_InQuira_Article__c = articleList[0].Id;
        caseArticleList.add(curCaseArticle);
            
       	insert caseArticleList;     
       	update caseList;    
       
         caseList = [
            SELECT Id,KCS_Known_vs_New__c
            FROM Case
            WHERE Id IN :caseList
        ];
        
        // test for Known
        for (Case c: caseList){
            System.assertEquals('Known', c.KCS_Known_vs_New__c);
        }
        
        // set article created date to today        
        articleList[0].Article_Created_Date__c = datetime.now();
                
        update articleList[0];
        update caseList;
         
        Case c = [SELECT Id,KCS_Known_vs_New__c
            FROM Case
            WHERE Id = :caseList[0].id];
        
        // test for New
        System.assertEquals('New', c.KCS_Known_vs_New__c);
      Test.stopTest();
      //TESTED 4/3/15           
      BatchKCSJob task = new BatchKCSJob(50);
      database.executebatch(task);
               
    }   
}