/****
*This batch class is used to query all WW HQ objects not in a deleted state
*Does an aggregation on multiple fields on the Account object based on the WW HQ object queried
*Does an additional query to determine the largest customer
*Finally updates the WW HQ records
*/

global without sharing class BatchWWHQStatistics  implements Database.Batchable<sObject>, Database.Stateful {

	global String Query;

	global BatchWWHQStatistics(){
		//Query = 'Select Id, NumberOfEmployees__c, AmountClosedOpportunities__c, FirstClosedOpportunity__c, LastClosedOpportunity__c, NoOfEndOfLifexx10Product__c, TotalClosedOpportunities__c, TotalOpenOpportunities__c, TotalStage2or6Opportunities__c, RiverbedProductsOwned__c, CloudProviders__c, KeyApplications__c, NetworkHardware__c, NetworkManagement__c, StorageHardware__c, StorageManagement__c, NationalVAR__c, NationalVARComments__c, SI__c, SIComments__c, SP__c, SPComments__c FROM WW_HQ__c WHERE Id=\'' + 'a1MR0000000AHFkMAO' + '\'';
   		Query = 'Select Id, NumberOfEmployees__c, AmountClosedOpportunities__c, FirstClosedOpportunity__c, LastClosedOpportunity__c, NoOfEndOfLifexx10Product__c, TotalClosedOpportunities__c, TotalOpenOpportunities__c, TotalStage2or6Opportunities__c, RiverbedProductsOwned__c, CloudProviders__c, KeyApplications__c, NetworkHardware__c, NetworkManagement__c, StorageHardware__c, StorageManagement__c, NationalVAR__c, NationalVARComments__c, SI__c, SIComments__c, SP__c, SPComments__c, Next_Desupport__c FROM WW_HQ__c WHERE (IsDeleted = False)';
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   	   	   		
   		List<WW_HQ__c> hqToUpdate = new List<WW_HQ__c>();

   		
   		for(sObject so : scope)
   		{
   			WW_HQ__c hq = (WW_HQ__c)so;
   		
   			/// UDPATE STATISTICS
   		
	   		Double NumEmp = 0, aco = 0, aoo = 0, eol = 0,tco = 0,too = 0,tso = 0;	   		
	   		Double SteelheadOwned=0,CMCOwned=0,CascadeOwned=0,InterceptorOwned=0,MazuLegacyOwned=0,MemoryOwned=0,RSPOwned=0,MobileOwned=0,
	   			SteelheadQuoted=0,CMCQuoted=0,CascadeQuoted=0,InterceptorQuoted=0,MazuLegacyQuoted=0,MemoryQuoted=0,RSPQuoted=0,MobileQuoted=0;
		   	Integer fm=0, aos=0, assetUnderTrial;
		   	Date fco, lco, nds;
		   	
		   	for (AggregateResult ar : [SELECT WW_HQ__c, Count(Id) fm, Sum(NumberOfEmployees) NumEmp,
		   		Sum(Amount_Closed_Opportunities__c) aco, Sum(Amount_Open_Opportunities__c) aoo, Sum(No_of_End_of_Life_xx10_Product__c) eol, 
		   		Sum(Total_Closed_Opportunities__c) tco, Sum(Total_Open_Opportunities__c) too, Sum(Total_Stage_2_or_6_Opportunities__c) tso,
		   		Min(First_Closed_Opportunity__c) fco, Max(Last_Closed_Opportunity__c) lco,
		   		Sum(SteelheadOwned__c) SteelheadOwned, Sum(CMCOwned__c) CMCOwned, Sum(CascadeOwned__c) CascadeOwned,
		   		Sum(InterceptorOwned__c) InterceptorOwned, Sum(MazuLegacyOwned__c) MazuLegacyOwned, Sum(MemoryOwned__c) MemoryOwned,
		   		Sum(RSPOwned__c) RSPOwned, Sum(MobileOwned__c) MobileOwned, Sum(SteelheadQuoted__c) SteelheadQuoted,
		   		Sum(CMCQuoted__c) CMCQuoted, Sum(CascadeQuoted__c) CascadeQuoted, Sum(InterceptorQuoted__c) InterceptorQuoted,
		   		Sum(MazuLegacyQuoted__c) MazuLegacyQuoted, Sum(MemoryQuoted__c) MemoryQuoted, Sum(RSPQuoted__c) RSPQuoted,
		   		Sum(MobileQuoted__c) MobileQuoted, Max(Next_Desupport__c) NextDesupport, SUM(Assets_Owned_Supported__c) aos,
		   		Sum(Assets_Under_Trial__c) assetUnder	   		
		   			FROM Account WHERE (WW_HQ__c = :hq.Id) group by WW_HQ__c]) 
	   		{				
				//NumEmp = Double.valueOf(ar.get('NumEmp'));
				aco = Double.valueOf(ar.get('aco'));
				aoo = Double.valueOf(ar.get('aoo'));
				eol = Double.valueOf(ar.get('eol'));
				tco = Double.valueOf(ar.get('tco'));
				too = Double.valueOf(ar.get('too'));
				tso = Double.valueOf(ar.get('tso'));
				fm =  Integer.valueOf(ar.get('fm'));
				try {
					aos = Double.valueOf(ar.get('aos')).intValue();
				}
				catch (Exception e) {}
				fco = Date.valueOf(ar.get('fco'));
				lco = Date.valueOf(ar.get('lco'));
				nds = Date.valueOf(ar.get('NextDesupport'));
				
				SteelheadOwned =  Double.valueOf(ar.get('SteelheadOwned'));
				CMCOwned =  Double.valueOf(ar.get('CMCOwned'));
				CascadeOwned =  Double.valueOf(ar.get('CascadeOwned'));
				InterceptorOwned =  Double.valueOf(ar.get('InterceptorOwned'));
				MazuLegacyOwned =  Double.valueOf(ar.get('MazuLegacyOwned'));
				MemoryOwned =  Double.valueOf(ar.get('MemoryOwned'));
				RSPOwned =  Double.valueOf(ar.get('RSPOwned'));
				MobileOwned =  Double.valueOf(ar.get('MobileOwned'));
				
				SteelheadQuoted =  Double.valueOf(ar.get('SteelheadQuoted'));
				CMCQuoted =  Double.valueOf(ar.get('CMCQuoted'));
				CascadeQuoted =  Double.valueOf(ar.get('CascadeQuoted'));
				InterceptorQuoted =  Double.valueOf(ar.get('InterceptorQuoted'));
				MazuLegacyQuoted =  Double.valueOf(ar.get('MazuLegacyQuoted'));
				MemoryQuoted =  Double.valueOf(ar.get('MemoryQuoted'));
				RSPQuoted =  Double.valueOf(ar.get('RSPQuoted'));
				MobileQuoted=  Double.valueOf(ar.get('MobileQuoted'));
				assetUnderTrial = Integer.ValueOf(ar.get('assetUnder'));
			}	
			
			/*   
			*  Update 8: Set the Company field “Related_Account_Prospect_Count__c” to the Count of the
			*			 Category = Prospect for each linked Account.
			*/
		   		
		   		for(AggregateResult lcount :  [ Select WW_HQ__c, Count(Category__c) FROM Account 
		   										WHERE WW_HQ__c = :hq.Id AND Category__c = 'Prospect' GROUP BY WW_HQ__c])
		   		{		   		
		   			hq.Related_Account_Prospect_Count__c = Integer.ValueOf(lcount.get('expr0'));
					system.debug('\n Related_Account_Prospect_Count__c :'+Integer.ValueOf(lcount.get('expr0')));
		   		}
				
		   /*   
			*  Update 9: Set the Company field “Related_Account_Customer_Count__c” to the Count of the
			*			 Category = Existing or New for each linked Account.
			*/
		   		
		   		for(AggregateResult lcount2 :  [ Select WW_HQ__c, Count(Category__c) FROM Account 
		   										WHERE WW_HQ__c = :hq.Id AND ( Category__c = 'Existing' OR Category__c = 'New') GROUP BY WW_HQ__c])		   		
		   		{
		   			hq.Related_Account_Customer_Count__c = Integer.ValueOf(lcount2.get('expr0'));
					system.debug('\n Related_Account_Customer_Count__c :'+Integer.ValueOf(lcount2.get('expr0')));
		   		}
			//hq.NumberOfEmployees__c = NumEmp;
			hq.AmountClosedOpportunities__c = aco;
			hq.AmountOpenOpportunities__c = aoo;
			hq.FirstClosedOpportunity__c = fco;		
			hq.LastClosedOpportunity__c = lco;
			hq.NoOfEndOfLifexx10Product__c = eol;
			hq.TotalClosedOpportunities__c = tco;
			hq.TotalOpenOpportunities__c = too;
			hq.TotalStage2or6Opportunities__c = tso;
			hq.CountOfFamilyMembers__c = fm;
			hq.Assets_Owned_Supported__c = aos;
			
			hq.SteelheadOwned__c = SteelheadOwned;
			hq.CMCOwned__c = CMCOwned;
			hq.CascadeOwned__c = CascadeOwned;
			hq.InterceptorOwned__c = InterceptorOwned;
			hq.MazuLegacyOwned__c = MazuLegacyOwned;
			hq.MemoryOwned__c = MemoryOwned;
			hq.RSPOwned__c = RSPOwned;
			hq.MobileOwned__c = MobileOwned;
						
			hq.SteelheadQuoted__c = SteelheadQuoted;
			hq.CMCQuoted__c = CMCQuoted;
			hq.CascadeQuoted__c = CascadeQuoted;
			hq.InterceptorQuoted__c = InterceptorQuoted;
			hq.MazuLegacyQuoted__c = MazuLegacyQuoted;
			hq.MemoryQuoted__c = MemoryQuoted;
			hq.RSPQuoted__c = RSPQuoted;
			hq.MobileQuoted__c = MobileQuoted;
			
			hq.Next_Desupport__c = nds;
			
			hq.Assets_Under_Trial__c = assetUnderTrial;
			
			/// DONE: UPDATE STATISTICS
			
			/// UPDATE LARGEST CUSTOMER
			
			List<Account> Largest_Customer = new List<Account>([SELECT Id FROM Account WHERE (WW_HQ__c = :hq.Id) AND (Amount_Closed_Opportunities__c > 0) ORDER BY Amount_Closed_Opportunities__c DESC LIMIT 1]);
			if (!Largest_Customer.IsEmpty())
				hq.Largest_Account__c = Largest_Customer[0].Id;
			
			/// DONE: UPDATE LARGEST CUSTOMER
			
			
			
			hqToUpdate.add(hq);
   		}
		update hqToUpdate;
								
   }
   
   global void finish(Database.BatchableContext BC){

   }
	
}