global with sharing class BatchKCSJob implements Database.Batchable<sObject> {

//The person excecuting this must have a bypass validation rules checkbox enabled

// /!\ NOTE: THIS BATCH JOB is created to update the cases that were changed by a user not having a knowledge license
// THe cases changes for user having the knowledge license will be in the caseArticleCheck trigger.

	global String query;
	global DateTime startDate;
	
	global BatchKCSJob() {
		
		startDate = DateTime.parse('1/1/2014 12:00 AM');		
		query = 'SELECT Id,Status,CreatedDate ' + 
				'FROM Case ' + 
				'WHERE (Status=\'Closed\' ' + 
				'OR Status=\'Closed - Resolved\' ' +
				'OR Status=\'Closed - Resolved (First Time Fix)\' ' +
				'OR Status=\'Closed - Unresolved\') ' +
				'AND CreatedDate >= :startDate';	
	}
	
	global BatchKCSJob (Integer limiter) {
		this();
		query += ' LIMIT ' + limiter;
	}
	
	global Database.QueryLocator start(Database.BatchableContext batchableContext) {
		return Database.getQueryLocator(query);			
	}
		
	global void execute(Database.BatchableContext batchableContext, List<sObject> scopeList) {
		update scopeList;
	}
	
	
	global void finish(Database.BatchableContext batchableContext) {
		
	}
}