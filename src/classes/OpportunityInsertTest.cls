public class OpportunityInsertTest 
{
    static testMethod void OpportunityInsertTest() 
    {
        
        //Rohit - 01082010
        DiscountApproval.isTest = true;

        VAR__c v = new VAR__c(Name='My New VAR');
        insert v;
        
        Account a = new Account(Name='Opp Update',BillingState='CA',BillingCountry='US',OwnerId='00530000000kv5F');
        insert a;
        System.debug('New Account Id -->' + a.Id);
        
        Opportunity o = new Opportunity (OwnerId='00530000000kv5F',AccountId= a.Id,Name='Test Trigger 2',Registering_Partner__c='0015000000FC60Q',VAR__c= v.id,Registering_Partner_User__c='00570000000oU0L',StageName='0 - Prospect',CloseDate=Date.newInstance(2008,06,30));
        try
        {
            insert o;
        }
        catch(DMLException e)
        {
            //System.debug(e.getMessage());
            System.assert(e.getMessage().contains('Invalid id'),e.getMessage());
        }
        System.debug('New Opp1 Id -->' + o.Id);
        DiscountApproval.run = false; //added by Ankita 1/27/2012
        
        Opportunity op = new Opportunity (OwnerId='00530000000kv5F',AccountId= a.Id,Name='Test Trigger 2',Registering_Partner__c='00130000003iRM5',VAR__c = v.id,Registering_Partner_User__c='00570000000oU0L',StageName='0 - Prospect',CloseDate=Date.newInstance(2008,06,30));
        insert op;
        System.debug('New Opp2 Id -->' + op.Id);

        a.OwnerId='00530000000l28L';
        update a;
        
        o.OwnerId='00530000000kv5F';
        update o;
        
        o.VAR__c = 'a0G70000000LB14';
        try 
        {
            update o;
        } 
        catch (DmlException e) 
        {
            //System.assert(e.getMessage().contains('first error:FIELD_INTEGRITY_EXCEPTION,'+ 'VAR: id value of incorrect type'),e.getMessage());
        }

    
        
    }
}