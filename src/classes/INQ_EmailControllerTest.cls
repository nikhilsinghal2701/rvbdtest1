@IsTest(SeeAllData=true)
public class INQ_EmailControllerTest {
    
    public static Case testCase() {
        Case c = [SELECT Id,Subject,ContactId FROM Case WHERE Status='New' OR Status='Working' LIMIT 1];
        return c;
    }
    
    public static Contact testContact() {
        Contact c = [SELECT Id,Email FROM Contact WHERE Email!='' AND Email!=NULL LIMIT 1];
        return c;
    }
    
    public static InQuira_Article_Info__c testArticle(String tURL, String tTitle, String tID, String tGUID, String tType) {
        InQuira_Article_Info__c a = new InQuira_Article_Info__c(
            Article_Type__c = tType,
            Display_URL__c = tURL,
            Document_GUID__c = tGUID,
            Document_ID__c = tID,
            Title__c = tTitle 
        );  
        insert a;
        return a;
    }
    
    public static List<InQuira_Article_Info__c> testArticles() {
        List<InQuira_Article_Info__c> articles = new List<InQuira_Article_Info__c>();
        articles.add(testArticle('http://www.riverbed.com/test.html', 'Test HTML Article', null, null, 'External'));
        articles.add(testArticle('http://supportkb.riverbed.com/support/index?page=content&id=S12345', 'Test Solution 12345', '12345', 'aefaefaefaefaefaef', 'IM'));
        return articles;
    }

    public static InQuira_Case_Info__c testLink(Case c, InQuira_Article_Info__c a, Decimal version) {
        InQuira_Case_Info__c link = new InQuira_Case_Info__c(
            Article_Version__c = version,
            Related_Case__c = c.Id,
            Related_InQuira_Article__c = a.Id
        );
        insert link;
        return link;
    }
    static testMethod void testIMAC() {
    INQ_MailedArticlesController IMAC= new INQ_MailedArticlesController();
    IMAC.setListEmpty('true');
    IMAC.setIsPlainText('true');
    IMAC.setIsSignature('true');
    IMAC.setSalutationName('true');
    IMAC.setSalutationEmail('true@true.com');
    IMAC.getThisCase();
    IMAC.getListEmpty();
    IMAC.getIsSignature();
    IMAC.getSalutationName();
    IMAC.getSalutationEmail();
    IMAC.getEmailMessage();
 
    }
    
   
}