@isTest (SeeAllData=True)
private class UpdateSalesGeoRegionTestClass {
    public static list<Country__c> countryList;
    static testMethod void validateUpdateSalesGeoRegion() {
        setupTestData();
        //Test Single record Insert - Country based
                Lead singleLead1 = new Lead(LastName='John', Company='Test', LeadSource='Email',Country='TestCountry 1');
                     // Insert Lead
                    insert singleLead1;
    
                    // Retrieve the new Lead and Test that the trigger correctly updated the geo and Region
                    System.assertEquals('TestGeo 1', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead1.Id].Sales_Geo__c);
                System.assertEquals('TestSalesRegion 1', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead1.Id].Sales_Region__c);
                System.assertEquals(true, [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead1.Id].Concierge_Country__c);
                
                //Test Single record Update  - Country based
                singleLead1.Country='TestCountry 2';
                     // update Lead
                    update singleLead1;
    
                    // Retrieve the new Lead and Test that the trigger correctly updated the geo and Region
                    System.assertEquals('TestGeo 2', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead1.Id].Sales_Geo__c);
                System.assertEquals('TestSalesRegion 2', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead1.Id].Sales_Region__c);
                System.assertEquals(true, [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead1.Id].Concierge_Country__c);

                //Test Single record Insert - ISOCode based
                Lead singleLead2 = new Lead(LastName='John', Company='Test', LeadSource='Email', Country='3');
                     // Insert Lead
                    insert singleLead2;
    
                    // Retrieve the new Lead and Test that the trigger correctly updated the geo and Region
                    System.assertEquals('TestGeo 3', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead2.Id].Sales_Geo__c);
                System.assertEquals('TestSalesRegion 3', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead2.Id].Sales_Region__c);
                System.assertEquals(true, [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead2.Id].Concierge_Country__c);
                
                //Test Single record Update  - IsoCode based
                singleLead2.Country='5';
                     // update Lead
                    update singleLead2;
    
                    // Retrieve the new Lead and Test that the trigger correctly updated the geo and Region
                    System.assertEquals('TestGeo 5', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead2.Id].Sales_Geo__c);
                System.assertEquals('TestSalesRegion 5', [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead2.Id].Sales_Region__c);
                System.assertEquals(true, [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id =: singleLead2.Id].Concierge_Country__c);


                //Test Bulk Insert 
                Lead lead1 = new Lead(LastName='John', Company='Test', LeadSource='Email', Country='TestCountry 1');
                      Lead lead2 = new Lead(LastName='John', Company='Test', LeadSource='Email', Country='TestCountry 2');
                Lead lead3 = new Lead(LastName='John', Company='Test', LeadSource='Email', Country='3');
                      Lead lead4 = new Lead(LastName='John', Company='Test', LeadSource='Email', Country='4');
                list<Lead> leadbulkTest = new list<Lead>{lead1, lead2, lead3, lead4};
                     // Insert Lead
                    insert leadbulkTest;
    
                    // Retrieve the new Lead and Test that the trigger correctly updated the geo and Region
                for(Lead l :  [SELECT Country,Sales_GEO__c,Sales_Region__c,Concierge_Country__c FROM Lead WHERE Id in : leadbulkTest]){
                    if(l.Country == 'TestCountry 1'){
                System.assertEquals('TestGeo 1',l.Sales_Geo__c);
                System.assertEquals('TestSalesRegion 1', l.Sales_Region__c);
                System.assertEquals(true,l.Concierge_Country__c);
                } else if(l.Country == 'TestCountry 2'){
                System.assertEquals('TestGeo 2',l.Sales_Geo__c);
                System.assertEquals('TestSalesRegion 2', l.Sales_Region__c);
                System.assertEquals(true,l.Concierge_Country__c);
                }else if(l.Country == '3'){
                System.assertEquals('TestGeo 3',l.Sales_Geo__c);
                System.assertEquals('TestSalesRegion 3', l.Sales_Region__c);
                System.assertEquals(true,l.Concierge_Country__c);
                }else if(l.Country == 'TestCountry 4'){
                System.assertEquals('TestGeo 4',l.Sales_Geo__c);
                System.assertEquals('TestSalesRegion 4', l.Sales_Region__c);
                System.assertEquals(true,l.Concierge_Country__c);
                }
                }
                
                
    }

    private static void setupTestData(){
        //Build Test Data for Country list
        countryList = new list<Country__c>();
        for(Integer i=0; i < 10; i++){
            Country__c sampleCountry = new  Country__c();
            sampleCountry.Name = 'TestCountry '+i;
            sampleCountry.Country__c = 'TestCountry '+i;
            sampleCountry.Geo__c = 'TestGeo '+i;
            sampleCountry.Sales_Region__c = 'TestSalesRegion '+i;
            sampleCountry.Concierge_Country__c = true;
            sampleCountry.ISO2__c = String.Valueof(i);
            
            countryList.add(sampleCountry);
        }
        insert countryList;
        
        //build test data for lead custtom setting Remove_Lead_Auto_Convert__c
        /*Remove_Lead_Auto_Convert__c testSetting = new Remove_Lead_Auto_Convert__c();        
        testSetting.Name = 'Lead Update';
        testSetting.Auto_Convert__c = '90';
        testSetting.Eloqua_Disassociation_Date__c = '7';
        insert testSetting ;*/
    }
}