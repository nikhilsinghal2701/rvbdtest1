/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This class is  for ArchiveMigration
Created Date : Dec 2011
*******************************************************************************************************/

global class ArchiveMigrationClass implements Database.Batchable <Sobject> {

    global Final String query;
    
    global ArchiveMigrationClass()
        {
            String ids='\''+'0068000000S6aIF'+'\'';
           DateTime dt = Date.today().addYears(-2);
           String now_datetime_as_string = dt.format('yyyy-MM-dd')+'T'+dt.format('HH:mm:ss')+dt.format('Z').substring(0, dt.format('Z').length()-2)+':00';
           // dt = DateTime.valueof(now_datetime_as_string);
           query = 'Select Id, CreatedDate, (Select Id From R00NS0000000MJ1pMAG__r), (Select Id, Name, CurrencyIsoCode, Category_Master__c, Date_Approved__c, Discount__c, Disti_Discount__c, Disti_Special_Discount__c, IsOpp__c, Opportunity__c, Special_Discount__c, Uplift__c From Discount_Details__r) From Opportunity where createddate<'+now_datetime_as_string;
            
        }  
        
        global ArchiveMigrationClass(String ids)//Called from test method
        {
            ids='\''+ids+'\'';
          
           query = 'Select Id, CreatedDate, (Select Id From R00NS0000000MJ1pMAG__r), (Select Id, Name, CurrencyIsoCode, Category_Master__c, Date_Approved__c, Discount__c, Disti_Discount__c, Disti_Special_Discount__c, IsOpp__c, Opportunity__c, Special_Discount__c, Uplift__c From Discount_Details__r) From Opportunity where id='+ids;
            
        }  
          
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<Opportunity> queryList){
    	List<Discount_Detail_Archive__c> archiveList = new List<Discount_Detail_Archive__c>();
    	List<Discount_Detail__c> detailList = new List<Discount_Detail__c>	();
    	List<Discount_Detail__c> quoteDetailList = new List<Discount_Detail__c>	();
    	Set<ID> archive = new Set<Id>();
    	
    	Set<Id> quoteIds = new Set<ID>();
    	for(Opportunity op :queryList)	
    	{
    		for(Discount_Detail__c d :op.Discount_Details__r)
    		{
    			Discount_Detail_Archive__c dc = new Discount_Detail_Archive__c();
    			dc.Date_Approved__c=d.Date_Approved__c;
    			dc.Discount__c=d.Discount__c;
    			dc.Category_Master__c=d.Category_Master__c;
    			dc.IsOpp__c=d.IsOpp__c;
    			dc.Opportunity__c=d.Opportunity__c;
    			dc.Special_Discount__c=d.Special_Discount__c;
    			dc.Uplift__c=d.Uplift__c;
    			if(!archive.contains(d.ID))
    			{
	    			detailList.add(d);
	    			archiveList.add(dc);
	    			archive.add(d.Id);
    			}
    		}
    		for(Quote__C q :op.R00NS0000000MJ1pMAG__r)
    		{
    			quoteIds.add(q.Id);
    			
    		}
    	}
    	quoteDetailList=[Select Id, Name, CurrencyIsoCode, Category_Master__c, Date_Approved__c, Discount__c, Disti_Discount__c, Disti_Special_Discount__c, IsOpp__c, Opportunity__c, Special_Discount__c, Uplift__c,quote__c From Discount_Detail__c where quote__C in :quoteIds];
    	for(Discount_Detail__c d :quoteDetailList)
    		{
    			Discount_Detail_Archive__c dc = new Discount_Detail_Archive__c();
    			dc.Date_Approved__c=d.Date_Approved__c;
    			dc.Discount__c=d.Discount__c;
    			dc.Category_Master__c=d.Category_Master__c;
    			dc.IsOpp__c=d.IsOpp__c;
    			dc.Quote__c=d.Quote__c;
    			dc.Special_Discount__c=d.Special_Discount__c;
    			dc.Uplift__c=d.Uplift__c;
    			if(!archive.contains(d.ID))
    			{
	    			detailList.add(d);
	    			archiveList.add(dc);
	    			archive.add(d.Id);
    			}
    		}
    		if(archiveList!=null)
    		insert(archiveList);
    		if(detailList!=null)
    		delete(detailList);
    		
    
    }
    
     global void finish(Database.BatchableContext BC){

   }
}