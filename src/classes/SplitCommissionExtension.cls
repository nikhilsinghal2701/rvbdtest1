public with sharing class SplitCommissionExtension {
    
    // List displayed in the UI table.
    public List<OpportunitySplitWrapper> oSplitWrapperList {get;set;}
    
    // Map containing objects that need to be deleted on Save.
    public Map<Integer,OpportunitySplitWrapper> deleteOSplitMap {get;set;}
    
    // Opportunity object that is associated with these split objects.
    public Opportunity opportunity {get;set;}
    
    // Maximum number of opporunity split objects (table rows). Limited by legacy!
    public Integer maxRowCount {get { return 10; } private set;}
    
    public Boolean isChanged 
    {
        get 
        { 
            if(isChanged == null) 
            {
                isChanged = false;
            } 
            return isChanged;
        }
        set;
    }
    
    // Integer identifier for wrapper objects.
    Integer idIterator = 1;
    
    public SplitCommissionExtension(ApexPages.StandardController controller){
       // maxRowCount = 4;
        isChanged = false;
        Split_Commissions__c tempOSplit = (Split_Commissions__c)controller.getRecord();
        if (tempOSplit.Id == null)
        {
            //String oppId = ApexPages.currentPage().getParameters().get('CF00NT0000001BMl3_lkid');
            String oppId = ApexPages.currentPage().getParameters().get('oppId');
            tempOSplit = new Split_Commissions__c(Opportunity__c = oppId);
        } else 
        {
            // retrieving the opportunity associated with this split object
            tempOSplit = [Select Opportunity__c from Split_Commissions__c where id = :tempOSplit.Id];
        }

        this.opportunity = [Select Name,Amount,OwnerId,AccountId,  
            Split_Rep1__c, Split_Rep2__c, Split_Rep3__c, Split_Rep4__c, Splits_Iteration__c,
            (Select Opportunity__c,Name, Is_Opportunity_Owner__c,Split_Comm_Rep__c, 
                Commission_Percentage__c,GAP_Additional__c,GAP_Split__c, Approved_by_Rep__c 
                From Split_Commissions__r) 
            From Opportunity o where id = :tempOSplit.Opportunity__c];
        this.oSplitWrapperList = new List<OpportunitySplitWrapper>();
        
        List<OpportunitySplitWrapper> first = new List<OpportunitySplitWrapper>();
        List<OpportunitySplitWrapper> rest = new List<OpportunitySplitWrapper>();
        
        // Populating the list to be displayed in the UI table.
        for(Split_Commissions__c oSplit:opportunity.Split_Commissions__r){
            OpportunitySplitWrapper wrapper = new OpportunitySplitWrapper(this.idIterator++,oSplit,true);
            if(oSplit.Is_Opportunity_Owner__c){
                first.add(wrapper);
            }else{
                rest.add(wrapper);
            }
        }
        oSplitWrapperList.addAll(first);
        oSplitWrapperList.addAll(rest);
        this.deleteOSplitMap = new Map<Integer,OpportunitySplitWrapper>();
    }
    /**
    * This method gets called on Save.
    */
    public PageReference doSave(){
        // validating user input and generating an error message
        String errorMessageString = generateErrorMessage();
        
        System.debug('--errormessage---'+errorMessageString);
        
        // If the error message is not empty it is an indication that there is an error message
        if(!''.equals(errorMessageString)){
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessageString);
            ApexPages.addMessage(errorMessage);
            return null;
        }
        
        // deleting objects removed from the table.
        deleteObjects();
        
        // inserted new objects.
        Boolean successOp = insertNewObjects();
        if(!successOp){
            return null;
        }

        
        //Updating legacy fields in Opportunity.
        updateOpportunityFields();
        
        // calculating the split detail objects for each of the opportunity splits.
        //ManageSplitDetails.evaluateSplitTypes(new Set<Id>{this.opportunity.Id});
        
        // redirecting the user back to the page reference.
        return new PageReference('/'+opportunity.id);
    }
    
    private void updateOpportunityFields(){
        if(this.oSplitWrapperList.size() == 0) return;
        Split_Commissions__c tempSplit;
        this.opportunity.Split_Rep1__c = null; 
        //this.opportunity.Split_Rep1_Approved__c = false;
        this.opportunity.Split_Rep2__c = null;
        //this.opportunity.Split_Rep2_Approved__c = false;
        this.opportunity.Split_Rep3__c = null;
        //this.opportunity.Split_Rep3_Approved__c = false;
        this.opportunity.Split_Rep4__c = null;                  
        //this.opportunity.Split_Rep4_Approved__c = false;
        //if(isChanged) this.opportunity.GAM_Approved__c = false;
        if(this.opportunity.Splits_Iteration__c == null){
            this.opportunity.Splits_Iteration__c = 0;
        }
        this.opportunity.Splits_Iteration__c += 1;
        
        
        List<Id> userIds = new List<Id>();
        for (OpportunitySplitWrapper aWrapper:oSplitWrapperList)
        {
            userIds.add(aWrapper.oSplit.Split_Comm_Rep__c);
        }
        
        List<User> SplitReps = [Select Id, Name, Email from User where ID in :userIds];
        System.Debug(SplitReps);
        
        
        
        if(this.oSplitWrapperList.size() > 0){
            //tempSplit = this.oSplitWrapperList.get(0).oSplit;
            this.opportunity.Split_Rep1__c = SplitReps.get(0).Email;
            //this.opportunity.X2nd_Salesperson_Split__c = tempSplit.Split__c;            
        }
        if(this.oSplitWrapperList.size() > 1){
            //tempSplit = this.oSplitWrapperList.get(1).oSplit;
            this.opportunity.Split_Rep2__c = SplitReps.get(1).Email;
            //this.opportunity.X2nd_Salesperson_Split__c = tempSplit.Split__c;            
        }
        if(this.oSplitWrapperList.size() > 2){
            //tempSplit = this.oSplitWrapperList.get(2).oSplit;
            //this.opportunity.X3rd_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.Split_Rep3__c = SplitReps.get(2).Email;            
        }
        if(this.oSplitWrapperList.size() > 3){
            //tempSplit = this.oSplitWrapperList.get(3).oSplit;
            //this.opportunity.X4th_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.Split_Rep4__c = SplitReps.get(3).Email;            
        }
        update this.opportunity;
    }
    
    /*private void updateOpportunityFields(){
        if(this.oSplitWrapperList.size() == 0) return;
        this.opportunity.X1st_Salesperson_Split__c = this.oSplitWrapperList.get(0).oSplit.Split__c;
        Opportunity_Split__c tempSplit;
        this.opportunity.X2nd_Salesperson__c = null;
        this.opportunity.X2nd_Salesperson_Split__c = 0;
        this.opportunity.X3rd_Salesperson_Split__c = 0;
        this.opportunity.X3rd_Salesperson__c = null;
        this.opportunity.X4th_Salesperson_Split__c = 0;
        this.opportunity.X4th_Salesperson__c = null;                  
        if(this.oSplitWrapperList.size() > 1){
            tempSplit = this.oSplitWrapperList.get(1).oSplit;
            this.opportunity.X2nd_Salesperson__c = tempSplit.Salesperson__c;
            this.opportunity.X2nd_Salesperson_Split__c = tempSplit.Split__c;            
        }
        if(this.oSplitWrapperList.size() > 2){
            tempSplit = this.oSplitWrapperList.get(2).oSplit;
            this.opportunity.X3rd_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.X3rd_Salesperson__c = tempSplit.Salesperson__c;            
        }
        if(this.oSplitWrapperList.size() > 2){
            tempSplit = this.oSplitWrapperList.get(3).oSplit;
            this.opportunity.X4th_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.X4th_Salesperson__c = tempSplit.Salesperson__c;            
        }
        update this.opportunity;
    }*/
    
    /**
    * This method validates the user input and generates an appropriate error message.
    */
    private String generateErrorMessage(){
        Decimal splitTotal = 0;
        Decimal gapTotal = 0;
        String errorMessageString = '';
        for(Integer index=0;index < this.oSplitWrapperList.size(); index++){
            OpportunitySplitWrapper wrapper = this.oSplitWrapperList.get(index);
            if(wrapper.oSplit.Commission_Percentage__c != null) splitTotal = splitTotal + wrapper.oSplit.Commission_Percentage__c;
            if(wrapper.oSplit.GAP_Split__c != null) gapTotal = gapTotal + wrapper.oSplit.GAP_Split__c;
            for(Integer subindex=index+1; subindex < this.oSplitWrapperList.size();subindex++){
                if(wrapper.oSplit.Split_Comm_Rep__c == this.oSplitWrapperList.get(subindex).oSplit.Split_Comm_Rep__c){
                    errorMessageString = 'Multiple splits cannot be made to the same Salesperson';
                    return errorMessageString; 
                }
            }
        }
        
        if(splitTotal != 100){  
            errorMessageString = 'The Split amount across all Opportunity Split objects must total to 100';
            return errorMessageString; 
        }
        if(gapTotal > 25){  
            errorMessageString = 'The GAP Additional Percentage accross all Split objects must not be greater than 25%';
            return errorMessageString; 
        }  
        return '';
    }
    
    private Boolean insertNewObjects(){
        System.Debug('Boolean isChanged:::: ' + isChanged);
        if(this.oSplitWrapperList.size() == 0) return true;
        List<Split_Commissions__c> addList = new List<Split_Commissions__c>();
        List<Split_Commissions__c> updateList = new List<Split_Commissions__c>();
        for(OpportunitySplitWrapper wrapper: this.oSplitWrapperList){
            if(wrapper.oSplit.Opportunity__c == null){
                wrapper.oSplit.Opportunity__c = this.opportunity.Id;
            }
            if(wrapper.oSplit.Split_Comm_Rep__c == this.opportunity.OwnerId){
                wrapper.oSplit.Is_Opportunity_Owner__c = true;
            }
            if(isChanged)wrapper.oSplit.Approved_by_Rep__c = false;  //set to false if Splits have been modified
            if(wrapper.existing){
                updateList.add(wrapper.oSplit);
            }else{
                addList.add(wrapper.oSplit);
            }         
        }
        try{
            update updateList;
            insert addList;
            //upsert addList;
        }catch(System.DmlException exp){
            ApexPages.addMessages(exp);
            return false;
        }
        return true;
    }
    
    private List<Split_Commissions__c> getDeleteList(){
        List<Split_Commissions__c> deleteList = new List<Split_Commissions__c>();
        List<OpportunitySplitWrapper> wrapperList = this.deleteOSplitMap.values();
        for(OpportunitySplitWrapper wrapper: wrapperList){
            if(wrapper.oSplit.Id != null && !''.equals(wrapper.oSplit.Id))
            deleteList.add(wrapper.oSplit);
        }
        return deleteList;  
    }
    
    private void deleteObjects(){
        delete getDeleteList();
    }
    
    public PageReference unApprove()
    {
        if(oSplitWrapperList.size() == 0) return null;
        else
        {
            for(OpportunitySplitWrapper aWrapper:oSplitWrapperList)
            {
                aWrapper.oSplit.Approved_by_Rep__c = false;
            }
            return null;
        }
    }
    /**
    * On cancel, the user is redirected to the opportunity page.
    */
    public PageReference doCancel(){
        return new PageReference('/'+opportunity.id);
    }
    
    /**
    * This method updates the list with a new entry.
    */
    public PageReference addRow(){
        OpportunitySplitWrapper newOSplitWrapper = 
                new OpportunitySplitWrapper(this.idIterator++,new Split_Commissions__c(Is_Opportunity_Owner__c=false),false);
        this.oSplitWrapperList.add(newOSplitWrapper);
        return null;
    }
    
    /**
    * This method deletes the record from the list based on the id parameter passed.
    * The id used is a custom generated id.
    */
    public PageReference deleteRow(){
        String deleteIdString = ApexPages.currentPage().getParameters().get('deleteId');
        Integer deleteId = Integer.valueOf(deleteIdString);
        // Iterating over the list to identify the record to remove based on the id parameter.
        for(Integer index=0;index < this.oSplitWrapperList.size();index++){
            OpportunitySplitWrapper wrapper = this.oSplitWrapperList.get(index);
            if(wrapper.id == deleteId){
                this.oSplitWrapperList.remove(index);
                deleteOSplitMap.put(deleteId,wrapper);
                break;
            }
        }
        return null;
    }
    // Wrapper class for Opportunity Split object.
    class OpportunitySplitWrapper {
        public Boolean existing {get;set;}
        public Integer id {get;set;}
        public Split_Commissions__c oSplit {get;set;}
        public OpportunitySplitWrapper(Integer id,Split_Commissions__c oSplit,Boolean isExisting){
            this.existing = isExisting;
            this.id = id;
            this.oSplit = oSplit;
        }
    }
    
    //**************
    //Tests
    
    public static testMethod void testSplit(){
        test.startTest();        
        Opportunity opp = new Opportunity(Name='test',StageName='Prospect',CloseDate=Date.today());
        insert opp;
        test.stopTest();
    
        Split_Commissions__c commission = [Select Id,Split_Comm_Rep__c  from Split_Commissions__c where Opportunity__c = :opp.Id];
        commission.Commission_Percentage__c = 50;
        update commission;
        
        User user = [Select Id,Username,LastName,Email,Alias,CommunityNickname,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey from User where Id = :commission.Split_Comm_Rep__c];
        
        User cloneUser = user.clone(false,true);
        cloneUser.Username='testtest2011@test.com';
        cloneUser.CommunityNickname='testnick';
        insert cloneUser;
        
        Split_Commissions__c clone = commission.clone(false,true);
        clone.Commission_Percentage__c = 50;
        clone.Opportunity__c = opp.Id;
        clone.Split_Comm_Rep__c = cloneUser.Id;
        insert clone;
        
        //Split_Commissions__c commission  = new Split_Commissions__c(Opportunity__c = opp.Id,Commission_Percentage__c=0);
        //insert commission;
        
        SplitCommissionExtension extension = new SplitCommissionExtension(new ApexPages.StandardController(commission));
        System.debug('-----osplitsize----'+extension.oSplitWrapperList);
        extension.doSave();
        
        Integer oldSize = extension.oSplitWrapperList.size();
        extension.addRow();
        System.assertEquals(oldSize +1 , extension.oSplitWrapperList.size());
        
        extension.deleteObjects();
        
        PageReference pageRef = Page.ManageSplitCommission;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('deleteId',''+extension.oSplitWrapperList.get(0).Id);
        extension.deleteRow();
        extension.doCancel();
        
        
    }

}