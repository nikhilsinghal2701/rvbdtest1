global without sharing class BatchAssignLead implements Database.Batchable<sObject>, Database.Stateful {
	// Id BatchProcessId = Database.executeBatch(new BatchAssignLead(),5);
   global String Query;

   global BatchAssignLead() {
		this(false);
   }
   
   global BatchAssignLead(Boolean IsTest) {
   		Query = 'SELECT Id, IsConverted, Email, Phone, Fax, Company, WW_HQ__c, Probable_Lead_Category__c, OSKeyID__c, OS_Ultimate_Parent_Key_ID__c FROM Lead WHERE (IsConverted = False) AND (WW_HQ__c = \'\') ORDER BY CreatedDate DESC';
   		if (IsTest)
   			Query = Query + ' LIMIT 100';
   		else
   			Query = Query + ' LIMIT 100000';
   }

   global Database.QueryLocator start(Database.BatchableContext BC) {
      return Database.getQueryLocator(Query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		
   		List<Lead> toUpdate = new List<Lead>();
   		
   		for (sObject obj : scope) {
   			Lead l = (Lead) obj;
   			
   			if (l.IsConverted) continue;
   			
   			// FIND A MATCHING COMPANY
   	
	   		String AccountQuery = 'SELECT Id, Name, Email_Domain_excludes__c, Phone, Fax, WW_HQ__c, OneSource__OSKeyID__c FROM Account WHERE (IsDeleted = False) ';
	   		system.debug('AccountQuery: ' + AccountQuery);
	   		List<String> QueryParts = new List<String>();
	   		
	   		if (l.OSKeyID__c != null && l.OSKeyID__c != '') {
	   				QueryParts.add('(OSKeyID__c = \'' + String.escapeSingleQuotes(l.OSKeyID__c) + '\')');
	   		}
	   		
	   		if (l.OS_Ultimate_Parent_Key_ID__c != null && l.OS_Ultimate_Parent_Key_ID__c != '') {
	   				QueryParts.add('(WW_HQ__r.OSKeyID__c = \'' + String.escapeSingleQuotes(l.OS_Ultimate_Parent_Key_ID__c) + '\')');
	   		}
	   		
	   		if (l.Email != null && l.Email != '') {
	   			List<String> parts = l.Email.split('@');
	   			
	   			if(parts.size() == 2)
	   			{
	   				String domain = parts[1];
	   				if (!BatchAssignLead.ExcludeDomains(domain))
	   					QueryParts.add('(Email_Domain_excludes__c = \'' + String.escapeSingleQuotes(domain) + '\')');
	   			}
	   		}
	
			if (l.Phone != null && l.Phone != '') {
				String tempPhone = BatchAssignLead.CleanPhone(l.Phone);
				if (tempPhone != null && tempPhone.length() == 10) {
					String p1 = tempPhone.substring(0,3);
					String p2 = tempPhone.substring(3,6);
					String p3 = tempPhone.substring(6,10);
					QueryParts.add('(Phone LIKE \'%' + p1 + '%' + p2 + '%' + p3 + '%\')');
				}
				else {
					QueryParts.add('(Phone LIKE \'%' +  String.escapeSingleQuotes(tempPhone)  + '%\')');
				}
			}
			
			if (l.Fax != null && l.Fax != '') {
				String tempPhone = BatchAssignLead.CleanPhone(l.Fax);
				if (tempPhone != null && tempPhone.length() == 10) {
					String p1 = tempPhone.substring(0,3);
					String p2 = tempPhone.substring(3,6);
					String p3 = tempPhone.substring(6,10);
					QueryParts.add('(Fax LIKE \'%' + p1 + '%' + p2 + '%' + p3 + '%\')');
				}
				else {
					QueryParts.add('(Fax LIKE \'%' + tempPhone + '%\')');
				}
			}
			
			if (l.Company != null && l.Company != '') {			
				QueryParts.add('(Name = \'' + String.escapeSingleQuotes(l.Company) + '\')');
			}
			
			if (QueryParts.isEmpty()) return;
			
			String SubQuery = BatchAssignLead.buildQuery('OR', QueryParts);
			system.debug('SubQuery: ' + SubQuery);
			
			if (SubQuery == null || SubQuery == '') return;
			
			AccountQuery = AccountQuery + ' AND (' + SubQuery + ') LIMIT 1000';
			
			system.debug('AccountQuery2: ' + AccountQuery);
			List<Account> Accounts = Database.query(AccountQuery);
			
			if (Accounts.isEmpty()) return;
			
			Id AccountHQ = null;
			
			for (Account a : Accounts) {
				if (AccountHQ != null && AccountHQ != a.WW_HQ__c) {
					// If there is more than one Company referenced, don't use any
					AccountHQ = null;
					break;
				}
				AccountHQ = a.WW_HQ__c;
			}
			
			if (AccountHQ != null) {
				l.WW_HQ__c = AccountHQ;
				
				toUpdate.add(l);
				continue;
			}
			
   			// DONE: FIND A MATCHING COMPANY
   			
   			// TRY TO FIND A PROBABLE CATEGORY
   			
   			if (l.OSKeyID__c != null && l.OSKeyID__c != '') {
	   			List<Account> osMatches = new List<Account>([SELECT Id, OneSource__OSKeyID__c, Category__c FROM Account WHERE (OneSource__OSKeyID__c = :l.OSKeyID__c) AND (Category__c != null) LIMIT 20]);
	   			if (osMatches.IsEmpty()) continue;
	   			
	   			Set<String> CategorySet = new Set<String>();
	   			for (Account a : osMatches) {
	   				if (a.Category__c != null && a.Category__c != '') CategorySet.add(a.Category__c);
	   			}
	   			
	   			if (CategorySet.size() > 0) {
		   			String Category = 'Suspect';
		   			
		   			if (CategorySet.Contains('New'))
		   				Category = 'New';
		   			else if (CategorySet.Contains('Existing'))
		   				Category = 'Existing';
		   			else if (CategorySet.Contains('Suspect'))
		   				Category = 'Suspect';
		   			else if (CategorySet.Contains('Prospect'))
		   				Category = 'Prospect';
		   				
	   				l.Probable_Lead_Category__c = Category;
	   				
	   				toUpdate.add(l);
	   			}
   			}
   			
   			// DONE: TRY TO FIND A PROBABLE CATEGORY
   		}
   		
   		if (!toUpdate.IsEmpty())
   			update toUpdate;
   }
   
   global void finish(Database.BatchableContext BC){

   }
	
	public static String CleanPhone(String strNumber) {
    	if (strNumber == null) return null;
    	
    	// Remove all non-digits from the String
        String digits;
        pattern nonDigits = pattern.compile('[^0-9]');
        matcher matchDigits = nonDigits.matcher(strNumber);
        digits = matchDigits.replaceAll('');

		if (digits.Length() != 10) return strNumber; // If the number is not ten-digits long, then it is already clean
		return digits;
	}
	
	public static String buildQuery (String operator, List<String> subQueries) {
		if (operator == null || operator == '') operator = 'AND';
		if (subQueries == null || subQueries.isEmpty()) return null;
		
		String Query = null;
		for (String subQuery : subQueries) {
			if (subQuery == null || subQuery == '') continue;
			
			if (Query == null)
				Query = subQuery;
			else
				Query = Query + ' ' + operator + ' ' + subQuery;
		}
		
		if (Query != null) {
			Query = '(' + Query + ')';
		}
		
		return Query;
	}
	
	public static Boolean ExcludeDomains(String domain) {
		if (domain.equalsIgnoreCase('gmail.com')) return true;
		if (domain.equalsIgnoreCase('yahoo.com')) return true;
		if (domain.equalsIgnoreCase('msn.com')) return true;
		if (domain.equalsIgnoreCase('hotmail.com')) return true;
		if (domain.equalsIgnoreCase('aol.com')) return true;
		if (domain.equalsIgnoreCase('ymail.com')) return true;
		
		return false;
	}
}