/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class InstantBatchCtrlTest {

    static testMethod void allowRollupTest() {
        TestDataFactory tdf = new TestDataFactory();
        RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
         Account grandParent = tdf.createAccount();
        insert grandParent;
        Account parentAcct = tdf.createAccount();
    	parentAcct.ParentId = grandParent.Id;
        insert parentAcct;
        Account acct = tdf.createAccount();
        acct.ParentId = parentAcct.Id;
        insert acct;
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0 ; i<5 ; i++){
            String cntLname = 'cnt_email_00'+i;
            String emailStr = 'cntemail00'+i+'@gmail.com';
            contactList.add(tdf.createContact(acct.Id, emailStr ,cntLname));
        }
        if(!contactList.isEmpty()){
            insert contactList;
        }
        List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
        List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
        for(String str: certList){
            listOfCertMaster.add(tdf.createMasterCerts(str));
        }
        insert listOfCertMaster;
        List<Certificate__c> certs = new List<Certificate__c>(); 
        List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(acct.Id);
        listOfAcctCerts.addAll(tdf.retriveCertSummary(parentAcct.Id));
        for(Acct_Cert_Summary__c pc: listOfAcctCerts){
            String rssstr = pc.TAP_short__c != 'General' ? 'RSS'+'-'+pc.TAP_short__c: 'RSS';
            String rtssstr = pc.TAP_short__c != 'General' ? 'RTSS'+'-'+pc.TAP_short__c: 'RTSS';
            String rcspstr = pc.TAP_short__c != 'General' ? 'RCSP'+'-'+pc.TAP_short__c: 'RCSP';
            String rcsastr = pc.TAP_short__c != 'General' ? 'RCSA'+'-'+pc.TAP_short__c: 'RCSA';
            System.debug('rssstr:'+rssstr+' rtssstr:'+rtssstr+' rcspstr:'+rcspstr);
            for(Integer i=0; i<=pc.RSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rssstr));
            }
            for(Integer i=0; i<=pc.RTSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rtssstr));
            }
            for(Integer i=0; i<=2; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcspstr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcsastr));
            }
        }
        // Create Partner level config records
        PartnerLevelConfig__c plc = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'VAR');
        insert plc;
        PartnerLevelConfig__c plcRegular = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'Regular');
        insert plcRegular;
        Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
        insert certs;
        Test.startTest();
            ApexPages.StandardController ctrl = new ApexPages.Standardcontroller(parentAcct);
            InstantBatchCtrl ctrlRef = new InstantBatchCtrl(ctrl);
            ctrlRef.init();
        Test.stopTest();
        for(Acct_Cert_Summary__c pc :tdf.retriveCertSummary(parentAcct.Id)){
            System.assert(pc.Roll_Up_RSS__c >=0);
            System.assert(pc.Roll_Up_RTSS__c >=0);
            System.assert(pc.Roll_Up_RCSA_RCSP__c >= 0);
        }
        
    }
    
     static testMethod void donotAllowRollupTest() {
        TestDataFactory tdf = new TestDataFactory();
        RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
        Account parentAcct = tdf.createAccount();
        ParentAcct.Do_not_rollup_Certs__c = true;
        insert parentAcct;
        Account acct = tdf.createAccount();
        acct.ParentId = parentAcct.Id;
        insert acct;
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0 ; i<5 ; i++){
            String cntLname = 'cnt_email_00'+i;
            String emailStr = 'cntemail00'+i+'@gmail.com';
            contactList.add(tdf.createContact(acct.Id, emailStr ,cntLname));
        }
        if(!contactList.isEmpty()){
            insert contactList;
        }
        List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
        List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
        for(String str: certList){
            listOfCertMaster.add(tdf.createMasterCerts(str));
        }
        insert listOfCertMaster;
        List<Certificate__c> certs = new List<Certificate__c>(); 
        List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(acct.Id);
        for(Acct_Cert_Summary__c pc: listOfAcctCerts){
            String rssstr = pc.TAP_short__c != 'General' ? 'RSS'+'-'+pc.TAP_short__c: 'RSS';
            String rtssstr = pc.TAP_short__c != 'General' ? 'RTSS'+'-'+pc.TAP_short__c: 'RTSS';
            String rcspstr = pc.TAP_short__c != 'General' ? 'RCSP'+'-'+pc.TAP_short__c: 'RCSP';
            String rcsastr = pc.TAP_short__c != 'General' ? 'RCSA'+'-'+pc.TAP_short__c: 'RCSA';
            System.debug('rssstr:'+rssstr+' rtssstr:'+rtssstr+' rcspstr:'+rcspstr);
            for(Integer i=0; i<=pc.RSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rssstr));
            }
            for(Integer i=0; i<=pc.RTSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rtssstr));
            }
            for(Integer i=0; i<=2; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcspstr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcsastr));
            }
        }
        // Create Partner level config records
        PartnerLevelConfig__c plc = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'VAR');
        insert plc;
        Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
        insert certs;
        Test.startTest();
            ApexPages.StandardController ctrl = new ApexPages.Standardcontroller(parentAcct);
            InstantBatchCtrl ctrlRef = new InstantBatchCtrl(ctrl);
            ctrlRef.init();
        Test.stopTest();
        for(Acct_Cert_Summary__c pc :tdf.retriveCertSummary(parentAcct.Id)){
            System.assertEquals(pc.Roll_Up_RSS__c,null);
            System.assertEquals(pc.Roll_Up_RTSS__c,null);
            System.assertEquals(pc.Roll_Up_RCSA_RCSP__c,null);
        }
        
    }
    // test scheduler
     static testMethod void certSchedulerTest() {
        TestDataFactory tdf = new TestDataFactory();
        RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
        Account parentAcct = tdf.createAccount();
        insert parentAcct;
        Account acct = tdf.createAccount();
        acct.ParentId = parentAcct.Id;
        insert acct;
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0 ; i<5 ; i++){
            String cntLname = 'cnt_email_00'+i;
            String emailStr = 'cntemail00'+i+'@gmail.com';
            contactList.add(tdf.createContact(acct.Id, emailStr ,cntLname));
        }
        if(!contactList.isEmpty()){
            insert contactList;
        }
        List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
        List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
        for(String str: certList){
            listOfCertMaster.add(tdf.createMasterCerts(str));
        }
        insert listOfCertMaster;
        List<Certificate__c> certs = new List<Certificate__c>(); 
        List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(acct.Id);
        for(Acct_Cert_Summary__c pc: listOfAcctCerts){
            String rssstr = pc.TAP_short__c != 'General' ? 'RSS'+'-'+pc.TAP_short__c: 'RSS';
            String rtssstr = pc.TAP_short__c != 'General' ? 'RTSS'+'-'+pc.TAP_short__c: 'RTSS';
            String rcspstr = pc.TAP_short__c != 'General' ? 'RCSP'+'-'+pc.TAP_short__c: 'RCSP';
            String rcsastr = pc.TAP_short__c != 'General' ? 'RCSA'+'-'+pc.TAP_short__c: 'RCSA';
            System.debug('rssstr:'+rssstr+' rtssstr:'+rtssstr+' rcspstr:'+rcspstr);
            for(Integer i=0; i<=pc.RSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rssstr));
            }
            for(Integer i=0; i<=pc.RTSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rtssstr));
            }
            for(Integer i=0; i<=2; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcspstr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcsastr));
            }
        }
        // Create Partner level config records
        PartnerLevelConfig__c plc = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'VAR');
        insert plc;
        Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
        insert certs;
        Test.startTest();
            String CORN_EXP = '0 16 05 * * ?';
            CertRollupScheduler test1 = new CertRollupScheduler();
           // Database.executebatch(Batch_Customer_old_scheduler.scheduleMe());
           // system.schedule('my batch', CORN_EXP, test1);
            string jobid = system.schedule('my test batch job', CORN_EXP, new CertRollupScheduler() );
           CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId]; 
           System.assertEquals(ct.NextFireTime.hour(),5);
        Test.stopTest();
    }
}