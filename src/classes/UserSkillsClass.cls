//Added by Santoshi on Jan 2011
public with sharing class UserSkillsClass {
//public List<User_Skills__c> userList {get;set;}
public List<User_Skills__c> trainingUserList {get;set;}
public List<User_Skills__c> SMEUserList {get;set;}
User usr ;
public List<SelectOption> smeSkillLevel;
public List<SelectOption> trainingSkillLevel;
private string smeSkillLevel1;
private string trngSkillLevel1;

    
    public UserSkillsClass(ApexPages.StandardController cont){
        //usr=(User) cont.getRecord();
        SMEUserList=new List<User_Skills__c>();
        trainingUserList=new List<User_Skills__c>();
        Map<Id,User_Skills__c> userList=new Map<Id,User_Skills__c>( [select SME_Skills__c,Training_Skills__c,level__c,user__c,id, SME_Skill_Level__c from User_Skills__c where user__c=:System.currentPageReference().getParameters().get('Id')  ]);
        
        if(userList==null)
        {
            
            addUserSkills();
            addTrainingSkills();
            
        }
        if(userList!=null)
        {
            
        for(User_Skills__c u:userList.Values())
        {     
            if(u.Training_Skills__c==null)
            {
            //userList.get(u.Id).SME_Skill_Level__c=smeSkillLevel1;
            SMEUserList.add(userList.get(u.Id));
            }
            else if(u.SME_Skills__c==null)
            {
            //userList.get(u.Id).level__c=trngSkillLevel1;
            trainingUserList.add(userList.get(u.Id));
            }
        }   
        }
    }
    
    public List<SelectOption> getSMESkillLevel(){
        smeSkillLevel=new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = User_Skills__c.SME_Skill_Level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
        system.debug('Picklistentry:'+'ple:::'+ple.size());
        for( Schema.PicklistEntry f : ple){
            smeSkillLevel.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        return smeSkillLevel;
    }
    public void setSMESkillLevel(List<SelectOption> options){
        this.SMESkillLevel=options;
    }
    public void setSmeSkillLevel1(string smeSkillLevel1){
        this.smeSkillLevel1=smeSkillLevel1;
    }
    public string getSmeSkillLevel1(){
        return smeSkillLevel1;
    }
    public List<SelectOption> getTrainingSkillLevel(){
        trainingSkillLevel=new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = User_Skills__c.Level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
        system.debug('Picklistentry:'+'ple:::'+ple.size());
        for( Schema.PicklistEntry f : ple){
            trainingSkillLevel.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        return trainingSkillLevel;
        
    }
    public void setTrainingSkillLevel(List<SelectOption> options){
        this.trainingSkillLevel=options;
    }
    public void setTrngSkillLevel1(string trngSkillLevel1){
        this.trngSkillLevel1=trngSkillLevel1;
    }
    public string getTrngSkillLevel1(){
        return trngSkillLevel1;
    }

    public void addUserSkills(){
        User_Skills__c usr = new User_Skills__c();
        SMEUserList.add(usr);
    }
    public void addTrainingSkills(){
        User_Skills__c usr = new User_Skills__c();
        trainingUserList.add(usr);
    }
    public void Save()
    {
        List<User_Skills__c> finallist = new List<User_Skills__c>();
        List<User_Skills__c> delList = new List<User_Skills__c>();
        for(User_Skills__c u :SMEUserList)
        {
            if(u.SME_Skills__c!=null &&!u.SME_Skills__c.contains('None') && !u.SME_Skill_Level__c.contains('None')&&!u.SME_Skill_Level__c.contains('No'))
            //if(u.SME_Skills__c!=null &&!u.SME_Skills__c.contains('None') && u.SME_Skill_Level__c!=null)
            {
                u.User__c=System.currentPageReference().getParameters().get('Id');
                finallist.add(u);
            }
            else if(u.SME_Skill_Level__c!=null&&u.SME_Skill_Level__c.contains('No') && u.Id!=null)//modified by prashant.singh@riverbed.com;date 6/18/2012;ticket#114221
            {
                delList.add(u);
            }
            
        }
        SMEUserList=finallist;
        if(!finallist.isempty())
        
        upsert(finallist);
        if(!delList.isempty())
        {
            delete (delList);
        }
        
    }
    
    public void SaveTraining()
    {
        List<User_Skills__c> finallist = new List<User_Skills__c>();
        List<User_Skills__c> delList = new List<User_Skills__c>();
        for(User_Skills__c u :trainingUserList)
        {
            if(u.Training_Skills__c!=null &&!u.Training_Skills__c.contains('None') && !u.level__c.contains('None')&&!u.level__c.contains('0'))
            //if(u.Training_Skills__c!=null &&!u.Training_Skills__c.contains('None') && u.level__c!=null &&!u.level__c.contains('0'))
            {
                u.User__c=System.currentPageReference().getParameters().get('Id');
                finallist.add(u);
            }
            else if(u.level__c!=null&&u.level__c.contains('0') && u.Id!=null)
            {
                delList.add(u);
            }
            
        }
        trainingUserList=finallist;
        if(!finallist.isempty())
        
        upsert(finallist);
        if(!delList.isempty())
        {
            delete (delList);
        }
        
    }
    
    @ istest
    public static void UserSkillsClassTest()
    {
        test.starttest();
        User u=[select Id from user limit 1];
        PageReference VFpage = Page.User_Skills_Page;
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(u);
        UserSkillsClass CLS = new UserSkillsClass(VFpage_Extn);
         cls.addUserSkills();
        User_Skills__c u1  = new User_Skills__c(SME_Skill_Level__c='1',SME_Skills__c='SH');
        User_Skills__c u2  = new User_Skills__c(SME_Skill_Level__c='2',SME_Skills__c='Casade');
        CLS.SMEUserList.add(u1);
        CLS.SMEUserList.add(u2);
        cls.Save();
        CLS.getSMESkillLevel();
        u1.SME_Skill_Level__c='No';
         cls.Save();
         User u11=[select Id from user where id != :u.id limit 1];
         VFpage = Page.User_Skills_Page;
        test.setCurrentPage(VFpage);
         VFpage_Extn = new ApexPages.StandardController(u11);
        CLS = new UserSkillsClass(VFpage_Extn);
          cls.addTrainingSkills();
        User_Skills__c u3  = new User_Skills__c(Level__c='1',Training_Skills__c='White Water');
        User_Skills__c u4  = new User_Skills__c(Level__c='2',Training_Skills__c='SharePoint');
        CLS.trainingUserList.add(u3);
        CLS.trainingUserList.add(u4);
        CLS.getTrainingSkillLevel();
        cls.SaveTraining();
        u4.Level__c='0';
         cls.SaveTraining();
        test.stoptest();
    }
    
}