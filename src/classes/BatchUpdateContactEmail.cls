global class BatchUpdateContactEmail implements Database.Batchable<Sobject> {
global final List<Contact> conLst=new List<Contact>();
global final String value;
global BatchUpdateContactEmail(string sandbox) {
value = sandbox;
}
global Database.QueryLocator start(Database.BatchableContext BC) {
return Database.getQueryLocator([Select c.Id,c.Email from Contact c]);
}
global void execute(Database.BatchableContext BC, List<Contact> records) {
for(Contact con:records){
if(con.Email!=Null){
con.Email=con.Email+'.'+value;
conLst.add(con);
}
}
if(conLst.size()>0) update conLst;
}
global void finish(Database.BatchableContext BC) {
}
}