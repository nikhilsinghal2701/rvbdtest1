/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class DiscountApprovalEmail implements Messaging.InboundEmailHandler {
	public enum Action {APPROVE, REJECT, CANCELL, INFO}
	public static boolean isTest = false;// Added by Santoshi on Dec-2011
	public static final String QUOTE_DELIM = 'Quote Id: ';
	private static final String COM_DELIM = '\n';
	public static final String EXT_COM_DELIM = 'EOM';
	private static final String EXT_COM_DELIM1 = 'SENT FROM';
	private static final String EXT_COM_DELIM2 = 'FROM: RIVERBED DISCOUNT APPROVAL';
	private static final String PENDING = 'Pending';
	private static final String APPROVE = 'Approve';
	private static final String REJECT = 'Reject';
	private static final String INFO = 'More Info';
	private static final String INSTRUCTIONS = 'Please reply to the Discount Approval notification (include the email body) with one of the following commands on the first line:\n\n'		
			+ '  approve\n'
			+ '  reject\n'
			+ '  more info\n\n'
			+ 'To enter comments, type one or more space characters after the command and enter text. Hit return.\n\n'
			+ 'To enter in multi-line comments, end the comments with the text: ' + EXT_COM_DELIM.toLowercase();
	
	private static final String USER_ERROR = 'User Error: ';											
	private static final String INVALID_USER = 'You are not permitted to perform this action.';

	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
		String plainText = email.plainTextBody;
		String fromEmail = email.fromAddress; 
		try {
			if (plainText == null){
				plainText = email.HTMLBody;
			}
			if (plainText != null){
				plainText = plainText.trim();
			} else {
				result.message = USER_ERROR + 'No email body found. Remember to include the orignal email in your reply.\n\n' + INSTRUCTIONS;
				result.success = false;	
				return result;
			}
			
			Id qId = getQuoteId(plainText);
			if (qId == null) {
				result.message = USER_ERROR + 'No Quote Id found in email body. Remember to include the orignal email in your reply.\n\n' + INSTRUCTIONS;
				result.success = false;	
				return result;
			}
	    
			Action act = getAction(plainText);
			if (act == null) { 
				result.message = USER_ERROR + 'No valid command found in email body. \n\n' + INSTRUCTIONS;
				result.success = false;	
				return result;
			}	

			Quote__c q = getQuote(qId);
			String comments = getComments(plainText, act);
			Set<Id> apprIds = DiscountApprovalHlp.extractApproverIds(q);	
			if (q.Submitter__c != null){
				apprIds.add(q.Submitter__c);
			}
		System.debug(LoggingLevel.INFO,'insideeeeeeeeeeee++++++++'  );
			User u = DiscountApprovalHlp.getUser(fromEmail, apprIds);
			System.debug(LoggingLevel.INFO,'uuuuuuuuuu++++++222' +u+'from--' +fromEmail+'apprIds--'+apprIds);
			System.debug('User:' + u);
			if (u == null) {
				result.message = INVALID_USER + ': ' + fromEmail + q;
				result.success = false;	
				System.debug(LoggingLevel.INFO,'insideeeeeeeeeeee++++++222'  );
				return result;		
			} else {
				if (act == Action.INFO) {
					DiscountApprovalEmailHlp emailSvc = DiscountApprovalEmailHlp.getInstance();
					emailSvc.doMoreInfo(q.Id, comments, u);
					emailsvc.flush();
					System.debug(LoggingLevel.INFO,'insideeeeeeeeeeee++++++++333'  );
					return null;
				}
			}
		System.debug(LoggingLevel.INFO,'insideeeeeeeeeeee++++++++111'  );
			Id wId = getWorkItem(qId, u.Id);
			System.debug(LoggingLevel.INFO,'checking---11' + wid );
			if (wId == null){ 
				result.success = true;	
				return result;
			}
			doAction(q, u, wId, act,comments, fromEmail);
		} catch (Exception ex) {
			result.message = ex.getMessage();
			System.debug('Unexpected Exception: ' + ex + '\n\n\n' + plainText);
			result.success = false;
		}
		return result;
	}

	private String doAction(Quote__c q, User u, Id wId, Action act, String comments, String fromEmail){
		NSD_Approval_History__c hist = null;
		String status = null;	
		Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
		if (act == Action.APPROVE) {
			req.setAction(APPROVE);
			status = QuoteApprovalHlp.STATUS_APPROVED;
			//Discount_Approver__c approver = new Discount_Approver__c(Approver__c = u.Id, Quote__c = q.Id);
			//insert approver;
		} else if (act == Action.REJECT) {
			req.setAction(REJECT);
			status = QuoteApprovalHlp.STATUS_REJECTED;
		}
		
		req.setComments(u.Name + ': ' + comments);
		req.setWorkItemId(wId);
		hist = DiscountApprovalHlp.createApprovalRecord(u.Id, u.Name, q, DateTime.now(), status,q.Next_Approver__c);
		Approval.ProcessResult result =  Approval.process(req);
		System.assert(result.isSuccess(), 'Result Status: ' + result.isSuccess());	
		insert hist;
		
		return null;	
	}

	private Quote__c getQuote(Id qId){
		return  ([SELECT Name, 
		          Id,
		          Submitter__c,Discount_Status__c,
		          Next_Approver__c,
		          Approver__c,
		          Approver9__c, Approver8__c, Approver7__c, Approver6__c, Approver5__c, Approver4__c, Approver3__c, Approver2__c, Approver1__c, Approver10__c,
		          Opportunity__c 
		          FROM Quote__c 
		          WHERE Id = :qId]);
	}
	
	public static Id getWorkItem(Id qId, Id uId){
		
		if(istest==true)//Added by Santoshi on December - 2011
		{
			String qids=qid;
			String uids=uid;
			if(qids.length()==18)
			{
				qids=qids.substring(0,qids.length()-3);
				qid=qids;
			}
			if(uids.length()==18)
			{
				uids=uids.substring(0,uids.length()-3);
				uid=uids;
			}
		}
		ProcessInstanceWorkitem[] wi = 
			[SELECT ProcessInstance.Status, 
			 ProcessInstance.TargetObjectId, 
			 ProcessInstanceId, 
			 OriginalActorId, 
			 Id, 
			 ActorId
			 FROM ProcessInstanceWorkitem
			 WHERE ProcessInstance.TargetObjectId = :qId AND ProcessInstance.Status = :PENDING AND OriginalActorId = :uId];	
System.debug('checking---'+wi);
System.debug(LoggingLevel.INFO,'checking---' + wi );
		if (wi.size() > 0){
			return wi[0].Id;
		} else {
			return null;		
		}		
	}
	
	private Id getQuoteId(String body){	
		Id qId = null;
		if (body.contains(QUOTE_DELIM)) {
			String strId = body.substring(body.indexOf(QUOTE_DELIM) + QUOTE_DELIM.length());
			if (strId.length() >= 15){
				qId = strId.substring(0,15);
			}
		}
		return qId;
	}
	
	private Action getAction(String body){
		Action act = null;
		if (body.toUpperCase().startsWith(APPROVE.toUppercase())) {
			return Action.APPROVE;
		} else if (body.toUpperCase().startsWith(REJECT.toUppercase())) {
			return Action.REJECT;
		} else  {	//if (body.toUpperCase().startsWith(INFO.toUppercase())) - all other responses will be handled as More Info
			return Action.INFO;
		}
		return null;		
	}

	private String getComments(String body, Action act){
		String text = '';
		Integer commandLen = 0;
		if (act == Action.APPROVE){
			commandLen = APPROVE.length();
		} else if (act == Action.REJECT) {
			commandLen = REJECT.length();
		} else if (act == Action.INFO) {	
			commandLen = INFO.length();
		}
		System.debug(LoggingLevel.INFO,'BODY = --'+body.toUpperCase().contains(EXT_COM_DELIM1)+'000--'+body);
		if (body.toUpperCase().contains(EXT_COM_DELIM)) {
			Integer i = 0;
			if(body.toUpperCase().contains(EXT_COM_DELIM1)){
				i = body.toUpperCase().indexOf(EXT_COM_DELIM1);
			}else if(body.toUpperCase().contains(EXT_COM_DELIM2)){
				i = body.toUpperCase().indexOf(EXT_COM_DELIM2);
			}
			if(i==0 || i > body.toUpperCase().indexOf(EXT_COM_DELIM)){
				text = body.substring(0, body.toUpperCase().indexOf(EXT_COM_DELIM));
			}else{
				if(body.toUpperCase().contains(EXT_COM_DELIM1)){
					text = body.substring(0, body.toUpperCase().indexOf(EXT_COM_DELIM1));
				}else if(body.toUpperCase().contains(EXT_COM_DELIM2)){
					text = body.substring(0, body.toUpperCase().indexOf(EXT_COM_DELIM2));
				}
			}
			text = text.trim();
			System.debug('First if');
		} else if (body.contains(COM_DELIM) ) {//&& body.indexOf(COM_DELIM) > commandLen + 1
			System.debug('New line');
			text = body.substring(0, body.indexOf(COM_DELIM));
			text = text.trim();
		}else if(body.toUpperCase().contains(EXT_COM_DELIM1)){
			System.debug('No New line, Sent regular');
			text = body.substring(0, body.toUpperCase().indexOf(EXT_COM_DELIM1));
			text = text.trim();
		}else if(body.toUpperCase().contains(EXT_COM_DELIM2)){
			System.debug('No New line, Sent regular');
			text = body.substring(0, body.toUpperCase().indexOf(EXT_COM_DELIM2));
			text = text.trim();
		}
		System.debug('text = ' + text);
		if (text.length() > commandLen && act != Action.INFO){
			text = text.substring(commandLen);
			System.debug('new text = '+ text);
		}
		text = text.trim();
		return text;
	}
}