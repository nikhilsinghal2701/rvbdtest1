/*
	Class: CertRollupScheduler
	Purpose: calls CertRollupBatch batch class which calculates the cert roll ups..
				Note: This class takes care of only three levels of account hirearchy. 
	Author: Jaya ( Perficient )
	Created Date: 7/28/2014					
	Reference: pc=> Partner Competency	
*/

global without sharing class CertRollupScheduler implements Schedulable{
     global String queryStr;
     global void execute(SchedulableContext SC) 
     {
      CertRollupBatch stageBatch = new CertRollupBatch(); 
      Database.executeBatch(stageBatch);
   	}

}