/*******************************************************************************************
// Class Name: TerritoryPlanningExtension
//
// Description: This class is the controller class for the TerritoryPlanView and  TerritoryPlanning VF page.
// This class Support New,view and Edit Functionality
//
// Created By: Nikhil Singhal (Perficient) - 7/10/2014
*******************************************************************************************/




public with sharing class TerritoryPlanningExtension {
    public  Territory__c  Tr {get; set;}
    public List<Territory_Plan_Detail__c> TPDList {get;set;}
    public Territory_Plan_Detail__c Mtier1{get; set;}
    public Territory_Plan_Detail__c Mtier2{get; set;}
    public Territory_Plan_Detail__c Mtier3{get; set;}
    public Territory_Plan_Detail__c Atier1{get; set;}
    public Territory_Plan_Detail__c Atier2{get; set;}
    public Territory_Plan_Detail__c Atier3{get; set;}
    public Territory_Plan_Detail__c ctier1{get; set;}
    public Territory_Plan_Detail__c ctier2{get; set;}
    public Territory_Plan_Detail__c ctier3{get; set;}
    public  String Tpowner {get; set;}
    public List<Attachment> attachments {get; set;}
 	public Attachment attachment1 {get; set;}
 	public List<Territory_Plan__History> TPH  {get;set;}
 	public List<FieldLabel> Wrows {get; set;}
	String type='Territory_Plan__c';
	public Boolean hasaccess {get; private set; }
	
	
	
    private final Territory_Plan__c Tp;
    public Territory_Plan__c Tpv{get; set;}
    public Territory_Plan__c Tpc{get; set;}
    
        //Variables for Territory Team
    public Territory_Planning__c deltp;
    public List<Territory_Planning__c> TpList {get;set;}
    public List<Territory_Planning__c> deltpList {get;set;}
    public Integer TprowNum{get;set;}
    // End oF Territory Team
    
    //Variables for Territory Team
    public Territory_Team__c delt;
    public List<Territory_Team__c> TList {get;set;}
    public List<Territory_Team__c> delTList {get;set;}
    public Integer TrowNum{get;set;}
    // End oF Territory Team
    
    public TerritoryPlanningExtension(ApexPages.StandardController controller) { 
                if (!Test.isRunningTest())  {
                controller.addFields(new String[]{'Territory__c'});
                }
                
                Wrows=New List<FieldLabel>();
                Tp = (Territory_Plan__c)controller.getRecord();
                Tpc=new Territory_Plan__c();
                
                if(ApexPages.currentPage().getParameters().get('tid') != null){
               Tp.Territory__c=ApexPages.currentPage().getParameters().get('tid');
               }
               
               
                Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
				Schema.SObjectType leadSchema = schemaMap.get(type);
				Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
               
               
               if(Tp.id != null){
               Tpv=[SELECT CreatedById, CreatedDate, CurrencyIsoCode, IsDeleted, Fiscal_Year__c, LastModifiedById, 
               LastModifiedDate, OwnerId, Id, Status__c, SystemModstamp, TSE__c, Territory__c, Territory_Competitors__c, 
               Name, Territory_Top_Customers__c, Tier_1_of_Accounts__c, Tier_2_of_Accounts__c, 
               Tier_3_of_Accounts__c,owner.name FROM Territory_Plan__c where id=:tp.id];
               
               hasaccess=[select RecordId, HasEditAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :Tp.id].HasEditAccess;
               
                attachment1 = new Attachment();
		        attachments = [ SELECT Id, Name,CreatedBy.name, BodyLength, CreatedById, LastModifiedDate ,CreatedDate
		                        FROM Attachment 
		                        WHERE parentId =: Tp.Id ORDER BY CreatedDate DESC];
		                        
		                        
               TPH =[ SELECT Field, CreatedById,CreatedBy.name, CreatedDate, IsDeleted, Id, NewValue, OldValue, ParentId FROM Territory_Plan__History where parentid=: tp.Id];
                 
                 
                 for(Territory_Plan__History a : TPH){
                 	if(a.Field == 'created'){
                 	Wrows.add(new FieldLabel('created',a));
                 	}else if(fieldMap.containskey(a.field)){
                 	Wrows.add(new FieldLabel(fieldMap.get(a.field).getDescribe().getLabel(),a));
                 	}else{
                 	Wrows.add(new FieldLabel(a.field,a));
                 	}
                 }
               
               
               }
               
                if(Tp.id != null){
                	Tpowner=Tpv.owner.name;
                }else{
                	Tp.ownerid=UserInfo.getUserId();
                 }
                 
                Tr=[Select ownerid,name,CreatedDate,SystemModstamp,TSE__r.name from Territory__c where id=:Tp.Territory__c];
                TPDList = new List<Territory_Plan_Detail__c>();
                
                TpList = new List<Territory_Planning__c>();
                deltpList= new List<Territory_Planning__c>();
               if(Tp.id != null){
                TpList =[SELECT Assessment_Area__c,Score__c, Id, SystemModstamp, Territory_Plan__c, Name FROM Territory_Planning__c where Territory_Plan__c =: Tp.Id];
                if(TpList.isEmpty()){
                TpList.add(new Territory_Planning__c());
                }
               }else{
                TpList.add(new Territory_Planning__c());
               }
               
                TList = new List<Territory_Team__c>();
                delTList= new List<Territory_Team__c>();
               if(Tp.id != null){
                TList =[Select id,User__c,Role__c,Territory_Plan__c,Access__c,CreatedDate,User__r.name from Territory_Team__c where Territory_Plan__c =: Tp.Id];
                if(TList.isEmpty()){
                TList.add(new Territory_Team__c());
                }
               }else{
                 TList.add(new Territory_Team__c());
               }
               
               if(Tp.id != null){
               	try{
                Mtier1 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier1' and Type__c='Marketing / Pipeline'];
                }catch(exception e){
                Mtier1 = new Territory_Plan_Detail__c(Type__c='Marketing / Pipeline',Segment__c='Tier1');
   				}
               	try{
                Mtier2 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier2' and Type__c='Marketing / Pipeline'];
                }catch(exception e){
                Mtier2 = new Territory_Plan_Detail__c(Type__c='Marketing / Pipeline',Segment__c='Tier2');
   				}
                try{
                Mtier3 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier3' and Type__c='Marketing / Pipeline'];
                }catch(exception e){
                Mtier3 = new Territory_Plan_Detail__c(Type__c='Marketing / Pipeline',Segment__c='Tier3');
   				}
                try{
                Atier1 = [SELECT Action_Items__c, Existing_Accounts__c, Prospect_Accounts__c, Suspect_Accounts__c,  Expected_Revenue__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier1' and Type__c='Account Coverage'];
                }catch(exception e){
                Atier1 = new Territory_Plan_Detail__c(Type__c='Account Coverage',Segment__c='Tier1');
   				}
                try{
                Atier2 = [SELECT Action_Items__c, Existing_Accounts__c, Prospect_Accounts__c, Suspect_Accounts__c,  Expected_Revenue__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier2' and Type__c='Account Coverage'];
                }catch(exception e){
                Atier2 = new Territory_Plan_Detail__c(Type__c='Account Coverage',Segment__c='Tier2');
   				}
                try{
                Atier3 = [SELECT Action_Items__c, Existing_Accounts__c, Prospect_Accounts__c, Suspect_Accounts__c,  Expected_Revenue__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier3' and Type__c='Account Coverage'];
               	}catch(exception e){
                Atier3 = new Territory_Plan_Detail__c(Type__c='Account Coverage',Segment__c='Tier3');
   				}
   				 try{
                ctier1 = [SELECT Action_Items__c, Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier1' and Type__c='Channel/Partner'];
                }catch(exception e){
                ctier1 = new Territory_Plan_Detail__c(Type__c='Channel/Partner',Segment__c='Tier1');
   				}
                try{
                ctier2 = [SELECT Action_Items__c,Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier2' and Type__c='Channel/Partner'];
                }catch(exception e){
                ctier2 = new Territory_Plan_Detail__c(Type__c='Channel/Partner',Segment__c='Tier2');
   				}
                try{
                ctier3 = [SELECT Action_Items__c,Development_Initiatives__c, Id, Segment__c,Territory_Plan__c, Name, Type__c 
                FROM Territory_Plan_Detail__c where Territory_Plan__c=:Tp.id and Segment__c='Tier3' and Type__c='Channel/Partner'];
               	}catch(exception e){
                ctier3 = new Territory_Plan_Detail__c(Type__c='Channel/Partner',Segment__c='Tier3');
   				}
               
               } else{
                Mtier1 = new Territory_Plan_Detail__c(Type__c='Marketing / Pipeline',Segment__c='Tier1');
                Mtier2 = new Territory_Plan_Detail__c(Type__c='Marketing / Pipeline',Segment__c='Tier2');
                Mtier3 = new Territory_Plan_Detail__c(Type__c='Marketing / Pipeline',Segment__c='Tier3');
                Atier1 = new Territory_Plan_Detail__c(Type__c='Account Coverage',Segment__c='Tier1');
                Atier2 = new Territory_Plan_Detail__c(Type__c='Account Coverage',Segment__c='Tier2');
                Atier3 = new Territory_Plan_Detail__c(Type__c='Account Coverage',Segment__c='Tier3');
                Ctier1 = new Territory_Plan_Detail__c(Type__c='Channel/Partner',Segment__c='Tier1');
                Ctier2 = new Territory_Plan_Detail__c(Type__c='Channel/Partner',Segment__c='Tier2');
                Ctier3 = new Territory_Plan_Detail__c(Type__c='Channel/Partner',Segment__c='Tier3');
               }
               
               TPDList.add(Mtier1);
               TPDList.add(Mtier2);
               TPDList.add(Mtier3);
               TPDList.add(Atier1);
               TPDList.add(Atier2);
               TPDList.add(Atier3);
               TPDList.add(Ctier1);
               TPDList.add(Ctier2);
               TPDList.add(Ctier3);
               

    }
    
    
       // Start Add /Delete Row Methods for each object
    
      public void AddT()
    {
                 TList.add(new Territory_Team__c());
    }
    public void delT()
    {
        TrowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('Tindex'));
        delt=TList.remove(TrowNum);
        if(delt.id != null){
        delTList.add(delt);  } 
    }
    
    public void AddTp()
    {
                TpList.add(new Territory_Planning__c());
    }
    public void delTp()
    {
        TprowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('Tpindex'));
        deltp=TpList.remove(TprowNum);
        if(deltp.id != null){
        deltpList.add(deltp);  } 
    }
    
       // End Add /Delete Row Methods for each object
    
       // Start Save Methods for each object
    
    
    public PageReference Save(){
    	
    	   for(Territory_Team__c apt: TList)   {
             if(apt.User__c == Tp.OwnerId){
ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'It is not necessary to add the territory plan owner to the territory team.'));
            return null;
            }
    	}
    	
    	
    	
    	
        upsert Tp;
            for(Territory_Team__c tl:TList) {
             if(tl.Territory_Plan__c == null)
            tl.Territory_Plan__c=Tp.id;
        }
        upsert TList;
        if(!delTList.IsEmpty())
        delete delTList;
         return (new ApexPages.StandardController(Tp)).view();
    }
        
         public PageReference Savetpd(){
        for(Territory_Plan_Detail__c tpd:TPDList){
            if(tpd.Territory_Plan__c == null)
            tpd.Territory_Plan__c=Tp.id;
        }
        upsert TPDList;
         return (new ApexPages.StandardController(Tp)).view();
    }
        
    
        
        
        
        public PageReference Savetp(){ 
        for(Territory_Planning__c tpl:TpList)   {
             if(tpl.Territory_Plan__c == null)
            tpl.Territory_Plan__c=Tp.id;
        }
        upsert TpList;
        if(!delTpList.IsEmpty())
        delete deltpList;
        return (new ApexPages.StandardController(Tp)).view();
    }
    
    
    
    
    public PageReference Cancel(){
     return new PageReference(''+ApexPages.currentPage().getParameters().get('retURL'));
 }
 
 public PageReference Cancelv(){
     return new PageReference('/'+tpv.Territory__c);
 }
 
 public PageReference Editv(){
     PageReference Pageref= new PageReference('/apex/TerritoryPlanning?id='+tpv.id+'&retURL='+tpv.Territory__c);
     Pageref.setRedirect(true);
     return Pageref;
 }
 
 //Clone MEthod
 public PageReference CClone(){
	Tpc.Fiscal_Year__c=Tpv.Fiscal_Year__c;
	Tpc.Status__c=Tpv.Status__c;
	Tpc.TSE__c=Tpv.TSE__c;
	Tpc.Territory__c=Tpv.Territory__c;
	Tpc.Territory_Competitors__c=Tpv.Territory_Competitors__c;
	Tpc.Territory_Top_Customers__c=Tpv.Territory_Top_Customers__c;
	Tpc.Tier_1_of_Accounts__c=Tpv.Tier_1_of_Accounts__c;
	Tpc.Tier_2_of_Accounts__c=Tpv.Tier_2_of_Accounts__c;
	Tpc.Tier_3_of_Accounts__c=Tpv.Tier_3_of_Accounts__c;
 	upsert tpc;
  return (new ApexPages.StandardController(Tpc)).Edit();
 }
 public PageReference upload(){
    	if (attachment1.name == null){
 	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select File'));
      	return null;
  		}
    	if (attachment1.Body.size() > 5242880){
 	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment 1: Max attachment size is 5 Mb'));
      	return null;
  		}
        attachment1.ParentId = Tpv.Id;
        attachment1.IsPrivate=false;
        insert attachment1;
        attachment1.body = null;
        attachment1.clear();
        attachment1 = new Attachment();
        Pagereference p = new Pagereference( '/apex/TerritoryPlanView?id='+Tpv.Id);
        p.setRedirect( true ); 
        return p;
    }
    
     // WrapperClass For  history field labels 
		     public without sharing class FieldLabel{
		public String Name      {get;set;}
		public Territory_Plan__History APHs {get;set;}
		public FieldLabel(String c , Territory_Plan__History s){
		this.Name=c;
		this.APHs=s;
		}
} 

    
}