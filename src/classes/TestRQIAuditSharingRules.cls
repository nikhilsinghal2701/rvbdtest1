@IsTest
private class TestRQIAuditSharingRules {

    static testMethod void testRQISharing() {
        TestAQIRQIUtils testTools = new TestAQIRQIUtils();
    	User testUser = testTools.createMember();
    	
    	Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yowww');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test3';
        cObj.Environment_Details__c = 'Test3';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test3';
        cObj.Issue_First_Seen__c = 'Test3';
        cObj.Problem_Definition__c = 'Test3';
        cObj.Problem_Details__c = 'Test3';
        cObj.Recent_Changes__c = 'Test3';
        cObj.Relevant_Data__c = 'Test3';
        cObj.Reproducibility__c = 'Test3';
        cObj.Version__c = 'Test3';
        cObj.Staff_Engineer__c = 'Chriss Gearyy';
        update cObj;
        
        Test.startTest();
        cObj.Re_escalation_Reason__c = 'Test3';
        cObj.Current_Status__c = 'Test3';
        cObj.Status = 'Closed';
        update cObj;
        
		List<case> needOneCase = [select id from case limit 1];
		
		RQI_Audit__c assessment = new RQI_Audit__c(Case__c = needOneCase[0].Id, Answer_No_1__c  = 'No' , Answer_No_2__c  = 'Yes', Answer_No_3__c  = 'Yes', Author__c = testUser.Id);
     	insert assessment;
     	
		System.runAs(testUser)  {
			List <RQI_Audit__c> assessments = [Select Id from RQI_Audit__c where Id=:assessment.Id];
			System.assert(assessments.size() > 0);
		}   
		Test.stopTest();		
    }
}