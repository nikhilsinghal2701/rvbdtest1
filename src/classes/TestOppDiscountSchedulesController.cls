/**
* Test class to verify functionality of the controller class
* which is used to display values of opportunity discount scheudle
* in a single opportunity
*
* @auther Remy.Chen
* @date 10/18/2013
*/
@isTest
public with sharing class TestOppDiscountSchedulesController {
    static testMethod void testFunctionalityOfController(){
        List<Opportunity> oppList = TestingUtilPP.createOpportunities(1, true, 
        new Map<String, Object>{'CloseDate'=>Date.today(), 'StageName'=>'0 - Pre-Qualification'});
        List<Opportunty_Discount_Schedule__c> oppDisSchList = TestingUtilPP.createOppDiscountSchedule(1,true, 
        new Map<String, Object>{'Authorization_Specialization__c'=>'Test', 'Opportunity__c'=>oppList[0].Id});
        List<Category_Master__c> cmList = TestingUtilPP.createCategoryMasters(1, true, 
        new Map<String, Object>());
        List<Discount_Detail__c> ddList = TestingUtilPP.createDiscountDetails(cmList, true, 
        new Map<String,Object>{'Opportunity__c'=>oppList[0].Id});
        
        ApexPages.StandardController std =  new ApexPages.StandardController(oppList[0]);
        OppDiscountSchedulesController oppController = new OppDiscountSchedulesController(std);
        
        //System.assertEquals(2, oppController.oppDSList.size());
        //System.assertEquals('Test', oppController.oppDSList[0].scheduleFor );
    }
}