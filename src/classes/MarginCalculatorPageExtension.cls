/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the extension class to MarginCalculatorPage
Created Date : Nov 2011
*******************************************************************************************************/


public with sharing class MarginCalculatorPageExtension {
    public List<Quote_Line_Item__c> quoteLines {get;set;}
    public List<QuoteLineWrapper> scenarioLines {get;set;}
    public Quote__c quote {get; private set;}
    private List<Product2> products {get; set;}
    public List<ProductWrapper> productsList {get; set;}
    public Product2 tempProduct {get; set;} 
    public List<SelectOption> options;
    public String family {get;set;}
    public String keyword {get;set;}
    public String subFamily {get;set;}
    public Boolean showNonCogs {get; private set;} 
    public Boolean error {get; private set;}
    public String errorMsg {get; private set;}
    public String severe {get; private set;}
    public Boolean showcolumns{get;set;}
    //for email -- start
    private RVBD_Email_Properties__c eProp;
    public String subj {get; set;}
    public String body {get; set;}
    public String toAddress {get; set;}
    public String ccAddress {get; set;}
    private List<string> toAddresses = new List<string>();
    private List<string> ccAddresses = new List<string>();
    private List<Quote_Line_Item__c> secList = new List<Quote_Line_Item__c>();
    List<Margin_Summary__c> marginSummaryList ;
    public List<MarginSummary> marginSummaryInnerClassList {get;set;}
    Map <String,Set<String>> groupMap = new Map <String,Set<String>> ();
    Map<String,MarginSummary> marginSummaryInnerMap = new  Map<String,MarginSummary>();
  
  
    //MarginCalculatorEmailHlp mEmailHlp;
    //for email -- end
    String prodQuery = ''; 
  
/*
    Constructor
*/  
    public MarginCalculatorPageExtension(ApexPages.StandardController controller){     
        System.debug('MarginCalculatorController - Constructor called');
        eProp=RVBD_Email_Properties__c.getValues('rvbd_email_settings');
        error = false;
        errorMsg = '';
        severe = '';
        marginSummaryInnerClassList = new List<MarginSummary>();
        Id quoteId = controller.getId();
        marginSummaryList = [Select m.Total_Value__c,Total_Weighted_Average__c,m.Extended_List_Price__c, m.Quote__c,m.Margin__c, m.Id, m.Group_Name__c, m.Cost__c,m.Extended_Discount__c 
        					From Margin_Summary__c m where m.Quote__C = :quoteId ];//order by m.Group_Name__c desc
        
         List<Category_Master__c> groupedResults= [Select c.Name,c.Id,c.group__c From Category_Master__c c  ];
          
           for(Category_Master__c c : groupedResults)
           	{
           		if(!groupMap.containskey(c.group__c))
           		GroupMap.put(c.group__c,new Set<String>());
           		Set<String> strList=GroupMap.get(c.group__c);
           		strList.add(c.Name);
           	    GroupMap.put(c.group__c,strList);
           		
           	}
           	
           	for(Margin_Summary__c m :marginSummaryList)
           	{
           		MarginSummary summary = new MarginSummary(m);
           		summary.Name=m.Group_Name__c;
           		marginSummaryInnerMap.put(m.Group_Name__c,summary);
           	}
        
        quote = [select id, name, Total_Weighted_Average__c, Total_Weighted_Average_for_Products__c, Total_Weighted_Average_for_Support__c,
                D_Discount_Demos__c, Margin_Product__c, Margin_Support__c, Margin_Training__c, Margin_Total__c,Opportunity__r.Primary_ISR__c, RSM__c,
                Scenario_Margin_P_Product__c, Scenario_Margin_P_Support__c, Scenario_Margin_P_Total__c, Scenario_Margin_P_Training__c,
                Opportunity__r.Pricebook2Id, Opportunity__r.stagename,Opportunity__c, Product_Quote_Total__c, Support_Quote_Total__c,
                Training_Quote_Total__c, Quote_Total__c
                from Quote__c where id = :quoteId];
            
           quoteLines = [select id, product_code__c, Qty_Ordered__c, D_Standard_Discount__c, Unit_List_Price__c, D_Unit_Price__c, D_Ext_Price__c,Unit_Cost__c,
                        Margin__c, Margin_Percent__c,Scenario_Discount__c, Scenarion_Margin_Percent__c, Scenario_Quantity__c,Scenario_Extended_Price__c,
                        Disti_Approved_Discount__c, Category__c
                        from Quote_Line_Item__c where quote__c = :quoteId 
                        and (Unit_List_Price__c != null and Unit_List_Price__c != 0)];//and (Unit_Cost__c != 0 and Unit_Cost__c  != null)
        scenarioLines = new List<QuoteLineWrapper>();
        for(Quote_Line_Item__c ql : quoteLines){
            Boolean costFlag = false;
            if(ql.Unit_Cost__c == null || ql.Unit_Cost__c == 0){
                costFlag = true;
            }
            QuoteLineWrapper qw = new QuoteLineWrapper(ql, costFlag);
            scenarioLines.add(qw);      
        }
        showNonCogs = true; 
        try{
            if(quote.Opportunity__r.stagename.equalsIgnoreCase('6 - Order Accepted') || quote.Opportunity__r.stagename.equalsIgnoreCase('7 - Closed (Not Won)'))    {
                error = true;
                severe = 'error';
                errorMsg = 'The opportunity is already closed.';
            }
        }catch(Exception e){
            error = true;
            severe = 'error';
            errorMsg = 'You are not authorized to perform the action on this quote. The quote\'s opportunity is not owned by you.';
        }
        
        calculate();
         for(String str:marginSummaryInnerMap.keyset())
            {
            	MarginSummary mar = marginSummaryInnerMap.get(Str);
            	mar.setNull();
            }
            if(!marginSummaryInnerClassList.isempty())
            {
            MarginSummary mar = marginSummaryInnerClassList[marginSummaryInnerClassList.size()-1];
            	if(mar!=null)
            	mar.setNull();
            }
        showcolumns=false;
    }
    
        //added by Ankita to sort the elements 
        public void orderElements(){
			  List<MarginSummary> tempList = new List<MarginSummary>();
			  tempList.add(marginSummaryInnerMap.get('Appliance'));
			  tempList.add(marginSummaryInnerMap.get('Support'));
			  tempList.add(marginSummaryInnerMap.get('Software'));
			  tempList.add(marginSummaryInnerMap.get('Services'));
			  marginSummaryInnerClassList.clear();
			  marginSummaryInnerClassList = tempList;
        }


    //opens a new page for product search and selection to be added to the scenario
    public PageReference addProducts(){
        if(products != null && !products.isEmpty()){
            products.clear();
        }
        if(productsList != null){
            productsList.clear();
        }else{
            productsList = new List<ProductWrapper>();
        }
        //tempProduct = new Product2();
        return Page.AddProductsToScenario;
    }
    
    public List<SelectOption> getOptions(){
        options = new List<SelectOption>();
        Schema.DescribeFieldResult F = Product2.Family.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();       
        options.add(new SelectOption('','None'));
        for(Schema.PickListEntry ple : P){
            options.add(new SelectOption(ple.getValue(), ple.getLabel()));
        }       
        return options;
    }
    //search products based on the criteria
    public PageReference searchProducts(){
        error = false;
        System.debug('Search Products ' + keyword);
        
/*      if(keyword == null || keyword == ''){
            error = true;
            severe = 'error';
            errorMsg = 'Please enter a keyword to search';
            return null;
        }*/
        System.debug('Family = ' + family);
        if(productsList != null){
            productsList.clear();
        }else{
            productsList = new List<ProductWrapper>();
        }
        if(prodQuery != ''){
            prodQuery = '';
        }
        prodQuery += 'select id, name, family, productcode, description, category__c, cost__c from Product2 ';
        System.debug('Query1 = ' + prodQuery);
        prodQuery += ' where (name like \'%' + keyword + '%\' or description like \'%' + keyword + '%\')';
        System.debug('Query2 = ' + prodQuery);
        if(subFamily != null && subFamily != ''){
            prodQuery += ' and Product_SubFamily__c = \'' + subFamily + '\'';
        }
        System.debug('Query3 = ' + prodQuery);
        if(family != null && family != ''){
            prodQuery += ' and family = \'' + family + '\'';
        }
        prodQuery += ' limit 1000';
        /*if(tempProduct.family != null ){
            prodQuery += ' and family = \'' + tempProduct.family + '\'';
        }*/
        System.debug('Query4 = ' + prodQuery);
        
    //  prodQuery =  and (name like \'%' + keyword + '%\' or description like \'%' + keyword + '%\'';
        products = Database.query(prodQuery);
        if(!products.isempty()){
            for(Product2 prod : products){
                ProductWrapper pw = new ProductWrapper(prod);
                productsList.add(pw);
            }
        }else{
            error = true;
            severe = 'info';
            errorMsg = 'Your search did not return any results. Please select a different criteria and try again.';
        }
        return null;
    }
    
    //add selected products to the scenario
    public PageReference addSelectedProducts(){
        Set<Id> productIds = new Set<Id>();
        for(ProductWrapper pw : productsList){
            if(pw.checked){
                productIds.add(pw.prod.id);
            }
        }
        List<PricebookEntry> priceBookEntries = [Select id,Productcode, Product2Id, Pricebook2Id,unitprice From PricebookEntry
                                             where pricebook2id  in (select pricebook2id from opportunity where id = :quote.Opportunity__c) 
                                             and product2id in :productIds and isactive = true];   
        Map<Id,PricebookEntry> productIdToPricebookEntryMap = new Map<Id,PricebookEntry>();
        for(PricebookEntry p : pricebookEntries) {
            productIdToPricebookEntryMap.put(p.Product2Id, p);
        }
        for(ProductWrapper pw : productsList){
            if(pw.checked){
                Decimal price = 0;
                Boolean costFlag = false;
                if(productIdToPricebookEntryMap.containsKey(pw.prod.Id)){
                    price = productIdToPricebookEntryMap.get(pw.prod.id).UnitPrice;
                    System.debug('Price = ' + price);
                    if(pw.prod.Cost__c == null || pw.prod.cost__c == 0){
                        costFlag = true;
                    }                                       
                }
                scenarioLines.add(new QuoteLineWrapper(new Quote_Line_Item__c(product_code__c = pw.prod.productcode,unit_List_price__c = price,
                                                        Unit_Cost__c = pw.prod.Cost__c, Category__c = pw.prod.category__c),costFlag));
            }
        }
        
        return Page.MarginCalculatorPage;
    }
    
    //remove products from the scenario
    public PageReference removeProducts(){
        List<QuoteLineWrapper> removeLines = new List<QuoteLineWrapper>();
        removeLines.addAll(scenarioLines);
        scenarioLines.clear();
        for(QuoteLineWrapper qw : removeLines){
            if(!qw.checked){
                scenarioLines.add(qw);
            }
        }
        return null;
    }

    //sends email to the requestor or any other recepients specified by the user
    public pageReference email(){
        set<Id> userIds=new set<Id>();      
        if(quote!=null){
            if(quote.Opportunity__r.Primary_ISR__c!=null) userIds.add(quote.Opportunity__r.Primary_ISR__c);
            if(quote.RSM__c!=null)userIds.add(quote.RSM__c);
            userIds.add(UserInfo.getUserId());
            if(userIds.size()>0){
                List<User> userList=[Select Id,email from User where Id IN:userIds];
                if(userList.size()>0){
                    for(User u:userList){
                        if(u.Id!=UserInfo.getUserId()){
                            toAddresses.add(u.email);
                        }else{
                            ccAddresses.add(u.email);
                            ccAddress = u.email;
                        }
                    }
                }
            }
            for(String temp : toAddresses){
                if(toAddress==null||toAddress==''){
                    toAddress=temp;
                }else{
                    toAddress = toAddress+','+temp;
                }  
            }       
            subj = 'Margin Analysis for quote: '+quote.Name;
        }
        return Page.scenarioToEmail;
    }
    
    public PageReference sendEmail() { 
        body =body+getBody(scenarioLines);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if(toAddress!=null && toAddress.length()>0){
            toAddresses.clear();
            toAddresses=toAddress.split(',');
            mail.setToAddresses(toAddresses);
        }
        if(ccAddress!=null && ccAddress.length()>0){
            ccAddresses=ccAddress.split(',');
            mail.setCcAddresses(ccAddresses);
        }
        //mail.setSenderDisplayName('Riverbed Business Application');
        //mail.setOrgWideEmailAddressId('0D2Q00000004Chj');
        mail.setOrgWideEmailAddressId(eProp.mc_org_wide_email_Id__c);
        mail.setSubject(subj);
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);      
        mail.setHtmlBody(body);
        if(mail!=null){
            try{
                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
                error = true;
                severe = 'Info';
                errorMsg = 'An Email has been sent to specified users';
            }catch(Exception e){
                error = true;
                severe = 'error';
                errorMsg = e.getMessage();
            }
        }
        toAddresses.clear();
        ccAddresses.clear();
        toAddress='';
        ccAddress='';
        body='';
        return Page.MarginCalculatorPage;         
    }
    public PageReference mCancel(){
        toAddresses.clear();
        ccAddresses.clear();
        toAddress='';
        ccAddress='';
        body='';
        return Page.MarginCalculatorPage;
    }
    private String getBody(List<QuoteLineWrapper> scenarioLines){
        //String body = '<body  style=" background-color:#FFFFFF; bLabel:body; bEditID:b1st1;"><center ><table id="topTable" height="450" width="500" cellpadding="0" cellspacing="0" ><tr valign="top" ><td  style=" vertical-align:middle; height:50; text-align:left; background-color:#FFFFFF; bLabel:header; bEditID:r1st1;"><img id="r1sp1" bLabel="headerImage" border="0" bEditID="r1sp1" src="/servlet/servlet.ImageServer?id=01570000000u6Qa&oid=00DT0000000Iopb"></img></td></tr><tr valign="top" ><td  style=" height:5; background-color:#697ca1; bLabel:accent1; bEditID:r2st1;"></td></tr><tr valign="top" ><td styleInsert="1" height="300"  style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;"><table height="400" width="600" cellpadding="5" border="0" cellspacing="5" ><tr height="50" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r1" aEditID="c1r1" locked="0" ></td></tr><tr height="300" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r2" aEditID="c1r2" locked="0" >';        
        String body = '<body  style=" background-color:#FFFFFF; bLabel:body; bEditID:b1st1;"><center ><table id="topTable" height="450" width="500" cellpadding="0" cellspacing="0" ><tr valign="top" ><td  style=" vertical-align:middle; height:50; text-align:left; background-color:#FFFFFF; bLabel:header; bEditID:r1st1;"><img id="r1sp1" bLabel="headerImage" border="0" bEditID="r1sp1"';
        body += 'src='+eProp.Server_URL__c+'/servlet/servlet.ImageServer?id=01570000000u6Qa&oid='+userInfo.getOrganizationId()+'></img></td></tr><tr valign="top" ><td  style=" height:5; background-color:#697ca1; bLabel:accent1; bEditID:r2st1;"></td></tr><tr valign="top" ><td styleInsert="1" height="300"  style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;"><table height="400" width="600" cellpadding="5" border="0" cellspacing="5" ><tr height="50" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r1" aEditID="c1r1" locked="0" ></td></tr><tr height="300" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r2" aEditID="c1r2" locked="0" >';
        body += '<b><u>Margin Analysis</u> - Scenario Details</b><br>';
        body += '<b>Scenario Information</b><br>';
        body += createTable(scenarioLines);  
        body += '</td></tr><tr height="50" valign="top" ><td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r3" aEditID="c1r3" locked="0" ></td></tr></table></td></tr><tr valign="top" ><td  style=" height:5; background-color:#697ca1; bLabel:accent2; bEditID:r4st1;"></td></tr><tr valign="top" ><td  style=" vertical-align:top; height:10; text-align:left; background-color:#FFFFFF; bLabel:footer; bEditID:r5st1;"></td></tr><tr valign="top" ><td  style=" height:5; background-color:#ffffff; bLabel:accent3; bEditID:r6st1;"></td></tr></table></center>';     
        return body;
    }
    
    private String createTable(List<QuoteLineWrapper> scenarioLines){
        String table = '<table cellspacing="2" cellpadding="3" border="1" Width="800">' +
                          '<tr>' +
                            '<th>' + 
                              '<b>Product Code</b>' + 
                            '</th>' +
                            '<th>' + 
                              '<b>Quote Quantity</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Scenario Quantity</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Quoted Discount (%)</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Scenario Discount (%)</b>' + 
                            '</th>' +                            
                            '<th>' + 
                              '<b>Extended Price (USD)</b>' + 
                            '</th>' + 
                            '<th>' + 
                              '<b>Scenario Extended Price (USD)</b>' + 
                            '</th>' +
                          '</tr>' ;
        for(QuoteLineWrapper qw : scenarioLines){
            table=table+'<tr>' +
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Product_Code__c) + '</td>' + 
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Qty_Ordered__c) + '</td>' +
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Scenario_Quantity__c) + '</td>' +
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Disti_Approved_Discount__c)+'%' + '</td>' + 
                '<td style="text-align:center">' + blankIfNull(qw.qLine.Scenario_Discount__c)+'%' + '</td>' + 
                '<td style="text-align:center">' + '$'+blankIfNull(qw.qLine.D_Ext_Price__c) + '</td>' +       
               '<td style="text-align:center">' +'$'+ Decimal.valueOf(blankIfNull(qw.qLine.Scenario_Extended_Price__c)) + '</td>' +       
                '</tr>';                
        } 
        return table+'</table>';                
    }
    
    private String blankIfNull(Object value){
        if (value == null){
            return '0';
        } else {
            return String.valueOf(value); 
        }   
    }

    //this method clears out the page data and refreshes the page with the quote lines from the quote
    public PageReference reset(){
        scenarioLines.clear();
        for(Quote_Line_Item__c ql : quoteLines){
            Boolean costFlag = false;
            if(ql.Unit_Cost__c == null || ql.Unit_Cost__c == 0){
                costFlag = true;
            }
            QuoteLineWrapper qw = new QuoteLineWrapper(ql,costFlag);
            scenarioLines.add(qw);      
        }       
        return null;
    }

    public PageReference back(){
        family = '';
        subFamily = '';
        keyword = '';
        errorMsg = '';
        error = false;
        return Page.MarginCalculatorPage;
    }

    public PageReference showNonCOGSItems(){
        List<Quote_Line_Item__c> nonCogsQuoteLines = [select id, product_code__c, Qty_Ordered__c, D_Standard_Discount__c, Unit_List_Price__c, D_Unit_Price__c, D_Ext_Price__c,
                        Margin__c, Margin_Percent__c,Scenario_Discount__c, Scenarion_Margin_Percent__c, Scenario_Quantity__c,Scenario_Extended_Price__c, Unit_Cost__c,
                        Disti_Approved_Discount__c, Category__c
                        from Quote_Line_Item__c where quote__c = :quote.Id 
                        and (Unit_Cost__c = 0 or Unit_Cost__c = null) and (Unit_List_Price__c = null or Unit_List_Price__c = 0)];
        //scenarioLines = new List<QuoteLineWrapper>();
        for(Quote_Line_Item__c ql : nonCogsQuoteLines){
            Boolean costFlag = false;
            if(ql.Unit_Cost__c == null || ql.Unit_Cost__c == 0){
                costFlag = true;
            }
            QuoteLineWrapper qw = new QuoteLineWrapper(ql,costFlag);
            scenarioLines.add(qw);      
        }       
        showNonCogs = false;
        return null;
    }
    
    public PageReference hideNonCOGSItems(){
        List<QuoteLineWrapper> removeLines = new List<QuoteLineWrapper>();
        removeLines.addAll(scenarioLines);
        scenarioLines.clear();
        for(QuoteLineWrapper qw : removeLines){
            if(qw.qLine.Unit_List_Price__c != null && qw.qLine.Unit_List_Price__c != 0){//qw.qLine.Unit_Cost__c != null && qw.qLine.Unit_Cost__c != 0 && 
                scenarioLines.add(qw);
            }
        }
        showNonCogs = true;
        return null;
    }
    
    
    public PageReference calculate(){ // Code added by Santoshi on Nov-14-11 for the dynamic display of margin summary records on page.
      Decimal totalCosts=0;
   	  Decimal totalExtPrices =0;
    Decimal totalExtListPrices =0;
  	Decimal	scenarioTotalDiscounts =0;
   	Decimal	scenarioTotalMargins =0;
   	Decimal	TotalExtPriceChange =0;
   	Decimal	TotalRequestedExtPrice=0;
   	Decimal	TotalRequestedDiscount =0;
   	Decimal	TotalDiscountChange =0;
   	Decimal	RequestedTotalMargin=0;
   	Decimal	QuoteTotal=0;
   	Decimal	CostTotal=0;
   	Decimal TotalExistingListPrice=0;
   	Decimal TotalExistingDiscount=0;
   		showcolumns=true;
  		 for(String str:marginSummaryInnerMap.keyset())
            {
            	MarginSummary mar = marginSummaryInnerMap.get(Str);
            	mar.reset(); 
            }
        for(QuoteLineWrapper qw : scenarioLines){
            String cat = qw.qLine.Category__c == null ? '' : qw.qLine.Category__c;
            Decimal scQty = qw.qLine.Scenario_Quantity__c == null ? qw.qLine.Qty_Ordered__c : qw.qLine.Scenario_Quantity__c;
            Decimal scDisc = qw.qLine.Scenario_Discount__c == null ? (qw.qLine.Disti_Approved_Discount__c == null ? 0 : qw.qLine.Disti_Approved_Discount__c) : qw.qLine.Scenario_Discount__c;
            System.debug('Scenario Discount = ' + scDisc);
            Decimal unitCost = qw.qLine.Unit_Cost__c == null ? 0 : qw.qLine.Unit_Cost__c;
            Decimal unitPrice = qw.qLine.Unit_List_Price__c == null ? 0 : qw.qLine.Unit_List_Price__c;
            if(unitPrice == 0 || scDisc==100){
                qw.qLine.Scenario_Extended_Price__c = 0;
            }else{
                qw.qLine.Scenario_Extended_Price__c = scQty * unitPrice * (1-scDisc/100);
                qw.qLine.Scenario_Extended_Price__c = qw.qLine.Scenario_Extended_Price__c.setscale(2);
                qw.qLine.Scenarion_Margin_Percent__c = 100 * (1 - ((scQty * unitCost)/qw.qLine.Scenario_Extended_Price__c));
            }
            
            for(String str:marginSummaryInnerMap.keyset())
            {
            	Set<String> catString = GroupMap.get(str);
            	
            	
            	if(catString!=null && catString.contains(cat))
            	{
            		MarginSummary mar = marginSummaryInnerMap.get(Str);
            		if(mar!=null)
            		{
					mar.Cost += (scQty * unitCost);
   					mar.ExtPrice += qw.qLine.Scenario_Extended_Price__c;
   	 				mar.ExtListPrice +=  (scQty * unitPrice);    
            		}     		
            		
            		break;
            	}
            }
            
            totalCosts += (scQty * unitCost);
           	totalExtPrices += qw.qLine.Scenario_Extended_Price__c;
            totalExtListPrices += (scQty * unitPrice);
        }
          for(String str:marginSummaryInnerMap.keyset())
            {
            	MarginSummary mar = marginSummaryInnerMap.get(Str);
            	if(mar!=null)
            	{
            		if(mar.m.Total_Weighted_Average__c!=null)
            	mar.RequestedDiscount = mar.m.Total_Weighted_Average__c;
            	if(mar.m.Total_Value__c != null)
            	mar.RequestedExtPrice = mar.m.Total_Value__c;
            	if(mar.m.margin__c!=null)
            	mar.RequestedMargin = mar.m.margin__c;
            	if(mar.ExtPrice!=null)
            	mar.ScenarioExtPrice = mar.ExtPrice;
               	QuoteTotal +=mar.m.Total_Value__c;
            	CostTotal += mar.m.Cost__c;
            	if(mar.ExtPrice !=null && mar.ExtPrice !=0)
            	{
            		mar.WhatIfScenarioMargin=(1 - (mar.Cost.divide(mar.ExtPrice,4)))*100;
            		if(mar.m!=null && mar.m.margin__C!=0)
            		mar.DifferenceInMargin =mar.m.margin__c- mar.WhatIfScenarioMargin;
            	}
            	
            	if(mar.ExtListPrice !=null && mar.ExtListPrice !=0)
            	{
            		mar.ScenarioDiscount=100*(mar.ExtListPrice - mar.ExtPrice).divide(mar.ExtListPrice,2);
            		if(mar.RequestedDiscount==null)
            		mar.RequestedDiscount=0;
            		if(mar.ScenarioDiscount != null )
            		mar.DiscountChange =mar.RequestedDiscount-mar.ScenarioDiscount;
            		
            	}
            	
            	if(mar.RequestedExtPrice!=null)
            	mar.ExtPriceChange =mar.RequestedExtPrice-mar.ExtPrice;
            	
            	
            	if(mar.RequestedDiscount!=null)
            	TotalRequestedDiscount +=mar.RequestedDiscount;
            		if(mar.ExtPriceChange!=null)
            	TotalExtPriceChange +=mar.ExtPriceChange;
            		if(mar.RequestedExtPrice!=null)
            	TotalRequestedExtPrice+=mar.RequestedExtPrice;
            	
            	TotalExistingListPrice += (mar.m.Extended_List_Price__c==null?0:mar.m.Extended_List_Price__c);
            	TotalExistingDiscount += (mar.m.Extended_Discount__c==null?0:mar.m.Extended_Discount__c);
            	}
            }
            if(totalExtPrices!=0)
            scenarioTotalMargins =(1-(totalCosts.divide(totalExtPrices,2)))*100;
            if(totalExtListPrices!=0)
           scenarioTotalDiscounts=(1-(totalExtPrices.divide(totalExtListPrices,2)))*100;
           
           System.debug('Q---------'+QuoteTotal+'C------------'+totalCosts);
            if(QuoteTotal!=0)
       RequestedTotalMargin=((QuoteTotal-CostTotal)/(QuoteTotal))*100;
           
    
       
      if(marginSummaryList.size()>=1)
      {
	       MarginSummary mar1= new MarginSummary();
	       mar1.Name ='Total';
	     	if(TotalExistingListPrice!=0){
		       mar1.RequestedDiscount = ((TotalExistingListPrice-TotalExistingDiscount)/TotalExistingListPrice)*100;
		       mar1.RequestedDiscount = mar1.RequestedDiscount.setscale(2);
	     	}
	     	mar1.ScenarioDiscount=scenarioTotalDiscounts;
	       mar1.DiscountChange = mar1.RequestedDiscount-mar1.ScenarioDiscount;
	       mar1.ExtPriceChange = TotalExtPriceChange;
	       mar1.RequestedExtPrice = TotalRequestedExtPrice;
	       mar1.ScenarioExtPrice =totalExtPrices;
	       mar1.WhatIfScenarioMargin = scenarioTotalMargins;
	       mar1.RequestedMargin=RequestedTotalMargin;
	       mar1.DifferenceInMargin=mar1.RequestedMargin-scenarioTotalMargins;
	       mar1.DifferenceInMargin= mar1.DifferenceInMargin.setscale(2);
	       mar1.RequestedMargin= mar1.RequestedMargin.setscale(2);
	       marginSummaryInnerClassList=marginSummaryInnerMap.Values();
	       orderElements();
	       marginSummaryInnerClassList.add(mar1);
      }
      else
      {
    	 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'No Margin Summary Records to display.Please create Margin Summary Records and try again..');
                 ApexPages.addMessage(myMsg);
                 return null; 
      }
        return null;
    }
    
    //wrapper class for products selection
    public class ProductWrapper  {
 
        public Boolean checked{ get; set; }
        public Product2 prod{ get; set;}
     
        public ProductWrapper (){
            prod = new Product2();
            checked = false;
        }
     
        public ProductWrapper (Product2 p){
            prod = p;
            checked = false;
        }
   }
   
   //wrapper class for quote lines
    public class QuoteLineWrapper  {
 
        public Boolean checked{ get; set; }
        public Quote_Line_Item__c qLine{ get; set;}
        public Boolean noCost {get; set;}
     
        public QuoteLineWrapper (Quote_Line_Item__c q, Boolean flag){
            qLine = q;
            checked = false;
            noCost = flag;
        }
   }
   
   
   public class MarginSummary // inner class added by Santoshi on Nov-2011 for the dynamic display of margin summary records on page.
   {
   	 public Margin_Summary__C m {get;set;}
   	 public Decimal RequestedDiscount {get;set;}
     public Decimal ScenarioDiscount {get;set;}
     public Decimal DiscountChange {get;set;}
   	 public Decimal RequestedExtPrice {get;set;}
   	 public Decimal ScenarioExtPrice {get;set;}
   	 public Decimal ExtPriceChange {get;set;}
   	 public Decimal RequestedMargin {get;set;}
   	 public Decimal WhatIfScenarioMargin {get;set;}
   	 public Decimal DifferenceInMargin {get;set;} 
   	  public Decimal Cost {get;set;}
   	 public Decimal ExtPrice {get;set;}
   	 public Decimal ExtListPrice {get;set;} 
   	  public Decimal ScenarioMargin {get;set;} 
   	  public String Name {get;set;}
   	 
   	
     public  MarginSummary()
   	  {
   	  	reset();
   	  }
   	 public MarginSummary(Margin_Summary__C m1)
   	 {
   	 	m=m1;
   	 	reset();
   	 }
      	 
   	   
        public void reset(){
            RequestedDiscount = 0;
            ScenarioDiscount = 0;
            DiscountChange = 0;
            ScenarioExtPrice = 0;
            RequestedExtPrice = 0;
            ExtPriceChange = 0;
            RequestedMargin = 0;
            WhatIfScenarioMargin = 0;
            DifferenceInMargin = 0;
            Cost=0;
            ExtPrice=0;
            ExtListPrice=0;
            ScenarioMargin=0;
            
        }
        
         public void setNull(){
            
            ScenarioDiscount = null;
            DiscountChange = null;
            WhatIfScenarioMargin = null;
            DifferenceInMargin = null;
           ExtPriceChange=ScenarioExtPrice;
           ScenarioExtPrice=0;
            
        }
   }
   
}