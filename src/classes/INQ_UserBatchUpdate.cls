/*
This class will query all users with Support Profiles and update their Roles and WorkTeam from InQuira
*/

global class INQ_UserBatchUpdate implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	class UserInfoDetail {
		public List<String> teams = null;
		public List<String> roles = null;
		public String login = null;
		public UserInfoDetail() {}		
	}
	
	// store List from REST API
	class UserInfo {
		public List<UserInfoDetail> userinfo;
		public UserInfo() {
			userinfo = new List<UserInfoDetail>();
		}
	}
		
	// store REST API connection info
	global final String endPointURL;
	global final String endPointUsername;
	global final String endPointPassword;
	global final String endPointSecret;
	global final Integer endPointQueryLimit;
	
	global String query;
	global Boolean isTesting;
	
	global INQ_UserBatchUpdate() {
		INQ_REST_Summary__c settings = INQ_REST_Summary__c.getInstance('INQ_REST_Summary_Settings');
		endPointURL = settings.Endpoint_URL__c;
		endPointUsername = settings.Endpoint_Username__c;
		endPointPassword = settings.Endpoint_Password__c;
		endPointSecret = settings.Endpoint_Secret__c;
		endPointQueryLimit = 100;
		isTesting = false;
		
		// Select Support staff
		query = 'SELECT Id,IsActive,FirstName,LastName,ProfileId,Username,KCS_Publisher__c,KCS_Engineer__c,KCS_Coach__c,KCS_Work_Team__c,KCS_Knowledge_Administrator__c FROM User ' +
			'WHERE (ProfileId=\'00e50000000uVEg\' '+
			'OR ProfileId=\'00e70000000wza6\' '+
			'OR ProfileId=\'00e400000017cXy\' '+
			'OR ProfileId=\'00e80000001BvTe\' '+
			'OR ProfileId=\'00e50000000uVEy\') '+
			'AND IsActive = true';	
	}
	
	global INQ_UserBatchUpdate(Integer limiter, Boolean testflag) {
		this();
		query = query + ' LIMIT ' + limiter;
		isTesting = testflag;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scopeList) {
		//TODO: Handle Http Request
		Integer start = 0;
		while (start<scopeList.size()) {
			handleRequest(start, scopeList);
			start += endPointQueryLimit;
		}
		// update User List
		update scopeList;
	}
	
	global void finish(Database.BatchableContext BC) {
	
	}
	
	private void handleRequest(Integer startPos, List<sObject> scopeList) {
		Integer length = Math.min(scopeList.size()-startPos,endPointQueryLimit);
		String userIds = '';
		
		// Map Username to User Object
		Map<String,User> userIdMap = new Map<String,User>();
		for (Integer i=startPos,n=startPos+length; i<n; i++) {
			User curUser = (User)scopeList[i];
			String curUserId = curUser.username;
			userIds = (userids.equals('') ? '' : userIds + ',' ) + curUserId; 
			userIdMap.put(curUserId,curUser);			
		}
		system.debug('USER LIST: ' + userIds);
		// filter HTTP Request for Testing or Live Run
		HttpRequest req = isTesting ? null : buildRequest(userIds);
		Http http = isTesting ? null : new Http();
		HttpResponse resp = isTesting ? null : http.send(req);
		Integer statusCode = isTesting ? 200 : resp.getStatusCode();
		String fullStatus = isTesting ? 'OK 200' : resp.getStatus() + statusCode;
		String json = isTesting ? '{"docinfo":[],"userinfo":[{"teams":["'+scopeList[startPos].get('FirstName')+'_COACH_TEAM","JEFF_SUMI_COACH_TEAM"], "roles":["KCS_ENGINEER_PUBLISHER","KCS_COACH"],"login":"'+scopeList[startPos].get('username')+ '"}]}' : resp.getBody();
								   	
		// complete Request
		if (statusCode == 200) {
			UserInfo userData = new UserInfo();
			system.debug('RAW JSON ' + json);
				
			try {
				userData = (UserInfo)System.JSON.deserialize(json, UserInfo.class);
				system.debug('JSON DATA ' + userData);
				updateUserInfo(userIdMap, userData);			
			
			} catch (Exception e) {
				System.debug('ERROR in InQuira User Batch Update: ' + e.getMessage());
			}
			
		} else {
			System.debug('ERROR: Status was not 200(' + fullStatus + '), cannot update User info using' + endpointURL);
		}	
	}
	
	//build REST Request with userids
	private HttpRequest buildRequest(String userids) {
		HttpRequest req = new HttpRequest();
		req.setHeader('Request-Service-User', endPointUsername);
		req.setHeader('Request-Service-Pass', endPointSecret + ';' + endPointPassword);
		req.setMethod('GET');
		req.setTimeout(20000);
		req.setEndpoint(endPointURL + (endpointURL.indexOf('?')>0 ? '&userids=' : '?userids=') + EncodingUtil.urlEncode(userids,'UTF-8'));
		
		return req;
	}
		
	// update SFDC user workteam, KCS Role with Inquira Data 
	private void updateUserInfo(Map<String,User> userIdMap, UserInfo userData) {
		User curUser;
		String userId;
		
		for (UserInfoDetail u : userData.userinfo) {
			userId = u.login;
			curUser = userIdMap.get(userId);
			
			if(u.roles != null) {
				updateRoles(u.roles,curUser);
			}
			
			List<String> teams = u.teams;
			  if (teams != null) {				
				String userTeam = curUser.KCS_Work_Team__c;
				// validate existing team, if any
				system.debug('CURRENT TEAM: ' + userTeam);	
				if (userTeam != null || userTeam !='') {
					if (!isCurrentTeam(teams, userTeam)) {
						curUser.KCS_Work_Team__c = '';
						setTeam(curUser, teams);										
					}
				} else {
					setTeam(curUser,teams);	
				}											
			}
			system.debug('KCS COACH:' + curUser.KCS_Coach__c);
			system.debug('KCS ADMINISTRATOR:' + curUser.KCS_Knowledge_Administrator__c);
			system.debug('SET TEAM: ' + curUser.KCS_Work_Team__c);
		}		
	}
	
	private void setTeam(User u, List<String> teams) {
		if (u.KCS_Coach__c == true) {
			setCoachTeam(u,teams); 
		} else {
			setMemberTeam(u,teams);
		}		
	}
	
	private void setCoachTeam(User u, List<String> teams) {
		// parse team for a first or last name match
		String firstName = u.firstname.toUpperCase();
		String lastName = u.lastname.toUpperCase();
		
		String buildRegex = '(' + firstName + '_|' + lastName + '_)';
		Pattern matchTeam = Pattern.compile(buildRegex);
		for (String t : teams) {
			Matcher isMatch = matchTeam.matcher(t);
			system.debug('TEAMLIST: '  + t);
			if (isMatch.find()) {
				u.KCS_Work_Team__c = t;
				//system.debug('MATCH POPULATED: ' + u.KCS_Work_Team__c);
			}
		}		
	}

	// set member team, assuming members belong to 1 work team in InQuira
	private void setMemberTeam(User u, List<String> teams) {
		u.KCS_Work_Team__c = teams.get(0);
	}
	
	private Boolean isCurrentTeam(List<String> teams, String userTeam) {
		Boolean isCurTeam = false;		
		for (String t : teams) {
			if (t == userTeam) {
				isCurTeam = true;
			}
		} 
		return isCurTeam;
	}
	
	private void updateRoles(List<String> roles, User curUser) {
		for (String role : roles) {
			if (role == 'KNOWLEDGE_ADMINISTRATOR') {
				curUser.KCS_Knowledge_Administrator__c = true;
			}
			if (role == 'KCS_COACH') {
				curUser.KCS_Coach__c = true;
			}
			if (role == 'KCS_ENGINEER_PUBLISHER') {
				curUser.KCS_Publisher__c = true;
			}
			if (role == 'KCS_ENGINEER_ROLE') {
				curUser.KCS_Engineer__c = true;
			}
		}		
	}


	
}