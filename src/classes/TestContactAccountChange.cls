@IsTest(seeAllData=true)
private class TestContactAccountChange {

		static testMethod void testTrigger() {
				User u =[Select u.Profile.Name, u.ProfileId, u.isActive
												From User u
												where Profile.Name ='System Administrator' and isActive = true
												limit 1];

				system.runAs(u){

					Account a = new Account();
								a.Name = 'test account';
								a.Type ='Partner Account';
								insert a;

								Account a2 = new Account();
								a2.Name = 'test account2';
								a2.Type ='Partner Account';
								insert a2;

								Acct_Cert_Summary__c c1 = new Acct_Cert_Summary__c();
								c1.Account__c = a.id;
								c1.TAP__c = 'General';
								c1.Status_Tap__c='No';

								Acct_Cert_Summary__c c2 = new Acct_Cert_Summary__c();
								c2.Account__c = a2.id;
								c2.TAP__c = 'General';
								c2.Status_Tap__c='yes';

								list<Acct_Cert_Summary__c> csList = new Acct_Cert_Summary__c[]{c1, c2};
								insert csList;
																c1.Status_Tap__c='yes';
																update c1;
																c2.Status_Tap__c='No';
												update c2;

								Contact cn1 = new Contact();
								cn1.AccountId = a.id;
								cn1.FirstName = 'test';
								cn1.LastName = 'tester';
								insert cn1;

								Certificate__c ce1 = new Certificate__c();
								ce1.Name = 'RSA';
								ce1.Contact__c = cn1.id;
								ce1.Certification_Summary__c = c1.id;
								insert ce1;

								cn1 = [select accountid from contact where id = :cn1.id];
								cn1.AccountId = a2.id;
								try{
										update cn1;
								}
								catch(exception e){
										system.assert(e.getMessage().contains('Error: cannot reassign the contact'));
								}


								Certificate__c ce2 = [select Certification_Summary__c from Certificate__c where id = :ce1.id];
						//    system.assert(ce2.Certification_Summary__c == c2.id);
				}
		}
}