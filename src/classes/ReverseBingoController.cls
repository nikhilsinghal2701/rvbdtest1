public with sharing class ReverseBingoController {
	public string caseId{set;get;}
	public string caseNumber{set;get;}
	public string caseOwner{set;get;}
	public string caseOwnerName{set;get;}
	public string caseComment{set;get;}
	public Account account;
	public ReverseBingoController(ApexPages.StandardController controller){
		this.account = (Account)controller.getRecord();
	}
	public void init(){
		Case caseObj = [Select c.Id, c.CaseNumber, c.AccountId, c.Additional_Comment__c, c.OwnerId, c.LastModifiedDate from Case c where c.AccountId=:account.Id order by c.LastModifiedDate desc limit 1];
		if(caseObj != null){
			caseId=caseObj.Id;
			caseNumber=caseObj.CaseNumber;
			caseOwner=caseObj.OwnerId;
			if(caseOwner.startsWith('005')){
				caseOwnerName=[Select Name from user where Id=:caseObj.OwnerId].Name;	
			}else if(caseOwner.startsWith('00G')){
				caseOwnerName=[Select g.Id, g.Name from Group g where Id=:caseObj.OwnerId].Name;
			}			
			caseComment=caseObj.Additional_Comment__c;
		}
	}
}