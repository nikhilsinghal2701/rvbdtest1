public class CaseTriggerHelper {
    
    /* RUN ONCE */
    private static boolean run = true;
    public static boolean runOnce(){
        if(run) {
            run = false;
            return true;
        } else { return run; }
    }
    
    /* RUN ONCE CASE COMMENT */
    private static boolean runCmnt = true;
    public static boolean runOnceCmnt(){
        if(runCmnt) {
            runCmnt = false;
            return true;
        } else { return runCmnt; }
    }
    
    /* ACTIONS AFTER INSERT */
    public static void doAfterInsert(List<Case> caseList) {
        // add team members to case 
        createCaseTeam(caseList); 
    }
    
    /* ACTIONS BEFORE UPDATE */
    public static void doBeforeUpdate(List<Case> caseList, Map<Id, Case> oldMap) {
        //  COMMENTED 26.5.15 // UNCOMMENTED 28.7.15
        for(Case cObj :caseList) {
            // validation on escalation comment
            if(cObj.Staff_Engineer__c != null && cObj.Staff_Engineer__c.equals('Pending') && cObj.Escalation_Comment__c == null) {
                cObj.addError('Escalation comment is required when Staff engineer is selected');
            }
            // validation on Re-escalation
            Case cOldObj = oldMap.get(cObj.id);
            if(cObj.Escalation_Status__c != null && cObj.Escalation_Status__c.equals('Open')
                    && cOldObj.Escalation_Status__c != null && cOldObj.Escalation_Status__c.equals('Closed')) {
                    
                if(cObj.Re_escalation_Reason__c == null)
                    cObj.Re_escalation_Reason__c.addError('Value is required.');
                if(cObj.Current_Status__c == null)
                    cObj.Current_Status__c.addError('Value is required.');
                    
            } else if(cObj.isClosed && !cOldObj.isClosed
                        && cObj.Escalation_Status__c != null && cObj.Escalation_Status__c.equals('Open')) {
                // if case is reopeed
                cObj.Escalation_Status__c = 'Closed'; 
            }
        }
        //  COMMENTED 26.5.15 // UNCOMMENTED 28.7.15
    }
    
    /* ACTIONS AFTER UPDATE */
    public static void doAfterUpdate(List<Case> caseList, Map<Id, Case> oldMap) {
        if(runOnce()) {
            sendEmailOnEscalation(caseList, oldMap); // send email when case is escalated 
            assignInactiveOwnerCase(caseList, oldMap); // assign case to queue if owner is inactive
        }
    }
    
    // method to create collection of contact ids to add in team members
    public static void createCaseTeam(List<Case> caseList) {
        Map<String, Id> conCaseIdMap = new Map<String, Id>();
        Map<Id, Id> accCaseIdMap = new Map<Id, Id>();
        for(Case c :caseList) {
            if(c.cc_Contact_Email__c != null) {
                for(String e :c.cc_Contact_Email__c.split(';')) {
                    if(e.trim().length() >= 15)
                    conCaseIdMap.put(e.trim().substring(0, 15), c.id);
                }
            }
            accCaseIdMap.put(c.AccountId, c.id);
        }
        List<Contact> conList = [select id, AccountId from Contact where Support_Role__c includes ('Primary Support Contact') 
                                    and AccountId in :accCaseIdMap.keySet()];
    
        for(Contact con :conList) {
            if(accCaseIdMap != null && accCaseIdMap.containsKey(con.AccountId)) {
                conCaseIdMap.put(String.valueOf(con.id).substring(0, 15), accCaseIdMap.get(con.AccountId));
            }
        }
        System.debug('conCaseIdMap===' + conCaseIdMap);
        addTeamMembers(conCaseIdMap);
    }
    
    // method to create team members
    public static void addTeamMembers(Map<String, Id> conCaseIdMap) {
        List<CaseTeamRole> role = [Select c.Name From CaseTeamRole c where Name = 'CCed on Email' limit 1];
        List<CaseTeamMember> memberList = new List<CaseTeamMember>();
        if(role != null && role.size() > 0) {
            for(String conId :conCaseIdMap.keySet()) {
                memberList.add(new CaseTeamMember(ParentId = conCaseIdMap.get(conId), MemberId = conId,
                                            TeamRoleId = role[0].id));
            }
        }
        
        if(memberList != null && memberList.size() > 0) {
            insert memberList;
        }
    }
    
    // method to assign case to queue if case owner is inactive
    public static void assignInactiveOwnerCase(List<Case> caseList, Map<Id, Case> oldMap) {
        Set<Id> ownerIds = new Set<Id>();
        for(Case cObj :caseList) {
            Case cOldObj = oldMap.get(cObj.id);
            if(cObj.Status != null && cObj.Status.equals('Needs Attention')
                    && cObj.Status != cOldObj.Status) {
                    ownerIds.add(cObj.OwnerId);
            }
        }
        
        Map<Id, User> inactiveOwnerMap = new Map<Id, User>([select id from User where id in :ownerIds 
                                                and isActive = false]);
        
        List<QueueSobject> queueList = [Select q.Queue.Name, q.QueueId From QueueSobject q where q.Queue.Name = 'Support'];
        List<Case> caseListToUpdate = new List<Case>();
        
        for(Case cObj :caseList) {
            if(inactiveOwnerMap != null && inactiveOwnerMap.containsKey(cObj.OwnerId) && queueList != null && queueList.size() > 0) {
                caseListToUpdate.add(new Case(id = cObj.id, OwnerId = queueList[0].QueueId));
            }
        }
        
        if(caseListToUpdate != null && caseListToUpdate.size() > 0) {
            update caseListToUpdate;
        }
    }
    
    // send email when case is escalated
    public static void sendEmailOnEscalation(List<Case> caseList, Map<Id, Case> oldMap) {
        
        try {
            
            Map<String, Id> mapTemplateId = new Map<String, Id>();
            mapTemplateId = mapTmpltId();
            List<Messaging.SingleEmailMessage> msgList = new List<Messaging.SingleEmailMessage>();
            
            List<CaseComment> cmntList = new List<CaseComment>();
            Map<Id, Set<String>> staffEnggMap = new Map<Id, Set<String>>();
            Set<String> staffEnggSet = new Set<String>();
            Boolean enggChange, caseClose;
            
            for(Case cObj :caseList) {
                Case cOldObj = oldMap.get(cObj.id);
                // if status is changed from null to open
                if(cObj.Escalation_Status__c != null && cObj.Escalation_Status__c.equals('Open')
                        && cOldObj.Escalation_Status__c == null) {
                    
                    List<String> toAddr = new List<String>();
                    toAddr.add(System.Label.Case_Escalation_To_Address);
                    //toAddr.add(cObj.Owner_Email__c);
                    System.debug('toAddresses===' + toAddr);
                    msgList.add(sendEmail(cObj.id, cObj.OwnerId, mapTemplateId.get(System.Label.Case_Escalation_Template_Name), toAddr));
                    cmntList.add(privateCaseComment(cObj, false));
                
                // if status is changed from closed to open
                } else if(cObj.Escalation_Status__c != null && cObj.Escalation_Status__c.equals('Open')
                        && cOldObj.Escalation_Status__c != null && cOldObj.Escalation_Status__c.equals('Closed')) {
                    
                    List<String> toAddr = new List<String>();
                    toAddr.add(System.Label.Case_Escalation_To_Address);
                    //toAddr.add(cObj.Owner_Email__c);
                    System.debug('toAddresses===' + toAddr);
                    msgList.add(sendEmail(cObj.id, cObj.OwnerId, mapTemplateId.get(System.Label.Case_Re_Escalation_Template_Name), toAddr));
                    cmntList.add(privateCaseComment(cObj, true));
                
                // if staff engineer is changed/ updated
                } else if(cObj.Staff_Engineer__c != null && !cObj.Staff_Engineer__c.equals('Pending')
                        && cOldObj.Staff_Engineer__c != null && !cOldObj.Staff_Engineer__c.equals('Pending')
                        && cObj.Staff_Engineer__c != cOldObj.Staff_Engineer__c) {
                    
                    enggChange = true;
                    staffEnggSet.add(cObj.Staff_Engineer__c);
                    staffEnggSet.add(cOldObj.Staff_Engineer__c);
                    staffEnggMap.put(cObj.id, new Set<String> {cObj.Staff_Engineer__c, cOldObj.Staff_Engineer__c});
                
                // if case is closed after escalation
                } else if(cObj.isClosed && !cOldObj.isClosed
                        && cObj.Escalation_Status__c != null && cObj.Escalation_Status__c.equals('Open')) {
                    
                    caseClose = true;
                    staffEnggSet.add(cObj.Staff_Engineer__c);
                    staffEnggMap.put(cObj.id, new Set<String> {cObj.Staff_Engineer__c});
                }
            }
            
            if(cmntList != null && cmntList.size() > 0) {
                insert cmntList;
            }
            
            if(msgList != null && msgList.size() > 0) {
                sendEmail(msgList); 
            }
            
            if(staffEnggSet != null && staffEnggSet.size() > 0) {
                if(enggChange != null && enggChange)
                    sendEmailToEngineers(staffEnggSet, staffEnggMap, caseList, mapTemplateId.get(System.Label.Case_Staff_Engineer_Update_Template));
                if(caseClose != null && caseClose)
                    sendEmailToEngineers(staffEnggSet, staffEnggMap, caseList, mapTemplateId.get(System.Label.Case_Closed_Template_Name));
            }
            
        } catch(Exception e) {
            System.debug('Error Message ::::: ' + e.getMessage());
        }
    }
    
    
    // method to send email to engineers when staff engineer is changed or case is closed
    public static void sendEmailToEngineers(Set<String> enggNames, Map<Id, Set<String>> staffEnggMap, List<Case> caseList, Id tempelateId) {
        Map<String, String> mapEnggId = new Map<String, String>();
        List<User> userList = [select Email, Name from User where Name in :enggNames];
        List<Messaging.SingleEmailMessage> msgLst = new List<Messaging.SingleEmailMessage>();
        
        for(User u :userList) {
            mapEnggId.put(u.Name, u.Email);
        }
        
        List<String> toAddr = new List<String>();
        for(Case c :caseList) {
            if(staffEnggMap != null && staffEnggMap.containsKey(c.id)) {
                for(String enggN :staffEnggMap.get(c.id)) {
                    if(mapEnggId != null && mapEnggId.containsKey(enggN)) {
                        toAddr.add(mapEnggId.get(enggN));
                    }
                }
                toAddr.add(System.Label.Case_Escalation_To_Address);
                //toAddr.add(c.Owner_Email__c);
                msgLst.add(sendEmail(c.id, c.OwnerId, tempelateId, toAddr));
            }
        }
        
        if(msgLst != null && msgLst.size() > 0) {
            sendEmail(msgLst);  
        }
    }
    
    // return map for template name and id
    public static Map<String, Id> mapTmpltId() {
        
        Map<String, Id> mapTempId = new Map<String, Id>();
        List<EmailTemplate> templateList = [Select Name, Id From EmailTemplate 
                                        where Name = :System.Label.Case_Escalation_Template_Name
                                           or Name = :System.Label.Case_Re_Escalation_Template_Name
                                           or Name = :System.Label.Case_Staff_Engineer_Update_Template
                                           or Name = :System.Label.Case_Closed_Template_Name]; 
        for(EmailTemplate tmplt :templateList) {
            mapTempId.put(tmplt.Name, tmplt.id);
        }
        
        return mapTempId;
    }
    
    // method to send emails and create email record
    public static void sendEmail(List<Messaging.SingleEmailMessage> msgList) {
        //  COMMENTED 26.5.15 // UNCOMMENTED 28.7.15
        System.debug('msgList===' + msgList);
        Messaging.sendEmail(msgList); 
        
        List<EmailMessage> emailMsgList = new List<EmailMessage>();
        for (Messaging.SingleEmailMessage msg : msgList) {
        
            String toAddr = '';
            for(String str :msg.getToAddresses()) {
                toAddr += str + ', ';
            }
            if(toAddr != '') {
                toAddr = toAddr.substring(0, toAddr.length() - 2);
            }
            emailMsgList.add(new EmailMessage(
                ParentId = msg.getWhatId(),
                Subject = msg.getSubject(),
                ToAddress = toAddr,
                MessageDate = System.now(),
                TextBody = msg.getPlainTextBody(),
                FromName = UserInfo.getUserName(),
                FromAddress = UserInfo.getUserEmail()
            ));
        }
        
        if(emailMsgList != null && emailMsgList.size() > 0) {
            insert emailMsgList; 
        }
        //  COMMENTED 26.5.15 // UNCOMMENTED 28.7.15
    }
    
    // return email message for escalation/ re-escalation
    public static Messaging.SingleEmailMessage sendEmail(Id caseId, Id cOwnerId, Id templateId, List<String> toAddresses) {
        System.debug('toAddresses===' + toAddresses);
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        
        email.setTargetObjectId(cOwnerId);
        email.setToAddresses(toAddresses);
        email.setWhatId(caseId);
        email.setTemplateId(templateId);  
        email.setSaveAsActivity(false);
        System.debug('email===' + email);
        return  email;
    }  
    
    // return case comment record
    public static CaseComment privateCaseComment(Case caseRec, Boolean isReEsc) {
        CaseComment caseCmnt = new CaseComment();
        if(isReEsc) {
            String reEscBody = 'Re-escalation Reason: ' + caseRec.Re_escalation_Reason__c + '\n' +
                               'Re-escalation Current Status: ' + caseRec.Current_Status__c + '\n';
            caseCmnt.CommentBody = reEscBody + commentBody(caseRec);
            
        } else {
            caseCmnt.CommentBody = commentBody(caseRec);
        }
        caseCmnt.IsPublished = false;
        caseCmnt.ParentId = caseRec.id;
        return caseCmnt;
    }  
    
    // retunr comment body
    public static String commentBody(Case caseRec) {
        String cmntBody =   'Escalation Priority: ' + caseRec.Escalation_Priority__c + '\n' +
                          + 'Escalation Severity: ' + caseRec.Escalation_Severity__c + '\n' +
                          + 'Installation Status: ' + caseRec.Installation_Status__c + '\n' +
                          + 'Problem Definition:   ' + caseRec.Problem_Definition__c + '\n' +
                          + 'Issue First Screen:   ' + caseRec.Issue_First_Seen__c + '\n' +
                          + 'Recent Changes:       ' + caseRec.Recent_Changes__c + '\n' +
                          + 'Frequency:            ' + caseRec.Frequency__c + '\n' +
                          + 'Reproducibility:      ' + caseRec.Reproducibility__c + '\n' +
                          + 'Environment Details:  ' + caseRec.Environment_Details__c + '\n' +
                          + 'Problem Details:      ' + caseRec.Problem_Details__c + '\n' +
                          + 'Data Location:        ' + '\n' +
                          + 'Relevant Data:        ' + caseRec.Relevant_Data__c;
        return cmntBody;
    }
}