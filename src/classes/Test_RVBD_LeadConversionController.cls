@isTest(SeeAllData=true)
public with sharing class Test_RVBD_LeadConversionController {      
    
    static testmethod void testRVBD_LeadConversionController(){
    	User usr = [Select ProfileId, LastName, Id From User u where ProfileId ='00e50000000uVEZ' and isactive = true limit 1];
    	
        List<Lead> uplead = new  List<Lead>();
        RecordType accRecType = [select id from RecordType where name ='Customer Account' and sObjectType='Account'];
        Account acc = new Account( Name ='Riverbed',Industry= 'Software',OS_City__c='San Francisco',OS_State__c='California' ,D_B_Country__c='U.S',recordTypeid= accRecType.Id );
        insert acc;
        Account probAcc = new Account( Name ='Salesforce',Industry= 'Software',OS_City__c='San Francisco',OS_State__c='California' ,D_B_Country__c='U.S',recordTypeid= accRecType.Id );
        insert probAcc;
        
        Contact con = new Contact(firstname = 'Steve', lastname = 'Jobs',email='Test@Riverbed.com', accountId = acc.Id );
        insert con;
        
        string ndate = string.valueof(system.today());
        Opportunity opp = new Opportunity(AccountId= acc.Id,StageName='0 - Prospect',CloseDate=System.today(), name = 'RVBD', Leadsource = 'advertisement', Channel__c='Direct',type = 'New', Is_Service_Renewal__c = false ,Competitors__c ='citrix'  );
        insert opp;
        
        RecordType genLead = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
        
        Lead newLead1  = new Lead(firstname = 'Tony', lastname = 'Montana', company = 'Riverbed', Ownerid= usr.Id,
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead.id, Associated_Contact__c=con.Id );
        uplead.add(newLead1);
        
        insert uplead;
        
       
        Test.startTest();
        System.runAs(usr)
        { 
        	ApexPages.StandardController cont = new ApexPages.StandardController(new Lead());
        	
        	System.currentPageReference().getParameters().put('id',newLead1.id);
        	RVBD_LeadConversionController lcc = new RVBD_LeadConversionController(cont);
       
        	lcc.isProbable = true;      
        	lcc.hasProbable =false;     
        	lcc.isEmailSend  =true;    
        	lcc.hasContactId  =false;    
        	lcc.hasAccountId = true;    
        
                            
        
        	lcc.leadId=newLead1.Id;
        	lcc.leadObj = newLead1;
        	lcc.accountSearch();
        	lcc.contactSearch();
        	lcc.OpportunitySearch();
        
       		lcc.strAccName ='Test Account';     
	        lcc.strAccCity ='San Francisco';    
	        lcc.strAccState = 'California';     
	        lcc.strAccCountry ='U.S';
	        lcc.strAccrctype = 'Customer Account'; 
	        lcc.accountSearch();
	        lcc.clearAcc();
	        
	        lcc.strConfName ='Steve';       
	        lcc.strConlName  ='Jobs';   
	        lcc.strConmail  =  'Test@Riverbed.com'; 
	        lcc.contactSearch();
	        lcc.clearConSearch();
	        
	        Boolean hasrecord = lcc.hasNext;
	        hasrecord = lcc.hasPrevious; 
	        Integer rec = lcc.totalAccountRecords;
	        Integer no = lcc.accPageNo;
	        
	        lcc.first();
	        lcc.last();
	        lcc.previous();
	        lcc.next();
	        lcc.clearAcc();
	        lcc.getAccounts();
	        lcc.getContacts();
	        lcc.getOpportunitys();
	        lcc.selectedAccount();
	        lcc.selectedContact();
	        System.currentPageReference().getParameters().put('opptid',opp.id);
	        lcc.selectedOpportunity();
	        lcc.createAccount();
	        lcc.createContact();
	        lcc.stropptStage ='0 - Prospect';       
	        lcc.stropptfrct  ='Pipeline';   
	        lcc.stropptOwner ='James Berry';
	        lcc.OpportunitySearch();
	        lcc.clearOppSearch();
	        
	        lcc.cancel();
	        lcc.probableAccount(); 
	        lcc.compareList();
	        lcc.createCompare('Trade Up','true','false',false,'true','false',false );
	        
	        lcc.createAcc =false;
	        lcc.selAccountid = null;
	        lcc.createCon = false;
	        lcc.createOpp =true;
	        lcc.selcontactid = null;
	        lcc.convertLead();
	        }
        Test.stopTest();
        }
        
        static testmethod void testLeadConversionController(){
        	User usr = [Select ProfileId, LastName, Id From User u where ProfileId ='00e50000000uVEZ' and isactive = true limit 1];
	       	Test.StartTest();
	       	System.runAs(usr)
        	{ 
	       	RecordType accRecType = [select id from RecordType where name ='Customer Account' and sObjectType='Account'];
	       	RecordType genLead = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
	       	Account acc2 = new Account( Name ='Riverbed',Industry= 'Software',OS_City__c='San Francisco',OS_State__c='California' ,D_B_Country__c='U.S',recordTypeid= accRecType.Id );
            insert acc2;
            Contact con2 = new Contact(firstname = 'Steve', lastname = 'Jobs',email='Test@Riverbed.com', accountId = acc2.Id );
            insert con2;
	       	Lead newLead3 = new Lead(firstname = 'tony', lastname = 'greg', company = 'Riverbed', 
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead.id,Probable_Account__c=acc2.Id );
            insert newLead3;
            
            Opportunity opp = new Opportunity(AccountId= acc2.Id,StageName='0 - Prospect',CloseDate=System.today(), name = 'RVBD', Leadsource = 'advertisement', Channel__c='Direct',type = 'New', Is_Service_Renewal__c = false ,Competitors__c ='citrix'  );
        	insert opp;    
            
            	ApexPages.StandardController cont2 = new ApexPages.StandardController(newLead3);               
		        System.currentPageReference().getParameters().put('id',newLead3.id);
		        RVBD_LeadConversionController lc3 = new RVBD_LeadConversionController(cont2);
		        
		        lc3.createAcc = false;
		        lc3.createCon =false;
		        lc3.createOpp = false;
		        lc3.updateCreateContact();
		        lc3.selAccountid = acc2.Id;
		        lc3.selcontactid = con2.Id;
		        lc3.selOpportunityid = opp.Id;
		        lc3.newtask.Subject ='Test Task';
		        lc3.newtask.ActivityDate =system.today();
		        lc3.isEmailSend = true;
		        lc3.clearComparisionList();
		        lc3.convertLead();
        	}
	        Test.StopTest();
        }
		
		static testmethod void testLeadConversion()
		{
			User usr = [Select ProfileId, LastName, Id From User u where ProfileId ='00e50000000uVEZ' and isactive = true limit 1];
			Test.StartTest();
			System.runAs(usr)
        	{ 
			RecordType accRecType = [select id from RecordType where name ='Customer Account' and sObjectType='Account'];
			RecordType genLead = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
			Account acc3 = new Account( Name ='Riverbed',Industry= 'Software',OS_City__c='San Francisco',OS_State__c='California' ,D_B_Country__c='U.S',recordTypeid= accRecType.Id );
            insert acc3;
            Contact con3 = new Contact(firstname = 'Steve', lastname = 'Jobs',email='Test@Riverbed.com', accountId = acc3.Id );
            insert con3;
			
			Lead newLead2 = new Lead(firstname = 'Tony', lastname = 'Montana', company = 'Riverbed', 
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead.id,Probable_Account__c=acc3.Id );          
                                                      
            insert newLead2;
            	
            	ApexPages.StandardController cont = new ApexPages.StandardController(new Lead());
		        System.currentPageReference().getParameters().put('id',newLead2.id);
		        RVBD_LeadConversionController lc2 = new RVBD_LeadConversionController(cont);
		        
		        lc2.selAccountid = acc3.Id;
		        lc2.selcontactid = con3.Id;
		        
		        
		        lc2.createAcc = false;
		        lc2.createCon =false;
		        lc2.createOpp = true;
		        //lc2.stringCompany =;
		        
		        
		        //lcc.selOpportunityid = opp.Id;
		        lc2.newtask.Subject ='Test Task';
		        lc2.newtask.ActivityDate =system.today();
		        lc2.isEmailSend = true;
		        lc2.convertLead();
        	}
		   Test.stopTest();
        }  

}