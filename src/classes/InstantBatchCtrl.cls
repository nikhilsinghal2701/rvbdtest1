/*
	Class: InstantBatchCtrl
	Purpose: call to instiantiate cert roll up and custom roll down batches.
	Created Date: 8/8/2014
	Created By: Jaya [ Perficient ]
*/
public without sharing class InstantBatchCtrl {
	
	public Account accountRec;
    public InstantBatchCtrl(ApexPages.StandardController controller) {
		accountRec = (Account)controller.getRecord();
    }
	public String acctId;
	/*action method on load of the page. This calls the batch process only for the current account hirearchy*/
	public void init(){
		Set<Id> relatedAcctIds = new Set<Id>();
		Id batchprocessId;
		acctId = accountRec.Id;
		if(acctId == null || acctId == ''){
			acctId =  ApexPages.currentPage().getParameters().get('Id');
		}
		if(acctId != null && acctId != '' && validateId(acctId)){
			relatedAcctIds = getAcctIds(acctId);
		}
		// calling batch execution
		if(!relatedAcctIds.isEmpty()){
			CertRollupBatch certRBatch = new CertRollupBatch();
	     	if(!relatedAcctIds.isEmpty()){
	     		certRBatch.accountIds.addAll(relatedAcctIds);
	     	}
	     	batchprocessId = Database.executeBatch(certRBatch);
	     	String URLL = 'https://' + ApexPages.currentPage().getHeaders().get('Host'); 
			String jobURL = '<a href="'+URLL+'/apexpages/setup/listAsyncApexJobs.apexp">View Status</a>';
	     	Apexpages.addMessage(new Apexpages.Message(Apexpages.severity.confirm,
                					'Batch job enqueued with ID: '+batchprocessId+
                					jobURL));
		}
	}/*back button to go back to the account detail page*/
	public PageReference backToAcct(){
		PageReference pref = new ApexPages.StandardController(accountRec).view();
		return pref;
	} /*validate if the given sObject id is account id or not.*/
	public Boolean validateId(String acctId){
		Schema.sObjectType objType = Schema.getGlobalDescribe().get('Account'); 
		String acctPrefix = objType.getDescribe().getKeyPrefix();
		System.debug('objType:'+objType.getDescribe().getKeyPrefix());
		return (acctId.startsWith(acctPrefix));
	}	
	/* retrive parent and parent account Ids for a given account*/
	public Set<Id> getAcctIds(String acctId){
		Set<Id> acctIds = new Set<Id>();
		Account acct = [SELECT Id,ParentId, Parent.ParentId FROM Account WHERE Id=: acctId];
		acctIds.add(acct.Id);
		if(acct.ParentId != null){
			acctIds.add(acct.ParentId);
		}if(acct.Parent.ParentId != null){
			acctIds.add(acct.Parent.ParentId);
		}
		return acctIds;
	}
}