/**
* Test class to verify the logic of support provider lookup field
* functionality in opportunity page 
*
* @auther Remy.Chen
* @date 10/28/2013
*/
@isTest
public with sharing class TestLookupController {
    static testMethod void testLookup() {
        List<Account> accList = TestingUtilPP.createAccounts(3, true, 
        new Map<String,Object>{'Authorizations_Specializations__c'=>'RASP'});
        PageReference pageRef = Page.LookupObjects;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('qryText', 'Test');
        LookupController lc = new LookupController();
        System.assertEquals(3, lc.accList.size());
        
        lc.searchStr = 'Test Account1';
        lc.search();
        System.assertEquals(1, lc.accList.size());
    }
}