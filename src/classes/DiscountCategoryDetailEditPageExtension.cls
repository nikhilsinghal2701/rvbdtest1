/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the extension class to DiscountCategoryDetailEditPage

Created Date : Oct 2011
*******************************************************************************************************/

public with sharing class DiscountCategoryDetailEditPageExtension {

	Discount_Category_Detail__c pageObj;	
	 String DiscountTableId {get;set;}
	public List <Discount_Category_Detail__C> detailList {set;get;}
	public String ParentName {get;set;} 

	 public DiscountCategoryDetailEditPageExtension(Apexpages.StandardController cont)
	  {
	  	pageobj=(Discount_Category_Detail__C) cont.getRecord();
	  	if(System.currentPageReference().getParameters().get('id')!=null)
	  	{
	  	DiscountTableId = pageobj.Discount_Table__c;
	  	ParentName=pageobj.Discount_Table__r.Name;
	  	}
	  	else
	  	{
	  	DiscountTableId = System.currentPageReference().getParameters().get('retUrl').substring(1);
	  	 pageobj.Discount_Table__c=DiscountTableId;
	  	 ParentName = System.currentPageReference().getParameters().get('parentName');
	  	
	  	}
	  	
	  	
	  	
	  	detailList = [select Id,Name,Category__c,Category__r.description__C,Category__r.Name,Discount_Table__c,Standard_Discount__c,Registered_Discount__c from Discount_Category_Detail__C  where Discount_Table__c =: DiscountTableId order by Category__r.Name];
	   
	  }
	  
	  public pagereference Save()
	  {
	  	update detailList;
	  	return (new Pagereference('/'+DiscountTableId));
	  }
	  
	  public pagereference Cancel()
	  {
	  	
	  	return (new Pagereference('/'+DiscountTableId));
	  }
	  
	 
}