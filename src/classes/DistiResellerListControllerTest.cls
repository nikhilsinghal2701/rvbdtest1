/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class DistiResellerListControllerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        /*TestDataFactory tdf = new TestDataFactory();
        RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        tdf.createDistiErrorMsg();
        insert rvbdEmail;
        Account account = tdf.createAccount();
        insert account;
        Contact contact = tdf.createContact(account.Id, 'disti@acc.com', 'distilName');
        insert contact;
        Profile profile = tdf.retriveProfile(Constant.PRM_DISTI_ADMIN_PROFILE);
        User user = tdf.createUser('disti@acc.com', contact.Id, profile.Id);
        user.username += '.rvbd';// validation rule user name must end with rvbd
        System.runAs(new User(Id = UserInfo.getUserId()))
        {
        //	insert user;
    	}
        List<Account> distiAccountList = new List<Account>();
        for(Integer i=0; i<100; i++){
        	Account distiaccount = tdf.createAccount();
        	distiaccount.Name = distiaccount.Name+i;
        	distiAccountList.add(distiaccount);
        }
        insert distiAccountList;
        // create dis reseller
        List<Distributor_Reseller__c> distiResellerList = new List<Distributor_Reseller__c>();
        for(Integer i=0; i<100; i++){
        	Distributor_Reseller__c distiReseller = tdf.createDisti(account.Id,distiAccountList[i].Id);	
        	distiResellerList.add(distiReseller);
        }
        insert distiResellerList;
        List<Account> retrivedAcctList = new List<Account>();
        PageReference pref;
        Test.startTest();
        	System.runAs(user){
        		DistiResellerListController distiCtrl = new DistiResellerListController();
        		distiCtrl.exportFileType = 'application/vnd.ms-excel;.xls';
        		distiCtrl.acctId = account.Id;
        		distiCtrl.init();
        		retrivedAcctList = distiCtrl.getSortList();
        		
        		pref = distiCtrl.exportData();
        		distiCtrl.sortString = 'Name';
        		distiCtrl.doSort();
        	}
        Test.stopTest();
       // System.assert(!retrivedAcctList.isEmpty());
        System.debug('pref:'+pref);
        */
        
        DistiResellerListController temp = new DistiResellerListController();
        List<Account> retrivedAcctList = new List<Account>();
        PageReference pref;
        Account accountRec = [SELECT Id FROM Account WHERE Name ='Arrow Enterprise Computing Systems' Limit 1];
        System.debug('accountRec:'+accountRec);
        temp.acctId = accountRec.Id;
        temp.exportFileType = 'application/vnd.ms-excel;.xls';
        temp.init();
		retrivedAcctList = temp.getSortList();
		/*System.debug('query:'+distiCtrl.getQuery());*/
		pref = temp.exportData();
		temp.sortString = 'Name';
		temp.doSort();
       
    }
}