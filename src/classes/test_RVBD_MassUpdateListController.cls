@isTest(SeeAllData=true)
public  class test_RVBD_MassUpdateListController {
    

     static testMethod void massUpdateTest() {
        List<Lead> uplead = new List<Lead>();
        List<Lead> leadlst = new List<Lead>();
        RecordType recType = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
        User eloquaUser = [select id, name from User where name = 'Eloqua Administrator'];
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
       /*for(integer i=0; i<10; i++){
            Lead ld = new Lead( Lastname = 'TEST', recordTypeid = recType.id, Company = 'RVBD', Status='Open', Email='test@rvbd.com', Lead_Score__c = 'B', LeadSource='Advertisement');
            leadlst.add(ld);
        }
        try{
            insert leadlst;
        } catch(Exception e){
            System.debug('Error in inserting Lead: '+ e.getMessage());
        }*/
    
        leadlst = [Select id,Lastname, Company,Lead_Score__c,concierge_Rep__c from Lead Limit 20 ];
        //system.debug(leadlst);
        uplead=[select ID, Concierge_Rep__c from Lead where Id IN:leadlst];
        string returnurl = '/00Q?fcf='+ApexPages.currentPage().getParameters().get('pg');
        Test.setCurrentPageReference(new PageReference(returnurl)); 
        
        ApexPages.StandardSetController con = new ApexPages.StandardSetController(uplead);       
        con.setselected(uplead);
        RVBD_MassUpdateListController mul = new RVBD_MassUpdateListController(con);

        System.runAs(eloquaUser) 
        {
            Lead ldtype = new Lead(); 
            system.debug(':::::::ldtype'+ldtype);
            Test.startTest(); 
            mul.discoverSObjectType(leadlst[0]);
            mul.processList(uplead);
            mul.save(); 
            Test.StopTest();
         }
    }
}