public class RVBD_LeadsConvertedToAssoContact {
	
	public List<Lead> convertedLeads {get; set;}
	public Id AssociatedContact;
    private ApexPages.StandardController controller {get; set;}
	private Contact con;
		
	public RVBD_LeadsConvertedToAssoContact(ApexPages.StandardController controller) {
        this.controller = controller;
        this.con= (Contact)controller.getRecord();
		convertedleads= [Select Id ,Name,Company,Street,Lead_Score__c,LeadSource,Pass_Lead_to_a_Partner__c,SourceDetail__c,City, State,Country From Lead where Associated_Contact__c =:con.Id AND IsConverted = True];
	
	}


}