/*************
Class: CaseBeforeInsert
Description: This class consists of all operations that happen on a case record before
it is inserted

Initial Version: Add method to add sticky notes from contact to case
Author: Clear Task (Rucha) - 4/1/2013
**************/

public with sharing class CaseBeforeInsert {
    public static Map<String,Schema.RecordTypeInfo> recTypeNameToIdMap = Schema.Sobjecttype.Case.getRecordTypeInfosByName();    
    
    //Copy sticky notes from case's contact to case
    public static void addContactStickyNotesToCase(List<Case> caseList,Set<Id> contactIdSet){
        Map<Id,Contact> contactIdToConMap = new Map<Id,Contact>([Select Id, Contact_Sticky_Notes__c from Contact where Id in :contactIdSet]);
        
        for(Case c : caseList){
            if(c.ContactId != null)
                c.Contact_Sticky_Notes__c = contactIdToConMap.get(c.ContactId).Contact_Sticky_Notes__c;
        }
    }
    
    //Copy opnet group id from asset to case
    public static void updateOpnetDetailsOnCase(List<Case> caseList, Set<Id> assetIdSet){
        Map<Id,Asset> assetIdMap = new Map<Id,Asset>([Select Id,Opnet_Group_ID__c from Asset where id in :assetIdSet]);
        Set<Id> opnetGrpIdSet = new Set<Id>();
        
        for(Case c : caseList){
            if(c.RecordTypeId == recTypeNameToIdMap.get('OPNET').getRecordTypeId()){
                if(assetIdMap.get(c.AssetId)!=null){
                    c.Opnet_Group_ID__c = assetIdMap.get(c.AssetId).Opnet_Group_ID__c;
                    if (c.Opnet_Group_ID__c != null)    // By Sukhdeep Singh on 11/6/2014 (# 211287)
                    opnetGrpIdSet.add(c.Opnet_Group_ID__c);
                }                
            }
        }
        
        if(!opnetGrpIdSet.isEmpty()){
            updatePocIdOnCase(caseList,opnetGrpIdSet);
        }
    }
    
     //Copy opnet poc id from opnet contact group id to case
    public static void updatePocIdOnCase(List<Case> caseList, Set<Id> opnetGrpIdSet){
        
        Map<Id,Id> grpidToPocIdMap = new Map<Id,Id>();
        List<Opnet_Group_ID__c> ogidList = [Select Opnet_Account_Group__c,OPnet_POC_ID__c,Contact__c from Opnet_Group_ID__c where Opnet_Account_Group__c in :opnetGrpIdSet];
        Integer ogidListSize = ogidList.size();
        for(Case c : caseList){
            Integer i =0;
            
                while(i<ogidListSize && !(c.Opnet_Group_ID__c == ogidList.get(i).Opnet_Account_Group__c && c.ContactId == ogidList.get(i).Contact__c)){
                    i++;
                }
                
                if(i<ogidListSize)
                    c.Opnet_POC_ID__c = ogidList.get(i).OPnet_POC_ID__c;
            
        }   
    }
    
    @isTest(SeeAllData=true)
    static void testCaseBeforeInsert(){
        Map<String,Schema.RecordTypeInfo> recTypeNameToIdMapinTest = Schema.Sobjecttype.Case.getRecordTypeInfosByName();
         //Check contact's notes are carried over to cases on case insertion
        Account acc = new Account(Name='CBI Test Account');
        insert acc;
        Account acc1 = new Account(Name='CBI Test Account_Asset');
        insert acc1;
        Contact c = new Contact(LastName='CBI Test Con 1',Contact_Sticky_Notes__c ='Test Notes',AccountId=acc.Id);
        insert c; 
        
        //Create opnet grp id record
        Opnet_Account_Group__c oac = new Opnet_Account_Group__c(Name='CBI Test123');
        insert oac;
        
        Opnet_Group_ID__c ogid = new Opnet_Group_ID__c(OPnet_POC_ID__c='1234',Opnet_Account_Group__c=oac.Id,Contact__c=c.Id);
        insert ogid;       
        
         
        
        //Create asset
        Asset a =new Asset(Name='CBI', Opnet_Group_ID__c=oac.Id,Instance_Number__c='CBI Inst Test 1',ContactId=c.Id, SKU__c = 'MD',AccountId=acc1.Id);
        insert a;
        
        List<Case> caseList = new List<Case>();
        for(Integer i=0; i<5; i++){
            if(Math.mod(i,2) == 0){
                caseList.add(new Case(ContactId=c.Id,Status='New',AssetId=a.Id,RecordTypeId=recTypeNameToIdMapinTest.get('OPNET').getRecordTypeId()));
            }
            else{
                caseList.add(new Case(ContactId=c.Id,Status='New',AssetId=a.Id));
            }
        }
        insert caseList;
        
        caseList = [Select Id,Opnet_Group_ID__c,Contact_Sticky_Notes__c from Case where id=:caseList];
        for(Integer i=0; i<5; i++){
            System.assertEquals(c.Contact_Sticky_Notes__c,caseList.get(i).Contact_Sticky_Notes__c);
            
            if(Math.mod(i,2) == 0){
                System.assert(caseList.get(i).Opnet_Group_ID__c != null);
            }
            else{
                System.assert(caseList.get(i).Opnet_Group_ID__c == null);
            }
        }
        
        //Verify when contact is changed, cases have notes of new contact
        Contact c2 = [Select Id,Contact_Sticky_Notes__c from Contact where Name = 'CBI Test Con 1' limit 1];
        List<Case> caseToUpdateList = new List<Case>();
        for(Integer i=0; i<5; i++){
            caseToUpdateList.add(new Case(Id=caseList.get(i).Id,ContactId=c2.Id));
        }
        update caseToUpdateList;
        
        caseToUpdateList = [Select Id,Contact_Sticky_Notes__c from Case where id=:caseToUpdateList];
        for(Integer i=0; i<5; i++){
            System.assertEquals(c2.Contact_Sticky_Notes__c,caseToUpdateList.get(i).Contact_Sticky_Notes__c);
        }
    }
}