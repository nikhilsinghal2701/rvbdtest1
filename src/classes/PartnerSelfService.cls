public without sharing class PartnerSelfService {
    
/**
*   Provides functionality to limit the number of partners a delegated partner user
*   can provision. The custom field Partner User Count on Account specifies this limit.
*   Additionally, the Account field Remaining Partner Users is maintained to keep a count
*   of the number of partner users that can still be created / reactivated.
*   
*   Author megar@apprivo.com
*/
//Data structures to persist through context
private static Map <Id,Contact> m_contactMap = null;
private static Map <Id,String> m_profileMap = null;
private static User m_contextUser = null;
    
//DML Operations for asynchronous write
private static Map <Id, Double> m_accUpdates = new Map <Id, Double> (); 
private static Map <Id, String> m_contactUpdates = new Map <Id, String> (); 
private static Map <Id, Id> m_contactShares = new Map <Id, Id> ();  
private static Map <Id, Id> m_adminShares = new Map <Id, Id> ();    
private static Map <Id, Id> m_oldAdminShares = new Map <Id, Id> (); 

//The warning message displayed when the limit is exceeded  
private static final String WARNING = 'License Limit Exceeded: '
                                        + 'All of your user licenses are currently in use by active partner users. '
                                        + 'Contact your channel account manager to request more licenses, or deactivate an active user to free up a license.';
                                        

private static final String ROLE_DEACTIVATED = 'Deactivated';
private static final String ROLE_NO_ACCESS = 'No partner portal access';  
private static final String ROLE_EXEC = 'Executive';
private static final String ROLE_MGR = 'Manager';
private static final String ROLE_USER = 'Portal Login';
private static final String ADMIN = 'Admin';
private static final String ORDERS = 'Orders';
private static final String SALES_REP = 'Portal Login';

private static String getRole(String profileName, boolean isActive){
    
    if (isActive != true){
        return  ROLE_DEACTIVATED;
    }else if (profileName.contains(ADMIN)){
        return ADMIN;
    }else if (profileName.contains(ORDERS)){
        return  ORDERS;
    }else if (profileName.contains(SALES_REP)){
        return  ROLE_USER;
    }else {
        return  ROLE_USER;
    }
}
                                        
/**
*   Process updates or new trigger events. Add an error to any user records where the 
*   number of active partner users associated with the related partner account exceeds
*   a maximum count of permissible users specified for the account.
*   
*   @param triggerNew the trigger context new List 
*   @param triggerNewMap the trigger context new Map 
*   @return void
*/
public static void process(List <User> triggerNew, Map <Id, User> triggerNewMap, Map <Id, User> triggerOldMap){
    
    System.debug('triggerNew: ' + triggerNew);
    m_contactMap = getPartnerContacts(triggerNew);
    System.debug('m_contactMap: ' + m_contactMap);
    Map <Id,User> partnerUsers = getPartnerUsers(m_contactMap);
    System.debug('partnerUsers: ' + partnerUsers);
    Map <Id, Integer> usrCntMap = getAccountUserCnt(partnerUsers, 
                                                triggerNew, 
                                                triggerNewMap,
                                                m_contactMap);  
    
    System.debug('usrCntMap: ' + usrCntMap);
    m_profileMap = getProfileMap(triggerNew, triggerOldMap);
    System.debug('m_profileMap: ' + m_profileMap);
    m_contextUser = [select Contactid from user where Id = : UserInfo.getUserId()];
    
    for (User u : triggerNew){
        
        if (u.ContactId != null){
            
            Contact c = m_contactMap.get(u.ContactId);
            
            
            //Populate the User Role name on Contact
            if (triggerOldMap == null
                || triggerOldMap.get(u.Id).ProfileId != u.ProfileId
                || triggerOldMap.get(u.Id).isActive != u.isActive){
                
                String profileName = m_profileMap.get(u.ProfileId);
                String role = getRole(profileName, u.isActive);
                        
                if (c != null){
                    m_contactUpdates.put(c.Id, role);
                }               
            }
            //only for new users or re/de-activated users
            if (triggerOldMap == null
                || (triggerOldMap.get(u.Id).isActive != u.isActive)){

                if (c != null){
                    
                    Id accId = c.AccountId;
                    Integer userCount = usrCntMap.get(accId);
                    if (userCount == null){
                        userCount =  0; 
                    }
					
                   /* if (c.Account.Partner_User_Count__c != null
                        && userCount > c.Account.Partner_User_Count__c
                        && u.isActive == true
                        //the context user is a portal user
                        && m_contextUser.Contactid != null){    */    // added by Sukhdeep Singh, Ticket# 140917, 07/06/2013
                    if ( (c.Account.Partner_User_Count__c != null
                        && userCount > c.Account.Partner_User_Count__c
                        && u.isActive == true
                        //the context user is a portal user
                        && m_contextUser.Contactid != null) || (c.Account.Partner_User_Count__c == null || ((c.Account.Partner_User_Count__c - userCount)<0 && u.isActive)) ){
                        //Add an error as limit has been exceeded
                        u.addError(WARNING);    
                    } else if (c.Account.Partner_User_Count__c != null){
                        m_accUpdates.put(accId,  c.Account.Partner_User_Count__c - userCount);
                    } 
                }                   
            }
        }
    }       
}   
 
/**
*   Perform operations that must run in the after trigger
*
*   @param triggerNew the trigger context new List 
*   @param triggerOldMap the trigger context old map
*   @return void
*/
public static void processAfter(List <User> triggerNew, Map <Id, User> triggerOldMap){
    
    List <ContactShare> contactShares = new List <ContactShare> ();
    
    if (triggerNew.size() == 1
        && triggerOldMap == null
        && m_contextUser.Contactid != null){
            //sendNotification(triggerNew[0]);//commented by Ankita on June 9, 2011 for ticket# 73602
    }
    //Create shares for each user's contact record
    //if (triggerOldMap == null){
        for (User u : triggerNew){
            if (triggerOldMap == null
                || triggerOldMap.get(u.Id).isActive != u.isActive){
                if (u.ContactId != null
                    && u.isActive == true){
                    m_contactShares.put(u.ContactId, u.Id);
                }
            }
        }
    //}
    
    //Create admin shares for delegated admins
    for (User u : triggerNew){
        
        if (u.ContactId != null
            && (triggerOldMap == null
                || triggerOldMap.get(u.Id).ProfileId != u.ProfileId
                || triggerOldMap.get(u.Id).isActive != u.isActive)){
            
            Contact c = m_contactMap.get(u.ContactId);
            String profileName = m_profileMap.get(u.ProfileId);
            
            if (c != null){
                if (isAdmin(profileName)
                    && u.IsActive == true){
                    //Try changing profile on unactive
                    m_adminShares.put(u.Id, c.AccountId);
                }else if (triggerOldMap != null
                            && triggerOldMap.get(u.Id).ProfileId != u.ProfileId){
                        
                    Id oldProfileId = triggerOldMap.get(u.Id).ProfileId;
                    String oldProfileName = m_profileMap.get(oldProfileId);
                    
                    if (isAdmin(oldProfileName)){                   
                        m_oldAdminShares.put(u.Id, c.AccountId);
                    }
                }
            }               
        }
    }           
}
/**
*   Write any model changes made during the after trigger.
*/
public static void flush(){

    if (m_accUpdates.size() > 0 || m_contactUpdates.size() > 0 || m_contactShares.size() > 0 || m_adminShares.size() > 0 || m_oldAdminShares.size() > 0){
        System.debug('Flush Ctx: ' + m_accUpdates + m_contactUpdates + m_contactShares + m_adminShares + m_oldAdminShares);
        asyncFlush(m_accUpdates, m_contactUpdates, m_contactShares, m_adminShares, m_oldAdminShares);
    }

}
/**
*   Write any model changes made during the after trigger in asynchronous mode
*/
@future
private static void asyncFlush(Map <Id, Double> accUpdateMap, 
                                Map <Id, String> contactUpdateMap, 
                                Map <Id, Id> newContactShareMap,
                                Map <Id, Id> adminShareMap,
                                Map <Id, Id> oldAdminShareMap){
                                    
    System.debug('accUpdateMap :' + accUpdateMap);
    System.debug('contactUpdateMap :' + contactUpdateMap);
    System.debug('oldAdminShareMap :' + oldAdminShareMap);
    System.debug('newContactShareMap :' + newContactShareMap);
    System.debug('adminShareMap :' + adminShareMap);
    
    List <Account> accList = new List <Account> ();
    List <Contact> contactList = new List <Contact> ();
    List <ContactShare> contactShares =  new List <ContactShare>() ;
    List <AccountShare> adminShares =  new List <AccountShare>() ;  
    String partnerRole;
    
/*  try{
        Account[] accs = [select id from Account where id in : accUpdateMap.keyset() for update];
    }catch (Exception e){
        System.debug(e);
        try{
            Account[] accs = [select id from Account where id in : accUpdateMap.keyset() for update];
        }catch (Exception e2){
            System.debug(e2);
        }       
    }*/
    
    if (accUpdateMap.size() > 0){
        for (Id accId : accUpdateMap.keyset()){
            Double remaining = accUpdateMap.get(accId);
            accList.add(New Account (Id = accId,  Remaining_Partner_Users__c = remaining));
        }
        update accList;
    }
    
    /*try{
        Contact[] cnts = [select id from Contact where id in : contactUpdateMap.keyset() for update];
    }catch (Exception e){
        System.debug(e);
        try{
            Contact[] cnts = [select id from Contact where id in : contactUpdateMap.keyset() for update];
        }catch (Exception e2){
            System.debug(e2);
        }       
    }   */
    if (contactUpdateMap.size() > 0){
        for (Id contactId : contactUpdateMap.keyset()){
            String name = contactUpdateMap.get(contactId);
            /*Requirement:To update Partner role on Contact based on the profile assigned to related user
            **By Prashant on 06/18/2010 - Start
            */
            if(name.contains(ADMIN)){
                partnerRole='logon,orders,admin';
            }else if(name.contains(ORDERS)){
                partnerRole='logon,orders';
            }else if(name.contains(SALES_REP)){
                partnerRole='logon';
            }else if(name.contains(ROLE_DEACTIVATED)||name.contains(ROLE_NO_ACCESS)){
                partnerRole='';
            }//Prashant - 06182010 - End
            contactList.add(New Contact (Id = contactId,  User_Profile__c = name, PartnerRole__c = partnerRole));   
        }
        update contactList;
    }   
    
    if (adminShareMap.size() > 0){
        for (Id userId : adminShareMap.keyset()){
            Id accId = adminShareMap.get(userId);
        
            adminShares.add(New AccountShare (UserOrGroupId = userId, 
                                OpportunityAccessLevel = 'Edit', 
                                ContactAccessLevel = 'Edit', 
                                CaseAccessLevel= 'Edit', 
                                AccountId = accId, 
                                AccountAccessLevel = 'Edit'));  
        }
        insert adminShares;
    }
    if (oldAdminShareMap.size() > 0){
        
        List <AccountShare> oldAccShares = new List <AccountShare> ();
        List<AccountShare> shares;
        if(DiscountApproval.isTest){
            shares = [Select Id, UserOrGroupId 
                        from AccountShare
                        where UserOrGroupId IN : oldAdminShareMap.keyset()
                                and OpportunityAccessLevel = 'Edit'
                                and ContactAccessLevel = 'Edit'
                                and CaseAccessLevel= 'Edit'
                                and AccountId IN : oldAdminShareMap.values() 
                                and AccountAccessLevel = 'Edit'
                                and RowCause = 'Manual' limit 10];
        }else{
            shares = [Select Id, UserOrGroupId 
                        from AccountShare
                        where UserOrGroupId IN : oldAdminShareMap.keyset()
                                and OpportunityAccessLevel = 'Edit'
                                and ContactAccessLevel = 'Edit'
                                and CaseAccessLevel= 'Edit'
                                and AccountId IN : oldAdminShareMap.values() 
                                and AccountAccessLevel = 'Edit'
                                and RowCause = 'Manual'];
        }
        for (AccountShare a : shares){
                                    
            if (oldAdminShareMap.get(a.UserOrGroupId) != null){
                oldAccShares.add(a);    
            }   
        }
                                
        delete oldAccShares;                        
    }

    if (newContactShareMap.size() > 0){
        for (Id contactId : newContactShareMap.keyset()){
            Id userId = newContactShareMap.get(contactId);
            contactShares.add(New ContactShare (ContactId = contactId,  UserOrGroupId = userId, ContactAccessLevel = 'Edit'));  
        }
        insert contactShares;
    }   
                        
}

/**
*   Check to see if a given profile name is an administrative profile.
*/
private static boolean isAdmin(String profileName){

    if (profileName.contains('Admin')){
        return true;    
    }else{
        return false;   
    }

}

/**
*   Query the profiles for the given Ids and returns as a map of profile Ids to name
*   
*   @param profileIds a list profile Ids to query on
*   @return a map of profile Ids to name
*/
private static Map <Id,String> getProfileMap(List <User> users, Map <Id, User> triggerOldMap ){
    
    
    Map <Id,String> profileMap = new Map <Id,String> ();
    Set <Id> profileIds = new Set <Id> ();
    
    for (User u : users){
        if (u.ContactId != null){
            profileIds.add(u.ProfileId);
        }
    }   
    
    if (triggerOldMap != null){
        for (User u : triggerOldMap.values()){
            if (u.ContactId != null){
                profileIds.add(u.ProfileId);
            }
        }
    }
    
    for (Profile p : [Select p.Name, p.Id From Profile p
                        where p.Id IN : profileIds]){
                            profileMap.put(p.Id, p.Name);
                        }
    return profileMap;
}
/**
*   Query any account contacts relating to the passed users
*   
*   @param users the users to query on
*   @return a map of Contact Ids to Contacts containing the users' contacts
*/
private static Map <Id,Contact> getPartnerContacts(List <User> users){
    
    
    Map <Id,Contact> contactMap = null;
    List <Id> contactIds = new List <Id> ();
    
    for (User u : users){
        if (u.ContactId != null){
                contactIds.add(u.ContactId);
            }
    }
    
    contactMap = new Map <Id,Contact> ([Select c.Id, c.Account.Partner_User_Count__c, c.email, c.AccountId, c.Account.name 
                                        From Contact c 
                                        where c.Id IN : contactIds]);
    return contactMap;                                                  
}
/**
*   Query any partner users associated with the passed contacts
*   
*   @param contactMap a map of contacts to query on
*   @return  Map <Id,User> a map of User Ids to User containing all the found partner users
*/

private static Map <Id,User> getPartnerUsers(Map <Id,Contact> contactMap){
    
    
    
    Map <Id,User> userMap = null;
    Map <Id,User> resultUserMap = null;
    List <Id> accIds = new List <Id> ();
    
                                                        
    for (Contact c : contactMap.values()){
        if (c.accountId != null){
                accIds.add(c.accountID );
            }
    }
    if(DiscountApproval.isTest){
    userMap = new Map <Id,User> ([Select u.IsActive, u.Contact.AccountId, u.ContactId 
                                    From User u
                                    where u.Contact.AccountId IN : accIds limit 10]);
    }else{
    userMap = new Map <Id,User> ([Select u.IsActive, u.Contact.AccountId, u.ContactId 
                                    From User u
                                    where u.Contact.AccountId IN : accIds]);
    }                                           
    return userMap;
}

private static void incCnt(Id accId, Map <Id, Integer> userCountMap){
    
    if (userCountMap.containsKey(accId)){
        userCountMap.put(accId,userCountMap.get(accId) + 1);
    }else{
        userCountMap.put(accId, 1);
    } 
    
}
/**
*   Create a map of account ids to the sum of all active partner users 
*   
*   @param userMap the queried partner users
*   @param triggerNew the trigger context new List 
*   @param triggerNewMap the trigger context new Map 
*   @param contactMap the queried contacts for the trigger user context
*   @return a List of active partner users relevant to the context
*/

private static Map <Id, Integer> getAccountUserCnt(Map <Id,User> userMap, 
                                                List <User> triggerNew, 
                                                Map <Id, User> triggerNewMap,
                                                Map <Id, Contact> contactMap){
    

    List <User> resultList = new List <User> ();
    Map <Id, Integer> userCountMap = new Map <Id, Integer> ();                                      

    for (User u : userMap.values()){
        User newUser = null; 
        if (triggerNewMap != null){
            newUser = triggerNewMap.get(u.Id);
        }
        if ((u.isActive == true && newUser == null)
            || (newUser != null && newUser.isActive == true)){
                
            Id accountId = u.Contact.AccountId;

            incCnt(accountId, userCountMap);    
        }       
    }
    
    if (triggerNewMap == null){
        for (User u : triggerNew){
            if (u.IsActive == true
                && u.ContactId != null){
                
                Contact c = contactMap.get(u.ContactId);
                if (c!= null){
                    Id accountId = c.AccountId;
                    incCnt(accountId, userCountMap);
                }else{
                    System.debug('PartnerSelfService: missing contact record - ' + u.ContactId);
                } 
            }               
        }   
    }
    
                                                        
    return userCountMap;
}

/**
*   Code coverate test method > 85%
*   @return void
*/
static  testMethod void coverage(){

    Account a = new Account(name = 'account');
    insert a;
    
    Contact c = new Contact (accountid = a.Id, lastname = 'con', email='test@test.com');
    insert c;
    
    a.IsPartner = true;
    update a;

    Profile p = [select id from profile where name='Partner User'];

    User newPartner = new User(alias = 'standt', email='standarduser@testorg.com', 
    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
    contactId = c.Id, localesidkey='en_US', profileid = p.Id, 
    timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com');   

    system.test.starttest();
    insert newPartner;
    process( new List <user> {newPartner} , null, null);
    system.test.stoptest();
}

private static void sendNotification(User u){
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

// Strings to hold the email addresses to which you are sending the email.
String[] toAddresses = new String[] {'Partner-Contact-Request@riverbed.com'}; 
String[] ccAddresses = new String[] {'megar@apprivo.com','ppatil@riverbed.com', 'tthomas@apprivo.com'};
  

// Assign the addresses for the To and CC lists to the mail object.
mail.setToAddresses(toAddresses);
//mail.setCcAddresses(ccAddresses);

// Specify the address used when the recipients reply to the email. 
mail.setReplyTo('noreply@riverbed.com');

// Specify the name used as the display name.
mail.setSenderDisplayName('Riverbed Business Application');

// Specify the subject line for your email address.
mail.setSubject('New Partner User Created : ' 
                + ((u.FirstName == null)? '': u.FirstName) + ' ' + u.LastName);

// Set to True if you want to BCC yourself on the email.
mail.setBccSender(false);

// Optionally append the salesforce.com email signature to the email.
// The email address of the user executing the Apex Code will be used.
mail.setUseSignature(false);

// Specify the text content of the email.
mail.setPlainTextBody('New Partner User Created : ' + u.FirstName + ' ' + u.LastName);

mail.setHtmlBody ('New partner user is created. Please enable appropriate access level for the Order Center.<p>' 
                    + 'View user <a href=https://na5.salesforce.com/'+u.Id+'>click here</a>'
                    + '<br>'
                    + '<br>Riverbed Business Application'
                    + '<br><br>Please do not reply to this email.');


// Send the email you have created.
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
}
/**
*   Code coverate test method > 85%
*   @return void
*/
static  testMethod void testInsert(){

/*  Account a = new Account(name = 'account');
    insert a;

    a.IsPartner = true;
    update a;
    
    User pu = new User(alias = 'standt', email='standarduser@testorg.com', 
    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
    contactId = c.Id, localesidkey='en_US', profileid = p.Id, 
    timezonesidkey='America/Los_Angeles', username='standarduser@testorg.com'); 

    */
    Profile p = [select id from profile where name='Partner User']; 
    Contact c = [Select accountid, id from Contact where account.isPartner = true limit 1];
    


    User pu = [Select Id from user 
                        where ContactId != null
                        and isActive = true 
                        limit 1];
    
    User newPartner = [Select Id from user 
                        where Id != :UserInfo.getUserId()
                        and ContactId != null
                        and Id != : pu.Id
                        and isActive = true 
                        limit 1];   


    newpartner.isActive = false;
    update newPartner;
    
    system.test.starttest();

        
    System.runAs(pu) {

    try {
        System.debug('p.Id: ' + p.Id);
        System.debug('UserInfo.getProfileId(): ' + UserInfo.getProfileId());
        newpartner.isActive = true;
        update newPartner;
    //catch the custom error
    }catch(DMLException e){}
        
    }
    system.test.stoptest(); 
}

/**
*   Code coverate test method > 85%
*   @return void
*/
static  testMethod void coverage2(){

    Account a = new Account(name = 'account');
    insert a;
    
    Contact c = new Contact (accountid = a.Id, lastname = 'con', email='test@test.com');
    insert c;
    
    a.IsPartner = true;
    update a;

    Profile p = [select id from profile where name='Partner User'];

    User oldPartner = [Select Id, isactive, ContactId, profileid from user 
                        where ContactId != null
                        and isActive = false 
                        limit 1];
    
    User newPartner = oldPartner.clone();
    newPartner.isActive = true;

    system.test.starttest();
    //insert newPartner;
    process( new List <user> {newPartner} , new Map <Id,User> {newPartner.Id => newPartner}, new Map <Id,User> {oldPartner.Id => oldPartner});
    system.test.stoptest();
    
    sendNotification(newPartner);
}
}