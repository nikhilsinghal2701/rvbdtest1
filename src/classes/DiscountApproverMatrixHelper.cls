public without sharing class DiscountApproverMatrixHelper {
	private static final String STANDARD='Standard';
    
	/**
	 * Returns names of custom fields in Base_Approbal_Matrix object that start with "BM_".
	 */
	private static List<String> getBaseMatrixFieldNames() {
		List<String> fieldNames = new List<String>();
		Map<String,Schema.SObjectField> fields = Base_Approval_Matrix__c.sObjectType.getDescribe().fields.getMap();
		for (Schema.SObjectField field : fields.values()) {
			String fieldName = field.getDescribe().getName();
			if (fieldName.toUpperCase().startsWith('BM_')) {
				fieldNames.add(fieldName);
			}
		}
		return fieldNames;
	}
    
	public static List<String> getAvailableCategories() {
		List<String> result = new List<String>();
		for (String name : getBaseMatrixFieldNames()) {
			if (name.startsWith('BM_Cat_')) {
				result.add(name.substring(7,8));
			}
		}
		result.sort();
		return result;
	}  
    
    
    public static String createQueryFromFields(Id id){
        List<String> stFields = getBaseMatrixFieldNames();
        String fields=null;
        for(integer i=0;i<stFields.size();i++){
           fields=stFields.get(i);
        }
           
        String query='SELECT '+fields +' FROM Base_Approval_Matrix__c WHERE BM_Active__c=true  and Quote__c=:id ';
   	  query=query+ ' ORDER BY BM_Level__c ASC ';  
        //System.debug('Query-:'+query);
        return query ;
        
     }
   
	 public static String getUserRoleNameByUserId(Id userId){
	 	   User uu;
	       for(User u1 :[Select u.UserRole.Name, u.Id From User u where isActive = true and Id = :userId]){
	       		uu=u1;
	       }
	       return uu.UserRole.Name;
	       
	       return null;
	 }
	
	 
	 public static  Id getUserIdByRoleId(Id roleId){
	 	   Map<Id,String> mapUserandRole=new    Map<Id,String>();    
	      for(User u1 :[Select u.UserRole.Name, u.Id From User u where isActive = true and UserRoleId = :roleId]){
	       		String roleName=u1.UserRole.Name+'';
	       		//return mapUserAndRole.put(u1.Id,roleName+'');
	       		return u1.Id;
	       }
		   return null;
	 }
	  /*
	  * Sorts a given String of categories
	  *separates with comma by category
	  *Param: String Categories
	  *Returns:String with comma separated Categories
	  */
	 public static String getSortedCategory(String pCategory){
	 	String strCategory=null;
	 	
	 	if(pCategory!=null && pCategory.length()>0){
	 		List<String> catArrays=new List<String>();
	 		if(!pCategory.contains(',')){
		 		for(integer i=0;i<pCategory.length();i<i++){
		 			catArrays.add(pCategory.substring(i,i+1));
		 			
		 		}
	 		}else{
	 			catArrays = pCategory.split(',');
	 		}
			catArrays.sort();
			
	 		for(integer j=0;j<catArrays.size();j<j++){
	 			if(strCategory!=null){
	 				strCategory=strCategory+','+catArrays.get(j);
	 			}else{
	 				strCategory=catArrays.get(j);
	 			}
	 			
	 		}
	 		
	 	}
		return strCategory;
		
	}	    
	
    private static Map<Id,UserRoleObj> mapAllUsersRoles=new Map<Id,UserRoleObj>();
    
    //gets exception mapping
    //Rohit - 02232010...added rsm to the call to be able to get GEO
    /* Ankita - 12/13/2010
    This method should now take quote as an argument so that we can check the quote segment and the federal nature of the opportunity
    */
    //public static  Map<String,UserRoleObj> getExceptionMapping(Id rsmId) {
    public static  Map<String,UserRoleObj> getExceptionMapping(Quote__c quote, Boolean isFederal, String dSegment) {
    	//Quote__c quote = [select id, RSM__c, Segment__c, Opportunity__r.Federal__c from Quote__c where id = :quoteId];
    	//get the RSM geo
    	String geo = '';
    	if(quote.RSM__c != null){
	    	User rsmUser = [Select Geo__c from user where Id = :quote.RSM__c];
	    	geo = (rsmUser.Geo__c == null) ? '' : rsmUser.Geo__c;
    	}
		Map<String,UserRoleObj> mapUserRoles = new Map<String,UserRoleObj>();
		
		List<Exception_Mapping__c> exceptionMappings = new List<Exception_Mapping__c>();
		//check if Opportunity is federal or not. If federal opp, then no need to check segment
		String quoteSegment = quote.Segment__c == null ? '' : quote.Segment__c;
		if(isFederal || quoteSegment.equalsIgnoreCase(dSegment)){
			exceptionMappings = [Select Exception_User__c,Role__c, Geo__c 
        						From Exception_Mapping__c WHERE Active__c=true And (Geo__c = null OR GEO__c = :geo) and segment__c = null];
		}else{
			exceptionMappings = [Select Exception_User__c,Role__c, Geo__c 
        						From Exception_Mapping__c WHERE Active__c=true And (Geo__c = null OR GEO__c = :geo) 
        						and (Segment__c = null or Segment__c = :quoteSegment)];//added condition for getting additional exception mappings for that segment
		}
        for (Exception_Mapping__c mappingrRec : exceptionMappings){
        	String mappingrRecGeo = (mappingrRec.Geo__c == null) ? '' : mappingrRec.Geo__c;
        	if (mapUserRoles.containsKey(mappingrRec.Role__c)) {
        		if (mappingrRecGeo.equals(geo)) {
		        	UserRoleObj urex=new UserRoleObj();
		        	urex.UserId=mappingrRec.Exception_User__c;
		        	urex.RoleName=mappingrRec.Role__c;
		        	mapUserRoles.put(mappingrRec.Role__c,urex);
        		}
        	} else {
	        	UserRoleObj urex=new UserRoleObj();
	        	urex.UserId=mappingrRec.Exception_User__c;
	        	urex.RoleName=mappingrRec.Role__c;
	        	mapUserRoles.put(mappingrRec.Role__c,urex);
        	}
        }
            
		return mapUserRoles;  
    } 
    
    public static Map<String,UserRoleObj> computeHierarchyByRoleName(Set<Id> rsmIds) {
    	Map<String,UserRoleObj> result = new Map<String,UserRoleObj>();
    	if (mapAllUsersRoles.isEmpty()) {
    		mapAllUsersRoles=loadUsersAndRoles();
    	}
    	
    	for (Id rsmid : rsmIds) {
	    	Id userId = rsmId;
	    	while (userId != null) {
	    		UserRoleObj uro = mapAllUsersRoles.get(userId);
	    		if (uro != null) {
	    			result.put(uro.RoleName, uro);
	    		}
	    		userId = getUserParentId(userId);
	    	}
    	}
    	return result;
    }
    
    private static Id getUserParentId(Id parentId){
    	if(mapAllUsersRoles.size()>0){
    		if(mapAllUsersRoles.get(parentId)!=null)
    		return mapAllUsersRoles.get(parentId).UserManagerId;
    	}
    	return null;
    }
    
    public static Map<Id,UserRoleObj> loadUsersAndRoles(){
    	Map<Id,String> roleNamesById = new Map<Id,String>();
    	for (UserRole ur :[SELECT Id, Name FROM UserRole WHERE PortalType !='Partner']) {
    		roleNamesById.put(ur.Id, ur.Name);
    	}
    	
    	for (User user : [SELECT Id, User_Manager__c, UserRoleId FROM User WHERE UserType = :STANDARD AND IsActive = true AND Approver__c = true]) {
    		UserRoleObj uro = new UserRoleObj();
    		uro.UserId = user.Id;
    		uro.RoleName = roleNamesById.get(user.UserRoleId);
    		uro.UserRoleId = user.UserRoleId;
    		uro.UserManagerId = user.User_Manager__c;
    		mapAllUsersRoles.put(user.Id, uro);		
    	}
    	return mapAllUsersRoles;
    }
    
    /*added by Ankita 5/19/2010 for Singapore routing 
    	Scenarios:
    	1. Delegation out of International into US - the last approver must be in Sinagapore for Singapore Quote
    	2. Quote originated in US but billed in Singapore
    	This method will check the change of previous approver field on the quote. 
    	if prev approver is not null and prevapprover.segment != quote.segment and quote.segment = SG, check if SG-GM is not in routing then add him to the routing 
    */
    public static void recalculateRoutingMatrix(Map<Id,Quote__c> quotes, Map<Id,Quote__c> oldQuotes){
    	MultiOrgSettings__c mProp=MultiOrgSettings__c.getValues('Segment');
    	//get routing matrix for these quotes and check if SG-GM is in routing
    	Map<Id,Id> quoteApproverIdMap = new Map<Id,Id>();
    //	List<User> prevApprover = new List<User>();
    	//identify quotes that are Singapore Opertaing Unit and previous approver has changed
    	for(Quote__c q : quotes.values()){
    		String qSegment = q.segment__c != null ? q.segment__c : '';
    		//if(qSegment.equalsIgnoreCase('Riverbed Operating Unit - SG')){
    		if(qSegment.equalsIgnoreCase(mProp.Segment_Name__c)){
    			/*if(q.Previous_delegated_Approver__c != null){
    				quoteApproverIdMap.put(q.Id,q.Previous_delegated_Approver__c); 
    				continue;
    			}else if((oldQuotes.get(q.Id).Previous_Approver__c != q.Previous_Approver__c) && (q.Previous_Approver__c != null)){
	    			quoteApproverIdMap.put(q.Id,q.Previous_Approver__c);  
	    		}*/
	    		if((oldQuotes.get(q.Id).Approver1__c != q.Approver1__c) && (q.Approver1__c != null)){
	    			quoteApproverIdMap.put(q.Id,q.Approver1__c);  
	    		}
    		}
    	}
    	if(quoteApproverIdMap.size() > 0){
    		Map<Id, Approvers_Routing_Matrix__c> quoteApproversMap = new Map<Id, Approvers_Routing_Matrix__c>();
    		//get routing matrix record for Singapore GM for the above quotes
    		/*
    		for(Approvers_Routing_Matrix__c mx : [select Quote__c,Actual_Role__c from Approvers_Routing_Matrix__c 
    												WHERE Quote__c in :quoteApproverIdMap.keySet() and
    												Actual_Role__c = 'Singapore GM']){*/
    		for(Approvers_Routing_Matrix__c mx : [select Quote__c,Actual_Role__c from Approvers_Routing_Matrix__c 
    												WHERE Quote__c in :quoteApproverIdMap.keySet() and
    												Actual_Role__c =:mProp.Actual_Role__c ]){										
    					quoteApproversMap.put(mx.Quote__c, mx);								
    		}
    		//get segment of previous approver on quote
    		Map<id,User> userMap = new Map<Id, User>([select id, segment__c from User where id in :quoteApproverIdMap.values()]);
    		//Exception_Mapping__c gmRec = [select Exception_User__c,Role__c, Geo__c from Exception_Mapping__c where segment__c = 'Riverbed Operating Unit - SG'];
    		Exception_Mapping__c gmRec = [select Exception_User__c,Role__c, Geo__c from Exception_Mapping__c where segment__c =:mProp.Segment_Name__c];
    		List<Approvers_Routing_Matrix__c> insertList = new List<Approvers_Routing_Matrix__c>();
    		for(Id i : quoteApproverIdMap.keySet()){	
    			//if singapore quote doesnt have SG-GM in routing													
    			if(!quoteApproversMap.containsKey(i)){
    				String approverSegment = '';
    				User prevApprover = userMap.get(quoteApproverIdMap.get(i));
    				if(prevApprover != null){
    					approverSegment = prevApprover.segment__c == null ? '' : prevApprover.segment__c ;
    				}
    				
    				if(!quotes.get(i).Segment__c.equalsIgnoreCase(approverSegment)){
    					//insert SG-GM to routing matrix
    					//insertList.add(new Approvers_Routing_Matrix__c(Quote__c = i, Reference_Role__c = 'Singapore GM', Actual_Role__c = 'Singapore GM', Level__c = 35, user__c = gmRec.Exception_User__c, route__c = true));
    					insertList.add(new Approvers_Routing_Matrix__c(Quote__c = i, Reference_Role__c =mProp.Actual_Role__c, Actual_Role__c =mProp.Actual_Role__c, Level__c = 35, user__c = gmRec.Exception_User__c, route__c = true));
    				}
    			}
    		}
    		if(insertList.size() > 0){
    			insert insertList;
    		}
    	}
    }
    
    public static void deleteOldMatrix(String qId,String status){
    	///insert matrix
    	List<Approvers_Routing_Matrix__c> list_ApproversMatrix=[Select Quote__c,Actual_Role__c FROM Approvers_Routing_Matrix__c WHERE Quote__c=:qId ];
               
        if(list_ApproversMatrix!=null && list_ApproversMatrix.size()>0 && status!=QuoteApprovalHlp.STATUS_APPROVED){
            //delete old
          //delete list_ApproversMatrix;
            Map<String,UserRoleObj> mapException;
            
       }
    }
    
     
    //***********************************Copied from DiscountRouting class-***************************
  /**  private static LineItemData getLineItemData(String qId){
    	System.debug('Quoteid'+qId);

    	return new LineItemData([Select q.Standard_Discount__c, 
                                q.Quote__c, 
                                q.Config_Quantity__c ,
                                q.Non_Standard_Discount__c, 
                                q.Category__c 
                                From Quote_Line_Item__c q
                                where q.Quote__c =: qId]);
    }
    
    
 private class LineItemData {
    private  Map<Id,Map<Id,Quote_Line_Item__c>> lineItems = new Map<Id,Map<Id,Quote_Line_Item__c>> ();
    
    public LineItemData(List<Quote_Line_Item__c> lis){
        for (Quote_Line_Item__c li: lis){
            this.put(li.Quote__c,li);
        }
    }
    public void putAll (List<Quote_Line_Item__c> lis){
        for (Quote_Line_Item__c li: lis){
            this.put(li.Quote__c,li);
            
        }
    }   
    public List<Quote_Line_Item__c> get(String key) {
        Map<Id,Quote_Line_Item__c> result = lineItems.get(key); 
     
        if (result != null){
            return result.values();
        }
        return null;
    }
    public void put(Id qId, Quote_Line_Item__c li) {
        
        Map<Id,Quote_Line_Item__c> tmp = lineItems.get(qId);
        if (tmp == null){
            lineItems.put(qId, new Map<Id, Quote_Line_Item__c>{li.Id => li});
        }else{
            tmp.put(li.Id , li);    
        }           
    } 
    public Set <Id> getQuoteIds(){
        return lineItems.keyset();
        
    }
   
}*/
    
}