/*
*	This class is used to Create RQI or AQI Object
*	RQI is where we grab a case and create an audit for it's Resolution
*	and all articles that are related to the case
*
*	RQI: Audits on closed cases with articles
*	Input: retCases
*	Output: 6 cases for RQI
*			adds list of related cases to Related_Solutions Object
*
*	AQI: Audits on articles
*	Input: retArticles
*	Output: 4 articles for AQI
*/

public with sharing class CreateRQIAQICtrl {

	public String redirectUrl { get; set; }
	public boolean alreadyExist { get; set; }
	public boolean validRequest { get; set; }
	public String  newAuditId { get; set; }
	public CreateRQIAQICtrl() {
		redirectUrl = 'not set';
		alreadyExist = false;
		validRequest = false;		
	}
	
	// create newAudit object for current author
	public PageReference createAudit(){
    	String idToUse = apexpages.currentpage().getparameters().get('id');
    	String authorIdToUse = apexpages.currentpage().getparameters().get('authorId');
    	String cType = apexpages.currentpage().getparameters().get('casesorarticles');
    	String docIdToUse = apexpages.currentpage().getparameters().get('docID');
    	validRequest = true;	
    	if(''.equals(cType) || ''.equals(authorIdToUse) || ''.equals(idToUse) ){
    		validRequest = false;
    	}

    	if(cType.equals('retCases')){
    		
    		// get a list of RQI Audits for current case   		
    		List<RQI_Audit__c> audits = [SELECT Id FROM RQI_Audit__c WHERE Case__c = :idToUse];
    		
    		// return null if audits exists    		
    		if(audits.size()>0){
    			alreadyExist = true;
    			redirectUrl = '/'+ audits[0].Id;
    			return null;
    		}
    		
    		// create new Audit object
    		RQI_Audit__c newRQIAudit = new RQI_Audit__c();
    		
    		// ensure case's KCS is up todate before we audit
    		Case toUpdate = [SELECT Id,KCS_Known_vs_New__c FROM Case WHERE Id = :idToUse];
    		update toUpdate;
    		
    		newRQIAudit.Case__c = idToUse;
    		newRQIAudit.OwnerId = UserInfo.getUserId();
    		newRQIAudit.Author__c = authorIdToUse;
    		newRQIAudit.Status__c = 'New';
    		newRQIAudit.Coach__c = UserInfo.getUserName();
    		
    		Case c = [SELECT Id,KCS_Known_vs_New__c FROM Case WHERE Id = :idToUse];
    		
    		List<RecordType> recType;
    		if(c.KCS_Known_vs_New__c!=null){
	    		if(c.KCS_Known_vs_New__c.equals('New')){
	    			recType = [SELECT Id FROM RecordType WHERE Name='New Case'];
	    			newRQIAudit.RecordTypeId =recType[0].Id;
	    		}else if(c.KCS_Known_vs_New__c.equals('Known')){
	    			recType = [SELECT Id FROM RecordType WHERE Name='Known Case'];
	    			newRQIAudit.RecordTypeId = recType[0].Id;
	    		}else if(c.KCS_Known_vs_New__c.equals('N/A')){
	    			recType = [SELECT Id FROM RecordType WHERE Name='No Solutions'];
	    			newRQIAudit.RecordTypeId = recType[0].Id;
	    		}
    		}else{
    			recType = [SELECT Id FROM RecordType WHERE Name='No Solutions'];
    			newRQIAudit.RecordTypeId = recType[0].Id;
    		}
    		insert newRQIAudit;
    		
    		// create RQI Related Solutions 
    		List<RQI_Related_Solution__c> listOfRelatedArticles = new List<RQI_Related_Solution__c>();
    		Map<String,String> ArticleCreatedBridge = new Map<String,String>();   		
    		for(InQuira_Case_Info__c ca: [Select Id, Related_InQuira_Article__c, Related_Case__c 
    									 FROM InQuira_Case_Info__c 
    									 WHERE Related_Case__c = :idToUse]){
	    		ArticleCreatedBridge.put(ca.Related_InQuira_Article__c, ca.Related_Case__c);
		    }

            // get fields and populate RQI_Related_Solution__c
		    for	(InQuira_Article_Info__c kav: [SELECT Id,Article_Type__c,Title__c,Document_ID__c,Display_URL__c FROM InQuira_Article_Info__c 
		    								  WHERE Id IN (SELECT Related_InQuira_Article__c 
		    								  FROM InQuira_Case_Info__c 
		    								  WHERE Related_InQuira_Article__c = :ArticleCreatedBridge.keySet())]){		    	
				RQI_Related_Solution__c newRQIRelatedArticle = new RQI_Related_Solution__c();
				newRQIRelatedArticle.RQI_Audit__c = newRQIAudit.id;
		        newRQIRelatedArticle.Solution_ID__c = kav.Document_ID__c;
		        newRQIRelatedArticle.Solution_Title__c = kav.Title__c;
		        newRQIRelatedArticle.Solution_Type__c = kav.Article_Type__c;
		        newRQIRelatedArticle.Solution_Link__c = kav.Display_URL__c;
		        listOfRelatedArticles.add(newRQIRelatedArticle);
		    }
		    
		    if(listOfRelatedArticles.size()>0){
		    	insert listOfRelatedArticles;
		    }
    		
		    //get the id of the new audit to redirect the user to it
		    newAuditId=newRQIAudit.Id;
		    redirectUrl = '/'+ newAuditId;
    	}else if (cType.equals('retArticles')){
    		
    		// Get a List of existing AQI from case, if any.
    		List<AQI_Audit__c> audits = [SELECT Id FROM AQI_Audit__c WHERE Solution_ID__c = :idToUse];
    		
    		// check if article exists and return null
    		if(audits.size()>0){
    			alreadyExist = true;
    			redirectUrl = '/'+ audits[0].Id;
    			return null;
    		}
    		    		
    		// Create and populate AQI data
    		AQI_Audit__c newAQIAudit = new AQI_Audit__c();
	    	newAQIAudit.Solution_Id__c = docIdToUse;
	    	newAQIAudit.OwnerId = UserInfo.getUserId();
	        newAQIAudit.Status__c = 'New'; 
	        InQuira_Article_Info__c kav = [Select Title__c,Display_URL__c,Article_Author__c FROM InQuira_Article_Info__c WHERE Id = :idToUse];        
	        newAQIAudit.Author__c = (kav.Article_Author__c != null && getAuthorId(kav.Article_Author__c) != null) ? getAuthorId(kav.Article_Author__c) : authorIdToUse;
	        newAQIAudit.Solution_Link__c = kav.Display_URL__c;
	        newAQIAudit.Solution_Title__c = kav.Title__c;
	        
		    insert newAQIAudit;
		    
		    //get the id of the new audit to redirect the user to it
		    newAuditId=newAQIAudit.Id;
		    redirectUrl = '/'+ newAuditId;
    	}else{
    		//TODO: can't determine cases or articles reload form alert the user
    	}   	
    	return null;
    }
    
    private String getAuthorId(String authorName) {
    	User author = [SELECT Id,Name FROM User WHERE Name = :authorName];
    	return (author.Id == null) ? null : author.Id;
    }	
}