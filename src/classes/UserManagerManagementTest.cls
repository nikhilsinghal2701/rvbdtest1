@isTest
private class UserManagerManagementTest {

    static testMethod void myUnitTest() {
    	List<User> users = new List<User>();
        UserManagerManagement umm;
        Profile profile=[select id,name from profile where name='System Administrator'];      
        User user1=new User();
        user1.FirstName='test1';
        user1.LastName='umm1';
        user1.Alias='tu1';
        user1.Email='test101@test1.com';
        user1.Username='test101@test1.com';
        user1.EmailEncodingKey='ISO-8859-1';
        user1.CurrencyIsoCode='USD';
        user1.TimeZoneSidKey='America/Los_Angeles';
        user1.LocaleSidKey=userInfo.getLocale();
        user1.LanguageLocaleKey='en_US';
        user1.ProfileId=profile.id;
        users.add(user1);
        //insert user1;
        //system.runAs(user1){
        // Open Custom Controller
        /*
		ApexPages.StandardController controller = new ApexPages.StandardController(user1);
		// Open extension
		umm = new UserManagerManagement(controller);
		umm.setUser1(user1);
		umm.getUser();
		umm.setUser2(user2);
		umm.getUser2();
		umm.getAllUser();
		umm.replaceManager();        	
        //}
        */
        User user2=new User();
        user2.FirstName='test2';
        user2.LastName='umm2';
        user2.Alias='tu2';
        user2.Email='test102@test2.com';
        user2.Username='test102@test2.com';
        user2.EmailEncodingKey='ISO-8859-1';
        user2.CurrencyIsoCode='USD';
        user2.TimeZoneSidKey='America/Los_Angeles';
        user2.LocaleSidKey=userInfo.getLocale();
        user2.LanguageLocaleKey='en_US';
        user2.User_Manager__c=user1.id;
        user2.ProfileId=profile.id;
        users.add(user2);
//        insert user2;
        /*
        system.runAs(user2){
        // Open Custom Controller
		ApexPages.StandardController controller = new ApexPages.StandardController(user1);
		// Open extension
		umm = new UserManagerManagement(controller);
		umm.getAllUser();
		umm.replaceManager();        	
        }
        */
        User user3=new User();
        user3.FirstName='test3';
        user3.LastName='umm3';
        user3.Alias='tu3';
        user3.Email='test103@test3.com';
        user3.Username='test103@test3.com';
        user3.EmailEncodingKey='ISO-8859-1';
        user3.CurrencyIsoCode='USD';
        user3.TimeZoneSidKey='America/Los_Angeles';
        user3.LocaleSidKey=userInfo.getLocale();
        user3.LanguageLocaleKey='en_US';
        user3.User_Manager__c=user1.id;
        user3.ProfileId=profile.id;
        users.add(user3);
        //insert user3;
        insert users;
        system.runAs(user3){
        // Open Custom Controller
		ApexPages.StandardController controller = new ApexPages.StandardController(user1);
		// Open extension
		umm = new UserManagerManagement(controller);
		//GenreicObject gObj;
		umm.setUser1(user2);
		umm.getUser1();
		umm.setUser2(user3);
		umm.getUser2();
		umm.getDisplay();
		//gObj.setname(user1.name);
		//gObj.getName();
		//gObj.setAction(ture);
		//gObj.getAction();
		//gObj.setIsGrayBox(true);
		//gObj.getIsGrayBox();
		//gObj.setUserId(user1.Id);
		//gObj.getUserId();
		//List<GenreicObject> userList=new List<GenreicObject>();
		//userList.add(gObj);
		//umm.setLocations(userList);
		//umm.getUsers();
		
		umm.getAllUser();
		umm.replaceManager();        	
        }
        // Open Custom Controller
		ApexPages.StandardController controller = new ApexPages.StandardController(user1);
		// Open extension
		umm = new UserManagerManagement(controller);
		//GenreicObject gObj=new GenreicObject();
		umm.getAllUser();
		umm.replaceManager();
    }  
}