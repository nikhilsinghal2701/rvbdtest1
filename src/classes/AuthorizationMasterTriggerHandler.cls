/*****************************************
	Name : AuthorizationMasterTriggerHandler
	Purpose : Helper class for Trigger AuthorizationMasterTrigger
	Created By : Rashmi ( Perficient ) 11/04/2013
****************************************/
public without sharing class AuthorizationMasterTriggerHandler {

	private static final String COMPETENCY = 'Competency';
	private static final String RTYPE_PARTNER_ACCOUNT_NAME = 'Partner Account';
	private static final string RVBD_SUPPORT_EMAIL = 'rvbd-sfdc-support@riverbed.com';
	private static final string EMAIL_SUBJECT = 'AuthorizationMasterTrigger : Error while Inserting Certification Summary Records';
	private static final string ACCOUNT = 'Account';

	//helper method to create certification summary records whenever the new Authorization master of type competency is created
	public static void createCertificationSummaryRecords( List<Authorizations_Master__c> newAuthorizationMasters ){
		Set<String> competencyAuthorizationMasterNames = new Set<String>();

		// get all authorization master records of type= competency
		for( Authorizations_Master__c authMaster : newAuthorizationMasters ){
			if( authMaster.Type__c == COMPETENCY ){
				competencyAuthorizationMasterNames.add( authMaster.Name );
			}
		}

		// if competency authorization masters list is not empty, query for all partner accounts and insert the certification summary records
		if( !competencyAuthorizationMasterNames.isEmpty() ){
			insertCertificationSummaryRecords( competencyAuthorizationMasterNames );
		}
	}

	// helper method to insert certification Summary records for all partner accounts
	@future
	static void insertCertificationSummaryRecords( Set<String> authMasterNames ){

		List<RecordType> rTypes = [ SELECT Id
									FROM RecordType
									WHERE Name = :RTYPE_PARTNER_ACCOUNT_NAME
									AND SobjectType = :ACCOUNT ];

		if( rTypes != null && !rTypes.isEmpty() ){
			Map<Id, Account> partnerAccounts = new Map<Id, Account>( [ SELECT Id
																		FROM Account
																		WHERE RecordTypeId = :rTypes[0].Id ] );
			// construct the list of certification summary records to insert
			List<Acct_Cert_Summary__c> certSummariesToInsert = new List<Acct_Cert_Summary__c>();
			if( partnerAccounts != null && !partnerAccounts.isEmpty() ){
				for( String authMasterName : authMasterNames ){
					for( Id partnerAccId : partnerAccounts.keySet() ){
						certSummariesToInsert.add( new Acct_Cert_Summary__c( Account__c = partnerAccId, TAP__c = authMasterName ) );
					}
				}
			}

			// if the certificateSummaries To Insert list is not empty, insert the certSummary records
			if( !certSummariesToInsert.isEmpty() ){
				try{
					insert certSummariesToInsert;
				}catch( DmlException ex ){
					handleExceptions( ex, certSummariesToInsert );
				}
			}
		}
	}


	// helper method to handle exceptions
		static void handleExceptions( DmlException ex, List<Acct_Cert_Summary__c> certSummariesToInsert ){
		// catch the exception
		Map<String, List<Id>> authMasterNamesToAccountIdList = new Map<String, List<Id>>();
		String authMasterName;
		Id partnerAccountId;

		// loop through the exceptions list
		for( Integer i = 0; i < ex.getNumDml(); i++ ){
			Integer index = ex.getDmlIndex( i );
			// get the authmaster name that failed to insert certification summary
			authMasterName = certSummariesToInsert[index].TAP__c;

			//get the accountId for which the insertion failed and construct the map of this
			partnerAccountId = certSummariesToInsert[index].Account__c;

			if( authMasterNamesToAccountIdList.containsKey( authMasterName  ) ){
				authMasterNamesToAccountIdList.get( authMasterName ).add( partnerAccountId );
			}else{
				authMasterNamesToAccountIdList.put( authMasterName, new List<Id>{ partnerAccountId } );
			}
		}

		// if the map is not empty, send an email to the SFDC support team indicating
		//there is an error inserting for the each authorization master names in the map - list of account ids
		if( !authMasterNamesToAccountIdList.isEmpty() ){
			sendEmail( authMasterNamesToAccountIdList );
		}
	}


	// helper method to send Exception emails to Riverbed support
	static void sendEmail( Map<String, List<Id>> authMasterNamesToAccountIdList ){
				Messaging.SingleEmailmessage message = new Messaging.SingleEmailMessage();
				message.setToAddresses( new String[] { RVBD_SUPPORT_EMAIL } );
				message.setSubject( EMAIL_SUBJECT );
				String body = ' <html>An error Occurred while inserting the Certification Summary Records for the following accounts with these competencies';

				body += '<table><tr><td> Authorization Master Name </td> <td><AccountIds List></td></tr>';
				for( String authMasterName : authMasterNamesToAccountIdList.keySet() ){
						body += '<tr><td>' + authMasterName + '</td>';
						for( String accId : authMasterNamesToAccountIdList.get( authMasterName ) ){
							body += '<td>' + accId + '</td>';
						}
						body += '</tr>';
				}
				body += '</table></html>';
				message.setHtmlBody( body );
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
	}
}