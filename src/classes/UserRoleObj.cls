public with sharing class UserRoleObj {
	public String roleName {get; set;}
	public Id userId {get; set;}
	public Id userRoleId {get; set;}
	public Id userManagerId {get; set;}
}