@isTest
global class TestBatchFinishEmailUtil implements database.Batchable<sObject>{
    global final string query;

    global TestBatchFinishEmailUtil(){
        query='select AccountId, Id, RVBD_Product_Family__c FROM Asset where RVBD_Product_Family__c!=null limit 1';
    }

    global database.QueryLocator start(database.BatchableContext BC){
        return database.getQueryLocator(query);
    }

    global void execute(database.BatchableContext BC,list<sObject> scope){
    BatchAccountProductSubscriptions accProdSubscription = new BatchAccountProductSubscriptions();
    }

    global void finish(database.BatchableContext BC){
        BatchFinishEmailUtil.finishEmail(BC);
    }
    static testMethod void FinishEmailTest() {
       list<Asset> asts=new list<Asset>();
       Account a = new Account();
        a.Name = 'test account';
        insert a;
    for(integer i=0;i<(math.round(math.random()*200)+1);i++){
        asts.add(new Asset(Name='test'+string.valueof(i),AccountID=a.id));
        }
        insert asts;
        test.startTest();
        id batchInstance=database.executeBatch(new TestBatchFinishEmailUtil());
        test.stopTest();
    } 
}