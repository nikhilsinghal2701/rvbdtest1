/*
 * Class Name : ChangeForecastCategoryListViewController
 * Description: Controller class for "Change Forecast Category List View" VF page
 *              which has the logic to update the forecast category independent of stage 
 *              for multiple selected opportunities.
 * Authour    : Sorna (Perficient)
 * Version    : 1.0
 * Date       : 07/10/2013
 */

public with sharing class ChangeForecastCategoryListViewController
{
    public List<Opportunity> optyList {get;set;}
    public Opportunity o {get;set;}
    public string category {get;set;}
    public List<SelectOption> categoriesList {get;set;}
    public Boolean showFields {get;set;}

    public string RDRVPComments {get;set;}
    public string NextStep {get;set;}
    public string CloseDate {get;set;}
    
    /* Constructor:
     * gets the selected opportunities from the list view; intializes and populates the custom picklist (selectOption) variables
     */
    public ChangeForecastCategoryListViewController(Apexpages.standardsetcontroller controller)
    {
        optyList = controller.getSelected();
        
        if(optyList.size() > 0)
        {
            showFields = true;
            o = new Opportunity();
            category = '';
            categoriesList = ChangeForecastCategoryUtil.getForecastCategoryValues();
        }
        else
        {
            showFields = false;
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select atleast one Opportunity'));
        }
    }
    
    /* SaveOpty:
     * sets the stage and forecast category chosen by the user to all the selected opportunities from list view; 
     * saves the optys and return back to the list view;
     */
    public PageReference saveOpty()
    {
        try
        {
            if(ChangeForecastCategoryUtil.validateCategory(category))
            {
                for(Opportunity opty : optyList)
                {
                    opty.StageName = o.StageName;
                    opty.ForecastCategoryName = category ;
                    
                    opty.NextStep = o.NextStep;
                    opty.CloseDate = o.CloseDate;
                    //opty.RD_RVP_Comments__c = o.RDRVPComments;
                }
                
                update optyList;
                
                return new pageReference(Apexpages.currentPage().getParameters().get('retURL'));
            }
            else
            {
                return null;
            }
        }
        catch(Exception e)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }
}