/*
Class: TestDataFactory
Purpose: Creates data for test class
*/
public without sharing class TestDataFactory {

	// create rvbd email settings
	public  RVBD_Email_Properties__c creatervbdEmail(){
		RVBD_Email_Properties__c rvbdEmail = new RVBD_Email_Properties__c(Name = 'ContactProdSubTriggerOnUpdate',
										mc_org_wide_email_Id__c = 'false');
		return rvbdEmail;
	}
	// create account
	public Account createAccount()
    {
        Id acctRecdId = CertRollup.getRecordType(Constant.ACCOUNT_PARTNER);
        Account acct = new Account(Name = 'tesAcctName', RecordTypeId = acctRecdId, Geographic_Coverage__c = 'Americas',
                        Region__c = 'Federal (US)', Type = Constant.PARTNET_TYPE_VAR, D_B_Country__c='Aland Islands',
                        Partner_Level__c = Constant.PARTNET_LEVEL, Partner_Status1__c = Constant.PARTNET_STATUS_PROBATION);
        return acct;
    }
    
  // create contact
    public Contact createContact(Id accountId, String email, String lName)
    {   RecordType rTypeContact=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Contact' and Name='Standard Contact'];
        Contact contact = new Contact(AccountId = accountId,Email = email,FirstName = lName, LastName = lName, RecordTypeId = rTypeContact.Id);
        return contact;
    }
    
    // query to retrive Partner Competencies 
    public List<Acct_Cert_Summary__c> retriveCertSummary(Id acctId){
        return ([SELECT Id, TAP__c, Account__c,RSS_Needed__c,RTSS_Needed__c,RCSP_Needed__c,TAP_short__c, Status_Tap__c,
                    Roll_Up_RSS__c,Roll_Up_RTSS__c,Roll_Up_RCSP__c,Roll_Up_RCSA_RCSP__c,RCSA_RCSP_Needed__c,RCSP__c,
                    RSA__c,RSS__c,RTSA__c,RTSS__c,RCSA_RCSP__c  
                    FROM Acct_Cert_Summary__c WHERE Account__c =: acctId]);
        
    }
    // retrive account authorization
    public List<Account_Authorizations__c> retriveAcctAuthorization(Id acctId){
        return ([SELECT Id,Status__c,Account__c FROM Account_Authorizations__c WHERE Account__c =: acctId]);
    }
    // create authorization master
    public List<Authorizations_Master__c> createAuthMaster(List<String> authMasterList){
        List<Authorizations_Master__c> list_of_AuthMaster = new List<Authorizations_Master__c>();
        for(String str: authMasterList){
            list_of_AuthMaster.add(new Authorizations_Master__c(Name = str,Description__c = 'test desc',Active_Inactive__c = true,
                                            Type__c = 'Competency'));
        }
        return list_of_AuthMaster;
    }
    // create certificate master
    public Certificate_Master__c createMasterCerts(String certName){
        return (new Certificate_Master__c(Name = certName));
    }
    // create cert
    public Certificate__c createCert(Id pcId, Id contId, String str){
        Date issueDt = Date.today();
        Date expireDt = issueDt.addMonths(3);
        return (new Certificate__c(Name = str, ClassName__c = str, Contact__c = contId, Certification_Summary__c = pcId, Contact_Left_Account__c = 'no',
                            Status__c = 'Complete',Score__c = 'Complete', IssuedOn__c = issueDt, Expiration_Date__c = expireDt));
    }
    // create disti reseller
    public Distributor_Reseller__c createDisti(Id masterAcctId, Id distiAcctId){
        Distributor_Reseller__c disti = new Distributor_Reseller__c(Account__c = masterAcctId, Distributor_Reseller_Name__c = distiAcctId, 
                                        Partner_Type__c = 'VAR');//, Primary_Distributor__c = 
        return disti;
    }
    // retrive Profile
    public Profile retriveProfile(String profileName){
        Profile profile = [SELECT Id FROM Profile WHERE Name =: profileName];
        return profile;
    }
    // create user 
    public User createUser(String email,Id contactId, Id profileId){
        User user = new User(alias = 'test123', email=email,
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',
                ContactId = contactId,
                timezonesidkey='America/Los_Angeles', username=email);
         return user;
    }
    // create probation
    public Probation_Partner_Type__c createProbationPartnerType(String partnerType){
        return (new Probation_Partner_Type__c(Name = partnerType));
    }
    // create email custom settings
    public void createPartnerEmailNotification(){
    	
    	List<Partner_Email_Notification__c> partnerEmailList = new List<Partner_Email_Notification__c>();
    	partnerEmailList.add(new Partner_Email_Notification__c(Name = 'Partners'));
    	partnerEmailList.add(new Partner_Email_Notification__c(Name = 'Distributor'));
    	partnerEmailList.add(new Partner_Email_Notification__c(Name = 'CSM'));
    	insert partnerEmailList;
    }
    // create disti reseller custom setting 
    public void createDistiErrorMsg(){
    	List<DistiResellerList__c> distiMsgList = new List<DistiResellerList__c>();
    	distiMsgList.add(new DistiResellerList__c(Name = 'disti', Message__c = 'disti msg'));
    	distiMsgList.add(new DistiResellerList__c(Name = 'acctId', Message__c = 'acct msg'));
    	insert distiMsgList;
    }
}