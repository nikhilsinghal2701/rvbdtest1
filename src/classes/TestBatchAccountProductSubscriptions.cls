@isTest
private class TestBatchAccountProductSubscriptions { 

static testmethod void testBatchClass() {         
    List <Asset> ast= new List<Asset>();
    Set<String> uniq=new Set<String>();
    Account a = new Account();
    a.Name = 'test account';
    a.Product_Subscriptions__c='test;test:';
    insert a;

    for(integer i = 0; i<200; i++){
    
       Asset a1 = new Asset(Name='testAsset'+i, RVBD_Product_Family__c= 'test'+i+';',Instance_Number__c='77777777XX'+i,AccountID=a.id);
       a1.oracle_product_family__c='test:tes';
       ast.add(a1);
       uniq.add(a1.RVBD_Product_Family__c);
    }
    insert ast;
    String prodstring='';
    
    for(String st : uniq){    
        prodstring = prodstring +';'+ st;   
    }
    a.Product_Subscriptions__c=prodstring;
    
    Test.startTest();
    BatchAccountProductSubscriptions  bc = new BatchAccountProductSubscriptions ();
    bc.error='TestError';
    ID batchprocessid = Database.executeBatch(bc);
    Test.stopTest();
    }
    
public static testMethod void testschedule() {
        test.starttest();
        BatchAccountProdSubscriptionsScheduler sco = new BatchAccountProdSubscriptionsScheduler ();
        String sch = '0 0 23 * * ?';
        system.schedule('BatchAccountProductSubscriptions', sch, sco);
        test.stopTest();
    }
}