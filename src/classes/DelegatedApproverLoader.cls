public without sharing class DelegatedApproverLoader {
    private static final DelegatedApproverLoader INSTANCE = new DelegatedApproverLoader();
    
    private List<Delegation_Detail__c> delegations;
    private Map<Id,List<Delegated_Approver__c>> approversByRequesterId;
    private List<Delegated_Approver__c> delegatedApprovers;
    private boolean initialized = false;
    private Delegated_Approver__c approver;
    
    public DelegatedApproverLoader() {
    }
    
    public static DelegatedApproverLoader getInstance() {
        return INSTANCE;
    }
    
    private void init() {
        delegations = [SELECT Requester__c, Requester__r.Name, 
                        (SELECT Id, User__c, User__r.Name, Geo__c, Region__c FROM Delegated_Approvers__r WHERE Active__c = true AND Start_Date__c <= TODAY AND End_Date__c >= TODAY) 
                       FROM Delegation_Detail__c
                       WHERE Active__c = true];
        approversByRequesterId = new Map<Id,List<Delegated_Approver__c>>();
        delegatedApprovers = new List<Delegated_Approver__c>();
        for (Delegation_Detail__c delegation : delegations) {
            List<Delegated_Approver__c> approvers = approversByRequesterId.get(delegation.Requester__c);
            if (approvers == null) {
                approvers = new List<Delegated_Approver__c>();
                approversByRequesterId.put(delegation.Requester__c, approvers);
            }
            for (Delegated_Approver__c approver : delegation.Delegated_Approvers__r) {
                approvers.add(approver);
                delegatedApprovers.add(approver);
            }
        }
        System.debug(LoggingLevel.INFO,'*** Delegated Approver Cache: ' + approversByRequesterId);
        initialized = true;
    }
    
    public Delegated_Approver__c findDelegatedApprover(Id requesterId, String geo, String region) {
        if (!initialized) {
            init();
        }
        
        List<Delegated_Approver__c> approvers = approversByRequesterId.get(requesterId);
        System.debug(LoggingLevel.INFO,'**** Requester: ' + requesterId + '; Approvers: ' + approvers);
        if ((approvers == null) || approvers.isEmpty()) {
            return null;
        }
        
        System.debug(LoggingLevel.INFO,'prev delegated approver = ' + approver);
        /*if(approver!= null && (requesterId == approver.User__c)){//commented to resolve Double delegation issue by psingh@riverbed.com on 4/23/2014
            System.debug(LoggingLevel.INFO,'Requester same as previous approver ' );
            return approver;
        }*/

        if ((geo != null) && (region != null)) {
            // First, look for most specific delegation (ie: geo and region)
            approver = findByGeoAndRegion(approvers, geo, region);
        }
        if ((approver == null) && (geo != null)) {
            // Next, look for just Geo
            approver = findByGeo(approvers, geo);
        }
        if (approver == null) {
            // Finally, grab the first global delegation (if exists)
            approver = findByGeoAndRegion(approvers, null, null);
            if (approver == null) {
                return null;
            }
        }
        System.debug(LoggingLevel.INFO,'approver = ' + approver);
        // Recursively check if delegated approver found had delegated to another person
        Delegated_Approver__c nested = findDelegatedApprover(approver.User__c, geo, region);
        if (nested == null) {
            return approver;
        } else {
            return nested;
        }
    }
    
    private Delegated_Approver__c findByGeoAndRegion(List<Delegated_Approver__c> approvers, String geo, String region) {
        for (Delegated_Approver__c approver : approvers) {
            if ((approver.Geo__c == geo) && (approver.Region__c == region)) {
                return approver;
            }
        }
        return null;
    }
    
    private Delegated_Approver__c findByGeo(List<Delegated_Approver__c> approvers, String geo) {
        for (Delegated_Approver__c approver : approvers) {
            if (approver.Geo__c == geo) {
                return approver;
            }
        }
        return null;
    }
}