public with sharing class OutstandingEvals {
     public List<Asset> OutEvalList {get;set;}
     public List<opportunity> opptylList {get;set;}
     public String opptyId;
     public Boolean message {get; set;}
     Id actId;
     
     public OutstandingEvals(ApexPages.StandardController controller)
     
    {
        opptyId = ApexPages.currentpage().getParameters().get('id');
        actId= [select AccountId from opportunity where id=:opptyId ].AccountId;
        OutEvalList = [Select id,name,SerialNumber,IB_Status__c,SKU__c,asset.Opportunity__r.ID,InstallDate,Opportunity__r.Account.Name from asset where IB_Status__c='Under Evaluation' and AccountId =:actId ];
    }
    
    public Void ValidateOpty()
    {
    	If (OutEvalList.size()>0)
    	{message=false;}
    	else
    	{message=true;}
    }
}