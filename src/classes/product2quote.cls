/*
*This class is used for forcasting
*when the user clickes the "user forcasting button on quote" product line items on the oppty gets
*overrided by quote line items forcasting check box should checked.
Author: Anil Madithati
Created Date:06/27/2010 
Updated Date:08/19/2010
Update Reason:To resolve "Too many SOQL exception" Issue.
Code modified by Santoshi on Nov-15-11 to optimize the code and make one update.
*/

public with sharing class product2quote {
    List <OpportunityLineItem> oplist= new List <OpportunityLineItem>();
    public Quote__c quote { get; private set;}   
    public PricebookEntry pe { get; private set;} 
    public Opportunity opp { get; private set; }  
   
    public OpportunityLineItem[] opplineitems { get; private set;} 
    public Quote__c[] quoteitems { get; private set;}
    public Quote_Line_Item__c[] quotelineitems { get; private set;}
    Quote__C pageobj;
    Profile p;
    
    public PageReference product2quotes() {
        p = [select id from Profile where name = 'System Administrator'];
        String st=Getproduct2quotes();
            string a =ApexPages.currentPage().getParameters().get('a');
        if(st=='success') {
        if(a=='1'){
      
            return  new pagereference('/'+opp.id);
           }
           else
           {
            return  new pagereference('/'+quote.id);
            }
            
        }
        
        
        
        else {
            return new pagereference('/apex/errorpage?id='+quote.id);
        }        
    }

    public product2quote(ApexPages.StandardController controller) {
        
    }    
    private ApexPages.StandardController controller;
    String quoteid;
    
    public void Setproduct2quotes(String ids) {
        quoteid=ids;
    } 
    public String Getproduct2quotes() {
        quoteid = ApexPages.CurrentPage().getParameters().get('id');
        //System.debug('This is quoteid:'+ quoteid);
        String st=queryOppId(quoteid);
        return st;
    }
   
    public String queryOppId(String qid){        
        quote =[select Uplift_Calculation__c,id,Opportunity__c, Opportunity__r.stagename,Opportunity__r.Pricebook2Id,Opportunity__r.Uplift_Calculation__c from Quote__c where id=:qid];
        //System.debug('This is opp id:'+ quote.Opportunity__c);
        //opp=[select id,StageName from Opportunity where Id=:quote.Opportunity__c];
        opp= new Opportunity(Id=quote.Opportunity__c);//fix for line no.66 and # 274999 issue
        if((quote.Opportunity__r.stagename=='6 - Order Accepted' || quote.Opportunity__r.stagename=='7 - Closed (Not Won)') && UserInfo.getProfileId() != p.id){ // sys Admin id:'00e30000000bqt9AAA'
            quote.addError('Opportunity stage is greater than 6');
            return 'error';
        }else{
           try{
                opplineitems =[Select o.Id from OpportunityLineItem o  where o.OpportunityId=:quote.Opportunity__c];
                delete(opplineitems);
                quotelineitems =[Select id,q.Unit_Price__c, q.Unit_List_Price__c, q.Qty_Ordered__c,q.SKU_Reference__c, q.Product_Code__c, q.Product2__c, q.D_Unit_Price__c, q.D_Ext_Price__c From Quote_Line_Item__c q where Quote__c=:quote.id order by q.Product2__c];
                List<ID> productIdList = new List<ID>();
                for (Quote_Line_Item__c q : quoteLineItems) {
                    productIdList.add(q.Product2__c);
                }
                if(quote.Opportunity__r.Pricebook2Id==null)
                {
                quote.Opportunity__r.Pricebook2Id=[select Id from pricebook2 where isactive = true].Id;
                //quote.Opportunity__r.Pricebook2Id='01s30000000038qAAA';
                //update(quote.Opportunity__r); Commented by Nikhil. Doing update at Uplifyt calculation time.
                }
                quote.Opportunity__r.Uplift_Calculation__c = quote.Uplift_Calculation__c; //Add by nikhil: set Uplift calculation
                update(quote.Opportunity__r); //update Opportunity.
                
                List<PricebookEntry> priceBookEntries = [Select id,Productcode, Product2Id, Pricebook2Id From PricebookEntry
                                                     where pricebook2id = :quote.Opportunity__r.Pricebook2Id and
                                                     product2id in :productIdList and isactive = true];   
                Map<String,Id> productIdToPricebookEntryIdMap = new Map<string,Id>();
                for(PricebookEntry p : pricebookEntries) {
                    productIdToPricebookEntryIdMap.put(p.productcode, p.id);
                }
                for(Quote_Line_Item__c qlt: quotelineitems){
                   OpportunityLineItem opl= new OpportunityLineItem();
                   opl.PricebookEntryId= productIdToPricebookEntryIdMap.get(qlt.Product_Code__c);
                   opl.Quantity = qlt.Qty_Ordered__c;
                   opl.Description =qlt.Product_Code__c;
                   opl.UnitPrice = qlt.D_Unit_Price__c;
                   opl.R_Sales_Price__c=qlt.Unit_Price__c;
                   opl.OpportunityId=quote.Opportunity__c;
                   opl.SKU_Reference__c =qlt.SKU_Reference__c;
                   oplist.add(opl);
                }
                insert oplist;
                Set<Id> qidSet = new Set<Id>();
                quoteitems =[select id,Forecasted_Quote__c from Quote__c where Opportunity__c=:quote.Opportunity__c and Forecasted_Quote__c=true];
                for(Quote__c qc: quoteitems){
                    qc.Forecasted_Quote__c=false;
                    qidSet.add(qc.Id);
                    if(qc.Id==quote.Id) // Code modified by Santoshi on Nov-15-11 to optimize the code and make one update.
                    quote.Forecasted_Quote__c=True;
                }
                if(!qidSet.contains(quote.Id))
                {
                    quote.Forecasted_Quote__c=True;
                    quoteitems.add(quote);
                }
                update quoteitems; 
                System.debug('Updating Quote');
             //   quote.Forecasted_Quote__c=True;
            //    update(quote);   
           }catch(Exception e){
               ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Fatal , 'Please contact system administrator' + e);
               ApexPages.addMessage(msg);
               return 'error';           
           }
        return 'success';                   
        }
    }            
}