/*Need to update all contacts owner same as account. needed to clean up data, initially for the data migration.
**by: prashant.singh@riverbed.com
**date:7/2/2012
*/
global without sharing class BatchAccountContactOwnerSync implements Database.Batchable<sObject> {

    global String Query;
    global BatchAccountContactOwnerSync(){      
        Query = 'SELECT Id, OwnerId,(Select Id,OwnerId from Contacts) from Account WHERE (IsDeleted = False)';  
    }

   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);
   }
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Contact> ContactToUpdate = new List<Contact>();
        for(sObject so : scope) {
            Account a = (Account)so;
            for(Contact temp:a.Contacts){
                if(a.OwnerId!=temp.OwnerId){
                    temp.OwnerId=a.OwnerId;
                    ContactToUpdate.add(temp);
                }   
            }
        }
        if(ContactToUpdate.size()>0 && !ContactToUpdate.isEmpty()){
            try{
                update ContactToUpdate;
            }catch(System.DMLException dme){
                system.debug('Updating Contacts owner record same as account owner:'+dme.getmessage());
            }
        }
   }
   global void finish(Database.BatchableContext BC){

   }   
}