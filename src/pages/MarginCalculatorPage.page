<!----------------------------------------------------------------------------------------------------------
Author: Santoshi Mishra
Purpose:The page will display Margin Calculator Analysis.
Created Date : Nov 2011
-------------------------------------------------------------------------------------------------------------->

<apex:page title="Margin Analysis" standardController="Quote__c" extensions="MarginCalculatorPageExtension">
    <apex:form >
    <apex:outputPanel rendered="{!error}" >
    	<apex:pageMessage summary="{!errorMsg}" severity="{!severe}" strength="3"/>
    	<apex:commandButton action="{!cancel}" value="Ok"/>
    </apex:outputPanel>
    
        <apex:pageBlock title="Margin Analysis for {!quote.Name}" rendered="{!NOT(error)}">
        <apex:pagemessages id="err"/>
            <apex:pageBlockButtons >
                <apex:commandButton value="Calculate" action="{!calculate}" rerender="margin1,margin2,margin3,err"/>
                <apex:commandButton value="Add Products" action="{!addProducts}"/>
                <apex:commandButton value="Remove Products" action="{!removeProducts}"/>
                <apex:commandButton value="Email" action="{!email}"/>
                <apex:commandButton value="Reset" action="{!reset}"/>
                <apex:commandButton value="Cancel" action="{!cancel}"/>
                <apex:commandButton value="Show All Lines" action="{!showNonCOGSItems}" rendered="{!showNonCogs}"/>
                <apex:commandButton value="Show only Costed Lines" action="{!hideNonCOGSItems}" rendered="{!NOT(showNonCogs)}"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="Discount/Pricing Summary" columns="1">
            	<apex:pageblocktable value="{!marginSummaryInnerClassList}" var="ms" id="margin1" width="300px"  cellspacing="5" border="5" cellpadding="10" align="center"  frame="box">
            	<apex:column >
            	<apex:facet Name="header">
            	<div style="text-align:center">
            	Group Name
            	</div>
            	 	</apex:facet>
            	 	<apex:outputtext rendered="{!if(ms.Name=='Total', true,false)}">
            	 	 <b> {!ms.Name}</b>
            	 	</apex:outputtext>
            	 	
            	 		<apex:outputtext rendered="{!!if(ms.Name=='Total', true,false)}">
            	 		{!ms.Name} 
            	 	</apex:outputtext>
            	</apex:column>
            	<apex:column value="{!ms.RequestedDiscount}"  style="text-align:right">
            	<apex:facet Name="header">
            	<div style="text-align:center">
            	Requested Discount
            	</div>
            	 	</apex:facet>
            	</apex:column>
            	
            	<apex:column value="{!ms.ScenarioDiscount}" style="text-align:right">
            	<apex:facet Name="header" >
            	<div style="text-align:center">
            	Scenario Discount
            	</div>
           	 	</apex:facet>
            	</apex:column>
            	
            	<apex:column value="{!ms.DiscountChange}" style="text-align:right">
            	<apex:facet Name="header" >  
            	<div style="text-align:center"> 
            	Discount Change
				</div>
           	 	</apex:facet>
            	</apex:column>
            	
            	<apex:column style="text-align:right">
            	<apex:facet Name="header" >
            	<div style="text-align:center">
            	Requested Ext Price
				</div>
           	 	</apex:facet>
           	 	
           	 	 <apex:outputText value="USD {0,number,#,###.##}" style="text-align:right">
                    	<apex:param value="{!ms.RequestedExtPrice}"/>
                    </apex:outputText>
            	</apex:column>
            	
            	<apex:column style="text-align:right">
            	<apex:facet Name="header">
            	<div style="text-align:center">
            	Scenario Ext Price
            	</div>
           	 	</apex:facet>
           	 	<apex:outputText value="USD {0,number,#,###.##}" style="text-align:right">
                    	<apex:param value="{!ms.ScenarioExtPrice}"/>
                    </apex:outputText>
            	</apex:column>
            	<apex:column style="text-align:right">
            	<apex:facet Name="header" >
            	<div style="text-align:center">
            	Ext Price Change
            	</div>
           	 	</apex:facet>
           	 	<apex:outputText value="USD {0,number,#,###.##}" style="text-align:right">
                    	<apex:param value="{!ms.ExtPriceChange}"/>
                    </apex:outputText>
           	 	
            	</apex:column>
            	
            	</apex:pageblocktable>
             
            </apex:pageBlockSection>            
            <apex:pageBlockSection title="Margin Summary" columns="1">
            <apex:pageblocktable value="{!marginSummaryInnerClassList}" var="ms" id="margin2" width="300px"  cellspacing="5" border="5" cellpadding="10" align="center"  frame="box">
            	<apex:column style="text-align:center">
            	<apex:facet Name="header">
            	<div style="text-align:center">
            	Group Name
            	</div>
            	 	</apex:facet>
            	 	<apex:outputtext rendered="{!if(ms.Name=='Total', true,false)}">
            	 	 <b> {!ms.Name} Margin</b>
            	 	</apex:outputtext>
            	 	
            	 		<apex:outputtext rendered="{!!if(ms.Name=='Total', true,false)}">
            	 		Margin {!ms.Name} 
            	 	</apex:outputtext>
            	</apex:column>
            	
            	<apex:column value="{!ms.RequestedMargin}%" style="text-align:right">
            	<apex:facet Name="header">
            	<div style="text-align:center">
            	Requested
            	</div>
            	 	</apex:facet>
            	</apex:column>
            	
            	<apex:column style="text-align:right">
            	<apex:facet Name="header">
            	<div style="text-align:center">
            	What If Scenario
            	</div>
            	 	</apex:facet>
            	 	
            	 		<apex:outputtext rendered="{!showcolumns}">
            	 		{!ms.WhatIfScenarioMargin}%
            	 	</apex:outputtext>
            	</apex:column>
            	
            	<apex:column value="{!ms.DifferenceInMargin}" style="text-align:right">
            	<apex:facet Name="header">
            	<div style="text-align:center">
            	Change
            	</div>
            	 	</apex:facet>
            	</apex:column> 
            
            </apex:pageblocktable> 
            
                
            </apex:pageBlockSection>
            <apex:pageBlockSection title="What-if Scenario" columns="1" id="margin3">
                <apex:outputLabel value="* These products do not have cost information in the system. Margin values for these will show up as 100%"/>
                <apex:pageBlockTable value="{!scenarioLines}" var="q" columns="10">
                    <apex:column headerValue="Select">
                        <apex:inputCheckbox value="{!q.checked}"/>
                    </apex:column>
                    <apex:column >
                    	<apex:facet name="header">Product Code</apex:facet>
                    	<apex:outputText value="{!q.qLine.Product_Code__c}"/>
                    	<apex:outputLabel value="*" rendered="{!q.noCost}" style="font-weight:bold"/>
                    </apex:column>
                    <apex:column value="{!q.qLine.Qty_Ordered__c}"/>
                    <apex:column >
                        <apex:facet name="header">Scenario Quantity</apex:facet>
                        <apex:inputField value="{!q.qLine.Scenario_Quantity__c}"/>
                    </apex:column>
                    <apex:column >
                    	<apex:facet name="header">Quoted Discount</apex:facet>
                    	<apex:outputText value="{!q.qLine.Disti_Approved_Discount__c}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Scenario Discount</apex:facet>
                        <apex:inputField value="{!q.qLine.Scenario_Discount__c}" rendered="{!IF(q.qLine.Category__c == 'F', false, true)}"/>
                    </apex:column>
                    <apex:column headerValue="Extended Price" value="{!q.qLine.D_Ext_Price__c}">
                    	
                    </apex:column>
                    <apex:column value="{!q.qLine.Scenario_Extended_Price__c}"/>
                    <apex:column value="{!q.qLine.Margin_Percent__c}"/>
                    <apex:column value="{!q.qLine.Scenarion_Margin_Percent__c}"/>
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>